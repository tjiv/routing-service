#include "DBManager.h"
#include "Timer.h"
#include <iomanip>
#include <iostream>
#include <memory>
#include <pqxx/pqxx>
#include <sstream>
#include <string>
#include <thread>
#include <unordered_map>
#include <string.h>

#include "../utils/Logger.h"
#include "../utils/ConcurrentHashMap.h"
#include "../utils/Configuration.h"

//用于保存客户端的上一次的寻找路径
static ConcurrentHashMap<string, pqxx::result> previous_path;

DBManager &DBManager::getInstance()
{
	static DBManager instance;
	return instance;
}

//给定一个点 在路段中寻找离给定点最近的目标点的下标并返回
int find_nearest_point(const std::pair<double, double> &start_pos, const pqxx::result &pq)
{
	//ST_X ST_Y分别代表point在result表格中的列
	const int ST_X = 1, ST_Y = 2;
	auto distance_comput = [](const std::pair<double, double> &pos1, const std::pair<double, double> &pos2) -> double
	{
		return (pos1.first - pos2.first) * (pos1.first - pos2.first) + (pos1.second - pos2.second) * (pos1.second - pos2.second);
	};

	int minimum_index = 0;
	double minimum_dis = distance_comput(std::make_pair(pq[0][ST_X].as<double>(), pq[0][ST_Y].as<double>()), start_pos);
	for (int i = 0; i < pq.size(); ++i)
	{
		double tmp_dis = distance_comput(std::make_pair(pq[i][ST_X].as<double>(), pq[i][ST_Y].as<double>()), start_pos);
		if (tmp_dis < minimum_dis)
		{
			minimum_dis = tmp_dis;
			minimum_index = i;
		}
	}
	return minimum_index;
}

//从索引位置  在给定路段中进行寻找  确定所有点包含的路径id
set<long> find_path_segment(const int index, const pqxx::result &pq)
{
	set<long> S;
	//在数据库中的路段id
	const int SEG_ID = 0;
	for (int i = index; i < pq.size(); ++i)
	{
		S.insert(pq[i][SEG_ID].as<long>());
	}
	return std::move(S);
}

//在参数传递时 使用std::forward
int DBManager::compute_cost(const std::string &map, const std::set<long> &&S)
{
	//首先生成查询字符串
	string query_str = "select sum(cost) as cost from topo_map where l_id in (";
	char buffer[50];
	for (auto it = S.begin(); it != S.end(); ++it)
	{
		memset(buffer, 0, sizeof(buffer));
		sprintf(buffer, "%ld", *it);

		if (it == S.begin())
			query_str += string(buffer);
		else
			query_str += ", " + string(buffer);
	}
	query_str += ");";

	auto pConn = pool.getConnection(map);
	if (pConn == nullptr)
	{
		LOG(ERROR) << "database connection disconnect";
		return -1;
	}
	pConn->transaction();
	pqxx::result cost = pConn->qeury(query_str);
	if (!pConn->getStatus())
		return -1;
	//关闭transcation
	pConn->commit();
	return static_cast<int>(cost[0][0].as<double>());
}

int DBManager::requestUpdateReferenceRoad(const std::string& client_id, const std::string &map,
										  const std::pair<double, double> &start_pos, const std::pair<double, double> &end_pos,
										  std::vector<pqxx::result, std::allocator<pqxx::result>> &results_vec)
{
	//首先根据客户端id获取上一次的路径
	bool is_find = previous_path.value_find(client_id);
	//若没找到则不存在比较问题
	if (!is_find)
		return -1;
	pqxx::result tmp;
	pqxx::result comp_path = previous_path.value_for(client_id, tmp);

	//确定出原始路上 当前点的索引
	int point_index = find_nearest_point(start_pos, comp_path);
	//获得原始路占据的路段id
	set<long> &&S = find_path_segment(point_index, comp_path);
	//获得当前路径的cost
	int pre_cost = compute_cost(map, std::forward<set<long>>(S));

	//获得更新路径 -- 和findreference相同
	auto pConn = pool.getConnection(map);
	if (pConn == nullptr)
	{
		LOG(ERROR) << "database connection disconnect";
		return -1;
	}
	
	/*																								------- by czh
	pConn->transaction();
	pConn->qeury("drop table if exists shortpath");

	std::ostringstream sql;
	//默认小数点后7位
	sql.setf(std::ios::fixed);
	sql.precision(7);
	sql << "select pgr_fromatob('topo_map', " << start_pos.first << ", "
		<< start_pos.second << ", " << end_pos.first << ", " << end_pos.second
		<< ") as pgr_fromatontob into temp shortpath";
	pConn->qeury(sql.str());
	//这里可能找不到最短路径
	// if (!pConn->getStatus()) return -1;
	pConn->qeury("select line2points('shortpath')");
	pqxx::result points = pConn->qeury("select * from result_path_points");
	pqxx::result cost = pConn->qeury("select calculate_cost('topo_map')");
	int cur_cost = static_cast<int>(cost[0][0].as<double>());
	*/
	//																								--------by czh 
	pConn->transaction();
	std::ostringstream sql;
	//默认小数点后7位
	sql.setf(std::ios::fixed);
	sql.precision(7);
	//sql函数名tiev_fromAtoB，返回cost
	sql << "select tiev_fromAtoB('topo_map', " << start_pos.first << ", "
		<< start_pos.second << ", " << end_pos.first << ", " << end_pos.second<< ") ";
	pqxx::result cost =pConn->qeury(sql.str());
	pqxx::result points = pConn->qeury("select * from result_path_points");
	if (!pConn->getStatus())
		return -1;
	pConn->commit();														
	int cur_cost = static_cast<int>(cost[0][0].as<double>());
	//when the cur_cost less than 0, don't do the following process
	if (cur_cost < 0)
		return cur_cost;

	//当cur_cost小于pre_cost到阈值时  阈值放进配置文件
	//首先从配置文件中获取阈值
	int cost_threshold = ConfigMananger::getInstance().getConfigByNameAs<int>("cost_threshold");
	if (cur_cost < pre_cost - cost_threshold)
	{
		results_vec.push_back(points);
		//更新ConcurrentHashMap
		previous_path.add_or_update_mapping(client_id, points);
		return cur_cost;
	}
	return pre_cost;
}

int DBManager::findReferenceRoad(const std::string &id, const std::string &map, 
	const std::pair<double, double> &start_pos, const std::pair<double, double> &end_pos,
	std::vector<pqxx::result, std::allocator<pqxx::result>> &results_vec)
{
	
	auto pConn = pool.getConnection(map);
	if (pConn == nullptr)
	{
		LOG(ERROR) << "database connection disconnect";
		return -1;
	}
	Timer t;
	/*																								------- by czh
	pConn->transaction();
	pConn->qeury("drop table if exists shortpath");

	std::ostringstream sql;
	//默认小数点后7位
	sql.setf(std::ios::fixed);
	sql.precision(7);
	
	sql << "select pgr_fromatob('topo_map', " << start_pos.first << ", "
		<< start_pos.second << ", " << end_pos.first << ", " << end_pos.second
		<< ") as pgr_fromatontob into temp shortpath";
	pqxx::result cost =pConn->qeury(sql.str());
	//这里可能找不到最短路径
	// if (!pConn->getStatus()) return -1;
	pConn->qeury("select line2points('shortpath')");
	pqxx::result points = pConn->qeury("select * from result_path_points");
	pqxx::result cost = pConn->qeury("select calculate_cost('topo_map')");
	if (!pConn->getStatus())
		return -1;
	//关闭transcation
	pConn->commit();
	*/
	//																								--------by czh
	pConn->transaction();
	std::ostringstream sql;
	sql.setf(std::ios::fixed);
	sql.precision(7);
	//sql函数名tiev_fromAtoB，直接返回cost
	sql << "select tiev_fromAtoB('topo_map', " << start_pos.first << ", "
		<< start_pos.second << ", " << end_pos.first << ", " << end_pos.second<< ") ";
	pqxx::result cost = pConn->qeury(sql.str());
	/*
		cost的返回值：
			-2：输入的起始点20、终点15米内无地图拓扑：距离地图过远
			-3：dijk返回为空，可能因为当前距终点附近地图点很近，理论上只要汽车在终点附近几米内调用规划就会触发
			以上两种情况，后续输出的点表均为空
		todo，以上两种返回值在上层应有对应的提醒，告知planner？尤其是-3?
	*/
	pqxx::result points = pConn->qeury("select * from result_path_points");
	if (!pConn->getStatus())
		return -1;
	pConn->commit();
	results_vec.push_back(points);
	int ret_cost = static_cast<int>(cost[0][0].as<double>());
	if(ret_cost < 0)
		return ret_cost;
	//存入ConcurrentMap  未查到点则不要放入map中
	previous_path.add_or_update_mapping(id, points);

	//按照要求需要转换成int
	LOG(INFO) << "use " << t.elapsed() << "ms" << endl;
	return ret_cost;
}

int DBManager::findReferenceRoadBlocked(const std::string &id, 
	const std::string &map, const std::pair<double, double> &blocked_point,
	const std::pair<double, double> &end_pos,
	std::vector<pqxx::result, std::allocator<pqxx::result>> &results_vec)
{
	auto pConn = pool.getConnection(map);
	if (pConn == nullptr)
	{
		LOG(ERROR) << "database connection disconnect";
		return -1;
	}
	pConn->transaction();
	std::stringstream sql;
	sql.setf(std::ios::fixed);
	sql.precision(7);
	/*
		下一行的sql可能为空，目前已略做处理，使其作用于后续query，输出特殊值
		以及原本用的是pgr_updateTopoMap函数，todo仍需明确需求
	*/
	sql << "select pgr_pointoffset('topo_map', " << blocked_point.first				
		<< ", " << blocked_point.second << ")";
	pqxx::result new_point = pConn->qeury(sql.str());
	if (!pConn->getStatus())
		return -1;
	double new_start_lon = new_point[0][0].as<double>(),
		   new_start_lat = new_point[1][0].as<double>();

	/*																								------- by czh
	sql.str("");
	sql << "select pgr_fromatob('topo_map', " << new_start_lon << ", "
		<< new_start_lat << ", " << end_pos.first << ", " << end_pos.second
		<< ") as pgr_fromatontob into temp shortpath";
	pConn->qeury(sql.str());
	//这里可能找不到最短路径
	// if (!pConn->getStatus()) return -1;
	pConn->qeury("select * from line2points('shortpath')");
	pqxx::result points = pConn->qeury("select * from result_path_points");
	pqxx::result cost = pConn->qeury("select calculate_cost('topo_map')");
	if (!pConn->getStatus())
		return -1;
	//关闭transcation
	pConn->commit();
	results_vec.push_back(points);
	*/
	//																								------- by czh
	sql.str("");
	sql << "select tiev_fromAtoB('topo_map', " << new_start_lon << ", "
		<<new_start_lat << ", " << end_pos.first << ", " << end_pos.second<< ") ";
	pqxx::result cost =pConn->qeury(sql.str());
	pqxx::result points = pConn->qeury("select * from result_path_points");
	if (!pConn->getStatus())
		return -1;
	pConn->commit();
	results_vec.push_back(points);
	int ret_cost = static_cast<int>(cost[0][0].as<double>());
	if(ret_cost < 0)
		return ret_cost;
	
	//存入ConcurrentMap
	previous_path.add_or_update_mapping(id, points);

	//按照要求需要转换成int
	return ret_cost;
}

pqxx::result DBManager::getClosestSemanticPoint(
	const std::string &map, const std::pair<double, double> &task_point)
{
	pqxx::result point;
	auto pConn = pool.getConnection(map);
	if (pConn == nullptr)
	{
		LOG(ERROR) << "database connection disconnect";
		point.clear();
		return point;
	}
	std::ostringstream sql;
	sql.setf(std::ios::fixed);
	sql.precision(7);
	sql << "select st_x(geom), st_y(geom), utmx, utmy, heading from "
		   "all_points_test order by geom <-> 'SRID=4326;POINT("
		<< task_point.first << " " << task_point.second << ")'::geometry limit 1";
	pConn->transaction();
	point = pConn->qeury(sql.str());
	if (!pConn->getStatus())
	{
		point.clear();
		return point;
	}
	pConn->commit();
	return point;
}