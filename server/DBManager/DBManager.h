#ifndef DB_MANAGER_H
#define DB_MANAGER_H

#include <pqxx/pqxx>
#include <vector>
#include <set>

#include "../utils/ConnectionPool.h"

class DBManager
{
private:
	ConnectionPool pool;
	DBManager() = default;
	DBManager(const DBManager &) = delete;
	DBManager &operator=(const DBManager &) = delete;

public:
	static DBManager &getInstance();
	//返回-1表示不存在一条路线，>0表示路径代价，结果添加至vector
	int findReferenceRoad(
		const std::string &, const std::string &, const std::pair<double, double> &,
		const std::pair<double, double> &,
		std::vector<pqxx::result, std::allocator<pqxx::result>> &);

	int findReferenceRoadBlocked(
		const std::string &, const std::string &, const std::pair<double, double> &,
		const std::pair<double, double> &,
		std::vector<pqxx::result, std::allocator<pqxx::result>> &);

	int requestUpdateReferenceRoad(const std::string &, const std::string &,
								   const std::pair<double, double> &, const std::pair<double, double> &,
								   std::vector<pqxx::result, std::allocator<pqxx::result>> &);

	int compute_cost(const std::string &map, const std::set<long> &&S);

	//如果未找到，返回空result
	pqxx::result getClosestSemanticPoint(const std::string &, const std::pair<double, double> &);
};

#endif