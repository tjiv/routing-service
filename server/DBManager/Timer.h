#ifndef TIMER
#define TIMER
#include <iostream>
#include <chrono>
using namespace std;

class Timer {  
private:  
    chrono::time_point<chrono::high_resolution_clock> m_begin;  
public:  
    Timer() : m_begin(chrono::high_resolution_clock::now()) {}; 
  
    void reset()
    {
        m_begin = chrono::high_resolution_clock::now();
    }
  
    //默认以ms形式输出  
    template<class Duration = chrono::milliseconds>  
    int64_t elapsed() const  
    {  
        return chrono::duration_cast<Duration>(chrono::high_resolution_clock::now() - m_begin).count();  
    }  
  
    //以us形式输出  
    int64_t elapsed_us() const  
    {  
        return elapsed<chrono::microseconds>();  
    }  
  
    //以ns形式输出  
    int64_t elapsed_ns() const  
    {  
        return elapsed<chrono::nanoseconds>();  
    }  
  
    //以s形式输出  
    int64_t elapsed_s() const  
    {  
        return elapsed<chrono::seconds>();  
    }  
  
    //以min形式输出  
    int64_t elapsed_min() const  
    {  
        return elapsed<chrono::minutes>();  
    }  
  
    //以hour形式输出  
    int64_t elapsed_hour() const  
    {  
        return elapsed<chrono::hours>();  
    }  
};  

#endif