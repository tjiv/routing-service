#ifndef LOGGING_INTERCEPTOR_H
#define LOGGGIN_INTERCEPTOR_H

#include <grpcpp/support/server_interceptor.h>

#include <string>

class LoggingInterceptor : public grpc::experimental::Interceptor {
 private:
  grpc::experimental::ServerRpcInfo* info;
 public:
  //每次RPC创建时将info传入
  LoggingInterceptor(grpc::experimental::ServerRpcInfo*);
  //重载拦截方法
  void Intercept(grpc::experimental::InterceptorBatchMethods*) override;
};

//抽象工厂模式，创建服务器时提供给不同类型的具体工厂，然后对于每一个RPC通过工厂创建对应的Interceptor
class LoggingInterceptorFactory
    : public grpc::experimental::ServerInterceptorFactoryInterface {
 public:
  grpc::experimental::Interceptor* CreateServerInterceptor(
      grpc::experimental::ServerRpcInfo*) override;
};

#endif