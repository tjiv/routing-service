#include "LoggingInterceptor.h"

#include <grpcpp/server_context.h>

#include "glog/logging.h"

LoggingInterceptor::LoggingInterceptor(grpc::experimental::ServerRpcInfo* info_)
    : info(info_) {}

void LoggingInterceptor::Intercept(
    grpc::experimental::InterceptorBatchMethods* methods) {
  //发送到服务器的数据
  //获取RPC方法名和调用者
  if (methods->QueryInterceptionHookPoint(
          grpc::experimental::InterceptionHookPoints::
              POST_RECV_INITIAL_METADATA)) {
    LOG(INFO) << "RPC received from: " << info->server_context()->peer()
              << ", method: " << info->method();
  }
  //服务器发出去的数据
  //报告返回的错误
  if (methods->QueryInterceptionHookPoint(
          grpc::experimental::InterceptionHookPoints::PRE_SEND_STATUS)) {
    auto status = methods->GetSendStatus();
    if (!status.ok())
      LOG(ERROR) << "RPC failed, method: " << info->method()
                 << ", message: " << status.error_message();
  }
  //最后放行
  methods->Proceed();
}

grpc::experimental::Interceptor*
LoggingInterceptorFactory::CreateServerInterceptor(
    grpc::experimental::ServerRpcInfo* info) {
  return new LoggingInterceptor(info);
}