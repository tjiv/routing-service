#ifndef MAP_SERVICE_IMPL_H
#define MAP_SERVICE_IMPL_H

#include "../../protos/routing_service.grpc.pb.h"

using grpc::ServerContext;
using grpc::ServerReader;
using grpc::Status;

using routing_service::MapService;
using routing_service::Point;
using routing_service::RoadPoints;
using routing_service::TaskPoint;
using routing_service::TaskRequest;

using google::protobuf::Empty;
using google::protobuf::StringValue;

class MapServiceImpl final : public MapService::Service {
  Status UpdateCarInfo(ServerContext*, ServerReader<routing_service::CarInfo>*,
                       Empty*) override;
  Status GetOneAvailableCar(ServerContext*, const Point*,
                            routing_service::CarInfo*) override;
  Status GetCarInfoById(ServerContext*, const StringValue*,
                        routing_service::CarInfo*) override;
  Status SendTaskPoint(ServerContext*, const TaskRequest*,
                       RoadPoints*) override;
  Status WaitForTaskPoint(ServerContext*, const Empty*, TaskPoint*) override;
};
#endif