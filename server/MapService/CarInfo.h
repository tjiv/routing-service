#ifndef CAR_INFO_H
#define CAR_INFO_H
#include <glog/logging.h>

#include <atomic>
#include <chrono>
#include <condition_variable>
#include <iomanip>
#include <memory>
#include <pqxx/pqxx>
#include <string>
#include <thread>

#include "../utils/ConcurrentHashMap.h"
#include "routing_service.grpc.pb.h"

using routing_service::TaskPoint;
/*
    存储车辆信息，以IP+PORT作为索引
*/

struct CarInfo {
  std::string id;   // IP+PORT
  std::string map;  //所使用地图
  double lon, lat;  //位置
  bool have_task;   //表示是否有任务
  std::condition_variable
      cv_have_task;  //之所以使用调节变量，是因为车辆可能会等待较长时间
  std::mutex mutex_have_task;  //供条件变量使用的互斥量
  std::atomic<bool>
      running;  //记录车辆状态，临时停车、初始状态都属于false，行驶中是true
  std::chrono::time_point<std::chrono::steady_clock>
      time_stamp;  //时间戳，记录上一次操作到现在的时间，用于清理无用信息
  TaskPoint task_point;  //记录下一个任务点
  CarInfo(const std::string& _id = "", const std::string& _map = "",
          const bool& _running = false, const double& _lon = -200,
          const double& _lat = -200)
      : id(_id),
        map(_map),
        lon(_lon),
        lat(_lat),
        running(_running),
        have_task(false),
        time_stamp(std::chrono::steady_clock::now()) {}
};

class CarInfoList {
 private:
  ConcurrentHashMap<std::string, std::shared_ptr<CarInfo>> list;
  CarInfoList() {
    // 定时启动清理线程
    std::thread clearCarInfoThread([this] {
      while (true) {
        std::this_thread::sleep_for(std::chrono::seconds(40));
        this->removeTimeOutCarInfo();
      }
    });
    clearCarInfoThread.detach();
  }
  ~CarInfoList() = default;
  CarInfoList(const CarInfoList&) = delete;
  CarInfoList& operator=(const CarInfoList&) = delete;

 public:
  static CarInfoList& getInstance() {
    static CarInfoList instance;
    return instance;
  }

  std::shared_ptr<CarInfo> getCarInfoById(const std::string& id) {
    auto p = list.value_for(id, nullptr);
    return p;
  }

  std::shared_ptr<CarInfo> getFirstAvailableCar() {
    return list.find_if(
        [](const std::shared_ptr<CarInfo>& p) {
          if (p->running == true) return false;
          std::lock_guard<std::mutex> lk(p->mutex_have_task);
          return !p->have_task;
        },
        nullptr);
  }

  void addOrUpdateCarInfo(const std::string& id,
                          const routing_service::CarInfo& info) {
    auto p = getCarInfoById(id);
    if (p == nullptr) {
      p = std::make_shared<CarInfo>(id);
    }
    p->map = info.map();
    auto pos = info.pos();
    p->lon = pos.lon();
    p->lat = pos.lat();
    p->running = info.running();
    p->time_stamp = std::chrono::steady_clock::now();
    list.add_or_update_mapping(id, p);
    LOG(INFO) << "update car info, id: " << id << std::fixed
              << std::setprecision(7) << ", lon: " << pos.lon()
              << ", lat: " << pos.lat()
              << ", running: " << (info.running() ? "true" : "false");
  }

  //清理超时的CarInfo
  void removeTimeOutCarInfo() {
    //超过这个时间间隔就会被清理
    list.remove_if([](const std::shared_ptr<CarInfo>& p) {
      // TODO: 应该以function模板封装，这样不用重新计算interval和now
      auto interval = std::chrono::milliseconds(20000);
      auto now = std::chrono::steady_clock::now();
      auto distance = std::chrono::duration_cast<std::chrono::milliseconds>(
          now - p->time_stamp);
      return distance > interval;
    });
  }
};

#endif