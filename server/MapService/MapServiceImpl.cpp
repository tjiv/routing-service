#include "MapServiceImpl.h"

#include "../DBManager/DBManager.h"
#include "CarInfo.h"

using grpc::StatusCode;

Status MapServiceImpl::UpdateCarInfo(
	ServerContext *context, ServerReader<routing_service::CarInfo> *reader,
	Empty *response)
{
	std::string id = context->peer();
	routing_service::CarInfo info;
	auto &list = CarInfoList::getInstance();
	while (reader->Read(&info))
		list.addOrUpdateCarInfo(id, info);
	return Status::OK;
}

Status MapServiceImpl::GetOneAvailableCar(ServerContext *context,
										  const Point *request,
										  routing_service::CarInfo *response)
{
	// TODO: 改为最近的
	auto &list = CarInfoList::getInstance();
	auto p = list.getFirstAvailableCar();
	if (p == nullptr)
		return Status(StatusCode::NOT_FOUND, "no available car");
	response->set_id(p->id);
	response->set_map(p->map);
	auto pos = response->mutable_pos();
	pos->set_lon(p->lon);
	pos->set_lat(p->lat);
	response->set_running(p->running);
	return Status::OK;
}

Status MapServiceImpl::GetCarInfoById(ServerContext *context, const StringValue *request, routing_service::CarInfo *response)
{
	auto &list = CarInfoList::getInstance();
	auto p = list.getCarInfoById(request->value());
	if (p == nullptr)
		return Status(StatusCode::DATA_LOSS, "can't find car info");
	response->set_id(p->id);
	response->set_map(p->map);
	auto pos = response->mutable_pos();
	pos->set_lon(p->lon);
	pos->set_lat(p->lat);
	response->set_running(p->running);
	return Status::OK;
}

Status MapServiceImpl::SendTaskPoint(ServerContext *context, const TaskRequest *request, RoadPoints *response)
{
	TaskPoint task_point;
	std::string map = request->map();
	// 1. 获取任务点在语义地图上最近的点，作为任务点
	auto &dbm = DBManager::getInstance();
	pqxx::result r = dbm.getClosestSemanticPoint(
		request->map(), {request->point().lon(), request->point().lat()});
	if (r.affected_rows() == 0)
		return Status(StatusCode::INVALID_ARGUMENT, "can't find closest semantic point");
	task_point.set_lon(r[0][0].as<double>());
	task_point.set_lat(r[0][1].as<double>());
	task_point.set_utmx(r[0][2].as<double>());
	task_point.set_utmy(r[0][3].as<double>());
	task_point.set_heading(r[0][4].as<double>());
	task_point.set_on_or_off(request->on_or_off());
	// 2. 将任务点填入CarInfo
	double car_lon, car_lat;
	auto &list = CarInfoList::getInstance();
	auto p = list.getCarInfoById(request->id());
	if (p == nullptr)
		return Status(StatusCode::DATA_LOSS, "can't find car info");
	//重新检查状态
	if (p->running == true)
		return Status(StatusCode::PERMISSION_DENIED, "already has a task");
	car_lon = p->lon;
	car_lat = p->lat;
	//使用互斥量更新
	{
		std::lock_guard<std::mutex> lk(p->mutex_have_task);
		if (p->have_task)
			return Status(StatusCode::PERMISSION_DENIED, "already has a task");
		p->have_task = true;
		p->task_point = task_point;
	}
	//唤醒等待的进程
	p->cv_have_task.notify_one();
	// 3. 等待直到running更新
	auto stamp = std::chrono::steady_clock::now();
	while (!p->running)
	{
		std::this_thread::sleep_for(std::chrono::seconds(1));
		auto sleep_time = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now() - stamp).count();
		if (sleep_time > 30)
			return Status(StatusCode::INTERNAL, "time out on waiting car to start task");
	}
	// 4. 计算路径规划并返回
	std::string id = context->peer();
	std::vector<pqxx::result, std::allocator<pqxx::result>> results_vec;
	int cost = dbm.findReferenceRoad(id, map, {car_lon, car_lat},  {task_point.lon(), task_point.lat()}, results_vec);
	if (cost == -1)
		return Status(StatusCode::INTERNAL, "can't find reference road to task point");
	//原来是写入文件，这里变为写入response
	pqxx::result path_points_result = results_vec[0];
	for (auto iter = path_points_result.begin(); iter != path_points_result.end(); iter++)
	{
		//对每一行
		auto point = response->add_point();
		point->set_lon(iter->at(1).as<double>());
		point->set_lat(iter->at(2).as<double>());
	}
	return Status::OK;
}

Status MapServiceImpl::WaitForTaskPoint(ServerContext *context,	const Empty *request, TaskPoint *response)
{
	std::string id = context->peer();
	auto &list = CarInfoList::getInstance();
	auto p = list.getCarInfoById(id);
	if (p == nullptr)
		return Status(StatusCode::DATA_LOSS, "can't find car info");
	std::unique_lock<std::mutex> lk(p->mutex_have_task);
	p->cv_have_task.wait(lk, [&p]
						 { return p->have_task; });
	p->have_task = false;
	response->set_lon(p->task_point.lon());
	response->set_lat(p->task_point.lat());
	response->set_utmx(p->task_point.utmx());
	response->set_utmy(p->task_point.utmy());
	response->set_heading(p->task_point.heading());
	response->set_on_or_off(p->task_point.on_or_off());
	return Status::OK;
}