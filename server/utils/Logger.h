#ifndef LOGGER_H
#define LOGGER_H

#include <glog/logging.h>
#include <grpcpp/grpcpp.h>

//将grpc内部的error log转到glog中
static void grpc_error_to_glog(gpr_log_func_args *args) {
  if (GPR_LOG_SEVERITY_ERROR == args->severity)
    LOG(ERROR) << "gRPC internal error occurred, in file: " << args->file
               << ", at line: " << args->line
               << ", with message: " << args->message;
}
#endif