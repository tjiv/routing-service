#include "ConnectionPool.h"

#include <iostream>

#include "Configuration.h"

ConnectionPoolInternal::ConnectionPoolInternal(const std::string& _connect_url)
    : connect_url(_connect_url) {
  //创建初始连接
  for (size_t i = 0; i < initSize; ++i) {
    Connection* p = new Connection(connect_url);
    p->refreshAliveTime();
    connectionQueue.push(p);
    ++connectionCnt;
  }

  //启动连接生产者线程
  std::thread producer(
      std::bind(&ConnectionPoolInternal::produceConnection, this));
  producer.detach();  //线程分离

  //连接回收线程
  std::thread scanner(std::bind(&ConnectionPoolInternal::scanConnection, this));
  scanner.detach();
}

void ConnectionPoolInternal::produceConnection() {
  while (true) {
    std::unique_lock<std::mutex> lk(queueMutex);
    //到队列空时创建新连接
    cv.wait(lk, [this] { return this->connectionQueue.empty(); });
    //到达上限前可以继续创建
    if (connectionCnt < maxSize) {
      Connection* p = new Connection(connect_url);
      p->refreshAliveTime();
      connectionQueue.push(p);
      ++connectionCnt;
    }
    //通知等待连接使用的消费者线程
    cv.notify_all();
    //到这里自动释放锁
  }
}

std::shared_ptr<Connection> ConnectionPoolInternal::getConnection() {
  std::unique_lock<std::mutex> lk(queueMutex);
  if (connectionQueue.empty()) {
    //当队列为空，需要等待（超时时间之内）
    if (std::cv_status::timeout ==
        cv.wait_for(lk, std::chrono::milliseconds(connectionTimeout))) {
      if (connectionQueue.empty()) {
        //超时，且没有连接可用返回空指针
        LOG(ERROR) << "connection timeout";
        return nullptr;
      }
    }
  }

  //给用户使用的connection智能指针，需要修改默认的删除器，不是直接delete掉，而是
  //返还给连接池
  std::shared_ptr<Connection> pConn(
      connectionQueue.front(), [this](Connection* p) {
        // 如果是不健康的连接，直接抛弃
        if(p->is_open()){
          //注意线程安全
          std::lock_guard<std::mutex> lk(this->queueMutex);
          p->refreshAliveTime();
          this->connectionQueue.push(p);
        }else{
          delete p;
        }
      });
  connectionQueue.pop();
  //当拿的是最后一个连接，通知生产者生产新连接
  if (connectionQueue.empty()) cv.notify_all();

  //如果连接中断，则返回nullptr，
  if(pConn->is_open())
    return pConn;
  return nullptr;
}

void ConnectionPoolInternal::scanConnection() {
  while (true) {
    //定时回收
    std::this_thread::sleep_for(std::chrono::seconds(maxIdleTime));

    //从头扫描队列，将超过maxIdleTime的线程回收
    std::lock_guard<std::mutex> lk(queueMutex);
    while (connectionCnt > initSize) {
      auto p = connectionQueue.front();
      //对头的连接没有超时，后面的一定不会超时
      if (p->getAliveTime() < maxIdleTime * 1000) break;
      connectionQueue.pop();
      --connectionCnt;
      delete p;
    }
    //到这里释放锁
  }
}

ConnectionPool::ConnectionPool() {
  //从配置中加载
  auto& cm = ConfigMananger::getInstance();
  user = cm.getConfigByNameAs<std::string>("user");
  password = cm.getConfigByNameAs<std::string>("password");
  host = cm.getConfigByNameAs<std::string>("host");
  port = cm.getConfigByNameAs<std::string>("port");
}

std::shared_ptr<Connection> ConnectionPool::getConnection(
    const std::string& dbname) {
  //获取ConncetionPoolInternal
  auto p = list.value_for(dbname, nullptr);
  if (p == nullptr) {
    //创建新ConnectionPoolInternal
    std::string connect_url = "dbname=" + dbname + " user=" + user +
                              " password=" + password + " hostaddr=" + host +
                              " port=" + port;
    p = std::make_shared<ConnectionPoolInternal>(connect_url);
    list.add_or_update_mapping(dbname, p);
  }
  //获取连接
  return p->getConnection();
}