/*
    修改自《C++并发编程实战》一书
    多读者单写者模型的哈希表
*/
#ifndef CONCURRENT_HASH_MAP_H
#define CONCURRENT_HASH_MAP_H
#include <functional>
#include <list>
#include <memory>
#include <shared_mutex>

template <typename Key, typename Value, typename Hash = std::hash<Key>>
class ConcurrentHashMap {
 private:
  //内部类：桶
  class bucket_type {
   public:
    typedef std::pair<Key, Value> bucket_value;
    typedef std::list<bucket_value> bucket_data;
    typedef typename bucket_data::iterator bucket_iterator;
    typedef typename bucket_data::const_iterator bucket_const_iterator;

    bucket_data data;
    mutable std::shared_mutex
        mutex;  //虽然破坏了const约束，但是mutex必须是可修改的

    bucket_const_iterator find_entry_for(const Key& key) const {
      return std::find_if(data.begin(), data.end(),
                          [&key](const bucket_value& item) {
                            return item.first == key;
                          });  //不是multi_map
    }

    bucket_iterator find_entry_for(const Key& key) {
      return std::find_if(data.begin(), data.end(),
                          [&key](const bucket_value& item) {
                            return item.first == key;
                          });  //不是multi_map
    }

    //没找到返回default_value，下同
    Value value_for(const Key& key, const Value& default_value) const {
      std::shared_lock<std::shared_mutex> lock(mutex);
      const bucket_const_iterator found_entry = find_entry_for(key);
      return (found_entry == data.end()) ? default_value : found_entry->second;
    }

    //没找到返回false，下同
    bool value_find(const Key& key) const {
      std::shared_lock<std::shared_mutex> lock(mutex);
      const bucket_const_iterator found_entry = find_entry_for(key);
      return (found_entry == data.end()) ? false : true;
    }

    void add_or_update_mapping(const Key& key, const Value& value) {
      std::unique_lock<std::shared_mutex> lock(mutex);  //单写者
      const bucket_iterator found_entry = find_entry_for(key);
      if (found_entry == data.end())
        data.push_back(bucket_value(key, value));
      else
        found_entry->second = value;
    }

    void remove_mapping(const Key& key) {
      std::unique_lock<std::shared_mutex> lock(mutex);
      const bucket_iterator found_entry = find_entry_for(key);
      //这里如果找不到不抛出错误
      if (found_entry != data.end()) data.erase(found_entry);
    }
  };

  std::vector<std::unique_ptr<bucket_type>> buckets;
  Hash hasher;

  bucket_type& get_bucket(const Key& key) const {
    const size_t bucket_index = hasher(key) % buckets.size();
    return *buckets[bucket_index];
  }

 public:
  typedef Key key_type;
  typedef Value mapped_type;
  typedef Hash hash_type;

  ConcurrentHashMap(unsigned num_buckets = 19, const Hash& hasher_ = Hash())
      : buckets(num_buckets), hasher(hasher_) {
    for (unsigned i = 0; i < num_buckets; ++i)
      buckets[i].reset(new bucket_type);
  }
  ConcurrentHashMap(const ConcurrentHashMap&) = delete;
  ConcurrentHashMap& operator=(const ConcurrentHashMap&) = delete;

  Value value_for(const Key& key, const Value& default_value) const {
    return get_bucket(key).value_for(key, default_value);
  }

  void add_or_update_mapping(const Key& key, const Value& value) {
    get_bucket(key).add_or_update_mapping(key, value);
  }

  void remove_mapping(const Key& key) { get_bucket(key).remove_mapping(key); }

  //删去满足谓词的元素
  void remove_if(bool (*pred)(const Value&)) {
    // 必须按照同一顺序上锁，否则会有死锁
    for (unsigned i = 0; i < buckets.size(); ++i) {
      std::unique_lock<std::shared_mutex> lock(buckets[i]->mutex);
      auto& list = buckets[i]->data;
      std::remove_if(
          list.begin(), list.end(),
          [&list, &pred](const typename bucket_type::bucket_value& item) {
            return pred(item.second);
          });
    }
  }

  //找到第一个满足谓词的元素
  Value find_if(bool (*pred)(const Value&), const Value& default_value) const {
    for (unsigned i = 0; i < buckets.size(); ++i) {
      std::unique_lock<std::shared_mutex> lock(buckets[i]->mutex);
      auto& list = buckets[i]->data;
      auto p = std::find_if(
          list.begin(), list.end(),
          [&list, &pred](const typename bucket_type::bucket_value& item) {
            return pred(item.second);
          });
      if (p != list.end()) return (*p).second;
    }
    return default_value;
  }

  //没找到返回false
  bool value_find(const Key& key) const {
    return get_bucket(key).value_find(key);
  }
};
#endif