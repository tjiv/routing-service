--
-- PostgreSQL database dump
--

-- Dumped from database version 12.6 (Ubuntu 12.6-1.pgdg18.04+1)
-- Dumped by pg_dump version 13.2 (Ubuntu 13.2-1.pgdg18.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: tiger; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA tiger;


ALTER SCHEMA tiger OWNER TO postgres;

--
-- Name: tiger_data; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA tiger_data;


ALTER SCHEMA tiger_data OWNER TO postgres;

--
-- Name: topology; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA topology;


ALTER SCHEMA topology OWNER TO postgres;

--
-- Name: SCHEMA topology; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA topology IS 'PostGIS Topology schema';


--
-- Name: fuzzystrmatch; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS fuzzystrmatch WITH SCHEMA public;


--
-- Name: EXTENSION fuzzystrmatch; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION fuzzystrmatch IS 'determine similarities and distance between strings';


--
-- Name: hstore; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS hstore WITH SCHEMA public;


--
-- Name: EXTENSION hstore; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION hstore IS 'data type for storing sets of (key, value) pairs';


--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


--
-- Name: pgrouting; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pgrouting WITH SCHEMA public;


--
-- Name: EXTENSION pgrouting; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pgrouting IS 'pgRouting Extension';


--
-- Name: postgis_tiger_geocoder; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS postgis_tiger_geocoder WITH SCHEMA tiger;


--
-- Name: EXTENSION postgis_tiger_geocoder; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis_tiger_geocoder IS 'PostGIS tiger geocoder and reverse geocoder';


--
-- Name: postgis_topology; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS postgis_topology WITH SCHEMA topology;


--
-- Name: EXTENSION postgis_topology; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis_topology IS 'PostGIS topology spatial types and functions';


--
-- Name: calculate_cost(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.calculate_cost(tbl character varying) RETURNS double precision
    LANGUAGE plpgsql STRICT
    AS $$ 
declare 
    start_point geometry;
    end_point geometry;
    start_gid integer;
    end_gid integer;
    start_line geometry;
    end_line geometry;
    start_line_cost double precision;
    end_line_cost double precision;

    per_start float;
    per_end float;
    
    sum_cost double precision;
begin
    --execute 'select st_startpoint(*) from shortpath' into start_point;
    --execute 'select st_endpoint(*) from shortpath' into end_point;
    select st_startpoint(pgr_fromatontob) from shortpath into start_point;
    select st_endpoint(pgr_fromatontob) from shortpath into end_point;

    -- 有可能出现多个查询结果, 使用最近搜索.
    -- 这个查询语句非常不优雅, 但是牵扯到excute的用法, 目前只能这样
    execute '
	SELECT gid, the_geom, cost FROM '||tbl||'
	WHERE ST_DWithin(geog, geography( (select st_startpoint(pgr_fromatontob) from shortpath) ), 0.5)
    ORDER BY ST_Distance(geog, geography( (select st_startpoint(pgr_fromatontob) from shortpath) )) limit 1'
    INTO start_gid, start_line, start_line_cost;
    execute'
    SELECT gid, the_geom, cost FROM '||tbl||'
	WHERE  ST_DWithin(geog, geography( (select st_endpoint(pgr_fromatontob) from shortpath) ), 0.5)
    ORDER BY ST_Distance(geog, geography( (select st_endpoint(pgr_fromatontob) from shortpath) )) limit 1'
    INTO end_gid, end_line, end_line_cost;
    
    -- 为了解决可能出现的重复路段id, 对table edge_id进行处理
    delete from edge_id where ctid not in (select min(ctid) from edge_id group by edge);
    
    drop table if exists path_cost;
    execute '
    create table path_cost as select t.gid, t.cost from '||tbl||' t, edge_id e
    where t.gid = e.edge;';

    select ST_LineLocatePoint(start_line, start_point) into per_start;
    select ST_LineLocatePoint(end_line, end_point) into per_end;

    select sum(cost) from path_cost where gid != start_gid and gid != end_gid
    into sum_cost;
    
    if start_gid = end_gid then
        sum_cost := (per_end-per_start) * start_line_cost;
    else
        sum_cost := sum_cost + (1-per_start)*start_line_cost + per_end*end_line_cost;
    end if;
    RAISE NOTICE 'start gid: %; end gid: %',start_gid,end_gid;
    RAISE NOTICE 'start per: %; end per: %',per_start,per_end;
    RAISE NOTICE 'sum cost: %',sum_cost;

    -- 初始化记录的edge id
    drop table if exists edge_id; --存储寻路的id
    create table edge_id(edge int);
    
    return sum_cost;
end;
$$;


ALTER FUNCTION public.calculate_cost(tbl character varying) OWNER TO postgres;

--
-- Name: line2points(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.line2points(tbl character varying) RETURNS void
    LANGUAGE plpgsql STRICT
    AS $$ 
declare 
	ii integer; -- 循环次数
	n integer;
	point_temp geometry; --path节点
	longtitude double precision;
	latitude double precision;

begin
	-- 根据short path的顺序依次获取节点
	drop table if exists result_points_tbl;
	CREATE TABLE result_points_tbl(id integer,point geometry,lon double precision ,lat double precision);

	execute 'select ST_NPoints(pgr_fromatontob) from ' || tbl|| ' ' into n;

	ii:=1;
	FOR ii IN 1..n LOOP	-- shaungfu: begin with num 1 instead of 0

	execute 'SELECT ST_PointN(pgr_fromatontob, ' ||ii|| ') FROM ' ||tbl|| ' ' into point_temp;
		   
	SELECT st_x(point_temp) into longtitude;
	SELECT st_y(point_temp) into latitude;
	insert into result_points_tbl(id,point,lon,lat) values(ii,point_temp,longtitude,latitude);
	end loop;

	-- 选取shortpath缓冲区的的属性点.
	-- 空间关系由buffer改为intersects, 节省时间..
	-- drop table if exists point_in_buffer;
	-- create table point_in_buffer as SELECT *  FROM all_points
	-- WHERE ST_Intersects(geom, (SELECT * FROM shortpath));

	-- drop table if exists point_in_buffer;
	-- create table point_in_buffer as SELECT *  FROM all_points
	-- WHERE ST_DWithin(geography(geom), geography((SELECT * FROM shortpath)), 0.5);
	
	drop table if exists point_in_buffer;
	create table point_in_buffer as SELECT * FROM all_points
    WHERE ST_Within(geom, (SELECT ST_Buffer((SELECT * FROM shortpath),0.000001,'endcap=round join=round')));

	-- 选取shortpath 0.5m缓冲区内的所有停止点.
	drop table if exists stop_point_in_buffer;
	create table stop_point_in_buffer as SELECT * FROM stop_points 
    WHERE ST_DWithin(geography(geom), geography((SELECT * FROM shortpath)), 0.5);
	-- 选取shortpath 0.5m缓冲区内的所有起始点.
	drop table if exists begin_point_in_buffer;
	create table begin_point_in_buffer as SELECT * FROM begin_points 
    WHERE ST_DWithin(geography(geom), geography((SELECT * FROM shortpath)), 0.5);
	

	-- 通过最近邻搜索, 获取属性点.
	-- 这里隐含了对result_path_points_meta的排序(以result_points_tbl为准)
	drop table if exists result_path_points_meta;
	create table result_path_points_meta as 
	SELECT p.id, b.p_id, p.lon, p.lat, b.utmx, b.utmy ,b.heading, b.curv, b.mode, b.speed_mode, 
	b.opposite_side_Mode, b.event_mode, b.lane_num, b.lane_seq, b.lane_width, p.point
	FROM result_points_tbl p 
	CROSS JOIN LATERAL ( 
	SELECT * 
	FROM point_in_buffer r 
	ORDER BY r.geom <-> p.point
	LIMIT 1 
	)b;

	-- 搜索停止点id
	drop table if exists stop_point_candidate;
	create table stop_point_candidate as 
	SELECT b.id
	FROM stop_point_in_buffer p 
	CROSS JOIN LATERAL ( 
	SELECT * FROM result_path_points_meta r 
	ORDER BY p.geom <-> r.point LIMIT 1 )b;
	-- 搜索起始点(路口结束, 新路段开启点)
	drop table if exists begin_point_candidate;
	create table begin_point_candidate as 
	SELECT b.id
	FROM begin_point_in_buffer p 
	CROSS JOIN LATERAL ( 
	SELECT * FROM result_path_points_meta r 
	ORDER BY p.geom <-> r.point LIMIT 1 )b;
	-- 更新停止点
	-- 这个操作导致了停止点信息被改变, 排到了表的末尾.	停止线是3
	update result_path_points_meta set event_mode = 0 where event_mode = 1 or event_mode = 2;
	update result_path_points_meta a set event_mode = 1 from stop_point_candidate b where a.id = b.id;
	update result_path_points_meta a set event_mode = 2 from begin_point_candidate b where a.id = b.id;

	-- 存入最后的结果, 并排序.
	drop table if exists result_path_points;
	create table result_path_points as 
	select p.p_id, p.lon, p.lat, p.utmx, p.utmy, p.heading, p.curv, p.mode, p.speed_mode,
	p.event_mode, p.opposite_side_Mode, p.lane_num, p.lane_seq, p.lane_width
	from result_path_points_meta p order by p.id;

end;
$$;


ALTER FUNCTION public.line2points(tbl character varying) OWNER TO postgres;

--
-- Name: line2points(character varying, double precision, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.line2points(tbl character varying, distance double precision, link_points_tbl character varying) RETURNS void
    LANGUAGE plpgsql STRICT
    AS $$ 
declare 
	ii integer; -- 循环次数
	n integer;
	point_temp geometry;--内插点 
	longtitude double precision;
	latitude double precision;
	id_temp integer;
	 
	begin
	drop table if exists result_points_tbl;
	CREATE TABLE result_points_tbl(id integer,point geometry,lon double precision ,lat double precision);

	execute 'select ST_NPoints(pgr_fromatob) from ' || tbl|| ' ' into n;

	ii:=1;
	FOR ii IN 1..n LOOP

	execute 'SELECT ST_PointN(pgr_fromatob, ' ||ii|| ') FROM ' ||tbl|| ' ' into point_temp;
		   
	SELECT st_x(point_temp) into longtitude;
	SELECT st_y(point_temp) into latitude;
	insert into result_points_tbl(id,point,lon,lat) values(ii,point_temp,longtitude,latitude);
	end loop;
end;
$$;


ALTER FUNCTION public.line2points(tbl character varying, distance double precision, link_points_tbl character varying) OWNER TO postgres;

--
-- Name: pgr_fromatob(character varying, double precision, double precision, double precision, double precision); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.pgr_fromatob(tbl character varying, startx double precision, starty double precision, endx double precision, endy double precision) RETURNS public.geometry
    LANGUAGE plpgsql STRICT
    AS $$  
declare  
    v_startLine geometry;--离起点最近的线  
    v_endLine geometry;--离终点最近的线

    gid_start_line int;--离起点最近的线gid
    gid_end_line int;--离终点最近的线gid
      
    v_startTarget integer;--距离起点最近线的终点
    v_endSource integer;--距离终点最近线的起点
  
    v_statpoint geometry;--在v_startLine上距离起点最近的点  
    v_endpoint geometry;--在v_endLine上距离终点最近的点  
      
    v_res geometry;--最短路径分析结果  
  
    v_perStart float;--v_statpoint在v_startLine上的百分比  
    v_perEnd float;--v_endpoint在v_endLine上的百分比
    v_start_res geometry; -- 起点到起点线段终点的一段路
    v_end_res geometry; -- 终点线起点到终点的一段路
  
    v_shPath geometry;--最终结果
    tempnode float;
begin     

    --查询离起点最近的线
    execute 'select gid, the_geom ,target from ' ||tbl||
            ' where ST_DWithin(geog,ST_Geogfromtext(''SRID=4326; point('|| startx ||' ' || starty||')''),20) 
            order by ST_Distance(geog,ST_Geogfromtext(''SRID=4326; point('|| startx ||' ' || starty||')'')) limit 1'
            into gid_start_line, v_startLine ,v_startTarget;

    --查询离终点最近的线  
    execute 'select gid, the_geom, source from ' ||tbl||
            ' where ST_DWithin(geog,ST_Geogfromtext(''SRID=4326; point('|| endx ||' ' || endy||')''),15) 
            order by ST_Distance(geog,ST_Geogfromtext(''SRID=4326; point('|| endx ||' ' || endy||')'')) limit 1'
            into gid_end_line, v_endLine, v_endSource;
  
    --如果没找到最近的线，就返回null  
    if (v_startLine is null) or (v_endLine is null) then
        return null;  
    end if ;  
  
    select  ST_ClosestPoint(v_startLine, ST_Geometryfromtext('point('|| startx ||' ' || starty ||')',4326)) into v_statpoint;  
    select  ST_ClosestPoint(v_endLine, ST_GeometryFromText('point('|| endx ||' ' || endy ||')',4326)) into v_endpoint;  
  
    --最短路径
    drop table if exists dijk;
    execute 'create table dijk as
    select * from pgr_dijkstra(''SELECT gid as id, source, target, cost FROM '||tbl||''' ,' ||v_startTarget||','||v_endSource||', true);';
    -- 合并查询路线
    execute 'SELECT st_linemerge(st_union(b.the_geom)) 
    FROM dijk a,' || tbl || ' b WHERE a.edge = b.gid' into v_res;
    -- 记录路段id, 包括起点和终点所在的路段.
    insert into edge_id(edge) values(gid_start_line);
    insert into edge_id(edge) select edge from dijk;
    insert into edge_id(edge) values(gid_end_line);
    
    RAISE NOTICE 'v_res: %',v_res; 
    --如果找不到最短路径，就返回null  
    --if(v_res is null) then  
    --    return null;  
    --end if;

    --将v_res,v_start_res,v_end_res进行拼接, 作为最终结果
    select ST_LineLocatePoint(v_startLine, v_statpoint) into v_perStart;
    select ST_LineLocatePoint(v_endLine, v_endpoint) into v_perEnd;

    -- 如果起点终点在同一路段,且起点在终点之前:
    RAISE NOTICE 'v_perStartes: %, v_perEnd: %',v_perStart,  v_perEnd;
    if(gid_start_line = gid_end_line) and (v_perStart < v_perEnd) then
        SELECT ST_LineSubString(v_startLine, v_perStart, v_perEnd) into v_start_res;
        return v_start_res;
    end if;

    SELECT ST_LineSubString(v_startLine,v_perStart, 1) into v_start_res;
    SELECT ST_LineSubString(v_endLine,0, v_perEnd) into v_end_res;
    -- 正常merge
    select  st_linemerge(ST_Union(array[v_start_res, v_res, v_end_res])) into v_shPath;
    return v_shPath;
end;  
$$;


ALTER FUNCTION public.pgr_fromatob(tbl character varying, startx double precision, starty double precision, endx double precision, endy double precision) OWNER TO postgres;

--
-- Name: pgr_fromatob_unnormal(character varying, character varying, double precision, double precision, double precision, double precision, double precision, double precision); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.pgr_fromatob_unnormal(tbl character varying, vertices character varying, blockedx double precision, blockedy double precision, startx double precision, starty double precision, endx double precision, endy double precision) RETURNS public.geometry
    LANGUAGE plpgsql STRICT
    AS $$  
declare  
    v_startLine geometry;--离起点最近的线  
    v_endLine geometry;--离终点最近的线  
    v_blockedLine geometry; --堵塞的线
      
    v_startTarget integer;--距离起点最近线的终点  
    v_startSource integer;--距离起点最近线的起点
    v_endSource integer;--距离终点最近线的起点  
    v_bloackedTarget integer;--距离堵塞点最近线的终点
    
    v_startSourceGeom geometry;
    v_blockedTargetGeom geometry;
    v_linebetweenBTSS geometry;
  
    v_statpoint geometry;--在v_startLine上距离起点最近的点  
    v_endpoint geometry;--在v_endLine上距离终点最近的点  
    v_blockedpoint geometry; --在v_blockedLine上距离堵塞点最近的点
      
    v_res geometry;--最短路径分析结果 
    v_shPathAdd geometry;--合并额外道路
    v_startLineRes geometry;--startLine上的额外道路
    v_blockedLineRes geometry;--blockedLine上的额外道路
  
    v_perStart float;--v_statpoint在v_res上的百分比  
    v_perEnd float;--v_endpoint在v_res上的百分比  
    v_perBlockedPoint float;
    v_perStartLineStart float;

  
    v_shPath geometry;--最终结果
    tempnode float; 
begin     
          
    --查询离起点最近的线   
    execute 'select the_geom ,target, source  from ' ||tbl||
            ' where 
            ST_DWithin(the_geom,ST_Geometryfromtext(''point('|| startx ||' ' || starty||')'',4326),15) 
            order by ST_Distance(the_geom,ST_GeometryFromText(''point('|| startx ||' '|| starty ||')'',4326))  limit 1' 
            into v_startLine ,v_startTarget, v_startSource; 

    execute 'select the_geom from ' || vertices ||
            ' where id = ' || v_startSource
            into v_startSourceGeom;
      
    --查询离终点最近的线  
    execute 'select the_geom,source  from ' ||tbl||
            ' where ST_DWithin(the_geom,ST_Geometryfromtext(''point('|| endx || ' ' || endy ||')'',4326),15) 
            order by ST_Distance(the_geom,ST_GeometryFromText(''point('|| endx ||' ' || endy ||')'',4326))  limit 1' 
            into v_endLine,v_endSource; 

    --查询离堵塞点最近的线 
    execute 'select the_geom,target  from ' ||tbl||
            ' where ST_DWithin(the_geom,ST_Geometryfromtext(''point('|| blockedx || ' ' || blockedy ||')'',4326),15) 
            order by ST_Distance(the_geom,ST_GeometryFromText(''point('|| blockedx ||' ' || blockedy ||')'',4326))  limit 1' 
            into v_blockedLine,v_bloackedTarget; 

    execute 'select the_geom from ' || vertices ||
            ' where id = ' || v_bloackedTarget
            into v_blockedTargetGeom;
  
    --如果没找到最近的线，就返回null  
    if (v_startLine is null) or (v_endLine is null) then  
        return null;  
    end if;  
  
    select  ST_ClosestPoint(v_startLine, ST_Geometryfromtext('point('|| startx ||' ' || starty ||')',4326)) into v_statpoint;  
    select  ST_ClosestPoint(v_endLine, ST_GeometryFromText('point('|| endx ||' ' || endy ||')',4326)) into v_endpoint;  
    select  ST_ClosestPoint(v_blockedLine, ST_GeometryFromText('point('|| blockedx ||' ' || blockedy ||')',4326)) into v_blockedpoint;
  
      
    --最短路径  
    execute 'SELECT st_linemerge(st_union(b.the_geom))' ||
    'FROM pgr_dijkstra(  
    ''SELECT gid as id, source, target, length as cost FROM ' || tbl ||''' ,' ||
    v_startTarget || ',' ||v_endSource || ', true  ) a,' || tbl || ' b    
    WHERE a.edge = b.gid'
    into v_res;  
  
    --如果找不到最短路径，就返回null  
    if(v_res is null) then  
        return null;  
    end if;  
      
    --将v_res,v_startLine,v_endLine进行拼接  
    select  st_linemerge(ST_Union(array[v_res,v_startLine,v_endLine])) into v_res;  
      
    select  ST_LineLocatePoint(v_res, v_statpoint) into v_perStart;  
    select  ST_LineLocatePoint(v_res, v_endpoint) into v_perEnd;  
    select  ST_LineLocatePoint(v_blockedLine, v_blockedpoint) into v_perBlockedPoint;  
    select  ST_LineLocatePoint(v_startLine, v_statpoint) into v_perStartLineStart;
    select  ST_MakeLine(v_blockedTargetGeom, v_startSourceGeom) into v_linebetweenBTSS;
    
    if(v_perStart > v_perEnd) then  
        tempnode =  v_perStart;
        v_perStart = v_perEnd;
        v_perEnd = tempnode;
    end if;

    --截取v_res  
    SELECT ST_LineSubString(v_res,v_perStart, v_perEnd) into v_shPath;
    SELECT ST_LineSubString(v_blockedLine,v_perBlockedPoint,1.0) into v_blockedLineRes;
    SELECT ST_LineSubString(v_startLine,0.0,v_perStartLineStart) into v_startLineRes;

    SELECT st_linemerge(ST_Union(array[v_blockedLineRes,v_linebetweenBTSS,v_startLineRes,v_shPath])) into v_shPathAdd;
        
    return v_shPathAdd;  

end;  
$$;


ALTER FUNCTION public.pgr_fromatob_unnormal(tbl character varying, vertices character varying, blockedx double precision, blockedy double precision, startx double precision, starty double precision, endx double precision, endy double precision) OWNER TO postgres;

--
-- Name: pgr_pointoffset(character varying, double precision, double precision); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.pgr_pointoffset(tbl character varying, blockedx double precision, blockedy double precision) RETURNS SETOF double precision
    LANGUAGE plpgsql STRICT
    AS $$  
declare  
    v_blockedLine geometry;--被阻断的线 
    point_in_line geometry;
    endpoint geometry;
    targetpoint geometry;
    id integer;--被阻断的线  
    start_x float;
    start_y float;
    sin float;
    cos float;
    end_x float;
    end_y float;
    target_x float;
    target_y float;
begin
    --查询离起点最近的线  
    execute 'select the_geom ,gid  from '|| tbl ||' 
                where 
            ST_DWithin(the_geom,ST_Geometryfromtext(''point('|| blockedx ||' ' || blockedy||')'',4326),15) 
            order by ST_Distance(the_geom,ST_GeometryFromText(''point('|| blockedx ||' '|| blockedy ||')'',4326))  limit 1' 
            into v_blockedLine ,id;  

    -- 得到线上距离阻塞点最近的一点
    select ST_LineInterpolatePoint(v_blockedLine, ST_LineLocatePoint(v_blockedLine ,ST_GeometryFromText('point('|| blockedx ||' ' || blockedy ||')',4326))) into point_in_line;
    --select ST_LineInterpolatePoint( v_blockedLine ||, ST_LineLocatePoint( v_blockedLine ||',''point('|| blockedx ||' ' || blockedy ||')''))' into point_in_line;
    --execute 'select ST_LineInterpolatePoint('|| v_blockedLine ||', ST_LineLocatePoint('|| v_blockedLine ||',ST_GeometryFromText(''point('|| blockedx ||' ' || blockedy ||')'',4326)))' into point_in_line;
    select st_x(ST_Transform(point_in_line,900913)) into start_x;
    select st_y(ST_Transform(point_in_line,900913)) into start_y;

    select ST_EndPoint(v_blockedLine) into  endpoint;
    select st_x(ST_Transform(endpoint,900913)) into end_x;
    select st_y(ST_Transform(endpoint,900913)) into end_y;

    sin = (end_x - start_x) / (|/ ((start_y - end_y)^2+(start_x - end_x)^2));
    cos = -(end_y - start_y) / (|/ ((start_y - end_y)^2+(start_x - end_x)^2));


    target_x = start_x + 10*cos;
    target_y = start_y + 10*sin;

    select ST_Transform(ST_Geometryfromtext('point('|| target_x ||' ' || target_y||')',900913),4326) into targetpoint;
    select st_x(targetpoint) into target_x;
    select st_y(targetpoint) into target_y;


    return next target_x;
    return next target_y;
    --return targetpoint;

end;  
$$;


ALTER FUNCTION public.pgr_pointoffset(tbl character varying, blockedx double precision, blockedy double precision) OWNER TO postgres;

--
-- Name: pgr_updatetopomap(character varying, double precision, double precision); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.pgr_updatetopomap(tbl character varying, blockedx double precision, blockedy double precision) RETURNS void
    LANGUAGE plpgsql STRICT
    AS $$  
declare  
    v_blockedLine geometry;--被阻断的线 
       
    id integer;--被阻断的线  
begin     
          
    --查询离起点最近的线  
    execute 'select the_geom ,gid  from '||tbl||' where 
            ST_DWithin(the_geom,ST_Geometryfromtext(''point('|| blockedx ||' ' || blockedy||')'',4326),15) 
            order by ST_Distance(the_geom,ST_GeometryFromText(''point('|| blockedx ||' '|| blockedy ||')'',4326))  limit 1' 
            into v_blockedLine ,id;  

    execute 'UPDATE '||tbl||'
            SET cost = 99999
            WHERE gid = '||id||' ';
  
      
end;  
$$;


ALTER FUNCTION public.pgr_updatetopomap(tbl character varying, blockedx double precision, blockedy double precision) OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: all_points; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.all_points (
    p_id bigint,
    utmx real,
    utmy real,
    heading real,
    curv real,
    mode bigint,
    speed_mode bigint,
    event_mode bigint,
    opposite_side_mode bigint,
    lane_num bigint,
    lane_seq bigint,
    lane_width real,
    geom public.geometry(Point,4326)
);


ALTER TABLE public.all_points OWNER TO postgres;

--
-- Name: begin_point_candidate; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.begin_point_candidate (
    id integer
);


ALTER TABLE public.begin_point_candidate OWNER TO postgres;

--
-- Name: begin_point_in_buffer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.begin_point_in_buffer (
    p_id bigint,
    utmx real,
    utmy real,
    heading real,
    curv real,
    mode bigint,
    speed_mode bigint,
    event_mode bigint,
    opposite_side_mode bigint,
    lane_num bigint,
    lane_seq bigint,
    lane_width real,
    geom public.geometry(Point,4326)
);


ALTER TABLE public.begin_point_in_buffer OWNER TO postgres;

--
-- Name: begin_points; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.begin_points (
    p_id bigint,
    utmx real,
    utmy real,
    heading real,
    curv real,
    mode bigint,
    speed_mode bigint,
    event_mode bigint,
    opposite_side_mode bigint,
    lane_num bigint,
    lane_seq bigint,
    lane_width real,
    geom public.geometry(Point,4326)
);


ALTER TABLE public.begin_points OWNER TO postgres;

--
-- Name: dijk; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.dijk (
    seq integer,
    path_seq integer,
    node bigint,
    edge bigint,
    cost double precision,
    agg_cost double precision
);


ALTER TABLE public.dijk OWNER TO postgres;

--
-- Name: edge_id; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.edge_id (
    edge integer
);


ALTER TABLE public.edge_id OWNER TO postgres;

--
-- Name: path_cost; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.path_cost (
    gid integer,
    cost double precision
);


ALTER TABLE public.path_cost OWNER TO postgres;

--
-- Name: point_in_buffer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.point_in_buffer (
    p_id bigint,
    utmx real,
    utmy real,
    heading real,
    curv real,
    mode bigint,
    speed_mode bigint,
    event_mode bigint,
    opposite_side_mode bigint,
    lane_num bigint,
    lane_seq bigint,
    lane_width real,
    geom public.geometry(Point,4326)
);


ALTER TABLE public.point_in_buffer OWNER TO postgres;

--
-- Name: result_path_points; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.result_path_points (
    p_id bigint,
    lon double precision,
    lat double precision,
    utmx real,
    utmy real,
    heading real,
    curv real,
    mode bigint,
    speed_mode bigint,
    event_mode bigint,
    opposite_side_mode bigint,
    lane_num bigint,
    lane_seq bigint,
    lane_width real
);


ALTER TABLE public.result_path_points OWNER TO postgres;

--
-- Name: result_path_points_meta; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.result_path_points_meta (
    id integer,
    p_id bigint,
    lon double precision,
    lat double precision,
    utmx real,
    utmy real,
    heading real,
    curv real,
    mode bigint,
    speed_mode bigint,
    opposite_side_mode bigint,
    event_mode bigint,
    lane_num bigint,
    lane_seq bigint,
    lane_width real,
    point public.geometry
);


ALTER TABLE public.result_path_points_meta OWNER TO postgres;

--
-- Name: result_points_tbl; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.result_points_tbl (
    id integer,
    point public.geometry,
    lon double precision,
    lat double precision
);


ALTER TABLE public.result_points_tbl OWNER TO postgres;

--
-- Name: shortpath; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.shortpath (
    pgr_fromatontob public.geometry
);


ALTER TABLE public.shortpath OWNER TO postgres;

--
-- Name: stop_point_candidate; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.stop_point_candidate (
    id integer
);


ALTER TABLE public.stop_point_candidate OWNER TO postgres;

--
-- Name: stop_point_in_buffer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.stop_point_in_buffer (
    p_id bigint,
    utmx real,
    utmy real,
    heading real,
    curv real,
    mode bigint,
    speed_mode bigint,
    event_mode bigint,
    opposite_side_mode bigint,
    lane_num bigint,
    lane_seq bigint,
    lane_width real,
    geom public.geometry(Point,4326)
);


ALTER TABLE public.stop_point_in_buffer OWNER TO postgres;

--
-- Name: stop_points; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.stop_points (
    p_id bigint,
    utmx real,
    utmy real,
    heading real,
    curv real,
    mode bigint,
    speed_mode bigint,
    event_mode bigint,
    opposite_side_mode bigint,
    lane_num bigint,
    lane_seq bigint,
    lane_width real,
    geom public.geometry(Point,4326)
);


ALTER TABLE public.stop_points OWNER TO postgres;

--
-- Name: topo_map; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.topo_map (
    gid integer NOT NULL,
    l_id bigint,
    the_geom public.geometry(LineString,4326),
    source integer,
    target integer,
    speed double precision,
    length double precision,
    cost double precision,
    geog public.geography
);


ALTER TABLE public.topo_map OWNER TO postgres;

--
-- Name: topo_map_gid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.topo_map_gid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.topo_map_gid_seq OWNER TO postgres;

--
-- Name: topo_map_gid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.topo_map_gid_seq OWNED BY public.topo_map.gid;


--
-- Name: topo_map_vertices_pgr; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.topo_map_vertices_pgr (
    id bigint NOT NULL,
    cnt integer,
    chk integer,
    ein integer,
    eout integer,
    the_geom public.geometry(Point,4326)
);


ALTER TABLE public.topo_map_vertices_pgr OWNER TO postgres;

--
-- Name: topo_map_vertices_pgr_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.topo_map_vertices_pgr_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.topo_map_vertices_pgr_id_seq OWNER TO postgres;

--
-- Name: topo_map_vertices_pgr_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.topo_map_vertices_pgr_id_seq OWNED BY public.topo_map_vertices_pgr.id;


--
-- Name: topo_map gid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.topo_map ALTER COLUMN gid SET DEFAULT nextval('public.topo_map_gid_seq'::regclass);


--
-- Name: topo_map_vertices_pgr id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.topo_map_vertices_pgr ALTER COLUMN id SET DEFAULT nextval('public.topo_map_vertices_pgr_id_seq'::regclass);


--
-- Data for Name: all_points; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.all_points (p_id, utmx, utmy, heading, curv, mode, speed_mode, event_mode, opposite_side_mode, lane_num, lane_seq, lane_width, geom) FROM stdin;
2	329764.78	3.4621772e+06	-1.5913798	0	0	3	0	3	2	1	3	0101000020E6100000061F8ACF8B4D5E40FA657DF90E483F40
2	329764.75	3.4621768e+06	-1.5956571	0	0	3	0	3	2	1	3	0101000020E61000007A991CCF8B4D5E40F2DE34A00E483F40
2	329764.72	3.4621762e+06	-1.5985552	0	0	3	0	3	2	1	3	0101000020E6100000715FB3CE8B4D5E40EDCA06500E483F40
2	329764.72	3.4621755e+06	-1.6004928	0	0	3	0	3	2	1	3	0101000020E6100000628E41CE8B4D5E406BD4B1F50D483F40
2	329764.7	3.462175e+06	-1.6021907	0	0	3	0	3	2	1	3	0101000020E6100000EAC7C1CD8B4D5E400F7A07970D483F40
2	329764.7	3.4621742e+06	-1.6022372	0	0	3	0	3	2	1	3	0101000020E6100000DBF64FCD8B4D5E40705CDF330D483F40
2	329764.66	3.4621738e+06	-1.601089	0	0	3	0	3	2	1	3	0101000020E6100000090608CD8B4D5E409BB52BE30C483F40
2	329764.66	3.4621732e+06	-1.5996642	0	0	3	0	3	2	1	3	0101000020E6100000C0F7CCCC8B4D5E4006C4F9920C483F40
2	329764.62	3.4621728e+06	-1.598105	0	0	3	0	3	2	1	3	0101000020E6100000192295CC8B4D5E40215D793B0C483F40
2	329764.62	3.462172e+06	-1.5962728	0	0	3	0	3	2	1	3	0101000020E61000009E676DCC8B4D5E401D3BF1E30B483F40
2	329764.6	3.4621715e+06	-1.5934278	0	0	3	0	3	2	1	3	0101000020E6100000F59761CC8B4D5E402D9FB2880B483F40
2	329764.6	3.4621708e+06	-1.5899522	0	0	3	0	3	2	1	3	0101000020E610000001C672CC8B4D5E4010E1652B0B483F40
2	329764.6	3.4621702e+06	-1.5860423	0	0	3	0	3	2	1	3	0101000020E61000008317A3CC8B4D5E40F6B1A2C80A483F40
2	329764.6	3.4621695e+06	-1.581891	0	0	3	0	3	2	1	3	0101000020E61000003BB2F4CC8B4D5E40B9D8DA620A483F40
2	329764.6	3.4621688e+06	-1.578461	0	0	3	0	3	2	1	3	0101000020E61000001F6856CD8B4D5E40D13CE0000A483F40
2	329764.6	3.4621682e+06	-1.5763057	0	0	3	0	3	2	1	3	0101000020E61000003B61ADCD8B4D5E40D9828EB009483F40
2	329764.6	3.4621678e+06	-1.5740738	0	0	3	0	3	2	1	3	0101000020E61000008FA325CE8B4D5E405256ED4909483F40
2	329764.6	3.4621672e+06	-1.572463	0	0	3	0	3	2	1	3	0101000020E6100000F5A48BCE8B4D5E40946350FC08483F40
2	329764.6	3.4621668e+06	-1.5706201	0	0	3	0	3	2	1	3	0101000020E61000000476FDCE8B4D5E40E6B8BAAF08483F40
2	329764.6	3.462166e+06	-1.5676329	0	0	3	0	3	2	1	3	0101000020E61000000ABCBECF8B4D5E403287AA3D08483F40
2	329764.6	3.4621652e+06	-1.5652409	0	0	3	0	3	2	1	3	0101000020E6100000169988D08B4D5E40740F37CE07483F40
2	329764.6	3.4621648e+06	-1.5644594	0	0	3	0	3	2	1	3	0101000020E6100000B4E30FD18B4D5E40662C3B8107483F40
2	329764.6	3.4621638e+06	-1.5653809	0	0	3	0	3	2	1	3	0101000020E6100000CDE8BED18B4D5E4001292A0D07483F40
2	329764.6	3.4621632e+06	-1.5668261	0	0	3	0	3	2	1	3	0101000020E6100000B19E20D28B4D5E409B92FBC006483F40
2	329764.6	3.4621628e+06	-1.5684685	0	0	3	0	3	2	1	3	0101000020E6100000D32E80D28B4D5E40D157F96E06483F40
2	329764.6	3.4621622e+06	-1.5701433	0	0	3	0	3	2	1	3	0101000020E61000000F15D6D28B4D5E40533E2A1C06483F40
2	329764.6	3.4621618e+06	-1.5718685	0	0	3	0	3	2	1	3	0101000020E6100000C1181FD38B4D5E40DF2024CB05483F40
2	329764.6	3.4621612e+06	-1.5737637	0	0	3	0	3	2	1	3	0101000020E6100000EB395BD38B4D5E40627EEA7805483F40
2	329764.6	3.4621608e+06	-1.5758542	0	0	3	0	3	2	1	3	0101000020E61000008C788AD38B4D5E40A57DF32305483F40
2	329764.6	3.46216e+06	-1.5781177	0	0	3	0	3	2	1	3	0101000020E610000084E7ADD38B4D5E408A05C6CA04483F40
2	329764.6	3.4621595e+06	-1.5802759	0	0	3	0	3	2	1	3	0101000020E6100000334EC2D38B4D5E400823487604483F40
2	329764.56	3.462159e+06	-1.5825218	0	0	3	0	3	2	1	3	0101000020E610000096ACC7D38B4D5E40FBC6561E04483F40
2	329764.56	3.4621582e+06	-1.5848428	0	0	3	0	3	2	1	3	0101000020E61000009015BFD38B4D5E40C3C569C303483F40
2	329764.56	3.4621578e+06	-1.5869108	0	0	3	0	3	2	1	3	0101000020E6100000C3C1ABD38B4D5E409825066C03483F40
2	329764.53	3.462157e+06	-1.5882158	0	0	3	0	3	2	1	3	0101000020E6100000F56D98D38B4D5E4059A5620E03483F40
2	329764.53	3.4621565e+06	-1.587673	0	0	3	0	3	2	1	3	0101000020E61000003ADF9ED38B4D5E403944B5B002483F40
2	329764.53	3.4621558e+06	-1.5851841	0	0	3	0	3	2	1	3	0101000020E6100000B599C6D38B4D5E40C4B8E14F02483F40
2	329764.5	3.4621552e+06	-1.582361	0	0	3	0	3	2	1	3	0101000020E61000003D82FFD38B4D5E40A76BADF401483F40
2	329764.5	3.4621545e+06	-1.5796307	0	0	3	0	3	2	1	3	0101000020E610000092BE4BD48B4D5E40C3E32B9501483F40
2	329764.5	3.462154e+06	-1.5770954	0	0	3	0	3	2	1	3	0101000020E61000003103A7D48B4D5E4069193B3601483F40
2	329764.5	3.4621532e+06	-1.5746566	0	0	3	0	3	2	1	3	0101000020E6100000DC7513D58B4D5E406B673DD800483F40
2	329764.5	3.4621528e+06	-1.5721564	0	0	3	0	3	2	1	3	0101000020E6100000166295D58B4D5E40B332407700483F40
2	329764.5	3.462152e+06	-1.5698733	0	0	3	0	3	2	1	3	0101000020E610000075D21ED68B4D5E4012BF831B00483F40
2	329764.53	3.4621515e+06	-1.5676988	0	0	3	0	3	2	1	3	0101000020E6100000A82DC4D68B4D5E4002650BB4FF473F40
2	329764.53	3.4621508e+06	-1.5664744	0	0	3	0	3	2	1	3	0101000020E610000071935BD78B4D5E40451DC555FF473F40
2	329764.53	3.4621502e+06	-1.5660477	0	0	3	0	3	2	1	3	0101000020E6100000BD44F7D78B4D5E40097DF2F1FE473F40
2	329764.53	3.4621495e+06	-1.5666547	0	0	3	0	3	2	1	3	0101000020E6100000E4718BD88B4D5E40D598468AFE473F40
2	329764.53	3.4621488e+06	-1.5679401	0	0	3	0	3	2	1	3	0101000020E6100000F8D905D98B4D5E40FC2B2F2AFE473F40
2	329764.53	3.4621482e+06	-1.5696152	0	0	3	0	3	2	1	3	0101000020E610000007AB77D98B4D5E409C9D3AC6FD473F40
2	329764.53	3.4621475e+06	-1.5712928	0	0	3	0	3	2	1	3	0101000020E61000000A4ED8D98B4D5E40CF71DB66FD473F40
2	329764.53	3.462147e+06	-1.573068	0	0	3	0	3	2	1	3	0101000020E6100000075A30DA8B4D5E4061766302FD473F40
2	329764.53	3.4621462e+06	-1.574882	0	0	3	0	3	2	1	3	0101000020E61000005C967CDA8B4D5E409474CD9BFC473F40
2	329764.5	3.4621455e+06	-1.576654	0	0	3	0	3	2	1	3	0101000020E610000067CAB9DA8B4D5E40C6539637FC473F40
2	329764.5	3.462145e+06	-1.5784044	0	0	3	0	3	2	1	3	0101000020E61000000709E9DA8B4D5E40AFB48CD4FB473F40
2	329764.5	3.4621442e+06	-1.5801678	0	0	3	0	3	2	1	3	0101000020E610000000780CDB8B4D5E40C417C070FB473F40
2	329764.5	3.4621435e+06	-1.581941	0	0	3	0	3	2	1	3	0101000020E6100000700423DB8B4D5E40D26C550CFB473F40
2	329764.47	3.4621428e+06	-1.5836849	0	0	3	0	3	2	1	3	0101000020E610000037C12DDB8B4D5E4002BB34A3FA473F40
2	329764.47	3.4621422e+06	-1.5851368	0	0	3	0	3	2	1	3	0101000020E6100000BA0C32DB8B4D5E4049593C3DFA473F40
2	329764.47	3.4621415e+06	-1.5862106	0	0	3	0	3	2	1	3	0101000020E6100000F8E62FDB8B4D5E40986685E5F9473F40
2	329764.44	3.462141e+06	-1.5871187	0	0	3	0	3	2	1	3	0101000020E6100000B47529DB8B4D5E4087D97B94F9473F40
2	329764.44	3.4621405e+06	-1.5882089	0	0	3	0	3	2	1	3	0101000020E6100000C73417DB8B4D5E40BDBF9330F9473F40
2	329764.44	3.4621398e+06	-1.5892583	0	0	3	0	3	2	1	3	0101000020E6100000D55CFCDA8B4D5E405D5170CFF8473F40
2	329764.4	3.4621392e+06	-1.5901753	0	0	3	0	3	2	1	3	0101000020E6100000404CDEDA8B4D5E40D7B26D7BF8473F40
2	329764.4	3.4621388e+06	-1.5930979	0	1	3	0	3	2	1	3	0101000020E6100000036CB4DA8B4D5E4002A11E27F8473F40
2	329764.38	3.4621382e+06	-1.5939559	0	1	3	0	3	2	1	3	0101000020E61000001EBC7EDA8B4D5E40E9FFF9D6F7473F40
2	329764.38	3.4621375e+06	-1.5943178	0	1	3	0	3	2	1	3	0101000020E6100000380C49DA8B4D5E40C2A4857EF7473F40
2	329764.34	3.462137e+06	-1.5943544	0	1	3	0	3	2	1	3	0101000020E610000098CD19DA8B4D5E4016A1CB29F7473F40
2	329764.34	3.4621362e+06	-1.5941054	0	1	3	0	3	2	1	3	0101000020E61000005556E7D98B4D5E40D17548C6F6473F40
2	329764.34	3.4621358e+06	-1.5933485	0	1	3	0	3	2	1	3	0101000020E6100000A158CAD98B4D5E405024B476F6473F40
2	329764.3	3.4621352e+06	-1.5916485	0	1	3	0	3	2	1	3	0101000020E6100000BAAEC0D98B4D5E40A4C28820F6473F40
2	329764.3	3.4621345e+06	-1.5889401	0	1	3	0	3	2	1	3	0101000020E6100000E5C9D0D98B4D5E40D0446EC3F5473F40
2	329764.3	3.462134e+06	-1.5863036	0	1	3	0	3	2	1	3	0101000020E61000005BEDEFD98B4D5E40F88C8C62F5473F40
2	329764.3	3.4621332e+06	-1.5841559	0	1	3	0	3	2	1	3	0101000020E610000078E01ADA8B4D5E400D714DF5F4473F40
2	329764.28	3.4621325e+06	-1.5825797	0	1	3	0	3	2	1	3	0101000020E6100000BB574DDA8B4D5E401B726985F4473F40
2	329764.28	3.462132e+06	-1.5815209	0	1	3	0	3	2	1	3	0101000020E6100000D94A78DA8B4D5E40F5963938F4473F40
2	329764.28	3.4621315e+06	-1.5803846	0	1	3	0	3	2	1	3	0101000020E61000001CC2AADA8B4D5E40A2D11EE9F3473F40
2	329764.28	3.462131e+06	-1.5791363	0	1	3	0	3	2	1	3	0101000020E6100000E81BEADA8B4D5E4074ECCB92F3473F40
2	329764.28	3.4621302e+06	-1.5779401	0	1	3	0	3	2	1	3	0101000020E610000056AE2CDB8B4D5E40CA210740F3473F40
2	329764.28	3.4621298e+06	-1.5766621	0	1	3	0	3	2	1	3	0101000020E61000002E367DDB8B4D5E40FB287EE7F2473F40
2	329764.28	3.4621292e+06	-1.5754875	0	1	3	0	3	2	1	3	0101000020E6100000A8F6D0DB8B4D5E401F5AD891F2473F40
2	329764.28	3.4621285e+06	-1.5743507	0	1	3	0	3	2	1	3	0101000020E61000004DD234DC8B4D5E401341D431F2473F40
2	329764.28	3.462128e+06	-1.5736374	0	1	3	0	3	2	1	3	0101000020E61000000C048FDC8B4D5E40887534DCF1473F40
2	329764.28	3.4621275e+06	-1.5733113	0	1	3	0	3	2	1	3	0101000020E610000047EAE4DC8B4D5E4009F87689F1473F40
2	329764.25	3.4621268e+06	-1.573214	0	1	3	0	3	2	1	3	0101000020E6100000EDC548DD8B4D5E405D484F26F1473F40
2	329764.25	3.4621262e+06	-1.5732163	0	1	3	0	3	2	1	3	0101000020E610000067869CDD8B4D5E40785994D4F0473F40
2	329764.25	3.4621258e+06	-1.573233	0	1	3	0	3	2	1	3	0101000020E61000005EFBEBDD8B4D5E40996C7D86F0473F40
2	329764.25	3.4621252e+06	-1.5732303	0	1	3	0	3	2	1	3	0101000020E6100000B23738DE8B4D5E404E39C03AF0473F40
2	329764.25	3.4621248e+06	-1.5735667	0	1	3	0	3	2	1	3	0101000020E61000008ABF88DE8B4D5E404C77ABEBEF473F40
2	329764.25	3.4621245e+06	-1.573572	0	1	3	1	3	2	1	3	0101000020E61000005EAAA4DE8B4D5E400D909CCFEF473F40
0	329764.25	3.462124e+06	-1.5715035	0	2	3	0	3	2	1	3	0101000020E610000047F70EDF8B4D5E409465CA7FEF473F40
0	329764.25	3.4621235e+06	-1.5677159	0	2	3	0	3	2	1	3	0101000020E61000006E24A3DF8B4D5E40C805AA31EF473F40
0	329764.28	3.462123e+06	-1.5625088	0	2	3	0	3	2	1	3	0101000020E6100000DE5F72E08B4D5E4032AA0CE3EE473F40
0	329764.3	3.4621225e+06	-1.5508212	0	2	3	0	3	2	1	3	0101000020E6100000B2A2D3E18B4D5E40F6D6A895EE473F40
0	329764.34	3.462122e+06	-1.5347928	0	2	3	0	3	2	1	3	0101000020E61000006210E6E38B4D5E40B13AF449EE473F40
0	329764.38	3.4621215e+06	-1.5176616	0	2	3	0	3	2	1	3	0101000020E6100000EF9C51E68B4D5E407AFE98FCED473F40
0	329764.44	3.462121e+06	-1.4994882	0	2	3	0	3	2	1	3	0101000020E61000008A005BE98B4D5E4084F2CFA7ED473F40
0	329764.5	3.4621202e+06	-1.4810406	0	2	3	0	3	2	1	3	0101000020E61000000189E9EC8B4D5E40F105EB50ED473F40
0	329764.6	3.4621198e+06	-1.463776	0	2	3	0	3	2	1	3	0101000020E61000009C9BABF08B4D5E40E0653AFFEC473F40
0	329764.7	3.4621192e+06	-1.4451852	0	2	3	0	3	2	1	3	0101000020E61000001E0730F58B4D5E4046BCFEA6EC473F40
0	329764.8	3.4621185e+06	-1.4244614	0	2	3	0	3	2	1	3	0101000020E6100000008CCAFA8B4D5E40B4437D44EC473F40
0	329764.9	3.462118e+06	-1.4078727	0	2	3	0	3	2	1	3	0101000020E6100000A91EB7FF8B4D5E405857A0F5EB473F40
0	329765.03	3.4621175e+06	-1.3910558	0	2	3	0	3	2	1	3	0101000020E610000024A817058C4D5E4044BDCAA5EB473F40
0	329765.16	3.462117e+06	-1.3725208	0	2	3	0	3	2	1	3	0101000020E6100000C16A640B8C4D5E40F185674FEB473F40
0	329765.28	3.4621165e+06	-1.3546202	0	2	3	0	3	2	1	3	0101000020E61000006C5596118C4D5E407C244401EB473F40
0	329765.44	3.462116e+06	-1.3356196	0	2	3	0	3	2	1	3	0101000020E6100000900648188C4D5E40E14445B3EA473F40
0	329765.56	3.4621155e+06	-1.3164097	0	2	3	0	3	2	1	3	0101000020E61000008D391E1F8C4D5E40D430D369EA473F40
0	329765.78	3.4621148e+06	-1.2902628	0	2	3	0	3	2	1	3	0101000020E6100000D91E62288C4D5E40AE58EE0EEA473F40
0	329765.97	3.4621142e+06	-1.2655717	0	2	3	0	3	2	1	3	0101000020E610000072005D318C4D5E408D7D60BEE9473F40
0	329766.16	3.4621138e+06	-1.2420492	0	2	3	0	3	2	1	3	0101000020E61000001373343A8C4D5E40EFC30D75E9473F40
0	329766.38	3.4621132e+06	-1.2187965	0	2	3	0	3	2	1	3	0101000020E6100000B88240438C4D5E40662E182FE9473F40
0	329766.62	3.4621128e+06	-1.1891292	0	2	3	0	3	2	1	3	0101000020E6100000FAEF294F8C4D5E40B4E49CD9E8473F40
0	329766.84	3.4621122e+06	-1.1642207	0	2	3	0	3	2	1	3	0101000020E6100000346EC5598C4D5E40663B7A92E8473F40
0	329767.12	3.4621118e+06	-1.1372356	0	2	3	0	3	2	1	3	0101000020E6100000B2BBD8658C4D5E40C2CA4D46E8473F40
0	329767.4	3.4621112e+06	-1.1077619	0	2	3	0	3	2	1	3	0101000020E6100000899751738C4D5E4089F10EF7E7473F40
0	329767.7	3.4621108e+06	-1.0827221	0	2	3	0	3	2	1	3	0101000020E6100000FEAAFB7E8C4D5E40450DC2B6E7473F40
0	329767.94	3.4621102e+06	-1.0574076	0	2	3	0	3	2	1	3	0101000020E61000008195438B8C4D5E40A3CB9276E7473F40
0	329768.25	3.46211e+06	-1.0293758	0	2	3	0	3	2	1	3	0101000020E6100000349641998C4D5E4094099831E7473F40
0	329768.6	3.4621095e+06	-0.99867505	0	2	3	0	3	2	1	3	0101000020E6100000CBA4E6A88C4D5E401F203AE9E6473F40
0	329768.97	3.462109e+06	-0.9668435	0	2	3	0	3	2	1	3	0101000020E610000001ADCBB98C4D5E401E3AD99FE6473F40
0	329769.38	3.4621085e+06	-0.9339743	0	2	3	0	3	2	1	3	0101000020E61000001FB7FFCB8C4D5E402F32B655E6473F40
0	329769.84	3.462108e+06	-0.8995383	0	2	3	0	3	2	1	3	0101000020E6100000B048F0DF8C4D5E40A2B9BD09E6473F40
0	329770.3	3.4621075e+06	-0.8634837	0	2	3	0	3	2	1	3	0101000020E610000026EEB3F58C4D5E403F305BBCE5473F40
0	329770.7	3.462107e+06	-0.8360806	0	2	3	0	3	2	1	3	0101000020E61000005AFCC4068D4D5E4059B74A83E5473F40
0	329771.25	3.4621065e+06	-0.7980256	0	2	3	0	3	2	1	3	0101000020E6100000D1573F1F8D4D5E4081878A36E5473F40
0	329771.62	3.4621062e+06	-0.77188486	0	2	3	0	3	2	1	3	0101000020E610000073F892308D4D5E4053219103E5473F40
0	329772.03	3.4621058e+06	-0.74609476	0	2	3	0	3	2	1	3	0101000020E6100000A9AF30428D4D5E40E5AE51D2E4473F40
0	329772.44	3.4621055e+06	-0.72001076	0	2	3	0	3	2	1	3	0101000020E610000055337A548D4D5E402B7FCDA1E4473F40
0	329772.88	3.4621052e+06	-0.6936418	0	2	3	0	3	2	1	3	0101000020E61000004E685F678D4D5E403D264A72E4473F40
0	329773.3	3.4621048e+06	-0.6668898	0	2	3	0	3	2	1	3	0101000020E61000009E7CF17A8D4D5E404DECC543E4473F40
0	329773.94	3.4621045e+06	-0.6298463	0	2	3	0	3	2	1	3	0101000020E6100000A31390968D4D5E4039E08E06E4473F40
0	329774.53	3.462104e+06	-0.5929911	0	2	3	0	3	2	1	3	0101000020E6100000D85C47B28D4D5E40229615CEE3473F40
0	329775.16	3.4621038e+06	-0.55722237	0	2	3	0	3	2	1	3	0101000020E61000005E965DCD8D4D5E408D47899BE3473F40
0	329775.75	3.4621035e+06	-0.522669	0	2	3	0	3	2	1	3	0101000020E6100000A49D30E78D4D5E40475BA46FE3473F40
0	329776.28	3.4621032e+06	-0.4891317	0	2	3	0	3	2	1	3	0101000020E61000007D5DDCFF8D4D5E406BAA9B49E3473F40
0	329776.8	3.462103e+06	-0.4571531	0	2	3	0	3	2	1	3	0101000020E6100000A4BBCD168E4D5E40D74D9C29E3473F40
0	329777.28	3.4621028e+06	-0.42642257	0	2	3	0	3	2	1	3	0101000020E6100000C524452C8E4D5E406211C30EE3473F40
0	329777.78	3.4621025e+06	-0.39496732	0	2	3	0	3	2	1	3	0101000020E6100000348447418E4D5E4001316AF7E2473F40
0	329778.3	3.4621025e+06	-0.35676426	0	2	3	0	3	2	1	3	0101000020E61000009F02F8588E4D5E40C3EDEBE0E2473F40
0	329778.8	3.4621022e+06	-0.31552663	0	2	3	0	3	2	1	3	0101000020E6100000009EB26F8E4D5E403977C3CFE2473F40
0	329779.38	3.4621022e+06	-0.26486725	0	2	3	0	3	2	1	3	0101000020E610000076FF58888E4D5E40EFE894C2E2473F40
0	329779.88	3.4621022e+06	-0.21530317	0	2	3	0	3	2	1	3	0101000020E61000003A4A609E8E4D5E40A1B2B8BBE2473F40
0	329780.4	3.4621022e+06	-0.15869163	0	2	3	0	3	2	1	3	0101000020E61000001F8964B68E4D5E4033134CB9E2473F40
0	329781	3.4621022e+06	-0.10205842	0	2	3	0	3	2	1	3	0101000020E6100000EA2657CF8E4D5E40C52945BBE2473F40
0	329781.53	3.4621022e+06	-0.057046834	0	2	3	0	3	2	1	3	0101000020E6100000B466D8E68E4D5E403C618CBFE2473F40
0	329782.06	3.4621022e+06	-0.030557144	0	2	3	0	3	2	1	3	0101000020E61000006B9624FF8E4D5E40A9E7D4C2E2473F40
0	329782.56	3.4621022e+06	-0.019744668	0	2	3	0	3	2	1	3	0101000020E61000004FCE2A158F4D5E405C116CC4E2473F40
0	329783.2	3.4621022e+06	-0.0124965	0	2	3	0	3	2	1	3	0101000020E6100000C09FC62F8F4D5E405C0BEFC5E2473F40
0	329783.78	3.4621022e+06	-0.007540167	0	2	3	0	3	2	1	3	0101000020E610000081107A4A8F4D5E40D15A88C7E2473F40
0	329784.3	3.4621022e+06	-0.0051625483	0	2	3	0	3	2	1	3	0101000020E6100000BAE23D628F4D5E401DEDD3C8E2473F40
0	329784.9	3.4621022e+06	-0.004233825	0	2	3	0	3	2	1	3	0101000020E61000007AFBAB7B8F4D5E408A0204CAE2473F40
0	329764.5	3.462124e+06	-1.5677091	0	2	3	0	3	2	1	3	0101000020E6100000D6B1F6E98B4D5E405B43EF6DEF473F40
0	329764.5	3.4621232e+06	-1.5750039	0	2	3	0	3	2	1	3	0101000020E610000059FDFAE98B4D5E407072611BEF473F40
0	329764.5	3.4621228e+06	-1.5809783	0	2	3	0	3	2	1	3	0101000020E61000003B0AD0E98B4D5E400D7269CDEE473F40
0	329764.47	3.4621222e+06	-1.5877765	0	2	3	0	3	2	1	3	0101000020E61000005EEB76E98B4D5E40F271017AEE473F40
0	329764.44	3.4621218e+06	-1.5974337	0	2	3	0	3	2	1	3	0101000020E61000003E4FBFE88B4D5E409ED03225EE473F40
0	329764.4	3.4621212e+06	-1.6127238	0	2	3	0	3	2	1	3	0101000020E61000002B3260E78B4D5E40A22037CEED473F40
0	329764.34	3.4621208e+06	-1.6345848	0	2	3	0	3	2	1	3	0101000020E610000050A93DE58B4D5E40BD000680ED473F40
0	329764.25	3.46212e+06	-1.6680127	0	2	3	0	3	2	1	3	0101000020E61000007B59B2E18B4D5E40717AFF2DED473F40
0	329764.12	3.4621195e+06	-1.7122076	0	2	3	0	3	2	1	3	0101000020E6100000A8A589DC8B4D5E402684B2DBEC473F40
0	329763.97	3.462119e+06	-1.7603898	0	2	3	0	3	2	1	3	0101000020E61000007D63FBD58B4D5E4026328689EC473F40
0	329763.78	3.4621185e+06	-1.8088001	0	2	3	0	3	2	1	3	0101000020E610000035795DCE8B4D5E406C390E3AEC473F40
0	329763.6	3.462118e+06	-1.8570561	0	2	3	0	3	2	1	3	0101000020E6100000F0D9DAC58B4D5E4025718EEEEB473F40
0	329763.34	3.4621175e+06	-1.9124099	0	2	3	0	3	2	1	3	0101000020E6100000151D10BB8B4D5E40F7A58F9CEB473F40
0	329763.06	3.462117e+06	-1.9667869	0	2	3	0	3	2	1	3	0101000020E6100000520DAFAF8B4D5E40E21F9152EB473F40
0	329762.78	3.4621165e+06	-2.0283172	0	2	3	0	3	2	1	3	0101000020E6100000D50AB7A28B4D5E40C42EB30BEB473F40
0	329762.44	3.462116e+06	-2.0992558	0	2	3	0	3	2	1	3	0101000020E61000006D630F948B4D5E40837CAEC9EA473F40
0	329762.06	3.4621158e+06	-2.1731536	0	2	3	0	3	2	1	3	0101000020E610000096CBB3838B4D5E406079B78AEA473F40
0	329761.62	3.4621152e+06	-2.246842	0	2	3	0	3	2	1	3	0101000020E61000001F97B7718B4D5E403168804EEA473F40
0	329761.22	3.462115e+06	-2.3190002	0	2	3	0	3	2	1	3	0101000020E61000001ED7215F8B4D5E406A790119EA473F40
0	329760.75	3.4621145e+06	-2.3944824	0	2	3	0	3	2	1	3	0101000020E6100000F397DE4A8B4D5E4031A756E7E9473F40
0	329760.22	3.4621142e+06	-2.47447	0	2	3	0	3	2	1	3	0101000020E6100000333B53348B4D5E4028ADC1B8E9473F40
0	329759.78	3.462114e+06	-2.5422819	0	2	3	0	3	2	1	3	0101000020E6100000768E52208B4D5E40F8C0ED95E9473F40
0	329759.3	3.462114e+06	-2.608607	0	2	3	0	3	2	1	3	0101000020E61000003A84C90B8B4D5E405B37AF77E9473F40
0	329758.78	3.4621138e+06	-2.6784635	0	2	3	0	3	2	1	3	0101000020E6100000A39FEDF48A4D5E4029E29F5BE9473F40
0	329758.22	3.4621135e+06	-2.7459354	0	2	3	0	3	2	1	3	0101000020E610000017DCF8DB8A4D5E402E15D341E9473F40
0	329757.62	3.4621135e+06	-2.795899	0	2	3	0	3	2	1	3	0101000020E6100000604E3BC28A4D5E400E099D28E9473F40
0	329757	3.4621132e+06	-2.8253303	0	2	3	0	3	2	1	3	0101000020E6100000FBFBF7A68A4D5E4060DA150DE9473F40
0	329756.38	3.462113e+06	-2.8392155	0	2	3	0	3	2	1	3	0101000020E61000005232828B8A4D5E4079424BF0E8473F40
0	329755.75	3.462113e+06	-2.850753	0	2	3	0	3	2	1	3	0101000020E61000008B2AC6708A4D5E4025B521D5E8473F40
0	329755.25	3.4621128e+06	-2.8644812	0	2	3	0	3	2	1	3	0101000020E6100000DD92545A8A4D5E40960432C0E8473F40
0	329754.75	3.4621128e+06	-2.8869207	0	2	3	0	3	2	1	3	0101000020E61000004D9DC6448A4D5E400ECD7BAFE8473F40
0	329754.22	3.4621125e+06	-2.9241824	0	2	3	0	3	2	1	3	0101000020E61000009910DB2C8A4D5E40687124A2E8473F40
0	329753.62	3.4621125e+06	-2.9736586	0	2	3	0	3	2	1	3	0101000020E610000039B311138A4D5E4085CCB899E8473F40
0	329753.06	3.4621125e+06	-3.021887	0	2	3	0	3	2	1	3	0101000020E61000002801B8F9894D5E407CC3C595E8473F40
0	329752.53	3.4621125e+06	-3.0618534	0	2	3	0	3	2	1	3	0101000020E61000009A5019E3894D5E406F820D95E8473F40
3	329752.03	3.4621125e+06	-3.1014268	0	2	3	0	3	2	1	3	0101000020E6100000CDCBA8CC894D5E4081F06297E8473F40
3	329751.5	3.4621125e+06	3.1376646	0	2	3	0	3	2	1	3	0101000020E6100000B58F70B5894D5E40B427BC9DE8473F40
8	329774.97	3.4621232e+06	1.5589936	0	2	3	0	3	2	1	3	0101000020E6100000CCF742B78D4D5E40317F2636EF473F40
8	329774.97	3.4621235e+06	1.5588441	0	2	3	2	3	2	1	3	0101000020E6100000AD0A44B78D4D5E406CCA9A3BEF473F40
8	329774.97	3.462124e+06	1.5551938	0	0	3	0	3	2	1	3	0101000020E610000094B44DB78D4D5E40562A108CEF473F40
8	329775	3.4621245e+06	1.5526977	0	0	3	0	3	2	1	3	0101000020E61000004E4973B78D4D5E40F34F2EDEEF473F40
8	329775	3.462125e+06	1.5512656	0	0	3	0	3	2	1	3	0101000020E6100000AA169CB78D4D5E40429A9638F0473F40
8	329775	3.4621255e+06	1.5521948	0	0	3	0	3	2	1	3	0101000020E61000004C4F9FB78D4D5E40FE1C4B89F0473F40
8	329775.03	3.4621262e+06	1.5534059	0	0	3	0	3	2	1	3	0101000020E6100000E9F099B78D4D5E40CABD3DE2F0473F40
8	329775.03	3.4621268e+06	1.5543876	0	0	3	0	3	2	1	3	0101000020E610000021348FB78D4D5E40F8BF2F2FF1473F40
8	329775.03	3.4621272e+06	1.5553857	0	0	3	0	3	2	1	3	0101000020E6100000F6187FB78D4D5E40F6CB3C7DF1473F40
8	329775.06	3.4621278e+06	1.5566615	0	0	3	0	3	2	1	3	0101000020E6100000421B62B78D4D5E40DBBE59E0F1473F40
8	329775.06	3.4621285e+06	1.557181	0	0	3	0	3	2	1	3	0101000020E6100000B3A14CB78D4D5E4090F5CF31F2473F40
8	329775.06	3.462129e+06	1.5564612	0	0	3	0	3	2	1	3	0101000020E6100000116949B78D4D5E402C36728DF2473F40
8	329775.06	3.4621295e+06	1.5557215	0	0	3	0	3	2	1	3	0101000020E6100000D28E4BB78D4D5E409C6000E4F2473F40
8	329775.1	3.4621302e+06	1.5550251	0	0	3	0	3	2	1	3	0101000020E6100000170052B78D4D5E40A5D9EE35F3473F40
8	329775.1	3.4621308e+06	1.5543706	0	0	3	0	3	2	1	3	0101000020E6100000DEBC5CB78D4D5E4009F30A83F3473F40
8	329775.1	3.4621312e+06	1.5534298	0	0	3	0	3	2	1	3	0101000020E6100000AC1070B78D4D5E40B9ACCCD8F3473F40
8	329775.12	3.4621318e+06	1.5520897	0	0	3	0	3	2	1	3	0101000020E6100000C36C92B78D4D5E40CD572E3CF4473F40
8	329775.12	3.4621325e+06	1.5512493	0	0	3	0	3	2	1	3	0101000020E61000003990B1B78D4D5E403C4F448FF4473F40
8	329775.12	3.462133e+06	1.5508374	0	0	3	0	3	2	1	3	0101000020E6100000AFB3D0B78D4D5E40D656F1E8F4473F40
8	329775.16	3.4621338e+06	1.5504098	0	0	3	0	3	2	1	3	0101000020E61000008835F5B78D4D5E40EA5FC44DF5473F40
8	329775.16	3.4621345e+06	1.5499336	0	0	3	0	3	2	1	3	0101000020E6100000674E22B88D4D5E40F81261BDF5473F40
8	329775.2	3.462135e+06	1.5495847	0	0	3	0	3	2	1	3	0101000020E61000009E9743B88D4D5E404E1D6B0BF6473F40
8	329775.2	3.4621355e+06	1.549211	0	0	3	0	3	2	1	3	0101000020E6100000393F6AB88D4D5E40B07C0460F6473F40
8	329775.22	3.462136e+06	1.5493453	0	0	3	0	3	2	1	3	0101000020E610000070888BB88D4D5E40BC6E8AB9F6473F40
8	329775.22	3.4621368e+06	1.5503047	0	0	3	0	3	2	1	3	0101000020E61000005DC99DB88D4D5E40F4CDE815F7473F40
8	329775.22	3.4621372e+06	1.551677	0	0	3	0	3	2	1	3	0101000020E61000006360A6B88D4D5E40A0838D77F7473F40
8	329775.25	3.462138e+06	1.5532963	0	0	3	0	3	2	1	3	0101000020E6100000FF01A1B88D4D5E4072986CDBF7473F40
8	329775.25	3.4621388e+06	1.5564476	0	0	3	0	3	2	1	3	0101000020E61000007DB070B88D4D5E400B8A2742F8473F40
8	329775.25	3.4621395e+06	1.5614815	0	0	3	0	3	2	1	3	0101000020E6100000369C09B88D4D5E401704CFAFF8473F40
8	329775.25	3.4621402e+06	1.5673528	0	0	3	0	3	2	1	3	0101000020E610000029C56BB78D4D5E404DA65F21F9473F40
8	329775.25	3.462141e+06	1.5730463	0	0	3	0	3	2	1	3	0101000020E6100000DE0DA4B68D4D5E40C467DD91F9473F40
8	329775.22	3.4621418e+06	1.5789915	0	0	3	0	3	2	1	3	0101000020E6100000C26594B58D4D5E404227840EFA473F40
8	329775.22	3.4621425e+06	1.5843118	0	0	3	0	3	2	1	3	0101000020E6100000CC3B60B48D4D5E406E720C89FA473F40
8	329775.2	3.4621432e+06	1.5883317	0	0	3	0	3	2	1	3	0101000020E6100000CBE31AB38D4D5E4027A04300FB473F40
8	329775.2	3.4621442e+06	1.5913639	0	0	3	0	3	2	1	3	0101000020E61000000FF7AFB18D4D5E40B362027FFB473F40
8	329775.16	3.462145e+06	1.5919833	0	0	3	0	3	2	1	3	0101000020E6100000603856B08D4D5E408991BEFDFB473F40
8	329775.16	3.4621458e+06	1.5886306	0	0	3	0	3	2	1	3	0101000020E6100000CE6C27AF8D4D5E40DCAE9B83FC473F40
8	329775.16	3.4621468e+06	1.5836434	0	0	3	0	3	2	1	3	0101000020E6100000BDF228AE8D4D5E40F3B95709FD473F40
8	329775.12	3.4621478e+06	1.5791459	0	0	3	0	3	2	1	3	0101000020E6100000753535AD8D4D5E40B02C6599FD473F40
8	329775.12	3.4621488e+06	1.5754303	0	0	3	0	3	2	1	3	0101000020E6100000ECA36FAC8D4D5E40E4A21125FE473F40
8	329775.12	3.4621492e+06	1.5734128	0	0	3	0	3	2	1	3	0101000020E61000002D7215AC8D4D5E40C6894D71FE473F40
8	329775.12	3.46215e+06	1.569488	0	0	3	0	3	2	1	3	0101000020E6100000B5AB95AB8D4D5E4057C675F9FE473F40
8	329775.16	3.462151e+06	1.5646013	0	0	3	0	3	2	1	3	0101000020E6100000BD3646AB8D4D5E40C97B3E8AFF473F40
8	329775.16	3.462152e+06	1.5601128	0	0	3	0	3	2	1	3	0101000020E6100000BF301AAB8D4D5E40E740061F00483F40
8	329775.2	3.462153e+06	1.5574644	0	0	3	0	3	2	1	3	0101000020E6100000490DFBAA8D4D5E4025A306B800483F40
8	329775.2	3.462154e+06	1.5570852	0	0	3	0	3	2	1	3	0101000020E6100000519ED7AA8D4D5E40FE4D8C4A01483F40
8	329775.2	3.462155e+06	1.5581845	0	0	3	0	3	2	1	3	0101000020E6100000EF39A6AA8D4D5E40AB21CEDB01483F40
8	329775.22	3.4621558e+06	1.5596974	0	0	3	0	3	2	1	3	0101000020E6100000C5186AAA8D4D5E40C2CA9D6802483F40
8	329775.22	3.4621568e+06	1.5611922	0	0	3	0	3	2	1	3	0101000020E6100000B54D24AA8D4D5E40156A38EA02483F40
8	329775.22	3.4621575e+06	1.5627047	0	0	3	0	3	2	1	3	0101000020E61000009EEBD5A98D4D5E4045AFE56203483F40
8	329775.22	3.4621582e+06	1.5641255	0	0	3	0	3	2	1	3	0101000020E6100000E65084A98D4D5E400F286FCD03483F40
8	329775.22	3.4621588e+06	1.5653466	0	0	3	0	3	2	1	3	0101000020E6100000CFEE35A98D4D5E40F0DF942704483F40
8	329775.22	3.4621595e+06	1.5667764	0	0	3	0	3	2	1	3	0101000020E6100000C6B4CCA88D4D5E40451C2C9104483F40
8	329775.22	3.46216e+06	1.5678338	0	0	3	0	3	2	1	3	0101000020E6100000C9A874A88D4D5E40803A5ADF04483F40
8	329775.22	3.4621605e+06	1.5689198	0	0	3	0	3	2	1	3	0101000020E6100000A71815A88D4D5E40997D0D2E05483F40
8	329775.22	3.462161e+06	1.5703892	0	0	3	0	3	2	1	3	0101000020E6100000669BB6A78D4D5E409612857B05483F40
8	329775.22	3.4621615e+06	1.5720363	0	1	3	0	3	2	1	3	0101000020E61000003D744EA78D4D5E40811A06C905483F40
8	329775.22	3.462162e+06	1.5723042	0	1	3	0	3	2	1	3	0101000020E6100000B2EEE0A68D4D5E404BF0E91506483F40
8	329775.22	3.4621625e+06	1.572285	0	1	3	0	3	2	1	3	0101000020E6100000266973A68D4D5E405931576406483F40
8	329775.22	3.462163e+06	1.5726343	0	1	3	0	3	2	1	3	0101000020E6100000179801A68D4D5E409FA9BFB506483F40
8	329775.22	3.4621638e+06	1.5729396	0	1	3	0	3	2	1	3	0101000020E610000046A18DA58D4D5E409444700807483F40
8	329775.22	3.4621642e+06	1.5723301	0	1	3	0	3	2	1	3	0101000020E6100000FF8C26A58D4D5E400AE6355C07483F40
8	329775.22	3.4621648e+06	1.5689547	0	1	3	0	3	2	1	3	0101000020E61000005E4EF7A48D4D5E403F8132B007483F40
8	329775.22	3.4621652e+06	1.5666012	0	1	3	0	3	2	1	3	0101000020E6100000405BCCA48D4D5E40DE10BC0008483F40
8	329775.25	3.4621658e+06	1.5651077	0	1	3	0	3	2	1	3	0101000020E6100000E48DA3A48D4D5E4073AAF94D08483F40
8	329775.25	3.4621665e+06	1.5653507	0	1	3	0	3	2	1	3	0101000020E6100000517759A48D4D5E40EEF69EA708483F40
8	329775.25	3.462167e+06	1.5661561	0	1	3	0	3	2	1	3	0101000020E6100000B232FEA38D4D5E402331DF0709483F40
8	329775.25	3.4621675e+06	1.5668356	0	1	3	0	3	2	1	3	0101000020E6100000F997ACA38D4D5E40422B6C5909483F40
8	329775.25	3.4621682e+06	1.5673566	0	1	3	0	3	2	1	3	0101000020E61000003B6652A38D4D5E4022D0EAB209483F40
8	329775.25	3.4621688e+06	1.567486	0	1	3	0	3	2	1	3	0101000020E6100000769DEFA28D4D5E404E2F63190A483F40
8	329775.25	3.4621695e+06	1.5669183	0	1	3	0	3	2	1	3	0101000020E61000005AA498A28D4D5E4062A94E800A483F40
8	329775.25	3.46217e+06	1.5661262	0	1	3	0	3	2	1	3	0101000020E6100000B3CE60A28D4D5E402A40C0CC0A483F40
8	329775.25	3.4621708e+06	1.5649205	0	1	3	0	3	2	1	3	0101000020E610000000CB17A28D4D5E40301DE23B0B483F40
8	329775.25	3.4621712e+06	1.5640576	0	1	3	0	3	2	1	3	0101000020E61000007F79E7A18D4D5E40949B1C8E0B483F40
8	329775.28	3.462172e+06	1.5632927	0	1	3	0	3	2	1	3	0101000020E6100000FD27B7A18D4D5E403B8C8DE50B483F40
8	329775.28	3.4621725e+06	1.562813	0	1	3	0	3	2	1	3	0101000020E61000009BC385A18D4D5E404C02753E0C483F40
8	329775.28	3.462173e+06	1.562795	0	1	3	0	3	2	1	3	0101000020E6100000D5004FA18D4D5E406360AC960C483F40
8	329775.28	3.4621738e+06	1.5633475	0	1	3	0	3	2	1	3	0101000020E6100000865B0BA18D4D5E4034AC7EF50C483F40
8	329775.28	3.4621742e+06	1.5642018	0	1	3	0	3	2	1	3	0101000020E61000001232C0A08D4D5E403B853E530D483F40
8	329775.28	3.462175e+06	1.5652184	0	1	3	0	3	2	1	3	0101000020E610000073ED64A08D4D5E40D965F6BC0D483F40
8	329775.28	3.4621758e+06	1.5661751	0	1	3	0	3	2	1	3	0101000020E6100000128307A08D4D5E4079E70C200E483F40
8	329775.28	3.4621762e+06	1.5671457	0	1	3	0	3	2	1	3	0101000020E6100000AC81A19F8D4D5E4038829C840E483F40
8	329775.28	3.462177e+06	1.5681176	0	1	3	0	3	2	1	3	0101000020E610000020FC339F8D4D5E403C3DEBE90E483F40
8	329775.28	3.4621778e+06	1.5690134	0	1	3	0	3	2	1	3	0101000020E6100000D250C49E8D4D5E40B126304C0F483F40
8	329775.28	3.4621782e+06	1.569768	0	1	3	0	3	2	1	3	0101000020E61000002D75609E8D4D5E40D33436A00F483F40
8	329775.28	3.462179e+06	1.5707248	0	1	3	0	3	2	1	3	0101000020E61000005150DB9D8D4D5E40D8806E0A10483F40
8	329775.28	3.4621795e+06	1.5714543	0	1	3	0	3	2	1	3	0101000020E6100000A6DD6E9D8D4D5E401054305B10483F40
8	329775.28	3.46218e+06	1.5722078	0	1	3	0	3	2	1	3	0101000020E6100000B6F9FB9C8D4D5E40B1809DAD10483F40
8	329775.28	3.4621802e+06	1.5724922	0	1	3	1	3	2	1	3	0101000020E61000009806D19C8D4D5E40BCA29BCA10483F40
0	329775.28	3.4621808e+06	1.5764493	0	2	3	0	3	2	1	3	0101000020E6100000A485299C8D4D5E4029E3791711483F40
0	329775.28	3.4621812e+06	1.5812776	0	2	3	0	3	2	1	3	0101000020E6100000615F3E9B8D4D5E404DE6577011483F40
0	329775.25	3.462182e+06	1.5886123	0	2	3	0	3	2	1	3	0101000020E61000006007F9998D4D5E409080F1CE11483F40
0	329775.22	3.4621825e+06	1.5984265	0	2	3	0	3	2	1	3	0101000020E61000006DD16C988D4D5E4099008D2412483F40
0	329775.2	3.462183e+06	1.6124552	0	2	3	0	3	2	1	3	0101000020E61000009AD926968D4D5E4030AC288712483F40
0	329775.16	3.4621838e+06	1.625341	0	2	3	0	3	2	1	3	0101000020E6100000A7F4E1938D4D5E40F66EC3D612483F40
0	329775.1	3.4621842e+06	1.6412762	0	2	3	0	3	2	1	3	0101000020E61000003E43F1908D4D5E404A7B2D2F13483F40
0	329775	3.4621848e+06	1.6604985	0	2	3	0	3	2	1	3	0101000020E610000012BD458D8D4D5E407DC0958A13483F40
0	329774.9	3.4621855e+06	1.6831137	0	2	3	0	3	2	1	3	0101000020E6100000CF2BBF888D4D5E40C386FAE813483F40
0	329774.8	3.462186e+06	1.7095038	0	2	3	0	3	2	1	3	0101000020E610000026EA19838D4D5E4063CA704D14483F40
0	329774.66	3.4621868e+06	1.7389239	0	2	3	0	3	2	1	3	0101000020E61000001D89327C8D4D5E40D5E68BB614483F40
0	329774.5	3.4621875e+06	1.7696942	0	2	3	0	3	2	1	3	0101000020E6100000E4C04D748D4D5E409B96661F15483F40
0	329774.34	3.462188e+06	1.793332	0	2	3	0	3	2	1	3	0101000020E61000006F76B06D8D4D5E40D6FF5E6E15483F40
0	329774.12	3.4621888e+06	1.8267801	0	2	3	0	3	2	1	3	0101000020E6100000F9C077638D4D5E4094B572DC15483F40
0	329773.94	3.4621892e+06	1.8525429	0	2	3	0	3	2	1	3	0101000020E610000024A8DF5A8D4D5E40F371AB3016483F40
0	329773.75	3.4621898e+06	1.8771242	0	2	3	0	3	2	1	3	0101000020E61000000D1815528D4D5E40E4814D8016483F40
0	329773.56	3.4621902e+06	1.9011837	0	2	3	0	3	2	1	3	0101000020E61000003CEDF8488D4D5E40DA0827CD16483F40
0	329773.34	3.4621908e+06	1.9253733	0	2	3	0	3	2	1	3	0101000020E610000024AE753F8D4D5E406B2AFD1717483F40
0	329773.12	3.4621915e+06	1.9513037	0	2	3	0	3	2	1	3	0101000020E6100000DD07F5348D4D5E40E334FE6417483F40
0	329772.78	3.462192e+06	1.9867584	0	2	3	0	3	2	1	3	0101000020E610000042B460268D4D5E406E9FE1C617483F40
0	329772.47	3.4621928e+06	2.023184	0	2	3	0	3	2	1	3	0101000020E6100000270970178D4D5E40F9CF9F2118483F40
0	329772.16	3.4621932e+06	2.0581636	0	2	3	0	3	2	1	3	0101000020E61000004844D5088D4D5E409A028A7218483F40
0	329771.8	3.4621938e+06	2.092933	0	2	3	0	3	2	1	3	0101000020E61000007304BFF98C4D5E40631091BF18483F40
0	329771.5	3.4621942e+06	2.126435	0	2	3	0	3	2	1	3	0101000020E61000005CFC2EEB8C4D5E4033E5E60319483F40
0	329771.1	3.4621948e+06	2.1673465	0	2	3	0	3	2	1	3	0101000020E61000008B9D6AD98C4D5E4060D4315019483F40
0	329770.72	3.462195e+06	2.2049487	0	2	3	0	3	2	1	3	0101000020E6100000F8CD88C88C4D5E40BC21CB9219483F40
0	329770.34	3.4621955e+06	2.2415588	0	2	3	0	3	2	1	3	0101000020E6100000417473B78C4D5E40F7D12BD119483F40
0	329769.9	3.462196e+06	2.2807572	0	2	3	0	3	2	1	3	0101000020E61000007BEB7AA48C4D5E4082B738111A483F40
0	329769.5	3.4621962e+06	2.319173	0	2	3	0	3	2	1	3	0101000020E61000003B51E7918C4D5E405D79CD4A1A483F40
0	329769.06	3.4621968e+06	2.356947	0	2	3	0	3	2	1	3	0101000020E610000054E11B7F8C4D5E409DC4B1801A483F40
0	329768.62	3.462197e+06	2.3965337	0	2	3	0	3	2	1	3	0101000020E61000005C4EC56A8C4D5E40CBE68DB61A483F40
0	329768.1	3.4621975e+06	2.440923	0	2	3	0	3	2	1	3	0101000020E6100000BDD80C548C4D5E407B3B4AED1A483F40
0	329767.62	3.4621978e+06	2.4811127	0	2	3	0	3	2	1	3	0101000020E6100000D855EB3E8C4D5E404CBAC61B1B483F40
0	329767.1	3.462198e+06	2.5263116	0	2	3	0	3	2	1	3	0101000020E61000009FD46E268C4D5E4071ECD34C1B483F40
0	329766.5	3.4621985e+06	2.571041	0	2	3	0	3	2	1	3	0101000020E6100000E458350D8C4D5E404A1B7D7A1B483F40
0	329766.03	3.4621988e+06	2.6073165	0	2	3	0	3	2	1	3	0101000020E6100000585123F78B4D5E4090312B9F1B483F40
0	329765.5	3.462199e+06	2.6425257	0	2	3	0	3	2	1	3	0101000020E610000004DE4DE08B4D5E4003C728C21B483F40
0	329764.97	3.462199e+06	2.6744041	0	2	3	0	3	2	1	3	0101000020E61000001EA575C88B4D5E40509380E41B483F40
0	329764.38	3.4621992e+06	2.70198	0	2	3	0	3	2	1	3	0101000020E6100000157ECCAE8B4D5E40CDC6D5071C483F40
0	329763.78	3.4621995e+06	2.7242513	0	2	3	0	3	2	1	3	0101000020E61000009C6C9B938B4D5E405C0A0F2C1C483F40
0	329763.12	3.4621998e+06	2.7410944	0	2	3	0	3	2	1	3	0101000020E6100000836D82778B4D5E40130B96501C483F40
0	329762.5	3.4622e+06	2.7536495	0	2	3	0	3	2	1	3	0101000020E6100000CFC6425B8B4D5E409DE391741C483F40
0	329761.84	3.4622002e+06	2.7651238	0	2	3	0	3	2	1	3	0101000020E6100000FC834B3E8B4D5E40AB6B80981C483F40
0	329761.22	3.4622005e+06	2.7781067	0	2	3	0	3	2	1	3	0101000020E61000005D4586228B4D5E40B129EEB81C483F40
0	329760.62	3.4622008e+06	2.7945256	0	2	3	0	3	2	1	3	0101000020E6100000086D41088B4D5E4064D659D51C483F40
0	329760.03	3.462201e+06	2.8185542	0	2	3	0	3	2	1	3	0101000020E61000003049F8ED8A4D5E40548E39EE1C483F40
0	329759.5	3.462201e+06	2.849499	0	2	3	0	3	2	1	3	0101000020E61000008C8752D68A4D5E40D1593E001D483F40
0	329759	3.4622012e+06	2.8854704	0	2	3	0	3	2	1	3	0101000020E6100000D2C1CFBF8A4D5E409EFB4D0D1D483F40
0	329758.34	3.4622012e+06	2.9350805	0	2	3	0	3	2	1	3	0101000020E6100000CB87D0A38A4D5E40606222181D483F40
0	329757.78	3.4622012e+06	2.9801936	0	2	3	0	3	2	1	3	0101000020E610000014A9CB8A8A4D5E40BB307B1D1D483F40
0	329757.28	3.4622012e+06	3.0171142	0	2	3	0	3	2	1	3	0101000020E61000001866EA738A4D5E4035FEF81F1D483F40
0	329756.66	3.4622012e+06	3.0491652	0	2	3	0	3	2	1	3	0101000020E6100000448790588A4D5E409AFF0D221D483F40
0	329756.12	3.4622015e+06	3.0653248	0	2	3	0	3	2	1	3	0101000020E610000035D608418A4D5E40B33037241D483F40
0	329755.53	3.4622015e+06	3.0767548	0	2	3	0	3	2	1	3	0101000020E6100000B97FE8268A4D5E40E291B7261D483F40
9	329755	3.4622015e+06	3.0843813	0	2	3	0	3	2	1	3	0101000020E610000059E42D108A4D5E40C40FA5281D483F40
9	329754.44	3.4622015e+06	3.094207	0	2	3	0	3	2	1	3	0101000020E6100000D014E1F6894D5E40FF40C5291D483F40
9	329753.9	3.4622015e+06	3.103325	0	2	3	0	3	2	1	3	0101000020E6100000A519BBDF894D5E404122122A1D483F40
0	329775.28	3.4621802e+06	1.5854015	0	2	3	0	3	2	1	3	0101000020E6100000B062F39C8D4D5E4042EC26DA10483F40
0	329775.28	3.4621808e+06	1.5821217	0	2	3	0	3	2	1	3	0101000020E61000008FCC679C8D4D5E4040A7EC2811483F40
0	329775.28	3.4621812e+06	1.5799358	0	2	3	0	3	2	1	3	0101000020E6100000DEC2F29B8D4D5E402F6BF27511483F40
0	329775.28	3.4621818e+06	1.5776342	0	2	3	0	3	2	1	3	0101000020E6100000523D859B8D4D5E40255D67C611483F40
0	329775.28	3.4621822e+06	1.5752916	0	2	3	0	3	2	1	3	0101000020E610000030AD259B8D4D5E40004ADC1712483F40
0	329775.28	3.462183e+06	1.5722781	0	2	3	0	3	2	1	3	0101000020E610000000F5E09A8D4D5E4023ED466812483F40
0	329775.3	3.4621835e+06	1.5642622	0	2	3	0	3	2	1	3	0101000020E61000009B9C079B8D4D5E40F9FD65B712483F40
0	329775.34	3.462184e+06	1.551045	0	2	3	0	3	2	1	3	0101000020E61000000541CE9B8D4D5E401FF2B90313483F40
0	329775.38	3.4621845e+06	1.5234219	0	2	3	0	3	2	1	3	0101000020E61000009B58EA9D8D4D5E40E54CEB5B13483F40
0	329775.47	3.462185e+06	1.4919524	0	2	3	0	3	2	1	3	0101000020E61000004F12EAA08D4D5E40ED9F8BAC13483F40
0	329775.56	3.4621855e+06	1.4563503	0	2	3	0	3	2	1	3	0101000020E61000001446E8A48D4D5E40343795FB13483F40
0	329775.7	3.4621862e+06	1.4137312	0	2	3	0	3	2	1	3	0101000020E6100000A62B6BAA8D4D5E401B59C85014483F40
0	329775.88	3.4621868e+06	1.3621305	0	2	3	0	3	2	1	3	0101000020E6100000381E18B28D4D5E40D804FDAE14483F40
0	329776.12	3.4621872e+06	1.2985166	0	2	3	0	3	2	1	3	0101000020E6100000006710BC8D4D5E4057277B0C15483F40
0	329776.38	3.462188e+06	1.2309209	0	2	3	0	3	2	1	3	0101000020E6100000B2B157C78D4D5E40B7E9305F15483F40
0	329776.66	3.4621885e+06	1.1621931	0	2	3	0	3	2	1	3	0101000020E610000015AFA0D38D4D5E40E41C99A815483F40
0	329777.06	3.4621888e+06	1.0686951	0	2	3	0	3	2	1	3	0101000020E6100000FF05EAE38D4D5E40753E6FF315483F40
0	329777.4	3.4621892e+06	0.97105587	0	2	3	0	3	2	1	3	0101000020E6100000A04EF8F38D4D5E40AE2A6E2916483F40
0	329777.88	3.4621895e+06	0.86300373	0	2	3	0	3	2	1	3	0101000020E610000057B537078E4D5E4071503E5D16483F40
0	329778.3	3.4621898e+06	0.7733257	0	2	3	0	3	2	1	3	0101000020E610000021DB641A8E4D5E40055F288A16483F40
0	329778.75	3.46219e+06	0.70077664	0	2	3	0	3	2	1	3	0101000020E6100000A838182E8E4D5E40D902C3B416483F40
0	329779.22	3.4621905e+06	0.64596814	0	2	3	0	3	2	1	3	0101000020E61000006E1F82428E4D5E4014DABADF16483F40
0	329779.7	3.4621908e+06	0.59848005	0	2	3	0	3	2	1	3	0101000020E61000007D1427578E4D5E402DAA0E0817483F40
0	329780.25	3.462191e+06	0.54020816	0	2	3	0	3	2	1	3	0101000020E610000086DD5E6F8E4D5E4004E4DF3017483F40
0	329780.75	3.462191e+06	0.47427034	0	2	3	0	3	2	1	3	0101000020E61000005B44F3848E4D5E40570D1B4C17483F40
0	329781.3	3.4621912e+06	0.37703577	0	2	3	0	3	2	1	3	0101000020E6100000B604889E8E4D5E40B9E6C65F17483F40
0	329781.8	3.4621912e+06	0.29099315	0	2	3	0	3	2	1	3	0101000020E6100000E53E71B48E4D5E40FE8B756817483F40
0	329782.44	3.4621912e+06	0.18603702	0	2	3	0	3	2	1	3	0101000020E61000002AFB28CF8E4D5E408135EB6917483F40
0	329782.94	3.4621912e+06	0.09763367	0	2	3	0	3	2	1	3	0101000020E6100000CF07EAE58E4D5E40B6F9CB6317483F40
0	329783.47	3.4621912e+06	0.01754384	0	2	3	0	3	2	1	3	0101000020E6100000DC0FE5FC8E4D5E404DBDBF5817483F40
0	329783.97	3.462191e+06	-0.008000024	0	2	3	0	3	2	1	3	0101000020E61000007BD6E4128F4D5E40DAF0B55417483F40
0	329784.5	3.462191e+06	-0.0069229696	0	2	3	0	3	2	1	3	0101000020E61000006F88E9298F4D5E4094029D5517483F40
5	329785	3.462191e+06	-0.0058601894	0	2	3	0	3	2	1	3	0101000020E61000008FA645408F4D5E407366945617483F40
5	329785.56	3.462191e+06	-0.00465494	0	2	3	0	3	2	1	3	0101000020E6100000ABDD23598F4D5E405C30C05717483F40
0	329758.56	3.4621908e+06	-0.060294636	0	2	3	0	3	2	1	3	0101000020E610000078ADA8B48A4D5E402C1C89DB16483F40
0	329759.16	3.4621905e+06	-0.10459411	0	2	3	0	3	2	1	3	0101000020E61000002C47BECE8A4D5E40C66889CC16483F40
0	329759.7	3.4621905e+06	-0.14913101	0	2	3	0	3	2	1	3	0101000020E61000005ED3C0E58A4D5E406EEF94BA16483F40
0	329760.22	3.4621902e+06	-0.2118863	0	2	3	0	3	2	1	3	0101000020E6100000344153FD8A4D5E40B78027A016483F40
0	329760.7	3.46219e+06	-0.29555753	0	2	3	0	3	2	1	3	0101000020E61000003D4EA8128B4D5E40228DC17C16483F40
0	329761.12	3.4621898e+06	-0.41030592	0	2	3	0	3	2	1	3	0101000020E610000055708C258B4D5E406970E44C16483F40
0	329761.47	3.4621892e+06	-0.5373333	0	2	3	0	3	2	1	3	0101000020E6100000E4F380358B4D5E407ED7601416483F40
0	329761.78	3.462189e+06	-0.66078186	0	2	3	0	3	2	1	3	0101000020E6100000D2DAD4438B4D5E40752434D515483F40
0	329762.06	3.4621885e+06	-0.7861553	0	2	3	0	3	2	1	3	0101000020E61000007C43F84F8B4D5E40C2D1F08E15483F40
0	329762.3	3.462188e+06	-0.88076985	0	2	3	0	3	2	1	3	0101000020E6100000162CF15A8B4D5E40D786824915483F40
0	329762.53	3.4621875e+06	-0.9625996	0	2	3	0	3	2	1	3	0101000020E61000001C04CC658B4D5E40C60D50FE14483F40
0	329762.75	3.462187e+06	-1.0198399	0	2	3	0	3	2	1	3	0101000020E6100000A578F26F8B4D5E40928EA9B514483F40
0	329762.97	3.4621865e+06	-1.0741355	0	2	3	0	3	2	1	3	0101000020E6100000AD9BE8798B4D5E407D3F9E6614483F40
0	329763.2	3.462186e+06	-1.1258143	0	2	3	0	3	2	1	3	0101000020E6100000E4C76A838B4D5E4045652F1214483F40
0	329763.34	3.4621855e+06	-1.1714463	0	2	3	0	3	2	1	3	0101000020E6100000B1F1B48A8B4D5E4055D3CFC613483F40
0	329763.47	3.462185e+06	-1.2189058	0	2	3	0	3	2	1	3	0101000020E6100000714461918B4D5E4087EFEF7513483F40
0	329763.62	3.4621842e+06	-1.2563925	0	2	3	0	3	2	1	3	0101000020E6100000C6F872978B4D5E40F474532613483F40
0	329763.75	3.4621838e+06	-1.2894719	0	2	3	0	3	2	1	3	0101000020E6100000AD1A429D8B4D5E408871E5D212483F40
0	329763.84	3.4621832e+06	-1.3153112	0	2	3	0	3	2	1	3	0101000020E610000041F46CA28B4D5E4039684F8312483F40
0	329763.97	3.4621828e+06	-1.3377309	0	2	3	0	3	2	1	3	0101000020E6100000BD77A1A78B4D5E408FF44E2E12483F40
0	329764.06	3.4621822e+06	-1.3546399	0	2	3	0	3	2	1	3	0101000020E6100000A14757AC8B4D5E407284FEDD11483F40
0	329764.16	3.4621815e+06	-1.3589997	0	2	3	0	3	2	1	3	0101000020E6100000332DDAB18B4D5E40B3F34D8911483F40
0	329764.28	3.462181e+06	-1.3618973	0	2	3	0	3	2	1	3	0101000020E6100000098463B78B4D5E407D32033411483F40
2	329764.38	3.4621805e+06	-1.3648859	0	2	3	2	3	2	1	3	0101000020E61000007AD35ABC8B4D5E4063723DE610483F40
2	329764.5	3.46218e+06	-1.3723115	0	2	3	0	3	2	1	3	0101000020E6100000B1E588C18B4D5E4062F3158F10483F40
2	329764.56	3.4621795e+06	-1.3990694	0	2	3	0	3	2	1	3	0101000020E61000002E5667C48B4D5E40F6D8854310483F40
2	329764.56	3.462179e+06	-1.4414954	0	2	3	0	3	2	1	3	0101000020E6100000B3F9B0C58B4D5E40C5D5C1F40F483F40
2	329764.56	3.4621785e+06	-1.4861888	0	2	3	0	3	2	1	3	0101000020E61000000399C8C58B4D5E40AD41C7A60F483F40
2	329764.56	3.462178e+06	-1.523203	0	2	3	0	3	2	1	3	0101000020E6100000068D70C58B4D5E40F4102C570F483F40
10	329784.97	3.4622015e+06	-3.1331809	0	2	3	0	3	2	1	3	0101000020E61000001A4952378F4D5E40ED9714861D483F40
0	329784.44	3.4622015e+06	-3.1343746	0	2	3	0	3	2	1	3	0101000020E610000078DFF1208F4D5E40CC52D0841D483F40
0	329783.94	3.4622015e+06	-3.136035	0	2	3	0	3	2	1	3	0101000020E61000005281000A8F4D5E4042AE63831D483F40
0	329783.34	3.4622015e+06	-3.1303601	0	2	3	0	3	2	1	3	0101000020E610000021E2A7F08E4D5E40789215801D483F40
0	329782.75	3.4622015e+06	-3.1175725	0	2	3	0	3	2	1	3	0101000020E6100000374A8CD58E4D5E4059FB597A1D483F40
0	329782.22	3.4622015e+06	-3.102105	0	2	3	0	3	2	1	3	0101000020E6100000890362BE8E4D5E404A036A731D483F40
0	329781.66	3.4622015e+06	-3.0770257	0	2	3	0	3	2	1	3	0101000020E6100000FD3F6DA58E4D5E40A28871681D483F40
0	329781.06	3.4622012e+06	-3.04251	0	2	3	0	3	2	1	3	0101000020E610000068420F8C8E4D5E40ADE4C6581D483F40
0	329780.56	3.4622012e+06	-3.0062668	0	2	3	0	3	2	1	3	0101000020E61000002AE66C768E4D5E4032F445471D483F40
0	329780.06	3.462201e+06	-2.9654062	0	2	3	0	3	2	1	3	0101000020E610000093AA1D608E4D5E4053BD62311D483F40
0	329779.6	3.462201e+06	-2.9252481	0	2	3	0	3	2	1	3	0101000020E61000007D75E34A8E4D5E4019101B191D483F40
0	329779.06	3.4622008e+06	-2.881938	0	2	3	0	3	2	1	3	0101000020E610000094F71B348E4D5E40AFE181FB1C483F40
0	329778.53	3.4622005e+06	-2.8383648	0	2	3	0	3	2	1	3	0101000020E610000077CD671D8E4D5E4014643DDA1C483F40
0	329778	3.4622002e+06	-2.791818	0	2	3	0	3	2	1	3	0101000020E6100000021B7A058E4D5E40A7AFE2B21C483F40
0	329777.4	3.4622e+06	-2.7399423	0	2	3	0	3	2	1	3	0101000020E6100000D85D45EB8D4D5E403E8364821C483F40
0	329776.94	3.4621998e+06	-2.7003915	0	2	3	0	3	2	1	3	0101000020E6100000FDBD19D78D4D5E405EAB3B591C483F40
0	329776.38	3.4621992e+06	-2.6544085	0	2	3	0	3	2	1	3	0101000020E610000026A7FABE8D4D5E406C6FB5231C483F40
0	329775.97	3.462199e+06	-2.6202857	0	2	3	0	3	2	1	3	0101000020E6100000F6D7ACAC8D4D5E406B58C5F71B483F40
0	329775.47	3.4621988e+06	-2.5784996	0	2	3	0	3	2	1	3	0101000020E6100000D862DD968D4D5E401E5B10BF1B483F40
0	329774.97	3.4621982e+06	-2.5385044	0	2	3	0	3	2	1	3	0101000020E6100000462234828D4D5E4023CBCC841B483F40
0	329774.53	3.462198e+06	-2.5023634	0	2	3	0	3	2	1	3	0101000020E61000002323866E8D4D5E40F174A6491B483F40
0	329774.1	3.4621975e+06	-2.4706383	0	2	3	0	3	2	1	3	0101000020E61000001DC6BB5B8D4D5E40455E2C0E1B483F40
0	329773.7	3.4621972e+06	-2.4495177	0	2	3	0	3	2	1	3	0101000020E610000063C9454A8D4D5E40808E4ED61A483F40
0	329773.3	3.4621968e+06	-2.439609	0	2	3	0	3	2	1	3	0101000020E6100000815420398D4D5E404A45B0A01A483F40
0	329772.9	3.4621965e+06	-2.4329574	0	2	3	0	3	2	1	3	0101000020E6100000C2692E288D4D5E403B29A66B1A483F40
0	329772.5	3.4621962e+06	-2.4261813	0	2	3	0	3	2	1	3	0101000020E61000000D0AED168D4D5E4084D9F2341A483F40
0	329772.12	3.4621958e+06	-2.417592	0	2	3	0	3	2	1	3	0101000020E6100000D0702B068D4D5E40C0CB74FE19483F40
0	329771.75	3.4621955e+06	-2.404293	0	2	3	0	3	2	1	3	0101000020E61000000B9EE9F58C4D5E40351A10C719483F40
0	329771.38	3.462195e+06	-2.384025	0	2	3	0	3	2	1	3	0101000020E61000002BD250E58C4D5E40076DC78A19483F40
0	329770.97	3.4621945e+06	-2.3579264	0	2	3	0	3	2	1	3	0101000020E6100000F2260BD48C4D5E40517ED04719483F40
0	329770.6	3.4621942e+06	-2.333068	0	2	3	0	3	2	1	3	0101000020E610000028BDC0C38C4D5E40E177750519483F40
0	329770.22	3.4621938e+06	-2.3106391	0	2	3	0	3	2	1	3	0101000020E6100000CE8819B48C4D5E408FC833C318483F40
0	329769.84	3.4621932e+06	-2.2882075	0	2	3	0	3	2	1	3	0101000020E61000001AD264A38C4D5E409379DB7918483F40
0	329769.44	3.4621928e+06	-2.2672412	0	2	3	0	3	2	1	3	0101000020E610000077373D928C4D5E402F15F52B18483F40
0	329769.03	3.4621922e+06	-2.2480497	0	2	3	0	3	2	1	3	0101000020E6100000889406818C4D5E40414A42DB17483F40
0	329768.75	3.4621918e+06	-2.235343	0	2	3	0	3	2	1	3	0101000020E61000003D4427748C4D5E40DB5F819D17483F40
0	329768.44	3.4621915e+06	-2.2224927	0	2	3	0	3	2	1	3	0101000020E6100000816731678C4D5E40ACE3AE5D17483F40
0	329768.12	3.462191e+06	-2.208815	0	2	3	0	3	2	1	3	0101000020E61000001C12A3598C4D5E402474071917483F40
0	329767.8	3.4621905e+06	-2.1954026	0	2	3	0	3	2	1	3	0101000020E6100000D2BB974C8C4D5E4019CC02D516483F40
0	329767.53	3.46219e+06	-2.181706	0	2	3	0	3	2	1	3	0101000020E6100000C63F8A3F8C4D5E4008B8EE8E16483F40
0	329767.22	3.4621895e+06	-2.167334	0	2	3	0	3	2	1	3	0101000020E61000008CB4C4328C4D5E4004B3EC4716483F40
0	329766.94	3.462189e+06	-2.149852	0	2	3	0	3	2	1	3	0101000020E6100000FBF2DE258C4D5E40B5C3AAFC15483F40
0	329766.62	3.4621885e+06	-2.1300533	0	2	3	0	3	2	1	3	0101000020E6100000557E63198C4D5E40AD77B7AF15483F40
0	329766.34	3.462188e+06	-2.10799	0	2	3	0	3	2	1	3	0101000020E61000008F1CE90C8C4D5E40B13F815E15483F40
0	329766.06	3.4621875e+06	-2.0843055	0	2	3	0	3	2	1	3	0101000020E6100000243D7C018C4D5E40125FB00E15483F40
0	329765.8	3.462187e+06	-2.0572338	0	2	3	0	3	2	1	3	0101000020E6100000341837F68B4D5E40E69FABB914483F40
0	329765.56	3.4621865e+06	-2.028358	0	2	3	0	3	2	1	3	0101000020E6100000A55D4FEB8B4D5E40FCA79F6014483F40
0	329765.3	3.4621858e+06	-1.9982351	0	2	3	0	3	2	1	3	0101000020E61000003ADC53E18B4D5E403653EB0614483F40
0	329765.1	3.4621852e+06	-1.9655477	0	2	3	0	3	2	1	3	0101000020E6100000128143D88B4D5E40DA63DCAB13483F40
0	329764.9	3.4621845e+06	-1.9285231	0	2	3	0	3	2	1	3	0101000020E61000002A5876D08B4D5E40BA0F595013483F40
0	329764.75	3.462184e+06	-1.8838044	0	2	3	0	3	2	1	3	0101000020E6100000554C08CA8B4D5E404ECB18F212483F40
0	329764.62	3.4621832e+06	-1.8318139	0	2	3	0	3	2	1	3	0101000020E610000024D1E2C48B4D5E40E2C3488D12483F40
0	329764.56	3.4621828e+06	-1.7914261	0	2	3	0	3	2	1	3	0101000020E6100000877305C28B4D5E40577D983F12483F40
0	329764.5	3.4621822e+06	-1.7459494	0	2	3	0	3	2	1	3	0101000020E61000006E10E5BF8B4D5E407BA1C8E711483F40
0	329764.47	3.4621818e+06	-1.7050751	0	2	3	0	3	2	1	3	0101000020E61000002BEAF9BE8B4D5E40015ED29811483F40
0	329764.47	3.462181e+06	-1.649758	0	2	3	0	3	2	1	3	0101000020E61000008D4E2BBF8B4D5E401A508F2F11483F40
0	329786.1	3.4621128e+06	-3.126462	0	2	3	0	3	2	1	3	0101000020E6100000E6461CA88F4D5E401E9FB91CE9473F40
0	329785.53	3.4621128e+06	-3.1303282	0	2	3	0	3	2	1	3	0101000020E6100000777C7E8F8F4D5E408FC7321BE9473F40
0	329785	3.4621128e+06	-3.134729	0	2	3	0	3	2	1	3	0101000020E61000004AD8CB778F4D5E404704131AE9473F40
0	329784.44	3.4621128e+06	-3.1410213	0	2	3	0	3	2	1	3	0101000020E610000081921E608F4D5E40BB1AB719E9473F40
0	329783.94	3.462113e+06	3.1295624	0	2	3	0	3	2	1	3	0101000020E6100000544CDD498F4D5E40CAAD881BE9473F40
0	329783.38	3.462113e+06	3.1094718	0	2	3	0	3	2	1	3	0101000020E6100000B2D552318F4D5E40CD6A3D20E9473F40
0	329782.88	3.462113e+06	3.0908778	0	2	3	0	3	2	1	3	0101000020E6100000FEF71F1A8F4D5E40DFA71C26E9473F40
0	329782.28	3.462113e+06	3.0695076	0	2	3	0	3	2	1	3	0101000020E61000004913EF008F4D5E400D1D742EE9473F40
0	329781.78	3.462113e+06	3.0454204	0	2	3	0	3	2	1	3	0101000020E6100000284C06EA8E4D5E4018B1D338E9473F40
0	329781.25	3.4621132e+06	3.0160446	0	2	3	0	3	2	1	3	0101000020E610000090B871D28E4D5E4050F17D46E9473F40
0	329780.72	3.4621132e+06	2.981042	0	2	3	0	3	2	1	3	0101000020E6100000AADD0ABC8E4D5E40BD747B57E9473F40
0	329780.22	3.4621135e+06	2.9396398	0	2	3	0	3	2	1	3	0101000020E61000003EBDCBA58E4D5E400B57C56CE9473F40
0	329779.66	3.4621138e+06	2.8839974	0	2	3	0	3	2	1	3	0101000020E61000000A2ACB8C8E4D5E4049E70F8BE9473F40
0	329779.16	3.462114e+06	2.825077	0	2	3	0	3	2	1	3	0101000020E6100000FADCE0768E4D5E401B7C7EACE9473F40
0	329778.72	3.4621142e+06	2.762565	0	2	3	0	3	2	1	3	0101000020E6100000B6F65F638E4D5E400A942FD1E9473F40
0	329778.3	3.4621145e+06	2.6906533	0	2	3	0	3	2	1	3	0101000020E610000042B60B518E4D5E40A95F12FCE9473F40
0	329777.94	3.4621148e+06	2.6037045	0	2	3	0	3	2	1	3	0101000020E6100000D65ED93F8E4D5E403C1EE52EEA473F40
0	329777.56	3.4621152e+06	2.5030787	0	2	3	0	3	2	1	3	0101000020E61000006182222F8E4D5E40F46F9A6CEA473F40
0	329777.2	3.4621158e+06	2.3935547	0	2	3	0	3	2	1	3	0101000020E6100000C8CAF01E8E4D5E408ED156B6EA473F40
0	329776.94	3.4621162e+06	2.302487	0	2	3	0	3	2	1	3	0101000020E6100000778C95128E4D5E40EB443EFAEA473F40
0	329776.66	3.4621168e+06	2.2145562	0	2	3	0	3	2	1	3	0101000020E6100000C6E3DC058E4D5E4012596349EB473F40
0	329776.4	3.4621172e+06	2.148654	0	2	3	0	3	2	1	3	0101000020E6100000435706FB8D4D5E4096D59293EB473F40
0	329776.2	3.4621178e+06	2.1051025	0	2	3	0	3	2	1	3	0101000020E610000021E445F18D4D5E40FB1EF5D7EB473F40
0	329775.94	3.4621182e+06	2.0745518	0	2	3	0	3	2	1	3	0101000020E6100000F1EA2EE68D4D5E407022A724EC473F40
0	329775.72	3.4621188e+06	2.0511284	0	2	3	0	3	2	1	3	0101000020E61000002AED51DB8D4D5E40B7489871EC473F40
0	329775.5	3.4621192e+06	2.0268059	0	2	3	0	3	2	1	3	0101000020E6100000945046D18D4D5E40C4B4FEBDEC473F40
0	329775.3	3.4621198e+06	1.9935732	0	2	3	0	3	2	1	3	0101000020E6100000F77AA3C88D4D5E404B32000AED473F40
0	329775.16	3.4621202e+06	1.939954	0	2	3	0	3	2	1	3	0101000020E61000006AD4E3C18D4D5E40BBE4C85AED473F40
0	329775.06	3.4621208e+06	1.8735254	0	2	3	0	3	2	1	3	0101000020E6100000D5FAB8BC8D4D5E40B2230AB3ED473F40
0	329775	3.4621215e+06	1.8038807	0	2	3	0	3	2	1	3	0101000020E6100000889366B98D4D5E405439550DEE473F40
0	329774.97	3.462122e+06	1.7434758	0	2	3	0	3	2	1	3	0101000020E6100000E6FCF1B78D4D5E409F3B9C5CEE473F40
0	329774.97	3.4621225e+06	1.6887585	0	2	3	0	3	2	1	3	0101000020E6100000BDD589B78D4D5E4084AC14AAEE473F40
0	329774.97	3.462123e+06	1.6379862	0	2	3	0	3	2	1	3	0101000020E6100000BCDBB5B78D4D5E4016F76A02EF473F40
8	329775	3.4621242e+06	1.5903852	0	2	3	0	3	2	1	3	0101000020E6100000D19477B78D4D5E40F83AE2B5EF473F40
13	329752.25	3.462102e+06	0.0156775	0	2	3	0	3	2	1	3	0101000020E6100000576121DD894D5E4077909768E2473F40
13	329752.75	3.462102e+06	0.01768605	0	2	3	0	3	2	1	3	0101000020E6100000627574F4894D5E409313DA6BE2473F40
13	329753.34	3.462102e+06	0.022050438	0	2	3	0	3	2	1	3	0101000020E61000000BDB4C0E8A4D5E404456F36FE2473F40
0	329754	3.4621022e+06	0.03042395	0	2	3	0	3	2	1	3	0101000020E6100000B0B01A2A8A4D5E408F3AB675E2473F40
0	329754.62	3.4621022e+06	0.044390105	0	2	3	0	3	2	1	3	0101000020E610000024D4CF458A4D5E40CE99A37DE2473F40
0	329755.12	3.4621022e+06	0.058370102	0	2	3	0	3	2	1	3	0101000020E6100000A750315C8A4D5E4075BCA785E2473F40
0	329755.7	3.4621022e+06	0.07729934	0	2	3	0	3	2	1	3	0101000020E610000061D296758A4D5E407904C990E2473F40
0	329756.28	3.4621022e+06	0.099859014	0	2	3	0	3	2	1	3	0101000020E6100000844F368F8A4D5E402E95809EE2473F40
0	329756.9	3.4621025e+06	0.12679963	0	2	3	0	3	2	1	3	0101000020E61000002DC238AB8A4D5E40FE8F56B0E2473F40
0	329757.66	3.4621025e+06	0.15897119	0	2	3	0	3	2	1	3	0101000020E6100000DD36DFCA8A4D5E400B7901C8E2473F40
0	329758.34	3.4621028e+06	0.19249411	0	2	3	0	3	2	1	3	0101000020E6100000987B25E98A4D5E4038D996E2E2473F40
0	329759.03	3.462103e+06	0.22931771	0	2	3	0	3	2	1	3	0101000020E610000005CAE0078B4D5E4089544902E3473F40
0	329759.75	3.4621032e+06	0.26808214	0	2	3	0	3	2	1	3	0101000020E6100000DE5FC3278B4D5E40B28A7327E3473F40
0	329760.28	3.4621035e+06	0.29664445	0	2	3	0	3	2	1	3	0101000020E6100000DE3FD93E8B4D5E4019C56445E3473F40
0	329760.97	3.4621038e+06	0.33475462	0	2	3	0	3	2	1	3	0101000020E6100000E48C2E5D8B4D5E40C1CAB070E3473F40
0	329761.62	3.462104e+06	0.37069649	0	2	3	0	3	2	1	3	0101000020E6100000D36A0B798B4D5E400F8D109DE3473F40
0	329762.25	3.4621042e+06	0.40739888	0	2	3	0	3	2	1	3	0101000020E610000009B4C2948B4D5E40D77A5FCDE3473F40
0	329762.8	3.4621045e+06	0.43989903	0	2	3	0	3	2	1	3	0101000020E6100000219FB3AC8B4D5E404DE1E8FAE3473F40
0	329763.3	3.4621048e+06	0.4702842	0	2	3	0	3	2	1	3	0101000020E610000064988AC28B4D5E40EEE5C027E4473F40
0	329763.78	3.4621052e+06	0.49933672	0	2	3	0	3	2	1	3	0101000020E610000024E8EBD68B4D5E4083589954E4473F40
0	329764.2	3.4621055e+06	0.5261192	0	2	3	0	3	2	1	3	0101000020E6100000783B41E98B4D5E407F55937FE4473F40
0	329764.7	3.4621058e+06	0.55901283	0	2	3	0	3	2	1	3	0101000020E6100000AC63A6FE8B4D5E40318B50B5E4473F40
0	329765.12	3.462106e+06	0.5887357	0	2	3	0	3	2	1	3	0101000020E610000018194A118C4D5E40BD9F73E7E4473F40
0	329765.56	3.4621065e+06	0.62180275	0	2	3	0	3	2	1	3	0101000020E6100000B4DE77258C4D5E4019B96421E5473F40
0	329765.97	3.4621068e+06	0.65120435	0	2	3	0	3	2	1	3	0101000020E61000000A7DE8368C4D5E40D4DDC056E5473F40
0	329766.38	3.4621072e+06	0.6798705	0	2	3	0	3	2	1	3	0101000020E6100000FA19F3478C4D5E401E12C78DE5473F40
0	329766.78	3.4621075e+06	0.7078325	0	2	3	0	3	2	1	3	0101000020E61000007A88585A8C4D5E408A9390CBE5473F40
0	329767.28	3.462108e+06	0.7345442	0	2	3	0	3	2	1	3	0101000020E61000002BB6006F8C4D5E4042DB4D13E6473F40
0	329767.66	3.4621085e+06	0.75389194	0	2	3	0	3	2	1	3	0101000020E610000066A6357F8C4D5E40754A984DE6473F40
0	329768.03	3.4621088e+06	0.77442455	0	2	3	0	3	2	1	3	0101000020E6100000259127908C4D5E4012AAF48CE6473F40
0	329768.4	3.4621092e+06	0.7941263	0	2	3	0	3	2	1	3	0101000020E6100000AF26A0A08C4D5E40CA08D4CCE6473F40
0	329768.8	3.4621098e+06	0.81328034	0	2	3	0	3	2	1	3	0101000020E610000071B4F2B18C4D5E4009831312E7473F40
0	329769.25	3.4621102e+06	0.8329294	0	2	3	0	3	2	1	3	0101000020E6100000766204C48C4D5E403CABF55CE7473F40
0	329769.62	3.4621108e+06	0.85219485	0	2	3	0	3	2	1	3	0101000020E6100000E0BF62D58C4D5E40D95ECBA7E7473F40
0	329770.06	3.4621112e+06	0.8733572	0	2	3	0	3	2	1	3	0101000020E61000008A4F04E88C4D5E409B346BFBE7473F40
0	329770.5	3.4621118e+06	0.8956928	0	2	3	0	3	2	1	3	0101000020E6100000877265FA8C4D5E40CD84FB51E8473F40
0	329770.94	3.4621125e+06	0.9203035	0	2	3	0	3	2	1	3	0101000020E610000024D4F50C8D4D5E4070D33BAEE8473F40
0	329771.22	3.4621128e+06	0.93733805	0	2	3	0	3	2	1	3	0101000020E6100000426664198D4D5E40734DD1EEE8473F40
0	329771.53	3.4621132e+06	0.95567536	0	2	3	0	3	2	1	3	0101000020E61000008DBC6F268D4D5E40CDD93A35E9473F40
0	329771.8	3.4621138e+06	0.9739123	0	2	3	0	3	2	1	3	0101000020E61000002CA00E338D4D5E40671B267CE9473F40
0	329772.12	3.4621142e+06	0.9929066	0	2	3	0	3	2	1	3	0101000020E61000001B23C53F8D4D5E40DB11AEC6E9473F40
0	329772.38	3.4621148e+06	1.0107192	0	2	3	0	3	2	1	3	0101000020E61000001B13504B8D4D5E403A6D430DEA473F40
0	329772.66	3.4621152e+06	1.0307283	0	2	3	0	3	2	1	3	0101000020E6100000CA18A8578D4D5E400450515CEA473F40
0	329772.94	3.4621158e+06	1.0516771	0	2	3	0	3	2	1	3	0101000020E61000006C4136638D4D5E4089AEB1AAEA473F40
0	329773.2	3.4621162e+06	1.0739319	0	2	3	0	3	2	1	3	0101000020E61000009B3A4D6E8D4D5E400E0795FAEA473F40
0	329773.44	3.4621168e+06	1.0962539	0	2	3	0	3	2	1	3	0101000020E610000041A8CA788D4D5E406B7DAE4AEB473F40
0	329773.7	3.4621172e+06	1.1198725	0	2	3	0	3	2	1	3	0101000020E61000009D070D838D4D5E4052A55E9EEB473F40
0	329773.9	3.4621178e+06	1.144539	0	2	3	0	3	2	1	3	0101000020E6100000AFAF878C8D4D5E402171ECF1EB473F40
0	329774.12	3.4621185e+06	1.171841	0	2	3	0	3	2	1	3	0101000020E610000060EDA4958D4D5E40B9DBB549EC473F40
0	329774.3	3.462119e+06	1.1999214	0	2	3	0	3	2	1	3	0101000020E6100000A4E39A9D8D4D5E40B2EECD9EEC473F40
0	329774.5	3.4621195e+06	1.2301216	0	2	3	0	3	2	1	3	0101000020E6100000B2E1B6A48D4D5E40F8808AF4EC473F40
0	329774.66	3.4621202e+06	1.2622726	0	2	3	0	3	2	1	3	0101000020E6100000A6DA23AB8D4D5E40F48EF74CED473F40
0	329774.78	3.4621208e+06	1.2925469	0	2	3	0	3	2	1	3	0101000020E61000007F2555B08D4D5E408C94A29FED473F40
0	329774.88	3.4621212e+06	1.321508	0	2	3	0	3	2	1	3	0101000020E6100000DE007AB48D4D5E403B6586EDED473F40
0	329774.97	3.4621218e+06	1.3559551	0	2	3	0	3	2	1	3	0101000020E6100000916F5EB88D4D5E40DE555548EE473F40
1	329709.5	3.462191e+06	0.011138936	0	0	3	2	3	2	1	3	0101000020E6100000CFE3C043824D5E405C987A9616483F40
1	329710	3.462191e+06	0.007745538	0	0	3	0	3	2	1	3	0101000020E6100000AB8AEA59824D5E40FA91BA9716483F40
1	329710.56	3.462191e+06	0.0026834009	0	0	3	0	3	2	1	3	0101000020E6100000431E7F71824D5E401960699816483F40
1	329711.12	3.462191e+06	-0.004371615	0	0	3	0	3	2	1	3	0101000020E61000003F1D438B824D5E402D514E9816483F40
1	329711.66	3.462191e+06	-0.008568909	0	0	3	0	3	2	1	3	0101000020E61000007C2ECAA1824D5E408CAE309816483F40
1	329712.2	3.462191e+06	-0.011219885	0	0	3	0	3	2	1	3	0101000020E6100000DE21CAB9824D5E408617289816483F40
1	329712.78	3.462191e+06	-0.012565731	0	0	3	0	3	2	1	3	0101000020E6100000587E16D4824D5E408B4A429816483F40
1	329713.44	3.462191e+06	-0.012526582	0	0	3	0	3	2	1	3	0101000020E61000006E8987F0824D5E404A7C9C9816483F40
1	329713.94	3.462191e+06	-0.013693434	0	0	3	0	3	2	1	3	0101000020E6100000E13AA306834D5E40C730989816483F40
1	329714.47	3.462191e+06	-0.014939565	0	0	3	0	3	2	1	3	0101000020E61000004F5CB41E834D5E408323809816483F40
1	329715.03	3.462191e+06	-0.014226806	0	0	3	0	3	2	1	3	0101000020E61000009BED6536834D5E407401C79816483F40
1	329715.56	3.462191e+06	-0.013254701	0	0	3	0	3	2	1	3	0101000020E6100000F7F8154F834D5E402C382A9916483F40
1	329716.2	3.462191e+06	-0.012512447	0	0	3	0	3	2	1	3	0101000020E610000092EBED69834D5E40479B9B9916483F40
1	329716.8	3.462191e+06	-0.011774507	0	0	3	0	3	2	1	3	0101000020E61000001F659985834D5E40172E219A16483F40
1	329717.5	3.462191e+06	-0.010983936	0	0	3	0	3	2	1	3	0101000020E6100000FE845AA3834D5E407CD1C49A16483F40
1	329718.2	3.462191e+06	-0.010188327	0	0	3	0	3	2	1	3	0101000020E6100000AF9563C1834D5E40635C7E9B16483F40
1	329718.9	3.462191e+06	-0.009477808	0	0	3	0	3	2	1	3	0101000020E6100000B24C82E1834D5E404F1A529C16483F40
1	329719.66	3.462191e+06	-0.009489631	0	0	3	0	3	2	1	3	0101000020E610000048C9A302844D5E40A0CC109D16483F40
1	329720.2	3.462191e+06	-0.009854489	0	0	3	0	3	2	1	3	0101000020E6100000D091F219844D5E40BB2F829D16483F40
1	329720.7	3.462191e+06	-0.010252387	0	0	3	0	3	2	1	3	0101000020E6100000162E2A30844D5E407366E59D16483F40
1	329721.25	3.462191e+06	-0.010932903	0	0	3	0	3	2	1	3	0101000020E6100000B6AAE048844D5E40D4023A9E16483F40
1	329721.9	3.462191e+06	-0.0120190075	0	0	3	0	3	2	1	3	0101000020E61000006FEE5465844D5E40B3B7789E16483F40
1	329722.47	3.462191e+06	-0.013018076	0	0	3	0	3	2	1	3	0101000020E6100000CDA2917E844D5E402FA4979E16483F40
1	329723	3.462191e+06	-0.013949188	0	0	3	0	3	2	1	3	0101000020E610000097E21296844D5E401C17A19E16483F40
1	329723.53	3.4621908e+06	-0.014856064	0	0	3	0	3	2	1	3	0101000020E6100000D796FAAC844D5E402FA4979E16483F40
1	329724.03	3.4621908e+06	-0.01576348	0	0	3	0	3	2	1	3	0101000020E61000005BBCE8C3844D5E404F277C9E16483F40
1	329724.56	3.4621908e+06	-0.0166791	0	0	3	0	3	2	1	3	0101000020E610000067CA0FDB844D5E4095C44D9E16483F40
1	329725.1	3.4621908e+06	-0.017618602	0	0	3	0	3	2	1	3	0101000020E610000061C2D5F2844D5E4034C40A9E16483F40
1	329725.7	3.4621908e+06	-0.018648189	0	0	3	0	3	2	1	3	0101000020E61000007323E80C854D5E40248FAA9D16483F40
1	329726.28	3.4621908e+06	-0.019582149	0	0	3	0	3	2	1	3	0101000020E6100000CB401C26854D5E40BEBF3B9D16483F40
1	329726.84	3.4621908e+06	-0.020071827	0	0	3	0	3	2	1	3	0101000020E6100000080E863F854D5E406A19D59C16483F40
1	329727.4	3.4621908e+06	-0.019874882	0	0	3	0	3	2	1	3	0101000020E6100000BF3DD257854D5E4047F38F9C16483F40
1	329727.97	3.4621908e+06	-0.019073566	0	0	3	0	3	2	1	3	0101000020E610000045197771854D5E4048BB6C9C16483F40
1	329728.53	3.4621908e+06	-0.017901484	0	0	3	0	3	2	1	3	0101000020E61000003CCC4D8A854D5E40B2E2719C16483F40
1	329729.12	3.4621908e+06	-0.016379286	0	0	3	0	3	2	1	3	0101000020E610000093E981A3854D5E40EE90A49C16483F40
1	329729.66	3.4621908e+06	-0.014620127	0	0	3	0	3	2	1	3	0101000020E6100000BAF62BBB854D5E40300E039D16483F40
1	329730.2	3.4621908e+06	-0.012377504	0	0	3	0	3	2	1	3	0101000020E610000010BC1AD3854D5E40BA67A59D16483F40
1	329730.72	3.4621908e+06	-0.009863421	0	0	3	0	3	2	1	3	0101000020E6100000414E49EA854D5E408706839E16483F40
1	329731.47	3.4621908e+06	-0.006211543	0	0	3	0	3	2	1	3	0101000020E61000004C45FD0A864D5E406D7818A016483F40
1	329732.16	3.4621908e+06	-0.0032847407	0	0	3	0	3	2	1	3	0101000020E6100000CD9DC128864D5E40C244CDA116483F40
1	329732.75	3.4621908e+06	-0.0013607534	0	0	3	0	3	2	1	3	0101000020E61000002C59FC43864D5E40A75274A316483F40
1	329733.34	3.4621908e+06	-0.0007803042	0	0	3	0	3	2	1	3	0101000020E6100000328CFD5D864D5E407568E7A416483F40
1	329733.88	3.4621908e+06	-0.0011853657	0	0	3	0	3	2	1	3	0101000020E61000007E6EF674864D5E40142A04A616483F40
1	329734.47	3.4621908e+06	-0.0019878845	0	0	3	0	3	2	1	3	0101000020E6100000DA86D08F864D5E40B28732A716483F40
1	329735	3.4621908e+06	-0.0027614338	0	0	3	0	3	2	1	3	0101000020E61000001CE444A7864D5E400EA025A816483F40
1	329735.6	3.4621908e+06	-0.0037910368	0	0	3	0	3	2	1	3	0101000020E61000002BF33DC0864D5E401FB009A916483F40
1	329736.1	3.4621908e+06	-0.0047834176	0	0	3	0	3	2	1	3	0101000020E6100000BD9158D6864D5E40F411BBA916483F40
1	329736.6	3.4621908e+06	-0.0061209095	0	0	3	0	3	2	1	3	0101000020E61000009D846FED864D5E4066AB4CAA16483F40
1	329737.16	3.4621908e+06	-0.0075775017	0	0	3	0	3	2	1	3	0101000020E6100000D9F99305874D5E404234C0AA16483F40
1	329737.72	3.4621908e+06	-0.009105753	0	0	3	0	3	2	1	3	0101000020E610000056EC161E874D5E408AAC15AB16483F40
1	329738.25	3.4621908e+06	-0.010542587	0	0	3	0	3	2	1	3	0101000020E6100000D9173135874D5E40BBC848AB16483F40
1	329738.75	3.4621908e+06	-0.012053343	0	0	3	0	3	2	1	3	0101000020E610000073F0B44B874D5E40F24361AB16483F40
1	329739.28	3.4621908e+06	-0.013513075	0	0	3	0	3	2	1	3	0101000020E6100000737F8363874D5E40E01A59AB16483F40
1	329739.78	3.4621908e+06	-0.014888634	0	0	3	0	3	2	1	3	0101000020E61000009834E879874D5E40ED7435AB16483F40
1	329740.38	3.4621908e+06	-0.016448291	0	0	3	0	3	2	1	3	0101000020E610000091904B93874D5E403B71ECAA16483F40
1	329740.94	3.4621908e+06	-0.017959282	0	0	3	0	3	2	1	3	0101000020E610000073DBA7AB874D5E400C8184AA16483F40
1	329741.47	3.4621908e+06	-0.007837234	0	1	3	0	3	2	1	3	0101000020E610000005D833C3874D5E4038DBB5AC16483F40
1	329742	3.4621908e+06	0.005562277	0	1	3	0	3	2	1	3	0101000020E6100000B86A4BDB874D5E40CFF9CFB016483F40
1	329742.5	3.4621908e+06	0.014621822	0	1	3	0	3	2	1	3	0101000020E610000069F664F1874D5E40FF1CB0B416483F40
1	329743	3.4621908e+06	0.022856845	0	1	3	0	3	2	1	3	0101000020E61000007CE6AF07884D5E406FF511B916483F40
1	329743.56	3.4621908e+06	0.029336851	0	1	3	0	3	2	1	3	0101000020E6100000C877611F884D5E40714ED8BD16483F40
1	329744.1	3.4621908e+06	0.03369484	0	1	3	0	3	2	1	3	0101000020E6100000D4344137884D5E402240AAC216483F40
1	329744.62	3.4621908e+06	0.034054905	0	1	3	0	3	2	1	3	0101000020E6100000D7B7B74E884D5E403DAAC8C616483F40
1	329745.16	3.4621908e+06	0.030896403	0	1	3	0	3	2	1	3	0101000020E6100000E11C5265884D5E4026811ECA16483F40
1	329745.66	3.4621908e+06	0.024671525	0	1	3	0	3	2	1	3	0101000020E610000049F8A17C884D5E40F4E15BCC16483F40
1	329746.22	3.4621908e+06	0.019854398	0	1	3	0	3	2	1	3	0101000020E610000065D10E94884D5E40269B72CE16483F40
1	329746.72	3.4621908e+06	0.018808842	0	1	3	0	3	2	1	3	0101000020E6100000620EC4AA884D5E4074B7FAD016483F40
1	329747.3	3.4621908e+06	0.02215537	0	1	3	0	3	2	1	3	0101000020E6100000795D52C4884D5E40DCB9E1D416483F40
1	329747.84	3.4621908e+06	0.025359683	0	1	3	0	3	2	1	3	0101000020E61000009536BFDB884D5E403193C0D816483F40
1	329748.44	3.462191e+06	0.02769368	0	1	3	0	3	2	1	3	0101000020E6100000EB08D8F5884D5E40A2CF10DD16483F40
1	329749.03	3.462191e+06	0.029099826	0	1	3	0	3	2	1	3	0101000020E6100000D5F13A10894D5E40AE7B64E116483F40
1	329749.56	3.462191e+06	0.027856132	0	1	3	0	3	2	1	3	0101000020E6100000A8BC6C27894D5E4091BBB1E416483F40
1	329750.06	3.462191e+06	0.024718396	0	1	3	0	3	2	1	3	0101000020E6100000A350953D894D5E40042A4AE716483F40
1	329750.7	3.462191e+06	0.021769209	0	1	3	0	3	2	1	3	0101000020E61000000D400D59894D5E406DE166EA16483F40
1	329751.25	3.462191e+06	0.01809671	0	1	3	0	3	2	1	3	0101000020E6100000C4CDCA72894D5E4077F0D6EC16483F40
1	329751.78	3.462191e+06	0.0145813385	0	1	3	0	3	2	1	3	0101000020E6100000E2A00B8A894D5E40F641B6EE16483F40
1	329752.34	3.462191e+06	0.011047325	0	1	3	0	3	2	1	3	0101000020E610000034787EA2894D5E408AE868F016483F40
1	329752.88	3.462191e+06	0.00799694	0	1	3	0	3	2	1	3	0101000020E61000008E2BE9B9894D5E40FA68D6F116483F40
1	329753.4	3.462191e+06	0.0048746495	0	1	3	0	3	2	1	3	0101000020E61000005677C2D1894D5E4028A408F316483F40
1	329753.66	3.462191e+06	0.0032000542	0	1	3	1	3	2	1	3	0101000020E6100000876AADDC894D5E40175684F316483F40
0	329754.2	3.462191e+06	0.0010434262	0	2	3	0	3	2	1	3	0101000020E6100000132178F3894D5E4097F8AAF416483F40
0	329754.72	3.462191e+06	-0.00042506354	0	2	3	0	3	2	1	3	0101000020E61000008193D00A8A4D5E403087ADF516483F40
0	329755.28	3.462191e+06	-0.0019557546	0	2	3	0	3	2	1	3	0101000020E6100000ACEC67238A4D5E40BEE79EF616483F40
0	329755.78	3.462191e+06	-0.003323636	0	2	3	0	3	2	1	3	0101000020E61000003E8B82398A4D5E4028BE5CF716483F40
0	329756.34	3.462191e+06	-0.0047694505	0	2	3	0	3	2	1	3	0101000020E6100000503DDC528A4D5E4021721EF816483F40
0	329756.9	3.462191e+06	-0.005677228	0	2	3	0	3	2	1	3	0101000020E610000033820C6B8A4D5E402121D7F816483F40
0	329757.4	3.462191e+06	-0.0061616404	0	2	3	0	3	2	1	3	0101000020E6100000F8CC13818A4D5E408C5B83F916483F40
0	329757.9	3.462191e+06	-0.0066420035	0	2	3	0	3	2	1	3	0101000020E610000051D7F1978A4D5E4029DE2DFA16483F40
0	329758.47	3.462191e+06	-0.0071484623	0	2	3	0	3	2	1	3	0101000020E610000067C80EB08A4D5E40EB16D7FA16483F40
0	329759.03	3.462191e+06	-0.007672235	0	2	3	0	3	2	1	3	0101000020E6100000EFEECEC88A4D5E405C4C7AFB16483F40
0	329759.62	3.462191e+06	-0.008222221	0	2	3	0	3	2	1	3	0101000020E6100000AFB6F5E28A4D5E40635A18FC16483F40
0	329760.25	3.462191e+06	-0.008793906	0	2	3	0	3	2	1	3	0101000020E6100000CE9D5EFE8A4D5E40C266B3FC16483F40
0	329760.9	3.462191e+06	-0.00939409	0	2	3	0	3	2	1	3	0101000020E61000003D82501B8B4D5E40E89347FD16483F40
0	329761.56	3.462191e+06	-0.0099498695	0	2	3	0	3	2	1	3	0101000020E610000067FB67388B4D5E405496D0FD16483F40
0	329762.25	3.462191e+06	-0.00987707	0	2	3	0	3	2	1	3	0101000020E6100000DF7734578B4D5E40B93974FE16483F40
0	329762.94	3.4621908e+06	-0.008718997	0	2	3	0	3	2	1	3	0101000020E6100000865400758B4D5E40532C65FF16483F40
0	329763.62	3.4621908e+06	-0.006831766	0	2	3	0	3	2	1	3	0101000020E610000000CBA0938B4D5E402905AC0017483F40
0	329764.38	3.4621908e+06	-0.004397152	0	2	3	0	3	2	1	3	0101000020E6100000134D05B48B4D5E40D6334C0217483F40
0	329765.1	3.4621908e+06	-0.0007225577	0	2	3	0	3	2	1	3	0101000020E61000001216A8D48B4D5E406BB5820417483F40
0	329765.62	3.462191e+06	0.0020490359	0	2	3	0	3	2	1	3	0101000020E61000005E49E8EA8B4D5E40EB6A500617483F40
0	329766.12	3.462191e+06	0.0049087373	0	2	3	0	3	2	1	3	0101000020E61000001EA673018C4D5E40F2D65F0817483F40
0	329766.62	3.462191e+06	0.007606521	0	2	3	0	3	2	1	3	0101000020E61000004D8F15188C4D5E4036F1A10A17483F40
0	329767.16	3.462191e+06	0.010012421	0	2	3	0	3	2	1	3	0101000020E610000071F95E2F8C4D5E401B4A130D17483F40
0	329767.7	3.462191e+06	0.012125905	0	2	3	0	3	2	1	3	0101000020E6100000600E2F468C4D5E40FF3E960F17483F40
0	329768.22	3.462191e+06	0.014124875	0	2	3	0	3	2	1	3	0101000020E6100000E6DCA95D8C4D5E40E1074E1217483F40
0	329768.78	3.462191e+06	0.015599198	0	2	3	0	3	2	1	3	0101000020E61000003AAEF0758C4D5E407105231517483F40
0	329769.28	3.462191e+06	0.01631559	0	2	3	0	3	2	1	3	0101000020E6100000597B058D8C4D5E402F7CCA1717483F40
0	329769.84	3.462191e+06	0.016304936	0	2	3	0	3	2	1	3	0101000020E610000078F7D2A48C4D5E40CD376A1A17483F40
0	329770.38	3.462191e+06	0.015275891	0	2	3	0	3	2	1	3	0101000020E6100000E812B8BC8C4D5E4003F8CF1C17483F40
0	329770.9	3.462191e+06	0.012915798	0	2	3	0	3	2	1	3	0101000020E6100000853D55D48C4D5E406F58CA1E17483F40
0	329771.47	3.462191e+06	0.010010822	0	2	3	0	3	2	1	3	0101000020E6100000260B53EC8C4D5E40BD29882017483F40
0	329772.03	3.462191e+06	0.0070520504	0	2	3	0	3	2	1	3	0101000020E6100000BFF62C058D4D5E40E2751B2217483F40
0	329772.6	3.462191e+06	0.004442685	0	2	3	0	3	2	1	3	0101000020E6100000166BD41D8D4D5E404C5F802317483F40
0	329773.12	3.462191e+06	0.00236755	0	2	3	0	3	2	1	3	0101000020E6100000D42BFD358D4D5E4086C8C32417483F40
0	329773.72	3.462191e+06	0.000962999	0	2	3	0	3	2	1	3	0101000020E610000078A985508D4D5E40F1B1282617483F40
0	329774.28	3.462191e+06	0.00081759045	0	2	3	0	3	2	1	3	0101000020E610000064281F698D4D5E40BFC79B2717483F40
0	329774.84	3.462191e+06	0.0017997642	0	2	3	0	3	2	1	3	0101000020E6100000121E02818D4D5E4008663F2917483F40
0	329775.4	3.462191e+06	0.003602724	0	2	3	0	3	2	1	3	0101000020E610000026CA2F9A8D4D5E40F17A352B17483F40
0	329775.97	3.462191e+06	0.0055926368	0	2	3	0	3	2	1	3	0101000020E6100000B09350B38D4D5E40AC16592D17483F40
0	329776.53	3.462191e+06	0.007541006	0	2	3	0	3	2	1	3	0101000020E61000002F80A7CB8D4D5E407977962F17483F40
0	329777.12	3.462191e+06	0.009693908	0	2	3	0	3	2	1	3	0101000020E6100000084D7DE68D4D5E4011383F3217483F40
0	329777.72	3.462191e+06	0.011687016	0	2	3	0	3	2	1	3	0101000020E6100000D2EA6FFF8D4D5E401A1BE43417483F40
0	329778.3	3.462191e+06	0.013579991	0	2	3	0	3	2	1	3	0101000020E61000005F06AA198E4D5E4052B6CD3717483F40
0	329778.88	3.462191e+06	0.01502609	0	2	3	0	3	2	1	3	0101000020E6100000DF9B8D328E4D5E40E14FB43A17483F40
0	329779.4	3.462191e+06	0.016113723	0	2	3	0	3	2	1	3	0101000020E6100000A1A1A5498E4D5E407F0B543D17483F40
0	329780.1	3.462191e+06	0.017622672	0	2	3	0	3	2	1	3	0101000020E6100000133622698E4D5E406BBD2D4117483F40
0	329780.78	3.462191e+06	0.019054916	0	2	3	0	3	2	1	3	0101000020E6100000D0C583868E4D5E403718EE4417483F40
0	329781.4	3.462191e+06	0.020395553	0	2	3	0	3	2	1	3	0101000020E610000074F2C4A18E4D5E40F9A88B4817483F40
0	329781.94	3.462191e+06	0.02153432	0	2	3	0	3	2	1	3	0101000020E6100000DE255ABA8E4D5E400836E04B17483F40
0	329782.5	3.462191e+06	0.022271486	0	2	3	0	3	2	1	3	0101000020E61000006DDDF6D28E4D5E400A31354F17483F40
0	329783.1	3.462191e+06	0.022963373	0	2	3	0	3	2	1	3	0101000020E61000005323F9EC8E4D5E4023C0CF5217483F40
0	329783.72	3.462191e+06	0.023692546	0	2	3	0	3	2	1	3	0101000020E6100000352A38088F4D5E4059DEA65617483F40
0	329784.28	3.4621912e+06	0.024339164	0	2	3	0	3	2	1	3	0101000020E610000007AA4E208F4D5E40D7C51A5A17483F40
5	329784.6	3.4621912e+06	0.024693362	0	2	3	2	3	2	1	3	0101000020E6100000BBA4202E8F4D5E4094291B5C17483F40
5	329785.1	3.4621912e+06	0.024547422	0	0	3	0	3	2	1	3	0101000020E6100000C3665A448F4D5E4039E0B15E17483F40
5	329785.66	3.4621912e+06	0.022278938	0	0	3	0	3	2	1	3	0101000020E6100000C44D6E5D8F4D5E40DBA2A06117483F40
5	329786.28	3.4621912e+06	0.01929513	0	0	3	0	3	2	1	3	0101000020E610000043A560798F4D5E40A21B8E6417483F40
5	329786.88	3.4621912e+06	0.0165084	0	0	3	0	3	2	1	3	0101000020E61000002E8897938F4D5E4004C50C6717483F40
5	329787.53	3.4621912e+06	0.013538444	0	0	3	0	3	2	1	3	0101000020E61000009FB7A4AF8F4D5E402161736917483F40
5	329788.03	3.4621912e+06	0.011094197	0	0	3	0	3	2	1	3	0101000020E6100000D143A7C68F4D5E40F27D356B17483F40
5	329788.6	3.4621912e+06	0.008522115	0	0	3	0	3	2	1	3	0101000020E6100000415F8CDE8F4D5E40EA18D36C17483F40
5	329789.22	3.4621912e+06	0.0054802517	0	0	3	0	3	2	1	3	0101000020E61000000E5CC2FA8F4D5E404049766E17483F40
5	329789.75	3.4621912e+06	0.0029369344	0	0	3	0	3	2	1	3	0101000020E6100000816B4F12904D5E400B589A6F17483F40
5	329790.28	3.4621912e+06	0.00044101945	0	0	3	0	3	2	1	3	0101000020E6100000A0386429904D5E4060D9847017483F40
5	329790.78	3.4621912e+06	-0.0019228096	0	0	3	0	3	2	1	3	0101000020E6100000AF8B7A3F904D5E401C17377117483F40
5	329791.28	3.4621912e+06	-0.0041264403	0	0	3	0	3	2	1	3	0101000020E6100000578CE355904D5E409A42C87117483F40
5	329791.94	3.4621912e+06	-0.006845599	0	0	3	0	3	2	1	3	0101000020E6100000BB42C472904D5E40CE654A7217483F40
5	329792.6	3.4621912e+06	-0.008730148	0	0	3	0	3	2	1	3	0101000020E6100000121CDB8E904D5E405AEBB77217483F40
5	329793.25	3.4621912e+06	-0.009328051	0	0	3	0	3	2	1	3	0101000020E61000005B82F1AB904D5E404ED04D7317483F40
5	329793.75	3.4621912e+06	-0.009616929	0	0	3	0	3	2	1	3	0101000020E61000000C0E0BC2904D5E406A33BF7317483F40
5	329794.44	3.4621912e+06	-0.0100050485	0	0	3	0	3	2	1	3	0101000020E610000048FBF4DF904D5E40E85E507417483F40
5	329794.94	3.4621912e+06	-0.010301028	0	0	3	0	3	2	1	3	0101000020E6100000433ED6F6904D5E40FE2AB97417483F40
5	329795.5	3.462191e+06	-0.010611405	0	0	3	0	3	2	1	3	0101000020E610000000052B0F914D5E4013F7217517483F40
5	329796.06	3.462191e+06	-0.009909079	0	0	3	0	3	2	1	3	0101000020E6100000F1714027914D5E405F76C67517483F40
5	329796.66	3.462191e+06	-0.0071469108	0	0	3	0	3	2	1	3	0101000020E610000003820B42914D5E401594057717483F40
5	329797.25	3.462191e+06	-0.004155108	0	0	3	0	3	2	1	3	0101000020E610000006C1645C914D5E40B1FD8B7817483F40
5	329797.88	3.462191e+06	-0.0010434535	0	0	3	0	3	2	1	3	0101000020E6100000E7CDCF77914D5E40172B6C7A17483F40
5	329798.53	3.462191e+06	0.0021369203	0	0	3	0	3	2	1	3	0101000020E61000007D81E493914D5E406ED2A47C17483F40
5	329799.16	3.4621912e+06	0.0053433664	0	0	3	0	3	2	1	3	0101000020E6100000875E44B0914D5E401984327F17483F40
5	329799.84	3.4621912e+06	0.008362327	0	0	3	0	3	2	1	3	0101000020E610000062E1D0CD914D5E409C8B198217483F40
5	329800.5	3.4621912e+06	0.010043312	0	0	3	0	3	2	1	3	0101000020E610000025083BEB914D5E406468F58417483F40
5	329801.2	3.4621912e+06	0.008828378	0	0	3	0	3	2	1	3	0101000020E61000003D1AAA09924D5E40C0DE598717483F40
5	329801.9	3.4621912e+06	0.0049950243	0	0	3	0	3	2	1	3	0101000020E6100000E303A029924D5E409892248917483F40
5	329802.4	3.4621912e+06	0.0022828614	0	0	3	0	3	2	1	3	0101000020E6100000104AE13F924D5E403EB3268A17483F40
5	329803.16	3.4621912e+06	-0.0008454544	0	0	3	0	3	2	1	3	0101000020E6100000ACFF995F924D5E4084AE698B17483F40
5	329803.84	3.4621912e+06	-0.0029597923	0	0	3	0	3	2	1	3	0101000020E610000084ECEF7E924D5E4035998E8C17483F40
5	329804.62	3.4621912e+06	-0.003575078	0	0	3	0	3	2	1	3	0101000020E6100000DA943FA0924D5E401D37EF8D17483F40
5	329805.38	3.4621912e+06	-0.0014506743	0	0	3	0	3	2	1	3	0101000020E6100000ADF18AC1924D5E40574FEB8F17483F40
5	329805.88	3.4621912e+06	0.0008617988	0	0	3	0	3	2	1	3	0101000020E61000003F3F5ED8924D5E40CB3A969117483F40
5	329806.4	3.4621912e+06	0.0032522525	0	0	3	0	3	2	1	3	0101000020E61000003F1F74EF924D5E403EFA759317483F40
5	329806.9	3.4621912e+06	0.005570799	0	0	3	0	3	2	1	3	0101000020E610000064D4D805934D5E40E239779517483F40
5	329807.47	3.4621912e+06	0.0079671135	0	0	3	0	3	2	1	3	0101000020E61000008B903B1E934D5E408F17D09717483F40
5	329808	3.4621912e+06	0.009945308	0	0	3	0	3	2	1	3	0101000020E61000009EDEF735934D5E40D269359A17483F40
5	329808.53	3.4621912e+06	0.011503256	0	0	3	0	3	2	1	3	0101000020E61000003FFD3C4D934D5E408B759F9C17483F40
5	329809.1	3.4621912e+06	0.012438561	0	0	3	0	3	2	1	3	0101000020E6100000E904A465934D5E40C7681F9F17483F40
5	329809.62	3.4621912e+06	0.01217544	0	0	3	0	3	2	1	3	0101000020E6100000B8E1597D934D5E402AA257A117483F40
5	329810.2	3.4621912e+06	0.010254766	0	0	3	0	3	2	1	3	0101000020E610000012447D95934D5E40CFA935A317483F40
5	329810.72	3.4621912e+06	0.0077066547	0	0	3	0	3	2	1	3	0101000020E61000001B0DB5AD934D5E40896AD5A417483F40
5	329811.28	3.4621912e+06	0.005116464	0	0	3	0	3	2	1	3	0101000020E6100000DBC7B1C5934D5E40F3533AA617483F40
5	329811.8	3.4621912e+06	0.0027025756	0	0	3	0	3	2	1	3	0101000020E610000030E413DD934D5E4092B168A717483F40
5	329812.38	3.4621912e+06	0.0004048005	0	0	3	0	3	2	1	3	0101000020E61000000B4D4CF6934D5E40B4BE89A817483F40
5	329812.9	3.4621912e+06	-0.0014480072	0	0	3	0	3	2	1	3	0101000020E6100000717F0F0D944D5E40C5CE6DA917483F40
5	329813.44	3.4621912e+06	-0.0033041309	0	0	3	0	3	2	1	3	0101000020E610000088702C25944D5E4054F73BAA17483F40
5	329814	3.4621912e+06	-0.0051587117	0	0	3	0	3	2	1	3	0101000020E6100000F497693D944D5E404878E3AA17483F40
5	329814.53	3.4621912e+06	-0.007007524	0	0	3	0	3	2	1	3	0101000020E6100000BAE96E55944D5E40E02B62AB17483F40
5	329815.28	3.4621912e+06	-0.009532582	0	0	3	0	3	2	1	3	0101000020E6100000438FF275944D5E409EF9CDAB17483F40
5	329815.97	3.4621912e+06	-0.011904222	0	0	3	0	3	2	1	3	0101000020E6100000FED33894944D5E40E79DEEAB17483F40
5	329816.6	3.4621912e+06	-0.013921084	0	0	3	0	3	2	1	3	0101000020E6100000FB88B3B0944D5E408471E0AB17483F40
5	329817.2	3.4621912e+06	-0.014502804	0	0	3	0	3	2	1	3	0101000020E6100000B41045CA944D5E402C0FF5AB17483F40
5	329817.72	3.4621912e+06	-0.014346531	0	0	3	0	3	2	1	3	0101000020E6100000EEDCDCE1944D5E406FB81EAC17483F40
5	329818.22	3.462191e+06	-0.014152408	0	0	3	0	3	2	1	3	0101000020E6100000BD5B21F8944D5E405B634BAC17483F40
5	329818.53	3.462191e+06	-0.014041828	0	0	3	0	3	2	1	3	0101000020E6100000EF550A05954D5E40477266AC17483F40
5	329819.06	3.462191e+06	-0.009292578	0	1	3	0	3	2	1	3	0101000020E61000008A8CFF1C954D5E40F5553CAD17483F40
5	329819.56	3.462191e+06	-0.0064258394	0	1	3	0	3	2	1	3	0101000020E6100000DA5C7433954D5E40100478AE17483F40
5	329820.1	3.462191e+06	-0.0035135392	0	1	3	0	3	2	1	3	0101000020E61000003BF82E4A954D5E404841F0AF17483F40
5	329820.66	3.462191e+06	-0.0003515233	0	1	3	0	3	2	1	3	0101000020E61000003B36B662954D5E408FB3C8B117483F40
5	329821.16	3.462191e+06	0.0025118939	0	1	3	0	3	2	1	3	0101000020E6100000488FF878954D5E40080AB1B317483F40
5	329821.72	3.4621912e+06	0.005747635	0	1	3	0	3	2	1	3	0101000020E6100000F6E24C92954D5E4038CF1FB617483F40
5	329822.28	3.4621912e+06	0.00890508	0	1	3	0	3	2	1	3	0101000020E610000070E127AB954D5E4002D8C6B817483F40
5	329822.8	3.4621912e+06	0.011874501	0	1	3	0	3	2	1	3	0101000020E6100000358AA0C2954D5E40035C86BB17483F40
5	329823.44	3.4621912e+06	0.01523062	0	1	3	0	3	2	1	3	0101000020E6100000CBDF43DD954D5E401185ECBE17483F40
5	329824.06	3.4621912e+06	0.018694863	0	1	3	0	3	2	1	3	0101000020E61000006C18DDF8954D5E403B11C4C217483F40
5	329824.6	3.4621912e+06	0.02153166	0	1	3	0	3	2	1	3	0101000020E6100000219F9C10964D5E40864C4BC617483F40
5	329825.16	3.4621912e+06	0.022392387	0	1	3	0	3	2	1	3	0101000020E6100000B3F99929964D5E40D9E6B7C917483F40
5	329825.75	3.4621912e+06	0.018137192	0	1	3	0	3	2	1	3	0101000020E6100000F8069943964D5E4073371ACC17483F40
5	329826.38	3.4621912e+06	0.012785835	0	1	3	0	3	2	1	3	0101000020E61000008F05C95E964D5E4076D4FDCD17483F40
5	329826.97	3.4621912e+06	0.008891762	0	1	3	0	3	2	1	3	0101000020E6100000BC655E79964D5E40229FAFCF17483F40
5	329827.6	3.4621912e+06	0.005281048	0	1	3	0	3	2	1	3	0101000020E61000002A435294964D5E40D1F91AD117483F40
5	329828.2	3.4621912e+06	0.0026557043	0	1	3	0	3	2	1	3	0101000020E6100000BDA44DAF964D5E40E54871D217483F40
5	329828.8	3.4621912e+06	0.0003131877	0	1	3	0	3	2	1	3	0101000020E6100000050466CA964D5E40E7369CD317483F40
5	329829.4	3.4621912e+06	-0.0004518668	0	1	3	0	3	2	1	3	0101000020E610000017B6BFE3964D5E40799EDCD417483F40
5	329830	3.4621912e+06	0.0010855043	0	1	3	0	3	2	1	3	0101000020E610000065F7FBFD964D5E405DE4A6D617483F40
5	329830.6	3.4621912e+06	0.0030514498	0	1	3	0	3	2	1	3	0101000020E6100000F40C0A18974D5E40B5B7AAD817483F40
5	329831.16	3.4621912e+06	0.0049673277	0	1	3	0	3	2	1	3	0101000020E6100000197E5131974D5E40BCBFCBDA17483F40
5	329831.66	3.4621912e+06	0.0066706855	0	1	3	0	3	2	1	3	0101000020E610000090CCA147974D5E40D6B8D1DC17483F40
5	329832.25	3.4621912e+06	0.008617457	0	1	3	0	3	2	1	3	0101000020E610000064A4FD60974D5E40253948DF17483F40
5	329832.75	3.4621912e+06	0.010331998	0	1	3	0	3	2	1	3	0101000020E61000008A533677974D5E4017EC95E117483F40
5	329833.25	3.4621912e+06	0.01213123	0	1	3	0	3	2	1	3	0101000020E6100000CA071E8E974D5E40E2BC19E417483F40
5	329833.28	3.4621912e+06	0.012198874	0	1	3	1	3	2	1	3	0101000020E61000007EB4F38E974D5E400DA632E417483F40
0	329833.8	3.4621912e+06	0.0074041192	0	2	3	0	3	2	1	3	0101000020E61000003EC037A6974D5E4086B150E517483F40
0	329834.34	3.4621912e+06	0.001071656	0	2	3	0	3	2	1	3	0101000020E6100000E18709BE974D5E40BDC87AE517483F40
0	329834.84	3.4621912e+06	-0.008478053	0	2	3	0	3	2	1	3	0101000020E61000002CBB49D4974D5E40637864E417483F40
0	329835.44	3.4621912e+06	-0.022265015	0	2	3	0	3	2	1	3	0101000020E61000001AE36FED974D5E4081139BE117483F40
0	329835.97	3.4621912e+06	-0.040933188	0	2	3	0	3	2	1	3	0101000020E61000002B8ECB04984D5E40EBD581DC17483F40
0	329836.5	3.462191e+06	-0.06755837	0	2	3	0	3	2	1	3	0101000020E610000071372D1D984D5E40B4C2D2D317483F40
0	329837.06	3.462191e+06	-0.10595062	0	2	3	0	3	2	1	3	0101000020E6100000D1DF1136984D5E404533DBC517483F40
0	329837.56	3.4621908e+06	-0.14751337	0	2	3	0	3	2	1	3	0101000020E61000005A44C34B984D5E40294FF5B417483F40
0	329838.06	3.4621908e+06	-0.19506177	0	2	3	0	3	2	1	3	0101000020E61000009C43C661984D5E404950359F17483F40
0	329838.56	3.4621905e+06	-0.24897416	0	2	3	0	3	2	1	3	0101000020E6100000C8890778984D5E4080D1E38317483F40
0	329839.06	3.4621902e+06	-0.30652463	0	2	3	0	3	2	1	3	0101000020E6100000C2235C8E984D5E401ADB0F6317483F40
0	329839.56	3.46219e+06	-0.36670324	0	2	3	0	3	2	1	3	0101000020E6100000A2BE2DA4984D5E40B37F1D3D17483F40
0	329840.03	3.4621898e+06	-0.42659327	0	2	3	0	3	2	1	3	0101000020E610000064B1EFB8984D5E40A04C4F1317483F40
0	329840.47	3.4621895e+06	-0.4898632	0	2	3	0	3	2	1	3	0101000020E6100000511635CD984D5E40E373EDE316483F40
0	329840.9	3.462189e+06	-0.5556178	0	2	3	0	3	2	1	3	0101000020E6100000ACAF4BE0984D5E40D6450EB016483F40
0	329841.3	3.4621888e+06	-0.6247014	0	2	3	0	3	2	1	3	0101000020E61000003E3412F2984D5E40D1249D7716483F40
0	329841.66	3.4621882e+06	-0.6910731	0	2	3	0	3	2	1	3	0101000020E6100000015EC701994D5E40FB31343E16483F40
0	329841.97	3.462188e+06	-0.7585422	0	2	3	0	3	2	1	3	0101000020E610000081619110994D5E407059510016483F40
0	329842.28	3.4621875e+06	-0.82475907	0	2	3	0	3	2	1	3	0101000020E61000005837DE1D994D5E40871662C015483F40
0	329842.56	3.462187e+06	-0.89396137	0	2	3	0	3	2	1	3	0101000020E61000000543622A994D5E402E9D9F7A15483F40
0	329842.78	3.4621865e+06	-0.96331406	0	2	3	0	3	2	1	3	0101000020E61000003EC72935994D5E408ABD5C3315483F40
0	329843	3.462186e+06	-1.0359434	0	2	3	0	3	2	1	3	0101000020E6100000E0E28D3E994D5E4082CFDAE714483F40
0	329843.16	3.4621855e+06	-1.1075964	0	2	3	0	3	2	1	3	0101000020E61000007FFD1F46994D5E408ACBEE9B14483F40
0	329843.28	3.462185e+06	-1.1811658	0	2	3	0	3	2	1	3	0101000020E61000005E8E124C994D5E404B0A604D14483F40
0	329843.38	3.4621845e+06	-1.2556746	0	2	3	0	3	2	1	3	0101000020E6100000BD693750994D5E40AA99D5FD13483F40
0	329843.4	3.462184e+06	-1.3371675	0	2	3	0	3	2	1	3	0101000020E61000006DD74952994D5E40484766AB13483F40
0	329843.4	3.4621835e+06	-1.417305	0	2	3	0	3	2	1	3	0101000020E6100000110A2152994D5E4025ABA85E13483F40
0	329843.34	3.462183e+06	-1.5047908	0	2	3	0	3	2	1	3	0101000020E6100000C6F4E74F994D5E406F031A0B13483F40
0	329843.25	3.4621822e+06	-1.5868853	0	2	3	0	3	2	1	3	0101000020E6100000A4F9EC4B994D5E4085E144B912483F40
6	329843.16	3.462182e+06	-1.6357403	0	2	3	2	3	2	1	3	0101000020E610000018B89C48994D5E4053542B8312483F40
6	329843.12	3.4621815e+06	-1.6249288	0	0	3	0	3	2	1	3	0101000020E6100000F0903448994D5E40049CDC3212483F40
6	329843.12	3.462181e+06	-1.6133285	0	0	3	0	3	2	1	3	0101000020E610000029D42948994D5E403DFEBDE211483F40
6	329843.12	3.4621805e+06	-1.6015191	0	0	3	0	3	2	1	3	0101000020E610000091CF6348994D5E40E394709111483F40
6	329843.12	3.46218e+06	-1.5904652	0	0	3	0	3	2	1	3	0101000020E610000023ECD948994D5E401B9A104511483F40
6	329843.12	3.4621792e+06	-1.5783043	0	0	3	0	3	2	1	3	0101000020E61000000FDCA449994D5E40838DB8EE10483F40
6	329843.16	3.4621788e+06	-1.5669863	0	0	3	0	3	2	1	3	0101000020E61000000C97B54A994D5E401694429110483F40
6	329843.16	3.462178e+06	-1.5582956	0	0	3	0	3	2	1	3	0101000020E6100000DD3CE24B994D5E40C0CA233410483F40
6	329843.2	3.4621775e+06	-1.552994	0	0	3	0	3	2	1	3	0101000020E610000038BFEF4C994D5E40B005F7E20F483F40
6	329843.2	3.462177e+06	-1.5498633	0	0	3	0	3	2	1	3	0101000020E610000079EB064E994D5E40391E128B0F483F40
6	329843.22	3.4621765e+06	-1.5508907	0	0	3	0	3	2	1	3	0101000020E6100000B0E3E04E994D5E40BE3C08350F483F40
6	329843.22	3.4621758e+06	-1.555186	0	0	3	0	3	2	1	3	0101000020E61000007FE0804F994D5E40D1B442DC0E483F40
6	329843.22	3.4621752e+06	-1.5593088	0	0	3	0	3	2	1	3	0101000020E61000004FD7F44F994D5E40C7D1E88B0E483F40
6	329843.22	3.4621748e+06	-1.5634922	0	0	3	0	3	2	1	3	0101000020E61000003FBB6750994D5E409EE58A2E0E483F40
6	329843.22	3.462174e+06	-1.5674428	0	0	3	0	3	2	1	3	0101000020E61000000384CA50994D5E40CA8BBCCB0D483F40
6	329843.22	3.4621735e+06	-1.5699569	0	0	3	0	3	2	1	3	0101000020E610000072160D51994D5E4096A17A7F0D483F40
6	329843.22	3.462173e+06	-1.5723157	0	0	3	0	3	2	1	3	0101000020E610000038D94351994D5E40A8F01B330D483F40
6	329843.22	3.4621725e+06	-1.5744754	0	0	3	0	3	2	1	3	0101000020E610000036DF6F51994D5E40D363BCE60C483F40
6	329843.2	3.462172e+06	-1.5766588	0	0	3	0	3	2	1	3	0101000020E61000006D289151994D5E40F412B7980C483F40
6	329843.2	3.4621715e+06	-1.5788552	0	0	3	0	3	2	1	3	0101000020E6100000FCA1A651994D5E4083092A4A0C483F40
6	329843.2	3.462171e+06	-1.5812855	0	0	3	0	3	2	1	3	0101000020E61000000239AF51994D5E401C3229F30B483F40
6	329843.2	3.4621702e+06	-1.5835093	0	0	3	0	3	2	1	3	0101000020E6100000BDC7A851994D5E4062DB3FA20B483F40
6	329843.16	3.4621698e+06	-1.585611	0	0	3	0	3	2	1	3	0101000020E610000092AC9851994D5E4099FEAE510B483F40
6	329843.16	3.4621692e+06	-1.5877492	0	0	3	0	3	2	1	3	0101000020E61000001D897951994D5E405E81DFFC0A483F40
6	329843.16	3.4621688e+06	-1.5898379	0	0	3	0	3	2	1	3	0101000020E6100000E0A84F51994D5E40A41DCCA90A483F40
6	329843.12	3.4621682e+06	-1.592035	0	0	3	0	3	2	1	3	0101000020E6100000979A1451994D5E40542B58520A483F40
6	329843.12	3.4621675e+06	-1.594203	0	0	3	0	3	2	1	3	0101000020E6100000C5A9CC50994D5E40E86B07FC09483F40
6	329843.1	3.462167e+06	-1.5963285	0	0	3	0	3	2	1	3	0101000020E61000004BE97850994D5E401C4957A709483F40
6	329843.06	3.4621665e+06	-1.59851	0	0	3	0	3	2	1	3	0101000020E6100000A60D1550994D5E404CE6625009483F40
6	329843.06	3.4621658e+06	-1.6008497	0	0	3	0	3	2	1	3	0101000020E6100000B092994F994D5E40B2D708F308483F40
6	329843.03	3.4621652e+06	-1.6030327	0	0	3	0	3	2	1	3	0101000020E6100000D46D144F994D5E40DDE9059908483F40
6	329843	3.4621645e+06	-1.6039112	0	0	3	0	3	2	1	3	0101000020E6100000A0189B4E994D5E402BEA243F08483F40
6	329843	3.462164e+06	-1.6014917	0	0	3	0	3	2	1	3	0101000020E61000003286584E994D5E408A7D15E707483F40
6	329843	3.4621635e+06	-1.5960455	0	0	3	0	3	2	1	3	0101000020E6100000D4BE5B4E994D5E4060F6D58E07483F40
6	329843	3.4621628e+06	-1.5886408	0	0	3	0	3	2	1	3	0101000020E6100000A6AFA34E994D5E4007660A3707483F40
6	329843	3.4621622e+06	-1.5809015	0	0	3	0	3	2	1	3	0101000020E61000005D50214F994D5E40040C5FDF06483F40
6	329843	3.4621618e+06	-1.5745121	0	0	3	0	3	2	1	3	0101000020E610000045A3B74F994D5E40CEECA28806483F40
6	329843	3.4621612e+06	-1.569208	0	0	3	0	3	2	1	3	0101000020E61000003FBB6750994D5E400540582F06483F40
6	329843	3.4621605e+06	-1.5649545	0	0	3	0	3	2	1	3	0101000020E6100000A3C82551994D5E40C41E56D805483F40
6	329843	3.46216e+06	-1.5617898	0	0	3	0	3	2	1	3	0101000020E61000008FB8F051994D5E401370BC7D05483F40
6	329843.03	3.4621592e+06	-1.5596969	0	0	3	0	3	2	1	3	0101000020E61000009B95BA52994D5E405248A52505483F40
6	329843.03	3.4621588e+06	-1.5591184	0	0	3	0	3	2	1	3	0101000020E6100000C0C87A53994D5E4065626ECB04483F40
6	329843.03	3.4621582e+06	-1.560496	0	0	3	0	3	2	1	3	0101000020E610000051EB1C54994D5E402D61817004483F40
6	329843.03	3.4621575e+06	-1.5624565	0	0	3	0	3	2	1	3	0101000020E61000009705B054994D5E40F601231404483F40
6	329843.03	3.462157e+06	-1.5642291	0	0	3	0	3	2	1	3	0101000020E610000079C13D55994D5E4031AC3EB503483F40
6	329843.03	3.4621562e+06	-1.5643008	0	0	3	0	3	2	1	3	0101000020E6100000E45FD855994D5E401158BB5903483F40
6	329843.06	3.4621558e+06	-1.5617694	0	0	3	0	3	2	1	3	0101000020E61000008CDE9C56994D5E408C03530103483F40
6	329843.06	3.4621552e+06	-1.5578374	0	0	3	0	3	2	1	3	0101000020E6100000723D8B57994D5E40FA8656A702483F40
6	329843.06	3.4621545e+06	-1.5539814	0	0	3	0	3	2	1	3	0101000020E61000002A879558994D5E406BD8B34B02483F40
6	329843.1	3.462154e+06	-1.5505658	0	0	3	0	3	2	1	3	0101000020E61000008BA0AB59994D5E40D17FBFF201483F40
6	329843.1	3.4621532e+06	-1.5475588	0	0	3	0	3	2	1	3	0101000020E61000007A33D75A994D5E40D8BCBE9701483F40
6	329843.12	3.4621528e+06	-1.5451677	0	0	3	0	3	2	1	3	0101000020E610000051700C5C994D5E40E758A33C01483F40
6	329843.16	3.4621522e+06	-1.5440826	0	0	3	0	3	2	1	3	0101000020E61000002116395D994D5E40C94392E100483F40
6	329843.16	3.4621515e+06	-1.545353	0	0	3	0	3	2	1	3	0101000020E61000004355515E994D5E40F2B1FE8100483F40
6	329843.16	3.462151e+06	-1.5483915	0	0	3	0	3	2	1	3	0101000020E6100000867B3C5F994D5E40228C7A2400483F40
6	329843.2	3.4621502e+06	-1.5522819	0	0	3	0	3	2	1	3	0101000020E610000035910960994D5E404CC2C3C4FF473F40
6	329843.2	3.4621498e+06	-1.5563134	0	0	3	0	3	2	1	3	0101000020E6100000BF1CA360994D5E400FF9576DFF473F40
6	329843.2	3.4621492e+06	-1.5603099	0	0	3	0	3	2	1	3	0101000020E6100000B5971E61994D5E40F26EB318FF473F40
6	329843.2	3.4621485e+06	-1.5638098	0	0	3	0	3	2	1	3	0101000020E610000015027C61994D5E40A72ECCCAFE473F40
6	329843.2	3.462148e+06	-1.5677654	0	0	3	0	3	2	1	3	0101000020E6100000577FDA61994D5E40AC188269FE473F40
6	329843.2	3.4621472e+06	-1.5714475	0	0	3	0	3	2	1	3	0101000020E610000042C61862994D5E4088087A0EFE473F40
6	329843.16	3.4621468e+06	-1.5748469	0	0	3	0	3	2	1	3	0101000020E61000003A353C62994D5E4006D5B4BAFD473F40
6	329843.16	3.4621462e+06	-1.5781299	0	0	3	0	3	2	1	3	0101000020E6100000C3174962994D5E4005E1F969FD473F40
6	329843.16	3.4621458e+06	-1.5812769	0	0	3	0	3	2	1	3	0101000020E610000060B94362994D5E403742671BFD473F40
6	329843.16	3.4621452e+06	-1.5841277	0	0	3	0	3	2	1	3	0101000020E610000092653062994D5E403416E7CDFC473F40
6	329843.12	3.4621448e+06	-1.5873357	0	0	3	0	3	2	1	3	0101000020E610000036980762994D5E40E4164974FC473F40
6	329843.12	3.4621442e+06	-1.5898876	0	0	3	0	3	2	1	3	0101000020E6100000B546D761994D5E409F6D6A26FC473F40
6	329843.12	3.4621435e+06	-1.5879302	0	0	3	0	3	2	1	3	0101000020E6100000FF4EE661994D5E40E10208D3FB473F40
6	329843.1	3.462143e+06	-1.5830383	0	1	3	0	3	2	1	3	0101000020E6100000B7E93762994D5E40D8839C82FB473F40
6	329843.12	3.4621425e+06	-1.5780686	0	1	3	0	3	2	1	3	0101000020E6100000EC3EB162994D5E40B52BFC32FB473F40
6	329843.12	3.462142e+06	-1.5726624	0	1	3	0	3	2	1	3	0101000020E610000076CA4A63994D5E4003F657E3FA473F40
6	329843.12	3.4621415e+06	-1.5670877	0	1	3	0	3	2	1	3	0101000020E6100000379F0564994D5E4033317793FA473F40
6	329843.12	3.462141e+06	-1.5602233	0	1	3	0	3	2	1	3	0101000020E61000006D9D0B65994D5E409EAD2B39FA473F40
6	329843.16	3.4621402e+06	-1.5539	0	1	3	0	3	2	1	3	0101000020E610000070EF2466994D5E406A25D6E5F9473F40
6	329843.2	3.4621398e+06	-1.5474967	0	1	3	0	3	2	1	3	0101000020E610000058F17367994D5E40D851158EF9473F40
6	329843.2	3.4621392e+06	-1.5433002	0	1	3	0	3	2	1	3	0101000020E610000067719E68994D5E40EDA70542F9473F40
6	329843.22	3.4621388e+06	-1.5407789	0	1	3	0	3	2	1	3	0101000020E6100000516DC169994D5E40ED7C57F6F8473F40
6	329843.22	3.4621382e+06	-1.5412613	0	1	3	0	3	2	1	3	0101000020E610000009B7CB6A994D5E40B2B5EDA5F8473F40
6	329843.25	3.4621378e+06	-1.5456198	0	1	3	0	3	2	1	3	0101000020E6100000BD63A16B994D5E403205B84FF8473F40
6	329843.25	3.462137e+06	-1.550112	0	1	3	0	3	2	1	3	0101000020E61000000284606C994D5E401E9B28F7F7473F40
6	329843.25	3.4621365e+06	-1.5531375	0	1	3	0	3	2	1	3	0101000020E610000066911E6D994D5E40375FA19DF7473F40
6	329843.25	3.4621358e+06	-1.5557499	0	1	3	0	3	2	1	3	0101000020E6100000852DD66D994D5E4073C59F41F7473F40
6	329843.25	3.4621352e+06	-1.5579157	0	1	3	0	3	2	1	3	0101000020E6100000A4C98D6E994D5E4027F719E1F6473F40
6	329843.28	3.4621345e+06	-1.5598924	0	1	3	0	3	2	1	3	0101000020E6100000B737346F994D5E40FB036D83F6473F40
6	329843.28	3.462134e+06	-1.5617609	0	1	3	0	3	2	1	3	0101000020E610000023D6CE6F994D5E4003E97725F6473F40
6	329843.28	3.4621332e+06	-1.5637169	0	1	3	0	3	2	1	3	0101000020E61000002A166470994D5E401F5A49C2F5473F40
6	329843.28	3.4621328e+06	-1.56567	0	1	3	0	3	2	1	3	0101000020E6100000C860EB70994D5E409265295FF5473F40
6	329843.28	3.4621322e+06	-1.5671662	0	1	3	0	3	2	1	3	0101000020E610000009DE4971994D5E40117F3013F5473F40
6	329843.28	3.4621318e+06	-1.568682	0	1	3	0	3	2	1	3	0101000020E610000045C49F71994D5E40619528C6F4473F40
6	329843.28	3.4621312e+06	-1.5702412	0	1	3	0	3	2	1	3	0101000020E61000001D4CF071994D5E40AB6BDC76F4473F40
6	329843.28	3.4621308e+06	-1.571765	0	1	3	0	3	2	1	3	0101000020E61000004C043572994D5E40E1825129F4473F40
6	329843.28	3.46213e+06	-1.5732965	0	1	3	0	3	2	1	3	0101000020E610000057387272994D5E40A9CF60DBF3473F40
6	329843.25	3.4621295e+06	-1.5749233	0	1	3	0	3	2	1	3	0101000020E61000001DFBA872994D5E40C18E8C88F3473F40
6	329843.25	3.462129e+06	-1.5765176	0	1	3	0	3	2	1	3	0101000020E61000001B01D572994D5E4067C15037F3473F40
6	329843.25	3.4621285e+06	-1.5780324	0	1	3	0	3	2	1	3	0101000020E6100000524AF672994D5E40F77923EAF2473F40
6	329843.25	3.462128e+06	-1.5796535	0	1	3	0	3	2	1	3	0101000020E6100000640F1073994D5E405BCB9A98F2473F40
6	329843.25	3.4621275e+06	-1.5813091	0	1	3	0	3	2	1	3	0101000020E6100000CE041E73994D5E40919DC248F2473F40
6	329843.22	3.4621268e+06	-1.5833361	0	1	3	0	3	2	1	3	0101000020E6100000703D2173994D5E40400011E8F1473F40
6	329843.22	3.4621262e+06	-1.5852098	0	1	3	0	3	2	1	3	0101000020E6100000A9801673994D5E409FE4488FF1473F40
6	329843.22	3.4621258e+06	-1.5868775	0	1	3	0	3	2	1	3	0101000020E6100000FA190273994D5E401453CE40F1473F40
6	329843.2	3.4621252e+06	-1.5886891	0	1	3	0	3	2	1	3	0101000020E6100000A4E3E172994D5E407313BAEDF0473F40
6	329843.2	3.462125e+06	-1.589001	0	1	3	1	3	2	1	3	0101000020E61000009E4CD972994D5E4066C822DDF0473F40
0	329843.2	3.4621245e+06	-1.5875003	0	2	3	0	3	2	1	3	0101000020E610000039F4FF72994D5E40D979F389F0473F40
0	329843.2	3.462124e+06	-1.5839472	0	2	3	0	3	2	1	3	0101000020E610000043283D73994D5E4040881F3DF0473F40
0	329843.2	3.4621235e+06	-1.579706	0	2	3	0	3	2	1	3	0101000020E610000066B89C73994D5E409201D6EAEF473F40
0	329843.2	3.462123e+06	-1.5765945	0	2	3	0	3	2	1	3	0101000020E61000002A81FF73994D5E4092DCA49DEF473F40
0	329843.2	3.4621225e+06	-1.5764788	0	2	3	0	3	2	1	3	0101000020E610000034B53C74994D5E40DA2FC950EF473F40
0	329843.2	3.462122e+06	-1.5804046	0	2	3	0	3	2	1	3	0101000020E61000002F1E3474994D5E4022631C01EF473F40
0	329843.16	3.4621215e+06	-1.5859764	0	2	3	0	3	2	1	3	0101000020E6100000A19EF273994D5E4060C7A1B4EE473F40
0	329843.12	3.462121e+06	-1.5964587	0	2	3	0	3	2	1	3	0101000020E6100000FFB63673994D5E40FBD87E65EE473F40
0	329843.1	3.4621202e+06	-1.6115937	0	2	3	0	3	2	1	3	0101000020E61000009469E371994D5E408BA4F818EE473F40
0	329843.06	3.4621198e+06	-1.6280404	0	2	3	0	3	2	1	3	0101000020E610000097FF1970994D5E400526C7C7ED473F40
0	329843	3.4621192e+06	-1.6436534	0	2	3	0	3	2	1	3	0101000020E61000008F61136E994D5E400BE3687BED473F40
0	329842.94	3.4621188e+06	-1.6627561	0	2	3	0	3	2	1	3	0101000020E6100000BF5D756B994D5E4097564C2FED473F40
0	329842.84	3.4621182e+06	-1.6886892	0	2	3	0	3	2	1	3	0101000020E6100000AC33EC67994D5E40EE16F4E2EC473F40
0	329842.72	3.4621178e+06	-1.7276915	0	2	3	0	3	2	1	3	0101000020E61000001DF1C962994D5E40DEA9BB94EC473F40
0	329842.56	3.4621172e+06	-1.776517	0	2	3	0	3	2	1	3	0101000020E6100000705D0B5C994D5E40E1783247EC473F40
0	329842.34	3.4621168e+06	-1.8382869	0	2	3	0	3	2	1	3	0101000020E6100000C65F8353994D5E40EDF9D7FFEB473F40
0	329842.1	3.4621165e+06	-1.9242178	0	2	3	0	3	2	1	3	0101000020E610000069A8FB47994D5E40C3D46BBAEB473F40
0	329841.75	3.462116e+06	-2.0191839	0	2	3	0	3	2	1	3	0101000020E6100000A6DCB739994D5E40E6092876EB473F40
0	329841.4	3.4621155e+06	-2.1054018	0	2	3	0	3	2	1	3	0101000020E6100000975FD82A994D5E40C8F52D3AEB473F40
0	329841.06	3.4621152e+06	-2.1781461	0	2	3	0	3	2	1	3	0101000020E61000007EAEBB1B994D5E4086200D03EB473F40
0	329840.66	3.4621148e+06	-2.2469668	0	2	3	0	3	2	1	3	0101000020E6100000CC3CF609994D5E40176DFBC6EA473F40
0	329840.28	3.4621145e+06	-2.2860327	0	2	3	0	3	2	1	3	0101000020E6100000A11707FA984D5E403C674990EA473F40
0	329839.9	3.462114e+06	-2.3177247	0	2	3	0	3	2	1	3	0101000020E61000008BABD9E9984D5E400FA95B5AEA473F40
0	329839.5	3.4621138e+06	-2.3571072	0	2	3	0	3	2	1	3	0101000020E6100000E179A9D8984D5E40A5F81D27EA473F40
0	329839.1	3.4621135e+06	-2.4214585	0	2	3	0	3	2	1	3	0101000020E6100000878F4BC6984D5E405C7602FDE9473F40
0	329838.62	3.4621132e+06	-2.5030696	0	2	3	0	3	2	1	3	0101000020E610000090A581B2984D5E40FE487FDAE9473F40
0	329838.16	3.462113e+06	-2.596917	0	2	3	0	3	2	1	3	0101000020E610000027D75B9D984D5E40CDE19ABFE9473F40
0	329837.66	3.462113e+06	-2.6898003	0	2	3	0	3	2	1	3	0101000020E610000082280C88984D5E407A7572ACE9473F40
0	329837.16	3.462113e+06	-2.7935247	0	2	3	0	3	2	1	3	0101000020E6100000A5D85571984D5E40299588A1E9473F40
0	329836.62	3.462113e+06	-2.904533	0	2	3	0	3	2	1	3	0101000020E6100000A3FE6B5A984D5E404F00BDA0E9473F40
0	329836.1	3.462113e+06	-2.9923956	0	2	3	0	3	2	1	3	0101000020E61000008725FF42984D5E4024C0DFA2E9473F40
0	329835.56	3.462113e+06	-3.0486357	0	2	3	0	3	2	1	3	0101000020E61000007E0B802B984D5E406718C2A3E9473F40
0	329835.03	3.462113e+06	-3.0720346	0	2	3	0	3	2	1	3	0101000020E6100000545B7513984D5E4094D5B1A0E9473F40
0	329834.4	3.462113e+06	-3.0728202	0	2	3	0	3	2	1	3	0101000020E61000006C6CE6F8974D5E403A5E1D99E9473F40
0	329833.9	3.4621128e+06	-3.0721297	0	2	3	0	3	2	1	3	0101000020E610000046BDADE2974D5E40DE697692E9473F40
0	329833.38	3.4621128e+06	-3.0712306	0	2	3	0	3	2	1	3	0101000020E6100000157CC6CA974D5E403C8D338BE9473F40
7	329832.94	3.4621128e+06	-3.0704613	0	2	3	2	3	2	1	3	0101000020E610000066A037B7974D5E40E6423C85E9473F40
7	329832.38	3.4621128e+06	-3.0789194	0	0	3	0	3	2	1	3	0101000020E610000053A3C29E974D5E40BE337780E9473F40
7	329831.75	3.4621128e+06	-3.0893645	0	0	3	0	3	2	1	3	0101000020E6100000F23EFB82974D5E4007AB9C7BE9473F40
7	329831.1	3.4621128e+06	-3.0999162	0	0	3	0	3	2	1	3	0101000020E61000006BAD9F66974D5E4087E8A477E9473F40
7	329830.5	3.4621128e+06	-3.1092734	0	0	3	0	3	2	1	3	0101000020E6100000E1850D4C974D5E40D993B674E9473F40
7	329829.97	3.4621128e+06	-3.1158164	0	0	3	0	3	2	1	3	0101000020E61000007665CE35974D5E40C6F99572E9473F40
7	329829.44	3.4621128e+06	-3.1206813	0	0	3	0	3	2	1	3	0101000020E6100000CD06F41D974D5E40764D5470E9473F40
7	329828.84	3.4621128e+06	-3.1250114	0	0	3	0	3	2	1	3	0101000020E6100000B31A3104974D5E40C7A71E6EE9473F40
7	329828.25	3.4621128e+06	-3.1294234	0	0	3	0	3	2	1	3	0101000020E6100000F3A351E9964D5E40F1BB306CE9473F40
7	329827.6	3.4621128e+06	-3.1338851	0	0	3	0	3	2	1	3	0101000020E6100000021DE8CC964D5E401840896AE9473F40
7	329826.9	3.4621128e+06	-3.1386333	0	0	3	0	3	2	1	3	0101000020E61000005E850BAE964D5E40EBCC3369E9473F40
7	329826.22	3.4621128e+06	3.1399043	0	0	3	0	3	2	1	3	0101000020E61000007388808F964D5E407CC35B68E9473F40
7	329825.5	3.4621128e+06	3.135259	0	0	3	0	3	2	1	3	0101000020E6100000D4DE1F70964D5E400367F667E9473F40
7	329824.78	3.4621128e+06	3.1309533	0	0	3	0	3	2	1	3	0101000020E61000001B301050964D5E40281DF567E9473F40
7	329824.25	3.4621128e+06	3.1283195	0	0	3	0	3	2	1	3	0101000020E61000003EE05939964D5E40EE112368E9473F40
7	329823.75	3.4621128e+06	3.1261868	0	0	3	0	3	2	1	3	0101000020E6100000056C0723964D5E401ECA6768E9473F40
7	329823.22	3.4621128e+06	3.1248581	0	0	3	0	3	2	1	3	0101000020E6100000145D630C964D5E40610FA368E9473F40
7	329822.7	3.4621128e+06	3.1244848	0	0	3	0	3	2	1	3	0101000020E610000061CA4BF4954D5E40C4D7C268E9473F40
7	329822.12	3.4621128e+06	3.1246164	0	0	3	0	3	2	1	3	0101000020E6100000AA9AFFDB954D5E403396D068E9473F40
7	329821.62	3.4621128e+06	3.1248307	0	0	3	0	3	2	1	3	0101000020E6100000AC6376C5954D5E408499D668E9473F40
7	329821.1	3.4621128e+06	3.1250613	0	0	3	0	3	2	1	3	0101000020E6100000B071DCAD954D5E407807D768E9473F40
7	329820.56	3.4621128e+06	3.125289	0	0	3	0	3	2	1	3	0101000020E6100000D70FA296954D5E40014ED268E9473F40
7	329820	3.4621128e+06	3.1255176	0	0	3	0	3	2	1	3	0101000020E6100000B447E77D954D5E40FBB6C968E9473F40
7	329819.47	3.4621128e+06	3.1256175	0	0	3	0	3	2	1	3	0101000020E6100000EB013A66954D5E40928FC468E9473F40
7	329818.84	3.4621128e+06	3.1256342	0	0	3	0	3	2	1	3	0101000020E61000001135644B954D5E40F61FC168E9473F40
7	329818.28	3.4621128e+06	3.1256404	0	0	3	0	3	2	1	3	0101000020E6100000F1605132954D5E406642BD68E9473F40
7	329817.7	3.4621128e+06	3.12581	0	0	3	0	3	2	1	3	0101000020E61000009F2B6D18954D5E4028CCAD68E9473F40
7	329817.1	3.4621128e+06	3.1264687	0	0	3	0	3	2	1	3	0101000020E61000002F54A5FD944D5E40C6677C68E9473F40
7	329816.5	3.4621128e+06	3.127725	0	0	3	0	3	2	1	3	0101000020E610000019565EE3944D5E4040791768E9473F40
7	329815.88	3.4621128e+06	3.129766	0	0	3	0	3	2	1	3	0101000020E610000013C5EBC7944D5E4078A96567E9473F40
7	329815.25	3.462113e+06	3.1324031	0	0	3	0	3	2	1	3	0101000020E6100000D893E4AC944D5E4010636166E9473F40
7	329814.66	3.462113e+06	3.1353364	0	0	3	0	3	2	1	3	0101000020E6100000D6FD1793944D5E4079641865E9473F40
7	329814.06	3.462113e+06	3.1385825	0	0	3	0	3	2	1	3	0101000020E6100000613E0079944D5E40B3117963E9473F40
7	329813.44	3.4621128e+06	-3.141127	0	0	3	0	3	2	1	3	0101000020E61000005CA7615D944D5E40FDA86F61E9473F40
7	329812.84	3.4621128e+06	-3.1380422	0	0	3	0	3	2	1	3	0101000020E6100000E444E942944D5E40DEE03D5FE9473F40
7	329812.22	3.4621128e+06	-3.1352885	0	0	3	0	3	2	1	3	0101000020E61000000826FA26944D5E40BB11BD5CE9473F40
7	329811.6	3.4621128e+06	-3.133033	0	0	3	0	3	2	1	3	0101000020E610000074C4FE0B944D5E40BF5C295AE9473F40
7	329810.88	3.4621128e+06	-3.1311598	0	0	3	0	3	2	1	3	0101000020E6100000BDBE7BEC934D5E40CD5E1157E9473F40
7	329810.25	3.4621128e+06	-3.1304839	0	0	3	0	3	2	1	3	0101000020E6100000D27DD3D0934D5E40C57B6C54E9473F40
7	329809.66	3.4621128e+06	-3.1309686	0	0	3	0	3	2	1	3	0101000020E6100000D23222B6934D5E400B0C1452E9473F40
7	329809.06	3.4621128e+06	-3.1329925	0	0	3	0	3	2	1	3	0101000020E61000006C447C9C934D5E40282A3850E9473F40
7	329808.5	3.4621128e+06	-3.1362195	0	0	3	0	3	2	1	3	0101000020E61000003608EF82934D5E40AB17CB4EE9473F40
7	329807.94	3.4621128e+06	-3.1396132	0	0	3	0	3	2	1	3	0101000020E61000007847C66A934D5E409ACFC34DE9473F40
7	329807.44	3.4621128e+06	3.140363	0	0	3	0	3	2	1	3	0101000020E61000005A233E54934D5E40A0B7134DE9473F40
7	329806.9	3.4621128e+06	3.1371503	0	0	3	0	3	2	1	3	0101000020E61000008C9ECD3D934D5E40D657A84CE9473F40
7	329806.25	3.4621128e+06	3.1329021	0	0	3	0	3	2	1	3	0101000020E6100000D79F4820934D5E404842814CE9473F40
7	329805.62	3.4621128e+06	3.1289744	0	0	3	0	3	2	1	3	0101000020E6100000C8832505934D5E40C266C34CE9473F40
7	329805.1	3.4621128e+06	3.125503	0	0	3	0	3	2	1	3	0101000020E6100000A90758ED924D5E40F08E4E4DE9473F40
7	329804.5	3.462113e+06	3.121672	0	0	3	0	3	2	1	3	0101000020E6100000228326D3924D5E40BCC93D4EE9473F40
7	329804	3.462113e+06	3.1196444	0	1	3	0	3	2	1	3	0101000020E6100000EFA5DCBC924D5E40A823234FE9473F40
7	329803.47	3.462113e+06	3.1221812	0	1	3	0	3	2	1	3	0101000020E6100000105C52A6924D5E40B6E1ED4EE9473F40
7	329802.94	3.462113e+06	3.1247697	0	1	3	0	3	2	1	3	0101000020E61000009C52F18E924D5E40B33E8D4EE9473F40
7	329802.4	3.462113e+06	3.1273777	0	1	3	0	3	2	1	3	0101000020E6100000EE5C0E77924D5E40675BFA4DE9473F40
7	329801.78	3.462113e+06	3.1304183	0	1	3	0	3	2	1	3	0101000020E610000005100E5B924D5E4068D80C4DE9473F40
7	329801.1	3.462113e+06	3.1336532	0	1	3	0	3	2	1	3	0101000020E6100000FBD43C3D924D5E4042FCBF4BE9473F40
7	329800.4	3.462113e+06	3.1369689	0	1	3	0	3	2	1	3	0101000020E610000087F5A41E924D5E40FF58134AE9473F40
7	329799.66	3.462113e+06	3.140446	0	1	3	0	3	2	1	3	0101000020E610000020E080FE914D5E4005E3F147E9473F40
7	329798.97	3.462113e+06	-3.1394627	0	1	3	0	3	2	1	3	0101000020E6100000B73426E0914D5E408A4D9745E9473F40
7	329798.44	3.462113e+06	-3.1368248	0	1	3	0	3	2	1	3	0101000020E610000040D9ABC7914D5E4000FA7143E9473F40
7	329797.7	3.462113e+06	-3.133306	0	1	3	0	3	2	1	3	0101000020E6100000B396F3A6914D5E40930F3B40E9473F40
7	329797.2	3.462113e+06	-3.1309068	0	1	3	0	3	2	1	3	0101000020E610000016C49B90914D5E40E695D03DE9473F40
7	329796.66	3.462113e+06	-3.128484	0	1	3	0	3	2	1	3	0101000020E6100000929EAD79914D5E405B67273BE9473F40
7	329796.1	3.462113e+06	-3.12623	0	1	3	0	3	2	1	3	0101000020E61000002D084D61914D5E4068A13238E9473F40
7	329795.53	3.4621128e+06	-3.1246884	0	1	3	0	3	2	1	3	0101000020E6100000BF378348914D5E4012AF2F35E9473F40
7	329795.03	3.4621128e+06	-3.1242805	0	1	3	0	3	2	1	3	0101000020E61000000EFDB031914D5E402F828932E9473F40
7	329794.44	3.4621128e+06	-3.1248126	0	1	3	0	3	2	1	3	0101000020E6100000C20D8E18914D5E403A90C92FE9473F40
7	329793.9	3.4621128e+06	-3.1254933	0	1	3	0	3	2	1	3	0101000020E61000009DFAB700914D5E4037E03E2DE9473F40
7	329793.34	3.4621128e+06	-3.1261935	0	1	3	0	3	2	1	3	0101000020E6100000187758E8904D5E403430B42AE9473F40
7	329792.78	3.4621128e+06	-3.1269233	0	1	3	0	3	2	1	3	0101000020E6100000B625E7CE904D5E408F791D28E9473F40
7	329792.22	3.4621128e+06	-3.1276214	0	1	3	0	3	2	1	3	0101000020E6100000B4ED8BB6904D5E40FC23B225E9473F40
7	329791.66	3.4621128e+06	-3.1283195	0	1	3	0	3	2	1	3	0101000020E610000094C8319E904D5E408D205723E9473F40
7	329791.12	3.4621128e+06	-3.1290636	0	1	3	0	3	2	1	3	0101000020E6100000759DAB85904D5E40B4910821E9473F40
7	329790.53	3.4621128e+06	-3.1305134	0	1	3	0	3	2	1	3	0101000020E6100000812FC46B904D5E406F13D81EE9473F40
7	329789.94	3.4621128e+06	-3.1330607	0	1	3	0	3	2	1	3	0101000020E61000009B924E52904D5E401BAB111DE9473F40
7	329789.22	3.4621128e+06	-3.1371489	0	1	3	0	3	2	1	3	0101000020E6100000AC49D632904D5E40E499641BE9473F40
7	329788.53	3.4621128e+06	-3.1413007	0	1	3	0	3	2	1	3	0101000020E6100000D40B3914904D5E408BAD3C1AE9473F40
7	329787.94	3.4621128e+06	3.1383061	0	1	3	0	3	2	1	3	0101000020E61000005612B8F98F4D5E40329C9819E9473F40
7	329787.4	3.4621128e+06	3.1352754	0	1	3	0	3	2	1	3	0101000020E61000000ECDF3E28F4D5E40CB044D19E9473F40
7	329786.88	3.4621128e+06	3.132113	0	1	3	0	3	2	1	3	0101000020E61000006817A6CB8F4D5E4093254619E9473F40
7	329786.8	3.4621128e+06	3.131791	0	1	3	1	3	2	1	3	0101000020E6100000884296C88F4D5E4041BE5119E9473F40
0	329786.25	3.4621128e+06	3.1342592	0	2	3	0	3	2	1	3	0101000020E6100000BEF5EAAE8F4D5E403A9D0C18E9473F40
0	329785.7	3.4621128e+06	3.1367118	0	2	3	0	3	2	1	3	0101000020E6100000ABA72E978F4D5E40B52BBA16E9473F40
0	329785.06	3.4621128e+06	3.1395128	0	2	3	0	3	2	1	3	0101000020E61000008529BD7B8F4D5E40F0A0F714E9473F40
0	329784.53	3.4621128e+06	-3.14125	0	2	3	0	3	2	1	3	0101000020E610000093C2D3638F4D5E4089AB3A13E9473F40
0	329783.88	3.4621128e+06	-3.1383839	0	2	3	0	3	2	1	3	0101000020E6100000960D59478F4D5E40FA88E910E9473F40
0	329783.2	3.4621128e+06	-3.1353455	0	2	3	0	3	2	1	3	0101000020E61000006C36D0288F4D5E40CEB7220EE9473F40
0	329782.62	3.4621128e+06	-3.1329286	0	2	3	0	3	2	1	3	0101000020E6100000E8B270108F4D5E40F6549F0BE9473F40
0	329782.06	3.4621128e+06	-3.130629	0	2	3	0	3	2	1	3	0101000020E61000003571A0F78E4D5E40513A1A09E9473F40
0	329781.47	3.4621128e+06	-3.1281419	0	2	3	0	3	2	1	3	0101000020E61000006141FFDC8E4D5E4027F90C06E9473F40
0	329780.84	3.4621128e+06	-3.125594	0	2	3	0	3	2	1	3	0101000020E6100000614795C18E4D5E40FFABA702E9473F40
0	329780.12	3.4621128e+06	-3.1231174	0	2	3	0	3	2	1	3	0101000020E6100000215F05A28E4D5E409F08A6FEE8473F40
0	329779.34	3.4621128e+06	-3.121055	0	2	3	0	3	2	1	3	0101000020E61000002432C57F8E4D5E40B40B02FAE8473F40
0	329778.53	3.4621128e+06	-3.1193523	0	2	3	0	3	2	1	3	0101000020E61000007FD7075C8E4D5E4041903FF5E8473F40
0	329777.66	3.4621128e+06	-3.1175632	0	2	3	0	3	2	1	3	0101000020E61000003C76E0348E4D5E403A55A6EFE8473F40
0	329776.78	3.4621128e+06	-3.1158764	0	2	3	0	3	2	1	3	0101000020E6100000E1674F0E8E4D5E40B62DEEE9E8473F40
0	329776.28	3.4621128e+06	-3.114898	0	2	3	0	3	2	1	3	0101000020E61000002A452DF88D4D5E4076BC89E6E8473F40
0	329775.3	3.4621128e+06	-3.1130633	0	2	3	0	3	2	1	3	0101000020E6100000ACF085CE8D4D5E401269FDDFE8473F40
0	329774.3	3.4621125e+06	-3.1115363	0	2	3	0	3	2	1	3	0101000020E61000001B1BE2A18D4D5E40692DD5D8E8473F40
0	329773.28	3.4621125e+06	-3.1115375	0	2	3	0	3	2	1	3	0101000020E6100000B954F6748D4D5E401728CDD1E8473F40
0	329772.78	3.4621125e+06	-3.1122088	0	2	3	0	3	2	1	3	0101000020E6100000EA26F95D8D4D5E4015F554CEE8473F40
0	329771.78	3.4621125e+06	-3.114937	0	2	3	0	3	2	1	3	0101000020E610000091492F328D4D5E40F31E27C8E8473F40
0	329771.25	3.4621125e+06	-3.1169257	0	2	3	0	3	2	1	3	0101000020E6100000F3CD4A1B8D4D5E4014E628C5E8473F40
0	329770.72	3.4621125e+06	-3.1191628	0	2	3	0	3	2	1	3	0101000020E61000004D18FD038D4D5E40142A46C2E8473F40
0	329770.22	3.4621125e+06	-3.1214468	0	2	3	0	3	2	1	3	0101000020E6100000FE415CED8C4D5E40D9FEA2BFE8473F40
0	329769.62	3.4621125e+06	-3.1241271	0	2	3	0	3	2	1	3	0101000020E6100000359E3DD48C4D5E407A81EFBCE8473F40
0	329769.06	3.4621125e+06	-3.1271405	0	2	3	0	3	2	1	3	0101000020E6100000350245BA8C4D5E404B206FBAE8473F40
0	329768.06	3.4621125e+06	-3.1329331	0	2	3	0	3	2	1	3	0101000020E610000090D1508F8C4D5E40CCD40CB7E8473F40
0	329767.56	3.4621125e+06	-3.1363637	0	2	3	0	3	2	1	3	0101000020E6100000A8FC15798C4D5E40ACF3B6B5E8473F40
0	329767.06	3.4621125e+06	-3.1400795	0	2	3	0	3	2	1	3	0101000020E610000034A26D628C4D5E40C85CA5B4E8473F40
0	329766.1	3.4621125e+06	3.1357942	0	2	3	0	3	2	1	3	0101000020E6100000A970FC378C4D5E4062426CB3E8473F40
0	329765.56	3.4621125e+06	3.1318095	0	2	3	0	3	2	1	3	0101000020E6100000EC0719218C4D5E404B4A38B3E8473F40
0	329764.72	3.4621125e+06	3.1258614	0	2	3	0	3	2	1	3	0101000020E6100000E4412CFB8B4D5E40B34572B3E8473F40
0	329763.84	3.4621125e+06	3.1214483	0	2	3	0	3	2	1	3	0101000020E6100000EA9DF8D48B4D5E406F8324B4E8473F40
0	329763.06	3.4621125e+06	3.118636	0	2	3	0	3	2	1	3	0101000020E6100000DC542BB38B4D5E402E2C14B5E8473F40
0	329762.34	3.4621125e+06	3.1163568	0	2	3	0	3	2	1	3	0101000020E6100000584CDC928B4D5E4069F945B6E8473F40
0	329761.62	3.4621125e+06	3.1143496	0	2	3	0	3	2	1	3	0101000020E6100000A197A0728B4D5E409B9FB5B7E8473F40
0	329760.97	3.4621125e+06	3.113168	0	2	3	0	3	2	1	3	0101000020E6100000427EF4558B4D5E40181611B9E8473F40
0	329760.38	3.4621128e+06	3.1131506	0	2	3	0	3	2	1	3	0101000020E61000006A5AAB3B8B4D5E40603D1FBAE8473F40
0	329759.8	3.4621128e+06	3.1144416	0	2	3	0	3	2	1	3	0101000020E6100000AD9356238B4D5E40CB77CBBAE8473F40
0	329759.2	3.4621128e+06	3.1169076	0	2	3	0	3	2	1	3	0101000020E6100000527550088B4D5E40F36C3CBBE8473F40
0	329758.7	3.4621128e+06	3.1191099	0	2	3	0	3	2	1	3	0101000020E610000057E127F28A4D5E40C6F369BBE8473F40
0	329757.97	3.4621128e+06	3.122265	0	2	3	0	3	2	1	3	0101000020E6100000E1A94AD28A4D5E409AA662BBE8473F40
0	329757.3	3.4621128e+06	3.125165	0	2	3	0	3	2	1	3	0101000020E6100000DB0BAEB48A4D5E403A6EFCBAE8473F40
0	329756.72	3.4621128e+06	3.1277373	0	2	3	0	3	2	1	3	0101000020E6100000AF5AD19A8A4D5E40F48560BAE8473F40
0	329756.16	3.4621128e+06	3.1301749	0	2	3	0	3	2	1	3	0101000020E61000003C9C8B828A4D5E4084189AB9E8473F40
0	329755.53	3.4621128e+06	3.132998	0	2	3	0	3	2	1	3	0101000020E610000097C6BD668A4D5E40BA0976B8E8473F40
0	329754.94	3.4621128e+06	3.1356368	0	2	3	0	3	2	1	3	0101000020E61000007643F24C8A4D5E40D20727B7E8473F40
0	329754.28	3.4621128e+06	3.1386344	0	2	3	0	3	2	1	3	0101000020E61000005AF2BF2F8A4D5E40710D61B5E8473F40
0	329753.75	3.4621128e+06	3.1409967	0	2	3	0	3	2	1	3	0101000020E6100000CA9EC0188A4D5E408504C3B3E8473F40
0	329753.16	3.4621128e+06	-3.1394217	0	2	3	0	3	2	1	3	0101000020E61000008D73E5FD894D5E40726AA2B1E8473F40
0	329752.66	3.4621128e+06	-3.1371434	0	2	3	0	3	2	1	3	0101000020E6100000A00176E7894D5E40A279ABAFE8473F40
3	329752.38	3.4621128e+06	-3.1350563	0	2	3	2	3	2	1	3	0101000020E610000023AE36DB894D5E40E39898AEE8473F40
3	329751.84	3.4621128e+06	-3.1339934	0	0	3	0	3	2	1	3	0101000020E6100000087E56C4894D5E40C70FD9ACE8473F40
3	329751.28	3.4621128e+06	-3.1369777	0	0	3	0	3	2	1	3	0101000020E6100000EE404CAB894D5E4049358FABE8473F40
3	329750.72	3.4621128e+06	-3.140059	0	0	3	0	3	2	1	3	0101000020E61000004DC49592894D5E40266091AAE8473F40
3	329750.16	3.4621128e+06	3.1406345	0	0	3	0	3	2	1	3	0101000020E610000075FEBD79894D5E40FB2BAEA9E8473F40
3	329749.6	3.4621128e+06	3.139101	0	0	3	0	3	2	1	3	0101000020E610000007DDAC61894D5E40921DCDA8E8473F40
3	329749.1	3.4621128e+06	3.1388175	0	0	3	0	3	2	1	3	0101000020E61000001877954B894D5E40D96FD4A7E8473F40
3	329748.56	3.4621128e+06	3.1391828	0	0	3	0	3	2	1	3	0101000020E61000008A173E34894D5E4073F1ACA6E8473F40
3	329748.03	3.4621128e+06	3.1395993	0	0	3	0	3	2	1	3	0101000020E61000000A31131C894D5E40716770A5E8473F40
3	329747.44	3.4621128e+06	3.1399784	0	0	3	0	3	2	1	3	0101000020E61000006D4A9301894D5E408AC90FA4E8473F40
3	329746.78	3.4621128e+06	3.1403606	0	0	3	0	3	2	1	3	0101000020E61000000994B2E4884D5E40141688A2E8473F40
3	329746.12	3.4621128e+06	3.140517	0	0	3	0	3	2	1	3	0101000020E6100000CA1092C8884D5E40F5FC0EA1E8473F40
3	329745.4	3.4621128e+06	3.140173	0	0	3	0	3	2	1	3	0101000020E6100000ED8C33A9884D5E406625889FE8473F40
3	329744.9	3.4621128e+06	3.1395843	0	0	3	0	3	2	1	3	0101000020E6100000CF68AB92884D5E405C74899EE8473F40
3	329744.16	3.4621128e+06	3.13827	0	0	3	0	3	2	1	3	0101000020E61000006A9EA271884D5E4047C1449DE8473F40
3	329743.38	3.4621128e+06	3.1367702	0	0	3	0	3	2	1	3	0101000020E61000007F367C4F884D5E401922249CE8473F40
3	329742.88	3.4621128e+06	3.1357675	0	0	3	0	3	2	1	3	0101000020E6100000EDE8A838884D5E404A577B9BE8473F40
3	329742.3	3.4621128e+06	3.1347365	0	0	3	0	3	2	1	3	0101000020E610000003BC2821884D5E40A1DEE29AE8473F40
3	329741.78	3.4621128e+06	3.133655	0	0	3	0	3	2	1	3	0101000020E6100000A1197008884D5E404E00599AE8473F40
3	329741.2	3.4621128e+06	3.1325653	0	0	3	0	3	2	1	3	0101000020E6100000B2F775EF874D5E407E09E599E8473F40
3	329740.62	3.4621128e+06	3.1314585	0	0	3	0	3	2	1	3	0101000020E610000036500ED6874D5E4017D68799E8473F40
3	329740.03	3.4621128e+06	3.1302972	0	0	3	0	3	2	1	3	0101000020E6100000118155BB874D5E4058403F99E8473F40
3	329739.4	3.4621128e+06	3.1291533	0	0	3	0	3	2	1	3	0101000020E61000000E42FCA0874D5E406C951299E8473F40
3	329738.8	3.4621128e+06	3.1279912	0	0	3	0	3	2	1	3	0101000020E6100000366FFA85874D5E40B765FE98E8473F40
3	329738.2	3.4621128e+06	3.1268845	0	0	3	0	3	2	1	3	0101000020E6100000F3FD5D6A874D5E4060670199E8473F40
3	329737.53	3.4621128e+06	3.125911	0	0	3	0	3	2	1	3	0101000020E6100000F3543B4E874D5E4008051699E8473F40
3	329736.9	3.4621128e+06	3.125133	0	0	3	0	3	2	1	3	0101000020E610000017364C32874D5E4045173799E8473F40
3	329736.28	3.4621128e+06	3.1244683	0	0	3	0	3	2	1	3	0101000020E61000008D5F0117874D5E40F2E76599E8473F40
3	329735.62	3.4621128e+06	3.1237595	0	0	3	0	3	2	1	3	0101000020E61000009A8052F9864D5E403BC4A999E8473F40
3	329734.97	3.4621128e+06	3.1230688	0	0	3	0	3	2	1	3	0101000020E6100000FF8050DC864D5E40B584FD99E8473F40
3	329734.25	3.4621128e+06	3.1223245	0	0	3	0	3	2	1	3	0101000020E610000022FDF1BC864D5E4034786B9AE8473F40
3	329733.53	3.4621128e+06	3.1218488	0	0	3	0	3	2	1	3	0101000020E61000005DCF899D864D5E408EB5DA9AE8473F40
3	329732.84	3.4621128e+06	3.1225743	0	0	3	0	3	2	1	3	0101000020E610000040D5CA7F864D5E401039029BE8473F40
3	329732.2	3.462113e+06	3.1248906	0	0	3	0	3	2	1	3	0101000020E610000009851562864D5E4013C9BB9AE8473F40
3	329731.53	3.462113e+06	3.1281576	0	0	3	0	3	2	1	3	0101000020E6100000F861F444864D5E40CD440E9AE8473F40
3	329730.88	3.462113e+06	3.1316056	0	0	3	0	3	2	1	3	0101000020E6100000B1532329864D5E40F5DB0D99E8473F40
3	329730.16	3.462113e+06	3.1357312	0	0	3	0	3	2	1	3	0101000020E6100000303F7C08864D5E40FD407097E8473F40
3	329729.62	3.462113e+06	3.1385865	0	0	3	0	3	2	1	3	0101000020E6100000F08A94F1854D5E4029300696E8473F40
3	329729.03	3.462113e+06	-3.141395	0	0	3	0	3	2	1	3	0101000020E6100000A537D4D6854D5E40EEB31B94E8473F40
3	329728.28	3.462113e+06	-3.1377773	0	0	3	0	3	2	1	3	0101000020E61000005C15DBB6854D5E40E6D07691E8473F40
3	329727.62	3.4621128e+06	-3.134666	0	0	3	0	3	2	1	3	0101000020E6100000964B1099854D5E40AD6DB08EE8473F40
3	329727	3.4621128e+06	-3.1319482	0	0	3	0	3	2	1	3	0101000020E6100000DF5FE17D854D5E40E42CE68BE8473F40
3	329726.5	3.4621128e+06	-3.1301608	0	0	3	0	3	2	1	3	0101000020E6100000B319A067854D5E4018948589E8473F40
3	329725.94	3.4621128e+06	-3.1285858	0	0	3	0	3	2	1	3	0101000020E61000006882C24F854D5E40D1D6E286E8473F40
3	329725.4	3.4621128e+06	-3.126733	0	1	3	0	3	2	1	3	0101000020E6100000D2394937854D5E40BDC52C84E8473F40
3	329724.88	3.4621128e+06	-3.1258326	0	1	3	0	3	2	1	3	0101000020E6100000CCC22A20854D5E40FAB77C81E8473F40
3	329724.3	3.4621128e+06	-3.1250775	0	1	3	0	3	2	1	3	0101000020E6100000822B4D08854D5E407CE3AF7EE8473F40
3	329723.7	3.4621128e+06	-3.1246004	0	1	3	0	3	2	1	3	0101000020E610000035D7BAEB844D5E402F7C5D7BE8473F40
3	329723.16	3.4621128e+06	-3.1247344	0	1	3	0	3	2	1	3	0101000020E6100000F37946D4844D5E40CF9ABB78E8473F40
3	329722.53	3.4621128e+06	-3.125982	0	1	3	0	3	2	1	3	0101000020E6100000354E82B8844D5E40320BE775E8473F40
3	329721.84	3.4621128e+06	-3.128553	0	1	3	0	3	2	1	3	0101000020E6100000053E629B844D5E40FD125E73E8473F40
3	329721.2	3.4621128e+06	-3.131293	0	1	3	0	3	2	1	3	0101000020E61000007FF7217E844D5E40F8D21971E8473F40
3	329720.53	3.4621128e+06	-3.1338205	0	1	3	0	3	2	1	3	0101000020E6100000FBAAB560844D5E400A8B096FE8473F40
3	329719.84	3.4621128e+06	-3.1357577	0	1	3	0	3	2	1	3	0101000020E61000001C8BF442844D5E40C3E00D6DE8473F40
3	329719.25	3.4621128e+06	-3.1360774	0	1	3	0	3	2	1	3	0101000020E610000016A93A28844D5E40C2A7186BE8473F40
3	329718.7	3.4621128e+06	-3.1342096	0	1	3	0	3	2	1	3	0101000020E6100000F4890C10844D5E404C45D868E8473F40
3	329718.03	3.4621128e+06	-3.1313396	0	1	3	0	3	2	1	3	0101000020E6100000E50F78F3834D5E40C30AD765E8473F40
3	329717.47	3.4621128e+06	-3.12902	0	1	3	0	3	2	1	3	0101000020E61000002E82BAD9834D5E40F596F262E8473F40
3	329716.94	3.4621128e+06	-3.1277237	0	1	3	0	3	2	1	3	0101000020E61000006988FAC2834D5E404AE56460E8473F40
3	329716.38	3.4621128e+06	-3.126906	0	1	3	0	3	2	1	3	0101000020E6100000C0D1DAA9834D5E408E369A5DE8473F40
3	329715.78	3.4621128e+06	-3.1276183	0	1	3	0	3	2	1	3	0101000020E61000002C1F988F834D5E4072C6FE5AE8473F40
3	329715.2	3.4621128e+06	-3.1297925	0	1	3	0	3	2	1	3	0101000020E610000035BD0876834D5E40B02FE458E8473F40
3	329714.7	3.4621128e+06	-3.1325119	0	1	3	0	3	2	1	3	0101000020E6100000D033D25F834D5E4078F26B57E8473F40
3	329714.12	3.4621128e+06	-3.1354346	0	1	3	0	3	2	1	3	0101000020E6100000FB105B47834D5E40E8520856E8473F40
3	329713.6	3.4621128e+06	-3.1382017	0	1	3	0	3	2	1	3	0101000020E61000005B435D2F834D5E4031D1DA54E8473F40
3	329713.16	3.4621128e+06	-3.1401467	0	1	3	1	3	2	1	3	0101000020E61000004321791C834D5E400CD01154E8473F40
0	329712.66	3.4621128e+06	3.1399088	0	2	3	0	3	2	1	3	0101000020E6100000C19EEB05834D5E402A787253E8473F40
0	329712.16	3.4621128e+06	3.1364045	0	2	3	0	3	2	1	3	0101000020E6100000ED889EEF824D5E40C4A80353E8473F40
0	329711.6	3.4621128e+06	3.1330466	0	2	3	0	3	2	1	3	0101000020E610000061231BD8824D5E403BC6F652E8473F40
0	329711.03	3.4621128e+06	3.128512	0	2	3	0	3	2	1	3	0101000020E610000025FF3DBF824D5E401EE67253E8473F40
0	329710.5	3.4621128e+06	3.1228325	0	2	3	0	3	2	1	3	0101000020E610000003E00FA7824D5E40521C9C54E8473F40
0	329709.97	3.4621128e+06	3.1157176	0	2	3	0	3	2	1	3	0101000020E61000001EFF7C90824D5E40869D8F56E8473F40
0	329709.44	3.4621128e+06	3.105592	0	2	3	0	3	2	1	3	0101000020E61000000320E478824D5E40256CD659E8473F40
0	329708.94	3.4621128e+06	3.0910318	0	2	3	0	3	2	1	3	0101000020E6100000A7215E62824D5E40AE17F05EE8473F40
0	329708.44	3.462113e+06	3.0715175	0	2	3	0	3	2	1	3	0101000020E61000002A3C054C824D5E40D80E6366E8473F40
0	329707.9	3.462113e+06	3.0483596	0	2	3	0	3	2	1	3	0101000020E610000097F45D35824D5E4001C2B870E8473F40
0	329707.4	3.462113e+06	3.0178335	0	2	3	0	3	2	1	3	0101000020E610000033B6421E824D5E409797687EE8473F40
0	329706.9	3.4621132e+06	2.988555	0	2	3	0	3	2	1	3	0101000020E6100000F559A008824D5E40962AB28DE8473F40
0	329706.34	3.4621132e+06	2.9543898	0	2	3	0	3	2	1	3	0101000020E61000006A907FEF814D5E40356149A2E8473F40
0	329705.78	3.4621135e+06	2.9205415	0	2	3	0	3	2	1	3	0101000020E6100000B3B17AD6814D5E40ED77C2B9E8473F40
0	329705.25	3.4621138e+06	2.889597	0	2	3	0	3	2	1	3	0101000020E6100000DA4F40BF814D5E408AED29D2E8473F40
0	329704.75	3.4621138e+06	2.859419	0	2	3	0	3	2	1	3	0101000020E6100000C177A5A9814D5E40443D98EBE8473F40
0	329704.28	3.462114e+06	2.827858	0	2	3	0	3	2	1	3	0101000020E6100000EDB99D94814D5E40B2C94407E9473F40
0	329703.8	3.4621142e+06	2.7965891	0	2	3	0	3	2	1	3	0101000020E610000008E63480814D5E409379BF24E9473F40
0	329703.3	3.4621145e+06	2.7595797	0	2	3	0	3	2	1	3	0101000020E6100000FCD70D69814D5E4073D3A549E9473F40
0	329702.8	3.4621148e+06	2.7183883	0	2	3	0	3	2	1	3	0101000020E6100000819B4153814D5E405BAC4271E9473F40
0	329702.38	3.462115e+06	2.675317	0	2	3	0	3	2	1	3	0101000020E61000009F6A393F814D5E40D1F8779AE9473F40
0	329701.94	3.4621152e+06	2.6308737	0	2	3	0	3	2	1	3	0101000020E61000001C59732C814D5E4028FBBDC5E9473F40
0	329701.53	3.4621158e+06	2.5822964	0	2	3	0	3	2	1	3	0101000020E6100000AFA9FB19814D5E40529E85F5E9473F40
0	329701.16	3.462116e+06	2.5301292	0	2	3	0	3	2	1	3	0101000020E6100000C79DCD08814D5E40ED831A28EA473F40
0	329700.75	3.4621165e+06	2.4726622	0	2	3	0	3	2	1	3	0101000020E6100000E9C5DCF7804D5E40E5AE9D60EA473F40
0	329700.4	3.4621168e+06	2.40669	0	2	3	0	3	2	1	3	0101000020E6100000CBC8D2E7804D5E40C6D0479FEA473F40
0	329700.1	3.4621172e+06	2.338972	0	2	3	0	3	2	1	3	0101000020E6100000162574D9804D5E403F4FACE0EA473F40
0	329699.8	3.4621178e+06	2.271258	0	2	3	0	3	2	1	3	0101000020E610000054B175CC804D5E40CA4E0D25EB473F40
0	329699.53	3.4621182e+06	2.2009926	0	2	3	0	3	2	1	3	0101000020E6100000C741A9C0804D5E4090B2066EEB473F40
0	329699.3	3.4621188e+06	2.1298862	0	2	3	0	3	2	1	3	0101000020E61000009F37E0B6804D5E40396E9FB7EB473F40
0	329699.12	3.4621192e+06	2.0500872	0	2	3	0	3	2	1	3	0101000020E6100000D3A9F8AD804D5E40AD47880BEC473F40
0	329699	3.4621198e+06	1.9837974	0	2	3	0	3	2	1	3	0101000020E610000047ACC5A7804D5E40CD201455EC473F40
0	329698.88	3.4621202e+06	1.9189591	0	2	3	0	3	2	1	3	0101000020E6100000681BD3A1804D5E401ED4B7A8EC473F40
0	329698.75	3.4621208e+06	1.8714417	0	2	3	0	3	2	1	3	0101000020E6100000E8FA699C804D5E404F4217FBEC473F40
0	329698.62	3.4621215e+06	1.83797	0	2	3	0	3	2	1	3	0101000020E61000001935BD96804D5E40DC97F351ED473F40
0	329698.53	3.462122e+06	1.8213828	0	2	3	0	3	2	1	3	0101000020E6100000A5426591804D5E404693E29FED473F40
0	329698.4	3.4621225e+06	1.8070316	0	2	3	0	3	2	1	3	0101000020E61000008F170A8C804D5E408930C7F0ED473F40
0	329698.3	3.462123e+06	1.787597	0	2	3	0	3	2	1	3	0101000020E6100000A2BCA387804D5E400781893EEE473F40
0	329698.25	3.4621235e+06	1.7618375	0	2	3	0	3	2	1	3	0101000020E6100000A8E81084804D5E40DCC9CB8DEE473F40
0	329698.2	3.462124e+06	1.7297914	0	2	3	0	3	2	1	3	0101000020E61000005B307781804D5E4057E2C8DEEE473F40
0	329698.2	3.4621245e+06	1.6886585	0	2	3	0	3	2	1	3	0101000020E6100000722E2880804D5E40A6A1C432EF473F40
0	329698.2	3.4621252e+06	1.6443529	0	2	3	0	3	2	1	3	0101000020E6100000722E2880804D5E40177ADB84EF473F40
0	329698.2	3.4621258e+06	1.6144253	0	2	3	0	3	2	1	3	0101000020E61000005F6F3A80804D5E40A5CE87D6EF473F40
4	329698.2	3.4621258e+06	1.6115699	0	2	3	2	3	2	1	3	0101000020E6100000228F1080804D5E4056D762EDEF473F40
4	329698.2	3.4621262e+06	1.6061472	0	0	3	0	3	2	1	3	0101000020E6100000A81F047F804D5E408DC2763BF0473F40
4	329698.16	3.462127e+06	1.601716	0	0	3	0	3	2	1	3	0101000020E61000005F62107E804D5E409F31B190F0473F40
4	329698.16	3.4621275e+06	1.5972211	0	0	3	0	3	2	1	3	0101000020E610000003E62E7D804D5E403CCF94E8F0473F40
4	329698.16	3.4621282e+06	1.5919838	0	0	3	0	3	2	1	3	0101000020E61000005C613E7C804D5E408F8E1953F1473F40
4	329698.16	3.4621288e+06	1.5883116	0	0	3	0	3	2	1	3	0101000020E61000002F9DA17B804D5E40CE624FA2F1473F40
4	329698.12	3.4621295e+06	1.5837007	0	0	3	0	3	2	1	3	0101000020E61000003585F17A804D5E40E3402909F2473F40
4	329698.12	3.4621302e+06	1.5792103	0	0	3	0	3	2	1	3	0101000020E6100000FD92437A804D5E40D4DC4E7EF2473F40
4	329698.12	3.462131e+06	1.5755264	0	0	3	0	3	2	1	3	0101000020E6100000789EB279804D5E40A35E29F0F2473F40
4	329698.12	3.4621318e+06	1.5724623	0	0	3	0	3	2	1	3	0101000020E610000058082779804D5E40372E566AF3473F40
4	329698.12	3.4621325e+06	1.5709516	0	0	3	0	3	2	1	3	0101000020E610000031DB9278804D5E405F28F5E8F3473F40
4	329698.12	3.4621335e+06	1.5709187	0	0	3	0	3	2	1	3	0101000020E610000081CBF177804D5E40DDB0A86AF4473F40
4	329698.12	3.4621342e+06	1.5714451	0	0	3	0	3	2	1	3	0101000020E610000043423B77804D5E4026AFA0F9F4473F40
4	329698.12	3.4621355e+06	1.5721205	0	0	3	0	3	2	1	3	0101000020E6100000753F6F76804D5E40367FAA93F5473F40
4	329698.12	3.462136e+06	1.5724671	0	0	3	0	3	2	1	3	0101000020E61000006C050676804D5E409CD738E1F5473F40
4	329698.12	3.462137e+06	1.5729374	0	0	3	0	3	2	1	3	0101000020E61000001CB73575804D5E40A3220F7AF6473F40
4	329698.12	3.4621375e+06	1.5729451	0	0	3	0	3	2	1	3	0101000020E610000096C8D074804D5E40EC2901C6F6473F40
4	329698.12	3.462138e+06	1.5727382	0	0	3	0	3	2	1	3	0101000020E6100000EB556474804D5E406F5DC619F7473F40
4	329698.12	3.4621385e+06	1.572308	0	0	3	0	3	2	1	3	0101000020E610000040E3F773804D5E4017CB4970F7473F40
4	329698.12	3.4621395e+06	1.5710964	0	0	3	0	3	2	1	3	0101000020E610000065B84673804D5E40B490A907F8473F40
4	329698.12	3.46214e+06	1.5702552	0	0	3	0	3	2	1	3	0101000020E6100000CC0AF472804D5E40FF21D854F8473F40
4	329698.12	3.4621405e+06	1.5692645	0	0	3	0	3	2	1	3	0101000020E610000077CEA772804D5E40FC5261A2F8473F40
4	329698.12	3.462141e+06	1.5680931	0	0	3	0	3	2	1	3	0101000020E610000066036272804D5E40196540F1F8473F40
4	329698.12	3.4621415e+06	1.566814	0	0	3	0	3	2	1	3	0101000020E61000003DE22572804D5E40ACBF0B3EF9473F40
4	329698.16	3.4621425e+06	1.5642484	0	0	3	0	3	2	1	3	0101000020E610000001FCCF71804D5E409113B3C8F9473F40
4	329698.16	3.4621435e+06	1.5614506	0	0	3	0	3	2	1	3	0101000020E6100000B8ED9471804D5E40ED3AB357FA473F40
4	329698.16	3.4621442e+06	1.5589515	0	0	3	0	3	2	1	3	0101000020E6100000A6287B71804D5E4054FCA8D5FA473F40
4	329698.2	3.462145e+06	1.5565265	0	0	3	0	3	2	1	3	0101000020E6100000C6157A71804D5E40CB04764FFB473F40
4	329698.2	3.4621458e+06	1.554322	0	0	3	0	3	2	1	3	0101000020E6100000B2568C71804D5E40BE035ABFFB473F40
4	329698.22	3.4621465e+06	1.5525024	0	0	3	0	3	2	1	3	0101000020E6100000287AAB71804D5E4021F19126FC473F40
4	329698.22	3.4621472e+06	1.5513991	0	0	3	0	3	2	1	3	0101000020E61000009E9DCA71804D5E404DA02187FC473F40
4	329698.22	3.4621478e+06	1.5512071	0	0	3	0	3	2	1	3	0101000020E61000000D2AE171804D5E40B4A41DE3FC473F40
4	329698.25	3.4621482e+06	1.5529073	0	0	3	0	3	2	1	3	0101000020E61000002680D771804D5E400973E73CFD473F40
4	329698.25	3.4621488e+06	1.5562553	0	0	3	0	3	2	1	3	0101000020E6100000A52EA771804D5E4032C67E8CFD473F40
4	329698.25	3.4621495e+06	1.5629526	0	0	3	0	3	2	1	3	0101000020E6100000C9092271804D5E4045998EFBFD473F40
4	329698.25	3.4621502e+06	1.569559	0	0	3	0	3	2	1	3	0101000020E61000008A806B70804D5E409732C564FE473F40
4	329698.22	3.462151e+06	1.5739175	0	0	3	0	3	2	1	3	0101000020E6100000EE2FB86F804D5E40FBA08AC2FE473F40
4	329698.22	3.4621515e+06	1.5768905	0	0	3	0	3	2	1	3	0101000020E610000058760D6F804D5E40BF572B15FF473F40
4	329698.22	3.462152e+06	1.5786659	0	0	3	0	3	2	1	3	0101000020E61000004A9F6F6E804D5E40E2F3E861FF473F40
4	329698.22	3.4621525e+06	1.5794219	0	0	3	0	3	2	1	3	0101000020E610000089CAB46D804D5E4055A438C2FF473F40
4	329698.22	3.462153e+06	1.5795491	0	0	3	0	3	2	1	3	0101000020E6100000C6FB256D804D5E40CE636F0E00483F40
4	329698.2	3.4621538e+06	1.5810163	0	0	3	0	3	2	1	3	0101000020E6100000CCE3756C804D5E40CEA8715E00483F40
4	329698.2	3.4621542e+06	1.584502	0	0	3	0	3	2	1	3	0101000020E61000006F67946B804D5E40264465B100483F40
4	329698.2	3.4621548e+06	1.5894061	0	0	3	0	3	2	1	3	0101000020E6100000A558706A804D5E40BFE1A60C01483F40
4	329698.16	3.4621555e+06	1.5950875	0	0	3	0	3	2	1	3	0101000020E610000028460369804D5E40E21FDA7101483F40
4	329698.12	3.4621562e+06	1.601436	0	0	3	0	3	2	1	3	0101000020E61000006F4D4067804D5E40A90F57E001483F40
4	329698.1	3.4621568e+06	1.605866	0	0	3	0	3	2	1	3	0101000020E610000043DAEA65804D5E400847A02C02483F40
4	329698.06	3.4621572e+06	1.6105243	0	0	3	0	3	2	1	3	0101000020E6100000374E6864804D5E408DB22A7D02483F40
4	329698.06	3.4621578e+06	1.6153331	0	0	3	0	3	2	1	3	0101000020E6100000E84AB362804D5E40CF3D98D202483F40
4	329698	3.4621585e+06	1.619679	0	0	3	0	3	2	1	3	0101000020E6100000F271C660804D5E40CF6CA82F03483F40
4	329697.97	3.462159e+06	1.6225652	0	0	3	0	3	2	1	3	0101000020E610000009C7EA5E804D5E408BD83A8803483F40
4	329697.94	3.4621595e+06	1.6239434	0	0	3	0	3	2	1	3	0101000020E6100000B526015D804D5E403A0445E503483F40
4	329697.9	3.4621602e+06	1.621949	0	0	3	0	3	2	1	3	0101000020E6100000BE53405B804D5E40721F864404483F40
4	329697.9	3.4621608e+06	1.6169055	0	0	3	0	3	2	1	3	0101000020E61000005500C159804D5E40C76B92A204483F40
4	329697.88	3.4621615e+06	1.6112881	0	0	3	0	3	2	1	3	0101000020E610000021FC8E58804D5E40FA3835F804483F40
4	329697.88	3.462162e+06	1.6054202	0	0	3	0	3	2	1	3	0101000020E610000081087B57804D5E405365125005483F40
4	329697.84	3.4621625e+06	1.6006204	0	0	3	0	3	2	1	3	0101000020E610000044799856804D5E40C36822A005483F40
4	329697.84	3.462163e+06	1.5969867	0	0	3	0	3	2	1	3	0101000020E6100000D53DC955804D5E40AB3131ED05483F40
4	329697.8	3.4621638e+06	1.5931115	0	0	3	0	3	2	1	3	0101000020E610000099A8BA54804D5E401BCBBC5806483F40
4	329697.8	3.4621645e+06	1.590266	0	0	3	0	3	2	1	3	0101000020E61000003D2CD953804D5E402A8721B906483F40
4	329697.8	3.462165e+06	1.5881374	0	0	3	0	3	2	1	3	0101000020E6100000EBE33453804D5E40C559F60407483F40
4	329697.8	3.4621655e+06	1.5860355	0	0	3	0	3	2	1	3	0101000020E6100000BE1F9852804D5E407187795207483F40
4	329697.8	3.462166e+06	1.5834459	0	1	3	0	3	2	1	3	0101000020E610000059180652804D5E40B068D9A307483F40
4	329697.8	3.4621665e+06	1.580602	0	1	3	0	3	2	1	3	0101000020E6100000FAA77C51804D5E400424FCFC07483F40
4	329697.78	3.462167e+06	1.5781157	0	1	3	0	3	2	1	3	0101000020E6100000C6520351804D5E40F6D1B15308483F40
4	329697.78	3.4621675e+06	1.576627	0	1	3	0	3	2	1	3	0101000020E6100000DC059950804D5E40513ACF9F08483F40
4	329697.78	3.4621682e+06	1.5755047	0	1	3	0	3	2	1	3	0101000020E6100000A8B01F50804D5E40166FB2F608483F40
4	329697.78	3.4621688e+06	1.5742561	0	1	3	0	3	2	1	3	0101000020E61000000A66984F804D5E404DFC3B5D09483F40
4	329697.78	3.4621695e+06	1.5733072	0	1	3	0	3	2	1	3	0101000020E610000027B0364F804D5E408F1561AB09483F40
4	329697.78	3.46217e+06	1.5723144	0	1	3	0	3	2	1	3	0101000020E61000000420D74E804D5E40C66B10FD09483F40
4	329697.78	3.4621705e+06	1.5713226	0	1	3	0	3	2	1	3	0101000020E610000046EE7C4E804D5E4027ABD84E0A483F40
4	329697.78	3.462171e+06	1.570329	0	1	3	0	3	2	1	3	0101000020E6100000AC402A4E804D5E4050A7ABA00A483F40
4	329697.8	3.4621715e+06	1.5693411	0	1	3	0	3	2	1	3	0101000020E61000003817DF4D804D5E4035DA83EF0A483F40
4	329697.8	3.462172e+06	1.5682236	0	1	3	0	3	2	1	3	0101000020E61000004739984D804D5E40454E69410B483F40
4	329697.8	3.4621725e+06	1.5665573	0	1	3	0	3	2	1	3	0101000020E61000005CF2594D804D5E4078D4BC8D0B483F40
4	329697.8	3.4621732e+06	1.5656154	0	1	3	0	3	2	1	3	0101000020E610000013E41E4D804D5E40923943E60B483F40
4	329697.8	3.4621738e+06	1.5647309	0	1	3	0	3	2	1	3	0101000020E61000009292EE4C804D5E40FF225D350C483F40
4	329697.8	3.4621742e+06	1.5641336	0	1	3	0	3	2	1	3	0101000020E61000004984B34C804D5E40E29737940C483F40
4	329697.8	3.462175e+06	1.5638843	0	1	3	0	3	2	1	3	0101000020E61000000076784C804D5E40151B40EE0C483F40
4	329697.8	3.4621755e+06	1.5636185	0	1	3	0	3	2	1	3	0101000020E6100000F5413B4C804D5E4081B7744F0D483F40
4	329697.84	3.462176e+06	1.5634023	0	1	3	0	3	2	1	3	0101000020E610000093DD094C804D5E40751FAD9E0D483F40
4	329697.84	3.4621765e+06	1.5631788	0	1	3	0	3	2	1	3	0101000020E61000003179D84B804D5E406F76E2F00D483F40
4	329697.84	3.4621772e+06	1.5629535	0	1	3	0	3	2	1	3	0101000020E6100000AF27A84B804D5E40164DFF430E483F40
4	329697.84	3.4621778e+06	1.5627292	0	1	3	0	3	2	1	3	0101000020E61000000FE9784B804D5E40B954F0960E483F40
4	329697.84	3.4621782e+06	1.5624963	0	1	3	0	3	2	1	3	0101000020E61000006EAA494B804D5E40F5F70DED0E483F40
4	329697.84	3.4621788e+06	1.562264	0	1	3	0	3	2	1	3	0101000020E6100000AE7E1B4B804D5E40B806A3420F483F40
4	329697.84	3.4621795e+06	1.5620071	0	1	3	0	3	2	1	3	0101000020E61000002D2DEB4A804D5E401981C0A00F483F40
4	329697.88	3.46218e+06	1.5617417	0	1	3	0	3	2	1	3	0101000020E61000002E27BF4A804D5E403F59A5FB0F483F40
4	329697.88	3.4621805e+06	1.561504	0	1	3	0	3	2	1	3	0101000020E610000055A59A4A804D5E40FAAE5E4710483F40
4	329697.88	3.4621805e+06	1.561506	0	1	3	1	3	2	1	3	0101000020E610000055A59A4A804D5E4078DAEF4710483F40
0	329697.88	3.462181e+06	1.5654507	0	2	3	0	3	2	1	3	0101000020E610000090DC374A804D5E403963C39310483F40
0	329697.88	3.4621815e+06	1.564213	0	2	3	0	3	2	1	3	0101000020E6100000B1C30A4A804D5E40DD2567E410483F40
0	329697.88	3.4621822e+06	1.5589213	0	2	3	0	3	2	1	3	0101000020E61000002D7E324A804D5E40E1845F3511483F40
0	329697.9	3.4621828e+06	1.5494722	0	2	3	0	3	2	1	3	0101000020E610000053ABC64A804D5E40DD9CC48311483F40
0	329697.94	3.4621832e+06	1.535094	0	2	3	0	3	2	1	3	0101000020E61000002451F34B804D5E40D0C10FDB11483F40
0	329698	3.4621838e+06	1.5145332	0	2	3	0	3	2	1	3	0101000020E6100000FA3CE14D804D5E40EB05952B12483F40
0	329698.06	3.4621842e+06	1.4839618	0	2	3	0	3	2	1	3	0101000020E610000081E1FC50804D5E405C0F227A12483F40
0	329698.2	3.4621848e+06	1.4402076	0	2	3	0	3	2	1	3	0101000020E61000005F1AAA55804D5E40C12EBBC512483F40
0	329698.34	3.4621852e+06	1.3812817	0	2	3	0	3	2	1	3	0101000020E610000028A7BF5C804D5E40AC43661513483F40
0	329698.56	3.4621858e+06	1.3172642	0	2	3	0	3	2	1	3	0101000020E6100000E26F8D65804D5E4039C30B6513483F40
0	329698.8	3.4621862e+06	1.2506833	0	2	3	0	3	2	1	3	0101000020E610000098A85070804D5E40CD12AFB613483F40
0	329699.1	3.4621868e+06	1.1853694	0	2	3	0	3	2	1	3	0101000020E6100000E349777C804D5E40A2484A0514483F40
0	329699.4	3.4621872e+06	1.1246737	0	2	3	0	3	2	1	3	0101000020E610000068DD4B89804D5E4022FCCA4D14483F40
0	329699.75	3.4621878e+06	1.0612063	0	2	3	0	3	2	1	3	0101000020E6100000EF71F297804D5E406565079614483F40
0	329700.16	3.4621882e+06	0.98997915	0	2	3	0	3	2	1	3	0101000020E6100000BE272AA9804D5E40EAE7B3DE14483F40
0	329700.6	3.4621888e+06	0.9077664	0	2	3	0	3	2	1	3	0101000020E6100000D7A153BD804D5E40A97BAF2515483F40
0	329701.06	3.462189e+06	0.8262535	0	2	3	0	3	2	1	3	0101000020E61000005968FED0804D5E40E67B275E15483F40
0	329701.5	3.4621895e+06	0.74740046	0	2	3	0	3	2	1	3	0101000020E6100000BE3552E4804D5E4065E4F68B15483F40
0	329702.03	3.4621898e+06	0.65554315	0	2	3	0	3	2	1	3	0101000020E6100000C74FD1FB804D5E40E1E188B915483F40
0	329702.5	3.46219e+06	0.5849386	0	2	3	0	3	2	1	3	0101000020E610000001608610814D5E401BBDFBDB15483F40
0	329703.03	3.4621902e+06	0.5270706	0	2	3	0	3	2	1	3	0101000020E61000003F6BE126814D5E40DE8D22FF15483F40
0	329703.5	3.4621902e+06	0.48438835	0	2	3	0	3	2	1	3	0101000020E61000000E8CB43B814D5E4037B9011E16483F40
0	329704	3.4621905e+06	0.44246095	0	2	3	0	3	2	1	3	0101000020E61000008288EB50814D5E40AEE3053A16483F40
0	329704.53	3.4621908e+06	0.3845517	0	2	3	0	3	2	1	3	0101000020E6100000EFAF2869814D5E40BDF2FE5216483F40
0	329705.1	3.4621908e+06	0.3143075	0	2	3	0	3	2	1	3	0101000020E61000007EB80C81814D5E4092AFD66216483F40
0	329705.6	3.4621908e+06	0.24435285	0	2	3	0	3	2	1	3	0101000020E6100000D8BCBE97814D5E402EB7706B16483F40
0	329706.16	3.4621908e+06	0.17043933	0	2	3	0	3	2	1	3	0101000020E6100000C14DDCB0814D5E400678656F16483F40
0	329706.78	3.4621908e+06	0.09836444	0	2	3	0	3	2	1	3	0101000020E61000000F3ED1CB814D5E40B7F1716E16483F40
0	329707.4	3.4621908e+06	0.046078544	0	2	3	0	3	2	1	3	0101000020E6100000A6F1E5E7814D5E40EAF4226C16483F40
0	329708.03	3.4621908e+06	0.029334722	0	2	3	0	3	2	1	3	0101000020E61000007927B302824D5E4041EDA26D16483F40
0	329708.53	3.4621908e+06	0.028081434	0	2	3	0	3	2	1	3	0101000020E61000005E084619824D5E40A840D17016483F40
0	329709.06	3.4621908e+06	0.02970756	0	2	3	0	3	2	1	3	0101000020E6100000C3EFED30824D5E403595C87416483F40
10	329834.56	3.462202e+06	-3.1182487	0	0	3	2	3	2	1	3	0101000020E6100000206905C0974D5E4099CAC5511E483F40
10	329834	3.462202e+06	-3.1141875	0	0	3	0	3	2	1	3	0101000020E6100000372F5BA6974D5E40745E614D1E483F40
10	329833.47	3.462202e+06	-3.111232	0	0	3	0	3	2	1	3	0101000020E610000049C31790974D5E40652254491E483F40
10	329832.78	3.462202e+06	-3.1072574	0	0	3	0	3	2	1	3	0101000020E6100000BA93B571974D5E40219D76431E483F40
10	329832.06	3.462202e+06	-3.1038923	0	0	3	0	3	2	1	3	0101000020E61000005CB8FA51974D5E40B08B1F3D1E483F40
10	329831.5	3.462202e+06	-3.1023798	0	0	3	0	3	2	1	3	0101000020E6100000DC735E38974D5E40469B0D381E483F40
10	329830.8	3.462202e+06	-3.1032546	0	0	3	0	3	2	1	3	0101000020E610000042C5A31A974D5E40BF7FAD321E483F40
10	329830.16	3.462202e+06	-3.1067715	0	0	3	0	3	2	1	3	0101000020E6100000E1B123FE964D5E409B77372E1E483F40
10	329829.38	3.462202e+06	-3.1124794	0	0	3	0	3	2	1	3	0101000020E6100000F3A69CDB964D5E4091BF8B291E483F40
10	329828.44	3.462202e+06	-3.1205106	0	0	3	0	3	2	1	3	0101000020E6100000672A10B2964D5E4029460F251E483F40
10	329827.5	3.462202e+06	-3.1309114	0	0	3	0	3	2	1	3	0101000020E6100000C3516188964D5E40080748221E483F40
10	329826.6	3.462202e+06	3.1402001	0	0	3	0	3	2	1	3	0101000020E6100000760D4460964D5E40936667211E483F40
10	329825.6	3.462202e+06	3.12745	0	0	3	0	3	2	1	3	0101000020E6100000DE557C34964D5E40C02A04221E483F40
10	329825.03	3.462202e+06	3.121928	0	0	3	0	3	2	1	3	0101000020E6100000BF2AF61B964D5E4068DBBF221E483F40
10	329824.5	3.462202e+06	3.1179159	0	0	3	0	3	2	1	3	0101000020E6100000E3255B04964D5E404E9E9C231E483F40
10	329823.97	3.462202e+06	3.114894	0	0	3	0	3	2	1	3	0101000020E6100000EFC49DEC954D5E406B78A3241E483F40
10	329823.3	3.462202e+06	3.112094	0	0	3	0	3	2	1	3	0101000020E61000000B15D2D0954D5E40F41C10261E483F40
10	329822.72	3.462202e+06	3.1100254	0	0	3	0	3	2	1	3	0101000020E6100000EC2D69B5954D5E40FAAD9B271E483F40
10	329822.06	3.462202e+06	3.10908	0	0	3	0	3	2	1	3	0101000020E6100000F3152399954D5E40C75F20291E483F40
10	329821.47	3.462202e+06	3.1096153	0	0	3	0	3	2	1	3	0101000020E6100000901B257F954D5E408B734D2A1E483F40
10	329820.78	3.462202e+06	3.1118617	0	0	3	0	3	2	1	3	0101000020E6100000E5A12461954D5E40FF4B512B1E483F40
10	329820.1	3.462202e+06	3.1151505	0	0	3	0	3	2	1	3	0101000020E6100000295DDE42954D5E40638B062C1E483F40
10	329819.3	3.462202e+06	3.1193383	0	0	3	0	3	2	1	3	0101000020E6100000132BEF1F954D5E40725C782C1E483F40
10	329818.6	3.462202e+06	3.1231802	0	0	3	0	3	2	1	3	0101000020E6100000D7DF9300954D5E40F5A77C2C1E483F40
10	329817.94	3.462202e+06	3.1267447	0	0	3	0	3	2	1	3	0101000020E6100000509F7FE3944D5E40D2E5252C1E483F40
10	329817.28	3.4622022e+06	3.1301734	0	0	3	0	3	2	1	3	0101000020E610000049B6C7C6944D5E40A08A802B1E483F40
10	329816.66	3.4622022e+06	3.133397	0	0	3	0	3	2	1	3	0101000020E610000055EA6EAB944D5E40632D952A1E483F40
10	329816	3.4622022e+06	3.136957	0	0	3	0	3	2	1	3	0101000020E6100000F32D628E944D5E40E5B639291E483F40
10	329815.28	3.4622022e+06	3.1410513	0	0	3	0	3	2	1	3	0101000020E61000006ED4CB6E944D5E40AB3A4F271E483F40
10	329814.6	3.4622022e+06	-3.1380951	0	0	3	0	3	2	1	3	0101000020E610000070185350944D5E40D84209251E483F40
10	329813.94	3.462202e+06	-3.1343372	0	0	3	0	3	2	1	3	0101000020E61000005C018A33944D5E405E758B221E483F40
10	329813.25	3.462202e+06	-3.1307993	0	0	3	0	3	2	1	3	0101000020E61000002114A015944D5E40ED96AC1F1E483F40
10	329812.53	3.462202e+06	-3.1280348	0	0	3	0	3	2	1	3	0101000020E61000009A17A9F5934D5E401B1C791C1E483F40
10	329811.94	3.462202e+06	-3.1265464	0	0	3	0	3	2	1	3	0101000020E6100000CC279DDB934D5E403953C1191E483F40
10	329811.38	3.462202e+06	-3.1255279	0	0	3	0	3	2	1	3	0101000020E6100000C89D28C2934D5E409C5FFE161E483F40
10	329810.8	3.462202e+06	-3.1247826	0	0	3	0	3	2	1	3	0101000020E6100000E9F52CAA934D5E400B9A4C141E483F40
10	329810.28	3.462202e+06	-3.1240928	0	0	3	0	3	2	1	3	0101000020E61000007637E791934D5E40170C7B111E483F40
10	329809.78	3.462202e+06	-3.1233668	0	0	3	0	3	2	1	3	0101000020E6100000246D9E7B934D5E4053FECA0E1E483F40
10	329809.22	3.462202e+06	-3.1224236	0	0	3	0	3	2	1	3	0101000020E6100000EAA00664934D5E4054A6D60B1E483F40
10	329808.28	3.462202e+06	-3.120572	0	0	3	0	3	2	1	3	0101000020E6100000E075AA3A934D5E40B56677061E483F40
10	329807.47	3.462202e+06	-3.1192162	0	0	3	0	3	2	1	3	0101000020E6100000BB14D815934D5E40872498011E483F40
10	329806.72	3.462202e+06	-3.1185737	0	0	3	0	3	2	1	3	0101000020E6100000AC2FA8F5924D5E408DA14CFD1D483F40
10	329806.12	3.462202e+06	-3.1186311	0	0	3	0	3	2	1	3	0101000020E61000008760EFDA924D5E401CB0C6F91D483F40
10	329805.4	3.462202e+06	-3.1191976	0	0	3	0	3	2	1	3	0101000020E610000099C003BC924D5E40C89EC4F51D483F40
10	329804.8	3.462202e+06	-3.119766	0	0	3	0	3	2	1	3	0101000020E61000003F4B8AA1924D5E40FEE664F21D483F40
10	329804.3	3.462202e+06	-3.120241	0	0	3	0	3	2	1	3	0101000020E61000007CFA568B924D5E40DEA79DEF1D483F40
10	329803.8	3.462202e+06	-3.1211824	0	1	3	0	3	2	1	3	0101000020E6100000C234D474924D5E4041B4DAEC1D483F40
10	329803.25	3.462202e+06	-3.1205463	0	1	3	0	3	2	1	3	0101000020E6100000CDD9425D924D5E405FEB22EA1D483F40
10	329802.75	3.462202e+06	-3.1204197	0	1	3	0	3	2	1	3	0101000020E6100000A62A0A47924D5E400B9C80E71D483F40
10	329802.22	3.462202e+06	-3.1208692	0	1	3	0	3	2	1	3	0101000020E6100000060CC52F924D5E408135B4E41D483F40
10	329801.7	3.462202e+06	-3.121553	0	1	3	0	3	2	1	3	0101000020E6100000B140AA17924D5E401DE9D4E11D483F40
10	329801.1	3.462202e+06	-3.12232	0	1	3	0	3	2	1	3	0101000020E610000041189BFD914D5E40C15FC9DE1D483F40
10	329800.53	3.4622018e+06	-3.1230478	0	1	3	0	3	2	1	3	0101000020E6100000D63B79E4914D5E406AA5E9DB1D483F40
10	329799.94	3.4622018e+06	-3.1237798	0	1	3	0	3	2	1	3	0101000020E610000009FB25CB914D5E40DAA714D91D483F40
10	329799.3	3.4622018e+06	-3.1248896	0	1	3	0	3	2	1	3	0101000020E6100000C395E1AF914D5E4031EA2ED61D483F40
10	329798.78	3.4622018e+06	-3.1263552	0	1	3	0	3	2	1	3	0101000020E6100000AE9E9897914D5E406551CED31D483F40
10	329798.22	3.4622018e+06	-3.1279511	0	1	3	0	3	2	1	3	0101000020E610000034A0BD7E914D5E40D03386D11D483F40
10	329797.62	3.4622018e+06	-3.129885	0	1	3	0	3	2	1	3	0101000020E610000015C67E65914D5E40010B6CCF1D483F40
10	329797.03	3.4622018e+06	-3.1320975	0	1	3	0	3	2	1	3	0101000020E610000068BD454B914D5E40F33F77CD1D483F40
10	329796.44	3.4622018e+06	-3.134319	0	1	3	0	3	2	1	3	0101000020E6100000210DE630914D5E406D8FB2CB1D483F40
10	329795.8	3.4622018e+06	-3.1366675	0	1	3	0	3	2	1	3	0101000020E610000064DBF514914D5E40A0A50ACA1D483F40
10	329795.16	3.4622018e+06	-3.1390972	0	1	3	0	3	2	1	3	0101000020E6100000EE5FFBF7904D5E40C0668FC81D483F40
10	329794.62	3.4622018e+06	-3.1409976	0	1	3	0	3	2	1	3	0101000020E6100000F12246E1904D5E40B6B590C71D483F40
10	329793.94	3.4622018e+06	3.1396856	0	1	3	0	3	2	1	3	0101000020E6100000E44414C3904D5E40BFF576C61D483F40
10	329793.44	3.4622018e+06	3.1379082	0	1	3	0	3	2	1	3	0101000020E6100000C577FFAB904D5E401DDCC3C51D483F40
10	329792.88	3.4622018e+06	3.1361058	0	1	3	0	3	2	1	3	0101000020E61000008565A693904D5E40B23D29C51D483F40
10	329792.3	3.4622018e+06	3.1344244	0	1	3	0	3	2	1	3	0101000020E6100000527B327B904D5E40E841ACC41D483F40
10	329791.72	3.4622018e+06	3.132795	0	1	3	0	3	2	1	3	0101000020E61000007FF41D61904D5E40ADBF44C41D483F40
10	329791.1	3.4622018e+06	3.1310947	0	1	3	0	3	2	1	3	0101000020E6100000DDC1B045904D5E40962BFFC31D483F40
10	329790.5	3.462202e+06	3.1294172	0	1	3	0	3	2	1	3	0101000020E61000001317942A904D5E40DC64E2C31D483F40
10	329789.84	3.462202e+06	3.1276715	0	1	3	0	3	2	1	3	0101000020E6100000BC37510E904D5E407E6BEEC31D483F40
10	329789.16	3.462202e+06	3.1258237	0	1	3	0	3	2	1	3	0101000020E61000005CC65FF08F4D5E40B41E2AC41D483F40
10	329788.53	3.462202e+06	3.1241226	0	1	3	0	3	2	1	3	0101000020E61000009AF73AD48F4D5E409E9D8BC41D483F40
10	329787.9	3.462202e+06	3.123096	0	1	3	0	3	2	1	3	0101000020E610000054E33DB88F4D5E40D388EAC41D483F40
10	329787.22	3.462202e+06	3.123114	0	1	3	0	3	2	1	3	0101000020E61000004411649A8F4D5E40FDA926C51D483F40
10	329786.56	3.462202e+06	3.123427	0	1	3	0	3	2	1	3	0101000020E61000006B37647D8F4D5E4033C150C51D483F40
10	329785.94	3.462202e+06	3.123734	0	1	3	0	3	2	1	3	0101000020E6100000CE9BFF618F4D5E40A31B70C51D483F40
10	329785.4	3.462202e+06	3.1239815	0	1	3	0	3	2	1	3	0101000020E61000002F26474B8F4D5E40329585C51D483F40
10	329784.9	3.462202e+06	3.1240115	0	1	3	0	3	2	1	3	0101000020E61000006B2C87348F4D5E40EC5BA2C51D483F40
10	329784.88	3.462202e+06	3.1240144	0	1	3	1	3	2	1	3	0101000020E610000066E099338F4D5E40C6A5A3C51D483F40
0	329784.34	3.462202e+06	3.1252224	0	2	3	0	3	2	1	3	0101000020E610000004ED991B8F4D5E408D9AD1C51D483F40
0	329783.72	3.462202e+06	3.1249053	0	2	3	0	3	2	1	3	0101000020E61000009BFD21008F4D5E406D17EDC51D483F40
0	329783.03	3.462202e+06	3.1245964	0	2	3	0	3	2	1	3	0101000020E61000001F3C66E28E4D5E404C9408C61D483F40
0	329782.4	3.462202e+06	3.1251001	0	2	3	0	3	2	1	3	0101000020E61000004A5D0CC78E4D5E40F6F9F9C51D483F40
0	329781.7	3.462202e+06	3.1266148	0	2	3	0	3	2	1	3	0101000020E6100000D23187A78E4D5E40B9AFB5C51D483F40
0	329781.2	3.462202e+06	3.1279085	0	2	3	0	3	2	1	3	0101000020E61000005B347E908E4D5E40729B4EC51D483F40
0	329780.6	3.462202e+06	3.129462	0	2	3	0	3	2	1	3	0101000020E6100000B66BDA768E4D5E407DB6B8C41D483F40
0	329779.97	3.462202e+06	3.1311848	0	2	3	0	3	2	1	3	0101000020E6100000F157C65B8E4D5E4001B7F2C31D483F40
0	329779.38	3.462202e+06	3.1332092	0	2	3	0	3	2	1	3	0101000020E610000055BC61408E4D5E406728F0C21D483F40
0	329778.7	3.462202e+06	3.1359212	0	2	3	0	3	2	1	3	0101000020E61000007F2751228E4D5E40862198C11D483F40
0	329777.94	3.462202e+06	3.13888	0	2	3	0	3	2	1	3	0101000020E6100000B4B327028E4D5E408320C6BF1D483F40
0	329777.16	3.462202e+06	-3.1411538	0	2	3	0	3	2	1	3	0101000020E61000007CA091DF8D4D5E4083778ABD1D483F40
0	329776.3	3.462202e+06	-3.1379607	0	2	3	0	3	2	1	3	0101000020E61000005A3367BA8D4D5E40000CB5BA1D483F40
0	329775.5	3.462202e+06	-3.1352794	0	2	3	0	3	2	1	3	0101000020E6100000AF92E8958D4D5E40BD0A97B71D483F40
0	329774.56	3.462202e+06	-3.1327379	0	2	3	0	3	2	1	3	0101000020E6100000818C116D8D4D5E408CBBEBB31D483F40
0	329773.7	3.462202e+06	-3.1304317	0	2	3	0	3	2	1	3	0101000020E610000014B966468D4D5E400ACD28B01D483F40
0	329772.75	3.462202e+06	-3.1285148	0	2	3	0	3	2	1	3	0101000020E61000006DEC0F1D8D4D5E40296EDCAB1D483F40
0	329771.84	3.462202e+06	-3.127913	0	2	3	0	3	2	1	3	0101000020E61000002F7964F58C4D5E408459D4A71D483F40
0	329770.94	3.462202e+06	-3.1289139	0	2	3	0	3	2	1	3	0101000020E61000008DF8FACC8C4D5E409F3E03A41D483F40
0	329770	3.462202e+06	-3.1308672	0	2	3	0	3	2	1	3	0101000020E6100000F89F76A48C4D5E40D0B777A01D483F40
0	329769.1	3.462202e+06	-3.133032	0	2	3	0	3	2	1	3	0101000020E610000093AEEF7C8C4D5E408288489D1D483F40
0	329768.2	3.462202e+06	-3.135325	0	2	3	0	3	2	1	3	0101000020E610000046BB19548C4D5E403EBF4D9A1D483F40
0	329767.66	3.462202e+06	-3.13662	0	2	3	0	3	2	1	3	0101000020E610000063CE2E3D8C4D5E40BB79C6981D483F40
0	329766.78	3.462202e+06	-3.1388168	0	2	3	0	3	2	1	3	0101000020E6100000D2767C168C4D5E4034526C961D483F40
0	329765.88	3.462202e+06	-3.1411083	0	2	3	0	3	2	1	3	0101000020E6100000CF3A6EEE8B4D5E40EE6F4D941D483F40
0	329765	3.462202e+06	3.1398404	0	2	3	0	3	2	1	3	0101000020E61000000C31A3C78B4D5E40B9C28E921D483F40
0	329764.2	3.462202e+06	3.1378093	0	2	3	0	3	2	1	3	0101000020E61000005F4509A48B4D5E40B20538911D483F40
0	329763.4	3.462202e+06	3.1363764	0	2	3	0	3	2	1	3	0101000020E6100000986716828B4D5E4013A809901D483F40
0	329762.7	3.462202e+06	3.1361444	0	2	3	0	3	2	1	3	0101000020E6100000E1B2DA618B4D5E4088D7D18E1D483F40
0	329761.97	3.462202e+06	3.136877	0	2	3	0	3	2	1	3	0101000020E610000092F9D8418B4D5E4017F3758D1D483F40
0	329761.28	3.462202e+06	3.138163	0	2	3	0	3	2	1	3	0101000020E6100000066DD7238B4D5E40056CFC8B1D483F40
0	329760.62	3.462202e+06	3.139679	0	2	3	0	3	2	1	3	0101000020E6100000CED106078B4D5E4044B0658A1D483F40
0	329760.03	3.462202e+06	3.1410968	0	2	3	0	3	2	1	3	0101000020E6100000BC70F4EC8A4D5E400740D3881D483F40
0	329759.44	3.462202e+06	-3.1406677	0	2	3	0	3	2	1	3	0101000020E6100000CF93E9D28A4D5E40994F1F871D483F40
0	329758.88	3.462202e+06	-3.1392856	0	2	3	0	3	2	1	3	0101000020E610000042D620BA8A4D5E4095EA5E851D483F40
0	329758.3	3.462202e+06	-3.137717	0	2	3	0	3	2	1	3	0101000020E61000009DBC35A18A4D5E40E4B46F831D483F40
0	329757.8	3.462202e+06	-3.1360626	0	2	3	0	3	2	1	3	0101000020E61000008435E28A8A4D5E40533A88811D483F40
0	329757.06	3.462202e+06	-3.1332629	0	2	3	0	3	2	1	3	0101000020E6100000D217AF6A8A4D5E40BED1757E1D483F40
0	329756.38	3.462202e+06	-3.130248	0	2	3	0	3	2	1	3	0101000020E61000007E25164C8A4D5E40B5DB297B1D483F40
0	329755.66	3.462202e+06	-3.127056	0	2	3	0	3	2	1	3	0101000020E610000060CDE52C8A4D5E40BDCF73771D483F40
0	329755.16	3.462202e+06	-3.1245937	0	2	3	0	3	2	1	3	0101000020E61000002DF09B168A4D5E40465A8C741D483F40
9	329754.97	3.462202e+06	-3.123961	0	2	3	2	3	2	1	3	0101000020E61000003E87390E8A4D5E40569569731D483F40
9	329754.44	3.462202e+06	-3.1286995	0	0	3	0	3	2	1	3	0101000020E6100000632B2BF7894D5E40F536B5711D483F40
9	329753.88	3.462202e+06	-3.132918	0	0	3	0	3	2	1	3	0101000020E6100000EEC984DE894D5E40AF034F701D483F40
9	329753.34	3.462202e+06	-3.137057	0	0	3	0	3	2	1	3	0101000020E6100000A2891AC6894D5E4067DC406F1D483F40
9	329752.8	3.462202e+06	-3.1409626	0	0	3	0	3	2	1	3	0101000020E61000006B60E3AE894D5E40C5C28D6E1D483F40
9	329752.25	3.462202e+06	3.138169	0	0	3	0	3	2	1	3	0101000020E6100000F20A9596894D5E40EED0226E1D483F40
9	329751.66	3.462202e+06	3.1341536	0	0	3	0	3	2	1	3	0101000020E61000000DBF667C894D5E40E901F76D1D483F40
9	329750.94	3.462202e+06	3.1311073	0	0	3	0	3	2	1	3	0101000020E6100000066B135C894D5E40432CBF6D1D483F40
9	329750.2	3.462202e+06	3.133372	0	0	3	0	3	2	1	3	0101000020E610000006A89C3B894D5E4098A07F6C1D483F40
9	329749.7	3.462202e+06	3.1369455	0	0	3	0	3	2	1	3	0101000020E610000007711325894D5E4034B2116B1D483F40
9	329749.16	3.462202e+06	3.1407573	0	0	3	0	3	2	1	3	0101000020E6100000F12EAF0D894D5E40ECDB4A691D483F40
9	329748.44	3.462202e+06	-3.1383433	0	0	3	0	3	2	1	3	0101000020E61000007AFDFDED884D5E405AB2AA661D483F40
9	329747.72	3.462202e+06	-3.1332538	0	0	3	0	3	2	1	3	0101000020E610000065307ECE884D5E402C6A4E631D483F40
9	329747.1	3.462202e+06	-3.1287754	0	0	3	0	3	2	1	3	0101000020E6100000CFDADAB3884D5E40492A01601D483F40
9	329746.53	3.462202e+06	-3.125428	0	0	3	0	3	2	1	3	0101000020E61000006D38229B884D5E407EAAC45C1D483F40
9	329746.03	3.462202e+06	-3.1241217	0	0	3	0	3	2	1	3	0101000020E6100000E424B884884D5E409C45FB591D483F40
9	329745.44	3.462202e+06	-3.1217313	0	0	3	0	3	2	1	3	0101000020E610000005C1D969884D5E404C3B48561D483F40
9	329744.9	3.4622018e+06	-3.1201656	0	0	3	0	3	2	1	3	0101000020E6100000A719C752884D5E402ABD0E531D483F40
9	329744.28	3.4622018e+06	-3.1198256	0	0	3	0	3	2	1	3	0101000020E6100000AD651E38884D5E40D1EF874F1D483F40
9	329743.62	3.4622018e+06	-3.1202383	0	0	3	0	3	2	1	3	0101000020E6100000E00A771A884D5E4037DDC54B1D483F40
9	329743	3.4622018e+06	-3.1226077	0	0	3	0	3	2	1	3	0101000020E61000002D0DC4FE874D5E40B439CD481D483F40
9	329742.44	3.4622018e+06	-3.1252196	0	0	3	0	3	2	1	3	0101000020E6100000D747D5E6874D5E40648D8B461D483F40
9	329741.84	3.4622018e+06	-3.1281462	0	0	3	0	3	2	1	3	0101000020E6100000F24CEECB874D5E4001B841441D483F40
9	329741.2	3.4622018e+06	-3.130905	0	0	3	0	3	2	1	3	0101000020E6100000CE1998AF874D5E406C360B421D483F40
9	329740.66	3.4622018e+06	-3.1321864	0	0	3	0	3	2	1	3	0101000020E610000084317398874D5E4088542F401D483F40
9	329740.16	3.4622018e+06	-3.13234	0	0	3	0	3	2	1	3	0101000020E6100000889D4A82874D5E4016F93D3E1D483F40
9	329739.6	3.4622018e+06	-3.1308823	0	0	3	0	3	2	1	3	0101000020E610000042967768874D5E4091619D3B1D483F40
9	329738.97	3.4622018e+06	-3.1280758	0	0	3	0	3	2	1	3	0101000020E61000004E79D74D874D5E404E607F381D483F40
9	329738.38	3.4622018e+06	-3.1247916	0	0	3	0	3	2	1	3	0101000020E610000010542833874D5E4002C109351D483F40
9	329737.7	3.4622018e+06	-3.1206653	0	0	3	0	3	2	1	3	0101000020E610000038C54315874D5E40DCF0B6301D483F40
9	329736.97	3.4622018e+06	-3.1164746	0	0	3	0	3	2	1	3	0101000020E61000000E3F02F6864D5E408461D02B1D483F40
9	329736.22	3.4622018e+06	-3.1131203	0	0	3	0	3	2	1	3	0101000020E6100000B5A20AD5864D5E40D78F71261D483F40
9	329735.47	3.4622018e+06	-3.1106133	0	0	3	0	3	2	1	3	0101000020E61000004E2F75B3864D5E40C426C7201D483F40
9	329734.72	3.4622018e+06	-3.109222	0	0	3	0	3	2	1	3	0101000020E6100000C9CEE091864D5E40144E191B1D483F40
9	329733.9	3.4622018e+06	-3.1094742	0	0	3	0	3	2	1	3	0101000020E610000015526A6E864D5E40D6FB55151D483F40
9	329733.06	3.4622015e+06	-3.111451	0	0	3	0	3	2	1	3	0101000020E61000007ACD7849864D5E400009BB0F1D483F40
9	329732.2	3.4622015e+06	-3.114539	0	0	3	0	3	2	1	3	0101000020E610000083CCA523864D5E4092AD6B0A1D483F40
9	329731.34	3.4622015e+06	-3.118327	0	0	3	0	3	2	1	3	0101000020E6100000CD9978FD854D5E4039BA96051D483F40
9	329730.44	3.4622015e+06	-3.122734	0	0	3	0	3	2	1	3	0101000020E6100000D68B7BD5854D5E4046FA1E011D483F40
9	329729.44	3.4622015e+06	-3.1276448	0	0	3	0	3	2	1	3	0101000020E6100000344F2FAA854D5E40A3ADF3FC1C483F40
9	329728.53	3.4622015e+06	-3.1322575	0	0	3	0	3	2	1	3	0101000020E6100000611CAD81854D5E406E06B2F91C483F40
9	329728	3.4622015e+06	-3.1349266	0	0	3	0	3	2	1	3	0101000020E610000000D2396A854D5E40054918F81C483F40
9	329727.03	3.4622015e+06	-3.1397812	0	0	3	0	3	2	1	3	0101000020E610000027FB843F854D5E409D40B4F51C483F40
9	329726.53	3.4622015e+06	3.1407921	0	0	3	0	3	2	1	3	0101000020E610000018A86E29854D5E40226DB9F41C483F40
9	329725.62	3.4622015e+06	3.1359515	0	0	3	0	3	2	1	3	0101000020E610000057E9BE01854D5E40661C60F31C483F40
9	329724.8	3.4622015e+06	3.131715	0	0	3	0	3	2	1	3	0101000020E6100000837F49DE844D5E404D49A8F21C483F40
9	329724	3.4622015e+06	3.1275837	0	0	3	0	3	2	1	3	0101000020E6100000E6B568BA844D5E405B0773F21C483F40
9	329723.34	3.4622015e+06	3.1243892	0	0	3	0	3	2	1	3	0101000020E6100000CFAAF79D844D5E402198B2F21C483F40
9	329722.75	3.4622015e+06	3.1214857	0	0	3	0	3	2	1	3	0101000020E610000027903A83844D5E40F7C140F31C483F40
9	329722.22	3.4622015e+06	3.1191063	0	0	3	0	3	2	1	3	0101000020E61000005C059E6C844D5E401095F8F31C483F40
9	329721.6	3.4622018e+06	3.1162453	0	0	3	0	3	2	1	3	0101000020E610000088201851844D5E40AEF226F51C483F40
9	329721.03	3.4622018e+06	3.113541	0	0	3	0	3	2	1	3	0101000020E610000025261A37844D5E4012E194F61C483F40
9	329720.5	3.4622018e+06	3.1140945	0	1	3	0	3	2	1	3	0101000020E61000008B476A20844D5E40F383FEF71C483F40
9	329719.94	3.4622018e+06	3.1191974	0	1	3	0	3	2	1	3	0101000020E6100000876CAE07844D5E40B79DA8F71C483F40
9	329719.4	3.4622018e+06	3.1248457	0	1	3	0	3	2	1	3	0101000020E6100000538866EF834D5E4010B5C9F61C483F40
9	329718.88	3.4622018e+06	3.1296759	0	1	3	0	3	2	1	3	0101000020E61000007B2000D8834D5E403FABAEF51C483F40
9	329718.25	3.4622018e+06	3.1342595	0	1	3	0	3	2	1	3	0101000020E6100000B21E70BD834D5E40AAD830F41C483F40
9	329717.56	3.4622018e+06	3.1391034	0	1	3	0	3	2	1	3	0101000020E610000060CF379F834D5E40904319F21C483F40
9	329717.06	3.4622018e+06	-3.1406271	0	1	3	0	3	2	1	3	0101000020E6100000A9AC1589834D5E4081B047F01C483F40
9	329716.53	3.4622018e+06	-3.1369886	0	1	3	0	3	2	1	3	0101000020E610000046BF4171834D5E4011E50FEE1C483F40
9	329715.94	3.4622018e+06	-3.133195	0	1	3	0	3	2	1	3	0101000020E610000069AD7C57834D5E405A695FEB1C483F40
9	329715.4	3.4622018e+06	-3.129824	0	1	3	0	3	2	1	3	0101000020E610000022B9FF3F834D5E408432A7E81C483F40
9	329714.88	3.4622018e+06	-3.1266806	0	1	3	0	3	2	1	3	0101000020E6100000C16E8C28834D5E40C94BB9E51C483F40
9	329714.34	3.4622015e+06	-3.123824	0	1	3	0	3	2	1	3	0101000020E6100000122E8E11834D5E404E07A6E21C483F40
9	329713.62	3.4622015e+06	-3.1202593	0	1	3	0	3	2	1	3	0101000020E6100000ABC722F2824D5E40491E26DE1C483F40
9	329713	3.4622015e+06	-3.1175334	0	1	3	0	3	2	1	3	0101000020E6100000402F1ED6824D5E40CCEBE7D91C483F40
9	329712.44	3.4622015e+06	-3.1154032	0	1	3	0	3	2	1	3	0101000020E61000008D3E95BC824D5E40E8FCE1D51C483F40
9	329711.88	3.4622015e+06	-3.1137948	0	1	3	0	3	2	1	3	0101000020E610000019867BA4824D5E40D2FD00D21C483F40
9	329711.38	3.4622015e+06	-3.1126947	0	1	3	0	3	2	1	3	0101000020E6100000DC74F48D824D5E4049B058CE1C483F40
9	329710.78	3.4622015e+06	-3.1114817	0	1	3	0	3	2	1	3	0101000020E61000002AD5B273824D5E40366DFCC91C483F40
9	329710.22	3.4622015e+06	-3.1103754	0	1	3	0	3	2	1	3	0101000020E610000005C2DC5B824D5E404DE7EDC51C483F40
9	329709.62	3.4622015e+06	-3.1091383	0	1	3	0	3	2	1	3	0101000020E610000086C85B41824D5E40C07F4FC11C483F40
9	329709.12	3.4622015e+06	-3.1082077	0	1	3	0	3	2	1	3	0101000020E6100000AD15DA2A824D5E408B2955BD1C483F40
9	329709.1	3.4622015e+06	-3.1081436	0	1	3	1	3	2	1	3	0101000020E610000098FEA629824D5E40FD771CBD1C483F40
0	329708.56	3.4622015e+06	-3.1107976	0	2	3	0	3	2	1	3	0101000020E610000072A0B512824D5E40DC25AEB91C483F40
0	329707.97	3.4622015e+06	-3.11245	0	2	3	0	3	2	1	3	0101000020E61000007204BDF8814D5E40745BEAB51C483F40
0	329707.38	3.4622015e+06	-3.1109593	0	2	3	0	3	2	1	3	0101000020E61000002760B5DE814D5E4030347EB11C483F40
0	329706.8	3.4622015e+06	-3.1070821	0	2	3	0	3	2	1	3	0101000020E61000008E74DBC5814D5E40290C8CAC1C483F40
0	329706.16	3.4622012e+06	-3.101758	0	2	3	0	3	2	1	3	0101000020E610000002464BA9814D5E407EB73FA61C483F40
0	329705.56	3.4622012e+06	-3.0960276	0	2	3	0	3	2	1	3	0101000020E61000004A60488E814D5E40C4C9A49F1C483F40
0	329705	3.4622012e+06	-3.0886245	0	2	3	0	3	2	1	3	0101000020E61000007E773A76814D5E40E11EBC981C483F40
0	329704.5	3.4622012e+06	-3.0787148	0	2	3	0	3	2	1	3	0101000020E6100000A6BE8C5F814D5E40A1F7F1901C483F40
0	329703.94	3.4622012e+06	-3.0658374	0	2	3	0	3	2	1	3	0101000020E610000026D86147814D5E40152B35871C483F40
0	329703.38	3.462201e+06	-3.0488122	0	2	3	0	3	2	1	3	0101000020E61000007CD0FA2E814D5E408F4C457B1C483F40
0	329702.8	3.462201e+06	-3.0257475	0	2	3	0	3	2	1	3	0101000020E6100000CA8E2A16814D5E4024662B6C1C483F40
0	329702.3	3.462201e+06	-3.003636	0	2	3	0	3	2	1	3	0101000020E6100000626293FF804D5E4062D25B5C1C483F40
0	329701.72	3.4622008e+06	-2.9796224	0	2	3	0	3	2	1	3	0101000020E61000008456FAE5804D5E40FAC595481C483F40
0	329701.2	3.4622008e+06	-2.9575903	0	2	3	0	3	2	1	3	0101000020E61000008124CBCD804D5E4029E311341C483F40
0	329700.56	3.4622005e+06	-2.9357069	0	2	3	0	3	2	1	3	0101000020E6100000499624B3804D5E407EAFDF1B1C483F40
0	329700.03	3.4622005e+06	-2.9177961	0	2	3	0	3	2	1	3	0101000020E61000006C40429C804D5E40BB90A3051C483F40
0	329699.5	3.4622002e+06	-2.8987474	0	2	3	0	3	2	1	3	0101000020E6100000BBA7FE83804D5E40CDCC6BEC1B483F40
0	329698.9	3.4622e+06	-2.8781264	0	2	3	0	3	2	1	3	0101000020E61000004D287C6A804D5E404DCCECCF1B483F40
0	329698.3	3.4621998e+06	-2.8563116	0	2	3	0	3	2	1	3	0101000020E610000035306150804D5E40B5EB8BB01B483F40
0	329697.72	3.4621998e+06	-2.8323534	0	2	3	0	3	2	1	3	0101000020E610000093ACAC35804D5E40CF07C78D1B483F40
0	329697.06	3.4621995e+06	-2.8066232	0	2	3	0	3	2	1	3	0101000020E6100000DFB4251A804D5E400B6816671B483F40
0	329696.56	3.4621992e+06	-2.7858322	0	2	3	0	3	2	1	3	0101000020E610000034C01404804D5E406EBE06461B483F40
0	329696.03	3.462199e+06	-2.76345	0	2	3	0	3	2	1	3	0101000020E61000001F7884EC7F4D5E40D1FAA2201B483F40
0	329695.53	3.4621988e+06	-2.7419076	0	2	3	0	3	2	1	3	0101000020E61000006B490AD67F4D5E401E07E8FA1A483F40
0	329695	3.4621985e+06	-2.7204247	0	2	3	0	3	2	1	3	0101000020E610000005C6FFBF7F4D5E400C58D9D31A483F40
0	329694.5	3.4621982e+06	-2.6970954	0	2	3	0	3	2	1	3	0101000020E6100000F7BD04A97F4D5E40D248BEA81A483F40
0	329694	3.462198e+06	-2.6749184	0	2	3	0	3	2	1	3	0101000020E6100000C832D4937F4D5E405DB1BE7E1A483F40
0	329693.5	3.4621975e+06	-2.6504285	0	2	3	0	3	2	1	3	0101000020E6100000286CD47D7F4D5E405E5581501A483F40
0	329693	3.4621972e+06	-2.6242976	0	2	3	0	3	2	1	3	0101000020E6100000BFF421687F4D5E407FD3EC1F1A483F40
0	329692.44	3.462197e+06	-2.593536	0	2	3	0	3	2	1	3	0101000020E61000001D2D50507F4D5E401CA6E0E619483F40
0	329691.94	3.4621965e+06	-2.563449	0	2	3	0	3	2	1	3	0101000020E610000095C2723A7F4D5E40DCB1C0AE19483F40
0	329691.5	3.4621962e+06	-2.5334928	0	2	3	0	3	2	1	3	0101000020E61000002124AD267F4D5E40F9E83A7819483F40
0	329691.03	3.4621958e+06	-2.5008593	0	2	3	0	3	2	1	3	0101000020E61000000C47B8127F4D5E40F812363D19483F40
0	329690.6	3.4621955e+06	-2.4658194	0	2	3	0	3	2	1	3	0101000020E610000075CD77FF7E4D5E4052869AFF18483F40
0	329690.16	3.462195e+06	-2.4273903	0	2	3	0	3	2	1	3	0101000020E61000006D76D9EC7E4D5E40A467A1BE18483F40
0	329689.75	3.4621945e+06	-2.3859692	0	2	3	0	3	2	1	3	0101000020E6100000ACE886DB7E4D5E406ED0067C18483F40
0	329689.4	3.4621942e+06	-2.3422177	0	2	3	0	3	2	1	3	0101000020E610000036BB88CB7E4D5E40E0DCFD3718483F40
0	329689.06	3.4621938e+06	-2.297404	0	2	3	0	3	2	1	3	0101000020E610000027ED61BD7E4D5E40EDDBE5F417483F40
0	329688.72	3.4621932e+06	-2.2443402	0	2	3	0	3	2	1	3	0101000020E6100000C2E81AAF7E4D5E40FA5B5BA817483F40
0	329688.44	3.4621928e+06	-2.1911104	0	2	3	0	3	2	1	3	0101000020E61000006A19E3A27E4D5E40D5F1435D17483F40
0	329688.2	3.4621922e+06	-2.13734	0	2	3	0	3	2	1	3	0101000020E61000007BA356987E4D5E400CF1661217483F40
0	329688	3.4621918e+06	-2.0844114	0	2	3	0	3	2	1	3	0101000020E6100000BB43808F7E4D5E40F6B532C916483F40
0	329687.8	3.4621912e+06	-2.029026	0	2	3	0	3	2	1	3	0101000020E61000005DF793877E4D5E4076D5B67B16483F40
0	329687.66	3.4621908e+06	-1.9729096	0	2	3	0	3	2	1	3	0101000020E610000033AFD9807E4D5E406836D82B16483F40
0	329687.5	3.4621902e+06	-1.9173506	0	2	3	0	3	2	1	3	0101000020E61000001D847E7B7E4D5E404367A2DB15483F40
0	329687.4	3.4621895e+06	-1.8598708	0	2	3	0	3	2	1	3	0101000020E61000005A4A54777E4D5E4052D9BD8715483F40
0	329687.34	3.462189e+06	-1.8015445	0	2	3	0	3	2	1	3	0101000020E61000008B408A747E4D5E40F486F03115483F40
0	329687.28	3.4621885e+06	-1.747241	0	2	3	0	3	2	1	3	0101000020E6100000010638737E4D5E4022D4E4E014483F40
0	329687.28	3.462188e+06	-1.6937078	0	2	3	0	3	2	1	3	0101000020E6100000DD7B04737E4D5E406F20FE8C14483F40
0	329687.28	3.4621875e+06	-1.6471614	0	2	3	0	3	2	1	3	0101000020E61000007F63C0737E4D5E40212A0F3E14483F40
0	329687.3	3.462187e+06	-1.6057783	0	2	3	0	3	2	1	3	0101000020E6100000B80427757E4D5E400349A6EB13483F40
0	329687.34	3.4621862e+06	-1.576035	0	2	3	0	3	2	1	3	0101000020E6100000D0BEBA767E4D5E40BE929D9B13483F40
0	329687.38	3.4621858e+06	-1.5611387	0	2	3	0	3	2	1	3	0101000020E61000004ECBFB777E4D5E4089A6594813483F40
0	329687.34	3.4621852e+06	-1.5647253	0	2	3	0	3	2	1	3	0101000020E61000004BD753787E4D5E402935EBF412483F40
0	329687.34	3.4621848e+06	-1.5737535	0	2	3	0	3	2	1	3	0101000020E6100000BC5D3E787E4D5E40F9ECD9A312483F40
0	329687.34	3.4621842e+06	-1.5855792	0	2	3	0	3	2	1	3	0101000020E61000006384BD777E4D5E40F41CC95012483F40
0	329687.3	3.4621838e+06	-1.5962223	0	2	3	0	3	2	1	3	0101000020E610000088590C777E4D5E40F322910112483F40
0	329687.28	3.462183e+06	-1.6050924	0	2	3	0	3	2	1	3	0101000020E6100000516132767E4D5E4002D9C9AA11483F40
0	329687.25	3.4621825e+06	-1.6108414	0	2	3	0	3	2	1	3	0101000020E6100000783055757E4D5E40745D9F5211483F40
0	329687.22	3.462182e+06	-1.6144563	0	2	3	0	3	2	1	3	0101000020E61000000E8C8E747E4D5E4028C6F30611483F40
0	329687.2	3.4621815e+06	-1.617587	0	2	3	0	3	2	1	3	0101000020E610000087F49C737E4D5E40070805AD10483F40
0	329687.16	3.4621808e+06	-1.6198026	0	2	3	0	3	2	1	3	0101000020E6100000883FB8727E4D5E404A14385A10483F40
0	329687.12	3.4621802e+06	-1.6199273	0	2	3	0	3	2	1	3	0101000020E61000005147DE717E4D5E403E6D7C0110483F40
14	329687.12	3.4621798e+06	-1.6199863	0	2	3	2	3	2	1	3	0101000020E6100000059642717E4D5E40A3FB4AC30F483F40
14	329687.1	3.4621792e+06	-1.6160516	0	0	3	0	3	2	1	3	0101000020E6100000FE55AD707E4D5E40E3247B6A0F483F40
14	329687.06	3.4621788e+06	-1.6118718	0	0	3	0	3	2	1	3	0101000020E610000033F641707E4D5E409D4177150F483F40
14	329687.06	3.4621782e+06	-1.6076921	0	0	3	0	3	2	1	3	0101000020E61000004218FB6F7E4D5E4022D2B5C60E483F40
14	329687.03	3.4621775e+06	-1.6022724	0	0	3	0	3	2	1	3	0101000020E6100000BB2FC26F7E4D5E402DE8D8610E483F40
14	329687.03	3.4621768e+06	-1.5960248	0	0	3	0	3	2	1	3	0101000020E6100000AF01B16F7E4D5E4088CB57ED0D483F40
14	329687.03	3.4621762e+06	-1.5916526	0	0	3	0	3	2	1	3	0101000020E610000019F7BE6F7E4D5E4008BDB0950D483F40
14	329687	3.4621755e+06	-1.5873315	0	0	3	0	3	2	1	3	0101000020E6100000B39EE56F7E4D5E40AC6883350D483F40
14	329687	3.462175e+06	-1.5833724	0	0	3	0	3	2	1	3	0101000020E61000009FE523707E4D5E4070FC58DA0C483F40
14	329687	3.4621742e+06	-1.5787416	0	0	3	0	3	2	1	3	0101000020E6100000A71F8D707E4D5E405799ED6E0C483F40
14	329687	3.4621735e+06	-1.5742636	0	0	3	0	3	2	1	3	0101000020E6100000267D15717E4D5E4042C190060C483F40
14	329687	3.4621728e+06	-1.5684892	0	0	3	0	3	2	1	3	0101000020E610000082F9F6717E4D5E404B136B7F0B483F40
14	329687.03	3.462172e+06	-1.5638059	0	0	3	0	3	2	1	3	0101000020E6100000FE62D7727E4D5E402CAE5B100B483F40
14	329687.03	3.4621715e+06	-1.5607132	0	0	3	0	3	2	1	3	0101000020E6100000D98D88737E4D5E40E8CC59C20A483F40
14	329687.03	3.4621705e+06	-1.5559746	0	0	3	0	3	2	1	3	0101000020E61000001F57D4747E4D5E40CC8E833C0A483F40
14	329687.06	3.4621695e+06	-1.5517548	0	0	3	0	3	2	1	3	0101000020E61000007B826E767E4D5E40441EF7A409483F40
14	329687.06	3.462169e+06	-1.5499494	0	0	3	0	3	2	1	3	0101000020E6100000793753777E4D5E40096A345509483F40
14	329687.1	3.462168e+06	-1.5473782	0	0	3	0	3	2	1	3	0101000020E6100000206BFC787E4D5E40A986AFC608483F40
14	329687.1	3.4621675e+06	-1.54621	0	0	3	0	3	2	1	3	0101000020E6100000F20AFD797E4D5E40CDC4337308483F40
14	329687.12	3.462167e+06	-1.545385	0	0	3	0	3	2	1	3	0101000020E6100000F656EA7A7E4D5E40D0550A2708483F40
14	329687.16	3.462166e+06	-1.5445999	0	0	3	0	3	2	1	3	0101000020E610000069E4D27C7E4D5E40358F7A8A07483F40
14	329687.2	3.462165e+06	-1.5454628	0	0	3	0	3	2	1	3	0101000020E610000008879F7E7E4D5E4076C276F006483F40
14	329687.2	3.4621642e+06	-1.5467097	0	0	3	0	3	2	1	3	0101000020E6100000CEF88E7F7E4D5E40A3DCF29B06483F40
14	329687.2	3.4621635e+06	-1.5500042	0	0	3	0	3	2	1	3	0101000020E6100000F3DA07817E4D5E40F120610B06483F40
14	329687.22	3.4621625e+06	-1.5542372	0	0	3	0	3	2	1	3	0101000020E6100000C28660827E4D5E4003B0EF7605483F40
14	329687.22	3.4621615e+06	-1.5589327	0	0	3	0	3	2	1	3	0101000020E61000000A4A80837E4D5E40E7E57CE504483F40
14	329687.22	3.4621605e+06	-1.5640424	0	0	3	0	3	2	1	3	0101000020E6100000D74C4C847E4D5E40D720826004483F40
14	329687.22	3.4621598e+06	-1.5696049	0	0	3	0	3	2	1	3	0101000020E6100000D35ED0847E4D5E408C0DC8E303483F40
14	329687.22	3.462159e+06	-1.5755782	0	0	3	0	3	2	1	3	0101000020E610000008AE1D857E4D5E408F0FE96503483F40
14	329687.22	3.4621582e+06	-1.5808733	0	0	3	0	3	2	1	3	0101000020E6100000B61432857E4D5E405C0A3CF602483F40
14	329687.2	3.4621575e+06	-1.5856441	0	0	3	0	3	2	1	3	0101000020E61000008BF921857E4D5E406E893C8702483F40
14	329687.2	3.4621568e+06	-1.58902	0	0	3	0	3	2	1	3	0101000020E6100000938AFE847E4D5E402CB0D22402483F40
14	329687.16	3.4621562e+06	-1.5915229	0	0	3	0	3	2	1	3	0101000020E61000005013CC847E4D5E4084FACFBF01483F40
14	329687.16	3.4621555e+06	-1.5923371	0	0	3	0	3	2	1	3	0101000020E6100000967EA6847E4D5E40FE85966401483F40
14	329687.16	3.462155e+06	-1.5911467	0	0	3	0	3	2	1	3	0101000020E6100000AFD49C847E4D5E403CE7E90B01483F40
14	329687.12	3.4621545e+06	-1.5882285	0	0	3	0	3	2	1	3	0101000020E6100000FF73B4847E4D5E402B408CB600483F40
14	329687.12	3.4621538e+06	-1.5834898	0	0	3	0	3	2	1	3	0101000020E6100000103FFA847E4D5E4069A7754200483F40
14	329687.12	3.462153e+06	-1.579038	0	0	3	0	3	2	1	3	0101000020E6100000386662857E4D5E40E2C46ED7FF473F40
14	329687.12	3.4621522e+06	-1.5749178	0	0	3	0	3	2	1	3	0101000020E61000007252E4857E4D5E40759EFD74FF473F40
14	329687.12	3.4621518e+06	-1.5715187	0	0	3	0	3	2	1	3	0101000020E610000073FB70867E4D5E40572B7B18FF473F40
14	329687.12	3.462151e+06	-1.5693661	0	0	3	0	3	2	1	3	0101000020E61000005020F6867E4D5E40177514C5FE473F40
14	329687.12	3.4621505e+06	-1.5679537	0	0	3	0	3	2	1	3	0101000020E610000026AE72877E4D5E40B3BFE677FE473F40
14	329687.16	3.46215e+06	-1.5669801	0	0	3	0	3	2	1	3	0101000020E6100000EF130A887E4D5E40B4FA4119FE473F40
14	329687.16	3.4621495e+06	-1.5662099	0	0	3	0	3	2	1	3	0101000020E61000002F9794887E4D5E4057C845C6FD473F40
14	329687.16	3.462149e+06	-1.5654899	0	0	3	0	3	2	1	3	0101000020E61000002AA918897E4D5E40BDC3CA78FD473F40
14	329687.16	3.4621485e+06	-1.5647792	0	0	3	0	3	2	1	3	0101000020E6100000E7E09E897E4D5E4058918A2CFD473F40
14	329687.16	3.462148e+06	-1.5633329	0	0	3	0	3	2	1	3	0101000020E610000033923A8A7E4D5E408A059FDEFC473F40
14	329687.16	3.4621472e+06	-1.5600885	0	0	3	0	3	2	1	3	0101000020E610000032471F8B7E4D5E40854B807EFC473F40
14	329687.2	3.4621468e+06	-1.5573038	0	0	3	0	3	2	1	3	0101000020E610000044BBF18B7E4D5E4007EE932EFC473F40
14	329687.2	3.4621462e+06	-1.5540162	0	0	3	0	3	2	1	3	0101000020E61000004E9EE78C7E4D5E405ABE0EDAFB473F40
14	329687.22	3.4621455e+06	-1.5492859	0	0	3	0	3	2	1	3	0101000020E6100000E069168E7E4D5E40CA161982FB473F40
14	329687.22	3.462145e+06	-1.5431978	0	0	3	0	3	2	1	3	0101000020E610000076D2798F7E4D5E4089F5162BFB473F40
14	329687.25	3.4621445e+06	-1.5362054	0	0	3	0	3	2	1	3	0101000020E6100000DF2B25917E4D5E40984631CFFA473F40
14	329687.28	3.4621438e+06	-1.5302672	0	0	3	0	3	2	1	3	0101000020E610000070A60C937E4D5E405773446BFA473F40
14	329687.3	3.4621432e+06	-1.5271717	0	0	3	0	3	2	1	3	0101000020E6100000C1A395947E4D5E40FF93331BFA473F40
14	329687.34	3.4621425e+06	-1.5248669	0	0	3	0	3	2	1	3	0101000020E610000091F87A967E4D5E40D85BDBB7F9473F40
14	329687.38	3.462142e+06	-1.5251039	0	0	3	0	3	2	1	3	0101000020E6100000E0FB2F987E4D5E40E3D61C58F9473F40
14	329687.4	3.4621412e+06	-1.5282035	0	0	3	0	3	2	1	3	0101000020E610000045B27A997E4D5E4077D3AE04F9473F40
14	329687.4	3.4621408e+06	-1.5311229	0	1	3	0	3	2	1	3	0101000020E6100000E4A58E9A7E4D5E406A6748B8F8473F40
14	329687.44	3.4621402e+06	-1.5341918	0	1	3	0	3	2	1	3	0101000020E61000004B56AD9B7E4D5E406F997D65F8473F40
14	329687.44	3.4621398e+06	-1.537039	0	1	3	0	3	2	1	3	0101000020E61000005CD0AB9C7E4D5E405C389D17F8473F40
14	329687.47	3.4621392e+06	-1.5407056	0	1	3	0	3	2	1	3	0101000020E6100000320DE19D7E4D5E409AE705B2F7473F40
14	329687.47	3.4621385e+06	-1.5438949	0	1	3	0	3	2	1	3	0101000020E61000003CF0D69E7E4D5E40F0B7225AF7473F40
14	329687.5	3.4621378e+06	-1.5477742	0	1	3	0	3	2	1	3	0101000020E61000001427E09F7E4D5E40B44E43F1F6473F40
14	329687.5	3.4621372e+06	-1.550698	0	1	3	0	3	2	1	3	0101000020E610000053B096A07E4D5E40E069EFA1F6473F40
14	329687.5	3.4621368e+06	-1.5536585	0	1	3	0	3	2	1	3	0101000020E61000006CB545A17E4D5E407FDF5C4FF6473F40
14	329687.5	3.4621362e+06	-1.5564635	0	1	3	0	3	2	1	3	0101000020E61000009E10EBA17E4D5E4045C67BFAF5473F40
14	329687.5	3.4621358e+06	-1.5592378	0	1	3	0	3	2	1	3	0101000020E61000002F338DA27E4D5E40D0165C9FF5473F40
14	329687.53	3.462135e+06	-1.5620865	0	1	3	0	3	2	1	3	0101000020E6100000FE2F2DA37E4D5E40B55D5C3BF5473F40
14	329687.53	3.4621342e+06	-1.5647877	0	1	3	0	3	2	1	3	0101000020E6100000A7A8C5A37E4D5E406FF236D2F4473F40
14	329687.53	3.4621335e+06	-1.5672553	0	1	3	0	3	2	1	3	0101000020E610000007194FA47E4D5E40C7D71568F4473F40
14	329687.53	3.4621328e+06	-1.570032	0	1	3	0	3	2	1	3	0101000020E6100000479CD9A47E4D5E40AD447DEBF3473F40
14	329687.5	3.462132e+06	-1.5726185	0	1	3	0	3	2	1	3	0101000020E6100000F20E46A57E4D5E402A28F375F3473F40
14	329687.5	3.4621312e+06	-1.5753053	0	1	3	0	3	2	1	3	0101000020E6100000B040A0A57E4D5E40CE2426FBF2473F40
14	329687.5	3.4621302e+06	-1.5781426	0	1	3	0	3	2	1	3	0101000020E6100000A11EE7A57E4D5E40C47AF378F2473F40
14	329687.5	3.4621295e+06	-1.5810786	0	1	3	0	3	2	1	3	0101000020E6100000813714A67E4D5E4005ACD4F1F1473F40
14	329687.47	3.4621285e+06	-1.5837837	0	1	3	0	3	2	1	3	0101000020E6100000AC5224A67E4D5E40E3556875F1473F40
14	329687.47	3.4621278e+06	-1.5867743	0	1	3	0	3	2	1	3	0101000020E6100000C5A81AA67E4D5E404C8237EDF0473F40
14	329687.44	3.4621268e+06	-1.5897745	0	1	3	0	3	2	1	3	0101000020E6100000CD39F7A57E4D5E40C1BF786EF0473F40
14	329687.44	3.462126e+06	-1.5926926	0	1	3	0	3	2	1	3	0101000020E6100000E789C1A57E4D5E4048164DFBEF473F40
14	329687.4	3.4621255e+06	-1.5954969	0	1	3	0	3	2	1	3	0101000020E6100000547377A57E4D5E4073D3A090EF473F40
14	329687.4	3.4621248e+06	-1.5979177	0	1	3	0	3	2	1	3	0101000020E61000009BD825A57E4D5E409D36E535EF473F40
14	329687.38	3.4621242e+06	-1.6001735	0	1	3	0	3	2	1	3	0101000020E61000003B6EC8A47E4D5E40AC5D36E1EE473F40
14	329687.38	3.4621238e+06	-1.6021876	0	1	3	0	3	2	1	3	0101000020E610000076A565A47E4D5E40A0F1CF94EE473F40
14	329687.34	3.462123e+06	-1.6047271	0	1	3	0	3	2	1	3	0101000020E6100000308BD2A37E4D5E408EE35133EE473F40
14	329687.3	3.4621225e+06	-1.6071372	0	1	3	0	3	2	1	3	0101000020E6100000A06830A37E4D5E40E7DE09D6ED473F40
14	329687.28	3.462122e+06	-1.609281	0	1	3	0	3	2	1	3	0101000020E6100000F0588FA27E4D5E40A5AB9083ED473F40
14	329687.28	3.4621218e+06	-1.6098561	0	1	3	1	3	2	1	3	0101000020E6100000302D61A27E4D5E40246EA16DED473F40
0	329687.28	3.4621212e+06	-1.5976546	0	2	3	0	3	2	1	3	0101000020E610000010468EA27E4D5E4024022117ED473F40
0	329687.28	3.4621205e+06	-1.5859637	0	2	3	0	3	2	1	3	0101000020E6100000754D20A37E4D5E40230B34BAEC473F40
0	329687.28	3.46212e+06	-1.575433	0	2	3	0	3	2	1	3	0101000020E61000003CB9E3A37E4D5E40FB3B5C6DEC473F40
0	329687.3	3.4621195e+06	-1.5621978	0	2	3	0	3	2	1	3	0101000020E61000005DFE27A57E4D5E40CB540710EC473F40
0	329687.34	3.4621188e+06	-1.5478009	0	2	3	0	3	2	1	3	0101000020E610000054D1E8A67E4D5E402CD96DABEB473F40
0	329687.38	3.4621182e+06	-1.5356625	0	2	3	0	3	2	1	3	0101000020E610000064FA9FA87E4D5E406787EB5DEB473F40
0	329687.4	3.4621175e+06	-1.5182827	0	2	3	0	3	2	1	3	0101000020E610000047C357AB7E4D5E407D61B4FAEA473F40
0	329687.47	3.462117e+06	-1.4994102	0	2	3	0	3	2	1	3	0101000020E6100000A9E36BAE7E4D5E4070B47BA3EA473F40
0	329687.56	3.4621165e+06	-1.4773948	0	2	3	0	3	2	1	3	0101000020E6100000071604B27E4D5E409DE84B53EA473F40
0	329687.66	3.462116e+06	-1.4462044	0	2	3	0	3	2	1	3	0101000020E6100000766B27B77E4D5E409ED90CF9E9473F40
0	329687.78	3.4621155e+06	-1.4142329	0	2	3	0	3	2	1	3	0101000020E610000048257CBC7E4D5E40791EAEAEE9473F40
0	329687.94	3.462115e+06	-1.3744588	0	2	3	0	3	2	1	3	0101000020E6100000B5E468C37E4D5E403DA20E5FE9473F40
0	329688.1	3.4621145e+06	-1.3332844	0	2	3	0	3	2	1	3	0101000020E6100000AFD24FCB7E4D5E4048345911E9473F40
0	329688.28	3.4621138e+06	-1.28769	0	2	3	0	3	2	1	3	0101000020E6100000859474D47E4D5E40CAA5F6C4E8473F40
0	329688.53	3.4621135e+06	-1.2363628	0	2	3	0	3	2	1	3	0101000020E6100000C10CE4DE7E4D5E407AFB017CE8473F40
0	329688.78	3.462113e+06	-1.1811155	0	2	3	0	3	2	1	3	0101000020E6100000CCD938EB7E4D5E40D8C94730E8473F40
0	329689.06	3.4621125e+06	-1.1302494	0	2	3	0	3	2	1	3	0101000020E6100000D4B811F87E4D5E4057104AE9E7473F40
0	329689.34	3.462112e+06	-1.0860071	0	2	3	0	3	2	1	3	0101000020E6100000A6489D047F4D5E404B56CEA9E7473F40
0	329689.66	3.4621115e+06	-1.0421276	0	2	3	0	3	2	1	3	0101000020E61000000C47B8127F4D5E4083517667E7473F40
0	329690	3.462111e+06	-1.0040694	0	2	3	0	3	2	1	3	0101000020E6100000128A2E217F4D5E402D442127E7473F40
0	329690.38	3.4621105e+06	-0.96873784	0	2	3	0	3	2	1	3	0101000020E610000039705A327F4D5E4020949DDDE6473F40
0	329690.72	3.4621102e+06	-0.9378588	0	2	3	0	3	2	1	3	0101000020E610000033E330427F4D5E40CE042D9EE6473F40
0	329691.12	3.4621098e+06	-0.9029944	0	2	3	0	3	2	1	3	0101000020E6100000208C93537F4D5E406CC9C65DE6473F40
0	329691.5	3.4621092e+06	-0.87081635	0	2	3	0	3	2	1	3	0101000020E6100000CCB16B647F4D5E40AAE06623E6473F40
0	329691.9	3.462109e+06	-0.83759403	0	2	3	0	3	2	1	3	0101000020E6100000AAE7CD767F4D5E40A58C7DE7E5473F40
0	329692.28	3.4621085e+06	-0.8090229	0	2	3	0	3	2	1	3	0101000020E6100000949C88887F4D5E40184B5FB0E5473F40
0	329692.7	3.4621082e+06	-0.7845668	0	2	3	0	3	2	1	3	0101000020E610000088D6C7997F4D5E403B39B37CE5473F40
0	329693.1	3.4621078e+06	-0.7633237	0	2	3	0	3	2	1	3	0101000020E6100000AD2091AC7F4D5E4076188E45E5473F40
0	329693.6	3.4621075e+06	-0.74016285	0	2	3	0	3	2	1	3	0101000020E6100000BCC4EEC17F4D5E4046A36809E5473F40
0	329694.1	3.462107e+06	-0.7154778	0	2	3	0	3	2	1	3	0101000020E6100000627A3CD97F4D5E40FA91DCCAE4473F40
0	329694.5	3.4621068e+06	-0.69662875	0	2	3	0	3	2	1	3	0101000020E6100000168F62EB7F4D5E40F7923E9CE4473F40
0	329695	3.4621062e+06	-0.6747827	0	2	3	0	3	2	1	3	0101000020E61000002BCAC800804D5E40EC7D8367E4473F40
0	329695.44	3.462106e+06	-0.65433556	0	2	3	0	3	2	1	3	0101000020E610000076F60A15804D5E40E62CCC37E4473F40
0	329695.94	3.4621058e+06	-0.63229406	0	2	3	0	3	2	1	3	0101000020E61000009BFCB62A804D5E4002781D07E4473F40
0	329696.4	3.4621055e+06	-0.61021787	0	2	3	0	3	2	1	3	0101000020E61000002BF24440804D5E40EE621ED9E3473F40
0	329696.9	3.462105e+06	-0.58751017	0	2	3	0	3	2	1	3	0101000020E6100000C378AF55804D5E40E7FFEFADE3473F40
0	329697.34	3.4621048e+06	-0.56588614	0	2	3	0	3	2	1	3	0101000020E6100000501C246A804D5E40E346CF86E3473F40
0	329697.8	3.4621045e+06	-0.54532945	0	2	3	0	3	2	1	3	0101000020E61000008143B77D804D5E40673F4563E3473F40
0	329698.38	3.4621042e+06	-0.5182115	0	2	3	0	3	2	1	3	0101000020E61000002E9D3797804D5E401EFBC937E3473F40
0	329698.94	3.462104e+06	-0.4905538	0	2	3	0	3	2	1	3	0101000020E61000005A9F5BB0804D5E40318B2410E3473F40
0	329699.5	3.4621038e+06	-0.46338907	0	2	3	0	3	2	1	3	0101000020E61000004DB5FDC8804D5E40D2FF2FECE2473F40
0	329700.03	3.4621035e+06	-0.43753058	0	2	3	0	3	2	1	3	0101000020E6100000EED342E0804D5E4071FED5CCE2473F40
0	329700.53	3.4621035e+06	-0.41208047	0	2	3	0	3	2	1	3	0101000020E61000003276A6F6804D5E40F14821B1E2473F40
0	329701	3.4621032e+06	-0.3872018	0	2	3	0	3	2	1	3	0101000020E6100000E05E5F0C814D5E4017897598E2473F40
0	329701.53	3.462103e+06	-0.3606927	0	2	3	0	3	2	1	3	0101000020E6100000FE82E722814D5E40E02D4781E2473F40
0	329702.06	3.462103e+06	-0.3326597	0	2	3	0	3	2	1	3	0101000020E6100000539F493A814D5E40D94CBD6BE2473F40
0	329702.66	3.4621028e+06	-0.30064613	0	2	3	0	3	2	1	3	0101000020E61000006A976454814D5E40ED6AB656E2473F40
0	329703.12	3.4621028e+06	-0.27331036	0	2	3	0	3	2	1	3	0101000020E61000009634196A814D5E406E93B747E2473F40
0	329703.66	3.4621025e+06	-0.24210255	0	2	3	0	3	2	1	3	0101000020E6100000AD705181814D5E40D401833AE2473F40
0	329704.2	3.4621025e+06	-0.2075529	0	2	3	0	3	2	1	3	0101000020E6100000D47DFB98814D5E4020405530E2473F40
0	329704.75	3.4621025e+06	-0.16675848	0	2	3	0	3	2	1	3	0101000020E6100000206D1EB2814D5E40AB8B9D29E2473F40
0	329705.38	3.4621025e+06	-0.1186504	0	2	3	0	3	2	1	3	0101000020E6100000FFD0FCCC814D5E406D086427E2473F40
0	329706.03	3.4621025e+06	-0.064171195	0	2	3	0	3	2	1	3	0101000020E610000078EF57EA814D5E40D08C662AE2473F40
0	329706.72	3.4621025e+06	-0.013432701	0	2	3	0	3	2	1	3	0101000020E61000006FBC8207824D5E404B87DF31E2473F40
0	329707.44	3.4621025e+06	0.028675316	0	2	3	0	3	2	1	3	0101000020E6100000A17C2D27824D5E40F6BD5C3CE2473F40
0	329708.2	3.4621025e+06	0.048770472	0	2	3	0	3	2	1	3	0101000020E6100000514F4548824D5E4050DE2C46E2473F40
0	329708.7	3.4621025e+06	0.048773136	0	2	3	0	3	2	1	3	0101000020E61000007F8F5A5E824D5E40400EEB4AE2473F40
0	329709.2	3.4621025e+06	0.043619383	0	2	3	0	3	2	1	3	0101000020E6100000835DEC74824D5E40DABCBE4EE2473F40
0	329709.7	3.4621025e+06	0.035135083	0	2	3	0	3	2	1	3	0101000020E6100000B63A368B824D5E40850A5E51E2473F40
0	329710.25	3.4621025e+06	0.021171598	0	2	3	0	3	2	1	3	0101000020E610000076A4EBA3824D5E4095EE7652E2473F40
0	329710.8	3.4621025e+06	0.006056028	0	2	3	0	3	2	1	3	0101000020E6100000554CE7BB824D5E4079271752E2473F40
0	329711.47	3.4621025e+06	-0.008492226	0	2	3	0	3	2	1	3	0101000020E6100000BF99D0D8824D5E403E5AE550E2473F40
0	329712.06	3.4621025e+06	-0.01687351	0	2	3	0	3	2	1	3	0101000020E6100000B0C2C8F3824D5E4010BBC44FE2473F40
13	329712.47	3.4621025e+06	-0.020786488	0	2	3	2	3	2	1	3	0101000020E61000004C7BCC05834D5E4044E4C34EE2473F40
13	329713.06	3.4621025e+06	-0.024967244	0	0	3	0	3	2	1	3	0101000020E6100000EEA00F1F834D5E409D4C2C4DE2473F40
13	329713.66	3.4621025e+06	-0.029644957	0	0	3	0	3	2	1	3	0101000020E610000086F08639834D5E4031EC314BE2473F40
13	329714.22	3.4621025e+06	-0.033752233	0	0	3	0	3	2	1	3	0101000020E61000005EB65E52834D5E40FF960949E2473F40
13	329714.9	3.4621025e+06	-0.038325585	0	0	3	0	3	2	1	3	0101000020E6100000373F1770834D5E40381E1C46E2473F40
13	329715.4	3.4621025e+06	-0.04093108	0	0	3	0	3	2	1	3	0101000020E610000074FF5687834D5E40BC88C143E2473F40
13	329716	3.4621025e+06	-0.042708695	0	0	3	0	3	2	1	3	0101000020E6100000D3AD67A0834D5E400ADC3C41E2473F40
13	329716.6	3.4621022e+06	-0.043664422	0	0	3	0	3	2	1	3	0101000020E6100000BD96CABA834D5E40D647A23EE2473F40
13	329717.22	3.4621022e+06	-0.044234622	0	0	3	0	3	2	1	3	0101000020E6100000D78FB7D6834D5E405E0ADE3BE2473F40
13	329717.94	3.4621022e+06	-0.04471403	0	0	3	0	3	2	1	3	0101000020E61000006BBABFF6834D5E409921AA38E2473F40
13	329718.7	3.4621022e+06	-0.04457578	0	0	3	0	3	2	1	3	0101000020E610000059B81C17844D5E4063B28B35E2473F40
13	329719.47	3.4621022e+06	-0.043046895	0	0	3	0	3	2	1	3	0101000020E6100000B35B123A844D5E4032768732E2473F40
13	329720.22	3.4621022e+06	-0.040538788	0	0	3	0	3	2	1	3	0101000020E6100000FED5505B844D5E409F840A30E2473F40
13	329721.06	3.4621022e+06	-0.037245244	0	0	3	0	3	2	1	3	0101000020E61000004EA37A7F844D5E406FF7BE2DE2473F40
13	329721.9	3.4621022e+06	-0.033635058	0	0	3	0	3	2	1	3	0101000020E6100000260896A4844D5E40B7C6D82BE2473F40
13	329722.72	3.4621022e+06	-0.030319855	0	0	3	0	3	2	1	3	0101000020E6100000AA7B80C8844D5E407F89602AE2473F40
13	329723.6	3.462102e+06	-0.027786022	0	0	3	0	3	2	1	3	0101000020E61000009B9403EF844D5E402132FB28E2473F40
13	329724.44	3.462102e+06	-0.026492214	0	0	3	0	3	2	1	3	0101000020E6100000ED682B15854D5E405254AB27E2473F40
13	329725.28	3.462102e+06	-0.025765002	0	0	3	0	3	2	1	3	0101000020E61000005E7B993A854D5E4031AB7826E2473F40
13	329726.2	3.462102e+06	-0.025024163	0	0	3	0	3	2	1	3	0101000020E6100000DD135E61854D5E40C4315A25E2473F40
13	329727.03	3.462102e+06	-0.024549708	0	0	3	0	3	2	1	3	0101000020E6100000D6B79187854D5E4082054324E2473F40
13	329727.88	3.462102e+06	-0.024673631	0	0	3	0	3	2	1	3	0101000020E6100000A1F4C7AC854D5E4086AE2023E2473F40
13	329728.75	3.462102e+06	-0.024662474	0	0	3	0	3	2	1	3	0101000020E6100000100D62D2854D5E4038F00922E2473F40
13	329729.56	3.462102e+06	-0.024003364	0	0	3	0	3	2	1	3	0101000020E6100000DE3D40F7854D5E4097022221E2473F40
13	329730.44	3.462102e+06	-0.022588383	0	0	3	0	3	2	1	3	0101000020E61000009613CE1D864D5E40C2A07020E2473F40
13	329731.28	3.462102e+06	-0.019612215	0	0	3	0	3	2	1	3	0101000020E6100000CD39BA42864D5E4072015920E2473F40
13	329732.16	3.462102e+06	-0.015206818	0	0	3	0	3	2	1	3	0101000020E6100000928EA068864D5E40B84DE320E2473F40
13	329733	3.462102e+06	-0.0097083915	0	0	3	0	3	2	1	3	0101000020E6100000D397828E864D5E40C00A3A22E2473F40
13	329733.88	3.462102e+06	-0.003441852	0	0	3	0	3	2	1	3	0101000020E6100000100430B4864D5E404FF56724E2473F40
13	329734.72	3.462102e+06	0.002682336	0	0	3	0	3	2	1	3	0101000020E61000006BB44FD9864D5E4081F94827E2473F40
13	329735.56	3.462102e+06	0.0075079836	0	0	3	0	3	2	1	3	0101000020E6100000232C6CFE864D5E408AEF942AE2473F40
13	329736.34	3.462102e+06	0.010302171	0	0	3	0	3	2	1	3	0101000020E610000038648721874D5E403DAFC02DE2473F40
13	329737.06	3.462102e+06	0.011856393	0	0	3	0	3	2	1	3	0101000020E6100000EC756241874D5E4054C7C530E2473F40
13	329737.7	3.462102e+06	0.012770923	0	0	3	0	3	2	1	3	0101000020E6100000E6D8C35C874D5E401E6C7E33E2473F40
13	329738.34	3.462102e+06	0.013427658	0	0	3	0	3	2	1	3	0101000020E6100000CE25C478874D5E40A76E5C36E2473F40
13	329739.03	3.462102e+06	0.01403699	0	0	3	0	3	2	1	3	0101000020E61000006FC9F897874D5E40EE3EA639E2473F40
13	329739.6	3.462102e+06	0.014498782	0	0	3	0	3	2	1	3	0101000020E6100000ADE17DB0874D5E401CD8493CE2473F40
13	329740.16	3.462102e+06	0.01496004	0	0	3	0	3	2	1	3	0101000020E6100000524CB0C8874D5E40E6E0F03EE2473F40
13	329740.66	3.462102e+06	0.015383485	0	1	3	0	3	2	1	3	0101000020E610000066EBB3DF874D5E40604A8041E2473F40
13	329741.2	3.462102e+06	0.01579734	0	1	3	0	3	2	1	3	0101000020E61000007F7207F6874D5E402B1B0444E2473F40
13	329741.72	3.462102e+06	0.017496966	0	1	3	0	3	2	1	3	0101000020E6100000CEA6190E884D5E40AF4EB646E2473F40
13	329742.34	3.462102e+06	0.017968878	0	1	3	0	3	2	1	3	0101000020E610000080F51329884D5E407ACEF249E2473F40
13	329743.03	3.462102e+06	0.018488193	0	1	3	0	3	2	1	3	0101000020E61000000B884147884D5E40C5419D4DE2473F40
13	329743.66	3.462102e+06	0.018962242	0	1	3	0	3	2	1	3	0101000020E610000000FD2663884D5E40E6930B51E2473F40
13	329744.16	3.462102e+06	0.01933615	0	1	3	0	3	2	1	3	0101000020E610000071B46E79884D5E4044ADD053E2473F40
13	329744.7	3.462102e+06	0.019704731	0	1	3	0	3	2	1	3	0101000020E6100000C32D7090884D5E4038D7B356E2473F40
13	329745.25	3.462102e+06	0.019937487	0	1	3	0	3	2	1	3	0101000020E6100000FA63D1A9884D5E40F728DF59E2473F40
13	329745.84	3.462102e+06	0.01976811	0	1	3	0	3	2	1	3	0101000020E610000074C01DC4884D5E40E1C7115DE2473F40
13	329746.5	3.462102e+06	0.01919926	0	1	3	0	3	2	1	3	0101000020E6100000ADAC35E0884D5E402F2F6460E2473F40
13	329747.16	3.462102e+06	0.01845624	0	1	3	0	3	2	1	3	0101000020E61000004FECCCFD884D5E407537D163E2473F40
13	329747.84	3.4621022e+06	0.01766741	0	1	3	0	3	2	1	3	0101000020E61000008C82431C894D5E4025674367E2473F40
13	329748.66	3.4621022e+06	0.016758207	0	1	3	0	3	2	1	3	0101000020E6100000C7383A3F894D5E40DED01E6BE2473F40
13	329749.4	3.4621022e+06	0.015843678	0	1	3	0	3	2	1	3	0101000020E6100000E3A3C060894D5E407414B56EE2473F40
13	329750.16	3.4621022e+06	0.014800783	0	1	3	0	3	2	1	3	0101000020E6100000BB3FCF80894D5E4000560572E2473F40
13	329750.75	3.4621022e+06	0.013869742	0	1	3	0	3	2	1	3	0101000020E6100000D783D79B894D5E4021F9BA74E2473F40
13	329751.34	3.4621022e+06	0.013063869	0	1	3	0	3	2	1	3	0101000020E6100000CDEB92B5894D5E40BA814077E2473F40
13	329751.84	3.4621022e+06	0.01249928	0	1	3	0	3	2	1	3	0101000020E6100000A3AA6CCC894D5E40B3937379E2473F40
13	329752.44	3.4621022e+06	0.011955461	0	1	3	0	3	2	1	3	0101000020E6100000DF7D02E6894D5E409FE7DB7BE2473F40
13	329753	3.4621022e+06	0.011527758	0	1	3	0	3	2	1	3	0101000020E61000000DCB41FE894D5E4046921A7EE2473F40
13	329753.34	3.4621022e+06	0.011292868	0	1	3	1	3	2	1	3	0101000020E61000004B00660D8A4D5E40097A7C7FE2473F40
0	329753.9	3.4621022e+06	0.0077945394	0	2	3	0	3	2	1	3	0101000020E610000014A484268A4D5E4067D1E180E2473F40
0	329754.47	3.4621022e+06	0.004020308	0	2	3	0	3	2	1	3	0101000020E61000007B34B93E8A4D5E40D4AEEE81E2473F40
0	329755	3.4621022e+06	2.7164246e-05	0	2	3	0	3	2	1	3	0101000020E610000039F5E1568A4D5E4090ECA082E2473F40
0	329755.62	3.4621022e+06	-0.0043698982	0	2	3	0	3	2	1	3	0101000020E61000009413E8718A4D5E4028040E83E2473F40
0	329756.3	3.4621022e+06	-0.00895862	0	2	3	0	3	2	1	3	0101000020E61000005A3B24918A4D5E409DF53583E2473F40
0	329756.88	3.4621022e+06	-0.012164092	0	2	3	0	3	2	1	3	0101000020E6100000B2FAE6A88A4D5E4060E31483E2473F40
0	329757.44	3.4621022e+06	-0.015286269	0	2	3	0	3	2	1	3	0101000020E61000006AD3BFC18A4D5E40AE43BA82E2473F40
0	329758.06	3.4621022e+06	-0.018293759	0	2	3	0	3	2	1	3	0101000020E6100000C5A07EDD8A4D5E40C6F02382E2473F40
0	329758.7	3.4621022e+06	-0.020979648	0	2	3	0	3	2	1	3	0101000020E610000071B6E1F98A4D5E4095C14981E2473F40
0	329759.38	3.462102e+06	-0.02345424	0	2	3	0	3	2	1	3	0101000020E6100000BDBF58178B4D5E40EFA04780E2473F40
0	329760.1	3.462102e+06	-0.0257992	0	2	3	0	3	2	1	3	0101000020E610000048B0F7368B4D5E4058A2FE7EE2473F40
0	329760.88	3.462102e+06	-0.027936814	0	2	3	0	3	2	1	3	0101000020E61000007D202D598B4D5E40B00A677DE2473F40
0	329761.7	3.462102e+06	-0.029447569	0	2	3	0	3	2	1	3	0101000020E6100000CCF3827D8B4D5E40C62D947BE2473F40
0	329762.53	3.462102e+06	-0.030214807	0	2	3	0	3	2	1	3	0101000020E6100000BC0508A28B4D5E402D54C779E2473F40
0	329763.4	3.462102e+06	-0.030306602	0	2	3	0	3	2	1	3	0101000020E610000028886BC98B4D5E4095DEE877E2473F40
0	329764.34	3.462102e+06	-0.029671345	0	2	3	0	3	2	1	3	0101000020E61000004C5A05F28B4D5E40F0721C76E2473F40
0	329765.28	3.462102e+06	-0.028355869	0	2	3	0	3	2	1	3	0101000020E6100000AF5EE21B8C4D5E40A6D47874E2473F40
0	329766.25	3.462102e+06	-0.026345357	0	2	3	0	3	2	1	3	0101000020E610000034F94A468C4D5E409F172273E2473F40
0	329766.8	3.462102e+06	-0.024869056	0	2	3	0	3	2	1	3	0101000020E6100000ECD1235F8C4D5E40410B8772E2473F40
0	329767.78	3.462102e+06	-0.021950485	0	2	3	0	3	2	1	3	0101000020E61000001D88858A8C4D5E407F36CC71E2473F40
0	329768.38	3.4621018e+06	-0.020185921	0	2	3	0	3	2	1	3	0101000020E61000005F436BA38C4D5E40305F9171E2473F40
0	329768.88	3.4621018e+06	-0.018518317	0	2	3	0	3	2	1	3	0101000020E610000090D599BA8C4D5E40BA097B71E2473F40
0	329769.44	3.4621018e+06	-0.016801741	0	2	3	0	3	2	1	3	0101000020E6100000906468D28C4D5E408E588571E2473F40
0	329770	3.4621018e+06	-0.015007498	0	2	3	0	3	2	1	3	0101000020E6100000333338EC8C4D5E40DE93AE71E2473F40
0	329770.6	3.4621018e+06	-0.013422093	0	2	3	0	3	2	1	3	0101000020E6100000B81409068D4D5E403302F271E2473F40
0	329771.2	3.4621018e+06	-0.012114535	0	2	3	0	3	2	1	3	0101000020E61000007F6D0C208D4D5E4055C44872E2473F40
0	329771.8	3.4621018e+06	-0.011041479	0	2	3	0	3	2	1	3	0101000020E6100000F296ED3B8D4D5E40FA6DB572E2473F40
0	329772.44	3.4621018e+06	-0.010349568	0	2	3	0	3	2	1	3	0101000020E6100000234370578D4D5E40B2402A73E2473F40
0	329773.03	3.4621018e+06	-0.010041294	0	2	3	0	3	2	1	3	0101000020E61000004B5718718D4D5E40700E9673E2473F40
0	329773.7	3.4621018e+06	-0.0100122355	0	2	3	0	3	2	1	3	0101000020E61000009B4E0B8E8D4D5E40012B0C74E2473F40
0	329774.28	3.4621018e+06	-0.010266509	0	2	3	0	3	2	1	3	0101000020E6100000833D9AA88D4D5E40DEB37F74E2473F40
0	329774.9	3.4621018e+06	-0.010759866	0	2	3	0	3	2	1	3	0101000020E6100000038F60C48D4D5E4051B1FF74E2473F40
0	329775.6	3.4621018e+06	-0.011567347	0	2	3	0	3	2	1	3	0101000020E610000034480DE28D4D5E40869C5E75E2473F40
0	329776.2	3.4621018e+06	-0.012490619	0	2	3	0	3	2	1	3	0101000020E610000014FD32FC8D4D5E4084709375E2473F40
0	329776.78	3.4621018e+06	-0.013513155	0	2	3	0	3	2	1	3	0101000020E61000005F9B0E168E4D5E409CCCB575E2473F40
0	329777.4	3.4621018e+06	-0.014655715	0	2	3	0	3	2	1	3	0101000020E61000001B2472318E4D5E403ED3C175E2473F40
0	329778	3.4621018e+06	-0.015802834	0	2	3	0	3	2	1	3	0101000020E6100000E8137E4B8E4D5E40C719BD75E2473F40
0	329778.53	3.4621018e+06	-0.016884651	0	2	3	0	3	2	1	3	0101000020E6100000BD873C638E4D5E4096999B75E2473F40
0	329779.06	3.4621018e+06	-0.01802935	0	2	3	0	3	2	1	3	0101000020E6100000DEAC967B8E4D5E4034356A75E2473F40
0	329779.6	3.4621018e+06	-0.019115034	0	2	3	0	3	2	1	3	0101000020E61000002449CE918E4D5E40A0EC2875E2473F40
0	329780.56	3.4621018e+06	-0.021425545	0	2	3	0	3	2	1	3	0101000020E610000056F903BD8E4D5E40B9616F74E2473F40
0	329781.44	3.4621018e+06	-0.023022259	0	2	3	0	3	2	1	3	0101000020E6100000563440E38E4D5E402AD5B273E2473F40
0	329782.2	3.4621018e+06	-0.022444356	0	2	3	0	3	2	1	3	0101000020E61000003AAD18048F4D5E400DAA6473E2473F40
0	329782.8	3.4621018e+06	-0.020011367	0	2	3	0	3	2	1	3	0101000020E6100000B21CBB208F4D5E4045257D73E2473F40
0	329783.44	3.4621018e+06	-0.01613768	0	2	3	0	3	2	1	3	0101000020E6100000EBAA613B8F4D5E4008260374E2473F40
0	329784.06	3.4621018e+06	-0.01091753	0	2	3	0	3	2	1	3	0101000020E6100000A29690568F4D5E40BA741675E2473F40
0	329784.62	3.4621018e+06	-0.005637521	0	2	3	0	3	2	1	3	0101000020E6100000A089FC6F8F4D5E4087269B76E2473F40
0	329785.25	3.4621018e+06	0.0001310275	0	2	3	0	3	2	1	3	0101000020E6100000E057388B8F4D5E4099F8DE78E2473F40
12	329785.7	3.4621018e+06	0.0044283043	0	2	3	2	3	2	1	3	0101000020E610000091276F9E8F4D5E402AABE97AE2473F40
12	329786.28	3.4621018e+06	0.0074115773	0	0	3	0	3	2	1	3	0101000020E6100000FCB249B88F4D5E4062AAC17DE2473F40
12	329786.84	3.4621018e+06	0.011919774	0	0	3	0	3	2	1	3	0101000020E61000004B9614D18F4D5E407363E180E2473F40
12	329787.4	3.4621018e+06	0.016424777	0	0	3	0	3	2	1	3	0101000020E610000085C01DEA8F4D5E402FC16484E2473F40
12	329788.06	3.4621018e+06	0.021593973	0	0	3	0	3	2	1	3	0101000020E6100000FE2FC006904D5E40095DDD88E2473F40
12	329788.8	3.4621018e+06	0.027634555	0	0	3	0	3	2	1	3	0101000020E6100000E9E82D28904D5E403AB9B28EE2473F40
12	329789.38	3.4621018e+06	0.0321614	0	0	3	0	3	2	1	3	0101000020E610000058628441904D5E4055D28993E2473F40
12	329789.97	3.4621018e+06	0.036321256	0	0	3	0	3	2	1	3	0101000020E610000075481B5B904D5E401CF4B298E2473F40
12	329790.66	3.4621018e+06	0.040164206	0	0	3	0	3	2	1	3	0101000020E6100000D85C6D79904D5E40DF6CFE9EE2473F40
12	329791.34	3.4621018e+06	0.043246012	0	0	3	0	3	2	1	3	0101000020E610000032EC3A98904D5E4029389DA5E2473F40
12	329792.1	3.4621018e+06	0.045972023	0	0	3	0	3	2	1	3	0101000020E61000003DDDC2B8904D5E40EA33D6ACE2473F40
12	329792.84	3.462102e+06	0.04802905	0	0	3	0	3	2	1	3	0101000020E610000092DC85D9904D5E4072243DB4E2473F40
12	329793.66	3.462102e+06	0.04923014	0	0	3	0	3	2	1	3	0101000020E610000030A666FD904D5E4051C259BCE2473F40
12	329794.47	3.462102e+06	0.04903892	0	0	3	0	3	2	1	3	0101000020E6100000F49CDB21914D5E40CD3368C4E2473F40
12	329795.44	3.462102e+06	0.046836488	0	0	3	0	3	2	1	3	0101000020E6100000307A504B914D5E40DF9018CDE2473F40
12	329796.34	3.462102e+06	0.04288275	0	0	3	0	3	2	1	3	0101000020E61000005C865374914D5E4094A90AD5E2473F40
12	329796.88	3.462102e+06	0.040298954	0	0	3	0	3	2	1	3	0101000020E61000002474BB8A914D5E403F551BD9E2473F40
12	329797.84	3.462102e+06	0.03473934	0	0	3	0	3	2	1	3	0101000020E6100000746863B5914D5E406C4036E0E2473F40
12	329798.8	3.4621022e+06	0.028558677	0	0	3	0	3	2	1	3	0101000020E61000005EB916E1914D5E4002A49DE6E2473F40
12	329799.38	3.4621022e+06	0.025140772	0	0	3	0	3	2	1	3	0101000020E61000002951DDF9914D5E4024BEE8E9E2473F40
12	329799.94	3.4621022e+06	0.02198439	0	0	3	0	3	2	1	3	0101000020E610000049CDAA11924D5E40AD5CD8ECE2473F40
12	329800.47	3.4621022e+06	0.018896192	0	0	3	0	3	2	1	3	0101000020E6100000D7D58E29924D5E404BB489EFE2473F40
12	329801	3.4621022e+06	0.01590706	0	0	3	0	3	2	1	3	0101000020E6100000E0EF0D41924D5E40EB9BF4F1E2473F40
12	329801.56	3.4621022e+06	0.012928579	0	0	3	0	3	2	1	3	0101000020E61000000D437959924D5E40030541F4E2473F40
12	329802.16	3.4621022e+06	0.0099596875	0	0	3	0	3	2	1	3	0101000020E61000009A5EB373924D5E40216984F6E2473F40
12	329802.72	3.4621022e+06	0.0073615084	0	0	3	0	3	2	1	3	0101000020E61000006E360F8D924D5E40425D81F8E2473F40
12	329803.34	3.4621022e+06	0.0048613343	0	0	3	0	3	2	1	3	0101000020E610000011B497A7924D5E40764263FAE2473F40
12	329803.9	3.4621022e+06	0.0026365295	0	0	3	0	3	2	1	3	0101000020E61000000573C6C0924D5E406946F8FBE2473F40
12	329804.53	3.4621022e+06	0.00025619607	0	0	3	0	3	2	1	3	0101000020E610000044F6E6DC924D5E40236B86FDE2473F40
12	329805.12	3.4621022e+06	-0.0018725623	0	0	3	0	3	2	1	3	0101000020E61000005DE8D5F6924D5E403187C2FEE2473F40
12	329805.72	3.4621022e+06	-0.0040209973	0	0	3	0	3	2	1	3	0101000020E610000027E43911934D5E402847DCFFE2473F40
12	329806.28	3.4621022e+06	-0.0058967313	0	0	3	0	3	2	1	3	0101000020E6100000F912092A934D5E405EA9D000E3473F40
12	329806.88	3.4621022e+06	-0.0075601474	0	0	3	0	3	2	1	3	0101000020E610000063F55643934D5E403D71B601E3473F40
12	329807.44	3.4621022e+06	-0.00888721	0	0	3	0	3	2	1	3	0101000020E6100000E4840E5C934D5E408EBF8602E3473F40
12	329807.97	3.4621022e+06	-0.009959612	0	0	3	0	3	2	1	3	0101000020E6100000BBA15974934D5E4081DC3F03E3473F40
12	329808.53	3.4621022e+06	-0.010832376	0	0	3	0	3	2	1	3	0101000020E6100000009C028C934D5E40957CDD03E3473F40
12	329809.5	3.4621022e+06	-0.011964898	0	0	3	0	3	2	1	3	0101000020E610000049AE86B7934D5E40BE4CD204E3473F40
12	329810.38	3.4621022e+06	-0.011919764	0	0	3	0	3	2	1	3	0101000020E61000004F80CBDD934D5E408BEBAF05E3473F40
12	329811.25	3.4621022e+06	-0.010885599	0	0	3	0	3	2	1	3	0101000020E610000089F8D003944D5E40E703A306E3473F40
12	329811.9	3.4621022e+06	-0.009618733	0	0	3	0	3	2	1	3	0101000020E6100000EB639621944D5E40C72F7707E3473F40
12	329812.56	3.4621022e+06	-0.007978703	0	0	3	0	3	2	1	3	0101000020E61000001040793E944D5E4067B97008E3473F40
12	329813.12	3.4621022e+06	-0.006307397	0	0	3	0	3	2	1	3	0101000020E6100000A4944A57944D5E404CB47009E3473F40
12	329813.84	3.4621022e+06	-0.0040711365	0	0	3	0	3	2	1	3	0101000020E61000009C687376944D5E40E186EE0AE3473F40
12	329814.44	3.4621022e+06	-0.002199303	0	0	3	0	3	2	1	3	0101000020E610000022441890944D5E40D452600CE3473F40
12	329815	3.4621022e+06	-0.00033503142	0	0	3	0	3	2	1	3	0101000020E610000047B55FA9944D5E40695D010EE3473F40
12	329815.5	3.4621022e+06	0.0012735236	0	0	3	0	3	2	1	3	0101000020E61000001EC580BF944D5E40423D970FE3473F40
12	329816	3.4621022e+06	0.0019297266	0	1	3	0	3	2	1	3	0101000020E61000006361B8D5944D5E401C811B11E3473F40
12	329816.53	3.4621022e+06	0.000557666	0	1	3	0	3	2	1	3	0101000020E6100000C6A5FFEC944D5E408657D911E3473F40
12	329817.1	3.4621022e+06	-0.0019899404	0	1	3	0	3	2	1	3	0101000020E610000050C06705954D5E4073799B12E3473F40
12	329817.72	3.4621022e+06	-0.0047585107	0	1	3	0	3	2	1	3	0101000020E610000051B4A520954D5E4022893C13E3473F40
12	329818.28	3.4621022e+06	-0.007253151	0	1	3	0	3	2	1	3	0101000020E61000006EEB8339954D5E407D2A9A13E3473F40
12	329818.94	3.462102e+06	-0.010118751	0	1	3	0	3	2	1	3	0101000020E6100000F7256C56954D5E40431FC813E3473F40
12	329819.62	3.462102e+06	-0.013034587	0	1	3	0	3	2	1	3	0101000020E6100000B7B69F75954D5E4005A9B813E3473F40
12	329820.4	3.462102e+06	-0.015627867	0	1	3	0	3	2	1	3	0101000020E61000005C043397954D5E40A4A87513E3473F40
12	329821.12	3.462102e+06	-0.017572494	0	1	3	0	3	2	1	3	0101000020E61000007CAE7CB7954D5E4037DE0F13E3473F40
12	329821.84	3.462102e+06	-0.018622538	0	1	3	0	3	2	1	3	0101000020E6100000CA0F39D6954D5E407FA7AC12E3473F40
12	329822.47	3.462102e+06	-0.019242832	0	1	3	0	3	2	1	3	0101000020E6100000A140ACF2954D5E40E0944812E3473F40
12	329823.06	3.462102e+06	-0.019801266	0	1	3	0	3	2	1	3	0101000020E61000008A2F3B0D964D5E4022C7DC11E3473F40
12	329823.62	3.462102e+06	-0.020195618	0	1	3	0	3	2	1	3	0101000020E61000008ABE0925964D5E408F467811E3473F40
12	329824.12	3.462102e+06	-0.020208305	0	1	3	0	3	2	1	3	0101000020E6100000D154153B964D5E408C3F2911E3473F40
12	329824.75	3.462102e+06	-0.020056354	0	1	3	0	3	2	1	3	0101000020E6100000E6B6F956964D5E400CE8CC10E3473F40
12	329825.3	3.462102e+06	-0.01991021	0	1	3	0	3	2	1	3	0101000020E61000007CFF726F964D5E40CA068010E3473F40
12	329825.84	3.462102e+06	-0.01933321	0	1	3	0	3	2	1	3	0101000020E610000024AF9486964D5E407ACB5610E3473F40
12	329826.44	3.462102e+06	-0.017121693	0	1	3	0	3	2	1	3	0101000020E61000007F1EE2A0964D5E4027389710E3473F40
12	329827	3.462102e+06	-0.014590333	0	1	3	0	3	2	1	3	0101000020E61000000B918FBA964D5E409FCC1F11E3473F40
12	329827.62	3.462102e+06	-0.011993999	0	1	3	0	3	2	1	3	0101000020E6100000E700C6D5964D5E409285EA11E3473F40
12	329828.16	3.462102e+06	-0.010026347	0	1	3	0	3	2	1	3	0101000020E610000043FF4BEC964D5E4078ACB512E3473F40
12	329828.7	3.462102e+06	-0.008247084	0	1	3	0	3	2	1	3	0101000020E61000002B32F803974D5E409CE5A113E3473F40
12	329829.22	3.462102e+06	-0.006498301	0	1	3	0	3	2	1	3	0101000020E6100000859A471C974D5E403510B614E3473F40
12	329829.84	3.462102e+06	-0.0045815296	0	1	3	0	3	2	1	3	0101000020E610000025242837974D5E408DD01216E3473F40
12	329830.47	3.462102e+06	-0.002777205	0	1	3	0	3	2	1	3	0101000020E610000037E3AB52974D5E400F169A17E3473F40
12	329831.12	3.462102e+06	-0.0010609986	0	1	3	0	3	2	1	3	0101000020E61000008D71A76F974D5E40137B5A19E3473F40
12	329831.75	3.462102e+06	0.00026684868	0	1	3	0	3	2	1	3	0101000020E61000002476038B974D5E409494161BE3473F40
12	329832.3	3.462102e+06	0.0010407635	0	1	3	0	3	2	1	3	0101000020E6100000188679A3974D5E40736FA31CE3473F40
12	329832.56	3.462102e+06	0.0012159994	0	1	3	1	3	2	1	3	0101000020E610000021B06DAF974D5E409F6B631DE3473F40
0	329833.12	3.462102e+06	0.003183543	0	2	3	0	3	2	1	3	0101000020E610000060717FC8974D5E40F4E6D01FE3473F40
0	329833.7	3.462102e+06	0.007529821	0	2	3	0	3	2	1	3	0101000020E6100000FB562DE1974D5E40BD8B8922E3473F40
0	329834.3	3.462102e+06	0.0133653395	0	2	3	0	3	2	1	3	0101000020E6100000378834FC974D5E4014913326E3473F40
0	329834.9	3.462102e+06	0.021376664	0	2	3	0	3	2	1	3	0101000020E610000017372E16984D5E40BA1CD12AE3473F40
0	329835.5	3.462102e+06	0.0333236	0	2	3	0	3	2	1	3	0101000020E61000007DDAB830984D5E40A2E72C31E3473F40
0	329836.06	3.462102e+06	0.046784297	0	2	3	0	3	2	1	3	0101000020E6100000B5AC7C48984D5E40EDC57238E3473F40
0	329836.56	3.4621022e+06	0.06191851	0	2	3	0	3	2	1	3	0101000020E61000004BE8CB5E984D5E40A4B9E840E3473F40
0	329837.12	3.4621022e+06	0.08299427	0	2	3	0	3	2	1	3	0101000020E61000005460BC77984D5E40BAD9CA4CE3473F40
0	329837.62	3.4621022e+06	0.10801364	0	2	3	0	3	2	1	3	0101000020E6100000F032148E984D5E400ADA855AE3473F40
0	329838.16	3.4621025e+06	0.14095435	0	2	3	0	3	2	1	3	0101000020E610000007C6BFA4984D5E40C1B05C6CE3473F40
0	329838.66	3.4621025e+06	0.17850609	0	2	3	0	3	2	1	3	0101000020E610000057E77BBA984D5E4017567A81E3473F40
0	329839.12	3.4621028e+06	0.21759126	0	2	3	0	3	2	1	3	0101000020E610000082D577CF984D5E406D1B6999E3473F40
0	329839.62	3.4621028e+06	0.2607739	0	2	3	0	3	2	1	3	0101000020E6100000A1F3D3E5984D5E402ADDC1B6E3473F40
0	329840.1	3.462103e+06	0.3020282	0	2	3	0	3	2	1	3	0101000020E6100000CD3217FA984D5E40F84A10D5E3473F40
0	329840.62	3.4621032e+06	0.35275322	0	2	3	0	3	2	1	3	0101000020E610000045D9AC11994D5E4091FB2AFDE3473F40
0	329841.16	3.4621035e+06	0.4036609	0	2	3	0	3	2	1	3	0101000020E61000008299EC28994D5E40FCB0B529E4473F40
0	329841.66	3.462104e+06	0.4487908	0	2	3	0	3	2	1	3	0101000020E6100000AA93403E994D5E40146F9256E4473F40
0	329842.16	3.4621042e+06	0.48741257	0	2	3	0	3	2	1	3	0101000020E610000045BD0B54994D5E402891BB86E4473F40
0	329842.7	3.4621045e+06	0.51826465	0	2	3	0	3	2	1	3	0101000020E6100000E6DB506B994D5E400FF077BBE4473F40
0	329843.12	3.4621048e+06	0.5331357	0	2	3	0	3	2	1	3	0101000020E6100000A4D36C7E994D5E402B7056E6E4473F40
0	329843.56	3.462105e+06	0.54250205	0	2	3	0	3	2	1	3	0101000020E6100000ABD9C391994D5E40C14C9A11E5473F40
0	329844	3.4621052e+06	0.55262	0	2	3	0	3	2	1	3	0101000020E61000007AED6CA4994D5E40021F893CE5473F40
0	329844.53	3.4621058e+06	0.5711148	0	2	3	0	3	2	1	3	0101000020E6100000680869BB994D5E40DA176F74E5473F40
0	329845	3.462106e+06	0.59515864	0	2	3	0	3	2	1	3	0101000020E6100000091A84D0994D5E40652DC2ABE5473F40
0	329845.44	3.4621065e+06	0.62315524	0	2	3	0	3	2	1	3	0101000020E610000083F1E0E2994D5E404E1C38E0E5473F40
0	329845.88	3.4621068e+06	0.65661967	0	2	3	0	3	2	1	3	0101000020E610000043E3D0F5994D5E407E97DA1AE6473F40
0	329846.28	3.4621072e+06	0.6855015	0	2	3	0	3	2	1	3	0101000020E61000000A082C079A4D5E40D9EE6B53E6473F40
0	329846.66	3.4621075e+06	0.7133688	0	2	3	0	3	2	1	3	0101000020E6100000052ABB179A4D5E40B3D57C8CE6473F40
0	329847.03	3.462108e+06	0.7428278	0	2	3	0	3	2	1	3	0101000020E61000007C0046289A4D5E40D1FC1DC9E6473F40
0	329847.44	3.4621082e+06	0.7729368	0	2	3	0	3	2	1	3	0101000020E61000007D626A399A4D5E40171A600BE7473F40
0	329847.8	3.4621088e+06	0.8029044	0	2	3	0	3	2	1	3	0101000020E61000009C08014A9A4D5E404D0F6C4FE7473F40
0	329848.16	3.4621092e+06	0.82984114	0	2	3	0	3	2	1	3	0101000020E61000006A0882589A4D5E4052575B8EE7473F40
0	329848.5	3.4621098e+06	0.8580864	0	2	3	0	3	2	1	3	0101000020E61000003120B3679A4D5E40A132C6D3E7473F40
0	329848.88	3.4621102e+06	0.8865879	0	2	3	0	3	2	1	3	0101000020E6100000ADE4B9779A4D5E402797A320E8473F40
0	329849.2	3.4621105e+06	0.9094867	0	2	3	0	3	2	1	3	0101000020E610000093DCBF849A4D5E4086292C62E8473F40
0	329849.5	3.462111e+06	0.9316398	0	2	3	0	3	2	1	3	0101000020E610000096CD1C929A4D5E40CC15F8A7E8473F40
0	329849.8	3.4621115e+06	0.9530605	0	2	3	0	3	2	1	3	0101000020E6100000108B25A09A4D5E40D642ECF3E8473F40
0	329850.12	3.462112e+06	0.97396994	0	2	3	0	3	2	1	3	0101000020E610000093D3DEAD9A4D5E40FFC07C41E9473F40
0	329850.47	3.4621128e+06	0.9970589	0	2	3	0	3	2	1	3	0101000020E61000007EC68ABC9A4D5E4019C89198E9473F40
0	329850.8	3.4621132e+06	1.0209497	0	2	3	0	3	2	1	3	0101000020E610000020ABFBCA9A4D5E4035021FF3E9473F40
0	329851.16	3.4621138e+06	1.0466557	0	2	3	0	3	2	1	3	0101000020E610000081120ED99A4D5E407D583D51EA473F40
0	329851.44	3.4621145e+06	1.0720782	0	2	3	0	3	2	1	3	0101000020E6100000FE1406E69A4D5E4080DFE3ADEA473F40
0	329851.72	3.462115e+06	1.097261	0	2	3	0	3	2	1	3	0101000020E6100000BB3C17F29A4D5E407595180AEB473F40
0	329851.97	3.4621155e+06	1.1200346	0	2	3	0	3	2	1	3	0101000020E6100000B43D54FC9A4D5E403C9ED25DEB473F40
0	329852.2	3.462116e+06	1.1407106	0	2	3	0	3	2	1	3	0101000020E610000043EB11059B4D5E400CF817AAEB473F40
0	329852.38	3.4621165e+06	1.1605788	0	2	3	0	3	2	1	3	0101000020E6100000FEFEFA0C9B4D5E40AF8096F3EB473F40
0	329852.6	3.4621172e+06	1.1844374	0	2	3	0	3	2	1	3	0101000020E610000003D0D7159B4D5E408B0B1F4CEC473F40
0	329852.75	3.4621178e+06	1.2069826	0	2	3	0	3	2	1	3	0101000020E61000006216981D9B4D5E40F94DFF9FEC473F40
0	329852.9	3.4621182e+06	1.2268938	0	2	3	0	3	2	1	3	0101000020E6100000E682EE239B4D5E400DA832EAEC473F40
0	329853.06	3.4621188e+06	1.2477691	0	2	3	0	3	2	1	3	0101000020E61000008ADC432A9B4D5E40F52C243AED473F40
0	329853.22	3.4621192e+06	1.2683487	0	2	3	0	3	2	1	3	0101000020E6100000A64D60309B4D5E4024825F8DED473F40
0	329853.38	3.4621198e+06	1.2901899	0	2	3	0	3	2	1	3	0101000020E6100000D7773E369B4D5E40E67153E5ED473F40
0	329853.47	3.4621205e+06	1.3111919	0	2	3	0	3	2	1	3	0101000020E610000017151D3B9B4D5E4004A40337EE473F40
0	329853.6	3.462121e+06	1.3304601	0	2	3	0	3	2	1	3	0101000020E6100000E8762C3F9B4D5E401E579F82EE473F40
0	329853.7	3.4621215e+06	1.3527719	0	2	3	0	3	2	1	3	0101000020E6100000538062439B4D5E40CAC5F4DAEE473F40
0	329853.78	3.4621222e+06	1.3772384	0	2	3	0	3	2	1	3	0101000020E6100000F8C661479B4D5E40B4F1A83CEF473F40
0	329853.88	3.4621228e+06	1.4021173	0	2	3	0	3	2	1	3	0101000020E61000007149C44A9B4D5E40DB74CBA0EF473F40
0	329853.94	3.4621232e+06	1.423924	0	2	3	0	3	2	1	3	0101000020E6100000D951284D9B4D5E40612D22F9EF473F40
0	329854	3.462124e+06	1.4474088	0	2	3	0	3	2	1	3	0101000020E610000076FA204F9B4D5E400C0EC058F0473F40
0	329854.03	3.4621248e+06	1.4719468	0	2	3	0	3	2	1	3	0101000020E6100000E8DE7C509B4D5E4084491EBCF0473F40
0	329854.06	3.4621252e+06	1.4937884	0	2	3	0	3	2	1	3	0101000020E6100000ABAD0B519B4D5E406C774B0EF1473F40
0	329854.06	3.4621258e+06	1.512552	0	2	3	0	3	2	1	3	0101000020E6100000E2F62C519B4D5E407941235CF1473F40
11	329854.06	3.4621258e+06	1.5159851	0	2	3	2	3	2	1	3	0101000020E61000009D8526519B4D5E40C185B76DF1473F40
11	329854.1	3.4621265e+06	1.523325	0	0	3	0	3	2	1	3	0101000020E61000006054B5519B4D5E40402AF3C6F1473F40
11	329854.1	3.462127e+06	1.5287669	0	0	3	0	3	2	1	3	0101000020E610000063F715529B4D5E40EDF5891BF2473F40
11	329854.12	3.4621275e+06	1.5336227	0	0	3	0	3	2	1	3	0101000020E610000016FB5E529B4D5E407918EE6EF2473F40
11	329854.12	3.4621282e+06	1.5385331	0	0	3	0	3	2	1	3	0101000020E6100000848DA1529B4D5E40D60382D2F2473F40
11	329854.16	3.4621288e+06	1.5422851	0	0	3	0	3	2	1	3	0101000020E61000009652BB529B4D5E40AA7D3A1EF3473F40
11	329854.16	3.4621292e+06	1.5464343	0	0	3	0	3	2	1	3	0101000020E6100000388BBE529B4D5E409CB32A6FF3473F40
11	329854.16	3.4621298e+06	1.5510232	0	0	3	0	3	2	1	3	0101000020E6100000E8EBA6529B4D5E4029B242C8F3473F40
11	329854.16	3.4621305e+06	1.5564845	0	0	3	0	3	2	1	3	0101000020E6100000994663529B4D5E40667EE02BF4473F40
11	329854.16	3.4621312e+06	1.5623993	0	0	3	0	3	2	1	3	0101000020E6100000A962F0519B4D5E40DDBA6E94F4473F40
11	329854.16	3.462132e+06	1.5703973	0	0	3	0	3	2	1	3	0101000020E61000002EF90F519B4D5E4087E4A221F5473F40
11	329854.16	3.462133e+06	1.5778852	0	0	3	0	3	2	1	3	0101000020E6100000502BFE4F9B4D5E401DEDC49DF5473F40
11	329854.12	3.4621338e+06	1.5860002	0	0	3	0	3	2	1	3	0101000020E61000003677964E9B4D5E40F6CB0B1FF6473F40
11	329854.12	3.4621348e+06	1.5946198	0	0	3	0	3	2	1	3	0101000020E6100000B6C1C84C9B4D5E408653FEA7F6473F40
11	329854.1	3.4621352e+06	1.5993179	0	0	3	0	3	2	1	3	0101000020E6100000CDC5A54B9B4D5E4033EBECF3F6473F40
11	329854.06	3.4621362e+06	1.6079488	0	0	3	0	3	2	1	3	0101000020E6100000141E2A499B4D5E40337FE989F7473F40
11	329854.03	3.4621368e+06	1.6118467	0	0	3	0	3	2	1	3	0101000020E61000003944C0479B4D5E40602A17D9F7473F40
11	329854	3.4621372e+06	1.6151497	0	0	3	0	3	2	1	3	0101000020E61000001AF94F469B4D5E40A79BD226F8473F40
11	329853.97	3.4621378e+06	1.6179122	0	0	3	0	3	2	1	3	0101000020E6100000D629D8449B4D5E400DE1B973F8473F40
11	329853.94	3.4621382e+06	1.6205487	0	0	3	0	3	2	1	3	0101000020E6100000F8AC0D439B4D5E40FD7CF8CEF8473F40
11	329853.9	3.462139e+06	1.6222247	0	0	3	0	3	2	1	3	0101000020E6100000905362419B4D5E400ED98B23F9473F40
11	329853.88	3.4621395e+06	1.6229851	0	0	3	0	3	2	1	3	0101000020E6100000B2D6973F9B4D5E406D6BFB7EF9473F40
11	329853.88	3.46214e+06	1.6226414	0	0	3	0	3	2	1	3	0101000020E6100000F24CF83D9B4D5E405086DFD3F9473F40
11	329853.84	3.4621408e+06	1.6210767	0	0	3	0	3	2	1	3	0101000020E61000006B064E3C9B4D5E408E1ED52EFA473F40
11	329853.8	3.4621412e+06	1.6186819	0	0	3	0	3	2	1	3	0101000020E61000007239B93A9B4D5E407699CE88FA473F40
11	329853.78	3.4621418e+06	1.6155312	0	0	3	0	3	2	1	3	0101000020E61000003B9226399B4D5E404E31DFE6FA473F40
11	329853.75	3.4621425e+06	1.6118373	0	0	3	0	3	2	1	3	0101000020E6100000FC59B7379B4D5E402A270343FB473F40
11	329853.75	3.462143e+06	1.6074976	0	0	3	0	3	2	1	3	0101000020E61000004D9B5D369B4D5E4088AD78A1FB473F40
11	329853.72	3.4621438e+06	1.6026002	0	0	3	0	3	2	1	3	0101000020E6100000A90A15359B4D5E4058381B04FC473F40
11	329853.72	3.4621445e+06	1.5973788	0	0	3	0	3	2	1	3	0101000020E61000007506E3339B4D5E40E9535B6AFC473F40
11	329853.7	3.462145e+06	1.592597	0	0	3	0	3	2	1	3	0101000020E6100000459FE5329B4D5E402F9155C9FC473F40
11	329853.7	3.4621458e+06	1.5878382	0	0	3	0	3	2	1	3	0101000020E6100000C49EFC319B4D5E409658E12BFD473F40
11	329853.7	3.4621462e+06	1.5833122	0	0	3	0	3	2	1	3	0101000020E61000007AE734319B4D5E4076824F8DFD473F40
11	329853.66	3.462147e+06	1.5785472	0	0	3	0	3	2	1	3	0101000020E6100000FD8380309B4D5E4045D081F6FD473F40
11	329853.66	3.4621478e+06	1.5741891	0	0	3	0	3	2	1	3	0101000020E61000009D13F72F9B4D5E4077BF5559FE473F40
11	329853.66	3.4621485e+06	1.5698147	0	0	3	0	3	2	1	3	0101000020E61000008E42852F9B4D5E40EA45DCC0FE473F40
11	329853.7	3.462149e+06	1.5658615	0	0	3	0	3	2	1	3	0101000020E6100000B7BA342F9B4D5E403EF70624FF473F40
11	329853.7	3.4621498e+06	1.5621113	0	0	3	0	3	2	1	3	0101000020E61000002FD2FB2E9B4D5E407E4DE188FF473F40
11	329853.7	3.4621505e+06	1.5586646	0	0	3	0	3	2	1	3	0101000020E6100000F888DA2E9B4D5E4034BB31EFFF473F40
11	329853.7	3.462151e+06	1.5555829	0	0	3	0	3	2	1	3	0101000020E61000008E93CC2E9B4D5E4076198B5900483F40
11	329853.72	3.4621518e+06	1.5535295	0	0	3	0	3	2	1	3	0101000020E61000000B48C82E9B4D5E405643F9BA00483F40
11	329853.72	3.4621525e+06	1.5525761	0	0	3	0	3	2	1	3	0101000020E6100000E6C3C02E9B4D5E40A05D792101483F40
11	329853.72	3.462153e+06	1.5529577	0	0	3	0	3	2	1	3	0101000020E6100000BBA8B02E9B4D5E40AD58AE8501483F40
11	329853.72	3.4621538e+06	1.5545232	0	0	3	0	3	2	1	3	0101000020E610000007AB932E9B4D5E40A25F2AE101483F40
11	329853.75	3.4621542e+06	1.5571668	0	0	3	0	3	2	1	3	0101000020E6100000666C642E9B4D5E40DED87E3E02483F40
11	329853.75	3.462155e+06	1.5603182	0	0	3	0	3	2	1	3	0101000020E61000003D4B282E9B4D5E40DA51879A02483F40
11	329853.75	3.4621555e+06	1.563478	0	0	3	0	3	2	1	3	0101000020E61000000D93E32D9B4D5E406F1F6DF002483F40
11	329853.75	3.462156e+06	1.566743	0	0	3	0	3	2	1	3	0101000020E610000054F8912D9B4D5E40BA86D24403483F40
11	329853.75	3.4621565e+06	1.5698632	0	0	3	0	3	2	1	3	0101000020E610000077D9382D9B4D5E40236F1A9203483F40
11	329853.75	3.4621575e+06	1.5757023	0	0	3	0	3	2	1	3	0101000020E6100000079E692C9B4D5E4066B6381E04483F40
11	329853.75	3.4621582e+06	1.5807801	0	0	3	0	3	2	1	3	0101000020E610000066B0812B9B4D5E40FB60E99804483F40
11	329853.72	3.462159e+06	1.585214	0	0	3	0	3	2	1	3	0101000020E6100000175C852A9B4D5E40DCDED00505483F40
11	329853.72	3.4621595e+06	1.5889475	0	0	3	0	3	2	1	3	0101000020E6100000E8F487299B4D5E4091509C6305483F40
11	329853.7	3.4621602e+06	1.5920063	0	0	3	0	3	2	1	3	0101000020E6100000228398289B4D5E400DD304B305483F40
11	329853.7	3.4621608e+06	1.5956839	0	0	3	0	3	2	1	3	0101000020E6100000D62244279B4D5E4029ACD51906483F40
11	329853.66	3.4621615e+06	1.5986731	0	0	3	0	3	2	1	3	0101000020E6100000517FFA259B4D5E4098D7937506483F40
11	329853.66	3.462162e+06	1.598583	0	1	3	0	3	2	1	3	0101000020E610000008C206259B4D5E4026FF44C206483F40
11	329853.66	3.4621625e+06	1.5921912	0	1	3	0	3	2	1	3	0101000020E6100000C83E7C249B4D5E404618731107483F40
11	329853.66	3.462163e+06	1.5866058	0	1	3	0	3	2	1	3	0101000020E61000006F65FB239B4D5E402E59476407483F40
11	329853.66	3.4621635e+06	1.5818808	0	1	3	0	3	2	1	3	0101000020E6100000AB9C98239B4D5E403F7BB5B107483F40
11	329853.66	3.4621642e+06	1.5762384	0	1	3	0	3	2	1	3	0101000020E6100000A8F937239B4D5E40606D4C1408483F40
11	329853.66	3.462165e+06	1.5695647	0	1	3	0	3	2	1	3	0101000020E61000001A7AF6229B4D5E405875818B08483F40
11	329853.66	3.4621655e+06	1.564733	0	1	3	0	3	2	1	3	0101000020E6100000EF5EE6229B4D5E40CC68BEE308483F40
11	329853.66	3.4621662e+06	1.5599456	0	1	3	0	3	2	1	3	0101000020E6100000972EF2229B4D5E40BCEBD83F09483F40
11	329853.7	3.4621668e+06	1.554943	0	1	3	0	3	2	1	3	0101000020E610000077471F239B4D5E40109D03A309483F40
11	329853.72	3.4621675e+06	1.549864	0	1	3	0	3	2	1	3	0101000020E61000002FE270239B4D5E402068940C0A483F40
11	329853.72	3.4621682e+06	1.5447124	0	1	3	0	3	2	1	3	0101000020E6100000C795EF239B4D5E401DE183820A483F40
11	329853.75	3.462169e+06	1.5399877	0	1	3	0	3	2	1	3	0101000020E610000077A590249B4D5E40C4BE34F30A483F40
11	329853.78	3.4621698e+06	1.535258	0	1	3	0	3	2	1	3	0101000020E6100000A26F59259B4D5E40FA0F7E650B483F40
11	329853.8	3.4621705e+06	1.5306907	0	1	3	0	3	2	1	3	0101000020E6100000237042269B4D5E401862E6D30B483F40
11	329853.84	3.4621712e+06	1.5263491	0	1	3	0	3	2	1	3	0101000020E6100000D62244279B4D5E407C0B013E0C483F40
11	329853.88	3.462172e+06	1.5224364	0	1	3	0	3	2	1	3	0101000020E61000009A9A5F289B4D5E40B6AC03A70C483F40
11	329853.94	3.4621725e+06	1.5194253	0	1	3	0	3	2	1	3	0101000020E61000009DEC78299B4D5E40CD89AD080D483F40
11	329853.97	3.462173e+06	1.517731	0	1	3	0	3	2	1	3	0101000020E6100000AE66772A9B4D5E40099EE3600D483F40
11	329853.97	3.4621738e+06	1.5182028	0	1	3	0	3	2	1	3	0101000020E610000068AA552B9B4D5E40D5D33AB60D483F40
11	329854	3.4621742e+06	1.5209472	0	1	3	0	3	2	1	3	0101000020E61000009A05FB2B9B4D5E40FEAF3C050E483F40
11	329854.03	3.462175e+06	1.5261438	0	1	3	0	3	2	1	3	0101000020E61000007BC7B42C9B4D5E40DABC69730E483F40
11	329854.06	3.4621758e+06	1.5312276	0	1	3	0	3	2	1	3	0101000020E610000057EC392D9B4D5E40841C7ADC0E483F40
11	329854.06	3.4621762e+06	1.5355288	0	1	3	0	3	2	1	3	0101000020E6100000D1AC8D2D9B4D5E4029E8CC370F483F40
11	329854.1	3.4621768e+06	1.5395601	0	1	3	0	3	2	1	3	0101000020E61000003411BF2D9B4D5E40FAA1858C0F483F40
11	329854.1	3.4621775e+06	1.5443555	0	1	3	0	3	2	1	3	0101000020E6100000C28AD42D9B4D5E404F97CDEC0F483F40
11	329854.1	3.462178e+06	1.5488611	0	1	3	0	3	2	1	3	0101000020E6100000976FC42D9B4D5E40B1090E4210483F40
11	329854.12	3.4621785e+06	1.5538629	0	1	3	0	3	2	1	3	0101000020E610000010878B2D9B4D5E404E661F8F10483F40
11	329854.12	3.462179e+06	1.5607287	0	1	3	0	3	2	1	3	0101000020E6100000BD44132D9B4D5E404F742EE410483F40
11	329854.1	3.4621795e+06	1.5673499	0	1	3	0	3	2	1	3	0101000020E6100000AF6D752C9B4D5E40D43D2A3611483F40
11	329854.1	3.4621802e+06	1.5718696	0	1	3	0	3	2	1	3	0101000020E6100000C183D62B9B4D5E40727C6C8711483F40
11	329854.1	3.4621805e+06	1.5732495	0	1	3	1	3	2	1	3	0101000020E610000066B0812B9B4D5E40ED7381B611483F40
0	329854.1	3.462181e+06	1.574888	0	2	3	0	3	2	1	3	0101000020E61000003AECE42A9B4D5E401AFFDD0212483F40
0	329854.1	3.4621815e+06	1.5794721	0	2	3	0	3	2	1	3	0101000020E61000004765112A9B4D5E4097B4BE5512483F40
0	329854.06	3.462182e+06	1.5845734	0	2	3	0	3	2	1	3	0101000020E610000075C510299B4D5E40B9B6CAAC12483F40
0	329854.06	3.4621825e+06	1.5893664	0	2	3	0	3	2	1	3	0101000020E610000040C70A289B4D5E40D745BCFA12483F40
0	329854.03	3.4621832e+06	1.5952569	0	2	3	0	3	2	1	3	0101000020E6100000052CD0269B4D5E40832F224B13483F40
0	329854	3.4621838e+06	1.6037567	0	2	3	0	3	2	1	3	0101000020E6100000775449259B4D5E409F1BB39813483F40
0	329853.97	3.4621842e+06	1.6171738	0	2	3	0	3	2	1	3	0101000020E6100000AF8A14239B4D5E40627512EF13483F40
0	329853.9	3.4621848e+06	1.6328299	0	2	3	0	3	2	1	3	0101000020E6100000947E67209B4D5E4023328E4314483F40
0	329853.84	3.4621852e+06	1.6523454	0	2	3	0	3	2	1	3	0101000020E6100000D78AFE1C9B4D5E407C13A19A14483F40
0	329853.78	3.4621858e+06	1.6732706	0	2	3	0	3	2	1	3	0101000020E6100000B29B5B199B4D5E4029FCD6E514483F40
0	329853.7	3.4621865e+06	1.6979816	0	2	3	0	3	2	1	3	0101000020E61000005D45BB149B4D5E4099F9693715483F40
0	329853.56	3.462187e+06	1.7240282	0	2	3	0	3	2	1	3	0101000020E61000003BEC4E0F9B4D5E404373E78A15483F40
0	329853.4	3.4621875e+06	1.7547388	0	2	3	0	3	2	1	3	0101000020E6100000AE458F089B4D5E40665DA1E415483F40
0	329853.28	3.462188e+06	1.7847992	0	2	3	0	3	2	1	3	0101000020E6100000438076019B4D5E403416883716483F40
0	329853.06	3.4621888e+06	1.8204211	0	2	3	0	3	2	1	3	0101000020E6100000347B5CF89A4D5E4050BBB09516483F40
0	329852.88	3.4621892e+06	1.8516887	0	2	3	0	3	2	1	3	0101000020E6100000E64AFDEF9A4D5E404C75A4E216483F40
0	329852.7	3.4621898e+06	1.8838559	0	2	3	0	3	2	1	3	0101000020E6100000BA52B8E69A4D5E402EBA513017483F40
0	329852.47	3.4621902e+06	1.9157909	0	2	3	0	3	2	1	3	0101000020E6100000692187DC9A4D5E405F59277F17483F40
0	329852.22	3.4621908e+06	1.9482684	0	2	3	0	3	2	1	3	0101000020E6100000C6FE24D19A4D5E402E188BD017483F40
0	329851.9	3.4621915e+06	1.9841748	0	2	3	0	3	2	1	3	0101000020E610000003E299C39A4D5E4030D0A72918483F40
0	329851.6	3.462192e+06	2.019838	0	2	3	0	3	2	1	3	0101000020E610000053D543B59A4D5E409A9B508018483F40
0	329851.28	3.4621925e+06	2.053351	0	2	3	0	3	2	1	3	0101000020E6100000324203A79A4D5E40D080E7CF18483F40
0	329850.97	3.462193e+06	2.08539	0	2	3	0	3	2	1	3	0101000020E61000005FAB79989A4D5E4015818A1B19483F40
0	329850.62	3.4621935e+06	2.1174214	0	2	3	0	3	2	1	3	0101000020E61000006C7838899A4D5E40240C926519483F40
0	329850.3	3.462194e+06	2.14714	0	2	3	0	3	2	1	3	0101000020E6100000C4FCBE7A9A4D5E40883523A719483F40
0	329849.97	3.4621942e+06	2.176603	0	2	3	0	3	2	1	3	0101000020E6100000D278366C9A4D5E40FC79C0E419483F40
0	329849.62	3.4621948e+06	2.2067316	0	2	3	0	3	2	1	3	0101000020E61000006A22D65C9A4D5E4015B911221A483F40
0	329849.28	3.4621952e+06	2.2361507	0	2	3	0	3	2	1	3	0101000020E610000086114E4D9A4D5E40B8E6695C1A483F40
0	329848.94	3.4621955e+06	2.2662764	0	2	3	0	3	2	1	3	0101000020E61000000113DE3C9A4D5E40C76792961A483F40
0	329848.53	3.462196e+06	2.2972825	0	2	3	0	3	2	1	3	0101000020E61000004716682B9A4D5E40460BB7D01A483F40
0	329848.12	3.4621962e+06	2.3287601	0	2	3	0	3	2	1	3	0101000020E6100000562118199A4D5E403D81040A1B483F40
0	329847.7	3.4621968e+06	2.3599832	0	2	3	0	3	2	1	3	0101000020E6100000021F0A069A4D5E403E3738421B483F40
0	329847.25	3.462197e+06	2.390648	0	2	3	0	3	2	1	3	0101000020E610000021978EF2994D5E40C7C24E781B483F40
0	329846.75	3.4621975e+06	2.4248135	0	2	3	0	3	2	1	3	0101000020E61000001BCF28DC994D5E403F078EB21B483F40
0	329846.28	3.4621978e+06	2.456581	0	2	3	0	3	2	1	3	0101000020E6100000EB943FC6994D5E404BCB01E81B483F40
0	329845.72	3.4621982e+06	2.4907339	0	2	3	0	3	2	1	3	0101000020E61000005BE3CEAD994D5E40BAC8AD1F1C483F40
0	329845.2	3.4621985e+06	2.5229697	0	2	3	0	3	2	1	3	0101000020E61000009ECB3296994D5E40DD19BA511C483F40
0	329844.62	3.462199e+06	2.5573335	0	2	3	0	3	2	1	3	0101000020E61000000CC27C7C994D5E40F6B34A841C483F40
0	329843.97	3.4621992e+06	2.594826	0	2	3	0	3	2	1	3	0101000020E61000004FE1D35F994D5E404EB215B81C483F40
0	329843.5	3.4621995e+06	2.6217396	0	2	3	0	3	2	1	3	0101000020E6100000E618DA4A994D5E4057BC1FDB1C483F40
0	329842.84	3.4621998e+06	2.6576207	0	2	3	0	3	2	1	3	0101000020E61000006669A22D994D5E4040D55E081D483F40
0	329842.16	3.4622e+06	2.6923187	0	2	3	0	3	2	1	3	0101000020E61000003E3BA60F994D5E406644DC321D483F40
0	329841.7	3.4622002e+06	2.7159526	0	2	3	0	3	2	1	3	0101000020E6100000DDFD5CFA984D5E408E33B74E1D483F40
0	329841.12	3.4622005e+06	2.742582	0	2	3	0	3	2	1	3	0101000020E61000000BCF8DE1984D5E40BAFEE76C1D483F40
0	329840.5	3.4622008e+06	2.770246	0	2	3	0	3	2	1	3	0101000020E610000082A1CFC6984D5E40AB7FD48A1D483F40
0	329840	3.4622008e+06	2.7932858	0	2	3	0	3	2	1	3	0101000020E61000007ED33DB0984D5E400E8CF8A11D483F40
0	329839.5	3.462201e+06	2.816253	0	2	3	0	3	2	1	3	0101000020E610000068409299984D5E4062054BB71D483F40
0	329838.97	3.4622012e+06	2.839825	0	2	3	0	3	2	1	3	0101000020E61000000D8D2782984D5E4038A453CB1D483F40
0	329838.4	3.4622012e+06	2.8640146	0	2	3	0	3	2	1	3	0101000020E61000002EE52B6A984D5E403B5EBDDD1D483F40
0	329837.84	3.4622015e+06	2.890173	0	2	3	0	3	2	1	3	0101000020E610000060A4D850984D5E404351BEEE1D483F40
0	329837.25	3.4622015e+06	2.918463	0	2	3	0	3	2	1	3	0101000020E610000069E4D735984D5E40F6AF2DFE1D483F40
0	329836.62	3.4622015e+06	2.9463375	0	2	3	0	3	2	1	3	0101000020E61000004AAC271B984D5E401216D10A1E483F40
0	329836.06	3.4622018e+06	2.9731238	0	2	3	0	3	2	1	3	0101000020E6100000B20B6901984D5E40212E92141E483F40
0	329835.38	3.4622018e+06	3.0041502	0	2	3	0	3	2	1	3	0101000020E610000069F699E3974D5E407C54DF1C1E483F40
\.


--
-- Data for Name: begin_point_candidate; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.begin_point_candidate (id) FROM stdin;
385
504
619
119
270
\.


--
-- Data for Name: begin_point_in_buffer; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.begin_point_in_buffer (p_id, utmx, utmy, heading, curv, mode, speed_mode, event_mode, opposite_side_mode, lane_num, lane_seq, lane_width, geom) FROM stdin;
0	329774.97	3.4621235e+06	1.5588441	0	2	3	2	3	2	1	3	0101000020E6100000AD0A44B78D4D5E406CCA9A3BEF473F40
3	329784.6	3.4621912e+06	0.024693362	0	2	3	2	3	2	1	3	0101000020E6100000BBA4202E8F4D5E4094291B5C17483F40
4	329843.16	3.462182e+06	-1.6357403	0	2	3	2	3	2	1	3	0101000020E610000018B89C48994D5E4053542B8312483F40
10	329687.12	3.4621798e+06	-1.6199863	0	2	3	2	3	2	1	3	0101000020E6100000059642717E4D5E40A3FB4AC30F483F40
11	329712.47	3.4621025e+06	-0.020786488	0	2	3	2	3	2	1	3	0101000020E61000004C7BCC05834D5E4044E4C34EE2473F40
\.


--
-- Data for Name: begin_points; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.begin_points (p_id, utmx, utmy, heading, curv, mode, speed_mode, event_mode, opposite_side_mode, lane_num, lane_seq, lane_width, geom) FROM stdin;
0	329774.97	3.4621235e+06	1.5588441	0	2	3	2	3	2	1	3	0101000020E6100000AD0A44B78D4D5E406CCA9A3BEF473F40
1	329764.38	3.4621805e+06	-1.3648859	0	2	3	2	3	2	1	3	0101000020E61000007AD35ABC8B4D5E4063723DE610483F40
2	329709.5	3.462191e+06	0.011138936	0	0	3	2	3	2	1	3	0101000020E6100000CFE3C043824D5E405C987A9616483F40
3	329784.6	3.4621912e+06	0.024693362	0	2	3	2	3	2	1	3	0101000020E6100000BBA4202E8F4D5E4094291B5C17483F40
4	329843.16	3.462182e+06	-1.6357403	0	2	3	2	3	2	1	3	0101000020E610000018B89C48994D5E4053542B8312483F40
5	329832.94	3.4621128e+06	-3.0704613	0	2	3	2	3	2	1	3	0101000020E610000066A037B7974D5E40E6423C85E9473F40
6	329752.38	3.4621128e+06	-3.1350563	0	2	3	2	3	2	1	3	0101000020E610000023AE36DB894D5E40E39898AEE8473F40
7	329698.2	3.4621258e+06	1.6115699	0	2	3	2	3	2	1	3	0101000020E6100000228F1080804D5E4056D762EDEF473F40
8	329834.56	3.462202e+06	-3.1182487	0	0	3	2	3	2	1	3	0101000020E6100000206905C0974D5E4099CAC5511E483F40
9	329754.97	3.462202e+06	-3.123961	0	2	3	2	3	2	1	3	0101000020E61000003E87390E8A4D5E40569569731D483F40
10	329687.12	3.4621798e+06	-1.6199863	0	2	3	2	3	2	1	3	0101000020E6100000059642717E4D5E40A3FB4AC30F483F40
11	329712.47	3.4621025e+06	-0.020786488	0	2	3	2	3	2	1	3	0101000020E61000004C7BCC05834D5E4044E4C34EE2473F40
12	329785.7	3.4621018e+06	0.0044283043	0	2	3	2	3	2	1	3	0101000020E610000091276F9E8F4D5E402AABE97AE2473F40
13	329854.06	3.4621258e+06	1.5159851	0	2	3	2	3	2	1	3	0101000020E61000009D8526519B4D5E40C185B76DF1473F40
\.


--
-- Data for Name: dijk; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.dijk (seq, path_seq, node, edge, cost, agg_cost) FROM stdin;
1	1	8	15	30.04372526214687	0
2	2	10	9	59.97684410647711	30.04372526214687
3	3	1	8	14.698460047865089	90.02056936862398
4	4	2	-1	0	104.71902941648906
\.


--
-- Data for Name: edge_id; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.edge_id (edge) FROM stdin;
\.


--
-- Data for Name: path_cost; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.path_cost (gid, cost) FROM stdin;
2	218.4697349919618
15	30.04372526214687
9	59.97684410647711
8	14.698460047865089
6	189.65322868710817
\.


--
-- Data for Name: point_in_buffer; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.point_in_buffer (p_id, utmx, utmy, heading, curv, mode, speed_mode, event_mode, opposite_side_mode, lane_num, lane_seq, lane_width, geom) FROM stdin;
0	329768.6	3.4621095e+06	-0.99867505	0	2	3	0	3	2	1	3	0101000020E6100000CBA4E6A88C4D5E401F203AE9E6473F40
8	329774.97	3.4621232e+06	1.5589936	0	2	3	0	3	2	1	3	0101000020E6100000CCF742B78D4D5E40317F2636EF473F40
8	329774.97	3.4621235e+06	1.5588441	0	2	3	2	3	2	1	3	0101000020E6100000AD0A44B78D4D5E406CCA9A3BEF473F40
8	329774.97	3.462124e+06	1.5551938	0	0	3	0	3	2	1	3	0101000020E610000094B44DB78D4D5E40562A108CEF473F40
8	329775	3.4621245e+06	1.5526977	0	0	3	0	3	2	1	3	0101000020E61000004E4973B78D4D5E40F34F2EDEEF473F40
8	329775	3.462125e+06	1.5512656	0	0	3	0	3	2	1	3	0101000020E6100000AA169CB78D4D5E40429A9638F0473F40
8	329775	3.4621255e+06	1.5521948	0	0	3	0	3	2	1	3	0101000020E61000004C4F9FB78D4D5E40FE1C4B89F0473F40
8	329775.03	3.4621262e+06	1.5534059	0	0	3	0	3	2	1	3	0101000020E6100000E9F099B78D4D5E40CABD3DE2F0473F40
8	329775.03	3.4621268e+06	1.5543876	0	0	3	0	3	2	1	3	0101000020E610000021348FB78D4D5E40F8BF2F2FF1473F40
8	329775.03	3.4621272e+06	1.5553857	0	0	3	0	3	2	1	3	0101000020E6100000F6187FB78D4D5E40F6CB3C7DF1473F40
8	329775.06	3.4621278e+06	1.5566615	0	0	3	0	3	2	1	3	0101000020E6100000421B62B78D4D5E40DBBE59E0F1473F40
8	329775.06	3.4621285e+06	1.557181	0	0	3	0	3	2	1	3	0101000020E6100000B3A14CB78D4D5E4090F5CF31F2473F40
8	329775.06	3.462129e+06	1.5564612	0	0	3	0	3	2	1	3	0101000020E6100000116949B78D4D5E402C36728DF2473F40
8	329775.06	3.4621295e+06	1.5557215	0	0	3	0	3	2	1	3	0101000020E6100000D28E4BB78D4D5E409C6000E4F2473F40
8	329775.1	3.4621302e+06	1.5550251	0	0	3	0	3	2	1	3	0101000020E6100000170052B78D4D5E40A5D9EE35F3473F40
8	329775.1	3.4621308e+06	1.5543706	0	0	3	0	3	2	1	3	0101000020E6100000DEBC5CB78D4D5E4009F30A83F3473F40
8	329775.1	3.4621312e+06	1.5534298	0	0	3	0	3	2	1	3	0101000020E6100000AC1070B78D4D5E40B9ACCCD8F3473F40
8	329775.12	3.4621318e+06	1.5520897	0	0	3	0	3	2	1	3	0101000020E6100000C36C92B78D4D5E40CD572E3CF4473F40
8	329775.12	3.4621325e+06	1.5512493	0	0	3	0	3	2	1	3	0101000020E61000003990B1B78D4D5E403C4F448FF4473F40
8	329775.12	3.462133e+06	1.5508374	0	0	3	0	3	2	1	3	0101000020E6100000AFB3D0B78D4D5E40D656F1E8F4473F40
8	329775.16	3.4621338e+06	1.5504098	0	0	3	0	3	2	1	3	0101000020E61000008835F5B78D4D5E40EA5FC44DF5473F40
8	329775.16	3.4621345e+06	1.5499336	0	0	3	0	3	2	1	3	0101000020E6100000674E22B88D4D5E40F81261BDF5473F40
8	329775.2	3.462135e+06	1.5495847	0	0	3	0	3	2	1	3	0101000020E61000009E9743B88D4D5E404E1D6B0BF6473F40
8	329775.2	3.4621355e+06	1.549211	0	0	3	0	3	2	1	3	0101000020E6100000393F6AB88D4D5E40B07C0460F6473F40
8	329775.22	3.462136e+06	1.5493453	0	0	3	0	3	2	1	3	0101000020E610000070888BB88D4D5E40BC6E8AB9F6473F40
8	329775.22	3.4621368e+06	1.5503047	0	0	3	0	3	2	1	3	0101000020E61000005DC99DB88D4D5E40F4CDE815F7473F40
8	329775.22	3.4621372e+06	1.551677	0	0	3	0	3	2	1	3	0101000020E61000006360A6B88D4D5E40A0838D77F7473F40
8	329775.25	3.462138e+06	1.5532963	0	0	3	0	3	2	1	3	0101000020E6100000FF01A1B88D4D5E4072986CDBF7473F40
8	329775.25	3.4621388e+06	1.5564476	0	0	3	0	3	2	1	3	0101000020E61000007DB070B88D4D5E400B8A2742F8473F40
8	329775.25	3.4621395e+06	1.5614815	0	0	3	0	3	2	1	3	0101000020E6100000369C09B88D4D5E401704CFAFF8473F40
8	329775.25	3.4621402e+06	1.5673528	0	0	3	0	3	2	1	3	0101000020E610000029C56BB78D4D5E404DA65F21F9473F40
8	329775.25	3.462141e+06	1.5730463	0	0	3	0	3	2	1	3	0101000020E6100000DE0DA4B68D4D5E40C467DD91F9473F40
8	329775.22	3.4621418e+06	1.5789915	0	0	3	0	3	2	1	3	0101000020E6100000C26594B58D4D5E404227840EFA473F40
8	329775.22	3.4621425e+06	1.5843118	0	0	3	0	3	2	1	3	0101000020E6100000CC3B60B48D4D5E406E720C89FA473F40
8	329775.2	3.4621432e+06	1.5883317	0	0	3	0	3	2	1	3	0101000020E6100000CBE31AB38D4D5E4027A04300FB473F40
8	329775.2	3.4621442e+06	1.5913639	0	0	3	0	3	2	1	3	0101000020E61000000FF7AFB18D4D5E40B362027FFB473F40
8	329775.16	3.462145e+06	1.5919833	0	0	3	0	3	2	1	3	0101000020E6100000603856B08D4D5E408991BEFDFB473F40
8	329775.16	3.4621458e+06	1.5886306	0	0	3	0	3	2	1	3	0101000020E6100000CE6C27AF8D4D5E40DCAE9B83FC473F40
8	329775.16	3.4621468e+06	1.5836434	0	0	3	0	3	2	1	3	0101000020E6100000BDF228AE8D4D5E40F3B95709FD473F40
8	329775.12	3.4621478e+06	1.5791459	0	0	3	0	3	2	1	3	0101000020E6100000753535AD8D4D5E40B02C6599FD473F40
8	329775.12	3.4621488e+06	1.5754303	0	0	3	0	3	2	1	3	0101000020E6100000ECA36FAC8D4D5E40E4A21125FE473F40
8	329775.12	3.4621492e+06	1.5734128	0	0	3	0	3	2	1	3	0101000020E61000002D7215AC8D4D5E40C6894D71FE473F40
8	329775.12	3.46215e+06	1.569488	0	0	3	0	3	2	1	3	0101000020E6100000B5AB95AB8D4D5E4057C675F9FE473F40
8	329775.16	3.462151e+06	1.5646013	0	0	3	0	3	2	1	3	0101000020E6100000BD3646AB8D4D5E40C97B3E8AFF473F40
8	329775.16	3.462152e+06	1.5601128	0	0	3	0	3	2	1	3	0101000020E6100000BF301AAB8D4D5E40E740061F00483F40
8	329775.2	3.462153e+06	1.5574644	0	0	3	0	3	2	1	3	0101000020E6100000490DFBAA8D4D5E4025A306B800483F40
8	329775.2	3.462154e+06	1.5570852	0	0	3	0	3	2	1	3	0101000020E6100000519ED7AA8D4D5E40FE4D8C4A01483F40
8	329775.2	3.462155e+06	1.5581845	0	0	3	0	3	2	1	3	0101000020E6100000EF39A6AA8D4D5E40AB21CEDB01483F40
8	329775.22	3.4621558e+06	1.5596974	0	0	3	0	3	2	1	3	0101000020E6100000C5186AAA8D4D5E40C2CA9D6802483F40
8	329775.22	3.4621568e+06	1.5611922	0	0	3	0	3	2	1	3	0101000020E6100000B54D24AA8D4D5E40156A38EA02483F40
8	329775.22	3.4621575e+06	1.5627047	0	0	3	0	3	2	1	3	0101000020E61000009EEBD5A98D4D5E4045AFE56203483F40
8	329775.22	3.4621582e+06	1.5641255	0	0	3	0	3	2	1	3	0101000020E6100000E65084A98D4D5E400F286FCD03483F40
8	329775.22	3.4621588e+06	1.5653466	0	0	3	0	3	2	1	3	0101000020E6100000CFEE35A98D4D5E40F0DF942704483F40
8	329775.22	3.4621595e+06	1.5667764	0	0	3	0	3	2	1	3	0101000020E6100000C6B4CCA88D4D5E40451C2C9104483F40
8	329775.22	3.46216e+06	1.5678338	0	0	3	0	3	2	1	3	0101000020E6100000C9A874A88D4D5E40803A5ADF04483F40
8	329775.22	3.4621605e+06	1.5689198	0	0	3	0	3	2	1	3	0101000020E6100000A71815A88D4D5E40997D0D2E05483F40
8	329775.22	3.462161e+06	1.5703892	0	0	3	0	3	2	1	3	0101000020E6100000669BB6A78D4D5E409612857B05483F40
8	329775.22	3.4621615e+06	1.5720363	0	1	3	0	3	2	1	3	0101000020E61000003D744EA78D4D5E40811A06C905483F40
8	329775.22	3.462162e+06	1.5723042	0	1	3	0	3	2	1	3	0101000020E6100000B2EEE0A68D4D5E404BF0E91506483F40
8	329775.22	3.4621625e+06	1.572285	0	1	3	0	3	2	1	3	0101000020E6100000266973A68D4D5E405931576406483F40
8	329775.22	3.462163e+06	1.5726343	0	1	3	0	3	2	1	3	0101000020E6100000179801A68D4D5E409FA9BFB506483F40
8	329775.22	3.4621638e+06	1.5729396	0	1	3	0	3	2	1	3	0101000020E610000046A18DA58D4D5E409444700807483F40
8	329775.22	3.4621642e+06	1.5723301	0	1	3	0	3	2	1	3	0101000020E6100000FF8C26A58D4D5E400AE6355C07483F40
8	329775.22	3.4621648e+06	1.5689547	0	1	3	0	3	2	1	3	0101000020E61000005E4EF7A48D4D5E403F8132B007483F40
8	329775.22	3.4621652e+06	1.5666012	0	1	3	0	3	2	1	3	0101000020E6100000405BCCA48D4D5E40DE10BC0008483F40
8	329775.25	3.4621658e+06	1.5651077	0	1	3	0	3	2	1	3	0101000020E6100000E48DA3A48D4D5E4073AAF94D08483F40
8	329775.25	3.4621665e+06	1.5653507	0	1	3	0	3	2	1	3	0101000020E6100000517759A48D4D5E40EEF69EA708483F40
8	329775.25	3.462167e+06	1.5661561	0	1	3	0	3	2	1	3	0101000020E6100000B232FEA38D4D5E402331DF0709483F40
8	329775.25	3.4621675e+06	1.5668356	0	1	3	0	3	2	1	3	0101000020E6100000F997ACA38D4D5E40422B6C5909483F40
8	329775.25	3.4621682e+06	1.5673566	0	1	3	0	3	2	1	3	0101000020E61000003B6652A38D4D5E4022D0EAB209483F40
8	329775.25	3.4621688e+06	1.567486	0	1	3	0	3	2	1	3	0101000020E6100000769DEFA28D4D5E404E2F63190A483F40
8	329775.25	3.4621695e+06	1.5669183	0	1	3	0	3	2	1	3	0101000020E61000005AA498A28D4D5E4062A94E800A483F40
8	329775.25	3.46217e+06	1.5661262	0	1	3	0	3	2	1	3	0101000020E6100000B3CE60A28D4D5E402A40C0CC0A483F40
8	329775.25	3.4621708e+06	1.5649205	0	1	3	0	3	2	1	3	0101000020E610000000CB17A28D4D5E40301DE23B0B483F40
8	329775.25	3.4621712e+06	1.5640576	0	1	3	0	3	2	1	3	0101000020E61000007F79E7A18D4D5E40949B1C8E0B483F40
8	329775.28	3.462172e+06	1.5632927	0	1	3	0	3	2	1	3	0101000020E6100000FD27B7A18D4D5E403B8C8DE50B483F40
8	329775.28	3.4621725e+06	1.562813	0	1	3	0	3	2	1	3	0101000020E61000009BC385A18D4D5E404C02753E0C483F40
8	329775.28	3.462173e+06	1.562795	0	1	3	0	3	2	1	3	0101000020E6100000D5004FA18D4D5E406360AC960C483F40
8	329775.28	3.4621738e+06	1.5633475	0	1	3	0	3	2	1	3	0101000020E6100000865B0BA18D4D5E4034AC7EF50C483F40
8	329775.28	3.4621742e+06	1.5642018	0	1	3	0	3	2	1	3	0101000020E61000001232C0A08D4D5E403B853E530D483F40
8	329775.28	3.462175e+06	1.5652184	0	1	3	0	3	2	1	3	0101000020E610000073ED64A08D4D5E40D965F6BC0D483F40
8	329775.28	3.4621758e+06	1.5661751	0	1	3	0	3	2	1	3	0101000020E6100000128307A08D4D5E4079E70C200E483F40
8	329775.28	3.4621762e+06	1.5671457	0	1	3	0	3	2	1	3	0101000020E6100000AC81A19F8D4D5E4038829C840E483F40
8	329775.28	3.462177e+06	1.5681176	0	1	3	0	3	2	1	3	0101000020E610000020FC339F8D4D5E403C3DEBE90E483F40
8	329775.28	3.4621778e+06	1.5690134	0	1	3	0	3	2	1	3	0101000020E6100000D250C49E8D4D5E40B126304C0F483F40
8	329775.28	3.4621782e+06	1.569768	0	1	3	0	3	2	1	3	0101000020E61000002D75609E8D4D5E40D33436A00F483F40
8	329775.28	3.462179e+06	1.5707248	0	1	3	0	3	2	1	3	0101000020E61000005150DB9D8D4D5E40D8806E0A10483F40
8	329775.28	3.4621795e+06	1.5714543	0	1	3	0	3	2	1	3	0101000020E6100000A6DD6E9D8D4D5E401054305B10483F40
8	329775.28	3.46218e+06	1.5722078	0	1	3	0	3	2	1	3	0101000020E6100000B6F9FB9C8D4D5E40B1809DAD10483F40
8	329775.28	3.4621802e+06	1.5724922	0	1	3	1	3	2	1	3	0101000020E61000009806D19C8D4D5E40BCA29BCA10483F40
0	329775.28	3.4621808e+06	1.5764493	0	2	3	0	3	2	1	3	0101000020E6100000A485299C8D4D5E4029E3791711483F40
0	329775.28	3.4621812e+06	1.5812776	0	2	3	0	3	2	1	3	0101000020E6100000615F3E9B8D4D5E404DE6577011483F40
0	329775.25	3.462182e+06	1.5886123	0	2	3	0	3	2	1	3	0101000020E61000006007F9998D4D5E409080F1CE11483F40
0	329775.22	3.4621825e+06	1.5984265	0	2	3	0	3	2	1	3	0101000020E61000006DD16C988D4D5E4099008D2412483F40
0	329775.28	3.4621802e+06	1.5854015	0	2	3	0	3	2	1	3	0101000020E6100000B062F39C8D4D5E4042EC26DA10483F40
0	329775.28	3.4621808e+06	1.5821217	0	2	3	0	3	2	1	3	0101000020E61000008FCC679C8D4D5E4040A7EC2811483F40
0	329775.28	3.4621812e+06	1.5799358	0	2	3	0	3	2	1	3	0101000020E6100000DEC2F29B8D4D5E402F6BF27511483F40
0	329775.28	3.4621818e+06	1.5776342	0	2	3	0	3	2	1	3	0101000020E6100000523D859B8D4D5E40255D67C611483F40
0	329775.28	3.4621822e+06	1.5752916	0	2	3	0	3	2	1	3	0101000020E610000030AD259B8D4D5E40004ADC1712483F40
0	329775.28	3.462183e+06	1.5722781	0	2	3	0	3	2	1	3	0101000020E610000000F5E09A8D4D5E4023ED466812483F40
0	329775.3	3.4621835e+06	1.5642622	0	2	3	0	3	2	1	3	0101000020E61000009B9C079B8D4D5E40F9FD65B712483F40
0	329775.34	3.462184e+06	1.551045	0	2	3	0	3	2	1	3	0101000020E61000000541CE9B8D4D5E401FF2B90313483F40
0	329775.38	3.4621845e+06	1.5234219	0	2	3	0	3	2	1	3	0101000020E61000009B58EA9D8D4D5E40E54CEB5B13483F40
0	329775.47	3.462185e+06	1.4919524	0	2	3	0	3	2	1	3	0101000020E61000004F12EAA08D4D5E40ED9F8BAC13483F40
0	329775.56	3.4621855e+06	1.4563503	0	2	3	0	3	2	1	3	0101000020E61000001446E8A48D4D5E40343795FB13483F40
0	329775.7	3.4621862e+06	1.4137312	0	2	3	0	3	2	1	3	0101000020E6100000A62B6BAA8D4D5E401B59C85014483F40
0	329775.88	3.4621868e+06	1.3621305	0	2	3	0	3	2	1	3	0101000020E6100000381E18B28D4D5E40D804FDAE14483F40
0	329776.12	3.4621872e+06	1.2985166	0	2	3	0	3	2	1	3	0101000020E6100000006710BC8D4D5E4057277B0C15483F40
0	329776.38	3.462188e+06	1.2309209	0	2	3	0	3	2	1	3	0101000020E6100000B2B157C78D4D5E40B7E9305F15483F40
0	329776.66	3.4621885e+06	1.1621931	0	2	3	0	3	2	1	3	0101000020E610000015AFA0D38D4D5E40E41C99A815483F40
0	329777.06	3.4621888e+06	1.0686951	0	2	3	0	3	2	1	3	0101000020E6100000FF05EAE38D4D5E40753E6FF315483F40
0	329777.4	3.4621892e+06	0.97105587	0	2	3	0	3	2	1	3	0101000020E6100000A04EF8F38D4D5E40AE2A6E2916483F40
0	329777.88	3.4621895e+06	0.86300373	0	2	3	0	3	2	1	3	0101000020E610000057B537078E4D5E4071503E5D16483F40
0	329778.3	3.4621898e+06	0.7733257	0	2	3	0	3	2	1	3	0101000020E610000021DB641A8E4D5E40055F288A16483F40
0	329778.75	3.46219e+06	0.70077664	0	2	3	0	3	2	1	3	0101000020E6100000A838182E8E4D5E40D902C3B416483F40
0	329779.22	3.4621905e+06	0.64596814	0	2	3	0	3	2	1	3	0101000020E61000006E1F82428E4D5E4014DABADF16483F40
0	329779.7	3.4621908e+06	0.59848005	0	2	3	0	3	2	1	3	0101000020E61000007D1427578E4D5E402DAA0E0817483F40
0	329780.25	3.462191e+06	0.54020816	0	2	3	0	3	2	1	3	0101000020E610000086DD5E6F8E4D5E4004E4DF3017483F40
0	329780.75	3.462191e+06	0.47427034	0	2	3	0	3	2	1	3	0101000020E61000005B44F3848E4D5E40570D1B4C17483F40
0	329781.3	3.4621912e+06	0.37703577	0	2	3	0	3	2	1	3	0101000020E6100000B604889E8E4D5E40B9E6C65F17483F40
0	329781.8	3.4621912e+06	0.29099315	0	2	3	0	3	2	1	3	0101000020E6100000E53E71B48E4D5E40FE8B756817483F40
0	329782.44	3.4621912e+06	0.18603702	0	2	3	0	3	2	1	3	0101000020E61000002AFB28CF8E4D5E408135EB6917483F40
0	329782.94	3.4621912e+06	0.09763367	0	2	3	0	3	2	1	3	0101000020E6100000CF07EAE58E4D5E40B6F9CB6317483F40
0	329783.47	3.4621912e+06	0.01754384	0	2	3	0	3	2	1	3	0101000020E6100000DC0FE5FC8E4D5E404DBDBF5817483F40
0	329783.97	3.462191e+06	-0.008000024	0	2	3	0	3	2	1	3	0101000020E61000007BD6E4128F4D5E40DAF0B55417483F40
0	329784.5	3.462191e+06	-0.0069229696	0	2	3	0	3	2	1	3	0101000020E61000006F88E9298F4D5E4094029D5517483F40
5	329785	3.462191e+06	-0.0058601894	0	2	3	0	3	2	1	3	0101000020E61000008FA645408F4D5E407366945617483F40
5	329785.56	3.462191e+06	-0.00465494	0	2	3	0	3	2	1	3	0101000020E6100000ABDD23598F4D5E405C30C05717483F40
0	329775	3.4621215e+06	1.8038807	0	2	3	0	3	2	1	3	0101000020E6100000889366B98D4D5E405439550DEE473F40
0	329774.97	3.462122e+06	1.7434758	0	2	3	0	3	2	1	3	0101000020E6100000E6FCF1B78D4D5E409F3B9C5CEE473F40
0	329774.97	3.4621225e+06	1.6887585	0	2	3	0	3	2	1	3	0101000020E6100000BDD589B78D4D5E4084AC14AAEE473F40
0	329774.97	3.462123e+06	1.6379862	0	2	3	0	3	2	1	3	0101000020E6100000BCDBB5B78D4D5E4016F76A02EF473F40
8	329775	3.4621242e+06	1.5903852	0	2	3	0	3	2	1	3	0101000020E6100000D19477B78D4D5E40F83AE2B5EF473F40
13	329753.34	3.462102e+06	0.022050438	0	2	3	0	3	2	1	3	0101000020E61000000BDB4C0E8A4D5E404456F36FE2473F40
0	329754	3.4621022e+06	0.03042395	0	2	3	0	3	2	1	3	0101000020E6100000B0B01A2A8A4D5E408F3AB675E2473F40
0	329754.62	3.4621022e+06	0.044390105	0	2	3	0	3	2	1	3	0101000020E610000024D4CF458A4D5E40CE99A37DE2473F40
0	329755.12	3.4621022e+06	0.058370102	0	2	3	0	3	2	1	3	0101000020E6100000A750315C8A4D5E4075BCA785E2473F40
0	329755.7	3.4621022e+06	0.07729934	0	2	3	0	3	2	1	3	0101000020E610000061D296758A4D5E407904C990E2473F40
0	329756.28	3.4621022e+06	0.099859014	0	2	3	0	3	2	1	3	0101000020E6100000844F368F8A4D5E402E95809EE2473F40
0	329756.9	3.4621025e+06	0.12679963	0	2	3	0	3	2	1	3	0101000020E61000002DC238AB8A4D5E40FE8F56B0E2473F40
0	329757.66	3.4621025e+06	0.15897119	0	2	3	0	3	2	1	3	0101000020E6100000DD36DFCA8A4D5E400B7901C8E2473F40
0	329758.34	3.4621028e+06	0.19249411	0	2	3	0	3	2	1	3	0101000020E6100000987B25E98A4D5E4038D996E2E2473F40
0	329759.03	3.462103e+06	0.22931771	0	2	3	0	3	2	1	3	0101000020E610000005CAE0078B4D5E4089544902E3473F40
0	329759.75	3.4621032e+06	0.26808214	0	2	3	0	3	2	1	3	0101000020E6100000DE5FC3278B4D5E40B28A7327E3473F40
0	329760.28	3.4621035e+06	0.29664445	0	2	3	0	3	2	1	3	0101000020E6100000DE3FD93E8B4D5E4019C56445E3473F40
0	329760.97	3.4621038e+06	0.33475462	0	2	3	0	3	2	1	3	0101000020E6100000E48C2E5D8B4D5E40C1CAB070E3473F40
0	329761.62	3.462104e+06	0.37069649	0	2	3	0	3	2	1	3	0101000020E6100000D36A0B798B4D5E400F8D109DE3473F40
0	329762.25	3.4621042e+06	0.40739888	0	2	3	0	3	2	1	3	0101000020E610000009B4C2948B4D5E40D77A5FCDE3473F40
0	329762.8	3.4621045e+06	0.43989903	0	2	3	0	3	2	1	3	0101000020E6100000219FB3AC8B4D5E404DE1E8FAE3473F40
0	329763.3	3.4621048e+06	0.4702842	0	2	3	0	3	2	1	3	0101000020E610000064988AC28B4D5E40EEE5C027E4473F40
0	329763.78	3.4621052e+06	0.49933672	0	2	3	0	3	2	1	3	0101000020E610000024E8EBD68B4D5E4083589954E4473F40
0	329764.2	3.4621055e+06	0.5261192	0	2	3	0	3	2	1	3	0101000020E6100000783B41E98B4D5E407F55937FE4473F40
0	329764.7	3.4621058e+06	0.55901283	0	2	3	0	3	2	1	3	0101000020E6100000AC63A6FE8B4D5E40318B50B5E4473F40
0	329765.12	3.462106e+06	0.5887357	0	2	3	0	3	2	1	3	0101000020E610000018194A118C4D5E40BD9F73E7E4473F40
0	329765.56	3.4621065e+06	0.62180275	0	2	3	0	3	2	1	3	0101000020E6100000B4DE77258C4D5E4019B96421E5473F40
0	329765.97	3.4621068e+06	0.65120435	0	2	3	0	3	2	1	3	0101000020E61000000A7DE8368C4D5E40D4DDC056E5473F40
0	329766.38	3.4621072e+06	0.6798705	0	2	3	0	3	2	1	3	0101000020E6100000FA19F3478C4D5E401E12C78DE5473F40
0	329766.78	3.4621075e+06	0.7078325	0	2	3	0	3	2	1	3	0101000020E61000007A88585A8C4D5E408A9390CBE5473F40
0	329767.28	3.462108e+06	0.7345442	0	2	3	0	3	2	1	3	0101000020E61000002BB6006F8C4D5E4042DB4D13E6473F40
0	329767.66	3.4621085e+06	0.75389194	0	2	3	0	3	2	1	3	0101000020E610000066A6357F8C4D5E40754A984DE6473F40
0	329768.03	3.4621088e+06	0.77442455	0	2	3	0	3	2	1	3	0101000020E6100000259127908C4D5E4012AAF48CE6473F40
0	329768.4	3.4621092e+06	0.7941263	0	2	3	0	3	2	1	3	0101000020E6100000AF26A0A08C4D5E40CA08D4CCE6473F40
0	329768.8	3.4621098e+06	0.81328034	0	2	3	0	3	2	1	3	0101000020E610000071B4F2B18C4D5E4009831312E7473F40
0	329769.25	3.4621102e+06	0.8329294	0	2	3	0	3	2	1	3	0101000020E6100000766204C48C4D5E403CABF55CE7473F40
0	329769.62	3.4621108e+06	0.85219485	0	2	3	0	3	2	1	3	0101000020E6100000E0BF62D58C4D5E40D95ECBA7E7473F40
0	329770.06	3.4621112e+06	0.8733572	0	2	3	0	3	2	1	3	0101000020E61000008A4F04E88C4D5E409B346BFBE7473F40
0	329770.5	3.4621118e+06	0.8956928	0	2	3	0	3	2	1	3	0101000020E6100000877265FA8C4D5E40CD84FB51E8473F40
0	329770.94	3.4621125e+06	0.9203035	0	2	3	0	3	2	1	3	0101000020E610000024D4F50C8D4D5E4070D33BAEE8473F40
0	329771.22	3.4621128e+06	0.93733805	0	2	3	0	3	2	1	3	0101000020E6100000426664198D4D5E40734DD1EEE8473F40
0	329771.53	3.4621132e+06	0.95567536	0	2	3	0	3	2	1	3	0101000020E61000008DBC6F268D4D5E40CDD93A35E9473F40
0	329771.8	3.4621138e+06	0.9739123	0	2	3	0	3	2	1	3	0101000020E61000002CA00E338D4D5E40671B267CE9473F40
0	329772.12	3.4621142e+06	0.9929066	0	2	3	0	3	2	1	3	0101000020E61000001B23C53F8D4D5E40DB11AEC6E9473F40
0	329772.38	3.4621148e+06	1.0107192	0	2	3	0	3	2	1	3	0101000020E61000001B13504B8D4D5E403A6D430DEA473F40
0	329772.66	3.4621152e+06	1.0307283	0	2	3	0	3	2	1	3	0101000020E6100000CA18A8578D4D5E400450515CEA473F40
0	329772.94	3.4621158e+06	1.0516771	0	2	3	0	3	2	1	3	0101000020E61000006C4136638D4D5E4089AEB1AAEA473F40
0	329773.2	3.4621162e+06	1.0739319	0	2	3	0	3	2	1	3	0101000020E61000009B3A4D6E8D4D5E400E0795FAEA473F40
0	329773.44	3.4621168e+06	1.0962539	0	2	3	0	3	2	1	3	0101000020E610000041A8CA788D4D5E406B7DAE4AEB473F40
0	329773.7	3.4621172e+06	1.1198725	0	2	3	0	3	2	1	3	0101000020E61000009D070D838D4D5E4052A55E9EEB473F40
0	329773.9	3.4621178e+06	1.144539	0	2	3	0	3	2	1	3	0101000020E6100000AFAF878C8D4D5E402171ECF1EB473F40
0	329774.12	3.4621185e+06	1.171841	0	2	3	0	3	2	1	3	0101000020E610000060EDA4958D4D5E40B9DBB549EC473F40
0	329774.3	3.462119e+06	1.1999214	0	2	3	0	3	2	1	3	0101000020E6100000A4E39A9D8D4D5E40B2EECD9EEC473F40
0	329774.5	3.4621195e+06	1.2301216	0	2	3	0	3	2	1	3	0101000020E6100000B2E1B6A48D4D5E40F8808AF4EC473F40
0	329774.66	3.4621202e+06	1.2622726	0	2	3	0	3	2	1	3	0101000020E6100000A6DA23AB8D4D5E40F48EF74CED473F40
0	329774.78	3.4621208e+06	1.2925469	0	2	3	0	3	2	1	3	0101000020E61000007F2555B08D4D5E408C94A29FED473F40
0	329774.88	3.4621212e+06	1.321508	0	2	3	0	3	2	1	3	0101000020E6100000DE007AB48D4D5E403B6586EDED473F40
0	329774.97	3.4621218e+06	1.3559551	0	2	3	0	3	2	1	3	0101000020E6100000916F5EB88D4D5E40DE555548EE473F40
0	329780.78	3.462191e+06	0.019054916	0	2	3	0	3	2	1	3	0101000020E6100000D0C583868E4D5E403718EE4417483F40
0	329783.1	3.462191e+06	0.022963373	0	2	3	0	3	2	1	3	0101000020E61000005323F9EC8E4D5E4023C0CF5217483F40
0	329783.72	3.462191e+06	0.023692546	0	2	3	0	3	2	1	3	0101000020E6100000352A38088F4D5E4059DEA65617483F40
0	329784.28	3.4621912e+06	0.024339164	0	2	3	0	3	2	1	3	0101000020E610000007AA4E208F4D5E40D7C51A5A17483F40
5	329784.6	3.4621912e+06	0.024693362	0	2	3	2	3	2	1	3	0101000020E6100000BBA4202E8F4D5E4094291B5C17483F40
5	329785.1	3.4621912e+06	0.024547422	0	0	3	0	3	2	1	3	0101000020E6100000C3665A448F4D5E4039E0B15E17483F40
5	329785.66	3.4621912e+06	0.022278938	0	0	3	0	3	2	1	3	0101000020E6100000C44D6E5D8F4D5E40DBA2A06117483F40
5	329786.28	3.4621912e+06	0.01929513	0	0	3	0	3	2	1	3	0101000020E610000043A560798F4D5E40A21B8E6417483F40
5	329786.88	3.4621912e+06	0.0165084	0	0	3	0	3	2	1	3	0101000020E61000002E8897938F4D5E4004C50C6717483F40
5	329787.53	3.4621912e+06	0.013538444	0	0	3	0	3	2	1	3	0101000020E61000009FB7A4AF8F4D5E402161736917483F40
5	329788.03	3.4621912e+06	0.011094197	0	0	3	0	3	2	1	3	0101000020E6100000D143A7C68F4D5E40F27D356B17483F40
5	329788.6	3.4621912e+06	0.008522115	0	0	3	0	3	2	1	3	0101000020E6100000415F8CDE8F4D5E40EA18D36C17483F40
5	329789.22	3.4621912e+06	0.0054802517	0	0	3	0	3	2	1	3	0101000020E61000000E5CC2FA8F4D5E404049766E17483F40
5	329789.75	3.4621912e+06	0.0029369344	0	0	3	0	3	2	1	3	0101000020E6100000816B4F12904D5E400B589A6F17483F40
5	329790.28	3.4621912e+06	0.00044101945	0	0	3	0	3	2	1	3	0101000020E6100000A0386429904D5E4060D9847017483F40
5	329790.78	3.4621912e+06	-0.0019228096	0	0	3	0	3	2	1	3	0101000020E6100000AF8B7A3F904D5E401C17377117483F40
5	329791.28	3.4621912e+06	-0.0041264403	0	0	3	0	3	2	1	3	0101000020E6100000578CE355904D5E409A42C87117483F40
5	329791.94	3.4621912e+06	-0.006845599	0	0	3	0	3	2	1	3	0101000020E6100000BB42C472904D5E40CE654A7217483F40
5	329792.6	3.4621912e+06	-0.008730148	0	0	3	0	3	2	1	3	0101000020E6100000121CDB8E904D5E405AEBB77217483F40
5	329793.25	3.4621912e+06	-0.009328051	0	0	3	0	3	2	1	3	0101000020E61000005B82F1AB904D5E404ED04D7317483F40
5	329793.75	3.4621912e+06	-0.009616929	0	0	3	0	3	2	1	3	0101000020E61000000C0E0BC2904D5E406A33BF7317483F40
5	329794.44	3.4621912e+06	-0.0100050485	0	0	3	0	3	2	1	3	0101000020E610000048FBF4DF904D5E40E85E507417483F40
5	329794.94	3.4621912e+06	-0.010301028	0	0	3	0	3	2	1	3	0101000020E6100000433ED6F6904D5E40FE2AB97417483F40
5	329795.5	3.462191e+06	-0.010611405	0	0	3	0	3	2	1	3	0101000020E610000000052B0F914D5E4013F7217517483F40
5	329796.06	3.462191e+06	-0.009909079	0	0	3	0	3	2	1	3	0101000020E6100000F1714027914D5E405F76C67517483F40
5	329796.66	3.462191e+06	-0.0071469108	0	0	3	0	3	2	1	3	0101000020E610000003820B42914D5E401594057717483F40
5	329797.25	3.462191e+06	-0.004155108	0	0	3	0	3	2	1	3	0101000020E610000006C1645C914D5E40B1FD8B7817483F40
5	329797.88	3.462191e+06	-0.0010434535	0	0	3	0	3	2	1	3	0101000020E6100000E7CDCF77914D5E40172B6C7A17483F40
5	329798.53	3.462191e+06	0.0021369203	0	0	3	0	3	2	1	3	0101000020E61000007D81E493914D5E406ED2A47C17483F40
5	329799.16	3.4621912e+06	0.0053433664	0	0	3	0	3	2	1	3	0101000020E6100000875E44B0914D5E401984327F17483F40
5	329799.84	3.4621912e+06	0.008362327	0	0	3	0	3	2	1	3	0101000020E610000062E1D0CD914D5E409C8B198217483F40
5	329800.5	3.4621912e+06	0.010043312	0	0	3	0	3	2	1	3	0101000020E610000025083BEB914D5E406468F58417483F40
5	329801.2	3.4621912e+06	0.008828378	0	0	3	0	3	2	1	3	0101000020E61000003D1AAA09924D5E40C0DE598717483F40
5	329801.9	3.4621912e+06	0.0049950243	0	0	3	0	3	2	1	3	0101000020E6100000E303A029924D5E409892248917483F40
5	329802.4	3.4621912e+06	0.0022828614	0	0	3	0	3	2	1	3	0101000020E6100000104AE13F924D5E403EB3268A17483F40
5	329803.16	3.4621912e+06	-0.0008454544	0	0	3	0	3	2	1	3	0101000020E6100000ACFF995F924D5E4084AE698B17483F40
5	329803.84	3.4621912e+06	-0.0029597923	0	0	3	0	3	2	1	3	0101000020E610000084ECEF7E924D5E4035998E8C17483F40
5	329804.62	3.4621912e+06	-0.003575078	0	0	3	0	3	2	1	3	0101000020E6100000DA943FA0924D5E401D37EF8D17483F40
5	329805.38	3.4621912e+06	-0.0014506743	0	0	3	0	3	2	1	3	0101000020E6100000ADF18AC1924D5E40574FEB8F17483F40
5	329805.88	3.4621912e+06	0.0008617988	0	0	3	0	3	2	1	3	0101000020E61000003F3F5ED8924D5E40CB3A969117483F40
5	329806.4	3.4621912e+06	0.0032522525	0	0	3	0	3	2	1	3	0101000020E61000003F1F74EF924D5E403EFA759317483F40
5	329806.9	3.4621912e+06	0.005570799	0	0	3	0	3	2	1	3	0101000020E610000064D4D805934D5E40E239779517483F40
5	329807.47	3.4621912e+06	0.0079671135	0	0	3	0	3	2	1	3	0101000020E61000008B903B1E934D5E408F17D09717483F40
5	329808	3.4621912e+06	0.009945308	0	0	3	0	3	2	1	3	0101000020E61000009EDEF735934D5E40D269359A17483F40
5	329808.53	3.4621912e+06	0.011503256	0	0	3	0	3	2	1	3	0101000020E61000003FFD3C4D934D5E408B759F9C17483F40
5	329809.1	3.4621912e+06	0.012438561	0	0	3	0	3	2	1	3	0101000020E6100000E904A465934D5E40C7681F9F17483F40
5	329809.62	3.4621912e+06	0.01217544	0	0	3	0	3	2	1	3	0101000020E6100000B8E1597D934D5E402AA257A117483F40
5	329810.2	3.4621912e+06	0.010254766	0	0	3	0	3	2	1	3	0101000020E610000012447D95934D5E40CFA935A317483F40
5	329810.72	3.4621912e+06	0.0077066547	0	0	3	0	3	2	1	3	0101000020E61000001B0DB5AD934D5E40896AD5A417483F40
5	329811.28	3.4621912e+06	0.005116464	0	0	3	0	3	2	1	3	0101000020E6100000DBC7B1C5934D5E40F3533AA617483F40
5	329811.8	3.4621912e+06	0.0027025756	0	0	3	0	3	2	1	3	0101000020E610000030E413DD934D5E4092B168A717483F40
5	329812.38	3.4621912e+06	0.0004048005	0	0	3	0	3	2	1	3	0101000020E61000000B4D4CF6934D5E40B4BE89A817483F40
5	329812.9	3.4621912e+06	-0.0014480072	0	0	3	0	3	2	1	3	0101000020E6100000717F0F0D944D5E40C5CE6DA917483F40
5	329813.44	3.4621912e+06	-0.0033041309	0	0	3	0	3	2	1	3	0101000020E610000088702C25944D5E4054F73BAA17483F40
5	329814	3.4621912e+06	-0.0051587117	0	0	3	0	3	2	1	3	0101000020E6100000F497693D944D5E404878E3AA17483F40
5	329814.53	3.4621912e+06	-0.007007524	0	0	3	0	3	2	1	3	0101000020E6100000BAE96E55944D5E40E02B62AB17483F40
5	329815.28	3.4621912e+06	-0.009532582	0	0	3	0	3	2	1	3	0101000020E6100000438FF275944D5E409EF9CDAB17483F40
5	329815.97	3.4621912e+06	-0.011904222	0	0	3	0	3	2	1	3	0101000020E6100000FED33894944D5E40E79DEEAB17483F40
5	329816.6	3.4621912e+06	-0.013921084	0	0	3	0	3	2	1	3	0101000020E6100000FB88B3B0944D5E408471E0AB17483F40
5	329817.2	3.4621912e+06	-0.014502804	0	0	3	0	3	2	1	3	0101000020E6100000B41045CA944D5E402C0FF5AB17483F40
5	329817.72	3.4621912e+06	-0.014346531	0	0	3	0	3	2	1	3	0101000020E6100000EEDCDCE1944D5E406FB81EAC17483F40
5	329818.22	3.462191e+06	-0.014152408	0	0	3	0	3	2	1	3	0101000020E6100000BD5B21F8944D5E405B634BAC17483F40
5	329818.53	3.462191e+06	-0.014041828	0	0	3	0	3	2	1	3	0101000020E6100000EF550A05954D5E40477266AC17483F40
5	329819.06	3.462191e+06	-0.009292578	0	1	3	0	3	2	1	3	0101000020E61000008A8CFF1C954D5E40F5553CAD17483F40
5	329819.56	3.462191e+06	-0.0064258394	0	1	3	0	3	2	1	3	0101000020E6100000DA5C7433954D5E40100478AE17483F40
5	329820.1	3.462191e+06	-0.0035135392	0	1	3	0	3	2	1	3	0101000020E61000003BF82E4A954D5E404841F0AF17483F40
5	329820.66	3.462191e+06	-0.0003515233	0	1	3	0	3	2	1	3	0101000020E61000003B36B662954D5E408FB3C8B117483F40
5	329821.16	3.462191e+06	0.0025118939	0	1	3	0	3	2	1	3	0101000020E6100000488FF878954D5E40080AB1B317483F40
5	329821.72	3.4621912e+06	0.005747635	0	1	3	0	3	2	1	3	0101000020E6100000F6E24C92954D5E4038CF1FB617483F40
5	329822.28	3.4621912e+06	0.00890508	0	1	3	0	3	2	1	3	0101000020E610000070E127AB954D5E4002D8C6B817483F40
5	329822.8	3.4621912e+06	0.011874501	0	1	3	0	3	2	1	3	0101000020E6100000358AA0C2954D5E40035C86BB17483F40
5	329823.44	3.4621912e+06	0.01523062	0	1	3	0	3	2	1	3	0101000020E6100000CBDF43DD954D5E401185ECBE17483F40
5	329824.06	3.4621912e+06	0.018694863	0	1	3	0	3	2	1	3	0101000020E61000006C18DDF8954D5E403B11C4C217483F40
5	329824.6	3.4621912e+06	0.02153166	0	1	3	0	3	2	1	3	0101000020E6100000219F9C10964D5E40864C4BC617483F40
5	329825.16	3.4621912e+06	0.022392387	0	1	3	0	3	2	1	3	0101000020E6100000B3F99929964D5E40D9E6B7C917483F40
5	329825.75	3.4621912e+06	0.018137192	0	1	3	0	3	2	1	3	0101000020E6100000F8069943964D5E4073371ACC17483F40
5	329826.38	3.4621912e+06	0.012785835	0	1	3	0	3	2	1	3	0101000020E61000008F05C95E964D5E4076D4FDCD17483F40
5	329826.97	3.4621912e+06	0.008891762	0	1	3	0	3	2	1	3	0101000020E6100000BC655E79964D5E40229FAFCF17483F40
5	329827.6	3.4621912e+06	0.005281048	0	1	3	0	3	2	1	3	0101000020E61000002A435294964D5E40D1F91AD117483F40
5	329828.2	3.4621912e+06	0.0026557043	0	1	3	0	3	2	1	3	0101000020E6100000BDA44DAF964D5E40E54871D217483F40
5	329828.8	3.4621912e+06	0.0003131877	0	1	3	0	3	2	1	3	0101000020E6100000050466CA964D5E40E7369CD317483F40
5	329829.4	3.4621912e+06	-0.0004518668	0	1	3	0	3	2	1	3	0101000020E610000017B6BFE3964D5E40799EDCD417483F40
5	329830	3.4621912e+06	0.0010855043	0	1	3	0	3	2	1	3	0101000020E610000065F7FBFD964D5E405DE4A6D617483F40
5	329830.6	3.4621912e+06	0.0030514498	0	1	3	0	3	2	1	3	0101000020E6100000F40C0A18974D5E40B5B7AAD817483F40
5	329831.16	3.4621912e+06	0.0049673277	0	1	3	0	3	2	1	3	0101000020E6100000197E5131974D5E40BCBFCBDA17483F40
5	329831.66	3.4621912e+06	0.0066706855	0	1	3	0	3	2	1	3	0101000020E610000090CCA147974D5E40D6B8D1DC17483F40
5	329832.25	3.4621912e+06	0.008617457	0	1	3	0	3	2	1	3	0101000020E610000064A4FD60974D5E40253948DF17483F40
5	329832.75	3.4621912e+06	0.010331998	0	1	3	0	3	2	1	3	0101000020E61000008A533677974D5E4017EC95E117483F40
5	329833.25	3.4621912e+06	0.01213123	0	1	3	0	3	2	1	3	0101000020E6100000CA071E8E974D5E40E2BC19E417483F40
5	329833.28	3.4621912e+06	0.012198874	0	1	3	1	3	2	1	3	0101000020E61000007EB4F38E974D5E400DA632E417483F40
0	329833.8	3.4621912e+06	0.0074041192	0	2	3	0	3	2	1	3	0101000020E61000003EC037A6974D5E4086B150E517483F40
0	329834.34	3.4621912e+06	0.001071656	0	2	3	0	3	2	1	3	0101000020E6100000E18709BE974D5E40BDC87AE517483F40
0	329834.84	3.4621912e+06	-0.008478053	0	2	3	0	3	2	1	3	0101000020E61000002CBB49D4974D5E40637864E417483F40
0	329835.44	3.4621912e+06	-0.022265015	0	2	3	0	3	2	1	3	0101000020E61000001AE36FED974D5E4081139BE117483F40
0	329835.97	3.4621912e+06	-0.040933188	0	2	3	0	3	2	1	3	0101000020E61000002B8ECB04984D5E40EBD581DC17483F40
0	329836.5	3.462191e+06	-0.06755837	0	2	3	0	3	2	1	3	0101000020E610000071372D1D984D5E40B4C2D2D317483F40
0	329837.06	3.462191e+06	-0.10595062	0	2	3	0	3	2	1	3	0101000020E6100000D1DF1136984D5E404533DBC517483F40
0	329837.56	3.4621908e+06	-0.14751337	0	2	3	0	3	2	1	3	0101000020E61000005A44C34B984D5E40294FF5B417483F40
0	329838.06	3.4621908e+06	-0.19506177	0	2	3	0	3	2	1	3	0101000020E61000009C43C661984D5E404950359F17483F40
0	329838.56	3.4621905e+06	-0.24897416	0	2	3	0	3	2	1	3	0101000020E6100000C8890778984D5E4080D1E38317483F40
0	329839.06	3.4621902e+06	-0.30652463	0	2	3	0	3	2	1	3	0101000020E6100000C2235C8E984D5E401ADB0F6317483F40
0	329839.56	3.46219e+06	-0.36670324	0	2	3	0	3	2	1	3	0101000020E6100000A2BE2DA4984D5E40B37F1D3D17483F40
0	329840.03	3.4621898e+06	-0.42659327	0	2	3	0	3	2	1	3	0101000020E610000064B1EFB8984D5E40A04C4F1317483F40
0	329840.47	3.4621895e+06	-0.4898632	0	2	3	0	3	2	1	3	0101000020E6100000511635CD984D5E40E373EDE316483F40
0	329840.9	3.462189e+06	-0.5556178	0	2	3	0	3	2	1	3	0101000020E6100000ACAF4BE0984D5E40D6450EB016483F40
0	329841.3	3.4621888e+06	-0.6247014	0	2	3	0	3	2	1	3	0101000020E61000003E3412F2984D5E40D1249D7716483F40
0	329841.66	3.4621882e+06	-0.6910731	0	2	3	0	3	2	1	3	0101000020E6100000015EC701994D5E40FB31343E16483F40
0	329841.97	3.462188e+06	-0.7585422	0	2	3	0	3	2	1	3	0101000020E610000081619110994D5E407059510016483F40
0	329842.28	3.4621875e+06	-0.82475907	0	2	3	0	3	2	1	3	0101000020E61000005837DE1D994D5E40871662C015483F40
0	329842.56	3.462187e+06	-0.89396137	0	2	3	0	3	2	1	3	0101000020E61000000543622A994D5E402E9D9F7A15483F40
0	329842.78	3.4621865e+06	-0.96331406	0	2	3	0	3	2	1	3	0101000020E61000003EC72935994D5E408ABD5C3315483F40
0	329843	3.462186e+06	-1.0359434	0	2	3	0	3	2	1	3	0101000020E6100000E0E28D3E994D5E4082CFDAE714483F40
0	329843.16	3.4621855e+06	-1.1075964	0	2	3	0	3	2	1	3	0101000020E61000007FFD1F46994D5E408ACBEE9B14483F40
0	329843.28	3.462185e+06	-1.1811658	0	2	3	0	3	2	1	3	0101000020E61000005E8E124C994D5E404B0A604D14483F40
0	329843.38	3.4621845e+06	-1.2556746	0	2	3	0	3	2	1	3	0101000020E6100000BD693750994D5E40AA99D5FD13483F40
0	329843.4	3.462184e+06	-1.3371675	0	2	3	0	3	2	1	3	0101000020E61000006DD74952994D5E40484766AB13483F40
0	329843.4	3.4621835e+06	-1.417305	0	2	3	0	3	2	1	3	0101000020E6100000110A2152994D5E4025ABA85E13483F40
0	329843.34	3.462183e+06	-1.5047908	0	2	3	0	3	2	1	3	0101000020E6100000C6F4E74F994D5E406F031A0B13483F40
0	329843.25	3.4621822e+06	-1.5868853	0	2	3	0	3	2	1	3	0101000020E6100000A4F9EC4B994D5E4085E144B912483F40
6	329843.16	3.462182e+06	-1.6357403	0	2	3	2	3	2	1	3	0101000020E610000018B89C48994D5E4053542B8312483F40
6	329843.12	3.4621815e+06	-1.6249288	0	0	3	0	3	2	1	3	0101000020E6100000F0903448994D5E40049CDC3212483F40
6	329843.12	3.462181e+06	-1.6133285	0	0	3	0	3	2	1	3	0101000020E610000029D42948994D5E403DFEBDE211483F40
6	329843.12	3.4621805e+06	-1.6015191	0	0	3	0	3	2	1	3	0101000020E610000091CF6348994D5E40E394709111483F40
6	329843.12	3.46218e+06	-1.5904652	0	0	3	0	3	2	1	3	0101000020E610000023ECD948994D5E401B9A104511483F40
6	329843.12	3.4621792e+06	-1.5783043	0	0	3	0	3	2	1	3	0101000020E61000000FDCA449994D5E40838DB8EE10483F40
6	329843.16	3.4621788e+06	-1.5669863	0	0	3	0	3	2	1	3	0101000020E61000000C97B54A994D5E401694429110483F40
6	329843.16	3.462178e+06	-1.5582956	0	0	3	0	3	2	1	3	0101000020E6100000DD3CE24B994D5E40C0CA233410483F40
6	329843.2	3.4621775e+06	-1.552994	0	0	3	0	3	2	1	3	0101000020E610000038BFEF4C994D5E40B005F7E20F483F40
6	329843.2	3.462177e+06	-1.5498633	0	0	3	0	3	2	1	3	0101000020E610000079EB064E994D5E40391E128B0F483F40
6	329843.22	3.4621765e+06	-1.5508907	0	0	3	0	3	2	1	3	0101000020E6100000B0E3E04E994D5E40BE3C08350F483F40
6	329843.22	3.4621758e+06	-1.555186	0	0	3	0	3	2	1	3	0101000020E61000007FE0804F994D5E40D1B442DC0E483F40
6	329843.22	3.4621752e+06	-1.5593088	0	0	3	0	3	2	1	3	0101000020E61000004FD7F44F994D5E40C7D1E88B0E483F40
6	329843.22	3.4621748e+06	-1.5634922	0	0	3	0	3	2	1	3	0101000020E61000003FBB6750994D5E409EE58A2E0E483F40
6	329843.22	3.462174e+06	-1.5674428	0	0	3	0	3	2	1	3	0101000020E61000000384CA50994D5E40CA8BBCCB0D483F40
6	329843.22	3.4621735e+06	-1.5699569	0	0	3	0	3	2	1	3	0101000020E610000072160D51994D5E4096A17A7F0D483F40
6	329843.22	3.462173e+06	-1.5723157	0	0	3	0	3	2	1	3	0101000020E610000038D94351994D5E40A8F01B330D483F40
6	329843.22	3.4621725e+06	-1.5744754	0	0	3	0	3	2	1	3	0101000020E610000036DF6F51994D5E40D363BCE60C483F40
6	329843.2	3.462172e+06	-1.5766588	0	0	3	0	3	2	1	3	0101000020E61000006D289151994D5E40F412B7980C483F40
6	329843.2	3.4621715e+06	-1.5788552	0	0	3	0	3	2	1	3	0101000020E6100000FCA1A651994D5E4083092A4A0C483F40
6	329843.2	3.462171e+06	-1.5812855	0	0	3	0	3	2	1	3	0101000020E61000000239AF51994D5E401C3229F30B483F40
6	329843.2	3.4621702e+06	-1.5835093	0	0	3	0	3	2	1	3	0101000020E6100000BDC7A851994D5E4062DB3FA20B483F40
6	329843.16	3.4621698e+06	-1.585611	0	0	3	0	3	2	1	3	0101000020E610000092AC9851994D5E4099FEAE510B483F40
6	329843.16	3.4621692e+06	-1.5877492	0	0	3	0	3	2	1	3	0101000020E61000001D897951994D5E405E81DFFC0A483F40
6	329843.16	3.4621688e+06	-1.5898379	0	0	3	0	3	2	1	3	0101000020E6100000E0A84F51994D5E40A41DCCA90A483F40
6	329843.12	3.4621682e+06	-1.592035	0	0	3	0	3	2	1	3	0101000020E6100000979A1451994D5E40542B58520A483F40
6	329843.12	3.4621675e+06	-1.594203	0	0	3	0	3	2	1	3	0101000020E6100000C5A9CC50994D5E40E86B07FC09483F40
6	329843.1	3.462167e+06	-1.5963285	0	0	3	0	3	2	1	3	0101000020E61000004BE97850994D5E401C4957A709483F40
6	329843.06	3.4621665e+06	-1.59851	0	0	3	0	3	2	1	3	0101000020E6100000A60D1550994D5E404CE6625009483F40
6	329843.06	3.4621658e+06	-1.6008497	0	0	3	0	3	2	1	3	0101000020E6100000B092994F994D5E40B2D708F308483F40
6	329843.03	3.4621652e+06	-1.6030327	0	0	3	0	3	2	1	3	0101000020E6100000D46D144F994D5E40DDE9059908483F40
6	329843	3.4621645e+06	-1.6039112	0	0	3	0	3	2	1	3	0101000020E6100000A0189B4E994D5E402BEA243F08483F40
6	329843	3.462164e+06	-1.6014917	0	0	3	0	3	2	1	3	0101000020E61000003286584E994D5E408A7D15E707483F40
6	329843	3.4621635e+06	-1.5960455	0	0	3	0	3	2	1	3	0101000020E6100000D4BE5B4E994D5E4060F6D58E07483F40
6	329843	3.4621628e+06	-1.5886408	0	0	3	0	3	2	1	3	0101000020E6100000A6AFA34E994D5E4007660A3707483F40
6	329843	3.4621622e+06	-1.5809015	0	0	3	0	3	2	1	3	0101000020E61000005D50214F994D5E40040C5FDF06483F40
6	329843	3.4621618e+06	-1.5745121	0	0	3	0	3	2	1	3	0101000020E610000045A3B74F994D5E40CEECA28806483F40
6	329843	3.4621612e+06	-1.569208	0	0	3	0	3	2	1	3	0101000020E61000003FBB6750994D5E400540582F06483F40
6	329843	3.4621605e+06	-1.5649545	0	0	3	0	3	2	1	3	0101000020E6100000A3C82551994D5E40C41E56D805483F40
6	329843	3.46216e+06	-1.5617898	0	0	3	0	3	2	1	3	0101000020E61000008FB8F051994D5E401370BC7D05483F40
6	329843.03	3.4621592e+06	-1.5596969	0	0	3	0	3	2	1	3	0101000020E61000009B95BA52994D5E405248A52505483F40
6	329843.03	3.4621588e+06	-1.5591184	0	0	3	0	3	2	1	3	0101000020E6100000C0C87A53994D5E4065626ECB04483F40
6	329843.03	3.4621582e+06	-1.560496	0	0	3	0	3	2	1	3	0101000020E610000051EB1C54994D5E402D61817004483F40
6	329843.03	3.4621575e+06	-1.5624565	0	0	3	0	3	2	1	3	0101000020E61000009705B054994D5E40F601231404483F40
6	329843.03	3.462157e+06	-1.5642291	0	0	3	0	3	2	1	3	0101000020E610000079C13D55994D5E4031AC3EB503483F40
6	329843.03	3.4621562e+06	-1.5643008	0	0	3	0	3	2	1	3	0101000020E6100000E45FD855994D5E401158BB5903483F40
6	329843.06	3.4621558e+06	-1.5617694	0	0	3	0	3	2	1	3	0101000020E61000008CDE9C56994D5E408C03530103483F40
6	329843.06	3.4621552e+06	-1.5578374	0	0	3	0	3	2	1	3	0101000020E6100000723D8B57994D5E40FA8656A702483F40
6	329843.06	3.4621545e+06	-1.5539814	0	0	3	0	3	2	1	3	0101000020E61000002A879558994D5E406BD8B34B02483F40
6	329843.1	3.462154e+06	-1.5505658	0	0	3	0	3	2	1	3	0101000020E61000008BA0AB59994D5E40D17FBFF201483F40
6	329843.1	3.4621532e+06	-1.5475588	0	0	3	0	3	2	1	3	0101000020E61000007A33D75A994D5E40D8BCBE9701483F40
6	329843.12	3.4621528e+06	-1.5451677	0	0	3	0	3	2	1	3	0101000020E610000051700C5C994D5E40E758A33C01483F40
6	329843.16	3.4621522e+06	-1.5440826	0	0	3	0	3	2	1	3	0101000020E61000002116395D994D5E40C94392E100483F40
6	329843.16	3.4621515e+06	-1.545353	0	0	3	0	3	2	1	3	0101000020E61000004355515E994D5E40F2B1FE8100483F40
6	329843.16	3.462151e+06	-1.5483915	0	0	3	0	3	2	1	3	0101000020E6100000867B3C5F994D5E40228C7A2400483F40
6	329843.2	3.4621502e+06	-1.5522819	0	0	3	0	3	2	1	3	0101000020E610000035910960994D5E404CC2C3C4FF473F40
6	329843.2	3.4621498e+06	-1.5563134	0	0	3	0	3	2	1	3	0101000020E6100000BF1CA360994D5E400FF9576DFF473F40
6	329843.2	3.4621492e+06	-1.5603099	0	0	3	0	3	2	1	3	0101000020E6100000B5971E61994D5E40F26EB318FF473F40
6	329843.2	3.4621485e+06	-1.5638098	0	0	3	0	3	2	1	3	0101000020E610000015027C61994D5E40A72ECCCAFE473F40
6	329843.2	3.462148e+06	-1.5677654	0	0	3	0	3	2	1	3	0101000020E6100000577FDA61994D5E40AC188269FE473F40
6	329843.2	3.4621472e+06	-1.5714475	0	0	3	0	3	2	1	3	0101000020E610000042C61862994D5E4088087A0EFE473F40
6	329843.16	3.4621468e+06	-1.5748469	0	0	3	0	3	2	1	3	0101000020E61000003A353C62994D5E4006D5B4BAFD473F40
6	329843.16	3.4621462e+06	-1.5781299	0	0	3	0	3	2	1	3	0101000020E6100000C3174962994D5E4005E1F969FD473F40
6	329843.16	3.4621458e+06	-1.5812769	0	0	3	0	3	2	1	3	0101000020E610000060B94362994D5E403742671BFD473F40
6	329843.16	3.4621452e+06	-1.5841277	0	0	3	0	3	2	1	3	0101000020E610000092653062994D5E403416E7CDFC473F40
6	329843.12	3.4621448e+06	-1.5873357	0	0	3	0	3	2	1	3	0101000020E610000036980762994D5E40E4164974FC473F40
6	329843.12	3.4621442e+06	-1.5898876	0	0	3	0	3	2	1	3	0101000020E6100000B546D761994D5E409F6D6A26FC473F40
6	329843.12	3.4621435e+06	-1.5879302	0	0	3	0	3	2	1	3	0101000020E6100000FF4EE661994D5E40E10208D3FB473F40
6	329843.1	3.462143e+06	-1.5830383	0	1	3	0	3	2	1	3	0101000020E6100000B7E93762994D5E40D8839C82FB473F40
6	329843.12	3.4621425e+06	-1.5780686	0	1	3	0	3	2	1	3	0101000020E6100000EC3EB162994D5E40B52BFC32FB473F40
6	329843.12	3.462142e+06	-1.5726624	0	1	3	0	3	2	1	3	0101000020E610000076CA4A63994D5E4003F657E3FA473F40
6	329843.12	3.4621415e+06	-1.5670877	0	1	3	0	3	2	1	3	0101000020E6100000379F0564994D5E4033317793FA473F40
6	329843.12	3.462141e+06	-1.5602233	0	1	3	0	3	2	1	3	0101000020E61000006D9D0B65994D5E409EAD2B39FA473F40
6	329843.16	3.4621402e+06	-1.5539	0	1	3	0	3	2	1	3	0101000020E610000070EF2466994D5E406A25D6E5F9473F40
6	329843.2	3.4621398e+06	-1.5474967	0	1	3	0	3	2	1	3	0101000020E610000058F17367994D5E40D851158EF9473F40
6	329843.2	3.4621392e+06	-1.5433002	0	1	3	0	3	2	1	3	0101000020E610000067719E68994D5E40EDA70542F9473F40
6	329843.22	3.4621388e+06	-1.5407789	0	1	3	0	3	2	1	3	0101000020E6100000516DC169994D5E40ED7C57F6F8473F40
6	329843.22	3.4621382e+06	-1.5412613	0	1	3	0	3	2	1	3	0101000020E610000009B7CB6A994D5E40B2B5EDA5F8473F40
6	329843.25	3.4621378e+06	-1.5456198	0	1	3	0	3	2	1	3	0101000020E6100000BD63A16B994D5E403205B84FF8473F40
6	329843.25	3.462137e+06	-1.550112	0	1	3	0	3	2	1	3	0101000020E61000000284606C994D5E401E9B28F7F7473F40
6	329843.25	3.4621365e+06	-1.5531375	0	1	3	0	3	2	1	3	0101000020E610000066911E6D994D5E40375FA19DF7473F40
6	329843.25	3.4621358e+06	-1.5557499	0	1	3	0	3	2	1	3	0101000020E6100000852DD66D994D5E4073C59F41F7473F40
6	329843.25	3.4621352e+06	-1.5579157	0	1	3	0	3	2	1	3	0101000020E6100000A4C98D6E994D5E4027F719E1F6473F40
6	329843.28	3.4621345e+06	-1.5598924	0	1	3	0	3	2	1	3	0101000020E6100000B737346F994D5E40FB036D83F6473F40
6	329843.28	3.462134e+06	-1.5617609	0	1	3	0	3	2	1	3	0101000020E610000023D6CE6F994D5E4003E97725F6473F40
6	329843.28	3.4621332e+06	-1.5637169	0	1	3	0	3	2	1	3	0101000020E61000002A166470994D5E401F5A49C2F5473F40
6	329843.28	3.4621328e+06	-1.56567	0	1	3	0	3	2	1	3	0101000020E6100000C860EB70994D5E409265295FF5473F40
6	329843.28	3.4621322e+06	-1.5671662	0	1	3	0	3	2	1	3	0101000020E610000009DE4971994D5E40117F3013F5473F40
6	329843.28	3.4621318e+06	-1.568682	0	1	3	0	3	2	1	3	0101000020E610000045C49F71994D5E40619528C6F4473F40
6	329843.28	3.4621312e+06	-1.5702412	0	1	3	0	3	2	1	3	0101000020E61000001D4CF071994D5E40AB6BDC76F4473F40
6	329843.28	3.4621308e+06	-1.571765	0	1	3	0	3	2	1	3	0101000020E61000004C043572994D5E40E1825129F4473F40
6	329843.28	3.46213e+06	-1.5732965	0	1	3	0	3	2	1	3	0101000020E610000057387272994D5E40A9CF60DBF3473F40
6	329843.25	3.4621295e+06	-1.5749233	0	1	3	0	3	2	1	3	0101000020E61000001DFBA872994D5E40C18E8C88F3473F40
6	329843.25	3.462129e+06	-1.5765176	0	1	3	0	3	2	1	3	0101000020E61000001B01D572994D5E4067C15037F3473F40
6	329843.25	3.4621285e+06	-1.5780324	0	1	3	0	3	2	1	3	0101000020E6100000524AF672994D5E40F77923EAF2473F40
9	329743	3.4622018e+06	-3.1226077	0	0	3	0	3	2	1	3	0101000020E61000002D0DC4FE874D5E40B439CD481D483F40
9	329742.44	3.4622018e+06	-3.1252196	0	0	3	0	3	2	1	3	0101000020E6100000D747D5E6874D5E40648D8B461D483F40
9	329741.84	3.4622018e+06	-3.1281462	0	0	3	0	3	2	1	3	0101000020E6100000F24CEECB874D5E4001B841441D483F40
9	329741.2	3.4622018e+06	-3.130905	0	0	3	0	3	2	1	3	0101000020E6100000CE1998AF874D5E406C360B421D483F40
9	329740.66	3.4622018e+06	-3.1321864	0	0	3	0	3	2	1	3	0101000020E610000084317398874D5E4088542F401D483F40
9	329740.16	3.4622018e+06	-3.13234	0	0	3	0	3	2	1	3	0101000020E6100000889D4A82874D5E4016F93D3E1D483F40
9	329739.6	3.4622018e+06	-3.1308823	0	0	3	0	3	2	1	3	0101000020E610000042967768874D5E4091619D3B1D483F40
9	329738.97	3.4622018e+06	-3.1280758	0	0	3	0	3	2	1	3	0101000020E61000004E79D74D874D5E404E607F381D483F40
9	329738.38	3.4622018e+06	-3.1247916	0	0	3	0	3	2	1	3	0101000020E610000010542833874D5E4002C109351D483F40
9	329737.7	3.4622018e+06	-3.1206653	0	0	3	0	3	2	1	3	0101000020E610000038C54315874D5E40DCF0B6301D483F40
9	329736.97	3.4622018e+06	-3.1164746	0	0	3	0	3	2	1	3	0101000020E61000000E3F02F6864D5E408461D02B1D483F40
9	329736.22	3.4622018e+06	-3.1131203	0	0	3	0	3	2	1	3	0101000020E6100000B5A20AD5864D5E40D78F71261D483F40
9	329735.47	3.4622018e+06	-3.1106133	0	0	3	0	3	2	1	3	0101000020E61000004E2F75B3864D5E40C426C7201D483F40
9	329734.72	3.4622018e+06	-3.109222	0	0	3	0	3	2	1	3	0101000020E6100000C9CEE091864D5E40144E191B1D483F40
9	329733.9	3.4622018e+06	-3.1094742	0	0	3	0	3	2	1	3	0101000020E610000015526A6E864D5E40D6FB55151D483F40
9	329733.06	3.4622015e+06	-3.111451	0	0	3	0	3	2	1	3	0101000020E61000007ACD7849864D5E400009BB0F1D483F40
9	329732.2	3.4622015e+06	-3.114539	0	0	3	0	3	2	1	3	0101000020E610000083CCA523864D5E4092AD6B0A1D483F40
9	329731.34	3.4622015e+06	-3.118327	0	0	3	0	3	2	1	3	0101000020E6100000CD9978FD854D5E4039BA96051D483F40
9	329730.44	3.4622015e+06	-3.122734	0	0	3	0	3	2	1	3	0101000020E6100000D68B7BD5854D5E4046FA1E011D483F40
9	329729.44	3.4622015e+06	-3.1276448	0	0	3	0	3	2	1	3	0101000020E6100000344F2FAA854D5E40A3ADF3FC1C483F40
9	329728.53	3.4622015e+06	-3.1322575	0	0	3	0	3	2	1	3	0101000020E6100000611CAD81854D5E406E06B2F91C483F40
9	329728	3.4622015e+06	-3.1349266	0	0	3	0	3	2	1	3	0101000020E610000000D2396A854D5E40054918F81C483F40
9	329727.03	3.4622015e+06	-3.1397812	0	0	3	0	3	2	1	3	0101000020E610000027FB843F854D5E409D40B4F51C483F40
9	329726.53	3.4622015e+06	3.1407921	0	0	3	0	3	2	1	3	0101000020E610000018A86E29854D5E40226DB9F41C483F40
9	329725.62	3.4622015e+06	3.1359515	0	0	3	0	3	2	1	3	0101000020E610000057E9BE01854D5E40661C60F31C483F40
9	329724.8	3.4622015e+06	3.131715	0	0	3	0	3	2	1	3	0101000020E6100000837F49DE844D5E404D49A8F21C483F40
9	329724	3.4622015e+06	3.1275837	0	0	3	0	3	2	1	3	0101000020E6100000E6B568BA844D5E405B0773F21C483F40
9	329723.34	3.4622015e+06	3.1243892	0	0	3	0	3	2	1	3	0101000020E6100000CFAAF79D844D5E402198B2F21C483F40
9	329722.75	3.4622015e+06	3.1214857	0	0	3	0	3	2	1	3	0101000020E610000027903A83844D5E40F7C140F31C483F40
9	329722.22	3.4622015e+06	3.1191063	0	0	3	0	3	2	1	3	0101000020E61000005C059E6C844D5E401095F8F31C483F40
9	329721.6	3.4622018e+06	3.1162453	0	0	3	0	3	2	1	3	0101000020E610000088201851844D5E40AEF226F51C483F40
9	329721.03	3.4622018e+06	3.113541	0	0	3	0	3	2	1	3	0101000020E610000025261A37844D5E4012E194F61C483F40
9	329720.5	3.4622018e+06	3.1140945	0	1	3	0	3	2	1	3	0101000020E61000008B476A20844D5E40F383FEF71C483F40
9	329719.94	3.4622018e+06	3.1191974	0	1	3	0	3	2	1	3	0101000020E6100000876CAE07844D5E40B79DA8F71C483F40
9	329719.4	3.4622018e+06	3.1248457	0	1	3	0	3	2	1	3	0101000020E6100000538866EF834D5E4010B5C9F61C483F40
9	329718.88	3.4622018e+06	3.1296759	0	1	3	0	3	2	1	3	0101000020E61000007B2000D8834D5E403FABAEF51C483F40
9	329718.25	3.4622018e+06	3.1342595	0	1	3	0	3	2	1	3	0101000020E6100000B21E70BD834D5E40AAD830F41C483F40
9	329717.56	3.4622018e+06	3.1391034	0	1	3	0	3	2	1	3	0101000020E610000060CF379F834D5E40904319F21C483F40
9	329717.06	3.4622018e+06	-3.1406271	0	1	3	0	3	2	1	3	0101000020E6100000A9AC1589834D5E4081B047F01C483F40
9	329716.53	3.4622018e+06	-3.1369886	0	1	3	0	3	2	1	3	0101000020E610000046BF4171834D5E4011E50FEE1C483F40
9	329715.94	3.4622018e+06	-3.133195	0	1	3	0	3	2	1	3	0101000020E610000069AD7C57834D5E405A695FEB1C483F40
9	329715.4	3.4622018e+06	-3.129824	0	1	3	0	3	2	1	3	0101000020E610000022B9FF3F834D5E408432A7E81C483F40
9	329714.88	3.4622018e+06	-3.1266806	0	1	3	0	3	2	1	3	0101000020E6100000C16E8C28834D5E40C94BB9E51C483F40
9	329714.34	3.4622015e+06	-3.123824	0	1	3	0	3	2	1	3	0101000020E6100000122E8E11834D5E404E07A6E21C483F40
9	329713.62	3.4622015e+06	-3.1202593	0	1	3	0	3	2	1	3	0101000020E6100000ABC722F2824D5E40491E26DE1C483F40
9	329713	3.4622015e+06	-3.1175334	0	1	3	0	3	2	1	3	0101000020E6100000402F1ED6824D5E40CCEBE7D91C483F40
9	329712.44	3.4622015e+06	-3.1154032	0	1	3	0	3	2	1	3	0101000020E61000008D3E95BC824D5E40E8FCE1D51C483F40
9	329711.88	3.4622015e+06	-3.1137948	0	1	3	0	3	2	1	3	0101000020E610000019867BA4824D5E40D2FD00D21C483F40
9	329711.38	3.4622015e+06	-3.1126947	0	1	3	0	3	2	1	3	0101000020E6100000DC74F48D824D5E4049B058CE1C483F40
9	329710.78	3.4622015e+06	-3.1114817	0	1	3	0	3	2	1	3	0101000020E61000002AD5B273824D5E40366DFCC91C483F40
9	329710.22	3.4622015e+06	-3.1103754	0	1	3	0	3	2	1	3	0101000020E610000005C2DC5B824D5E404DE7EDC51C483F40
9	329709.62	3.4622015e+06	-3.1091383	0	1	3	0	3	2	1	3	0101000020E610000086C85B41824D5E40C07F4FC11C483F40
9	329709.12	3.4622015e+06	-3.1082077	0	1	3	0	3	2	1	3	0101000020E6100000AD15DA2A824D5E408B2955BD1C483F40
9	329709.1	3.4622015e+06	-3.1081436	0	1	3	1	3	2	1	3	0101000020E610000098FEA629824D5E40FD771CBD1C483F40
0	329708.56	3.4622015e+06	-3.1107976	0	2	3	0	3	2	1	3	0101000020E610000072A0B512824D5E40DC25AEB91C483F40
0	329707.97	3.4622015e+06	-3.11245	0	2	3	0	3	2	1	3	0101000020E61000007204BDF8814D5E40745BEAB51C483F40
0	329707.38	3.4622015e+06	-3.1109593	0	2	3	0	3	2	1	3	0101000020E61000002760B5DE814D5E4030347EB11C483F40
0	329706.8	3.4622015e+06	-3.1070821	0	2	3	0	3	2	1	3	0101000020E61000008E74DBC5814D5E40290C8CAC1C483F40
0	329706.16	3.4622012e+06	-3.101758	0	2	3	0	3	2	1	3	0101000020E610000002464BA9814D5E407EB73FA61C483F40
0	329705.56	3.4622012e+06	-3.0960276	0	2	3	0	3	2	1	3	0101000020E61000004A60488E814D5E40C4C9A49F1C483F40
0	329705	3.4622012e+06	-3.0886245	0	2	3	0	3	2	1	3	0101000020E61000007E773A76814D5E40E11EBC981C483F40
0	329704.5	3.4622012e+06	-3.0787148	0	2	3	0	3	2	1	3	0101000020E6100000A6BE8C5F814D5E40A1F7F1901C483F40
0	329703.94	3.4622012e+06	-3.0658374	0	2	3	0	3	2	1	3	0101000020E610000026D86147814D5E40152B35871C483F40
0	329703.38	3.462201e+06	-3.0488122	0	2	3	0	3	2	1	3	0101000020E61000007CD0FA2E814D5E408F4C457B1C483F40
0	329702.8	3.462201e+06	-3.0257475	0	2	3	0	3	2	1	3	0101000020E6100000CA8E2A16814D5E4024662B6C1C483F40
0	329702.3	3.462201e+06	-3.003636	0	2	3	0	3	2	1	3	0101000020E6100000626293FF804D5E4062D25B5C1C483F40
0	329701.72	3.4622008e+06	-2.9796224	0	2	3	0	3	2	1	3	0101000020E61000008456FAE5804D5E40FAC595481C483F40
0	329701.2	3.4622008e+06	-2.9575903	0	2	3	0	3	2	1	3	0101000020E61000008124CBCD804D5E4029E311341C483F40
0	329700.56	3.4622005e+06	-2.9357069	0	2	3	0	3	2	1	3	0101000020E6100000499624B3804D5E407EAFDF1B1C483F40
0	329700.03	3.4622005e+06	-2.9177961	0	2	3	0	3	2	1	3	0101000020E61000006C40429C804D5E40BB90A3051C483F40
0	329699.5	3.4622002e+06	-2.8987474	0	2	3	0	3	2	1	3	0101000020E6100000BBA7FE83804D5E40CDCC6BEC1B483F40
0	329698.9	3.4622e+06	-2.8781264	0	2	3	0	3	2	1	3	0101000020E61000004D287C6A804D5E404DCCECCF1B483F40
0	329698.3	3.4621998e+06	-2.8563116	0	2	3	0	3	2	1	3	0101000020E610000035306150804D5E40B5EB8BB01B483F40
0	329697.72	3.4621998e+06	-2.8323534	0	2	3	0	3	2	1	3	0101000020E610000093ACAC35804D5E40CF07C78D1B483F40
0	329697.06	3.4621995e+06	-2.8066232	0	2	3	0	3	2	1	3	0101000020E6100000DFB4251A804D5E400B6816671B483F40
0	329696.56	3.4621992e+06	-2.7858322	0	2	3	0	3	2	1	3	0101000020E610000034C01404804D5E406EBE06461B483F40
0	329696.03	3.462199e+06	-2.76345	0	2	3	0	3	2	1	3	0101000020E61000001F7884EC7F4D5E40D1FAA2201B483F40
0	329695.53	3.4621988e+06	-2.7419076	0	2	3	0	3	2	1	3	0101000020E61000006B490AD67F4D5E401E07E8FA1A483F40
0	329695	3.4621985e+06	-2.7204247	0	2	3	0	3	2	1	3	0101000020E610000005C6FFBF7F4D5E400C58D9D31A483F40
0	329694.5	3.4621982e+06	-2.6970954	0	2	3	0	3	2	1	3	0101000020E6100000F7BD04A97F4D5E40D248BEA81A483F40
0	329694	3.462198e+06	-2.6749184	0	2	3	0	3	2	1	3	0101000020E6100000C832D4937F4D5E405DB1BE7E1A483F40
0	329693.5	3.4621975e+06	-2.6504285	0	2	3	0	3	2	1	3	0101000020E6100000286CD47D7F4D5E405E5581501A483F40
0	329693	3.4621972e+06	-2.6242976	0	2	3	0	3	2	1	3	0101000020E6100000BFF421687F4D5E407FD3EC1F1A483F40
0	329692.44	3.462197e+06	-2.593536	0	2	3	0	3	2	1	3	0101000020E61000001D2D50507F4D5E401CA6E0E619483F40
0	329691.94	3.4621965e+06	-2.563449	0	2	3	0	3	2	1	3	0101000020E610000095C2723A7F4D5E40DCB1C0AE19483F40
0	329691.5	3.4621962e+06	-2.5334928	0	2	3	0	3	2	1	3	0101000020E61000002124AD267F4D5E40F9E83A7819483F40
0	329691.03	3.4621958e+06	-2.5008593	0	2	3	0	3	2	1	3	0101000020E61000000C47B8127F4D5E40F812363D19483F40
0	329690.6	3.4621955e+06	-2.4658194	0	2	3	0	3	2	1	3	0101000020E610000075CD77FF7E4D5E4052869AFF18483F40
0	329690.16	3.462195e+06	-2.4273903	0	2	3	0	3	2	1	3	0101000020E61000006D76D9EC7E4D5E40A467A1BE18483F40
0	329689.75	3.4621945e+06	-2.3859692	0	2	3	0	3	2	1	3	0101000020E6100000ACE886DB7E4D5E406ED0067C18483F40
0	329689.4	3.4621942e+06	-2.3422177	0	2	3	0	3	2	1	3	0101000020E610000036BB88CB7E4D5E40E0DCFD3718483F40
0	329689.06	3.4621938e+06	-2.297404	0	2	3	0	3	2	1	3	0101000020E610000027ED61BD7E4D5E40EDDBE5F417483F40
0	329688.72	3.4621932e+06	-2.2443402	0	2	3	0	3	2	1	3	0101000020E6100000C2E81AAF7E4D5E40FA5B5BA817483F40
0	329688.44	3.4621928e+06	-2.1911104	0	2	3	0	3	2	1	3	0101000020E61000006A19E3A27E4D5E40D5F1435D17483F40
0	329688.2	3.4621922e+06	-2.13734	0	2	3	0	3	2	1	3	0101000020E61000007BA356987E4D5E400CF1661217483F40
0	329688	3.4621918e+06	-2.0844114	0	2	3	0	3	2	1	3	0101000020E6100000BB43808F7E4D5E40F6B532C916483F40
0	329687.8	3.4621912e+06	-2.029026	0	2	3	0	3	2	1	3	0101000020E61000005DF793877E4D5E4076D5B67B16483F40
0	329687.66	3.4621908e+06	-1.9729096	0	2	3	0	3	2	1	3	0101000020E610000033AFD9807E4D5E406836D82B16483F40
0	329687.5	3.4621902e+06	-1.9173506	0	2	3	0	3	2	1	3	0101000020E61000001D847E7B7E4D5E404367A2DB15483F40
0	329687.4	3.4621895e+06	-1.8598708	0	2	3	0	3	2	1	3	0101000020E61000005A4A54777E4D5E4052D9BD8715483F40
0	329687.34	3.462189e+06	-1.8015445	0	2	3	0	3	2	1	3	0101000020E61000008B408A747E4D5E40F486F03115483F40
0	329687.28	3.4621885e+06	-1.747241	0	2	3	0	3	2	1	3	0101000020E6100000010638737E4D5E4022D4E4E014483F40
0	329687.28	3.462188e+06	-1.6937078	0	2	3	0	3	2	1	3	0101000020E6100000DD7B04737E4D5E406F20FE8C14483F40
0	329687.28	3.4621875e+06	-1.6471614	0	2	3	0	3	2	1	3	0101000020E61000007F63C0737E4D5E40212A0F3E14483F40
0	329687.3	3.462187e+06	-1.6057783	0	2	3	0	3	2	1	3	0101000020E6100000B80427757E4D5E400349A6EB13483F40
0	329687.34	3.4621862e+06	-1.576035	0	2	3	0	3	2	1	3	0101000020E6100000D0BEBA767E4D5E40BE929D9B13483F40
0	329687.38	3.4621858e+06	-1.5611387	0	2	3	0	3	2	1	3	0101000020E61000004ECBFB777E4D5E4089A6594813483F40
0	329687.34	3.4621852e+06	-1.5647253	0	2	3	0	3	2	1	3	0101000020E61000004BD753787E4D5E402935EBF412483F40
0	329687.34	3.4621848e+06	-1.5737535	0	2	3	0	3	2	1	3	0101000020E6100000BC5D3E787E4D5E40F9ECD9A312483F40
0	329687.34	3.4621842e+06	-1.5855792	0	2	3	0	3	2	1	3	0101000020E61000006384BD777E4D5E40F41CC95012483F40
0	329687.3	3.4621838e+06	-1.5962223	0	2	3	0	3	2	1	3	0101000020E610000088590C777E4D5E40F322910112483F40
0	329687.28	3.462183e+06	-1.6050924	0	2	3	0	3	2	1	3	0101000020E6100000516132767E4D5E4002D9C9AA11483F40
0	329687.25	3.4621825e+06	-1.6108414	0	2	3	0	3	2	1	3	0101000020E6100000783055757E4D5E40745D9F5211483F40
0	329687.22	3.462182e+06	-1.6144563	0	2	3	0	3	2	1	3	0101000020E61000000E8C8E747E4D5E4028C6F30611483F40
0	329687.2	3.4621815e+06	-1.617587	0	2	3	0	3	2	1	3	0101000020E610000087F49C737E4D5E40070805AD10483F40
0	329687.16	3.4621808e+06	-1.6198026	0	2	3	0	3	2	1	3	0101000020E6100000883FB8727E4D5E404A14385A10483F40
0	329687.12	3.4621802e+06	-1.6199273	0	2	3	0	3	2	1	3	0101000020E61000005147DE717E4D5E403E6D7C0110483F40
14	329687.12	3.4621798e+06	-1.6199863	0	2	3	2	3	2	1	3	0101000020E6100000059642717E4D5E40A3FB4AC30F483F40
14	329687.1	3.4621792e+06	-1.6160516	0	0	3	0	3	2	1	3	0101000020E6100000FE55AD707E4D5E40E3247B6A0F483F40
14	329687.06	3.4621788e+06	-1.6118718	0	0	3	0	3	2	1	3	0101000020E610000033F641707E4D5E409D4177150F483F40
14	329687.06	3.4621782e+06	-1.6076921	0	0	3	0	3	2	1	3	0101000020E61000004218FB6F7E4D5E4022D2B5C60E483F40
14	329687.03	3.4621775e+06	-1.6022724	0	0	3	0	3	2	1	3	0101000020E6100000BB2FC26F7E4D5E402DE8D8610E483F40
14	329687.03	3.4621768e+06	-1.5960248	0	0	3	0	3	2	1	3	0101000020E6100000AF01B16F7E4D5E4088CB57ED0D483F40
14	329687.03	3.4621762e+06	-1.5916526	0	0	3	0	3	2	1	3	0101000020E610000019F7BE6F7E4D5E4008BDB0950D483F40
14	329687	3.4621755e+06	-1.5873315	0	0	3	0	3	2	1	3	0101000020E6100000B39EE56F7E4D5E40AC6883350D483F40
14	329687	3.462175e+06	-1.5833724	0	0	3	0	3	2	1	3	0101000020E61000009FE523707E4D5E4070FC58DA0C483F40
14	329687	3.4621742e+06	-1.5787416	0	0	3	0	3	2	1	3	0101000020E6100000A71F8D707E4D5E405799ED6E0C483F40
14	329687	3.4621735e+06	-1.5742636	0	0	3	0	3	2	1	3	0101000020E6100000267D15717E4D5E4042C190060C483F40
14	329687	3.4621728e+06	-1.5684892	0	0	3	0	3	2	1	3	0101000020E610000082F9F6717E4D5E404B136B7F0B483F40
14	329687.03	3.462172e+06	-1.5638059	0	0	3	0	3	2	1	3	0101000020E6100000FE62D7727E4D5E402CAE5B100B483F40
14	329687.03	3.4621715e+06	-1.5607132	0	0	3	0	3	2	1	3	0101000020E6100000D98D88737E4D5E40E8CC59C20A483F40
14	329687.03	3.4621705e+06	-1.5559746	0	0	3	0	3	2	1	3	0101000020E61000001F57D4747E4D5E40CC8E833C0A483F40
14	329687.06	3.4621695e+06	-1.5517548	0	0	3	0	3	2	1	3	0101000020E61000007B826E767E4D5E40441EF7A409483F40
14	329687.06	3.462169e+06	-1.5499494	0	0	3	0	3	2	1	3	0101000020E6100000793753777E4D5E40096A345509483F40
14	329687.1	3.462168e+06	-1.5473782	0	0	3	0	3	2	1	3	0101000020E6100000206BFC787E4D5E40A986AFC608483F40
14	329687.1	3.4621675e+06	-1.54621	0	0	3	0	3	2	1	3	0101000020E6100000F20AFD797E4D5E40CDC4337308483F40
14	329687.12	3.462167e+06	-1.545385	0	0	3	0	3	2	1	3	0101000020E6100000F656EA7A7E4D5E40D0550A2708483F40
14	329687.16	3.462166e+06	-1.5445999	0	0	3	0	3	2	1	3	0101000020E610000069E4D27C7E4D5E40358F7A8A07483F40
14	329687.2	3.462165e+06	-1.5454628	0	0	3	0	3	2	1	3	0101000020E610000008879F7E7E4D5E4076C276F006483F40
14	329687.2	3.4621642e+06	-1.5467097	0	0	3	0	3	2	1	3	0101000020E6100000CEF88E7F7E4D5E40A3DCF29B06483F40
14	329687.2	3.4621635e+06	-1.5500042	0	0	3	0	3	2	1	3	0101000020E6100000F3DA07817E4D5E40F120610B06483F40
14	329687.22	3.4621625e+06	-1.5542372	0	0	3	0	3	2	1	3	0101000020E6100000C28660827E4D5E4003B0EF7605483F40
14	329687.22	3.4621615e+06	-1.5589327	0	0	3	0	3	2	1	3	0101000020E61000000A4A80837E4D5E40E7E57CE504483F40
14	329687.22	3.4621605e+06	-1.5640424	0	0	3	0	3	2	1	3	0101000020E6100000D74C4C847E4D5E40D720826004483F40
14	329687.22	3.4621598e+06	-1.5696049	0	0	3	0	3	2	1	3	0101000020E6100000D35ED0847E4D5E408C0DC8E303483F40
14	329687.22	3.462159e+06	-1.5755782	0	0	3	0	3	2	1	3	0101000020E610000008AE1D857E4D5E408F0FE96503483F40
14	329687.22	3.4621582e+06	-1.5808733	0	0	3	0	3	2	1	3	0101000020E6100000B61432857E4D5E405C0A3CF602483F40
14	329687.2	3.4621575e+06	-1.5856441	0	0	3	0	3	2	1	3	0101000020E61000008BF921857E4D5E406E893C8702483F40
14	329687.2	3.4621568e+06	-1.58902	0	0	3	0	3	2	1	3	0101000020E6100000938AFE847E4D5E402CB0D22402483F40
14	329687.16	3.4621562e+06	-1.5915229	0	0	3	0	3	2	1	3	0101000020E61000005013CC847E4D5E4084FACFBF01483F40
14	329687.16	3.4621555e+06	-1.5923371	0	0	3	0	3	2	1	3	0101000020E6100000967EA6847E4D5E40FE85966401483F40
14	329687.16	3.462155e+06	-1.5911467	0	0	3	0	3	2	1	3	0101000020E6100000AFD49C847E4D5E403CE7E90B01483F40
14	329687.12	3.4621545e+06	-1.5882285	0	0	3	0	3	2	1	3	0101000020E6100000FF73B4847E4D5E402B408CB600483F40
14	329687.12	3.4621538e+06	-1.5834898	0	0	3	0	3	2	1	3	0101000020E6100000103FFA847E4D5E4069A7754200483F40
14	329687.12	3.462153e+06	-1.579038	0	0	3	0	3	2	1	3	0101000020E6100000386662857E4D5E40E2C46ED7FF473F40
14	329687.12	3.4621522e+06	-1.5749178	0	0	3	0	3	2	1	3	0101000020E61000007252E4857E4D5E40759EFD74FF473F40
14	329687.12	3.4621518e+06	-1.5715187	0	0	3	0	3	2	1	3	0101000020E610000073FB70867E4D5E40572B7B18FF473F40
14	329687.12	3.462151e+06	-1.5693661	0	0	3	0	3	2	1	3	0101000020E61000005020F6867E4D5E40177514C5FE473F40
14	329687.12	3.4621505e+06	-1.5679537	0	0	3	0	3	2	1	3	0101000020E610000026AE72877E4D5E40B3BFE677FE473F40
14	329687.16	3.46215e+06	-1.5669801	0	0	3	0	3	2	1	3	0101000020E6100000EF130A887E4D5E40B4FA4119FE473F40
14	329687.16	3.4621495e+06	-1.5662099	0	0	3	0	3	2	1	3	0101000020E61000002F9794887E4D5E4057C845C6FD473F40
14	329687.16	3.462149e+06	-1.5654899	0	0	3	0	3	2	1	3	0101000020E61000002AA918897E4D5E40BDC3CA78FD473F40
14	329687.16	3.4621485e+06	-1.5647792	0	0	3	0	3	2	1	3	0101000020E6100000E7E09E897E4D5E4058918A2CFD473F40
14	329687.16	3.462148e+06	-1.5633329	0	0	3	0	3	2	1	3	0101000020E610000033923A8A7E4D5E408A059FDEFC473F40
14	329687.16	3.4621472e+06	-1.5600885	0	0	3	0	3	2	1	3	0101000020E610000032471F8B7E4D5E40854B807EFC473F40
14	329687.2	3.4621468e+06	-1.5573038	0	0	3	0	3	2	1	3	0101000020E610000044BBF18B7E4D5E4007EE932EFC473F40
14	329687.2	3.4621462e+06	-1.5540162	0	0	3	0	3	2	1	3	0101000020E61000004E9EE78C7E4D5E405ABE0EDAFB473F40
14	329687.22	3.4621455e+06	-1.5492859	0	0	3	0	3	2	1	3	0101000020E6100000E069168E7E4D5E40CA161982FB473F40
14	329687.22	3.462145e+06	-1.5431978	0	0	3	0	3	2	1	3	0101000020E610000076D2798F7E4D5E4089F5162BFB473F40
14	329687.25	3.4621445e+06	-1.5362054	0	0	3	0	3	2	1	3	0101000020E6100000DF2B25917E4D5E40984631CFFA473F40
14	329687.28	3.4621438e+06	-1.5302672	0	0	3	0	3	2	1	3	0101000020E610000070A60C937E4D5E405773446BFA473F40
14	329687.3	3.4621432e+06	-1.5271717	0	0	3	0	3	2	1	3	0101000020E6100000C1A395947E4D5E40FF93331BFA473F40
14	329687.34	3.4621425e+06	-1.5248669	0	0	3	0	3	2	1	3	0101000020E610000091F87A967E4D5E40D85BDBB7F9473F40
14	329687.38	3.462142e+06	-1.5251039	0	0	3	0	3	2	1	3	0101000020E6100000E0FB2F987E4D5E40E3D61C58F9473F40
14	329687.4	3.4621412e+06	-1.5282035	0	0	3	0	3	2	1	3	0101000020E610000045B27A997E4D5E4077D3AE04F9473F40
14	329687.4	3.4621408e+06	-1.5311229	0	1	3	0	3	2	1	3	0101000020E6100000E4A58E9A7E4D5E406A6748B8F8473F40
14	329687.44	3.4621402e+06	-1.5341918	0	1	3	0	3	2	1	3	0101000020E61000004B56AD9B7E4D5E406F997D65F8473F40
14	329687.44	3.4621398e+06	-1.537039	0	1	3	0	3	2	1	3	0101000020E61000005CD0AB9C7E4D5E405C389D17F8473F40
14	329687.47	3.4621392e+06	-1.5407056	0	1	3	0	3	2	1	3	0101000020E6100000320DE19D7E4D5E409AE705B2F7473F40
14	329687.47	3.4621385e+06	-1.5438949	0	1	3	0	3	2	1	3	0101000020E61000003CF0D69E7E4D5E40F0B7225AF7473F40
14	329687.5	3.4621378e+06	-1.5477742	0	1	3	0	3	2	1	3	0101000020E61000001427E09F7E4D5E40B44E43F1F6473F40
14	329687.5	3.4621372e+06	-1.550698	0	1	3	0	3	2	1	3	0101000020E610000053B096A07E4D5E40E069EFA1F6473F40
14	329687.5	3.4621368e+06	-1.5536585	0	1	3	0	3	2	1	3	0101000020E61000006CB545A17E4D5E407FDF5C4FF6473F40
14	329687.5	3.4621362e+06	-1.5564635	0	1	3	0	3	2	1	3	0101000020E61000009E10EBA17E4D5E4045C67BFAF5473F40
14	329687.5	3.4621358e+06	-1.5592378	0	1	3	0	3	2	1	3	0101000020E61000002F338DA27E4D5E40D0165C9FF5473F40
14	329687.53	3.462135e+06	-1.5620865	0	1	3	0	3	2	1	3	0101000020E6100000FE2F2DA37E4D5E40B55D5C3BF5473F40
14	329687.53	3.4621342e+06	-1.5647877	0	1	3	0	3	2	1	3	0101000020E6100000A7A8C5A37E4D5E406FF236D2F4473F40
14	329687.53	3.4621335e+06	-1.5672553	0	1	3	0	3	2	1	3	0101000020E610000007194FA47E4D5E40C7D71568F4473F40
14	329687.53	3.4621328e+06	-1.570032	0	1	3	0	3	2	1	3	0101000020E6100000479CD9A47E4D5E40AD447DEBF3473F40
14	329687.5	3.462132e+06	-1.5726185	0	1	3	0	3	2	1	3	0101000020E6100000F20E46A57E4D5E402A28F375F3473F40
14	329687.5	3.4621312e+06	-1.5753053	0	1	3	0	3	2	1	3	0101000020E6100000B040A0A57E4D5E40CE2426FBF2473F40
14	329687.5	3.4621302e+06	-1.5781426	0	1	3	0	3	2	1	3	0101000020E6100000A11EE7A57E4D5E40C47AF378F2473F40
14	329687.5	3.4621295e+06	-1.5810786	0	1	3	0	3	2	1	3	0101000020E6100000813714A67E4D5E4005ACD4F1F1473F40
14	329687.47	3.4621285e+06	-1.5837837	0	1	3	0	3	2	1	3	0101000020E6100000AC5224A67E4D5E40E3556875F1473F40
14	329687.47	3.4621278e+06	-1.5867743	0	1	3	0	3	2	1	3	0101000020E6100000C5A81AA67E4D5E404C8237EDF0473F40
14	329687.44	3.4621268e+06	-1.5897745	0	1	3	0	3	2	1	3	0101000020E6100000CD39F7A57E4D5E40C1BF786EF0473F40
14	329687.44	3.462126e+06	-1.5926926	0	1	3	0	3	2	1	3	0101000020E6100000E789C1A57E4D5E4048164DFBEF473F40
14	329687.4	3.4621255e+06	-1.5954969	0	1	3	0	3	2	1	3	0101000020E6100000547377A57E4D5E4073D3A090EF473F40
14	329687.4	3.4621248e+06	-1.5979177	0	1	3	0	3	2	1	3	0101000020E61000009BD825A57E4D5E409D36E535EF473F40
14	329687.38	3.4621242e+06	-1.6001735	0	1	3	0	3	2	1	3	0101000020E61000003B6EC8A47E4D5E40AC5D36E1EE473F40
14	329687.38	3.4621238e+06	-1.6021876	0	1	3	0	3	2	1	3	0101000020E610000076A565A47E4D5E40A0F1CF94EE473F40
14	329687.34	3.462123e+06	-1.6047271	0	1	3	0	3	2	1	3	0101000020E6100000308BD2A37E4D5E408EE35133EE473F40
14	329687.3	3.4621225e+06	-1.6071372	0	1	3	0	3	2	1	3	0101000020E6100000A06830A37E4D5E40E7DE09D6ED473F40
14	329687.28	3.462122e+06	-1.609281	0	1	3	0	3	2	1	3	0101000020E6100000F0588FA27E4D5E40A5AB9083ED473F40
14	329687.28	3.4621218e+06	-1.6098561	0	1	3	1	3	2	1	3	0101000020E6100000302D61A27E4D5E40246EA16DED473F40
0	329687.28	3.4621212e+06	-1.5976546	0	2	3	0	3	2	1	3	0101000020E610000010468EA27E4D5E4024022117ED473F40
0	329687.28	3.4621205e+06	-1.5859637	0	2	3	0	3	2	1	3	0101000020E6100000754D20A37E4D5E40230B34BAEC473F40
0	329687.28	3.46212e+06	-1.575433	0	2	3	0	3	2	1	3	0101000020E61000003CB9E3A37E4D5E40FB3B5C6DEC473F40
0	329687.3	3.4621195e+06	-1.5621978	0	2	3	0	3	2	1	3	0101000020E61000005DFE27A57E4D5E40CB540710EC473F40
0	329687.34	3.4621188e+06	-1.5478009	0	2	3	0	3	2	1	3	0101000020E610000054D1E8A67E4D5E402CD96DABEB473F40
0	329687.38	3.4621182e+06	-1.5356625	0	2	3	0	3	2	1	3	0101000020E610000064FA9FA87E4D5E406787EB5DEB473F40
0	329687.4	3.4621175e+06	-1.5182827	0	2	3	0	3	2	1	3	0101000020E610000047C357AB7E4D5E407D61B4FAEA473F40
0	329687.47	3.462117e+06	-1.4994102	0	2	3	0	3	2	1	3	0101000020E6100000A9E36BAE7E4D5E4070B47BA3EA473F40
0	329687.56	3.4621165e+06	-1.4773948	0	2	3	0	3	2	1	3	0101000020E6100000071604B27E4D5E409DE84B53EA473F40
0	329687.66	3.462116e+06	-1.4462044	0	2	3	0	3	2	1	3	0101000020E6100000766B27B77E4D5E409ED90CF9E9473F40
0	329687.78	3.4621155e+06	-1.4142329	0	2	3	0	3	2	1	3	0101000020E610000048257CBC7E4D5E40791EAEAEE9473F40
0	329687.94	3.462115e+06	-1.3744588	0	2	3	0	3	2	1	3	0101000020E6100000B5E468C37E4D5E403DA20E5FE9473F40
0	329688.1	3.4621145e+06	-1.3332844	0	2	3	0	3	2	1	3	0101000020E6100000AFD24FCB7E4D5E4048345911E9473F40
0	329688.28	3.4621138e+06	-1.28769	0	2	3	0	3	2	1	3	0101000020E6100000859474D47E4D5E40CAA5F6C4E8473F40
0	329688.53	3.4621135e+06	-1.2363628	0	2	3	0	3	2	1	3	0101000020E6100000C10CE4DE7E4D5E407AFB017CE8473F40
0	329688.78	3.462113e+06	-1.1811155	0	2	3	0	3	2	1	3	0101000020E6100000CCD938EB7E4D5E40D8C94730E8473F40
0	329689.06	3.4621125e+06	-1.1302494	0	2	3	0	3	2	1	3	0101000020E6100000D4B811F87E4D5E4057104AE9E7473F40
0	329689.34	3.462112e+06	-1.0860071	0	2	3	0	3	2	1	3	0101000020E6100000A6489D047F4D5E404B56CEA9E7473F40
0	329689.66	3.4621115e+06	-1.0421276	0	2	3	0	3	2	1	3	0101000020E61000000C47B8127F4D5E4083517667E7473F40
0	329690	3.462111e+06	-1.0040694	0	2	3	0	3	2	1	3	0101000020E6100000128A2E217F4D5E402D442127E7473F40
0	329690.38	3.4621105e+06	-0.96873784	0	2	3	0	3	2	1	3	0101000020E610000039705A327F4D5E4020949DDDE6473F40
0	329690.72	3.4621102e+06	-0.9378588	0	2	3	0	3	2	1	3	0101000020E610000033E330427F4D5E40CE042D9EE6473F40
0	329691.12	3.4621098e+06	-0.9029944	0	2	3	0	3	2	1	3	0101000020E6100000208C93537F4D5E406CC9C65DE6473F40
0	329691.5	3.4621092e+06	-0.87081635	0	2	3	0	3	2	1	3	0101000020E6100000CCB16B647F4D5E40AAE06623E6473F40
0	329691.9	3.462109e+06	-0.83759403	0	2	3	0	3	2	1	3	0101000020E6100000AAE7CD767F4D5E40A58C7DE7E5473F40
0	329692.28	3.4621085e+06	-0.8090229	0	2	3	0	3	2	1	3	0101000020E6100000949C88887F4D5E40184B5FB0E5473F40
0	329692.7	3.4621082e+06	-0.7845668	0	2	3	0	3	2	1	3	0101000020E610000088D6C7997F4D5E403B39B37CE5473F40
0	329693.1	3.4621078e+06	-0.7633237	0	2	3	0	3	2	1	3	0101000020E6100000AD2091AC7F4D5E4076188E45E5473F40
0	329693.6	3.4621075e+06	-0.74016285	0	2	3	0	3	2	1	3	0101000020E6100000BCC4EEC17F4D5E4046A36809E5473F40
0	329694.1	3.462107e+06	-0.7154778	0	2	3	0	3	2	1	3	0101000020E6100000627A3CD97F4D5E40FA91DCCAE4473F40
0	329694.5	3.4621068e+06	-0.69662875	0	2	3	0	3	2	1	3	0101000020E6100000168F62EB7F4D5E40F7923E9CE4473F40
0	329695	3.4621062e+06	-0.6747827	0	2	3	0	3	2	1	3	0101000020E61000002BCAC800804D5E40EC7D8367E4473F40
0	329695.44	3.462106e+06	-0.65433556	0	2	3	0	3	2	1	3	0101000020E610000076F60A15804D5E40E62CCC37E4473F40
0	329695.94	3.4621058e+06	-0.63229406	0	2	3	0	3	2	1	3	0101000020E61000009BFCB62A804D5E4002781D07E4473F40
0	329696.4	3.4621055e+06	-0.61021787	0	2	3	0	3	2	1	3	0101000020E61000002BF24440804D5E40EE621ED9E3473F40
0	329696.9	3.462105e+06	-0.58751017	0	2	3	0	3	2	1	3	0101000020E6100000C378AF55804D5E40E7FFEFADE3473F40
0	329697.34	3.4621048e+06	-0.56588614	0	2	3	0	3	2	1	3	0101000020E6100000501C246A804D5E40E346CF86E3473F40
0	329697.8	3.4621045e+06	-0.54532945	0	2	3	0	3	2	1	3	0101000020E61000008143B77D804D5E40673F4563E3473F40
0	329698.38	3.4621042e+06	-0.5182115	0	2	3	0	3	2	1	3	0101000020E61000002E9D3797804D5E401EFBC937E3473F40
0	329698.94	3.462104e+06	-0.4905538	0	2	3	0	3	2	1	3	0101000020E61000005A9F5BB0804D5E40318B2410E3473F40
0	329699.5	3.4621038e+06	-0.46338907	0	2	3	0	3	2	1	3	0101000020E61000004DB5FDC8804D5E40D2FF2FECE2473F40
0	329700.03	3.4621035e+06	-0.43753058	0	2	3	0	3	2	1	3	0101000020E6100000EED342E0804D5E4071FED5CCE2473F40
0	329700.53	3.4621035e+06	-0.41208047	0	2	3	0	3	2	1	3	0101000020E61000003276A6F6804D5E40F14821B1E2473F40
0	329701	3.4621032e+06	-0.3872018	0	2	3	0	3	2	1	3	0101000020E6100000E05E5F0C814D5E4017897598E2473F40
0	329701.53	3.462103e+06	-0.3606927	0	2	3	0	3	2	1	3	0101000020E6100000FE82E722814D5E40E02D4781E2473F40
0	329702.06	3.462103e+06	-0.3326597	0	2	3	0	3	2	1	3	0101000020E6100000539F493A814D5E40D94CBD6BE2473F40
0	329702.66	3.4621028e+06	-0.30064613	0	2	3	0	3	2	1	3	0101000020E61000006A976454814D5E40ED6AB656E2473F40
0	329703.12	3.4621028e+06	-0.27331036	0	2	3	0	3	2	1	3	0101000020E61000009634196A814D5E406E93B747E2473F40
0	329703.66	3.4621025e+06	-0.24210255	0	2	3	0	3	2	1	3	0101000020E6100000AD705181814D5E40D401833AE2473F40
0	329704.2	3.4621025e+06	-0.2075529	0	2	3	0	3	2	1	3	0101000020E6100000D47DFB98814D5E4020405530E2473F40
0	329704.75	3.4621025e+06	-0.16675848	0	2	3	0	3	2	1	3	0101000020E6100000206D1EB2814D5E40AB8B9D29E2473F40
0	329705.38	3.4621025e+06	-0.1186504	0	2	3	0	3	2	1	3	0101000020E6100000FFD0FCCC814D5E406D086427E2473F40
0	329706.03	3.4621025e+06	-0.064171195	0	2	3	0	3	2	1	3	0101000020E610000078EF57EA814D5E40D08C662AE2473F40
0	329706.72	3.4621025e+06	-0.013432701	0	2	3	0	3	2	1	3	0101000020E61000006FBC8207824D5E404B87DF31E2473F40
0	329707.44	3.4621025e+06	0.028675316	0	2	3	0	3	2	1	3	0101000020E6100000A17C2D27824D5E40F6BD5C3CE2473F40
0	329708.2	3.4621025e+06	0.048770472	0	2	3	0	3	2	1	3	0101000020E6100000514F4548824D5E4050DE2C46E2473F40
0	329708.7	3.4621025e+06	0.048773136	0	2	3	0	3	2	1	3	0101000020E61000007F8F5A5E824D5E40400EEB4AE2473F40
0	329709.2	3.4621025e+06	0.043619383	0	2	3	0	3	2	1	3	0101000020E6100000835DEC74824D5E40DABCBE4EE2473F40
0	329709.7	3.4621025e+06	0.035135083	0	2	3	0	3	2	1	3	0101000020E6100000B63A368B824D5E40850A5E51E2473F40
0	329710.25	3.4621025e+06	0.021171598	0	2	3	0	3	2	1	3	0101000020E610000076A4EBA3824D5E4095EE7652E2473F40
0	329710.8	3.4621025e+06	0.006056028	0	2	3	0	3	2	1	3	0101000020E6100000554CE7BB824D5E4079271752E2473F40
0	329711.47	3.4621025e+06	-0.008492226	0	2	3	0	3	2	1	3	0101000020E6100000BF99D0D8824D5E403E5AE550E2473F40
0	329712.06	3.4621025e+06	-0.01687351	0	2	3	0	3	2	1	3	0101000020E6100000B0C2C8F3824D5E4010BBC44FE2473F40
13	329712.47	3.4621025e+06	-0.020786488	0	2	3	2	3	2	1	3	0101000020E61000004C7BCC05834D5E4044E4C34EE2473F40
13	329713.06	3.4621025e+06	-0.024967244	0	0	3	0	3	2	1	3	0101000020E6100000EEA00F1F834D5E409D4C2C4DE2473F40
13	329713.66	3.4621025e+06	-0.029644957	0	0	3	0	3	2	1	3	0101000020E610000086F08639834D5E4031EC314BE2473F40
13	329714.22	3.4621025e+06	-0.033752233	0	0	3	0	3	2	1	3	0101000020E61000005EB65E52834D5E40FF960949E2473F40
13	329714.9	3.4621025e+06	-0.038325585	0	0	3	0	3	2	1	3	0101000020E6100000373F1770834D5E40381E1C46E2473F40
13	329715.4	3.4621025e+06	-0.04093108	0	0	3	0	3	2	1	3	0101000020E610000074FF5687834D5E40BC88C143E2473F40
13	329716	3.4621025e+06	-0.042708695	0	0	3	0	3	2	1	3	0101000020E6100000D3AD67A0834D5E400ADC3C41E2473F40
13	329716.6	3.4621022e+06	-0.043664422	0	0	3	0	3	2	1	3	0101000020E6100000BD96CABA834D5E40D647A23EE2473F40
13	329717.22	3.4621022e+06	-0.044234622	0	0	3	0	3	2	1	3	0101000020E6100000D78FB7D6834D5E405E0ADE3BE2473F40
13	329717.94	3.4621022e+06	-0.04471403	0	0	3	0	3	2	1	3	0101000020E61000006BBABFF6834D5E409921AA38E2473F40
13	329718.7	3.4621022e+06	-0.04457578	0	0	3	0	3	2	1	3	0101000020E610000059B81C17844D5E4063B28B35E2473F40
13	329719.47	3.4621022e+06	-0.043046895	0	0	3	0	3	2	1	3	0101000020E6100000B35B123A844D5E4032768732E2473F40
13	329720.22	3.4621022e+06	-0.040538788	0	0	3	0	3	2	1	3	0101000020E6100000FED5505B844D5E409F840A30E2473F40
13	329721.06	3.4621022e+06	-0.037245244	0	0	3	0	3	2	1	3	0101000020E61000004EA37A7F844D5E406FF7BE2DE2473F40
13	329721.9	3.4621022e+06	-0.033635058	0	0	3	0	3	2	1	3	0101000020E6100000260896A4844D5E40B7C6D82BE2473F40
13	329722.72	3.4621022e+06	-0.030319855	0	0	3	0	3	2	1	3	0101000020E6100000AA7B80C8844D5E407F89602AE2473F40
13	329723.6	3.462102e+06	-0.027786022	0	0	3	0	3	2	1	3	0101000020E61000009B9403EF844D5E402132FB28E2473F40
13	329724.44	3.462102e+06	-0.026492214	0	0	3	0	3	2	1	3	0101000020E6100000ED682B15854D5E405254AB27E2473F40
13	329725.28	3.462102e+06	-0.025765002	0	0	3	0	3	2	1	3	0101000020E61000005E7B993A854D5E4031AB7826E2473F40
13	329726.2	3.462102e+06	-0.025024163	0	0	3	0	3	2	1	3	0101000020E6100000DD135E61854D5E40C4315A25E2473F40
13	329727.03	3.462102e+06	-0.024549708	0	0	3	0	3	2	1	3	0101000020E6100000D6B79187854D5E4082054324E2473F40
13	329727.88	3.462102e+06	-0.024673631	0	0	3	0	3	2	1	3	0101000020E6100000A1F4C7AC854D5E4086AE2023E2473F40
13	329728.75	3.462102e+06	-0.024662474	0	0	3	0	3	2	1	3	0101000020E6100000100D62D2854D5E4038F00922E2473F40
13	329729.56	3.462102e+06	-0.024003364	0	0	3	0	3	2	1	3	0101000020E6100000DE3D40F7854D5E4097022221E2473F40
13	329730.44	3.462102e+06	-0.022588383	0	0	3	0	3	2	1	3	0101000020E61000009613CE1D864D5E40C2A07020E2473F40
13	329731.28	3.462102e+06	-0.019612215	0	0	3	0	3	2	1	3	0101000020E6100000CD39BA42864D5E4072015920E2473F40
13	329732.16	3.462102e+06	-0.015206818	0	0	3	0	3	2	1	3	0101000020E6100000928EA068864D5E40B84DE320E2473F40
13	329733	3.462102e+06	-0.0097083915	0	0	3	0	3	2	1	3	0101000020E6100000D397828E864D5E40C00A3A22E2473F40
13	329733.88	3.462102e+06	-0.003441852	0	0	3	0	3	2	1	3	0101000020E6100000100430B4864D5E404FF56724E2473F40
13	329734.72	3.462102e+06	0.002682336	0	0	3	0	3	2	1	3	0101000020E61000006BB44FD9864D5E4081F94827E2473F40
13	329735.56	3.462102e+06	0.0075079836	0	0	3	0	3	2	1	3	0101000020E6100000232C6CFE864D5E408AEF942AE2473F40
13	329736.34	3.462102e+06	0.010302171	0	0	3	0	3	2	1	3	0101000020E610000038648721874D5E403DAFC02DE2473F40
13	329737.06	3.462102e+06	0.011856393	0	0	3	0	3	2	1	3	0101000020E6100000EC756241874D5E4054C7C530E2473F40
13	329737.7	3.462102e+06	0.012770923	0	0	3	0	3	2	1	3	0101000020E6100000E6D8C35C874D5E401E6C7E33E2473F40
13	329738.34	3.462102e+06	0.013427658	0	0	3	0	3	2	1	3	0101000020E6100000CE25C478874D5E40A76E5C36E2473F40
13	329739.03	3.462102e+06	0.01403699	0	0	3	0	3	2	1	3	0101000020E61000006FC9F897874D5E40EE3EA639E2473F40
13	329739.6	3.462102e+06	0.014498782	0	0	3	0	3	2	1	3	0101000020E6100000ADE17DB0874D5E401CD8493CE2473F40
13	329740.16	3.462102e+06	0.01496004	0	0	3	0	3	2	1	3	0101000020E6100000524CB0C8874D5E40E6E0F03EE2473F40
13	329740.66	3.462102e+06	0.015383485	0	1	3	0	3	2	1	3	0101000020E610000066EBB3DF874D5E40604A8041E2473F40
13	329741.2	3.462102e+06	0.01579734	0	1	3	0	3	2	1	3	0101000020E61000007F7207F6874D5E402B1B0444E2473F40
13	329741.72	3.462102e+06	0.017496966	0	1	3	0	3	2	1	3	0101000020E6100000CEA6190E884D5E40AF4EB646E2473F40
13	329742.34	3.462102e+06	0.017968878	0	1	3	0	3	2	1	3	0101000020E610000080F51329884D5E407ACEF249E2473F40
13	329743.03	3.462102e+06	0.018488193	0	1	3	0	3	2	1	3	0101000020E61000000B884147884D5E40C5419D4DE2473F40
13	329743.66	3.462102e+06	0.018962242	0	1	3	0	3	2	1	3	0101000020E610000000FD2663884D5E40E6930B51E2473F40
13	329744.16	3.462102e+06	0.01933615	0	1	3	0	3	2	1	3	0101000020E610000071B46E79884D5E4044ADD053E2473F40
13	329744.7	3.462102e+06	0.019704731	0	1	3	0	3	2	1	3	0101000020E6100000C32D7090884D5E4038D7B356E2473F40
13	329745.25	3.462102e+06	0.019937487	0	1	3	0	3	2	1	3	0101000020E6100000FA63D1A9884D5E40F728DF59E2473F40
13	329745.84	3.462102e+06	0.01976811	0	1	3	0	3	2	1	3	0101000020E610000074C01DC4884D5E40E1C7115DE2473F40
13	329746.5	3.462102e+06	0.01919926	0	1	3	0	3	2	1	3	0101000020E6100000ADAC35E0884D5E402F2F6460E2473F40
13	329747.16	3.462102e+06	0.01845624	0	1	3	0	3	2	1	3	0101000020E61000004FECCCFD884D5E407537D163E2473F40
13	329747.84	3.4621022e+06	0.01766741	0	1	3	0	3	2	1	3	0101000020E61000008C82431C894D5E4025674367E2473F40
13	329748.66	3.4621022e+06	0.016758207	0	1	3	0	3	2	1	3	0101000020E6100000C7383A3F894D5E40DED01E6BE2473F40
13	329749.4	3.4621022e+06	0.015843678	0	1	3	0	3	2	1	3	0101000020E6100000E3A3C060894D5E407414B56EE2473F40
13	329750.16	3.4621022e+06	0.014800783	0	1	3	0	3	2	1	3	0101000020E6100000BB3FCF80894D5E4000560572E2473F40
13	329750.75	3.4621022e+06	0.013869742	0	1	3	0	3	2	1	3	0101000020E6100000D783D79B894D5E4021F9BA74E2473F40
13	329751.34	3.4621022e+06	0.013063869	0	1	3	0	3	2	1	3	0101000020E6100000CDEB92B5894D5E40BA814077E2473F40
13	329751.84	3.4621022e+06	0.01249928	0	1	3	0	3	2	1	3	0101000020E6100000A3AA6CCC894D5E40B3937379E2473F40
13	329752.44	3.4621022e+06	0.011955461	0	1	3	0	3	2	1	3	0101000020E6100000DF7D02E6894D5E409FE7DB7BE2473F40
13	329753	3.4621022e+06	0.011527758	0	1	3	0	3	2	1	3	0101000020E61000000DCB41FE894D5E4046921A7EE2473F40
13	329753.34	3.4621022e+06	0.011292868	0	1	3	1	3	2	1	3	0101000020E61000004B00660D8A4D5E40097A7C7FE2473F40
0	329753.9	3.4621022e+06	0.0077945394	0	2	3	0	3	2	1	3	0101000020E610000014A484268A4D5E4067D1E180E2473F40
0	329754.47	3.4621022e+06	0.004020308	0	2	3	0	3	2	1	3	0101000020E61000007B34B93E8A4D5E40D4AEEE81E2473F40
0	329755	3.4621022e+06	2.7164246e-05	0	2	3	0	3	2	1	3	0101000020E610000039F5E1568A4D5E4090ECA082E2473F40
0	329755.62	3.4621022e+06	-0.0043698982	0	2	3	0	3	2	1	3	0101000020E61000009413E8718A4D5E4028040E83E2473F40
\.


--
-- Data for Name: result_path_points; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.result_path_points (p_id, lon, lat, utmx, utmy, heading, curv, mode, speed_mode, event_mode, opposite_side_mode, lane_num, lane_seq, lane_width) FROM stdin;
9	121.21142600380644	31.281696858059238	329743	3.4622018e+06	-3.1226077	0	0	3	0	3	2	1	3
9	121.211425487	31.2816968442	329743	3.4622018e+06	-3.1226077	0	0	3	0	3	2	1	3
9	121.211419781	31.2816967097	329742.44	3.4622018e+06	-3.1252196	0	0	3	0	3	2	1	3
9	121.211413367	31.2816965733	329741.84	3.4622018e+06	-3.1281462	0	0	3	0	3	2	1	3
9	121.211406611	31.2816964414	329741.2	3.4622018e+06	-3.130905	0	0	3	0	3	2	1	3
9	121.211401093	31.2816963306	329740.66	3.4622018e+06	-3.1321864	0	0	3	0	3	2	1	3
9	121.21139581	31.2816962148	329740.16	3.4622018e+06	-3.13234	0	0	3	0	3	2	1	3
9	121.211389653	31.2816960582	329739.6	3.4622018e+06	-3.1308823	0	0	3	0	3	2	1	3
9	121.211383305	31.2816958724	329738.97	3.4622018e+06	-3.1280758	0	0	3	0	3	2	1	3
9	121.211376943	31.2816956662	329738.38	3.4622018e+06	-3.1247916	0	0	3	0	3	2	1	3
9	121.211369816	31.2816954085	329737.7	3.4622018e+06	-3.1206653	0	0	3	0	3	2	1	3
9	121.211362364	31.2816951164	329736.97	3.4622018e+06	-3.1164746	0	0	3	0	3	2	1	3
9	121.211354504	31.2816947963	329736.22	3.4622018e+06	-3.1131203	0	0	3	0	3	2	1	3
9	121.211346497	31.2816944586	329735.47	3.4622018e+06	-3.1106133	0	0	3	0	3	2	1	3
9	121.211338491	31.2816941201	329734.72	3.4622018e+06	-3.109222	0	0	3	0	3	2	1	3
9	121.211330036	31.2816937766	329733.9	3.4622018e+06	-3.1094742	0	0	3	0	3	2	1	3
9	121.211321228	31.2816934425	329733.06	3.4622015e+06	-3.111451	0	0	3	0	3	2	1	3
9	121.21131221	31.281693126	329732.2	3.4622015e+06	-3.114539	0	0	3	0	3	2	1	3
9	121.211303108	31.281692838	329731.34	3.4622015e+06	-3.118327	0	0	3	0	3	2	1	3
9	121.211293574	31.2816925717	329730.44	3.4622015e+06	-3.122734	0	0	3	0	3	2	1	3
9	121.211283251	31.2816923232	329729.44	3.4622015e+06	-3.1276448	0	0	3	0	3	2	1	3
9	121.211273593	31.2816921291	329728.53	3.4622015e+06	-3.1322575	0	0	3	0	3	2	1	3
9	121.211268002	31.2816920337	329728	3.4622015e+06	-3.1349266	0	0	3	0	3	2	1	3
9	121.21125782	31.2816918912	329727.03	3.4622015e+06	-3.1397812	0	0	3	0	3	2	1	3
9	121.211252554	31.2816918328	329726.53	3.4622015e+06	3.1407921	0	0	3	0	3	2	1	3
9	121.211243092	31.2816917524	329725.62	3.4622015e+06	3.1359515	0	0	3	0	3	2	1	3
9	121.211234638	31.2816917096	329724.8	3.4622015e+06	3.131715	0	0	3	0	3	2	1	3
9	121.211226084	31.2816916972	329724	3.4622015e+06	3.1275837	0	0	3	0	3	2	1	3
9	121.211219303	31.281691712	329723.34	3.4622015e+06	3.1243892	0	0	3	0	3	2	1	3
9	121.211212928	31.2816917451	329722.75	3.4622015e+06	3.1214857	0	0	3	0	3	2	1	3
9	121.211207537	31.2816917879	329722.22	3.4622015e+06	3.1191063	0	0	3	0	3	2	1	3
9	121.211200975	31.2816918583	329721.6	3.4622018e+06	3.1162453	0	0	3	0	3	2	1	3
9	121.211194778	31.2816919435	329721.03	3.4622018e+06	3.113541	0	0	3	0	3	2	1	3
9	121.211189369	31.2816920277	329720.5	3.4622018e+06	3.1140945	0	1	3	0	3	2	1	3
9	121.211183472	31.2816920077	329719.94	3.4622018e+06	3.1191974	0	1	3	0	3	2	1	3
9	121.211177683	31.2816919558	329719.4	3.4622018e+06	3.1248457	0	1	3	0	3	2	1	3
9	121.211172104	31.2816918899	329718.88	3.4622018e+06	3.1296759	0	1	3	0	3	2	1	3
9	121.211165771	31.281691801	329718.25	3.4622018e+06	3.1342595	0	1	3	0	3	2	1	3
9	121.211158566	31.2816916763	329717.56	3.4622018e+06	3.1391034	0	1	3	0	3	2	1	3
9	121.211153289	31.2816915679	329717.06	3.4622018e+06	-3.1406271	0	1	3	0	3	2	1	3
9	121.211147608	31.2816914357	329716.53	3.4622018e+06	-3.1369886	0	1	3	0	3	2	1	3
9	121.211141464	31.2816912754	329715.94	3.4622018e+06	-3.133195	0	1	3	0	3	2	1	3
9	121.211135864	31.2816911133	329715.4	3.4622018e+06	-3.129824	0	1	3	0	3	2	1	3
9	121.211130273	31.2816909387	329714.88	3.4622018e+06	-3.1266806	0	1	3	0	3	2	1	3
9	121.211124791	31.2816907554	329714.34	3.4622015e+06	-3.123824	0	1	3	0	3	2	1	3
9	121.2111173	31.2816904872	329713.62	3.4622015e+06	-3.1202593	0	1	3	0	3	2	1	3
9	121.21111062	31.2816902343	329713	3.4622015e+06	-3.1175334	0	1	3	0	3	2	1	3
9	121.211104532	31.2816899945	329712.44	3.4622015e+06	-3.1154032	0	1	3	0	3	2	1	3
9	121.211098786	31.2816897633	329711.88	3.4622015e+06	-3.1137948	0	1	3	0	3	2	1	3
9	121.211093415	31.2816895453	329711.38	3.4622015e+06	-3.1126947	0	1	3	0	3	2	1	3
9	121.211087155	31.2816892854	329710.78	3.4622015e+06	-3.1114817	0	1	3	0	3	2	1	3
9	121.211081472	31.2816890436	329710.22	3.4622015e+06	-3.1103754	0	1	3	0	3	2	1	3
9	121.211075153	31.2816887683	329709.62	3.4622015e+06	-3.1091383	0	1	3	0	3	2	1	3
9	121.211069787	31.2816885312	329709.12	3.4622015e+06	-3.1082077	0	1	3	0	3	2	1	3
9	121.211069501	31.281688518	329709.1	3.4622015e+06	-3.1081436	0	1	3	1	3	2	1	3
0	121.211064031	31.2816883135	329708.56	3.4622015e+06	-3.1107976	0	2	3	0	3	2	1	3
0	121.211057839	31.2816880891	329707.97	3.4622015e+06	-3.11245	0	2	3	0	3	2	1	3
0	121.211051633	31.2816878255	329707.38	3.4622015e+06	-3.1109593	0	2	3	0	3	2	1	3
0	121.211045708	31.2816875307	329706.8	3.4622015e+06	-3.1070821	0	2	3	0	3	2	1	3
0	121.211038898	31.2816871553	329706.16	3.4622012e+06	-3.101758	0	2	3	0	3	2	1	3
0	121.211032458	31.2816867616	329705.56	3.4622012e+06	-3.0960276	0	2	3	0	3	2	1	3
0	121.211026723	31.2816863498	329705	3.4622012e+06	-3.0886245	0	2	3	0	3	2	1	3
0	121.211021316	31.2816858855	329704.5	3.4622012e+06	-3.0787148	0	2	3	0	3	2	1	3
0	121.211015554	31.2816853051	329703.94	3.4622012e+06	-3.0658374	0	2	3	0	3	2	1	3
0	121.211009736	31.2816845936	329703.38	3.462201e+06	-3.0488122	0	2	3	0	3	2	1	3
0	121.21100382	31.2816836935	329702.8	3.462201e+06	-3.0257475	0	2	3	0	3	2	1	3
0	121.210998434	31.2816827511	329702.3	3.462201e+06	-3.003636	0	2	3	0	3	2	1	3
0	121.210992331	31.2816815725	329701.72	3.4622008e+06	-2.9796224	0	2	3	0	3	2	1	3
0	121.210986565	31.2816803497	329701.2	3.4622008e+06	-2.9575903	0	2	3	0	3	2	1	3
0	121.210980211	31.2816789075	329700.56	3.4622005e+06	-2.9357069	0	2	3	0	3	2	1	3
0	121.210974755	31.2816775822	329700.03	3.4622005e+06	-2.9177961	0	2	3	0	3	2	1	3
0	121.21096897	31.2816760791	329699.5	3.4622002e+06	-2.8987474	0	2	3	0	3	2	1	3
0	121.210962888	31.2816743806	329698.9	3.4622e+06	-2.8781264	0	2	3	0	3	2	1	3
0	121.210956664	31.2816725103	329698.3	3.4621998e+06	-2.8563116	0	2	3	0	3	2	1	3
0	121.210950297	31.2816704379	329697.72	3.4621998e+06	-2.8323534	0	2	3	0	3	2	1	3
0	121.210943734	31.2816681318	329697.06	3.4621995e+06	-2.8066232	0	2	3	0	3	2	1	3
0	121.210938473	31.2816661612	329696.56	3.4621992e+06	-2.7858322	0	2	3	0	3	2	1	3
0	121.210932855	31.2816639326	329696.03	3.462199e+06	-2.76345	0	2	3	0	3	2	1	3
0	121.210927496	31.2816616837	329695.53	3.4621988e+06	-2.7419076	0	2	3	0	3	2	1	3
0	121.210922241	31.2816593557	329695	3.4621985e+06	-2.7204247	0	2	3	0	3	2	1	3
0	121.210916762	31.2816567864	329694.5	3.4621982e+06	-2.6970954	0	2	3	0	3	2	1	3
0	121.21091171	31.2816542831	329694	3.462198e+06	-2.6749184	0	2	3	0	3	2	1	3
0	121.210906465	31.281651527	329693.5	3.4621975e+06	-2.6504285	0	2	3	0	3	2	1	3
0	121.210901292	31.2816486314	329693	3.4621972e+06	-2.6242976	0	2	3	0	3	2	1	3
0	121.210895613	31.2816452311	329692.44	3.462197e+06	-2.593536	0	2	3	0	3	2	1	3
0	121.2108904	31.2816418858	329691.94	3.4621965e+06	-2.563449	0	2	3	0	3	2	1	3
0	121.210885686	31.281638636	329691.5	3.4621962e+06	-2.5334928	0	2	3	0	3	2	1	3
0	121.210880928	31.2816351182	329691.03	3.4621958e+06	-2.5008593	0	2	3	0	3	2	1	3
0	121.210876338	31.2816314461	329690.6	3.4621955e+06	-2.4658194	0	2	3	0	3	2	1	3
0	121.210871899	31.2816275734	329690.16	3.462195e+06	-2.4273903	0	2	3	0	3	2	1	3
0	121.210867769	31.2816236035	329689.75	3.4621945e+06	-2.3859692	0	2	3	0	3	2	1	3
0	121.210863956	31.2816195483	329689.4	3.4621942e+06	-2.3422177	0	2	3	0	3	2	1	3
0	121.210860582	31.2816155492	329689.06	3.4621938e+06	-2.297404	0	2	3	0	3	2	1	3
0	121.210857178	31.281610987	329688.72	3.4621932e+06	-2.2443402	0	2	3	0	3	2	1	3
0	121.210854265	31.2816065112	329688.44	3.4621928e+06	-2.1911104	0	2	3	0	3	2	1	3
0	121.21085175	31.281602049	329688.2	3.4621922e+06	-2.13734	0	2	3	0	3	2	1	3
0	121.210849643	31.2815976857	329688	3.4621918e+06	-2.0844114	0	2	3	0	3	2	1	3
0	121.210847754	31.2815930673	329687.8	3.4621912e+06	-2.029026	0	2	3	0	3	2	1	3
0	121.21084615	31.2815883067	329687.66	3.4621908e+06	-1.9729096	0	2	3	0	3	2	1	3
0	121.210844873	31.2815835258	329687.5	3.4621902e+06	-1.9173506	0	2	3	0	3	2	1	3
0	121.21084388	31.2815785254	329687.4	3.4621895e+06	-1.8598708	0	2	3	0	3	2	1	3
0	121.210843215	31.2815734112	329687.34	3.462189e+06	-1.8015445	0	2	3	0	3	2	1	3
0	121.2108429	31.2815685805	329687.28	3.4621885e+06	-1.747241	0	2	3	0	3	2	1	3
0	121.210842852	31.2815635796	329687.28	3.462188e+06	-1.6937078	0	2	3	0	3	2	1	3
0	121.210843027	31.2815588748	329687.28	3.4621875e+06	-1.6471614	0	2	3	0	3	2	1	3
0	121.210843361	31.2815539628	329687.3	3.462187e+06	-1.6057783	0	2	3	0	3	2	1	3
0	121.210843737	31.2815491924	329687.34	3.4621862e+06	-1.576035	0	2	3	0	3	2	1	3
0	121.210844036	31.2815442294	329687.38	3.4621858e+06	-1.5611387	0	2	3	0	3	2	1	3
0	121.210844118	31.2815392565	329687.34	3.4621852e+06	-1.5647253	0	2	3	0	3	2	1	3
0	121.210844098	31.2815344245	329687.34	3.4621848e+06	-1.5737535	0	2	3	0	3	2	1	3
0	121.210843978	31.2815294734	329687.34	3.4621842e+06	-1.5855792	0	2	3	0	3	2	1	3
0	121.210843813	31.2815247516	329687.3	3.4621838e+06	-1.5962223	0	2	3	0	3	2	1	3
0	121.21084361	31.2815195792	329687.28	3.462183e+06	-1.6050924	0	2	3	0	3	2	1	3
0	121.210843404	31.2815143241	329687.25	3.4621825e+06	-1.6108414	0	2	3	0	3	2	1	3
0	121.210843219	31.2815098138	329687.22	3.462182e+06	-1.6144563	0	2	3	0	3	2	1	3
0	121.210842994	31.2815044534	329687.2	3.4621815e+06	-1.617587	0	2	3	0	3	2	1	3
0	121.210842781	31.2814995181	329687.16	3.4621808e+06	-1.6198026	0	2	3	0	3	2	1	3
0	121.210842578	31.2814942292	329687.12	3.4621802e+06	-1.6199273	0	2	3	0	3	2	1	3
14	121.210842433	31.2814905222	329687.12	3.4621798e+06	-1.6199863	0	2	3	2	3	2	1	3
14	121.210842294	31.2814852286	329687.1	3.4621792e+06	-1.6160516	0	0	3	0	3	2	1	3
14	121.210842194	31.2814801613	329687.06	3.4621788e+06	-1.6118718	0	0	3	0	3	2	1	3
14	121.210842128	31.2814754671	329687.06	3.4621782e+06	-1.6076921	0	0	3	0	3	2	1	3
14	121.210842075	31.2814694552	329687.03	3.4621775e+06	-1.6022724	0	0	3	0	3	2	1	3
14	121.210842059	31.281462511	329687.03	3.4621768e+06	-1.5960248	0	0	3	0	3	2	1	3
14	121.210842072	31.2814572865	329687.03	3.4621762e+06	-1.5916526	0	0	3	0	3	2	1	3
14	121.210842108	31.2814515539	329687	3.4621755e+06	-1.5873315	0	0	3	0	3	2	1	3
14	121.210842166	31.28144612	329687	3.462175e+06	-1.5833724	0	0	3	0	3	2	1	3
14	121.210842264	31.2814397173	329687	3.4621742e+06	-1.5787416	0	0	3	0	3	2	1	3
14	121.210842391	31.2814334968	329687	3.4621735e+06	-1.5742636	0	0	3	0	3	2	1	3
14	121.210842601	31.2814254414	329687	3.4621728e+06	-1.5684892	0	0	3	0	3	2	1	3
14	121.21084281	31.2814188217	329687.03	3.462172e+06	-1.5638059	0	0	3	0	3	2	1	3
14	121.210842975	31.2814141721	329687.03	3.4621715e+06	-1.5607132	0	0	3	0	3	2	1	3
14	121.210843284	31.2814061948	329687.03	3.4621705e+06	-1.5559746	0	0	3	0	3	2	1	3
14	121.210843666	31.2813971618	329687.06	3.4621695e+06	-1.5517548	0	0	3	0	3	2	1	3
14	121.210843879	31.2813924077	329687.06	3.462169e+06	-1.5499494	0	0	3	0	3	2	1	3
14	121.210844275	31.2813839129	329687.1	3.462168e+06	-1.5473782	0	0	3	0	3	2	1	3
14	121.210844514	31.2813789369	329687.1	3.4621675e+06	-1.54621	0	0	3	0	3	2	1	3
14	121.210844735	31.2813743973	329687.12	3.462167e+06	-1.545385	0	0	3	0	3	2	1	3
14	121.21084519	31.2813650655	329687.16	3.462166e+06	-1.5445999	0	0	3	0	3	2	1	3
14	121.210845619	31.2813558855	329687.2	3.462165e+06	-1.5454628	0	0	3	0	3	2	1	3
14	121.210845842	31.281350848	329687.2	3.4621642e+06	-1.5467097	0	0	3	0	3	2	1	3
14	121.210846193	31.281342231	329687.2	3.4621635e+06	-1.5500042	0	0	3	0	3	2	1	3
14	121.210846514	31.2813333831	329687.22	3.4621625e+06	-1.5542372	0	0	3	0	3	2	1	3
14	121.210846782	31.2813247137	329687.22	3.4621615e+06	-1.5589327	0	0	3	0	3	2	1	3
14	121.210846972	31.2813167875	329687.22	3.4621605e+06	-1.5640424	0	0	3	0	3	2	1	3
14	121.210847095	31.2813093532	329687.22	3.4621598e+06	-1.5696049	0	0	3	0	3	2	1	3
14	121.210847167	31.2813018507	329687.22	3.462159e+06	-1.5755782	0	0	3	0	3	2	1	3
14	121.210847186	31.2812951943	329687.22	3.4621582e+06	-1.5808733	0	0	3	0	3	2	1	3
14	121.210847171	31.2812885783	329687.2	3.4621575e+06	-1.5856441	0	0	3	0	3	2	1	3
14	121.210847138	31.2812827124	329687.2	3.4621568e+06	-1.58902	0	0	3	0	3	2	1	3
14	121.210847091	31.2812766917	329687.16	3.4621562e+06	-1.5915229	0	0	3	0	3	2	1	3
14	121.210847056	31.2812712543	329687.16	3.4621555e+06	-1.5923371	0	0	3	0	3	2	1	3
14	121.210847047	31.2812659689	329687.16	3.462155e+06	-1.5911467	0	0	3	0	3	2	1	3
14	121.210847069	31.2812608807	329687.12	3.4621545e+06	-1.5882285	0	0	3	0	3	2	1	3
14	121.210847134	31.2812539613	329687.12	3.4621538e+06	-1.5834898	0	0	3	0	3	2	1	3
14	121.210847231	31.281247582	329687.12	3.462153e+06	-1.579038	0	0	3	0	3	2	1	3
14	121.210847352	31.2812417144	329687.12	3.4621522e+06	-1.5749178	0	0	3	0	3	2	1	3
14	121.210847483	31.2812362004	329687.12	3.4621518e+06	-1.5715187	0	0	3	0	3	2	1	3
14	121.210847607	31.2812312293	329687.12	3.462151e+06	-1.5693661	0	0	3	0	3	2	1	3
14	121.210847723	31.2812266291	329687.12	3.4621505e+06	-1.5679537	0	0	3	0	3	2	1	3
14	121.210847864	31.2812209879	329687.16	3.46215e+06	-1.5669801	0	0	3	0	3	2	1	3
14	121.210847993	31.2812160416	329687.16	3.4621495e+06	-1.5662099	0	0	3	0	3	2	1	3
14	121.210848116	31.2812114234	329687.16	3.462149e+06	-1.5654899	0	0	3	0	3	2	1	3
14	121.210848241	31.2812068785	329687.16	3.4621485e+06	-1.5647792	0	0	3	0	3	2	1	3
14	121.210848386	31.2812022341	329687.16	3.462148e+06	-1.5633329	0	0	3	0	3	2	1	3
14	121.210848599	31.2811965049	329687.16	3.4621472e+06	-1.5600885	0	0	3	0	3	2	1	3
14	121.210848795	31.2811917411	329687.2	3.4621468e+06	-1.5573038	0	0	3	0	3	2	1	3
14	121.210849024	31.2811867033	329687.2	3.4621462e+06	-1.5540162	0	0	3	0	3	2	1	3
14	121.210849306	31.2811814605	329687.22	3.4621455e+06	-1.5492859	0	0	3	0	3	2	1	3
14	121.210849637	31.2811762744	329687.22	3.462145e+06	-1.5431978	0	0	3	0	3	2	1	3
14	121.210850035	31.2811707969	329687.25	3.4621445e+06	-1.5362054	0	0	3	0	3	2	1	3
14	121.210850489	31.2811648409	329687.28	3.4621438e+06	-1.5302672	0	0	3	0	3	2	1	3
14	121.210850855	31.2811600686	329687.3	3.4621432e+06	-1.5271717	0	0	3	0	3	2	1	3
14	121.210851307	31.2811541472	329687.34	3.4621425e+06	-1.5248669	0	0	3	0	3	2	1	3
14	121.210851714	31.2811484404	329687.38	3.462142e+06	-1.5251039	0	0	3	0	3	2	1	3
14	121.210852022	31.2811434676	329687.4	3.4621412e+06	-1.5282035	0	0	3	0	3	2	1	3
14	121.210852279	31.2811389138	329687.4	3.4621408e+06	-1.5311229	0	1	3	0	3	2	1	3
14	121.210852546	31.281133979	329687.44	3.4621402e+06	-1.5341918	0	1	3	0	3	2	1	3
14	121.210852783	31.2811293372	329687.44	3.4621398e+06	-1.537039	0	1	3	0	3	2	1	3
14	121.210853071	31.2811232819	329687.47	3.4621392e+06	-1.5407056	0	1	3	0	3	2	1	3
14	121.2108533	31.2811180434	329687.47	3.4621385e+06	-1.5438949	0	1	3	0	3	2	1	3
14	121.210853547	31.2811117925	329687.5	3.4621378e+06	-1.5477742	0	1	3	0	3	2	1	3
14	121.210853717	31.2811070642	329687.5	3.4621372e+06	-1.550698	0	1	3	0	3	2	1	3
14	121.21085388	31.2811021425	329687.5	3.4621368e+06	-1.5536585	0	1	3	0	3	2	1	3
14	121.210854034	31.2810970833	329687.5	3.4621362e+06	-1.5564635	0	1	3	0	3	2	1	3
14	121.210854185	31.2810916519	329687.5	3.4621358e+06	-1.5592378	0	1	3	0	3	2	1	3
14	121.210854334	31.2810856915	329687.53	3.462135e+06	-1.5620865	0	1	3	0	3	2	1	3
14	121.210854476	31.2810794243	329687.53	3.4621342e+06	-1.5647877	0	1	3	0	3	2	1	3
14	121.210854604	31.2810730985	329687.53	3.4621335e+06	-1.5672553	0	1	3	0	3	2	1	3
14	121.210854733	31.281065672	329687.53	3.4621328e+06	-1.570032	0	1	3	0	3	2	1	3
14	121.210854834	31.2810586661	329687.5	3.462132e+06	-1.5726185	0	1	3	0	3	2	1	3
14	121.210854918	31.2810513466	329687.5	3.4621312e+06	-1.5753053	0	1	3	0	3	2	1	3
14	121.210854984	31.2810435862	329687.5	3.4621302e+06	-1.5781426	0	1	3	0	3	2	1	3
14	121.210855026	31.2810355324	329687.5	3.4621295e+06	-1.5810786	0	1	3	0	3	2	1	3
14	121.210855041	31.2810281162	329687.47	3.4621285e+06	-1.5837837	0	1	3	0	3	2	1	3
14	121.210855032	31.2810199986	329687.47	3.4621278e+06	-1.5867743	0	1	3	0	3	2	1	3
14	121.210854999	31.281012444	329687.44	3.4621268e+06	-1.5897745	0	1	3	0	3	2	1	3
14	121.210854949	31.2810055793	329687.44	3.462126e+06	-1.5926926	0	1	3	0	3	2	1	3
14	121.21085488	31.2809992211	329687.4	3.4621255e+06	-1.5954969	0	1	3	0	3	2	1	3
14	121.210854804	31.280993813	329687.4	3.4621248e+06	-1.5979177	0	1	3	0	3	2	1	3
14	121.210854717	31.2809887655	329687.38	3.4621242e+06	-1.6001735	0	1	3	0	3	2	1	3
14	121.210854625	31.2809842117	329687.38	3.4621238e+06	-1.6021876	0	1	3	0	3	2	1	3
14	121.210854488	31.2809784007	329687.34	3.462123e+06	-1.6047271	0	1	3	0	3	2	1	3
14	121.210854337	31.2809728407	329687.3	3.4621225e+06	-1.6071372	0	1	3	0	3	2	1	3
14	121.210854187	31.2809679249	329687.28	3.462122e+06	-1.609281	0	1	3	0	3	2	1	3
14	121.210854144	31.2809666175	329687.28	3.4621218e+06	-1.6098561	0	1	3	1	3	2	1	3
0	121.210854186	31.2809614616	329687.28	3.4621212e+06	-1.5976546	0	2	3	0	3	2	1	3
0	121.210854322	31.2809559228	329687.28	3.4621205e+06	-1.5859637	0	2	3	0	3	2	1	3
0	121.210854504	31.2809513426	329687.28	3.46212e+06	-1.575433	0	2	3	0	3	2	1	3
0	121.210854806	31.2809457796	329687.3	3.4621195e+06	-1.5621978	0	2	3	0	3	2	1	3
0	121.210855224	31.2809397834	329687.34	3.4621188e+06	-1.5478009	0	2	3	0	3	2	1	3
0	121.210855633	31.2809351635	329687.38	3.4621182e+06	-1.5356625	0	2	3	0	3	2	1	3
0	121.210856281	31.2809292498	329687.4	3.4621175e+06	-1.5182827	0	2	3	0	3	2	1	3
0	121.210857015	31.280924051	329687.47	3.462117e+06	-1.4994102	0	2	3	0	3	2	1	3
0	121.210857872	31.2809192715	329687.56	3.4621165e+06	-1.4773948	0	2	3	0	3	2	1	3
0	121.210859097	31.2809138924	329687.66	3.462116e+06	-1.4462044	0	2	3	0	3	2	1	3
0	121.210860368	31.2809094596	329687.78	3.4621155e+06	-1.4142329	0	2	3	0	3	2	1	3
0	121.210862019	31.2809047137	329687.94	3.462115e+06	-1.3744588	0	2	3	0	3	2	1	3
0	121.210863903	31.2809000819	329688.1	3.4621145e+06	-1.3332844	0	2	3	0	3	2	1	3
0	121.210866083	31.280895529	329688.28	3.4621138e+06	-1.28769	0	2	3	0	3	2	1	3
0	121.210868571	31.2808911805	329688.53	3.4621135e+06	-1.2363628	0	2	3	0	3	2	1	3
0	121.210871511	31.2808866668	329688.78	3.462113e+06	-1.1811155	0	2	3	0	3	2	1	3
0	121.210874574	31.2808824354	329689.06	3.4621125e+06	-1.1302494	0	2	3	0	3	2	1	3
0	121.210877565	31.2808786515	329689.34	3.462112e+06	-1.0860071	0	2	3	0	3	2	1	3
0	121.210880928	31.2808746971	329689.66	3.4621115e+06	-1.0421276	0	2	3	0	3	2	1	3
0	121.210884376	31.2808708626	329690	3.462111e+06	-1.0040694	0	2	3	0	3	2	1	3
0	121.21088847	31.2808664808	329690.38	3.4621105e+06	-0.96873784	0	2	3	0	3	2	1	3
0	121.210892246	31.2808626995	329690.72	3.4621102e+06	-0.9378588	0	2	3	0	3	2	1	3
0	121.210896391	31.280858861	329691.12	3.4621098e+06	-0.9029944	0	2	3	0	3	2	1	3
0	121.210900407	31.2808553816	329691.5	3.4621092e+06	-0.87081635	0	2	3	0	3	2	1	3
0	121.21090479	31.2808518106	329691.9	3.462109e+06	-0.83759403	0	2	3	0	3	2	1	3
0	121.210909017	31.2808485253	329692.28	3.4621085e+06	-0.8090229	0	2	3	0	3	2	1	3
0	121.210913129	31.2808454454	329692.7	3.4621082e+06	-0.7845668	0	2	3	0	3	2	1	3
0	121.210917608	31.2808421585	329693.1	3.4621078e+06	-0.7633237	0	2	3	0	3	2	1	3
0	121.210922702	31.2808385735	329693.6	3.4621075e+06	-0.74016285	0	2	3	0	3	2	1	3
0	121.210928258	31.2808348454	329694.1	3.462107e+06	-0.7154778	0	2	3	0	3	2	1	3
0	121.210932585	31.2808320668	329694.5	3.4621068e+06	-0.69662875	0	2	3	0	3	2	1	3
0	121.210937687	31.2808289238	329695	3.4621062e+06	-0.6747827	0	2	3	0	3	2	1	3
0	121.210942517	31.2808260797	329695.44	3.462106e+06	-0.65433556	0	2	3	0	3	2	1	3
0	121.210947684	31.280823178	329695.94	3.4621058e+06	-0.63229406	0	2	3	0	3	2	1	3
0	121.210952823	31.2808204364	329696.4	3.4621055e+06	-0.61021787	0	2	3	0	3	2	1	3
0	121.210957929	31.2808178626	329696.9	3.462105e+06	-0.58751017	0	2	3	0	3	2	1	3
0	121.210962806	31.2808155304	329697.34	3.4621048e+06	-0.56588614	0	2	3	0	3	2	1	3
0	121.210967473	31.2808134121	329697.8	3.4621045e+06	-0.54532945	0	2	3	0	3	2	1	3
0	121.210973553	31.2808108204	329698.38	3.4621042e+06	-0.5182115	0	2	3	0	3	2	1	3
0	121.210979547	31.2808084573	329698.94	3.462104e+06	-0.4905538	0	2	3	0	3	2	1	3
0	121.21098542	31.2808063142	329699.5	3.4621038e+06	-0.46338907	0	2	3	0	3	2	1	3
0	121.210990968	31.2808044455	329700.03	3.4621035e+06	-0.43753058	0	2	3	0	3	2	1	3
0	121.210996306	31.2808027941	329700.53	3.4621035e+06	-0.41208047	0	2	3	0	3	2	1	3
0	121.211001485	31.2808013236	329701	3.4621032e+06	-0.3872018	0	2	3	0	3	2	1	3
0	121.211006857	31.2807999419	329701.53	3.462103e+06	-0.3606927	0	2	3	0	3	2	1	3
0	121.211012432	31.2807986581	329702.06	3.462103e+06	-0.3326597	0	2	3	0	3	2	1	3
0	121.211018656	31.2807974048	329702.66	3.4621028e+06	-0.30064613	0	2	3	0	3	2	1	3
0	121.211023831	31.280796511	329703.12	3.4621028e+06	-0.27331036	0	2	3	0	3	2	1	3
0	121.211029367	31.2807957239	329703.66	3.4621025e+06	-0.24210255	0	2	3	0	3	2	1	3
0	121.211035009	31.2807951172	329704.2	3.4621025e+06	-0.2075529	0	2	3	0	3	2	1	3
0	121.211041002	31.2807947168	329704.75	3.4621025e+06	-0.16675848	0	2	3	0	3	2	1	3
0	121.211047408	31.2807945842	329705.38	3.4621025e+06	-0.1186504	0	2	3	0	3	2	1	3
0	121.211054407	31.2807947636	329706.03	3.4621025e+06	-0.064171195	0	2	3	0	3	2	1	3
0	121.211061361	31.280795209	329706.72	3.4621025e+06	-0.013432701	0	2	3	0	3	2	1	3
0	121.211068911	31.2807958342	329707.44	3.4621025e+06	0.028675316	0	2	3	0	3	2	1	3
0	121.211076801	31.2807964191	329708.2	3.4621025e+06	0.048770472	0	2	3	0	3	2	1	3
0	121.211082066	31.2807967018	329708.7	3.4621025e+06	0.048773136	0	2	3	0	3	2	1	3
0	121.211087447	31.2807969299	329709.2	3.4621025e+06	0.043619383	0	2	3	0	3	2	1	3
0	121.211092761	31.2807970862	329709.7	3.4621025e+06	0.035135083	0	2	3	0	3	2	1	3
0	121.211098652	31.2807971516	329710.25	3.4621025e+06	0.021171598	0	2	3	0	3	2	1	3
0	121.21110437	31.2807971293	329710.8	3.4621025e+06	0.006056028	0	2	3	0	3	2	1	3
0	121.211111263	31.2807970581	329711.47	3.4621025e+06	-0.008492226	0	2	3	0	3	2	1	3
0	121.211117693	31.2807969909	329712.06	3.4621025e+06	-0.01687351	0	2	3	0	3	2	1	3
13	121.211121988	31.2807969311	329712.47	3.4621025e+06	-0.020786488	0	2	3	2	3	2	1	3
13	121.211128011	31.2807968362	329713.06	3.4621025e+06	-0.024967244	0	0	3	0	3	2	1	3
13	121.211134321	31.2807967183	329713.66	3.4621025e+06	-0.029644957	0	0	3	0	3	2	1	3
13	121.211140244	31.2807965897	329714.22	3.4621025e+06	-0.033752233	0	0	3	0	3	2	1	3
13	121.21114733	31.2807964152	329714.9	3.4621025e+06	-0.038325585	0	0	3	0	3	2	1	3
13	121.211152873	31.2807962749	329715.4	3.4621025e+06	-0.04093108	0	0	3	0	3	2	1	3
13	121.211158849	31.2807961248	329716	3.4621025e+06	-0.042708695	0	0	3	0	3	2	1	3
13	121.21116514	31.2807959696	329716.6	3.4621022e+06	-0.043664422	0	0	3	0	3	2	1	3
13	121.211171798	31.2807958047	329717.22	3.4621022e+06	-0.044234622	0	0	3	0	3	2	1	3
13	121.211179435	31.2807956138	329717.94	3.4621022e+06	-0.04471403	0	0	3	0	3	2	1	3
13	121.211187151	31.2807954279	329718.7	3.4621022e+06	-0.04457578	0	0	3	0	3	2	1	3
13	121.211195486	31.2807952481	329719.47	3.4621022e+06	-0.043046895	0	0	3	0	3	2	1	3
13	121.211203412	31.2807950998	329720.22	3.4621022e+06	-0.040538788	0	0	3	0	3	2	1	3
13	121.211212034	31.280794963	329721.06	3.4621022e+06	-0.037245244	0	0	3	0	3	2	1	3
13	121.211220881	31.2807948498	329721.9	3.4621022e+06	-0.033635058	0	0	3	0	3	2	1	3
13	121.211229444	31.2807947622	329722.72	3.4621022e+06	-0.030319855	0	0	3	0	3	2	1	3
13	121.211238626	31.280794679	329723.6	3.462102e+06	-0.027786022	0	0	3	0	3	2	1	3
13	121.211247723	31.2807946008	329724.44	3.462102e+06	-0.026492214	0	0	3	0	3	2	1	3
13	121.211256647	31.2807945294	329725.28	3.462102e+06	-0.025765002	0	0	3	0	3	2	1	3
13	121.21126589	31.2807944627	329726.2	3.462102e+06	-0.025024163	0	0	3	0	3	2	1	3
13	121.211274998	31.2807943977	329727.03	3.462102e+06	-0.024549708	0	0	3	0	3	2	1	3
13	121.21128387	31.2807943301	329727.88	3.462102e+06	-0.024673631	0	0	3	0	3	2	1	3
13	121.211292835	31.2807942652	329728.75	3.462102e+06	-0.024662474	0	0	3	0	3	2	1	3
13	121.211301625	31.2807942112	329729.56	3.462102e+06	-0.024003364	0	0	3	0	3	2	1	3
13	121.211310817	31.2807941699	329730.44	3.462102e+06	-0.022588383	0	0	3	0	3	2	1	3
13	121.21131962	31.2807941644	329731.28	3.462102e+06	-0.019612215	0	0	3	0	3	2	1	3
13	121.211328656	31.2807941966	329732.16	3.462102e+06	-0.015206818	0	0	3	0	3	2	1	3
13	121.211337688	31.2807942764	329733	3.462102e+06	-0.0097083915	0	0	3	0	3	2	1	3
13	121.211346671	31.2807944063	329733.88	3.462102e+06	-0.003441852	0	0	3	0	3	2	1	3
13	121.211355522	31.2807945779	329734.72	3.462102e+06	0.002682336	0	0	3	0	3	2	1	3
13	121.21136437	31.2807947744	329735.56	3.462102e+06	0.0075079836	0	0	3	0	3	2	1	3
13	121.21137274	31.2807949634	329736.34	3.462102e+06	0.010302171	0	0	3	0	3	2	1	3
13	121.211380335	31.2807951434	329737.06	3.462102e+06	0.011856393	0	0	3	0	3	2	1	3
13	121.211386863	31.2807953056	329737.7	3.462102e+06	0.012770923	0	0	3	0	3	2	1	3
13	121.211393539	31.2807954765	329738.34	3.462102e+06	0.013427658	0	0	3	0	3	2	1	3
13	121.211400979	31.2807956725	329739.03	3.462102e+06	0.01403699	0	0	3	0	3	2	1	3
13	121.211406825	31.2807958298	329739.6	3.462102e+06	0.014498782	0	0	3	0	3	2	1	3
13	121.211412594	31.2807959879	329740.16	3.462102e+06	0.01496004	0	0	3	0	3	2	1	3
13	121.211418081	31.2807961405	329740.66	3.462102e+06	0.015383485	0	1	3	0	3	2	1	3
13	121.211423404	31.2807962904	329741.2	3.462102e+06	0.01579734	0	1	3	0	3	2	1	3
13	121.211429143	31.2807964511	329741.72	3.462102e+06	0.017496966	0	1	3	0	3	2	1	3
13	121.211435575	31.280796644	329742.34	3.462102e+06	0.017968878	0	1	3	0	3	2	1	3
13	121.21144277	31.2807968625	329743.03	3.462102e+06	0.018488193	0	1	3	0	3	2	1	3
13	121.211449421	31.280797067	329743.66	3.462102e+06	0.018962242	0	1	3	0	3	2	1	3
13	121.211454733	31.2807972321	329744.16	3.462102e+06	0.01933615	0	1	3	0	3	2	1	3
13	121.211460218	31.2807974042	329744.7	3.462102e+06	0.019704731	0	1	3	0	3	2	1	3
13	121.211466269	31.2807975931	329745.25	3.462102e+06	0.019937487	0	1	3	0	3	2	1	3
13	121.211472539	31.2807977837	329745.84	3.462102e+06	0.01976811	0	1	3	0	3	2	1	3
13	121.211479237	31.2807979817	329746.5	3.462102e+06	0.01919926	0	1	3	0	3	2	1	3
13	121.211486292	31.2807981859	329747.16	3.462102e+06	0.01845624	0	1	3	0	3	2	1	3
13	121.211493555	31.2807983913	329747.84	3.4621022e+06	0.01766741	0	1	3	0	3	2	1	3
13	121.211501891	31.2807986212	329748.66	3.4621022e+06	0.016758207	0	1	3	0	3	2	1	3
13	121.211509884	31.280798835	329749.4	3.4621022e+06	0.015843678	0	1	3	0	3	2	1	3
13	121.211517527	31.2807990325	329750.16	3.4621022e+06	0.014800783	0	1	3	0	3	2	1	3
13	121.211523972	31.280799194	329750.75	3.4621022e+06	0.013869742	0	1	3	0	3	2	1	3
13	121.211530107	31.2807993443	329751.34	3.4621022e+06	0.013063869	0	1	3	0	3	2	1	3
13	121.211535555	31.2807994754	329751.84	3.4621022e+06	0.01249928	0	1	3	0	3	2	1	3
13	121.211541655	31.2807996189	329752.44	3.4621022e+06	0.011955461	0	1	3	0	3	2	1	3
13	121.211547436	31.2807997527	329753	3.4621022e+06	0.011527758	0	1	3	0	3	2	1	3
13	121.211551046	31.2807998351	329753.34	3.4621022e+06	0.011292868	0	1	3	1	3	2	1	3
0	121.211557035	31.2807999183	329753.9	3.4621022e+06	0.0077945394	0	2	3	0	3	2	1	3
0	121.211562806	31.2807999809	329754.47	3.4621022e+06	0.004020308	0	2	3	0	3	2	1	3
0	121.211568566	31.2808000224	329755	3.4621022e+06	2.7164246e-05	0	2	3	0	3	2	1	3
0	121.211575887	31.2808008662	329755.7	3.4621022e+06	0.07729934	0	2	3	0	3	2	1	3
0	121.211581996	31.2808016838	329756.28	3.4621022e+06	0.099859014	0	2	3	0	3	2	1	3
0	121.211588674	31.2808027469	329756.9	3.4621025e+06	0.12679963	0	2	3	0	3	2	1	3
0	121.21159622	31.2808041576	329757.66	3.4621025e+06	0.15897119	0	2	3	0	3	2	1	3
0	121.211603438	31.2808057421	329758.34	3.4621028e+06	0.19249411	0	2	3	0	3	2	1	3
0	121.211610765	31.2808076314	329759.03	3.462103e+06	0.22931771	0	2	3	0	3	2	1	3
0	121.211618367	31.2808098466	329759.75	3.4621032e+06	0.26808214	0	2	3	0	3	2	1	3
0	121.211623871	31.2808116313	329760.28	3.4621035e+06	0.29664445	0	2	3	0	3	2	1	3
0	121.211631103	31.280814212	329760.97	3.4621038e+06	0.33475462	0	2	3	0	3	2	1	3
0	121.211637746	31.2808168569	329761.62	3.462104e+06	0.37069649	0	2	3	0	3	2	1	3
0	121.211644354	31.2808197363	329762.25	3.4621042e+06	0.40739888	0	2	3	0	3	2	1	3
0	121.211650062	31.2808224505	329762.8	3.4621045e+06	0.43989903	0	2	3	0	3	2	1	3
0	121.211655269	31.2808251234	329763.3	3.4621048e+06	0.4702842	0	2	3	0	3	2	1	3
0	121.211660128	31.2808277964	329763.78	3.4621052e+06	0.49933672	0	2	3	0	3	2	1	3
0	121.211664499	31.280830358	329764.2	3.4621055e+06	0.5261192	0	2	3	0	3	2	1	3
0	121.2116696	31.2808335611	329764.7	3.4621058e+06	0.55901283	0	2	3	0	3	2	1	3
0	121.211674044	31.2808365495	329765.12	3.462106e+06	0.5887357	0	2	3	0	3	2	1	3
0	121.211678855	31.2808400031	329765.56	3.4621065e+06	0.62180275	0	2	3	0	3	2	1	3
0	121.211683013	31.2808431836	329765.97	3.4621068e+06	0.65120435	0	2	3	0	3	2	1	3
0	121.211687076	31.2808464633	329766.38	3.4621072e+06	0.6798705	0	2	3	0	3	2	1	3
0	121.211691462	31.2808501461	329766.78	3.4621075e+06	0.7078325	0	2	3	0	3	2	1	3
0	121.211696387	31.2808544221	329767.28	3.462108e+06	0.7345442	0	2	3	0	3	2	1	3
0	121.211700251	31.2808578965	329767.66	3.4621085e+06	0.75389194	0	2	3	0	3	2	1	3
0	121.211704291	31.2808616731	329768.03	3.4621088e+06	0.77442455	0	2	3	0	3	2	1	3
0	121.211708218	31.2808654802	329768.4	3.4621092e+06	0.7941263	0	2	3	0	3	2	1	3
0	121.211712348	31.2808696077	329768.8	3.4621098e+06	0.81328034	0	2	3	0	3	2	1	3
0	121.211716656	31.2808740711	329769.25	3.4621102e+06	0.8329294	0	2	3	0	3	2	1	3
0	121.211720797	31.2808785316	329769.62	3.4621108e+06	0.85219485	0	2	3	0	3	2	1	3
0	121.211725239	31.280883516	329770.06	3.4621112e+06	0.8733572	0	2	3	0	3	2	1	3
0	121.211729621	31.2808886756	329770.5	3.4621118e+06	0.8956928	0	2	3	0	3	2	1	3
0	121.211734047	31.2808941742	329770.94	3.4621125e+06	0.9203035	0	2	3	0	3	2	1	3
0	121.211737011	31.2808980237	329771.22	3.4621128e+06	0.93733805	0	2	3	0	3	2	1	3
0	121.211740121	31.2809022206	329771.53	3.4621132e+06	0.95567536	0	2	3	0	3	2	1	3
0	121.21174313	31.2809064477	329771.8	3.4621138e+06	0.9739123	0	2	3	0	3	2	1	3
0	121.211746161	31.2809108901	329772.12	3.4621142e+06	0.9929066	0	2	3	0	3	2	1	3
0	121.211748913	31.2809150972	329772.38	3.4621148e+06	1.0107192	0	2	3	0	3	2	1	3
0	121.211751856	31.2809198092	329772.66	3.4621152e+06	1.0307283	0	2	3	0	3	2	1	3
0	121.211754611	31.2809244808	329772.94	3.4621158e+06	1.0516771	0	2	3	0	3	2	1	3
0	121.211757255	31.2809292425	329773.2	3.4621162e+06	1.0739319	0	2	3	0	3	2	1	3
0	121.211759756	31.2809340168	329773.44	3.4621168e+06	1.0962539	0	2	3	0	3	2	1	3
0	121.211762202	31.280939005	329773.7	3.4621172e+06	1.1198725	0	2	3	0	3	2	1	3
0	121.211764462	31.2809439852	329773.9	3.4621178e+06	1.144539	0	2	3	0	3	2	1	3
0	121.211766635	31.2809492177	329774.12	3.4621185e+06	1.171841	0	2	3	0	3	2	1	3
0	121.211768533	31.2809542897	329774.3	3.462119e+06	1.1999214	0	2	3	0	3	2	1	3
0	121.211770228	31.2809594	329774.5	3.4621195e+06	1.2301216	0	2	3	0	3	2	1	3
0	121.21177176	31.2809646706	329774.66	3.4621202e+06	1.2622726	0	2	3	0	3	2	1	3
0	121.211772998	31.280969598	329774.78	3.4621208e+06	1.2925469	0	2	3	0	3	2	1	3
0	121.211773986	31.2809742406	329774.88	3.4621212e+06	1.321508	0	2	3	0	3	2	1	3
0	121.211774813	31.2809808618	329774.97	3.462122e+06	1.7434758	0	2	3	0	3	2	1	3
0	121.211774716	31.2809854794	329774.97	3.4621225e+06	1.6887585	0	2	3	0	3	2	1	3
0	121.211774757	31.2809907447	329774.97	3.462123e+06	1.6379862	0	2	3	0	3	2	1	3
8	121.21177465	31.2809938282	329774.97	3.4621232e+06	1.5589936	0	2	3	0	3	2	1	3
8	121.211774651	31.2809941533	329774.97	3.4621235e+06	1.5588441	0	2	3	2	3	2	1	3
8	121.21177466	31.280998949	329774.97	3.462124e+06	1.5551938	0	0	3	0	3	2	1	3
8	121.211774695	31.2810038436	329775	3.4621245e+06	1.5526977	0	0	3	0	3	2	1	3
8	121.211774733	31.2810092323	329775	3.462125e+06	1.5512656	0	0	3	0	3	2	1	3
8	121.211774736	31.2810140427	329775	3.4621255e+06	1.5521948	0	0	3	0	3	2	1	3
8	121.211774731	31.2810193444	329775.03	3.4621262e+06	1.5534059	0	0	3	0	3	2	1	3
8	121.211774721	31.2810239307	329775.03	3.4621268e+06	1.5543876	0	0	3	0	3	2	1	3
8	121.211774706	31.2810285829	329775.03	3.4621272e+06	1.5553857	0	0	3	0	3	2	1	3
8	121.211774679	31.2810344905	329775.06	3.4621278e+06	1.5566615	0	0	3	0	3	2	1	3
8	121.211774659	31.281039346	329775.06	3.4621285e+06	1.557181	0	0	3	0	3	2	1	3
8	121.211774656	31.2810448078	329775.06	3.462129e+06	1.5564612	0	0	3	0	3	2	1	3
8	121.211774658	31.2810499669	329775.06	3.4621295e+06	1.5557215	0	0	3	0	3	2	1	3
8	121.211774664	31.2810548504	329775.1	3.4621302e+06	1.5550251	0	0	3	0	3	2	1	3
8	121.211774674	31.2810594465	329775.1	3.4621308e+06	1.5543706	0	0	3	0	3	2	1	3
8	121.211774692	31.281064558	329775.1	3.4621312e+06	1.5534298	0	0	3	0	3	2	1	3
8	121.211774724	31.2810704816	329775.12	3.4621318e+06	1.5520897	0	0	3	0	3	2	1	3
8	121.211774753	31.2810754339	329775.12	3.4621325e+06	1.5512493	0	0	3	0	3	2	1	3
8	121.211774782	31.281080779	329775.12	3.462133e+06	1.5508374	0	0	3	0	3	2	1	3
8	121.211774816	31.2810867886	329775.16	3.4621338e+06	1.5504098	0	0	3	0	3	2	1	3
8	121.211774858	31.2810934412	329775.16	3.4621345e+06	1.5499336	0	0	3	0	3	2	1	3
8	121.211774889	31.2810980927	329775.2	3.462135e+06	1.5495847	0	0	3	0	3	2	1	3
8	121.211774925	31.2811031352	329775.2	3.4621355e+06	1.549211	0	0	3	0	3	2	1	3
8	121.211774956	31.2811084712	329775.22	3.462136e+06	1.5493453	0	0	3	0	3	2	1	3
8	121.211774973	31.2811139768	329775.22	3.4621368e+06	1.5503047	0	0	3	0	3	2	1	3
8	121.211774981	31.2811197968	329775.22	3.4621372e+06	1.551677	0	0	3	0	3	2	1	3
8	121.211774976	31.2811257496	329775.25	3.462138e+06	1.5532963	0	0	3	0	3	2	1	3
8	121.211774931	31.2811318728	329775.25	3.4621388e+06	1.5564476	0	0	3	0	3	2	1	3
8	121.211774835	31.2811384087	329775.25	3.4621395e+06	1.5614815	0	0	3	0	3	2	1	3
8	121.211774688	31.2811451777	329775.25	3.4621402e+06	1.5673528	0	0	3	0	3	2	1	3
8	121.211774502	31.2811518827	329775.25	3.462141e+06	1.5730463	0	0	3	0	3	2	1	3
8	121.211774249	31.2811593125	329775.22	3.4621418e+06	1.5789915	0	0	3	0	3	2	1	3
8	121.211773962	31.281166616	329775.22	3.4621425e+06	1.5843118	0	0	3	0	3	2	1	3
8	121.211773659	31.2811737218	329775.2	3.4621432e+06	1.5883317	0	0	3	0	3	2	1	3
8	121.211773321	31.2811812764	329775.2	3.4621442e+06	1.5913639	0	0	3	0	3	2	1	3
8	121.211772999	31.2811888304	329775.16	3.462145e+06	1.5919833	0	0	3	0	3	2	1	3
8	121.211772717	31.2811968093	329775.16	3.4621458e+06	1.5886306	0	0	3	0	3	2	1	3
8	121.21177248	31.2812047805	329775.16	3.4621468e+06	1.5836434	0	0	3	0	3	2	1	3
8	121.211772253	31.2812133667	329775.12	3.4621478e+06	1.5791459	0	0	3	0	3	2	1	3
8	121.211772069	31.2812216919	329775.12	3.4621488e+06	1.5754303	0	0	3	0	3	2	1	3
8	121.211771985	31.2812262358	329775.12	3.4621492e+06	1.5734128	0	0	3	0	3	2	1	3
8	121.211771866	31.2812343514	329775.12	3.46215e+06	1.569488	0	0	3	0	3	2	1	3
8	121.211771792	31.2812429812	329775.16	3.462151e+06	1.5646013	0	0	3	0	3	2	1	3
8	121.211771751	31.2812518492	329775.16	3.462152e+06	1.5601128	0	0	3	0	3	2	1	3
8	121.211771722	31.2812609688	329775.2	3.462153e+06	1.5574644	0	0	3	0	3	2	1	3
8	121.211771689	31.2812697022	329775.2	3.462154e+06	1.5570852	0	0	3	0	3	2	1	3
8	121.211771643	31.2812783602	329775.2	3.462155e+06	1.5581845	0	0	3	0	3	2	1	3
8	121.211771587	31.2812867532	329775.22	3.4621558e+06	1.5596974	0	0	3	0	3	2	1	3
8	121.211771522	31.2812944782	329775.22	3.4621568e+06	1.5611922	0	0	3	0	3	2	1	3
8	121.211771449	31.2813016711	329775.22	3.4621575e+06	1.5627047	0	0	3	0	3	2	1	3
8	121.211771373	31.2813080212	329775.22	3.4621582e+06	1.5641255	0	0	3	0	3	2	1	3
8	121.2117713	31.2813133944	329775.22	3.4621588e+06	1.5653466	0	0	3	0	3	2	1	3
8	121.211771202	31.2813196881	329775.22	3.4621595e+06	1.5667764	0	0	3	0	3	2	1	3
8	121.21177112	31.281324348	329775.22	3.46216e+06	1.5678338	0	0	3	0	3	2	1	3
8	121.211771031	31.2813290389	329775.22	3.4621605e+06	1.5689198	0	0	3	0	3	2	1	3
8	121.211770943	31.2813336563	329775.22	3.462161e+06	1.5703892	0	0	3	0	3	2	1	3
8	121.211770846	31.2813382759	329775.22	3.4621615e+06	1.5720363	0	1	3	0	3	2	1	3
8	121.211770744	31.2813428589	329775.22	3.462162e+06	1.5723042	0	1	3	0	3	2	1	3
8	121.211770642	31.2813475335	329775.22	3.4621625e+06	1.572285	0	1	3	0	3	2	1	3
8	121.211770536	31.2813523858	329775.22	3.462163e+06	1.5726343	0	1	3	0	3	2	1	3
8	121.211770428	31.2813573145	329775.22	3.4621638e+06	1.5729396	0	1	3	0	3	2	1	3
8	121.211770332	31.2813623077	329775.22	3.4621642e+06	1.5723301	0	1	3	0	3	2	1	3
8	121.211770288	31.2813673137	329775.22	3.4621648e+06	1.5689547	0	1	3	0	3	2	1	3
8	121.211770248	31.2813721141	329775.22	3.4621652e+06	1.5666012	0	1	3	0	3	2	1	3
8	121.21177021	31.281376718	329775.25	3.4621658e+06	1.5651077	0	1	3	0	3	2	1	3
8	121.211770141	31.2813820613	329775.25	3.4621665e+06	1.5653507	0	1	3	0	3	2	1	3
8	121.211770056	31.2813877983	329775.25	3.462167e+06	1.5661561	0	1	3	0	3	2	1	3
8	121.21176998	31.2813926591	329775.25	3.4621675e+06	1.5668356	0	1	3	0	3	2	1	3
8	121.211769896	31.2813979934	329775.25	3.4621682e+06	1.5673566	0	1	3	0	3	2	1	3
8	121.211769804	31.2814041011	329775.25	3.4621688e+06	1.567486	0	1	3	0	3	2	1	3
8	121.211769723	31.2814102356	329775.25	3.4621695e+06	1.5669183	0	1	3	0	3	2	1	3
8	121.211769671	31.281414792	329775.25	3.46217e+06	1.5661262	0	1	3	0	3	2	1	3
8	121.211769603	31.281421416	329775.25	3.4621708e+06	1.5649205	0	1	3	0	3	2	1	3
8	121.211769558	31.2814263172	329775.25	3.4621712e+06	1.5640576	0	1	3	0	3	2	1	3
8	121.211769513	31.2814315291	329775.28	3.462172e+06	1.5632927	0	1	3	0	3	2	1	3
8	121.211769467	31.2814368282	329775.28	3.4621725e+06	1.562813	0	1	3	0	3	2	1	3
8	121.211769416	31.2814420863	329775.28	3.462173e+06	1.562795	0	1	3	0	3	2	1	3
8	121.211769353	31.2814477381	329775.28	3.4621738e+06	1.5633475	0	1	3	0	3	2	1	3
8	121.211769283	31.281453326	329775.28	3.4621742e+06	1.5642018	0	1	3	0	3	2	1	3
8	121.211769198	31.2814596273	329775.28	3.462175e+06	1.5652184	0	1	3	0	3	2	1	3
8	121.211769111	31.2814655334	329775.28	3.4621758e+06	1.5661751	0	1	3	0	3	2	1	3
8	121.211769016	31.2814715273	329775.28	3.4621762e+06	1.5671457	0	1	3	0	3	2	1	3
8	121.211768914	31.2814775657	329775.28	3.462177e+06	1.5681176	0	1	3	0	3	2	1	3
8	121.21176881	31.281483423	329775.28	3.4621778e+06	1.5690134	0	1	3	0	3	2	1	3
8	121.211768717	31.2814884312	329775.28	3.4621782e+06	1.569768	0	1	3	0	3	2	1	3
8	121.211768593	31.2814947624	329775.28	3.462179e+06	1.5707248	0	1	3	0	3	2	1	3
8	121.211768492	31.2814995759	329775.28	3.4621795e+06	1.5714543	0	1	3	0	3	2	1	3
8	121.211768385	31.2815044889	329775.28	3.46218e+06	1.5722078	0	1	3	0	3	2	1	3
8	121.211768345	31.281506217	329775.28	3.4621802e+06	1.5724922	0	1	3	1	3	2	1	3
0	121.211768189	31.2815107987	329775.28	3.4621808e+06	1.5764493	0	2	3	0	3	2	1	3
0	121.21176797	31.2815160956	329775.28	3.4621812e+06	1.5812776	0	2	3	0	3	2	1	3
0	121.211767667	31.2815217342	329775.25	3.462182e+06	1.5886123	0	2	3	0	3	2	1	3
0	121.211767947	31.2815260804	329775.28	3.4621822e+06	1.5752916	0	2	3	0	3	2	1	3
0	121.211767883	31.2815308736	329775.28	3.462183e+06	1.5722781	0	2	3	0	3	2	1	3
0	121.211767919	31.2815355896	329775.3	3.4621835e+06	1.5642622	0	2	3	0	3	2	1	3
0	121.211768104	31.2815401391	329775.34	3.462184e+06	1.551045	0	2	3	0	3	2	1	3
0	121.211768607	31.2815453958	329775.38	3.4621845e+06	1.5234219	0	2	3	0	3	2	1	3
0	121.211769322	31.2815502015	329775.47	3.462185e+06	1.4919524	0	2	3	0	3	2	1	3
0	121.211770274	31.2815549125	329775.56	3.4621855e+06	1.4563503	0	2	3	0	3	2	1	3
0	121.211771588	31.2815599908	329775.7	3.4621862e+06	1.4137312	0	2	3	0	3	2	1	3
0	121.211773418	31.2815656059	329775.88	3.4621868e+06	1.3621305	0	2	3	0	3	2	1	3
0	121.211775795	31.2815711785	329776.12	3.4621872e+06	1.2985166	0	2	3	0	3	2	1	3
0	121.211778484	31.2815761084	329776.38	3.462188e+06	1.2309209	0	2	3	0	3	2	1	3
0	121.211781413	31.2815804838	329776.66	3.4621885e+06	1.1621931	0	2	3	0	3	2	1	3
0	121.211785296	31.2815849444	329777.06	3.4621888e+06	1.0686951	0	2	3	0	3	2	1	3
0	121.211789124	31.2815881628	329777.4	3.4621892e+06	0.97105587	0	2	3	0	3	2	1	3
0	121.211793713	31.2815912511	329777.88	3.4621895e+06	0.86300373	0	2	3	0	3	2	1	3
0	121.211798285	31.2815939282	329778.3	3.4621898e+06	0.7733257	0	2	3	0	3	2	1	3
0	121.211802982	31.2815964676	329778.75	3.46219e+06	0.70077664	0	2	3	0	3	2	1	3
0	121.211807849	31.2815990287	329779.22	3.4621905e+06	0.64596814	0	2	3	0	3	2	1	3
0	121.211812771	31.2816014324	329779.7	3.4621908e+06	0.59848005	0	2	3	0	3	2	1	3
0	121.211818545	31.2816038653	329780.25	3.462191e+06	0.54020816	0	2	3	0	3	2	1	3
0	121.21182369	31.2816054884	329780.75	3.462191e+06	0.47427034	0	2	3	0	3	2	1	3
0	121.211829789	31.2816066609	329781.3	3.4621912e+06	0.37703577	0	2	3	0	3	2	1	3
0	121.211835013	31.2816071784	329781.8	3.4621912e+06	0.29099315	0	2	3	0	3	2	1	3
0	121.211841383	31.2816072654	329782.44	3.4621912e+06	0.18603702	0	2	3	0	3	2	1	3
0	121.211846808	31.2816069005	329782.94	3.4621912e+06	0.09763367	0	2	3	0	3	2	1	3
0	121.211852287	31.281606242	329783.47	3.4621912e+06	0.01754384	0	2	3	0	3	2	1	3
0	121.211854987	31.281606117	329783.72	3.462191e+06	0.023692546	0	2	3	0	3	2	1	3
0	121.21186073	31.2816063228	329784.28	3.4621912e+06	0.024339164	0	2	3	0	3	2	1	3
5	121.211864025	31.2816064421	329784.6	3.4621912e+06	0.024693362	0	2	3	2	3	2	1	3
5	121.211869324	31.2816065964	329785.1	3.4621912e+06	0.024547422	0	0	3	0	3	2	1	3
5	121.211875303	31.2816067712	329785.66	3.4621912e+06	0.022278938	0	0	3	0	3	2	1	3
5	121.211881966	31.2816069457	329786.28	3.4621912e+06	0.01929513	0	0	3	0	3	2	1	3
5	121.211888216	31.2816070944	329786.88	3.4621912e+06	0.0165084	0	0	3	0	3	2	1	3
5	121.211894904	31.2816072375	329787.53	3.4621912e+06	0.013538444	0	0	3	0	3	2	1	3
5	121.21190039	31.2816073423	329788.03	3.4621912e+06	0.011094197	0	0	3	0	3	2	1	3
5	121.211906087	31.2816074386	329788.6	3.4621912e+06	0.008522115	0	0	3	0	3	2	1	3
5	121.211912813	31.2816075362	329789.22	3.4621912e+06	0.0054802517	0	0	3	0	3	2	1	3
5	121.211918428	31.2816076042	329789.75	3.4621912e+06	0.0029369344	0	0	3	0	3	2	1	3
5	121.211923931	31.2816076588	329790.28	3.4621912e+06	0.00044101945	0	0	3	0	3	2	1	3
5	121.211929197	31.2816077003	329790.78	3.4621912e+06	-0.0019228096	0	0	3	0	3	2	1	3
5	121.21193454	31.2816077341	329791.28	3.4621912e+06	-0.0041264403	0	0	3	0	3	2	1	3
5	121.211941425	31.2816077644	329791.94	3.4621912e+06	-0.006845599	0	0	3	0	3	2	1	3
5	121.211948122	31.2816077899	329792.6	3.4621912e+06	-0.008730148	0	0	3	0	3	2	1	3
5	121.211955057	31.2816078248	329793.25	3.4621912e+06	-0.009328051	0	0	3	0	3	2	1	3
5	121.211960326	31.2816078512	329793.75	3.4621912e+06	-0.009616929	0	0	3	0	3	2	1	3
5	121.211967458	31.281607885	329794.44	3.4621912e+06	-0.0100050485	0	0	3	0	3	2	1	3
5	121.211972913	31.2816079094	329794.94	3.4621912e+06	-0.010301028	0	0	3	0	3	2	1	3
5	121.211978714	31.2816079338	329795.5	3.462191e+06	-0.010611405	0	0	3	0	3	2	1	3
5	121.211984456	31.2816079721	329796.06	3.462191e+06	-0.009909079	0	0	3	0	3	2	1	3
5	121.211990844	31.2816080464	329796.66	3.462191e+06	-0.0071469108	0	0	3	0	3	2	1	3
5	121.211997126	31.2816081373	329797.25	3.462191e+06	-0.004155108	0	0	3	0	3	2	1	3
5	121.212003663	31.2816082491	329797.88	3.462191e+06	-0.0010434535	0	0	3	0	3	2	1	3
5	121.212010358	31.2816083815	329798.53	3.462191e+06	0.0021369203	0	0	3	0	3	2	1	3
5	121.212017123	31.2816085337	329799.16	3.4621912e+06	0.0053433664	0	0	3	0	3	2	1	3
5	121.212024168	31.2816087067	329799.84	3.4621912e+06	0.008362327	0	0	3	0	3	2	1	3
5	121.212031181	31.2816088771	329800.5	3.4621912e+06	0.010043312	0	0	3	0	3	2	1	3
5	121.212038437	31.2816090197	329801.2	3.4621912e+06	0.008828378	0	0	3	0	3	2	1	3
5	121.212046057	31.2816091265	329801.9	3.4621912e+06	0.0049950243	0	0	3	0	3	2	1	3
5	121.212051363	31.2816091866	329802.4	3.4621912e+06	0.0022828614	0	0	3	0	3	2	1	3
5	121.212058926	31.2816092618	329803.16	3.4621912e+06	-0.0008454544	0	0	3	0	3	2	1	3
5	121.212066397	31.28160933	329803.84	3.4621912e+06	-0.0029597923	0	0	3	0	3	2	1	3
5	121.212074339	31.2816094121	329804.62	3.4621912e+06	-0.003575078	0	0	3	0	3	2	1	3
5	121.212082277	31.2816095304	329805.38	3.4621912e+06	-0.0014506743	0	0	3	0	3	2	1	3
5	121.212087719	31.2816096298	329805.88	3.4621912e+06	0.0008617988	0	0	3	0	3	2	1	3
5	121.212093223	31.2816097415	329806.4	3.4621912e+06	0.0032522525	0	0	3	0	3	2	1	3
5	121.212098562	31.281609861	329806.9	3.4621912e+06	0.005570799	0	0	3	0	3	2	1	3
5	121.212104376	31.2816100009	329807.47	3.4621912e+06	0.0079671135	0	0	3	0	3	2	1	3
5	121.212110035	31.2816101437	329808	3.4621912e+06	0.009945308	0	0	3	0	3	2	1	3
5	121.212115583	31.2816102876	329808.53	3.4621912e+06	0.011503256	0	0	3	0	3	2	1	3
5	121.212121401	31.2816104366	329809.1	3.4621912e+06	0.012438561	0	0	3	0	3	2	1	3
5	121.212127054	31.2816105689	329809.62	3.4621912e+06	0.01217544	0	0	3	0	3	2	1	3
5	121.212132809	31.2816106802	329810.2	3.4621912e+06	0.010254766	0	0	3	0	3	2	1	3
5	121.212138583	31.281610777	329810.72	3.4621912e+06	0.0077066547	0	0	3	0	3	2	1	3
5	121.212144302	31.2816108601	329811.28	3.4621912e+06	0.005116464	0	0	3	0	3	2	1	3
5	121.212149877	31.2816109305	329811.8	3.4621912e+06	0.0027025756	0	0	3	0	3	2	1	3
5	121.21215589	31.2816109978	329812.38	3.4621912e+06	0.0004048005	0	0	3	0	3	2	1	3
5	121.212161317	31.2816110509	329812.9	3.4621912e+06	-0.0014480072	0	0	3	0	3	2	1	3
5	121.212167066	31.2816110989	329813.44	3.4621912e+06	-0.0033041309	0	0	3	0	3	2	1	3
5	121.212172845	31.2816111379	329814	3.4621912e+06	-0.0051587117	0	0	3	0	3	2	1	3
5	121.212178572	31.2816111674	329814.53	3.4621912e+06	-0.007007524	0	0	3	0	3	2	1	3
5	121.212186324	31.2816111925	329815.28	3.4621912e+06	-0.009532582	0	0	3	0	3	2	1	3
5	121.212193542	31.2816112001	329815.97	3.4621912e+06	-0.011904222	0	0	3	0	3	2	1	3
5	121.212200332	31.2816111968	329816.6	3.4621912e+06	-0.013921084	0	0	3	0	3	2	1	3
5	121.212206428	31.2816112016	329817.2	3.4621912e+06	-0.014502804	0	0	3	0	3	2	1	3
5	121.212212053	31.2816112113	329817.72	3.4621912e+06	-0.014346531	0	0	3	0	3	2	1	3
5	121.212217362	31.2816112217	329818.22	3.462191e+06	-0.014152408	0	0	3	0	3	2	1	3
5	121.21222044	31.281611228	329818.53	3.462191e+06	-0.014041828	0	0	3	0	3	2	1	3
5	121.212226152	31.2816112778	329819.06	3.462191e+06	-0.009292578	0	1	3	0	3	2	1	3
5	121.212231506	31.2816113513	329819.56	3.462191e+06	-0.0064258394	0	1	3	0	3	2	1	3
5	121.212236925	31.2816114389	329820.1	3.462191e+06	-0.0035135392	0	1	3	0	3	2	1	3
5	121.212242773	31.2816115489	329820.66	3.462191e+06	-0.0003515233	0	1	3	0	3	2	1	3
5	121.21224808	31.2816116626	329821.16	3.462191e+06	0.0025118939	0	1	3	0	3	2	1	3
5	121.212254119	31.2816118076	329821.72	3.4621912e+06	0.005747635	0	1	3	0	3	2	1	3
5	121.212260045	31.2816119657	329822.28	3.4621912e+06	0.00890508	0	1	3	0	3	2	1	3
5	121.212265641	31.2816121295	329822.8	3.4621912e+06	0.011874501	0	1	3	0	3	2	1	3
5	121.212271992	31.2816123321	329823.44	3.4621912e+06	0.01523062	0	1	3	0	3	2	1	3
5	121.212278572	31.2816125611	329824.06	3.4621912e+06	0.018694863	0	1	3	0	3	2	1	3
5	121.212284234	31.2816127714	329824.6	3.4621912e+06	0.02153166	0	1	3	0	3	2	1	3
5	121.212290192	31.2816129755	329825.16	3.4621912e+06	0.022392387	0	1	3	0	3	2	1	3
5	121.21229639	31.2816131176	329825.75	3.4621912e+06	0.018137192	0	1	3	0	3	2	1	3
5	121.212302872	31.2816132302	329826.38	3.4621912e+06	0.012785835	0	1	3	0	3	2	1	3
5	121.21230921	31.2816133312	329826.97	3.4621912e+06	0.008891762	0	1	3	0	3	2	1	3
5	121.212315636	31.2816134158	329827.6	3.4621912e+06	0.005281048	0	1	3	0	3	2	1	3
5	121.212322069	31.2816134955	329828.2	3.4621912e+06	0.0026557043	0	1	3	0	3	2	1	3
5	121.212328529	31.2816135651	329828.8	3.4621912e+06	0.0003131877	0	1	3	0	3	2	1	3
5	121.212334573	31.2816136397	329829.4	3.4621912e+06	-0.0004518668	0	1	3	0	3	2	1	3
5	121.212340828	31.2816137464	329830	3.4621912e+06	0.0010855043	0	1	3	0	3	2	1	3
5	121.21234704	31.2816138665	329830.6	3.4621912e+06	0.0030514498	0	1	3	0	3	2	1	3
5	121.212353067	31.2816139934	329831.16	3.4621912e+06	0.0049673277	0	1	3	0	3	2	1	3
5	121.212358387	31.281614114	329831.66	3.4621912e+06	0.0066706855	0	1	3	0	3	2	1	3
5	121.212364433	31.2816142608	329832.25	3.4621912e+06	0.008617457	0	1	3	0	3	2	1	3
5	121.212369731	31.2816143981	329832.75	3.4621912e+06	0.010331998	0	1	3	0	3	2	1	3
5	121.212375192	31.281614548	329833.25	3.4621912e+06	0.01213123	0	1	3	0	3	2	1	3
5	121.212375391	31.2816145538	329833.28	3.4621912e+06	0.012198874	0	1	3	1	3	2	1	3
0	121.212380938	31.2816146204	329833.8	3.4621912e+06	0.0074041192	0	2	3	0	3	2	1	3
0	121.212386617	31.2816146302	329834.34	3.4621912e+06	0.001071656	0	2	3	0	3	2	1	3
0	121.212391922	31.2816145654	329834.84	3.4621912e+06	-0.008478053	0	2	3	0	3	2	1	3
0	121.212397918	31.2816143993	329835.44	3.4621912e+06	-0.022265015	0	2	3	0	3	2	1	3
0	121.212403487	31.2816140954	329835.97	3.4621912e+06	-0.040933188	0	2	3	0	3	2	1	3
0	121.2124093	31.2816135778	329836.5	3.462191e+06	-0.06755837	0	2	3	0	3	2	1	3
0	121.212415235	31.2816127453	329837.06	3.462191e+06	-0.10595062	0	2	3	0	3	2	1	3
0	121.212420407	31.2816117381	329837.56	3.4621908e+06	-0.14751337	0	2	3	0	3	2	1	3
0	121.212425655	31.2816104417	329838.06	3.4621908e+06	-0.19506177	0	2	3	0	3	2	1	3
0	121.212430961	31.2816088134	329838.56	3.4621905e+06	-0.24897416	0	2	3	0	3	2	1	3
0	121.212436285	31.2816068567	329839.06	3.4621902e+06	-0.30652463	0	2	3	0	3	2	1	3
0	121.212441487	31.2816045949	329839.56	3.46219e+06	-0.36670324	0	2	3	0	3	2	1	3
0	121.212446436	31.2816021031	329840.03	3.4621898e+06	-0.42659327	0	2	3	0	3	2	1	3
0	121.212451269	31.2815992789	329840.47	3.4621895e+06	-0.4898632	0	2	3	0	3	2	1	3
0	121.21245582	31.2815961871	329840.9	3.462189e+06	-0.5556178	0	2	3	0	3	2	1	3
0	121.212460058	31.2815928229	329841.3	3.4621888e+06	-0.6247014	0	2	3	0	3	2	1	3
0	121.212463803	31.281589401	329841.66	3.4621882e+06	-0.6910731	0	2	3	0	3	2	1	3
0	121.212467329	31.2815857123	329841.97	3.462188e+06	-0.7585422	0	2	3	0	3	2	1	3
0	121.2124705	31.2815819015	329842.28	3.4621875e+06	-0.82475907	0	2	3	0	3	2	1	3
0	121.212473484	31.2815777435	329842.56	3.462187e+06	-0.89396137	0	2	3	0	3	2	1	3
0	121.212476054	31.281573496	329842.78	3.4621865e+06	-0.96331406	0	2	3	0	3	2	1	3
0	121.212478293	31.2815689954	329843	3.462186e+06	-1.0359434	0	2	3	0	3	2	1	3
0	121.212480098	31.2815644701	329843.16	3.4621855e+06	-1.1075964	0	2	3	0	3	2	1	3
0	121.212481516	31.2815597877	329843.28	3.462185e+06	-1.1811658	0	2	3	0	3	2	1	3
0	121.212482504	31.2815550467	329843.38	3.4621845e+06	-1.2556746	0	2	3	0	3	2	1	3
0	121.212482998	31.2815501332	329843.4	3.462184e+06	-1.3371675	0	2	3	0	3	2	1	3
0	121.21248296	31.2815455591	329843.4	3.4621835e+06	-1.417305	0	2	3	0	3	2	1	3
0	121.21248243	31.2815405787	329843.34	3.462183e+06	-1.5047908	0	2	3	0	3	2	1	3
0	121.212481481	31.2815357011	329843.25	3.4621822e+06	-1.5868853	0	2	3	0	3	2	1	3
6	121.212480691	31.2815324765	329843.16	3.462182e+06	-1.6357403	0	2	3	2	3	2	1	3
6	121.212480594	31.2815276898	329843.12	3.4621815e+06	-1.6249288	0	0	3	0	3	2	1	3
6	121.212480584	31.2815229143	329843.12	3.462181e+06	-1.6133285	0	0	3	0	3	2	1	3
6	121.212480638	31.2815180683	329843.12	3.4621805e+06	-1.6015191	0	0	3	0	3	2	1	3
6	121.212480748	31.281513516	329843.12	3.46218e+06	-1.5904652	0	0	3	0	3	2	1	3
6	121.212480937	31.2815083695	329843.12	3.4621792e+06	-1.5783043	0	0	3	0	3	2	1	3
6	121.212481191	31.2815027988	329843.16	3.4621788e+06	-1.5669863	0	0	3	0	3	2	1	3
6	121.212481471	31.2814972484	329843.16	3.462178e+06	-1.5582956	0	0	3	0	3	2	1	3
6	121.212481722	31.28149241	329843.2	3.4621775e+06	-1.552994	0	0	3	0	3	2	1	3
6	121.212481982	31.2814871711	329843.2	3.462177e+06	-1.5498633	0	0	3	0	3	2	1	3
6	121.212482185	31.2814820428	329843.22	3.4621765e+06	-1.5508907	0	0	3	0	3	2	1	3
6	121.212482334	31.2814767516	329843.22	3.4621758e+06	-1.555186	0	0	3	0	3	2	1	3
6	121.212482442	31.2814719623	329843.22	3.4621752e+06	-1.5593088	0	0	3	0	3	2	1	3
6	121.212482549	31.2814663972	329843.22	3.4621748e+06	-1.5634922	0	0	3	0	3	2	1	3
6	121.212482641	31.2814605079	329843.22	3.462174e+06	-1.5674428	0	0	3	0	3	2	1	3
6	121.212482703	31.2814559626	329843.22	3.4621735e+06	-1.5699569	0	0	3	0	3	2	1	3
6	121.212482754	31.2814514106	329843.22	3.462173e+06	-1.5723157	0	0	3	0	3	2	1	3
6	121.212482795	31.2814468584	329843.22	3.4621725e+06	-1.5744754	0	0	3	0	3	2	1	3
6	121.212482826	31.281442208	329843.2	3.462172e+06	-1.5766588	0	0	3	0	3	2	1	3
6	121.212482846	31.281437526	329843.2	3.4621715e+06	-1.5788552	0	0	3	0	3	2	1	3
6	121.212482854	31.2814323402	329843.2	3.462171e+06	-1.5812855	0	0	3	0	3	2	1	3
6	121.212482848	31.2814275175	329843.2	3.4621702e+06	-1.5835093	0	0	3	0	3	2	1	3
6	121.212482833	31.2814227154	329843.16	3.4621698e+06	-1.585611	0	0	3	0	3	2	1	3
6	121.212482804	31.2814176603	329843.16	3.4621692e+06	-1.5877492	0	0	3	0	3	2	1	3
6	121.212482765	31.2814127086	329843.16	3.4621688e+06	-1.5898379	0	0	3	0	3	2	1	3
6	121.21248271	31.281407496	329843.12	3.4621682e+06	-1.592035	0	0	3	0	3	2	1	3
6	121.212482643	31.2814023512	329843.12	3.4621675e+06	-1.594203	0	0	3	0	3	2	1	3
6	121.212482565	31.2813973034	329843.1	3.462167e+06	-1.5963285	0	0	3	0	3	2	1	3
6	121.212482472	31.2813921205	329843.06	3.4621665e+06	-1.59851	0	0	3	0	3	2	1	3
6	121.212482357	31.2813865563	329843.06	3.4621658e+06	-1.6008497	0	0	3	0	3	2	1	3
6	121.212482233	31.2813811912	329843.03	3.4621652e+06	-1.6030327	0	0	3	0	3	2	1	3
6	121.21248212	31.281375834	329843	3.4621645e+06	-1.6039112	0	0	3	0	3	2	1	3
6	121.212482058	31.2813705852	329843	3.462164e+06	-1.6014917	0	0	3	0	3	2	1	3
6	121.212482061	31.2813653252	329843	3.4621635e+06	-1.5960455	0	0	3	0	3	2	1	3
6	121.212482128	31.2813600922	329843	3.4621628e+06	-1.5886408	0	0	3	0	3	2	1	3
6	121.212482245	31.2813548667	329843	3.4621622e+06	-1.5809015	0	0	3	0	3	2	1	3
6	121.212482385	31.2813496969	329843	3.4621618e+06	-1.5745121	0	0	3	0	3	2	1	3
6	121.212482549	31.2813443747	329843	3.4621612e+06	-1.569208	0	0	3	0	3	2	1	3
6	121.212482726	31.2813391886	329843	3.4621605e+06	-1.5649545	0	0	3	0	3	2	1	3
6	121.212482915	31.2813337884	329843	3.46216e+06	-1.5617898	0	0	3	0	3	2	1	3
6	121.212483103	31.2813285378	329843.03	3.4621592e+06	-1.5596969	0	0	3	0	3	2	1	3
6	121.212483282	31.2813231606	329843.03	3.4621588e+06	-1.5591184	0	0	3	0	3	2	1	3
6	121.212483433	31.281317741	329843.03	3.4621582e+06	-1.560496	0	0	3	0	3	2	1	3
6	121.21248357	31.2813122354	329843.03	3.4621575e+06	-1.5624565	0	0	3	0	3	2	1	3
6	121.212483702	31.2813065794	329843.03	3.462157e+06	-1.5642291	0	0	3	0	3	2	1	3
6	121.212483846	31.2813011248	329843.03	3.4621562e+06	-1.5643008	0	0	3	0	3	2	1	3
6	121.212484029	31.2812958553	329843.06	3.4621558e+06	-1.5617694	0	0	3	0	3	2	1	3
6	121.212484251	31.2812904917	329843.06	3.4621552e+06	-1.5578374	0	0	3	0	3	2	1	3
6	121.212484499	31.2812850298	329843.06	3.4621545e+06	-1.5539814	0	0	3	0	3	2	1	3
6	121.212484758	31.2812797277	329843.1	3.462154e+06	-1.5505658	0	0	3	0	3	2	1	3
6	121.212485037	31.2812743035	329843.1	3.4621532e+06	-1.5475588	0	0	3	0	3	2	1	3
6	121.212485325	31.2812688731	329843.12	3.4621528e+06	-1.5451677	0	0	3	0	3	2	1	3
6	121.212485605	31.2812634451	329843.16	3.4621522e+06	-1.5440826	0	0	3	0	3	2	1	3
6	121.212485866	31.2812577483	329843.16	3.4621515e+06	-1.545353	0	0	3	0	3	2	1	3
6	121.212486085	31.2812521743	329843.16	3.462151e+06	-1.5483915	0	0	3	0	3	2	1	3
6	121.212486276	31.2812464693	329843.2	3.4621502e+06	-1.5522819	0	0	3	0	3	2	1	3
6	121.212486419	31.2812412586	329843.2	3.4621498e+06	-1.5563134	0	0	3	0	3	2	1	3
6	121.212486534	31.2812362135	329843.2	3.4621492e+06	-1.5603099	0	0	3	0	3	2	1	3
6	121.212486621	31.2812315701	329843.2	3.4621485e+06	-1.5638098	0	0	3	0	3	2	1	3
6	121.212486709	31.2812257712	329843.2	3.462148e+06	-1.5677654	0	0	3	0	3	2	1	3
6	121.212486767	31.2812203453	329843.2	3.4621472e+06	-1.5714475	0	0	3	0	3	2	1	3
6	121.2124868	31.2812153522	329843.16	3.4621468e+06	-1.5748469	0	0	3	0	3	2	1	3
6	121.212486812	31.2812105403	329843.16	3.4621462e+06	-1.5781299	0	0	3	0	3	2	1	3
6	121.212486807	31.281205857	329843.16	3.4621458e+06	-1.5812769	0	0	3	0	3	2	1	3
6	121.212486789	31.2812012376	329843.16	3.4621452e+06	-1.5841277	0	0	3	0	3	2	1	3
6	121.212486751	31.281195896	329843.12	3.4621448e+06	-1.5873357	0	0	3	0	3	2	1	3
6	121.212486706	31.2811912546	329843.12	3.4621442e+06	-1.5898876	0	0	3	0	3	2	1	3
6	121.21248672	31.2811862845	329843.12	3.4621435e+06	-1.5879302	0	0	3	0	3	2	1	3
6	121.212486796	31.2811814911	329843.1	3.462143e+06	-1.5830383	0	1	3	0	3	2	1	3
6	121.212486909	31.281176745	329843.12	3.4621425e+06	-1.5780686	0	1	3	0	3	2	1	3
6	121.212487052	31.281171998	329843.12	3.462142e+06	-1.5726624	0	1	3	0	3	2	1	3
6	121.212487226	31.2811672369	329843.12	3.4621415e+06	-1.5670877	0	1	3	0	3	2	1	3
6	121.21248747	31.2811618549	329843.12	3.462141e+06	-1.5602233	0	1	3	0	3	2	1	3
6	121.212487732	31.2811568878	329843.16	3.4621402e+06	-1.5539	0	1	3	0	3	2	1	3
6	121.212488044	31.2811516573	329843.2	3.4621398e+06	-1.5474967	0	1	3	0	3	2	1	3
6	121.212488322	31.2811471237	329843.2	3.4621392e+06	-1.5433002	0	1	3	0	3	2	1	3
6	121.212488593	31.2811426128	329843.22	3.4621388e+06	-1.5407789	0	1	3	0	3	2	1	3
6	121.212488841	31.2811378198	329843.22	3.4621382e+06	-1.5412613	0	1	3	0	3	2	1	3
6	121.21248904	31.2811326813	329843.25	3.4621378e+06	-1.5456198	0	1	3	0	3	2	1	3
6	121.212489218	31.2811274027	329843.25	3.462137e+06	-1.550112	0	1	3	0	3	2	1	3
6	121.212489395	31.2811220664	329843.25	3.4621365e+06	-1.5531375	0	1	3	0	3	2	1	3
6	121.212489566	31.2811165824	329843.25	3.4621358e+06	-1.5557499	0	1	3	0	3	2	1	3
6	121.212489737	31.2811108292	329843.25	3.4621352e+06	-1.5579157	0	1	3	0	3	2	1	3
6	121.212489892	31.2811052457	329843.28	3.4621345e+06	-1.5598924	0	1	3	0	3	2	1	3
6	121.212490036	31.2810996454	329843.28	3.462134e+06	-1.5617609	0	1	3	0	3	2	1	3
6	121.212490175	31.2810937337	329843.28	3.4621332e+06	-1.5637169	0	1	3	0	3	2	1	3
6	121.212490301	31.2810878254	329843.28	3.4621328e+06	-1.56567	0	1	3	0	3	2	1	3
6	121.212490389	31.2810832971	329843.28	3.4621322e+06	-1.5671662	0	1	3	0	3	2	1	3
6	121.212490469	31.2810787057	329843.28	3.4621318e+06	-1.568682	0	1	3	0	3	2	1	3
6	121.212490544	31.2810739792	329843.28	3.4621312e+06	-1.5702412	0	1	3	0	3	2	1	3
6	121.212490608	31.2810693573	329843.28	3.4621308e+06	-1.571765	0	1	3	0	3	2	1	3
6	121.212490665	31.2810647117	329843.28	3.46213e+06	-1.5732965	0	1	3	0	3	2	1	3
6	121.212490716	31.2810597747	329843.25	3.4621295e+06	-1.5749233	0	1	3	0	3	2	1	3
6	121.212490757	31.2810549328	329843.25	3.462129e+06	-1.5765176	0	1	3	0	3	2	1	3
6	121.21249078514774	31.2810507559487	329843.25	3.4621285e+06	-1.5780324	0	1	3	0	3	2	1	3
\.


--
-- Data for Name: result_path_points_meta; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.result_path_points_meta (id, p_id, lon, lat, utmx, utmy, heading, curv, mode, speed_mode, opposite_side_mode, event_mode, lane_num, lane_seq, lane_width, point) FROM stdin;
1	9	121.21142600380644	31.281696858059238	329743	3.4622018e+06	-3.1226077	0	0	3	3	0	2	1	3	0101000020E6100000D9F7EE00884D5E4019C008491D483F40
2	9	121.211425487	31.2816968442	329743	3.4622018e+06	-3.1226077	0	0	3	3	0	2	1	3	0101000020E61000002D0DC4FE874D5E40B439CD481D483F40
3	9	121.211419781	31.2816967097	329742.44	3.4622018e+06	-3.1252196	0	0	3	3	0	2	1	3	0101000020E6100000D747D5E6874D5E40648D8B461D483F40
4	9	121.211413367	31.2816965733	329741.84	3.4622018e+06	-3.1281462	0	0	3	3	0	2	1	3	0101000020E6100000F24CEECB874D5E4001B841441D483F40
5	9	121.211406611	31.2816964414	329741.2	3.4622018e+06	-3.130905	0	0	3	3	0	2	1	3	0101000020E6100000CE1998AF874D5E406C360B421D483F40
6	9	121.211401093	31.2816963306	329740.66	3.4622018e+06	-3.1321864	0	0	3	3	0	2	1	3	0101000020E610000084317398874D5E4088542F401D483F40
7	9	121.21139581	31.2816962148	329740.16	3.4622018e+06	-3.13234	0	0	3	3	0	2	1	3	0101000020E6100000889D4A82874D5E4016F93D3E1D483F40
8	9	121.211389653	31.2816960582	329739.6	3.4622018e+06	-3.1308823	0	0	3	3	0	2	1	3	0101000020E610000042967768874D5E4091619D3B1D483F40
9	9	121.211383305	31.2816958724	329738.97	3.4622018e+06	-3.1280758	0	0	3	3	0	2	1	3	0101000020E61000004E79D74D874D5E404E607F381D483F40
10	9	121.211376943	31.2816956662	329738.38	3.4622018e+06	-3.1247916	0	0	3	3	0	2	1	3	0101000020E610000010542833874D5E4002C109351D483F40
11	9	121.211369816	31.2816954085	329737.7	3.4622018e+06	-3.1206653	0	0	3	3	0	2	1	3	0101000020E610000038C54315874D5E40DCF0B6301D483F40
12	9	121.211362364	31.2816951164	329736.97	3.4622018e+06	-3.1164746	0	0	3	3	0	2	1	3	0101000020E61000000E3F02F6864D5E408461D02B1D483F40
13	9	121.211354504	31.2816947963	329736.22	3.4622018e+06	-3.1131203	0	0	3	3	0	2	1	3	0101000020E6100000B5A20AD5864D5E40D78F71261D483F40
14	9	121.211346497	31.2816944586	329735.47	3.4622018e+06	-3.1106133	0	0	3	3	0	2	1	3	0101000020E61000004E2F75B3864D5E40C426C7201D483F40
15	9	121.211338491	31.2816941201	329734.72	3.4622018e+06	-3.109222	0	0	3	3	0	2	1	3	0101000020E6100000C9CEE091864D5E40144E191B1D483F40
16	9	121.211330036	31.2816937766	329733.9	3.4622018e+06	-3.1094742	0	0	3	3	0	2	1	3	0101000020E610000015526A6E864D5E40D6FB55151D483F40
17	9	121.211321228	31.2816934425	329733.06	3.4622015e+06	-3.111451	0	0	3	3	0	2	1	3	0101000020E61000007ACD7849864D5E400009BB0F1D483F40
18	9	121.21131221	31.281693126	329732.2	3.4622015e+06	-3.114539	0	0	3	3	0	2	1	3	0101000020E610000083CCA523864D5E4092AD6B0A1D483F40
19	9	121.211303108	31.281692838	329731.34	3.4622015e+06	-3.118327	0	0	3	3	0	2	1	3	0101000020E6100000CD9978FD854D5E4039BA96051D483F40
20	9	121.211293574	31.2816925717	329730.44	3.4622015e+06	-3.122734	0	0	3	3	0	2	1	3	0101000020E6100000D68B7BD5854D5E4046FA1E011D483F40
21	9	121.211283251	31.2816923232	329729.44	3.4622015e+06	-3.1276448	0	0	3	3	0	2	1	3	0101000020E6100000344F2FAA854D5E40A3ADF3FC1C483F40
22	9	121.211273593	31.2816921291	329728.53	3.4622015e+06	-3.1322575	0	0	3	3	0	2	1	3	0101000020E6100000611CAD81854D5E406E06B2F91C483F40
23	9	121.211268002	31.2816920337	329728	3.4622015e+06	-3.1349266	0	0	3	3	0	2	1	3	0101000020E610000000D2396A854D5E40054918F81C483F40
24	9	121.21125782	31.2816918912	329727.03	3.4622015e+06	-3.1397812	0	0	3	3	0	2	1	3	0101000020E610000027FB843F854D5E409D40B4F51C483F40
25	9	121.211252554	31.2816918328	329726.53	3.4622015e+06	3.1407921	0	0	3	3	0	2	1	3	0101000020E610000018A86E29854D5E40226DB9F41C483F40
26	9	121.211243092	31.2816917524	329725.62	3.4622015e+06	3.1359515	0	0	3	3	0	2	1	3	0101000020E610000057E9BE01854D5E40661C60F31C483F40
27	9	121.211234638	31.2816917096	329724.8	3.4622015e+06	3.131715	0	0	3	3	0	2	1	3	0101000020E6100000837F49DE844D5E404D49A8F21C483F40
28	9	121.211226084	31.2816916972	329724	3.4622015e+06	3.1275837	0	0	3	3	0	2	1	3	0101000020E6100000E6B568BA844D5E405B0773F21C483F40
29	9	121.211219303	31.281691712	329723.34	3.4622015e+06	3.1243892	0	0	3	3	0	2	1	3	0101000020E6100000CFAAF79D844D5E402198B2F21C483F40
30	9	121.211212928	31.2816917451	329722.75	3.4622015e+06	3.1214857	0	0	3	3	0	2	1	3	0101000020E610000027903A83844D5E40F7C140F31C483F40
31	9	121.211207537	31.2816917879	329722.22	3.4622015e+06	3.1191063	0	0	3	3	0	2	1	3	0101000020E61000005C059E6C844D5E401095F8F31C483F40
32	9	121.211200975	31.2816918583	329721.6	3.4622018e+06	3.1162453	0	0	3	3	0	2	1	3	0101000020E610000088201851844D5E40AEF226F51C483F40
33	9	121.211194778	31.2816919435	329721.03	3.4622018e+06	3.113541	0	0	3	3	0	2	1	3	0101000020E610000025261A37844D5E4012E194F61C483F40
34	9	121.211189369	31.2816920277	329720.5	3.4622018e+06	3.1140945	0	1	3	3	0	2	1	3	0101000020E61000008B476A20844D5E40F383FEF71C483F40
35	9	121.211183472	31.2816920077	329719.94	3.4622018e+06	3.1191974	0	1	3	3	0	2	1	3	0101000020E6100000876CAE07844D5E40B79DA8F71C483F40
36	9	121.211177683	31.2816919558	329719.4	3.4622018e+06	3.1248457	0	1	3	3	0	2	1	3	0101000020E6100000538866EF834D5E4010B5C9F61C483F40
37	9	121.211172104	31.2816918899	329718.88	3.4622018e+06	3.1296759	0	1	3	3	0	2	1	3	0101000020E61000007B2000D8834D5E403FABAEF51C483F40
38	9	121.211165771	31.281691801	329718.25	3.4622018e+06	3.1342595	0	1	3	3	0	2	1	3	0101000020E6100000B21E70BD834D5E40AAD830F41C483F40
39	9	121.211158566	31.2816916763	329717.56	3.4622018e+06	3.1391034	0	1	3	3	0	2	1	3	0101000020E610000060CF379F834D5E40904319F21C483F40
40	9	121.211153289	31.2816915679	329717.06	3.4622018e+06	-3.1406271	0	1	3	3	0	2	1	3	0101000020E6100000A9AC1589834D5E4081B047F01C483F40
41	9	121.211147608	31.2816914357	329716.53	3.4622018e+06	-3.1369886	0	1	3	3	0	2	1	3	0101000020E610000046BF4171834D5E4011E50FEE1C483F40
42	9	121.211141464	31.2816912754	329715.94	3.4622018e+06	-3.133195	0	1	3	3	0	2	1	3	0101000020E610000069AD7C57834D5E405A695FEB1C483F40
43	9	121.211135864	31.2816911133	329715.4	3.4622018e+06	-3.129824	0	1	3	3	0	2	1	3	0101000020E610000022B9FF3F834D5E408432A7E81C483F40
44	9	121.211130273	31.2816909387	329714.88	3.4622018e+06	-3.1266806	0	1	3	3	0	2	1	3	0101000020E6100000C16E8C28834D5E40C94BB9E51C483F40
45	9	121.211124791	31.2816907554	329714.34	3.4622015e+06	-3.123824	0	1	3	3	0	2	1	3	0101000020E6100000122E8E11834D5E404E07A6E21C483F40
46	9	121.2111173	31.2816904872	329713.62	3.4622015e+06	-3.1202593	0	1	3	3	0	2	1	3	0101000020E6100000ABC722F2824D5E40491E26DE1C483F40
47	9	121.21111062	31.2816902343	329713	3.4622015e+06	-3.1175334	0	1	3	3	0	2	1	3	0101000020E6100000402F1ED6824D5E40CCEBE7D91C483F40
48	9	121.211104532	31.2816899945	329712.44	3.4622015e+06	-3.1154032	0	1	3	3	0	2	1	3	0101000020E61000008D3E95BC824D5E40E8FCE1D51C483F40
49	9	121.211098786	31.2816897633	329711.88	3.4622015e+06	-3.1137948	0	1	3	3	0	2	1	3	0101000020E610000019867BA4824D5E40D2FD00D21C483F40
50	9	121.211093415	31.2816895453	329711.38	3.4622015e+06	-3.1126947	0	1	3	3	0	2	1	3	0101000020E6100000DC74F48D824D5E4049B058CE1C483F40
51	9	121.211087155	31.2816892854	329710.78	3.4622015e+06	-3.1114817	0	1	3	3	0	2	1	3	0101000020E61000002AD5B273824D5E40366DFCC91C483F40
52	9	121.211081472	31.2816890436	329710.22	3.4622015e+06	-3.1103754	0	1	3	3	0	2	1	3	0101000020E610000005C2DC5B824D5E404DE7EDC51C483F40
53	9	121.211075153	31.2816887683	329709.62	3.4622015e+06	-3.1091383	0	1	3	3	0	2	1	3	0101000020E610000086C85B41824D5E40C07F4FC11C483F40
54	9	121.211069787	31.2816885312	329709.12	3.4622015e+06	-3.1082077	0	1	3	3	0	2	1	3	0101000020E6100000AD15DA2A824D5E408B2955BD1C483F40
56	0	121.211064031	31.2816883135	329708.56	3.4622015e+06	-3.1107976	0	2	3	3	0	2	1	3	0101000020E610000072A0B512824D5E40DC25AEB91C483F40
57	0	121.211057839	31.2816880891	329707.97	3.4622015e+06	-3.11245	0	2	3	3	0	2	1	3	0101000020E61000007204BDF8814D5E40745BEAB51C483F40
58	0	121.211051633	31.2816878255	329707.38	3.4622015e+06	-3.1109593	0	2	3	3	0	2	1	3	0101000020E61000002760B5DE814D5E4030347EB11C483F40
59	0	121.211045708	31.2816875307	329706.8	3.4622015e+06	-3.1070821	0	2	3	3	0	2	1	3	0101000020E61000008E74DBC5814D5E40290C8CAC1C483F40
60	0	121.211038898	31.2816871553	329706.16	3.4622012e+06	-3.101758	0	2	3	3	0	2	1	3	0101000020E610000002464BA9814D5E407EB73FA61C483F40
61	0	121.211032458	31.2816867616	329705.56	3.4622012e+06	-3.0960276	0	2	3	3	0	2	1	3	0101000020E61000004A60488E814D5E40C4C9A49F1C483F40
62	0	121.211026723	31.2816863498	329705	3.4622012e+06	-3.0886245	0	2	3	3	0	2	1	3	0101000020E61000007E773A76814D5E40E11EBC981C483F40
63	0	121.211021316	31.2816858855	329704.5	3.4622012e+06	-3.0787148	0	2	3	3	0	2	1	3	0101000020E6100000A6BE8C5F814D5E40A1F7F1901C483F40
64	0	121.211015554	31.2816853051	329703.94	3.4622012e+06	-3.0658374	0	2	3	3	0	2	1	3	0101000020E610000026D86147814D5E40152B35871C483F40
65	0	121.211009736	31.2816845936	329703.38	3.462201e+06	-3.0488122	0	2	3	3	0	2	1	3	0101000020E61000007CD0FA2E814D5E408F4C457B1C483F40
66	0	121.21100382	31.2816836935	329702.8	3.462201e+06	-3.0257475	0	2	3	3	0	2	1	3	0101000020E6100000CA8E2A16814D5E4024662B6C1C483F40
67	0	121.210998434	31.2816827511	329702.3	3.462201e+06	-3.003636	0	2	3	3	0	2	1	3	0101000020E6100000626293FF804D5E4062D25B5C1C483F40
68	0	121.210992331	31.2816815725	329701.72	3.4622008e+06	-2.9796224	0	2	3	3	0	2	1	3	0101000020E61000008456FAE5804D5E40FAC595481C483F40
69	0	121.210986565	31.2816803497	329701.2	3.4622008e+06	-2.9575903	0	2	3	3	0	2	1	3	0101000020E61000008124CBCD804D5E4029E311341C483F40
70	0	121.210980211	31.2816789075	329700.56	3.4622005e+06	-2.9357069	0	2	3	3	0	2	1	3	0101000020E6100000499624B3804D5E407EAFDF1B1C483F40
71	0	121.210974755	31.2816775822	329700.03	3.4622005e+06	-2.9177961	0	2	3	3	0	2	1	3	0101000020E61000006C40429C804D5E40BB90A3051C483F40
72	0	121.21096897	31.2816760791	329699.5	3.4622002e+06	-2.8987474	0	2	3	3	0	2	1	3	0101000020E6100000BBA7FE83804D5E40CDCC6BEC1B483F40
73	0	121.210962888	31.2816743806	329698.9	3.4622e+06	-2.8781264	0	2	3	3	0	2	1	3	0101000020E61000004D287C6A804D5E404DCCECCF1B483F40
74	0	121.210956664	31.2816725103	329698.3	3.4621998e+06	-2.8563116	0	2	3	3	0	2	1	3	0101000020E610000035306150804D5E40B5EB8BB01B483F40
75	0	121.210950297	31.2816704379	329697.72	3.4621998e+06	-2.8323534	0	2	3	3	0	2	1	3	0101000020E610000093ACAC35804D5E40CF07C78D1B483F40
76	0	121.210943734	31.2816681318	329697.06	3.4621995e+06	-2.8066232	0	2	3	3	0	2	1	3	0101000020E6100000DFB4251A804D5E400B6816671B483F40
77	0	121.210938473	31.2816661612	329696.56	3.4621992e+06	-2.7858322	0	2	3	3	0	2	1	3	0101000020E610000034C01404804D5E406EBE06461B483F40
78	0	121.210932855	31.2816639326	329696.03	3.462199e+06	-2.76345	0	2	3	3	0	2	1	3	0101000020E61000001F7884EC7F4D5E40D1FAA2201B483F40
79	0	121.210927496	31.2816616837	329695.53	3.4621988e+06	-2.7419076	0	2	3	3	0	2	1	3	0101000020E61000006B490AD67F4D5E401E07E8FA1A483F40
80	0	121.210922241	31.2816593557	329695	3.4621985e+06	-2.7204247	0	2	3	3	0	2	1	3	0101000020E610000005C6FFBF7F4D5E400C58D9D31A483F40
81	0	121.210916762	31.2816567864	329694.5	3.4621982e+06	-2.6970954	0	2	3	3	0	2	1	3	0101000020E6100000F7BD04A97F4D5E40D248BEA81A483F40
82	0	121.21091171	31.2816542831	329694	3.462198e+06	-2.6749184	0	2	3	3	0	2	1	3	0101000020E6100000C832D4937F4D5E405DB1BE7E1A483F40
83	0	121.210906465	31.281651527	329693.5	3.4621975e+06	-2.6504285	0	2	3	3	0	2	1	3	0101000020E6100000286CD47D7F4D5E405E5581501A483F40
84	0	121.210901292	31.2816486314	329693	3.4621972e+06	-2.6242976	0	2	3	3	0	2	1	3	0101000020E6100000BFF421687F4D5E407FD3EC1F1A483F40
85	0	121.210895613	31.2816452311	329692.44	3.462197e+06	-2.593536	0	2	3	3	0	2	1	3	0101000020E61000001D2D50507F4D5E401CA6E0E619483F40
86	0	121.2108904	31.2816418858	329691.94	3.4621965e+06	-2.563449	0	2	3	3	0	2	1	3	0101000020E610000095C2723A7F4D5E40DCB1C0AE19483F40
87	0	121.210885686	31.281638636	329691.5	3.4621962e+06	-2.5334928	0	2	3	3	0	2	1	3	0101000020E61000002124AD267F4D5E40F9E83A7819483F40
88	0	121.210880928	31.2816351182	329691.03	3.4621958e+06	-2.5008593	0	2	3	3	0	2	1	3	0101000020E61000000C47B8127F4D5E40F812363D19483F40
89	0	121.210876338	31.2816314461	329690.6	3.4621955e+06	-2.4658194	0	2	3	3	0	2	1	3	0101000020E610000075CD77FF7E4D5E4052869AFF18483F40
90	0	121.210871899	31.2816275734	329690.16	3.462195e+06	-2.4273903	0	2	3	3	0	2	1	3	0101000020E61000006D76D9EC7E4D5E40A467A1BE18483F40
91	0	121.210867769	31.2816236035	329689.75	3.4621945e+06	-2.3859692	0	2	3	3	0	2	1	3	0101000020E6100000ACE886DB7E4D5E406ED0067C18483F40
92	0	121.210863956	31.2816195483	329689.4	3.4621942e+06	-2.3422177	0	2	3	3	0	2	1	3	0101000020E610000036BB88CB7E4D5E40E0DCFD3718483F40
93	0	121.210860582	31.2816155492	329689.06	3.4621938e+06	-2.297404	0	2	3	3	0	2	1	3	0101000020E610000027ED61BD7E4D5E40EDDBE5F417483F40
94	0	121.210857178	31.281610987	329688.72	3.4621932e+06	-2.2443402	0	2	3	3	0	2	1	3	0101000020E6100000C2E81AAF7E4D5E40FA5B5BA817483F40
95	0	121.210854265	31.2816065112	329688.44	3.4621928e+06	-2.1911104	0	2	3	3	0	2	1	3	0101000020E61000006A19E3A27E4D5E40D5F1435D17483F40
96	0	121.21085175	31.281602049	329688.2	3.4621922e+06	-2.13734	0	2	3	3	0	2	1	3	0101000020E61000007BA356987E4D5E400CF1661217483F40
97	0	121.210849643	31.2815976857	329688	3.4621918e+06	-2.0844114	0	2	3	3	0	2	1	3	0101000020E6100000BB43808F7E4D5E40F6B532C916483F40
98	0	121.210847754	31.2815930673	329687.8	3.4621912e+06	-2.029026	0	2	3	3	0	2	1	3	0101000020E61000005DF793877E4D5E4076D5B67B16483F40
99	0	121.21084615	31.2815883067	329687.66	3.4621908e+06	-1.9729096	0	2	3	3	0	2	1	3	0101000020E610000033AFD9807E4D5E406836D82B16483F40
100	0	121.210844873	31.2815835258	329687.5	3.4621902e+06	-1.9173506	0	2	3	3	0	2	1	3	0101000020E61000001D847E7B7E4D5E404367A2DB15483F40
101	0	121.21084388	31.2815785254	329687.4	3.4621895e+06	-1.8598708	0	2	3	3	0	2	1	3	0101000020E61000005A4A54777E4D5E4052D9BD8715483F40
102	0	121.210843215	31.2815734112	329687.34	3.462189e+06	-1.8015445	0	2	3	3	0	2	1	3	0101000020E61000008B408A747E4D5E40F486F03115483F40
103	0	121.2108429	31.2815685805	329687.28	3.4621885e+06	-1.747241	0	2	3	3	0	2	1	3	0101000020E6100000010638737E4D5E4022D4E4E014483F40
104	0	121.210842852	31.2815635796	329687.28	3.462188e+06	-1.6937078	0	2	3	3	0	2	1	3	0101000020E6100000DD7B04737E4D5E406F20FE8C14483F40
105	0	121.210843027	31.2815588748	329687.28	3.4621875e+06	-1.6471614	0	2	3	3	0	2	1	3	0101000020E61000007F63C0737E4D5E40212A0F3E14483F40
106	0	121.210843361	31.2815539628	329687.3	3.462187e+06	-1.6057783	0	2	3	3	0	2	1	3	0101000020E6100000B80427757E4D5E400349A6EB13483F40
107	0	121.210843737	31.2815491924	329687.34	3.4621862e+06	-1.576035	0	2	3	3	0	2	1	3	0101000020E6100000D0BEBA767E4D5E40BE929D9B13483F40
108	0	121.210844036	31.2815442294	329687.38	3.4621858e+06	-1.5611387	0	2	3	3	0	2	1	3	0101000020E61000004ECBFB777E4D5E4089A6594813483F40
109	0	121.210844118	31.2815392565	329687.34	3.4621852e+06	-1.5647253	0	2	3	3	0	2	1	3	0101000020E61000004BD753787E4D5E402935EBF412483F40
110	0	121.210844098	31.2815344245	329687.34	3.4621848e+06	-1.5737535	0	2	3	3	0	2	1	3	0101000020E6100000BC5D3E787E4D5E40F9ECD9A312483F40
111	0	121.210843978	31.2815294734	329687.34	3.4621842e+06	-1.5855792	0	2	3	3	0	2	1	3	0101000020E61000006384BD777E4D5E40F41CC95012483F40
112	0	121.210843813	31.2815247516	329687.3	3.4621838e+06	-1.5962223	0	2	3	3	0	2	1	3	0101000020E610000088590C777E4D5E40F322910112483F40
113	0	121.21084361	31.2815195792	329687.28	3.462183e+06	-1.6050924	0	2	3	3	0	2	1	3	0101000020E6100000516132767E4D5E4002D9C9AA11483F40
114	0	121.210843404	31.2815143241	329687.25	3.4621825e+06	-1.6108414	0	2	3	3	0	2	1	3	0101000020E6100000783055757E4D5E40745D9F5211483F40
115	0	121.210843219	31.2815098138	329687.22	3.462182e+06	-1.6144563	0	2	3	3	0	2	1	3	0101000020E61000000E8C8E747E4D5E4028C6F30611483F40
116	0	121.210842994	31.2815044534	329687.2	3.4621815e+06	-1.617587	0	2	3	3	0	2	1	3	0101000020E610000087F49C737E4D5E40070805AD10483F40
117	0	121.210842781	31.2814995181	329687.16	3.4621808e+06	-1.6198026	0	2	3	3	0	2	1	3	0101000020E6100000883FB8727E4D5E404A14385A10483F40
118	0	121.210842578	31.2814942292	329687.12	3.4621802e+06	-1.6199273	0	2	3	3	0	2	1	3	0101000020E61000005147DE717E4D5E403E6D7C0110483F40
120	14	121.210842294	31.2814852286	329687.1	3.4621792e+06	-1.6160516	0	0	3	3	0	2	1	3	0101000020E6100000FE55AD707E4D5E40E3247B6A0F483F40
121	14	121.210842194	31.2814801613	329687.06	3.4621788e+06	-1.6118718	0	0	3	3	0	2	1	3	0101000020E610000033F641707E4D5E409D4177150F483F40
122	14	121.210842128	31.2814754671	329687.06	3.4621782e+06	-1.6076921	0	0	3	3	0	2	1	3	0101000020E61000004218FB6F7E4D5E4022D2B5C60E483F40
123	14	121.210842075	31.2814694552	329687.03	3.4621775e+06	-1.6022724	0	0	3	3	0	2	1	3	0101000020E6100000BB2FC26F7E4D5E402DE8D8610E483F40
124	14	121.210842059	31.281462511	329687.03	3.4621768e+06	-1.5960248	0	0	3	3	0	2	1	3	0101000020E6100000AF01B16F7E4D5E4088CB57ED0D483F40
125	14	121.210842072	31.2814572865	329687.03	3.4621762e+06	-1.5916526	0	0	3	3	0	2	1	3	0101000020E610000019F7BE6F7E4D5E4008BDB0950D483F40
126	14	121.210842108	31.2814515539	329687	3.4621755e+06	-1.5873315	0	0	3	3	0	2	1	3	0101000020E6100000B39EE56F7E4D5E40AC6883350D483F40
127	14	121.210842166	31.28144612	329687	3.462175e+06	-1.5833724	0	0	3	3	0	2	1	3	0101000020E61000009FE523707E4D5E4070FC58DA0C483F40
128	14	121.210842264	31.2814397173	329687	3.4621742e+06	-1.5787416	0	0	3	3	0	2	1	3	0101000020E6100000A71F8D707E4D5E405799ED6E0C483F40
129	14	121.210842391	31.2814334968	329687	3.4621735e+06	-1.5742636	0	0	3	3	0	2	1	3	0101000020E6100000267D15717E4D5E4042C190060C483F40
130	14	121.210842601	31.2814254414	329687	3.4621728e+06	-1.5684892	0	0	3	3	0	2	1	3	0101000020E610000082F9F6717E4D5E404B136B7F0B483F40
131	14	121.21084281	31.2814188217	329687.03	3.462172e+06	-1.5638059	0	0	3	3	0	2	1	3	0101000020E6100000FE62D7727E4D5E402CAE5B100B483F40
132	14	121.210842975	31.2814141721	329687.03	3.4621715e+06	-1.5607132	0	0	3	3	0	2	1	3	0101000020E6100000D98D88737E4D5E40E8CC59C20A483F40
133	14	121.210843284	31.2814061948	329687.03	3.4621705e+06	-1.5559746	0	0	3	3	0	2	1	3	0101000020E61000001F57D4747E4D5E40CC8E833C0A483F40
134	14	121.210843666	31.2813971618	329687.06	3.4621695e+06	-1.5517548	0	0	3	3	0	2	1	3	0101000020E61000007B826E767E4D5E40441EF7A409483F40
135	14	121.210843879	31.2813924077	329687.06	3.462169e+06	-1.5499494	0	0	3	3	0	2	1	3	0101000020E6100000793753777E4D5E40096A345509483F40
136	14	121.210844275	31.2813839129	329687.1	3.462168e+06	-1.5473782	0	0	3	3	0	2	1	3	0101000020E6100000206BFC787E4D5E40A986AFC608483F40
137	14	121.210844514	31.2813789369	329687.1	3.4621675e+06	-1.54621	0	0	3	3	0	2	1	3	0101000020E6100000F20AFD797E4D5E40CDC4337308483F40
138	14	121.210844735	31.2813743973	329687.12	3.462167e+06	-1.545385	0	0	3	3	0	2	1	3	0101000020E6100000F656EA7A7E4D5E40D0550A2708483F40
139	14	121.21084519	31.2813650655	329687.16	3.462166e+06	-1.5445999	0	0	3	3	0	2	1	3	0101000020E610000069E4D27C7E4D5E40358F7A8A07483F40
140	14	121.210845619	31.2813558855	329687.2	3.462165e+06	-1.5454628	0	0	3	3	0	2	1	3	0101000020E610000008879F7E7E4D5E4076C276F006483F40
141	14	121.210845842	31.281350848	329687.2	3.4621642e+06	-1.5467097	0	0	3	3	0	2	1	3	0101000020E6100000CEF88E7F7E4D5E40A3DCF29B06483F40
142	14	121.210846193	31.281342231	329687.2	3.4621635e+06	-1.5500042	0	0	3	3	0	2	1	3	0101000020E6100000F3DA07817E4D5E40F120610B06483F40
143	14	121.210846514	31.2813333831	329687.22	3.4621625e+06	-1.5542372	0	0	3	3	0	2	1	3	0101000020E6100000C28660827E4D5E4003B0EF7605483F40
144	14	121.210846782	31.2813247137	329687.22	3.4621615e+06	-1.5589327	0	0	3	3	0	2	1	3	0101000020E61000000A4A80837E4D5E40E7E57CE504483F40
145	14	121.210846972	31.2813167875	329687.22	3.4621605e+06	-1.5640424	0	0	3	3	0	2	1	3	0101000020E6100000D74C4C847E4D5E40D720826004483F40
146	14	121.210847095	31.2813093532	329687.22	3.4621598e+06	-1.5696049	0	0	3	3	0	2	1	3	0101000020E6100000D35ED0847E4D5E408C0DC8E303483F40
147	14	121.210847167	31.2813018507	329687.22	3.462159e+06	-1.5755782	0	0	3	3	0	2	1	3	0101000020E610000008AE1D857E4D5E408F0FE96503483F40
148	14	121.210847186	31.2812951943	329687.22	3.4621582e+06	-1.5808733	0	0	3	3	0	2	1	3	0101000020E6100000B61432857E4D5E405C0A3CF602483F40
149	14	121.210847171	31.2812885783	329687.2	3.4621575e+06	-1.5856441	0	0	3	3	0	2	1	3	0101000020E61000008BF921857E4D5E406E893C8702483F40
150	14	121.210847138	31.2812827124	329687.2	3.4621568e+06	-1.58902	0	0	3	3	0	2	1	3	0101000020E6100000938AFE847E4D5E402CB0D22402483F40
151	14	121.210847091	31.2812766917	329687.16	3.4621562e+06	-1.5915229	0	0	3	3	0	2	1	3	0101000020E61000005013CC847E4D5E4084FACFBF01483F40
152	14	121.210847056	31.2812712543	329687.16	3.4621555e+06	-1.5923371	0	0	3	3	0	2	1	3	0101000020E6100000967EA6847E4D5E40FE85966401483F40
153	14	121.210847047	31.2812659689	329687.16	3.462155e+06	-1.5911467	0	0	3	3	0	2	1	3	0101000020E6100000AFD49C847E4D5E403CE7E90B01483F40
154	14	121.210847069	31.2812608807	329687.12	3.4621545e+06	-1.5882285	0	0	3	3	0	2	1	3	0101000020E6100000FF73B4847E4D5E402B408CB600483F40
155	14	121.210847134	31.2812539613	329687.12	3.4621538e+06	-1.5834898	0	0	3	3	0	2	1	3	0101000020E6100000103FFA847E4D5E4069A7754200483F40
156	14	121.210847231	31.281247582	329687.12	3.462153e+06	-1.579038	0	0	3	3	0	2	1	3	0101000020E6100000386662857E4D5E40E2C46ED7FF473F40
157	14	121.210847352	31.2812417144	329687.12	3.4621522e+06	-1.5749178	0	0	3	3	0	2	1	3	0101000020E61000007252E4857E4D5E40759EFD74FF473F40
158	14	121.210847483	31.2812362004	329687.12	3.4621518e+06	-1.5715187	0	0	3	3	0	2	1	3	0101000020E610000073FB70867E4D5E40572B7B18FF473F40
159	14	121.210847607	31.2812312293	329687.12	3.462151e+06	-1.5693661	0	0	3	3	0	2	1	3	0101000020E61000005020F6867E4D5E40177514C5FE473F40
160	14	121.210847723	31.2812266291	329687.12	3.4621505e+06	-1.5679537	0	0	3	3	0	2	1	3	0101000020E610000026AE72877E4D5E40B3BFE677FE473F40
161	14	121.210847864	31.2812209879	329687.16	3.46215e+06	-1.5669801	0	0	3	3	0	2	1	3	0101000020E6100000EF130A887E4D5E40B4FA4119FE473F40
162	14	121.210847993	31.2812160416	329687.16	3.4621495e+06	-1.5662099	0	0	3	3	0	2	1	3	0101000020E61000002F9794887E4D5E4057C845C6FD473F40
163	14	121.210848116	31.2812114234	329687.16	3.462149e+06	-1.5654899	0	0	3	3	0	2	1	3	0101000020E61000002AA918897E4D5E40BDC3CA78FD473F40
164	14	121.210848241	31.2812068785	329687.16	3.4621485e+06	-1.5647792	0	0	3	3	0	2	1	3	0101000020E6100000E7E09E897E4D5E4058918A2CFD473F40
165	14	121.210848386	31.2812022341	329687.16	3.462148e+06	-1.5633329	0	0	3	3	0	2	1	3	0101000020E610000033923A8A7E4D5E408A059FDEFC473F40
166	14	121.210848599	31.2811965049	329687.16	3.4621472e+06	-1.5600885	0	0	3	3	0	2	1	3	0101000020E610000032471F8B7E4D5E40854B807EFC473F40
167	14	121.210848795	31.2811917411	329687.2	3.4621468e+06	-1.5573038	0	0	3	3	0	2	1	3	0101000020E610000044BBF18B7E4D5E4007EE932EFC473F40
168	14	121.210849024	31.2811867033	329687.2	3.4621462e+06	-1.5540162	0	0	3	3	0	2	1	3	0101000020E61000004E9EE78C7E4D5E405ABE0EDAFB473F40
169	14	121.210849306	31.2811814605	329687.22	3.4621455e+06	-1.5492859	0	0	3	3	0	2	1	3	0101000020E6100000E069168E7E4D5E40CA161982FB473F40
170	14	121.210849637	31.2811762744	329687.22	3.462145e+06	-1.5431978	0	0	3	3	0	2	1	3	0101000020E610000076D2798F7E4D5E4089F5162BFB473F40
171	14	121.210850035	31.2811707969	329687.25	3.4621445e+06	-1.5362054	0	0	3	3	0	2	1	3	0101000020E6100000DF2B25917E4D5E40984631CFFA473F40
172	14	121.210850489	31.2811648409	329687.28	3.4621438e+06	-1.5302672	0	0	3	3	0	2	1	3	0101000020E610000070A60C937E4D5E405773446BFA473F40
173	14	121.210850855	31.2811600686	329687.3	3.4621432e+06	-1.5271717	0	0	3	3	0	2	1	3	0101000020E6100000C1A395947E4D5E40FF93331BFA473F40
174	14	121.210851307	31.2811541472	329687.34	3.4621425e+06	-1.5248669	0	0	3	3	0	2	1	3	0101000020E610000091F87A967E4D5E40D85BDBB7F9473F40
175	14	121.210851714	31.2811484404	329687.38	3.462142e+06	-1.5251039	0	0	3	3	0	2	1	3	0101000020E6100000E0FB2F987E4D5E40E3D61C58F9473F40
176	14	121.210852022	31.2811434676	329687.4	3.4621412e+06	-1.5282035	0	0	3	3	0	2	1	3	0101000020E610000045B27A997E4D5E4077D3AE04F9473F40
177	14	121.210852279	31.2811389138	329687.4	3.4621408e+06	-1.5311229	0	1	3	3	0	2	1	3	0101000020E6100000E4A58E9A7E4D5E406A6748B8F8473F40
178	14	121.210852546	31.281133979	329687.44	3.4621402e+06	-1.5341918	0	1	3	3	0	2	1	3	0101000020E61000004B56AD9B7E4D5E406F997D65F8473F40
179	14	121.210852783	31.2811293372	329687.44	3.4621398e+06	-1.537039	0	1	3	3	0	2	1	3	0101000020E61000005CD0AB9C7E4D5E405C389D17F8473F40
180	14	121.210853071	31.2811232819	329687.47	3.4621392e+06	-1.5407056	0	1	3	3	0	2	1	3	0101000020E6100000320DE19D7E4D5E409AE705B2F7473F40
181	14	121.2108533	31.2811180434	329687.47	3.4621385e+06	-1.5438949	0	1	3	3	0	2	1	3	0101000020E61000003CF0D69E7E4D5E40F0B7225AF7473F40
182	14	121.210853547	31.2811117925	329687.5	3.4621378e+06	-1.5477742	0	1	3	3	0	2	1	3	0101000020E61000001427E09F7E4D5E40B44E43F1F6473F40
183	14	121.210853717	31.2811070642	329687.5	3.4621372e+06	-1.550698	0	1	3	3	0	2	1	3	0101000020E610000053B096A07E4D5E40E069EFA1F6473F40
184	14	121.21085388	31.2811021425	329687.5	3.4621368e+06	-1.5536585	0	1	3	3	0	2	1	3	0101000020E61000006CB545A17E4D5E407FDF5C4FF6473F40
185	14	121.210854034	31.2810970833	329687.5	3.4621362e+06	-1.5564635	0	1	3	3	0	2	1	3	0101000020E61000009E10EBA17E4D5E4045C67BFAF5473F40
186	14	121.210854185	31.2810916519	329687.5	3.4621358e+06	-1.5592378	0	1	3	3	0	2	1	3	0101000020E61000002F338DA27E4D5E40D0165C9FF5473F40
187	14	121.210854334	31.2810856915	329687.53	3.462135e+06	-1.5620865	0	1	3	3	0	2	1	3	0101000020E6100000FE2F2DA37E4D5E40B55D5C3BF5473F40
188	14	121.210854476	31.2810794243	329687.53	3.4621342e+06	-1.5647877	0	1	3	3	0	2	1	3	0101000020E6100000A7A8C5A37E4D5E406FF236D2F4473F40
189	14	121.210854604	31.2810730985	329687.53	3.4621335e+06	-1.5672553	0	1	3	3	0	2	1	3	0101000020E610000007194FA47E4D5E40C7D71568F4473F40
190	14	121.210854733	31.281065672	329687.53	3.4621328e+06	-1.570032	0	1	3	3	0	2	1	3	0101000020E6100000479CD9A47E4D5E40AD447DEBF3473F40
191	14	121.210854834	31.2810586661	329687.5	3.462132e+06	-1.5726185	0	1	3	3	0	2	1	3	0101000020E6100000F20E46A57E4D5E402A28F375F3473F40
192	14	121.210854918	31.2810513466	329687.5	3.4621312e+06	-1.5753053	0	1	3	3	0	2	1	3	0101000020E6100000B040A0A57E4D5E40CE2426FBF2473F40
193	14	121.210854984	31.2810435862	329687.5	3.4621302e+06	-1.5781426	0	1	3	3	0	2	1	3	0101000020E6100000A11EE7A57E4D5E40C47AF378F2473F40
194	14	121.210855026	31.2810355324	329687.5	3.4621295e+06	-1.5810786	0	1	3	3	0	2	1	3	0101000020E6100000813714A67E4D5E4005ACD4F1F1473F40
195	14	121.210855041	31.2810281162	329687.47	3.4621285e+06	-1.5837837	0	1	3	3	0	2	1	3	0101000020E6100000AC5224A67E4D5E40E3556875F1473F40
196	14	121.210855032	31.2810199986	329687.47	3.4621278e+06	-1.5867743	0	1	3	3	0	2	1	3	0101000020E6100000C5A81AA67E4D5E404C8237EDF0473F40
197	14	121.210854999	31.281012444	329687.44	3.4621268e+06	-1.5897745	0	1	3	3	0	2	1	3	0101000020E6100000CD39F7A57E4D5E40C1BF786EF0473F40
198	14	121.210854949	31.2810055793	329687.44	3.462126e+06	-1.5926926	0	1	3	3	0	2	1	3	0101000020E6100000E789C1A57E4D5E4048164DFBEF473F40
199	14	121.21085488	31.2809992211	329687.4	3.4621255e+06	-1.5954969	0	1	3	3	0	2	1	3	0101000020E6100000547377A57E4D5E4073D3A090EF473F40
200	14	121.210854804	31.280993813	329687.4	3.4621248e+06	-1.5979177	0	1	3	3	0	2	1	3	0101000020E61000009BD825A57E4D5E409D36E535EF473F40
201	14	121.210854717	31.2809887655	329687.38	3.4621242e+06	-1.6001735	0	1	3	3	0	2	1	3	0101000020E61000003B6EC8A47E4D5E40AC5D36E1EE473F40
202	14	121.210854625	31.2809842117	329687.38	3.4621238e+06	-1.6021876	0	1	3	3	0	2	1	3	0101000020E610000076A565A47E4D5E40A0F1CF94EE473F40
203	14	121.210854488	31.2809784007	329687.34	3.462123e+06	-1.6047271	0	1	3	3	0	2	1	3	0101000020E6100000308BD2A37E4D5E408EE35133EE473F40
204	14	121.210854337	31.2809728407	329687.3	3.4621225e+06	-1.6071372	0	1	3	3	0	2	1	3	0101000020E6100000A06830A37E4D5E40E7DE09D6ED473F40
205	14	121.210854187	31.2809679249	329687.28	3.462122e+06	-1.609281	0	1	3	3	0	2	1	3	0101000020E6100000F0588FA27E4D5E40A5AB9083ED473F40
207	0	121.210854186	31.2809614616	329687.28	3.4621212e+06	-1.5976546	0	2	3	3	0	2	1	3	0101000020E610000010468EA27E4D5E4024022117ED473F40
208	0	121.210854322	31.2809559228	329687.28	3.4621205e+06	-1.5859637	0	2	3	3	0	2	1	3	0101000020E6100000754D20A37E4D5E40230B34BAEC473F40
209	0	121.210854504	31.2809513426	329687.28	3.46212e+06	-1.575433	0	2	3	3	0	2	1	3	0101000020E61000003CB9E3A37E4D5E40FB3B5C6DEC473F40
210	0	121.210854806	31.2809457796	329687.3	3.4621195e+06	-1.5621978	0	2	3	3	0	2	1	3	0101000020E61000005DFE27A57E4D5E40CB540710EC473F40
211	0	121.210855224	31.2809397834	329687.34	3.4621188e+06	-1.5478009	0	2	3	3	0	2	1	3	0101000020E610000054D1E8A67E4D5E402CD96DABEB473F40
212	0	121.210855633	31.2809351635	329687.38	3.4621182e+06	-1.5356625	0	2	3	3	0	2	1	3	0101000020E610000064FA9FA87E4D5E406787EB5DEB473F40
213	0	121.210856281	31.2809292498	329687.4	3.4621175e+06	-1.5182827	0	2	3	3	0	2	1	3	0101000020E610000047C357AB7E4D5E407D61B4FAEA473F40
214	0	121.210857015	31.280924051	329687.47	3.462117e+06	-1.4994102	0	2	3	3	0	2	1	3	0101000020E6100000A9E36BAE7E4D5E4070B47BA3EA473F40
215	0	121.210857872	31.2809192715	329687.56	3.4621165e+06	-1.4773948	0	2	3	3	0	2	1	3	0101000020E6100000071604B27E4D5E409DE84B53EA473F40
216	0	121.210859097	31.2809138924	329687.66	3.462116e+06	-1.4462044	0	2	3	3	0	2	1	3	0101000020E6100000766B27B77E4D5E409ED90CF9E9473F40
217	0	121.210860368	31.2809094596	329687.78	3.4621155e+06	-1.4142329	0	2	3	3	0	2	1	3	0101000020E610000048257CBC7E4D5E40791EAEAEE9473F40
218	0	121.210862019	31.2809047137	329687.94	3.462115e+06	-1.3744588	0	2	3	3	0	2	1	3	0101000020E6100000B5E468C37E4D5E403DA20E5FE9473F40
219	0	121.210863903	31.2809000819	329688.1	3.4621145e+06	-1.3332844	0	2	3	3	0	2	1	3	0101000020E6100000AFD24FCB7E4D5E4048345911E9473F40
220	0	121.210866083	31.280895529	329688.28	3.4621138e+06	-1.28769	0	2	3	3	0	2	1	3	0101000020E6100000859474D47E4D5E40CAA5F6C4E8473F40
221	0	121.210868571	31.2808911805	329688.53	3.4621135e+06	-1.2363628	0	2	3	3	0	2	1	3	0101000020E6100000C10CE4DE7E4D5E407AFB017CE8473F40
222	0	121.210871511	31.2808866668	329688.78	3.462113e+06	-1.1811155	0	2	3	3	0	2	1	3	0101000020E6100000CCD938EB7E4D5E40D8C94730E8473F40
223	0	121.210874574	31.2808824354	329689.06	3.4621125e+06	-1.1302494	0	2	3	3	0	2	1	3	0101000020E6100000D4B811F87E4D5E4057104AE9E7473F40
224	0	121.210877565	31.2808786515	329689.34	3.462112e+06	-1.0860071	0	2	3	3	0	2	1	3	0101000020E6100000A6489D047F4D5E404B56CEA9E7473F40
225	0	121.210880928	31.2808746971	329689.66	3.4621115e+06	-1.0421276	0	2	3	3	0	2	1	3	0101000020E61000000C47B8127F4D5E4083517667E7473F40
226	0	121.210884376	31.2808708626	329690	3.462111e+06	-1.0040694	0	2	3	3	0	2	1	3	0101000020E6100000128A2E217F4D5E402D442127E7473F40
227	0	121.21088847	31.2808664808	329690.38	3.4621105e+06	-0.96873784	0	2	3	3	0	2	1	3	0101000020E610000039705A327F4D5E4020949DDDE6473F40
228	0	121.210892246	31.2808626995	329690.72	3.4621102e+06	-0.9378588	0	2	3	3	0	2	1	3	0101000020E610000033E330427F4D5E40CE042D9EE6473F40
229	0	121.210896391	31.280858861	329691.12	3.4621098e+06	-0.9029944	0	2	3	3	0	2	1	3	0101000020E6100000208C93537F4D5E406CC9C65DE6473F40
230	0	121.210900407	31.2808553816	329691.5	3.4621092e+06	-0.87081635	0	2	3	3	0	2	1	3	0101000020E6100000CCB16B647F4D5E40AAE06623E6473F40
231	0	121.21090479	31.2808518106	329691.9	3.462109e+06	-0.83759403	0	2	3	3	0	2	1	3	0101000020E6100000AAE7CD767F4D5E40A58C7DE7E5473F40
232	0	121.210909017	31.2808485253	329692.28	3.4621085e+06	-0.8090229	0	2	3	3	0	2	1	3	0101000020E6100000949C88887F4D5E40184B5FB0E5473F40
233	0	121.210913129	31.2808454454	329692.7	3.4621082e+06	-0.7845668	0	2	3	3	0	2	1	3	0101000020E610000088D6C7997F4D5E403B39B37CE5473F40
234	0	121.210917608	31.2808421585	329693.1	3.4621078e+06	-0.7633237	0	2	3	3	0	2	1	3	0101000020E6100000AD2091AC7F4D5E4076188E45E5473F40
235	0	121.210922702	31.2808385735	329693.6	3.4621075e+06	-0.74016285	0	2	3	3	0	2	1	3	0101000020E6100000BCC4EEC17F4D5E4046A36809E5473F40
236	0	121.210928258	31.2808348454	329694.1	3.462107e+06	-0.7154778	0	2	3	3	0	2	1	3	0101000020E6100000627A3CD97F4D5E40FA91DCCAE4473F40
237	0	121.210932585	31.2808320668	329694.5	3.4621068e+06	-0.69662875	0	2	3	3	0	2	1	3	0101000020E6100000168F62EB7F4D5E40F7923E9CE4473F40
238	0	121.210937687	31.2808289238	329695	3.4621062e+06	-0.6747827	0	2	3	3	0	2	1	3	0101000020E61000002BCAC800804D5E40EC7D8367E4473F40
239	0	121.210942517	31.2808260797	329695.44	3.462106e+06	-0.65433556	0	2	3	3	0	2	1	3	0101000020E610000076F60A15804D5E40E62CCC37E4473F40
240	0	121.210947684	31.280823178	329695.94	3.4621058e+06	-0.63229406	0	2	3	3	0	2	1	3	0101000020E61000009BFCB62A804D5E4002781D07E4473F40
241	0	121.210952823	31.2808204364	329696.4	3.4621055e+06	-0.61021787	0	2	3	3	0	2	1	3	0101000020E61000002BF24440804D5E40EE621ED9E3473F40
242	0	121.210957929	31.2808178626	329696.9	3.462105e+06	-0.58751017	0	2	3	3	0	2	1	3	0101000020E6100000C378AF55804D5E40E7FFEFADE3473F40
243	0	121.210962806	31.2808155304	329697.34	3.4621048e+06	-0.56588614	0	2	3	3	0	2	1	3	0101000020E6100000501C246A804D5E40E346CF86E3473F40
244	0	121.210967473	31.2808134121	329697.8	3.4621045e+06	-0.54532945	0	2	3	3	0	2	1	3	0101000020E61000008143B77D804D5E40673F4563E3473F40
245	0	121.210973553	31.2808108204	329698.38	3.4621042e+06	-0.5182115	0	2	3	3	0	2	1	3	0101000020E61000002E9D3797804D5E401EFBC937E3473F40
246	0	121.210979547	31.2808084573	329698.94	3.462104e+06	-0.4905538	0	2	3	3	0	2	1	3	0101000020E61000005A9F5BB0804D5E40318B2410E3473F40
247	0	121.21098542	31.2808063142	329699.5	3.4621038e+06	-0.46338907	0	2	3	3	0	2	1	3	0101000020E61000004DB5FDC8804D5E40D2FF2FECE2473F40
248	0	121.210990968	31.2808044455	329700.03	3.4621035e+06	-0.43753058	0	2	3	3	0	2	1	3	0101000020E6100000EED342E0804D5E4071FED5CCE2473F40
249	0	121.210996306	31.2808027941	329700.53	3.4621035e+06	-0.41208047	0	2	3	3	0	2	1	3	0101000020E61000003276A6F6804D5E40F14821B1E2473F40
250	0	121.211001485	31.2808013236	329701	3.4621032e+06	-0.3872018	0	2	3	3	0	2	1	3	0101000020E6100000E05E5F0C814D5E4017897598E2473F40
251	0	121.211006857	31.2807999419	329701.53	3.462103e+06	-0.3606927	0	2	3	3	0	2	1	3	0101000020E6100000FE82E722814D5E40E02D4781E2473F40
252	0	121.211012432	31.2807986581	329702.06	3.462103e+06	-0.3326597	0	2	3	3	0	2	1	3	0101000020E6100000539F493A814D5E40D94CBD6BE2473F40
253	0	121.211018656	31.2807974048	329702.66	3.4621028e+06	-0.30064613	0	2	3	3	0	2	1	3	0101000020E61000006A976454814D5E40ED6AB656E2473F40
254	0	121.211023831	31.280796511	329703.12	3.4621028e+06	-0.27331036	0	2	3	3	0	2	1	3	0101000020E61000009634196A814D5E406E93B747E2473F40
255	0	121.211029367	31.2807957239	329703.66	3.4621025e+06	-0.24210255	0	2	3	3	0	2	1	3	0101000020E6100000AD705181814D5E40D401833AE2473F40
256	0	121.211035009	31.2807951172	329704.2	3.4621025e+06	-0.2075529	0	2	3	3	0	2	1	3	0101000020E6100000D47DFB98814D5E4020405530E2473F40
257	0	121.211041002	31.2807947168	329704.75	3.4621025e+06	-0.16675848	0	2	3	3	0	2	1	3	0101000020E6100000206D1EB2814D5E40AB8B9D29E2473F40
258	0	121.211047408	31.2807945842	329705.38	3.4621025e+06	-0.1186504	0	2	3	3	0	2	1	3	0101000020E6100000FFD0FCCC814D5E406D086427E2473F40
259	0	121.211054407	31.2807947636	329706.03	3.4621025e+06	-0.064171195	0	2	3	3	0	2	1	3	0101000020E610000078EF57EA814D5E40D08C662AE2473F40
260	0	121.211061361	31.280795209	329706.72	3.4621025e+06	-0.013432701	0	2	3	3	0	2	1	3	0101000020E61000006FBC8207824D5E404B87DF31E2473F40
261	0	121.211068911	31.2807958342	329707.44	3.4621025e+06	0.028675316	0	2	3	3	0	2	1	3	0101000020E6100000A17C2D27824D5E40F6BD5C3CE2473F40
262	0	121.211076801	31.2807964191	329708.2	3.4621025e+06	0.048770472	0	2	3	3	0	2	1	3	0101000020E6100000514F4548824D5E4050DE2C46E2473F40
263	0	121.211082066	31.2807967018	329708.7	3.4621025e+06	0.048773136	0	2	3	3	0	2	1	3	0101000020E61000007F8F5A5E824D5E40400EEB4AE2473F40
264	0	121.211087447	31.2807969299	329709.2	3.4621025e+06	0.043619383	0	2	3	3	0	2	1	3	0101000020E6100000835DEC74824D5E40DABCBE4EE2473F40
265	0	121.211092761	31.2807970862	329709.7	3.4621025e+06	0.035135083	0	2	3	3	0	2	1	3	0101000020E6100000B63A368B824D5E40850A5E51E2473F40
266	0	121.211098652	31.2807971516	329710.25	3.4621025e+06	0.021171598	0	2	3	3	0	2	1	3	0101000020E610000076A4EBA3824D5E4095EE7652E2473F40
267	0	121.21110437	31.2807971293	329710.8	3.4621025e+06	0.006056028	0	2	3	3	0	2	1	3	0101000020E6100000554CE7BB824D5E4079271752E2473F40
268	0	121.211111263	31.2807970581	329711.47	3.4621025e+06	-0.008492226	0	2	3	3	0	2	1	3	0101000020E6100000BF99D0D8824D5E403E5AE550E2473F40
269	0	121.211117693	31.2807969909	329712.06	3.4621025e+06	-0.01687351	0	2	3	3	0	2	1	3	0101000020E6100000B0C2C8F3824D5E4010BBC44FE2473F40
271	13	121.211128011	31.2807968362	329713.06	3.4621025e+06	-0.024967244	0	0	3	3	0	2	1	3	0101000020E6100000EEA00F1F834D5E409D4C2C4DE2473F40
272	13	121.211134321	31.2807967183	329713.66	3.4621025e+06	-0.029644957	0	0	3	3	0	2	1	3	0101000020E610000086F08639834D5E4031EC314BE2473F40
273	13	121.211140244	31.2807965897	329714.22	3.4621025e+06	-0.033752233	0	0	3	3	0	2	1	3	0101000020E61000005EB65E52834D5E40FF960949E2473F40
274	13	121.21114733	31.2807964152	329714.9	3.4621025e+06	-0.038325585	0	0	3	3	0	2	1	3	0101000020E6100000373F1770834D5E40381E1C46E2473F40
275	13	121.211152873	31.2807962749	329715.4	3.4621025e+06	-0.04093108	0	0	3	3	0	2	1	3	0101000020E610000074FF5687834D5E40BC88C143E2473F40
276	13	121.211158849	31.2807961248	329716	3.4621025e+06	-0.042708695	0	0	3	3	0	2	1	3	0101000020E6100000D3AD67A0834D5E400ADC3C41E2473F40
277	13	121.21116514	31.2807959696	329716.6	3.4621022e+06	-0.043664422	0	0	3	3	0	2	1	3	0101000020E6100000BD96CABA834D5E40D647A23EE2473F40
278	13	121.211171798	31.2807958047	329717.22	3.4621022e+06	-0.044234622	0	0	3	3	0	2	1	3	0101000020E6100000D78FB7D6834D5E405E0ADE3BE2473F40
279	13	121.211179435	31.2807956138	329717.94	3.4621022e+06	-0.04471403	0	0	3	3	0	2	1	3	0101000020E61000006BBABFF6834D5E409921AA38E2473F40
280	13	121.211187151	31.2807954279	329718.7	3.4621022e+06	-0.04457578	0	0	3	3	0	2	1	3	0101000020E610000059B81C17844D5E4063B28B35E2473F40
281	13	121.211195486	31.2807952481	329719.47	3.4621022e+06	-0.043046895	0	0	3	3	0	2	1	3	0101000020E6100000B35B123A844D5E4032768732E2473F40
282	13	121.211203412	31.2807950998	329720.22	3.4621022e+06	-0.040538788	0	0	3	3	0	2	1	3	0101000020E6100000FED5505B844D5E409F840A30E2473F40
283	13	121.211212034	31.280794963	329721.06	3.4621022e+06	-0.037245244	0	0	3	3	0	2	1	3	0101000020E61000004EA37A7F844D5E406FF7BE2DE2473F40
284	13	121.211220881	31.2807948498	329721.9	3.4621022e+06	-0.033635058	0	0	3	3	0	2	1	3	0101000020E6100000260896A4844D5E40B7C6D82BE2473F40
285	13	121.211229444	31.2807947622	329722.72	3.4621022e+06	-0.030319855	0	0	3	3	0	2	1	3	0101000020E6100000AA7B80C8844D5E407F89602AE2473F40
286	13	121.211238626	31.280794679	329723.6	3.462102e+06	-0.027786022	0	0	3	3	0	2	1	3	0101000020E61000009B9403EF844D5E402132FB28E2473F40
287	13	121.211247723	31.2807946008	329724.44	3.462102e+06	-0.026492214	0	0	3	3	0	2	1	3	0101000020E6100000ED682B15854D5E405254AB27E2473F40
288	13	121.211256647	31.2807945294	329725.28	3.462102e+06	-0.025765002	0	0	3	3	0	2	1	3	0101000020E61000005E7B993A854D5E4031AB7826E2473F40
289	13	121.21126589	31.2807944627	329726.2	3.462102e+06	-0.025024163	0	0	3	3	0	2	1	3	0101000020E6100000DD135E61854D5E40C4315A25E2473F40
290	13	121.211274998	31.2807943977	329727.03	3.462102e+06	-0.024549708	0	0	3	3	0	2	1	3	0101000020E6100000D6B79187854D5E4082054324E2473F40
291	13	121.21128387	31.2807943301	329727.88	3.462102e+06	-0.024673631	0	0	3	3	0	2	1	3	0101000020E6100000A1F4C7AC854D5E4086AE2023E2473F40
292	13	121.211292835	31.2807942652	329728.75	3.462102e+06	-0.024662474	0	0	3	3	0	2	1	3	0101000020E6100000100D62D2854D5E4038F00922E2473F40
293	13	121.211301625	31.2807942112	329729.56	3.462102e+06	-0.024003364	0	0	3	3	0	2	1	3	0101000020E6100000DE3D40F7854D5E4097022221E2473F40
294	13	121.211310817	31.2807941699	329730.44	3.462102e+06	-0.022588383	0	0	3	3	0	2	1	3	0101000020E61000009613CE1D864D5E40C2A07020E2473F40
295	13	121.21131962	31.2807941644	329731.28	3.462102e+06	-0.019612215	0	0	3	3	0	2	1	3	0101000020E6100000CD39BA42864D5E4072015920E2473F40
296	13	121.211328656	31.2807941966	329732.16	3.462102e+06	-0.015206818	0	0	3	3	0	2	1	3	0101000020E6100000928EA068864D5E40B84DE320E2473F40
297	13	121.211337688	31.2807942764	329733	3.462102e+06	-0.0097083915	0	0	3	3	0	2	1	3	0101000020E6100000D397828E864D5E40C00A3A22E2473F40
298	13	121.211346671	31.2807944063	329733.88	3.462102e+06	-0.003441852	0	0	3	3	0	2	1	3	0101000020E6100000100430B4864D5E404FF56724E2473F40
299	13	121.211355522	31.2807945779	329734.72	3.462102e+06	0.002682336	0	0	3	3	0	2	1	3	0101000020E61000006BB44FD9864D5E4081F94827E2473F40
300	13	121.21136437	31.2807947744	329735.56	3.462102e+06	0.0075079836	0	0	3	3	0	2	1	3	0101000020E6100000232C6CFE864D5E408AEF942AE2473F40
301	13	121.21137274	31.2807949634	329736.34	3.462102e+06	0.010302171	0	0	3	3	0	2	1	3	0101000020E610000038648721874D5E403DAFC02DE2473F40
302	13	121.211380335	31.2807951434	329737.06	3.462102e+06	0.011856393	0	0	3	3	0	2	1	3	0101000020E6100000EC756241874D5E4054C7C530E2473F40
303	13	121.211386863	31.2807953056	329737.7	3.462102e+06	0.012770923	0	0	3	3	0	2	1	3	0101000020E6100000E6D8C35C874D5E401E6C7E33E2473F40
304	13	121.211393539	31.2807954765	329738.34	3.462102e+06	0.013427658	0	0	3	3	0	2	1	3	0101000020E6100000CE25C478874D5E40A76E5C36E2473F40
305	13	121.211400979	31.2807956725	329739.03	3.462102e+06	0.01403699	0	0	3	3	0	2	1	3	0101000020E61000006FC9F897874D5E40EE3EA639E2473F40
306	13	121.211406825	31.2807958298	329739.6	3.462102e+06	0.014498782	0	0	3	3	0	2	1	3	0101000020E6100000ADE17DB0874D5E401CD8493CE2473F40
307	13	121.211412594	31.2807959879	329740.16	3.462102e+06	0.01496004	0	0	3	3	0	2	1	3	0101000020E6100000524CB0C8874D5E40E6E0F03EE2473F40
308	13	121.211418081	31.2807961405	329740.66	3.462102e+06	0.015383485	0	1	3	3	0	2	1	3	0101000020E610000066EBB3DF874D5E40604A8041E2473F40
309	13	121.211423404	31.2807962904	329741.2	3.462102e+06	0.01579734	0	1	3	3	0	2	1	3	0101000020E61000007F7207F6874D5E402B1B0444E2473F40
310	13	121.211429143	31.2807964511	329741.72	3.462102e+06	0.017496966	0	1	3	3	0	2	1	3	0101000020E6100000CEA6190E884D5E40AF4EB646E2473F40
311	13	121.211435575	31.280796644	329742.34	3.462102e+06	0.017968878	0	1	3	3	0	2	1	3	0101000020E610000080F51329884D5E407ACEF249E2473F40
312	13	121.21144277	31.2807968625	329743.03	3.462102e+06	0.018488193	0	1	3	3	0	2	1	3	0101000020E61000000B884147884D5E40C5419D4DE2473F40
313	13	121.211449421	31.280797067	329743.66	3.462102e+06	0.018962242	0	1	3	3	0	2	1	3	0101000020E610000000FD2663884D5E40E6930B51E2473F40
314	13	121.211454733	31.2807972321	329744.16	3.462102e+06	0.01933615	0	1	3	3	0	2	1	3	0101000020E610000071B46E79884D5E4044ADD053E2473F40
315	13	121.211460218	31.2807974042	329744.7	3.462102e+06	0.019704731	0	1	3	3	0	2	1	3	0101000020E6100000C32D7090884D5E4038D7B356E2473F40
316	13	121.211466269	31.2807975931	329745.25	3.462102e+06	0.019937487	0	1	3	3	0	2	1	3	0101000020E6100000FA63D1A9884D5E40F728DF59E2473F40
317	13	121.211472539	31.2807977837	329745.84	3.462102e+06	0.01976811	0	1	3	3	0	2	1	3	0101000020E610000074C01DC4884D5E40E1C7115DE2473F40
318	13	121.211479237	31.2807979817	329746.5	3.462102e+06	0.01919926	0	1	3	3	0	2	1	3	0101000020E6100000ADAC35E0884D5E402F2F6460E2473F40
319	13	121.211486292	31.2807981859	329747.16	3.462102e+06	0.01845624	0	1	3	3	0	2	1	3	0101000020E61000004FECCCFD884D5E407537D163E2473F40
320	13	121.211493555	31.2807983913	329747.84	3.4621022e+06	0.01766741	0	1	3	3	0	2	1	3	0101000020E61000008C82431C894D5E4025674367E2473F40
321	13	121.211501891	31.2807986212	329748.66	3.4621022e+06	0.016758207	0	1	3	3	0	2	1	3	0101000020E6100000C7383A3F894D5E40DED01E6BE2473F40
322	13	121.211509884	31.280798835	329749.4	3.4621022e+06	0.015843678	0	1	3	3	0	2	1	3	0101000020E6100000E3A3C060894D5E407414B56EE2473F40
323	13	121.211517527	31.2807990325	329750.16	3.4621022e+06	0.014800783	0	1	3	3	0	2	1	3	0101000020E6100000BB3FCF80894D5E4000560572E2473F40
324	13	121.211523972	31.280799194	329750.75	3.4621022e+06	0.013869742	0	1	3	3	0	2	1	3	0101000020E6100000D783D79B894D5E4021F9BA74E2473F40
325	13	121.211530107	31.2807993443	329751.34	3.4621022e+06	0.013063869	0	1	3	3	0	2	1	3	0101000020E6100000CDEB92B5894D5E40BA814077E2473F40
326	13	121.211535555	31.2807994754	329751.84	3.4621022e+06	0.01249928	0	1	3	3	0	2	1	3	0101000020E6100000A3AA6CCC894D5E40B3937379E2473F40
327	13	121.211541655	31.2807996189	329752.44	3.4621022e+06	0.011955461	0	1	3	3	0	2	1	3	0101000020E6100000DF7D02E6894D5E409FE7DB7BE2473F40
328	13	121.211547436	31.2807997527	329753	3.4621022e+06	0.011527758	0	1	3	3	0	2	1	3	0101000020E61000000DCB41FE894D5E4046921A7EE2473F40
330	0	121.211557035	31.2807999183	329753.9	3.4621022e+06	0.0077945394	0	2	3	3	0	2	1	3	0101000020E610000014A484268A4D5E4067D1E180E2473F40
331	0	121.211562806	31.2807999809	329754.47	3.4621022e+06	0.004020308	0	2	3	3	0	2	1	3	0101000020E61000007B34B93E8A4D5E40D4AEEE81E2473F40
332	0	121.211568566	31.2808000224	329755	3.4621022e+06	2.7164246e-05	0	2	3	3	0	2	1	3	0101000020E610000039F5E1568A4D5E4090ECA082E2473F40
333	0	121.211575887	31.2808008662	329755.7	3.4621022e+06	0.07729934	0	2	3	3	0	2	1	3	0101000020E610000061D296758A4D5E407904C990E2473F40
334	0	121.211581996	31.2808016838	329756.28	3.4621022e+06	0.099859014	0	2	3	3	0	2	1	3	0101000020E6100000844F368F8A4D5E402E95809EE2473F40
335	0	121.211588674	31.2808027469	329756.9	3.4621025e+06	0.12679963	0	2	3	3	0	2	1	3	0101000020E61000002DC238AB8A4D5E40FE8F56B0E2473F40
336	0	121.21159622	31.2808041576	329757.66	3.4621025e+06	0.15897119	0	2	3	3	0	2	1	3	0101000020E6100000DD36DFCA8A4D5E400B7901C8E2473F40
337	0	121.211603438	31.2808057421	329758.34	3.4621028e+06	0.19249411	0	2	3	3	0	2	1	3	0101000020E6100000987B25E98A4D5E4038D996E2E2473F40
338	0	121.211610765	31.2808076314	329759.03	3.462103e+06	0.22931771	0	2	3	3	0	2	1	3	0101000020E610000005CAE0078B4D5E4089544902E3473F40
339	0	121.211618367	31.2808098466	329759.75	3.4621032e+06	0.26808214	0	2	3	3	0	2	1	3	0101000020E6100000DE5FC3278B4D5E40B28A7327E3473F40
340	0	121.211623871	31.2808116313	329760.28	3.4621035e+06	0.29664445	0	2	3	3	0	2	1	3	0101000020E6100000DE3FD93E8B4D5E4019C56445E3473F40
341	0	121.211631103	31.280814212	329760.97	3.4621038e+06	0.33475462	0	2	3	3	0	2	1	3	0101000020E6100000E48C2E5D8B4D5E40C1CAB070E3473F40
342	0	121.211637746	31.2808168569	329761.62	3.462104e+06	0.37069649	0	2	3	3	0	2	1	3	0101000020E6100000D36A0B798B4D5E400F8D109DE3473F40
343	0	121.211644354	31.2808197363	329762.25	3.4621042e+06	0.40739888	0	2	3	3	0	2	1	3	0101000020E610000009B4C2948B4D5E40D77A5FCDE3473F40
344	0	121.211650062	31.2808224505	329762.8	3.4621045e+06	0.43989903	0	2	3	3	0	2	1	3	0101000020E6100000219FB3AC8B4D5E404DE1E8FAE3473F40
345	0	121.211655269	31.2808251234	329763.3	3.4621048e+06	0.4702842	0	2	3	3	0	2	1	3	0101000020E610000064988AC28B4D5E40EEE5C027E4473F40
346	0	121.211660128	31.2808277964	329763.78	3.4621052e+06	0.49933672	0	2	3	3	0	2	1	3	0101000020E610000024E8EBD68B4D5E4083589954E4473F40
347	0	121.211664499	31.280830358	329764.2	3.4621055e+06	0.5261192	0	2	3	3	0	2	1	3	0101000020E6100000783B41E98B4D5E407F55937FE4473F40
348	0	121.2116696	31.2808335611	329764.7	3.4621058e+06	0.55901283	0	2	3	3	0	2	1	3	0101000020E6100000AC63A6FE8B4D5E40318B50B5E4473F40
349	0	121.211674044	31.2808365495	329765.12	3.462106e+06	0.5887357	0	2	3	3	0	2	1	3	0101000020E610000018194A118C4D5E40BD9F73E7E4473F40
350	0	121.211678855	31.2808400031	329765.56	3.4621065e+06	0.62180275	0	2	3	3	0	2	1	3	0101000020E6100000B4DE77258C4D5E4019B96421E5473F40
351	0	121.211683013	31.2808431836	329765.97	3.4621068e+06	0.65120435	0	2	3	3	0	2	1	3	0101000020E61000000A7DE8368C4D5E40D4DDC056E5473F40
352	0	121.211687076	31.2808464633	329766.38	3.4621072e+06	0.6798705	0	2	3	3	0	2	1	3	0101000020E6100000FA19F3478C4D5E401E12C78DE5473F40
353	0	121.211691462	31.2808501461	329766.78	3.4621075e+06	0.7078325	0	2	3	3	0	2	1	3	0101000020E61000007A88585A8C4D5E408A9390CBE5473F40
354	0	121.211696387	31.2808544221	329767.28	3.462108e+06	0.7345442	0	2	3	3	0	2	1	3	0101000020E61000002BB6006F8C4D5E4042DB4D13E6473F40
355	0	121.211700251	31.2808578965	329767.66	3.4621085e+06	0.75389194	0	2	3	3	0	2	1	3	0101000020E610000066A6357F8C4D5E40754A984DE6473F40
356	0	121.211704291	31.2808616731	329768.03	3.4621088e+06	0.77442455	0	2	3	3	0	2	1	3	0101000020E6100000259127908C4D5E4012AAF48CE6473F40
357	0	121.211708218	31.2808654802	329768.4	3.4621092e+06	0.7941263	0	2	3	3	0	2	1	3	0101000020E6100000AF26A0A08C4D5E40CA08D4CCE6473F40
358	0	121.211712348	31.2808696077	329768.8	3.4621098e+06	0.81328034	0	2	3	3	0	2	1	3	0101000020E610000071B4F2B18C4D5E4009831312E7473F40
359	0	121.211716656	31.2808740711	329769.25	3.4621102e+06	0.8329294	0	2	3	3	0	2	1	3	0101000020E6100000766204C48C4D5E403CABF55CE7473F40
360	0	121.211720797	31.2808785316	329769.62	3.4621108e+06	0.85219485	0	2	3	3	0	2	1	3	0101000020E6100000E0BF62D58C4D5E40D95ECBA7E7473F40
361	0	121.211725239	31.280883516	329770.06	3.4621112e+06	0.8733572	0	2	3	3	0	2	1	3	0101000020E61000008A4F04E88C4D5E409B346BFBE7473F40
362	0	121.211729621	31.2808886756	329770.5	3.4621118e+06	0.8956928	0	2	3	3	0	2	1	3	0101000020E6100000877265FA8C4D5E40CD84FB51E8473F40
363	0	121.211734047	31.2808941742	329770.94	3.4621125e+06	0.9203035	0	2	3	3	0	2	1	3	0101000020E610000024D4F50C8D4D5E4070D33BAEE8473F40
364	0	121.211737011	31.2808980237	329771.22	3.4621128e+06	0.93733805	0	2	3	3	0	2	1	3	0101000020E6100000426664198D4D5E40734DD1EEE8473F40
365	0	121.211740121	31.2809022206	329771.53	3.4621132e+06	0.95567536	0	2	3	3	0	2	1	3	0101000020E61000008DBC6F268D4D5E40CDD93A35E9473F40
366	0	121.21174313	31.2809064477	329771.8	3.4621138e+06	0.9739123	0	2	3	3	0	2	1	3	0101000020E61000002CA00E338D4D5E40671B267CE9473F40
367	0	121.211746161	31.2809108901	329772.12	3.4621142e+06	0.9929066	0	2	3	3	0	2	1	3	0101000020E61000001B23C53F8D4D5E40DB11AEC6E9473F40
368	0	121.211748913	31.2809150972	329772.38	3.4621148e+06	1.0107192	0	2	3	3	0	2	1	3	0101000020E61000001B13504B8D4D5E403A6D430DEA473F40
369	0	121.211751856	31.2809198092	329772.66	3.4621152e+06	1.0307283	0	2	3	3	0	2	1	3	0101000020E6100000CA18A8578D4D5E400450515CEA473F40
370	0	121.211754611	31.2809244808	329772.94	3.4621158e+06	1.0516771	0	2	3	3	0	2	1	3	0101000020E61000006C4136638D4D5E4089AEB1AAEA473F40
371	0	121.211757255	31.2809292425	329773.2	3.4621162e+06	1.0739319	0	2	3	3	0	2	1	3	0101000020E61000009B3A4D6E8D4D5E400E0795FAEA473F40
372	0	121.211759756	31.2809340168	329773.44	3.4621168e+06	1.0962539	0	2	3	3	0	2	1	3	0101000020E610000041A8CA788D4D5E406B7DAE4AEB473F40
373	0	121.211762202	31.280939005	329773.7	3.4621172e+06	1.1198725	0	2	3	3	0	2	1	3	0101000020E61000009D070D838D4D5E4052A55E9EEB473F40
374	0	121.211764462	31.2809439852	329773.9	3.4621178e+06	1.144539	0	2	3	3	0	2	1	3	0101000020E6100000AFAF878C8D4D5E402171ECF1EB473F40
375	0	121.211766635	31.2809492177	329774.12	3.4621185e+06	1.171841	0	2	3	3	0	2	1	3	0101000020E610000060EDA4958D4D5E40B9DBB549EC473F40
376	0	121.211768533	31.2809542897	329774.3	3.462119e+06	1.1999214	0	2	3	3	0	2	1	3	0101000020E6100000A4E39A9D8D4D5E40B2EECD9EEC473F40
377	0	121.211770228	31.2809594	329774.5	3.4621195e+06	1.2301216	0	2	3	3	0	2	1	3	0101000020E6100000B2E1B6A48D4D5E40F8808AF4EC473F40
378	0	121.21177176	31.2809646706	329774.66	3.4621202e+06	1.2622726	0	2	3	3	0	2	1	3	0101000020E6100000A6DA23AB8D4D5E40F48EF74CED473F40
379	0	121.211772998	31.280969598	329774.78	3.4621208e+06	1.2925469	0	2	3	3	0	2	1	3	0101000020E61000007F2555B08D4D5E408C94A29FED473F40
380	0	121.211773986	31.2809742406	329774.88	3.4621212e+06	1.321508	0	2	3	3	0	2	1	3	0101000020E6100000DE007AB48D4D5E403B6586EDED473F40
381	0	121.211774813	31.2809808618	329774.97	3.462122e+06	1.7434758	0	2	3	3	0	2	1	3	0101000020E6100000E6FCF1B78D4D5E409F3B9C5CEE473F40
382	0	121.211774716	31.2809854794	329774.97	3.4621225e+06	1.6887585	0	2	3	3	0	2	1	3	0101000020E6100000BDD589B78D4D5E4084AC14AAEE473F40
383	0	121.211774757	31.2809907447	329774.97	3.462123e+06	1.6379862	0	2	3	3	0	2	1	3	0101000020E6100000BCDBB5B78D4D5E4016F76A02EF473F40
384	8	121.21177465	31.2809938282	329774.97	3.4621232e+06	1.5589936	0	2	3	3	0	2	1	3	0101000020E6100000CCF742B78D4D5E40317F2636EF473F40
386	8	121.21177466	31.280998949	329774.97	3.462124e+06	1.5551938	0	0	3	3	0	2	1	3	0101000020E610000094B44DB78D4D5E40562A108CEF473F40
387	8	121.211774695	31.2810038436	329775	3.4621245e+06	1.5526977	0	0	3	3	0	2	1	3	0101000020E61000004E4973B78D4D5E40F34F2EDEEF473F40
388	8	121.211774733	31.2810092323	329775	3.462125e+06	1.5512656	0	0	3	3	0	2	1	3	0101000020E6100000AA169CB78D4D5E40429A9638F0473F40
389	8	121.211774736	31.2810140427	329775	3.4621255e+06	1.5521948	0	0	3	3	0	2	1	3	0101000020E61000004C4F9FB78D4D5E40FE1C4B89F0473F40
390	8	121.211774731	31.2810193444	329775.03	3.4621262e+06	1.5534059	0	0	3	3	0	2	1	3	0101000020E6100000E9F099B78D4D5E40CABD3DE2F0473F40
391	8	121.211774721	31.2810239307	329775.03	3.4621268e+06	1.5543876	0	0	3	3	0	2	1	3	0101000020E610000021348FB78D4D5E40F8BF2F2FF1473F40
392	8	121.211774706	31.2810285829	329775.03	3.4621272e+06	1.5553857	0	0	3	3	0	2	1	3	0101000020E6100000F6187FB78D4D5E40F6CB3C7DF1473F40
393	8	121.211774679	31.2810344905	329775.06	3.4621278e+06	1.5566615	0	0	3	3	0	2	1	3	0101000020E6100000421B62B78D4D5E40DBBE59E0F1473F40
394	8	121.211774659	31.281039346	329775.06	3.4621285e+06	1.557181	0	0	3	3	0	2	1	3	0101000020E6100000B3A14CB78D4D5E4090F5CF31F2473F40
395	8	121.211774656	31.2810448078	329775.06	3.462129e+06	1.5564612	0	0	3	3	0	2	1	3	0101000020E6100000116949B78D4D5E402C36728DF2473F40
396	8	121.211774658	31.2810499669	329775.06	3.4621295e+06	1.5557215	0	0	3	3	0	2	1	3	0101000020E6100000D28E4BB78D4D5E409C6000E4F2473F40
397	8	121.211774664	31.2810548504	329775.1	3.4621302e+06	1.5550251	0	0	3	3	0	2	1	3	0101000020E6100000170052B78D4D5E40A5D9EE35F3473F40
398	8	121.211774674	31.2810594465	329775.1	3.4621308e+06	1.5543706	0	0	3	3	0	2	1	3	0101000020E6100000DEBC5CB78D4D5E4009F30A83F3473F40
399	8	121.211774692	31.281064558	329775.1	3.4621312e+06	1.5534298	0	0	3	3	0	2	1	3	0101000020E6100000AC1070B78D4D5E40B9ACCCD8F3473F40
400	8	121.211774724	31.2810704816	329775.12	3.4621318e+06	1.5520897	0	0	3	3	0	2	1	3	0101000020E6100000C36C92B78D4D5E40CD572E3CF4473F40
401	8	121.211774753	31.2810754339	329775.12	3.4621325e+06	1.5512493	0	0	3	3	0	2	1	3	0101000020E61000003990B1B78D4D5E403C4F448FF4473F40
402	8	121.211774782	31.281080779	329775.12	3.462133e+06	1.5508374	0	0	3	3	0	2	1	3	0101000020E6100000AFB3D0B78D4D5E40D656F1E8F4473F40
403	8	121.211774816	31.2810867886	329775.16	3.4621338e+06	1.5504098	0	0	3	3	0	2	1	3	0101000020E61000008835F5B78D4D5E40EA5FC44DF5473F40
404	8	121.211774858	31.2810934412	329775.16	3.4621345e+06	1.5499336	0	0	3	3	0	2	1	3	0101000020E6100000674E22B88D4D5E40F81261BDF5473F40
405	8	121.211774889	31.2810980927	329775.2	3.462135e+06	1.5495847	0	0	3	3	0	2	1	3	0101000020E61000009E9743B88D4D5E404E1D6B0BF6473F40
406	8	121.211774925	31.2811031352	329775.2	3.4621355e+06	1.549211	0	0	3	3	0	2	1	3	0101000020E6100000393F6AB88D4D5E40B07C0460F6473F40
407	8	121.211774956	31.2811084712	329775.22	3.462136e+06	1.5493453	0	0	3	3	0	2	1	3	0101000020E610000070888BB88D4D5E40BC6E8AB9F6473F40
408	8	121.211774973	31.2811139768	329775.22	3.4621368e+06	1.5503047	0	0	3	3	0	2	1	3	0101000020E61000005DC99DB88D4D5E40F4CDE815F7473F40
409	8	121.211774981	31.2811197968	329775.22	3.4621372e+06	1.551677	0	0	3	3	0	2	1	3	0101000020E61000006360A6B88D4D5E40A0838D77F7473F40
410	8	121.211774976	31.2811257496	329775.25	3.462138e+06	1.5532963	0	0	3	3	0	2	1	3	0101000020E6100000FF01A1B88D4D5E4072986CDBF7473F40
411	8	121.211774931	31.2811318728	329775.25	3.4621388e+06	1.5564476	0	0	3	3	0	2	1	3	0101000020E61000007DB070B88D4D5E400B8A2742F8473F40
412	8	121.211774835	31.2811384087	329775.25	3.4621395e+06	1.5614815	0	0	3	3	0	2	1	3	0101000020E6100000369C09B88D4D5E401704CFAFF8473F40
413	8	121.211774688	31.2811451777	329775.25	3.4621402e+06	1.5673528	0	0	3	3	0	2	1	3	0101000020E610000029C56BB78D4D5E404DA65F21F9473F40
414	8	121.211774502	31.2811518827	329775.25	3.462141e+06	1.5730463	0	0	3	3	0	2	1	3	0101000020E6100000DE0DA4B68D4D5E40C467DD91F9473F40
415	8	121.211774249	31.2811593125	329775.22	3.4621418e+06	1.5789915	0	0	3	3	0	2	1	3	0101000020E6100000C26594B58D4D5E404227840EFA473F40
416	8	121.211773962	31.281166616	329775.22	3.4621425e+06	1.5843118	0	0	3	3	0	2	1	3	0101000020E6100000CC3B60B48D4D5E406E720C89FA473F40
417	8	121.211773659	31.2811737218	329775.2	3.4621432e+06	1.5883317	0	0	3	3	0	2	1	3	0101000020E6100000CBE31AB38D4D5E4027A04300FB473F40
418	8	121.211773321	31.2811812764	329775.2	3.4621442e+06	1.5913639	0	0	3	3	0	2	1	3	0101000020E61000000FF7AFB18D4D5E40B362027FFB473F40
419	8	121.211772999	31.2811888304	329775.16	3.462145e+06	1.5919833	0	0	3	3	0	2	1	3	0101000020E6100000603856B08D4D5E408991BEFDFB473F40
420	8	121.211772717	31.2811968093	329775.16	3.4621458e+06	1.5886306	0	0	3	3	0	2	1	3	0101000020E6100000CE6C27AF8D4D5E40DCAE9B83FC473F40
421	8	121.21177248	31.2812047805	329775.16	3.4621468e+06	1.5836434	0	0	3	3	0	2	1	3	0101000020E6100000BDF228AE8D4D5E40F3B95709FD473F40
422	8	121.211772253	31.2812133667	329775.12	3.4621478e+06	1.5791459	0	0	3	3	0	2	1	3	0101000020E6100000753535AD8D4D5E40B02C6599FD473F40
423	8	121.211772069	31.2812216919	329775.12	3.4621488e+06	1.5754303	0	0	3	3	0	2	1	3	0101000020E6100000ECA36FAC8D4D5E40E4A21125FE473F40
424	8	121.211771985	31.2812262358	329775.12	3.4621492e+06	1.5734128	0	0	3	3	0	2	1	3	0101000020E61000002D7215AC8D4D5E40C6894D71FE473F40
425	8	121.211771866	31.2812343514	329775.12	3.46215e+06	1.569488	0	0	3	3	0	2	1	3	0101000020E6100000B5AB95AB8D4D5E4057C675F9FE473F40
426	8	121.211771792	31.2812429812	329775.16	3.462151e+06	1.5646013	0	0	3	3	0	2	1	3	0101000020E6100000BD3646AB8D4D5E40C97B3E8AFF473F40
427	8	121.211771751	31.2812518492	329775.16	3.462152e+06	1.5601128	0	0	3	3	0	2	1	3	0101000020E6100000BF301AAB8D4D5E40E740061F00483F40
428	8	121.211771722	31.2812609688	329775.2	3.462153e+06	1.5574644	0	0	3	3	0	2	1	3	0101000020E6100000490DFBAA8D4D5E4025A306B800483F40
429	8	121.211771689	31.2812697022	329775.2	3.462154e+06	1.5570852	0	0	3	3	0	2	1	3	0101000020E6100000519ED7AA8D4D5E40FE4D8C4A01483F40
430	8	121.211771643	31.2812783602	329775.2	3.462155e+06	1.5581845	0	0	3	3	0	2	1	3	0101000020E6100000EF39A6AA8D4D5E40AB21CEDB01483F40
431	8	121.211771587	31.2812867532	329775.22	3.4621558e+06	1.5596974	0	0	3	3	0	2	1	3	0101000020E6100000C5186AAA8D4D5E40C2CA9D6802483F40
432	8	121.211771522	31.2812944782	329775.22	3.4621568e+06	1.5611922	0	0	3	3	0	2	1	3	0101000020E6100000B54D24AA8D4D5E40156A38EA02483F40
433	8	121.211771449	31.2813016711	329775.22	3.4621575e+06	1.5627047	0	0	3	3	0	2	1	3	0101000020E61000009EEBD5A98D4D5E4045AFE56203483F40
434	8	121.211771373	31.2813080212	329775.22	3.4621582e+06	1.5641255	0	0	3	3	0	2	1	3	0101000020E6100000E65084A98D4D5E400F286FCD03483F40
435	8	121.2117713	31.2813133944	329775.22	3.4621588e+06	1.5653466	0	0	3	3	0	2	1	3	0101000020E6100000CFEE35A98D4D5E40F0DF942704483F40
436	8	121.211771202	31.2813196881	329775.22	3.4621595e+06	1.5667764	0	0	3	3	0	2	1	3	0101000020E6100000C6B4CCA88D4D5E40451C2C9104483F40
437	8	121.21177112	31.281324348	329775.22	3.46216e+06	1.5678338	0	0	3	3	0	2	1	3	0101000020E6100000C9A874A88D4D5E40803A5ADF04483F40
438	8	121.211771031	31.2813290389	329775.22	3.4621605e+06	1.5689198	0	0	3	3	0	2	1	3	0101000020E6100000A71815A88D4D5E40997D0D2E05483F40
439	8	121.211770943	31.2813336563	329775.22	3.462161e+06	1.5703892	0	0	3	3	0	2	1	3	0101000020E6100000669BB6A78D4D5E409612857B05483F40
440	8	121.211770846	31.2813382759	329775.22	3.4621615e+06	1.5720363	0	1	3	3	0	2	1	3	0101000020E61000003D744EA78D4D5E40811A06C905483F40
441	8	121.211770744	31.2813428589	329775.22	3.462162e+06	1.5723042	0	1	3	3	0	2	1	3	0101000020E6100000B2EEE0A68D4D5E404BF0E91506483F40
442	8	121.211770642	31.2813475335	329775.22	3.4621625e+06	1.572285	0	1	3	3	0	2	1	3	0101000020E6100000266973A68D4D5E405931576406483F40
443	8	121.211770536	31.2813523858	329775.22	3.462163e+06	1.5726343	0	1	3	3	0	2	1	3	0101000020E6100000179801A68D4D5E409FA9BFB506483F40
444	8	121.211770428	31.2813573145	329775.22	3.4621638e+06	1.5729396	0	1	3	3	0	2	1	3	0101000020E610000046A18DA58D4D5E409444700807483F40
445	8	121.211770332	31.2813623077	329775.22	3.4621642e+06	1.5723301	0	1	3	3	0	2	1	3	0101000020E6100000FF8C26A58D4D5E400AE6355C07483F40
446	8	121.211770288	31.2813673137	329775.22	3.4621648e+06	1.5689547	0	1	3	3	0	2	1	3	0101000020E61000005E4EF7A48D4D5E403F8132B007483F40
447	8	121.211770248	31.2813721141	329775.22	3.4621652e+06	1.5666012	0	1	3	3	0	2	1	3	0101000020E6100000405BCCA48D4D5E40DE10BC0008483F40
448	8	121.21177021	31.281376718	329775.25	3.4621658e+06	1.5651077	0	1	3	3	0	2	1	3	0101000020E6100000E48DA3A48D4D5E4073AAF94D08483F40
449	8	121.211770141	31.2813820613	329775.25	3.4621665e+06	1.5653507	0	1	3	3	0	2	1	3	0101000020E6100000517759A48D4D5E40EEF69EA708483F40
450	8	121.211770056	31.2813877983	329775.25	3.462167e+06	1.5661561	0	1	3	3	0	2	1	3	0101000020E6100000B232FEA38D4D5E402331DF0709483F40
451	8	121.21176998	31.2813926591	329775.25	3.4621675e+06	1.5668356	0	1	3	3	0	2	1	3	0101000020E6100000F997ACA38D4D5E40422B6C5909483F40
452	8	121.211769896	31.2813979934	329775.25	3.4621682e+06	1.5673566	0	1	3	3	0	2	1	3	0101000020E61000003B6652A38D4D5E4022D0EAB209483F40
453	8	121.211769804	31.2814041011	329775.25	3.4621688e+06	1.567486	0	1	3	3	0	2	1	3	0101000020E6100000769DEFA28D4D5E404E2F63190A483F40
454	8	121.211769723	31.2814102356	329775.25	3.4621695e+06	1.5669183	0	1	3	3	0	2	1	3	0101000020E61000005AA498A28D4D5E4062A94E800A483F40
455	8	121.211769671	31.281414792	329775.25	3.46217e+06	1.5661262	0	1	3	3	0	2	1	3	0101000020E6100000B3CE60A28D4D5E402A40C0CC0A483F40
456	8	121.211769603	31.281421416	329775.25	3.4621708e+06	1.5649205	0	1	3	3	0	2	1	3	0101000020E610000000CB17A28D4D5E40301DE23B0B483F40
457	8	121.211769558	31.2814263172	329775.25	3.4621712e+06	1.5640576	0	1	3	3	0	2	1	3	0101000020E61000007F79E7A18D4D5E40949B1C8E0B483F40
458	8	121.211769513	31.2814315291	329775.28	3.462172e+06	1.5632927	0	1	3	3	0	2	1	3	0101000020E6100000FD27B7A18D4D5E403B8C8DE50B483F40
459	8	121.211769467	31.2814368282	329775.28	3.4621725e+06	1.562813	0	1	3	3	0	2	1	3	0101000020E61000009BC385A18D4D5E404C02753E0C483F40
460	8	121.211769416	31.2814420863	329775.28	3.462173e+06	1.562795	0	1	3	3	0	2	1	3	0101000020E6100000D5004FA18D4D5E406360AC960C483F40
461	8	121.211769353	31.2814477381	329775.28	3.4621738e+06	1.5633475	0	1	3	3	0	2	1	3	0101000020E6100000865B0BA18D4D5E4034AC7EF50C483F40
462	8	121.211769283	31.281453326	329775.28	3.4621742e+06	1.5642018	0	1	3	3	0	2	1	3	0101000020E61000001232C0A08D4D5E403B853E530D483F40
463	8	121.211769198	31.2814596273	329775.28	3.462175e+06	1.5652184	0	1	3	3	0	2	1	3	0101000020E610000073ED64A08D4D5E40D965F6BC0D483F40
464	8	121.211769111	31.2814655334	329775.28	3.4621758e+06	1.5661751	0	1	3	3	0	2	1	3	0101000020E6100000128307A08D4D5E4079E70C200E483F40
465	8	121.211769016	31.2814715273	329775.28	3.4621762e+06	1.5671457	0	1	3	3	0	2	1	3	0101000020E6100000AC81A19F8D4D5E4038829C840E483F40
466	8	121.211768914	31.2814775657	329775.28	3.462177e+06	1.5681176	0	1	3	3	0	2	1	3	0101000020E610000020FC339F8D4D5E403C3DEBE90E483F40
467	8	121.21176881	31.281483423	329775.28	3.4621778e+06	1.5690134	0	1	3	3	0	2	1	3	0101000020E6100000D250C49E8D4D5E40B126304C0F483F40
468	8	121.211768717	31.2814884312	329775.28	3.4621782e+06	1.569768	0	1	3	3	0	2	1	3	0101000020E61000002D75609E8D4D5E40D33436A00F483F40
469	8	121.211768593	31.2814947624	329775.28	3.462179e+06	1.5707248	0	1	3	3	0	2	1	3	0101000020E61000005150DB9D8D4D5E40D8806E0A10483F40
470	8	121.211768492	31.2814995759	329775.28	3.4621795e+06	1.5714543	0	1	3	3	0	2	1	3	0101000020E6100000A6DD6E9D8D4D5E401054305B10483F40
471	8	121.211768385	31.2815044889	329775.28	3.46218e+06	1.5722078	0	1	3	3	0	2	1	3	0101000020E6100000B6F9FB9C8D4D5E40B1809DAD10483F40
473	0	121.211768189	31.2815107987	329775.28	3.4621808e+06	1.5764493	0	2	3	3	0	2	1	3	0101000020E6100000A485299C8D4D5E4029E3791711483F40
474	0	121.21176797	31.2815160956	329775.28	3.4621812e+06	1.5812776	0	2	3	3	0	2	1	3	0101000020E6100000615F3E9B8D4D5E404DE6577011483F40
475	0	121.211767667	31.2815217342	329775.25	3.462182e+06	1.5886123	0	2	3	3	0	2	1	3	0101000020E61000006007F9998D4D5E409080F1CE11483F40
476	0	121.211767947	31.2815260804	329775.28	3.4621822e+06	1.5752916	0	2	3	3	0	2	1	3	0101000020E610000030AD259B8D4D5E40004ADC1712483F40
477	0	121.211767883	31.2815308736	329775.28	3.462183e+06	1.5722781	0	2	3	3	0	2	1	3	0101000020E610000000F5E09A8D4D5E4023ED466812483F40
478	0	121.211767919	31.2815355896	329775.3	3.4621835e+06	1.5642622	0	2	3	3	0	2	1	3	0101000020E61000009B9C079B8D4D5E40F9FD65B712483F40
479	0	121.211768104	31.2815401391	329775.34	3.462184e+06	1.551045	0	2	3	3	0	2	1	3	0101000020E61000000541CE9B8D4D5E401FF2B90313483F40
480	0	121.211768607	31.2815453958	329775.38	3.4621845e+06	1.5234219	0	2	3	3	0	2	1	3	0101000020E61000009B58EA9D8D4D5E40E54CEB5B13483F40
481	0	121.211769322	31.2815502015	329775.47	3.462185e+06	1.4919524	0	2	3	3	0	2	1	3	0101000020E61000004F12EAA08D4D5E40ED9F8BAC13483F40
482	0	121.211770274	31.2815549125	329775.56	3.4621855e+06	1.4563503	0	2	3	3	0	2	1	3	0101000020E61000001446E8A48D4D5E40343795FB13483F40
483	0	121.211771588	31.2815599908	329775.7	3.4621862e+06	1.4137312	0	2	3	3	0	2	1	3	0101000020E6100000A62B6BAA8D4D5E401B59C85014483F40
484	0	121.211773418	31.2815656059	329775.88	3.4621868e+06	1.3621305	0	2	3	3	0	2	1	3	0101000020E6100000381E18B28D4D5E40D804FDAE14483F40
485	0	121.211775795	31.2815711785	329776.12	3.4621872e+06	1.2985166	0	2	3	3	0	2	1	3	0101000020E6100000006710BC8D4D5E4057277B0C15483F40
486	0	121.211778484	31.2815761084	329776.38	3.462188e+06	1.2309209	0	2	3	3	0	2	1	3	0101000020E6100000B2B157C78D4D5E40B7E9305F15483F40
487	0	121.211781413	31.2815804838	329776.66	3.4621885e+06	1.1621931	0	2	3	3	0	2	1	3	0101000020E610000015AFA0D38D4D5E40E41C99A815483F40
488	0	121.211785296	31.2815849444	329777.06	3.4621888e+06	1.0686951	0	2	3	3	0	2	1	3	0101000020E6100000FF05EAE38D4D5E40753E6FF315483F40
489	0	121.211789124	31.2815881628	329777.4	3.4621892e+06	0.97105587	0	2	3	3	0	2	1	3	0101000020E6100000A04EF8F38D4D5E40AE2A6E2916483F40
490	0	121.211793713	31.2815912511	329777.88	3.4621895e+06	0.86300373	0	2	3	3	0	2	1	3	0101000020E610000057B537078E4D5E4071503E5D16483F40
491	0	121.211798285	31.2815939282	329778.3	3.4621898e+06	0.7733257	0	2	3	3	0	2	1	3	0101000020E610000021DB641A8E4D5E40055F288A16483F40
492	0	121.211802982	31.2815964676	329778.75	3.46219e+06	0.70077664	0	2	3	3	0	2	1	3	0101000020E6100000A838182E8E4D5E40D902C3B416483F40
493	0	121.211807849	31.2815990287	329779.22	3.4621905e+06	0.64596814	0	2	3	3	0	2	1	3	0101000020E61000006E1F82428E4D5E4014DABADF16483F40
494	0	121.211812771	31.2816014324	329779.7	3.4621908e+06	0.59848005	0	2	3	3	0	2	1	3	0101000020E61000007D1427578E4D5E402DAA0E0817483F40
495	0	121.211818545	31.2816038653	329780.25	3.462191e+06	0.54020816	0	2	3	3	0	2	1	3	0101000020E610000086DD5E6F8E4D5E4004E4DF3017483F40
496	0	121.21182369	31.2816054884	329780.75	3.462191e+06	0.47427034	0	2	3	3	0	2	1	3	0101000020E61000005B44F3848E4D5E40570D1B4C17483F40
497	0	121.211829789	31.2816066609	329781.3	3.4621912e+06	0.37703577	0	2	3	3	0	2	1	3	0101000020E6100000B604889E8E4D5E40B9E6C65F17483F40
498	0	121.211835013	31.2816071784	329781.8	3.4621912e+06	0.29099315	0	2	3	3	0	2	1	3	0101000020E6100000E53E71B48E4D5E40FE8B756817483F40
499	0	121.211841383	31.2816072654	329782.44	3.4621912e+06	0.18603702	0	2	3	3	0	2	1	3	0101000020E61000002AFB28CF8E4D5E408135EB6917483F40
500	0	121.211846808	31.2816069005	329782.94	3.4621912e+06	0.09763367	0	2	3	3	0	2	1	3	0101000020E6100000CF07EAE58E4D5E40B6F9CB6317483F40
501	0	121.211852287	31.281606242	329783.47	3.4621912e+06	0.01754384	0	2	3	3	0	2	1	3	0101000020E6100000DC0FE5FC8E4D5E404DBDBF5817483F40
502	0	121.211854987	31.281606117	329783.72	3.462191e+06	0.023692546	0	2	3	3	0	2	1	3	0101000020E6100000352A38088F4D5E4059DEA65617483F40
503	0	121.21186073	31.2816063228	329784.28	3.4621912e+06	0.024339164	0	2	3	3	0	2	1	3	0101000020E610000007AA4E208F4D5E40D7C51A5A17483F40
505	5	121.211869324	31.2816065964	329785.1	3.4621912e+06	0.024547422	0	0	3	3	0	2	1	3	0101000020E6100000C3665A448F4D5E4039E0B15E17483F40
506	5	121.211875303	31.2816067712	329785.66	3.4621912e+06	0.022278938	0	0	3	3	0	2	1	3	0101000020E6100000C44D6E5D8F4D5E40DBA2A06117483F40
507	5	121.211881966	31.2816069457	329786.28	3.4621912e+06	0.01929513	0	0	3	3	0	2	1	3	0101000020E610000043A560798F4D5E40A21B8E6417483F40
508	5	121.211888216	31.2816070944	329786.88	3.4621912e+06	0.0165084	0	0	3	3	0	2	1	3	0101000020E61000002E8897938F4D5E4004C50C6717483F40
509	5	121.211894904	31.2816072375	329787.53	3.4621912e+06	0.013538444	0	0	3	3	0	2	1	3	0101000020E61000009FB7A4AF8F4D5E402161736917483F40
510	5	121.21190039	31.2816073423	329788.03	3.4621912e+06	0.011094197	0	0	3	3	0	2	1	3	0101000020E6100000D143A7C68F4D5E40F27D356B17483F40
511	5	121.211906087	31.2816074386	329788.6	3.4621912e+06	0.008522115	0	0	3	3	0	2	1	3	0101000020E6100000415F8CDE8F4D5E40EA18D36C17483F40
512	5	121.211912813	31.2816075362	329789.22	3.4621912e+06	0.0054802517	0	0	3	3	0	2	1	3	0101000020E61000000E5CC2FA8F4D5E404049766E17483F40
513	5	121.211918428	31.2816076042	329789.75	3.4621912e+06	0.0029369344	0	0	3	3	0	2	1	3	0101000020E6100000816B4F12904D5E400B589A6F17483F40
514	5	121.211923931	31.2816076588	329790.28	3.4621912e+06	0.00044101945	0	0	3	3	0	2	1	3	0101000020E6100000A0386429904D5E4060D9847017483F40
515	5	121.211929197	31.2816077003	329790.78	3.4621912e+06	-0.0019228096	0	0	3	3	0	2	1	3	0101000020E6100000AF8B7A3F904D5E401C17377117483F40
516	5	121.21193454	31.2816077341	329791.28	3.4621912e+06	-0.0041264403	0	0	3	3	0	2	1	3	0101000020E6100000578CE355904D5E409A42C87117483F40
517	5	121.211941425	31.2816077644	329791.94	3.4621912e+06	-0.006845599	0	0	3	3	0	2	1	3	0101000020E6100000BB42C472904D5E40CE654A7217483F40
518	5	121.211948122	31.2816077899	329792.6	3.4621912e+06	-0.008730148	0	0	3	3	0	2	1	3	0101000020E6100000121CDB8E904D5E405AEBB77217483F40
519	5	121.211955057	31.2816078248	329793.25	3.4621912e+06	-0.009328051	0	0	3	3	0	2	1	3	0101000020E61000005B82F1AB904D5E404ED04D7317483F40
520	5	121.211960326	31.2816078512	329793.75	3.4621912e+06	-0.009616929	0	0	3	3	0	2	1	3	0101000020E61000000C0E0BC2904D5E406A33BF7317483F40
521	5	121.211967458	31.281607885	329794.44	3.4621912e+06	-0.0100050485	0	0	3	3	0	2	1	3	0101000020E610000048FBF4DF904D5E40E85E507417483F40
522	5	121.211972913	31.2816079094	329794.94	3.4621912e+06	-0.010301028	0	0	3	3	0	2	1	3	0101000020E6100000433ED6F6904D5E40FE2AB97417483F40
523	5	121.211978714	31.2816079338	329795.5	3.462191e+06	-0.010611405	0	0	3	3	0	2	1	3	0101000020E610000000052B0F914D5E4013F7217517483F40
524	5	121.211984456	31.2816079721	329796.06	3.462191e+06	-0.009909079	0	0	3	3	0	2	1	3	0101000020E6100000F1714027914D5E405F76C67517483F40
525	5	121.211990844	31.2816080464	329796.66	3.462191e+06	-0.0071469108	0	0	3	3	0	2	1	3	0101000020E610000003820B42914D5E401594057717483F40
526	5	121.211997126	31.2816081373	329797.25	3.462191e+06	-0.004155108	0	0	3	3	0	2	1	3	0101000020E610000006C1645C914D5E40B1FD8B7817483F40
527	5	121.212003663	31.2816082491	329797.88	3.462191e+06	-0.0010434535	0	0	3	3	0	2	1	3	0101000020E6100000E7CDCF77914D5E40172B6C7A17483F40
528	5	121.212010358	31.2816083815	329798.53	3.462191e+06	0.0021369203	0	0	3	3	0	2	1	3	0101000020E61000007D81E493914D5E406ED2A47C17483F40
529	5	121.212017123	31.2816085337	329799.16	3.4621912e+06	0.0053433664	0	0	3	3	0	2	1	3	0101000020E6100000875E44B0914D5E401984327F17483F40
530	5	121.212024168	31.2816087067	329799.84	3.4621912e+06	0.008362327	0	0	3	3	0	2	1	3	0101000020E610000062E1D0CD914D5E409C8B198217483F40
531	5	121.212031181	31.2816088771	329800.5	3.4621912e+06	0.010043312	0	0	3	3	0	2	1	3	0101000020E610000025083BEB914D5E406468F58417483F40
532	5	121.212038437	31.2816090197	329801.2	3.4621912e+06	0.008828378	0	0	3	3	0	2	1	3	0101000020E61000003D1AAA09924D5E40C0DE598717483F40
533	5	121.212046057	31.2816091265	329801.9	3.4621912e+06	0.0049950243	0	0	3	3	0	2	1	3	0101000020E6100000E303A029924D5E409892248917483F40
534	5	121.212051363	31.2816091866	329802.4	3.4621912e+06	0.0022828614	0	0	3	3	0	2	1	3	0101000020E6100000104AE13F924D5E403EB3268A17483F40
535	5	121.212058926	31.2816092618	329803.16	3.4621912e+06	-0.0008454544	0	0	3	3	0	2	1	3	0101000020E6100000ACFF995F924D5E4084AE698B17483F40
536	5	121.212066397	31.28160933	329803.84	3.4621912e+06	-0.0029597923	0	0	3	3	0	2	1	3	0101000020E610000084ECEF7E924D5E4035998E8C17483F40
537	5	121.212074339	31.2816094121	329804.62	3.4621912e+06	-0.003575078	0	0	3	3	0	2	1	3	0101000020E6100000DA943FA0924D5E401D37EF8D17483F40
538	5	121.212082277	31.2816095304	329805.38	3.4621912e+06	-0.0014506743	0	0	3	3	0	2	1	3	0101000020E6100000ADF18AC1924D5E40574FEB8F17483F40
539	5	121.212087719	31.2816096298	329805.88	3.4621912e+06	0.0008617988	0	0	3	3	0	2	1	3	0101000020E61000003F3F5ED8924D5E40CB3A969117483F40
540	5	121.212093223	31.2816097415	329806.4	3.4621912e+06	0.0032522525	0	0	3	3	0	2	1	3	0101000020E61000003F1F74EF924D5E403EFA759317483F40
541	5	121.212098562	31.281609861	329806.9	3.4621912e+06	0.005570799	0	0	3	3	0	2	1	3	0101000020E610000064D4D805934D5E40E239779517483F40
542	5	121.212104376	31.2816100009	329807.47	3.4621912e+06	0.0079671135	0	0	3	3	0	2	1	3	0101000020E61000008B903B1E934D5E408F17D09717483F40
543	5	121.212110035	31.2816101437	329808	3.4621912e+06	0.009945308	0	0	3	3	0	2	1	3	0101000020E61000009EDEF735934D5E40D269359A17483F40
544	5	121.212115583	31.2816102876	329808.53	3.4621912e+06	0.011503256	0	0	3	3	0	2	1	3	0101000020E61000003FFD3C4D934D5E408B759F9C17483F40
545	5	121.212121401	31.2816104366	329809.1	3.4621912e+06	0.012438561	0	0	3	3	0	2	1	3	0101000020E6100000E904A465934D5E40C7681F9F17483F40
546	5	121.212127054	31.2816105689	329809.62	3.4621912e+06	0.01217544	0	0	3	3	0	2	1	3	0101000020E6100000B8E1597D934D5E402AA257A117483F40
547	5	121.212132809	31.2816106802	329810.2	3.4621912e+06	0.010254766	0	0	3	3	0	2	1	3	0101000020E610000012447D95934D5E40CFA935A317483F40
548	5	121.212138583	31.281610777	329810.72	3.4621912e+06	0.0077066547	0	0	3	3	0	2	1	3	0101000020E61000001B0DB5AD934D5E40896AD5A417483F40
549	5	121.212144302	31.2816108601	329811.28	3.4621912e+06	0.005116464	0	0	3	3	0	2	1	3	0101000020E6100000DBC7B1C5934D5E40F3533AA617483F40
550	5	121.212149877	31.2816109305	329811.8	3.4621912e+06	0.0027025756	0	0	3	3	0	2	1	3	0101000020E610000030E413DD934D5E4092B168A717483F40
551	5	121.21215589	31.2816109978	329812.38	3.4621912e+06	0.0004048005	0	0	3	3	0	2	1	3	0101000020E61000000B4D4CF6934D5E40B4BE89A817483F40
552	5	121.212161317	31.2816110509	329812.9	3.4621912e+06	-0.0014480072	0	0	3	3	0	2	1	3	0101000020E6100000717F0F0D944D5E40C5CE6DA917483F40
553	5	121.212167066	31.2816110989	329813.44	3.4621912e+06	-0.0033041309	0	0	3	3	0	2	1	3	0101000020E610000088702C25944D5E4054F73BAA17483F40
554	5	121.212172845	31.2816111379	329814	3.4621912e+06	-0.0051587117	0	0	3	3	0	2	1	3	0101000020E6100000F497693D944D5E404878E3AA17483F40
555	5	121.212178572	31.2816111674	329814.53	3.4621912e+06	-0.007007524	0	0	3	3	0	2	1	3	0101000020E6100000BAE96E55944D5E40E02B62AB17483F40
556	5	121.212186324	31.2816111925	329815.28	3.4621912e+06	-0.009532582	0	0	3	3	0	2	1	3	0101000020E6100000438FF275944D5E409EF9CDAB17483F40
557	5	121.212193542	31.2816112001	329815.97	3.4621912e+06	-0.011904222	0	0	3	3	0	2	1	3	0101000020E6100000FED33894944D5E40E79DEEAB17483F40
558	5	121.212200332	31.2816111968	329816.6	3.4621912e+06	-0.013921084	0	0	3	3	0	2	1	3	0101000020E6100000FB88B3B0944D5E408471E0AB17483F40
559	5	121.212206428	31.2816112016	329817.2	3.4621912e+06	-0.014502804	0	0	3	3	0	2	1	3	0101000020E6100000B41045CA944D5E402C0FF5AB17483F40
560	5	121.212212053	31.2816112113	329817.72	3.4621912e+06	-0.014346531	0	0	3	3	0	2	1	3	0101000020E6100000EEDCDCE1944D5E406FB81EAC17483F40
561	5	121.212217362	31.2816112217	329818.22	3.462191e+06	-0.014152408	0	0	3	3	0	2	1	3	0101000020E6100000BD5B21F8944D5E405B634BAC17483F40
562	5	121.21222044	31.281611228	329818.53	3.462191e+06	-0.014041828	0	0	3	3	0	2	1	3	0101000020E6100000EF550A05954D5E40477266AC17483F40
563	5	121.212226152	31.2816112778	329819.06	3.462191e+06	-0.009292578	0	1	3	3	0	2	1	3	0101000020E61000008A8CFF1C954D5E40F5553CAD17483F40
564	5	121.212231506	31.2816113513	329819.56	3.462191e+06	-0.0064258394	0	1	3	3	0	2	1	3	0101000020E6100000DA5C7433954D5E40100478AE17483F40
565	5	121.212236925	31.2816114389	329820.1	3.462191e+06	-0.0035135392	0	1	3	3	0	2	1	3	0101000020E61000003BF82E4A954D5E404841F0AF17483F40
566	5	121.212242773	31.2816115489	329820.66	3.462191e+06	-0.0003515233	0	1	3	3	0	2	1	3	0101000020E61000003B36B662954D5E408FB3C8B117483F40
567	5	121.21224808	31.2816116626	329821.16	3.462191e+06	0.0025118939	0	1	3	3	0	2	1	3	0101000020E6100000488FF878954D5E40080AB1B317483F40
568	5	121.212254119	31.2816118076	329821.72	3.4621912e+06	0.005747635	0	1	3	3	0	2	1	3	0101000020E6100000F6E24C92954D5E4038CF1FB617483F40
569	5	121.212260045	31.2816119657	329822.28	3.4621912e+06	0.00890508	0	1	3	3	0	2	1	3	0101000020E610000070E127AB954D5E4002D8C6B817483F40
570	5	121.212265641	31.2816121295	329822.8	3.4621912e+06	0.011874501	0	1	3	3	0	2	1	3	0101000020E6100000358AA0C2954D5E40035C86BB17483F40
571	5	121.212271992	31.2816123321	329823.44	3.4621912e+06	0.01523062	0	1	3	3	0	2	1	3	0101000020E6100000CBDF43DD954D5E401185ECBE17483F40
572	5	121.212278572	31.2816125611	329824.06	3.4621912e+06	0.018694863	0	1	3	3	0	2	1	3	0101000020E61000006C18DDF8954D5E403B11C4C217483F40
573	5	121.212284234	31.2816127714	329824.6	3.4621912e+06	0.02153166	0	1	3	3	0	2	1	3	0101000020E6100000219F9C10964D5E40864C4BC617483F40
574	5	121.212290192	31.2816129755	329825.16	3.4621912e+06	0.022392387	0	1	3	3	0	2	1	3	0101000020E6100000B3F99929964D5E40D9E6B7C917483F40
575	5	121.21229639	31.2816131176	329825.75	3.4621912e+06	0.018137192	0	1	3	3	0	2	1	3	0101000020E6100000F8069943964D5E4073371ACC17483F40
576	5	121.212302872	31.2816132302	329826.38	3.4621912e+06	0.012785835	0	1	3	3	0	2	1	3	0101000020E61000008F05C95E964D5E4076D4FDCD17483F40
577	5	121.21230921	31.2816133312	329826.97	3.4621912e+06	0.008891762	0	1	3	3	0	2	1	3	0101000020E6100000BC655E79964D5E40229FAFCF17483F40
578	5	121.212315636	31.2816134158	329827.6	3.4621912e+06	0.005281048	0	1	3	3	0	2	1	3	0101000020E61000002A435294964D5E40D1F91AD117483F40
579	5	121.212322069	31.2816134955	329828.2	3.4621912e+06	0.0026557043	0	1	3	3	0	2	1	3	0101000020E6100000BDA44DAF964D5E40E54871D217483F40
580	5	121.212328529	31.2816135651	329828.8	3.4621912e+06	0.0003131877	0	1	3	3	0	2	1	3	0101000020E6100000050466CA964D5E40E7369CD317483F40
581	5	121.212334573	31.2816136397	329829.4	3.4621912e+06	-0.0004518668	0	1	3	3	0	2	1	3	0101000020E610000017B6BFE3964D5E40799EDCD417483F40
582	5	121.212340828	31.2816137464	329830	3.4621912e+06	0.0010855043	0	1	3	3	0	2	1	3	0101000020E610000065F7FBFD964D5E405DE4A6D617483F40
583	5	121.21234704	31.2816138665	329830.6	3.4621912e+06	0.0030514498	0	1	3	3	0	2	1	3	0101000020E6100000F40C0A18974D5E40B5B7AAD817483F40
584	5	121.212353067	31.2816139934	329831.16	3.4621912e+06	0.0049673277	0	1	3	3	0	2	1	3	0101000020E6100000197E5131974D5E40BCBFCBDA17483F40
585	5	121.212358387	31.281614114	329831.66	3.4621912e+06	0.0066706855	0	1	3	3	0	2	1	3	0101000020E610000090CCA147974D5E40D6B8D1DC17483F40
586	5	121.212364433	31.2816142608	329832.25	3.4621912e+06	0.008617457	0	1	3	3	0	2	1	3	0101000020E610000064A4FD60974D5E40253948DF17483F40
587	5	121.212369731	31.2816143981	329832.75	3.4621912e+06	0.010331998	0	1	3	3	0	2	1	3	0101000020E61000008A533677974D5E4017EC95E117483F40
588	5	121.212375192	31.281614548	329833.25	3.4621912e+06	0.01213123	0	1	3	3	0	2	1	3	0101000020E6100000CA071E8E974D5E40E2BC19E417483F40
590	0	121.212380938	31.2816146204	329833.8	3.4621912e+06	0.0074041192	0	2	3	3	0	2	1	3	0101000020E61000003EC037A6974D5E4086B150E517483F40
591	0	121.212386617	31.2816146302	329834.34	3.4621912e+06	0.001071656	0	2	3	3	0	2	1	3	0101000020E6100000E18709BE974D5E40BDC87AE517483F40
592	0	121.212391922	31.2816145654	329834.84	3.4621912e+06	-0.008478053	0	2	3	3	0	2	1	3	0101000020E61000002CBB49D4974D5E40637864E417483F40
593	0	121.212397918	31.2816143993	329835.44	3.4621912e+06	-0.022265015	0	2	3	3	0	2	1	3	0101000020E61000001AE36FED974D5E4081139BE117483F40
594	0	121.212403487	31.2816140954	329835.97	3.4621912e+06	-0.040933188	0	2	3	3	0	2	1	3	0101000020E61000002B8ECB04984D5E40EBD581DC17483F40
595	0	121.2124093	31.2816135778	329836.5	3.462191e+06	-0.06755837	0	2	3	3	0	2	1	3	0101000020E610000071372D1D984D5E40B4C2D2D317483F40
596	0	121.212415235	31.2816127453	329837.06	3.462191e+06	-0.10595062	0	2	3	3	0	2	1	3	0101000020E6100000D1DF1136984D5E404533DBC517483F40
597	0	121.212420407	31.2816117381	329837.56	3.4621908e+06	-0.14751337	0	2	3	3	0	2	1	3	0101000020E61000005A44C34B984D5E40294FF5B417483F40
598	0	121.212425655	31.2816104417	329838.06	3.4621908e+06	-0.19506177	0	2	3	3	0	2	1	3	0101000020E61000009C43C661984D5E404950359F17483F40
599	0	121.212430961	31.2816088134	329838.56	3.4621905e+06	-0.24897416	0	2	3	3	0	2	1	3	0101000020E6100000C8890778984D5E4080D1E38317483F40
600	0	121.212436285	31.2816068567	329839.06	3.4621902e+06	-0.30652463	0	2	3	3	0	2	1	3	0101000020E6100000C2235C8E984D5E401ADB0F6317483F40
601	0	121.212441487	31.2816045949	329839.56	3.46219e+06	-0.36670324	0	2	3	3	0	2	1	3	0101000020E6100000A2BE2DA4984D5E40B37F1D3D17483F40
602	0	121.212446436	31.2816021031	329840.03	3.4621898e+06	-0.42659327	0	2	3	3	0	2	1	3	0101000020E610000064B1EFB8984D5E40A04C4F1317483F40
603	0	121.212451269	31.2815992789	329840.47	3.4621895e+06	-0.4898632	0	2	3	3	0	2	1	3	0101000020E6100000511635CD984D5E40E373EDE316483F40
604	0	121.21245582	31.2815961871	329840.9	3.462189e+06	-0.5556178	0	2	3	3	0	2	1	3	0101000020E6100000ACAF4BE0984D5E40D6450EB016483F40
605	0	121.212460058	31.2815928229	329841.3	3.4621888e+06	-0.6247014	0	2	3	3	0	2	1	3	0101000020E61000003E3412F2984D5E40D1249D7716483F40
606	0	121.212463803	31.281589401	329841.66	3.4621882e+06	-0.6910731	0	2	3	3	0	2	1	3	0101000020E6100000015EC701994D5E40FB31343E16483F40
607	0	121.212467329	31.2815857123	329841.97	3.462188e+06	-0.7585422	0	2	3	3	0	2	1	3	0101000020E610000081619110994D5E407059510016483F40
608	0	121.2124705	31.2815819015	329842.28	3.4621875e+06	-0.82475907	0	2	3	3	0	2	1	3	0101000020E61000005837DE1D994D5E40871662C015483F40
609	0	121.212473484	31.2815777435	329842.56	3.462187e+06	-0.89396137	0	2	3	3	0	2	1	3	0101000020E61000000543622A994D5E402E9D9F7A15483F40
610	0	121.212476054	31.281573496	329842.78	3.4621865e+06	-0.96331406	0	2	3	3	0	2	1	3	0101000020E61000003EC72935994D5E408ABD5C3315483F40
611	0	121.212478293	31.2815689954	329843	3.462186e+06	-1.0359434	0	2	3	3	0	2	1	3	0101000020E6100000E0E28D3E994D5E4082CFDAE714483F40
612	0	121.212480098	31.2815644701	329843.16	3.4621855e+06	-1.1075964	0	2	3	3	0	2	1	3	0101000020E61000007FFD1F46994D5E408ACBEE9B14483F40
613	0	121.212481516	31.2815597877	329843.28	3.462185e+06	-1.1811658	0	2	3	3	0	2	1	3	0101000020E61000005E8E124C994D5E404B0A604D14483F40
614	0	121.212482504	31.2815550467	329843.38	3.4621845e+06	-1.2556746	0	2	3	3	0	2	1	3	0101000020E6100000BD693750994D5E40AA99D5FD13483F40
615	0	121.212482998	31.2815501332	329843.4	3.462184e+06	-1.3371675	0	2	3	3	0	2	1	3	0101000020E61000006DD74952994D5E40484766AB13483F40
616	0	121.21248296	31.2815455591	329843.4	3.4621835e+06	-1.417305	0	2	3	3	0	2	1	3	0101000020E6100000110A2152994D5E4025ABA85E13483F40
617	0	121.21248243	31.2815405787	329843.34	3.462183e+06	-1.5047908	0	2	3	3	0	2	1	3	0101000020E6100000C6F4E74F994D5E406F031A0B13483F40
618	0	121.212481481	31.2815357011	329843.25	3.4621822e+06	-1.5868853	0	2	3	3	0	2	1	3	0101000020E6100000A4F9EC4B994D5E4085E144B912483F40
620	6	121.212480594	31.2815276898	329843.12	3.4621815e+06	-1.6249288	0	0	3	3	0	2	1	3	0101000020E6100000F0903448994D5E40049CDC3212483F40
621	6	121.212480584	31.2815229143	329843.12	3.462181e+06	-1.6133285	0	0	3	3	0	2	1	3	0101000020E610000029D42948994D5E403DFEBDE211483F40
622	6	121.212480638	31.2815180683	329843.12	3.4621805e+06	-1.6015191	0	0	3	3	0	2	1	3	0101000020E610000091CF6348994D5E40E394709111483F40
623	6	121.212480748	31.281513516	329843.12	3.46218e+06	-1.5904652	0	0	3	3	0	2	1	3	0101000020E610000023ECD948994D5E401B9A104511483F40
624	6	121.212480937	31.2815083695	329843.12	3.4621792e+06	-1.5783043	0	0	3	3	0	2	1	3	0101000020E61000000FDCA449994D5E40838DB8EE10483F40
625	6	121.212481191	31.2815027988	329843.16	3.4621788e+06	-1.5669863	0	0	3	3	0	2	1	3	0101000020E61000000C97B54A994D5E401694429110483F40
626	6	121.212481471	31.2814972484	329843.16	3.462178e+06	-1.5582956	0	0	3	3	0	2	1	3	0101000020E6100000DD3CE24B994D5E40C0CA233410483F40
627	6	121.212481722	31.28149241	329843.2	3.4621775e+06	-1.552994	0	0	3	3	0	2	1	3	0101000020E610000038BFEF4C994D5E40B005F7E20F483F40
628	6	121.212481982	31.2814871711	329843.2	3.462177e+06	-1.5498633	0	0	3	3	0	2	1	3	0101000020E610000079EB064E994D5E40391E128B0F483F40
629	6	121.212482185	31.2814820428	329843.22	3.4621765e+06	-1.5508907	0	0	3	3	0	2	1	3	0101000020E6100000B0E3E04E994D5E40BE3C08350F483F40
630	6	121.212482334	31.2814767516	329843.22	3.4621758e+06	-1.555186	0	0	3	3	0	2	1	3	0101000020E61000007FE0804F994D5E40D1B442DC0E483F40
631	6	121.212482442	31.2814719623	329843.22	3.4621752e+06	-1.5593088	0	0	3	3	0	2	1	3	0101000020E61000004FD7F44F994D5E40C7D1E88B0E483F40
632	6	121.212482549	31.2814663972	329843.22	3.4621748e+06	-1.5634922	0	0	3	3	0	2	1	3	0101000020E61000003FBB6750994D5E409EE58A2E0E483F40
633	6	121.212482641	31.2814605079	329843.22	3.462174e+06	-1.5674428	0	0	3	3	0	2	1	3	0101000020E61000000384CA50994D5E40CA8BBCCB0D483F40
634	6	121.212482703	31.2814559626	329843.22	3.4621735e+06	-1.5699569	0	0	3	3	0	2	1	3	0101000020E610000072160D51994D5E4096A17A7F0D483F40
635	6	121.212482754	31.2814514106	329843.22	3.462173e+06	-1.5723157	0	0	3	3	0	2	1	3	0101000020E610000038D94351994D5E40A8F01B330D483F40
636	6	121.212482795	31.2814468584	329843.22	3.4621725e+06	-1.5744754	0	0	3	3	0	2	1	3	0101000020E610000036DF6F51994D5E40D363BCE60C483F40
637	6	121.212482826	31.281442208	329843.2	3.462172e+06	-1.5766588	0	0	3	3	0	2	1	3	0101000020E61000006D289151994D5E40F412B7980C483F40
638	6	121.212482846	31.281437526	329843.2	3.4621715e+06	-1.5788552	0	0	3	3	0	2	1	3	0101000020E6100000FCA1A651994D5E4083092A4A0C483F40
639	6	121.212482854	31.2814323402	329843.2	3.462171e+06	-1.5812855	0	0	3	3	0	2	1	3	0101000020E61000000239AF51994D5E401C3229F30B483F40
640	6	121.212482848	31.2814275175	329843.2	3.4621702e+06	-1.5835093	0	0	3	3	0	2	1	3	0101000020E6100000BDC7A851994D5E4062DB3FA20B483F40
641	6	121.212482833	31.2814227154	329843.16	3.4621698e+06	-1.585611	0	0	3	3	0	2	1	3	0101000020E610000092AC9851994D5E4099FEAE510B483F40
642	6	121.212482804	31.2814176603	329843.16	3.4621692e+06	-1.5877492	0	0	3	3	0	2	1	3	0101000020E61000001D897951994D5E405E81DFFC0A483F40
643	6	121.212482765	31.2814127086	329843.16	3.4621688e+06	-1.5898379	0	0	3	3	0	2	1	3	0101000020E6100000E0A84F51994D5E40A41DCCA90A483F40
644	6	121.21248271	31.281407496	329843.12	3.4621682e+06	-1.592035	0	0	3	3	0	2	1	3	0101000020E6100000979A1451994D5E40542B58520A483F40
645	6	121.212482643	31.2814023512	329843.12	3.4621675e+06	-1.594203	0	0	3	3	0	2	1	3	0101000020E6100000C5A9CC50994D5E40E86B07FC09483F40
646	6	121.212482565	31.2813973034	329843.1	3.462167e+06	-1.5963285	0	0	3	3	0	2	1	3	0101000020E61000004BE97850994D5E401C4957A709483F40
647	6	121.212482472	31.2813921205	329843.06	3.4621665e+06	-1.59851	0	0	3	3	0	2	1	3	0101000020E6100000A60D1550994D5E404CE6625009483F40
648	6	121.212482357	31.2813865563	329843.06	3.4621658e+06	-1.6008497	0	0	3	3	0	2	1	3	0101000020E6100000B092994F994D5E40B2D708F308483F40
649	6	121.212482233	31.2813811912	329843.03	3.4621652e+06	-1.6030327	0	0	3	3	0	2	1	3	0101000020E6100000D46D144F994D5E40DDE9059908483F40
650	6	121.21248212	31.281375834	329843	3.4621645e+06	-1.6039112	0	0	3	3	0	2	1	3	0101000020E6100000A0189B4E994D5E402BEA243F08483F40
651	6	121.212482058	31.2813705852	329843	3.462164e+06	-1.6014917	0	0	3	3	0	2	1	3	0101000020E61000003286584E994D5E408A7D15E707483F40
652	6	121.212482061	31.2813653252	329843	3.4621635e+06	-1.5960455	0	0	3	3	0	2	1	3	0101000020E6100000D4BE5B4E994D5E4060F6D58E07483F40
653	6	121.212482128	31.2813600922	329843	3.4621628e+06	-1.5886408	0	0	3	3	0	2	1	3	0101000020E6100000A6AFA34E994D5E4007660A3707483F40
654	6	121.212482245	31.2813548667	329843	3.4621622e+06	-1.5809015	0	0	3	3	0	2	1	3	0101000020E61000005D50214F994D5E40040C5FDF06483F40
655	6	121.212482385	31.2813496969	329843	3.4621618e+06	-1.5745121	0	0	3	3	0	2	1	3	0101000020E610000045A3B74F994D5E40CEECA28806483F40
656	6	121.212482549	31.2813443747	329843	3.4621612e+06	-1.569208	0	0	3	3	0	2	1	3	0101000020E61000003FBB6750994D5E400540582F06483F40
657	6	121.212482726	31.2813391886	329843	3.4621605e+06	-1.5649545	0	0	3	3	0	2	1	3	0101000020E6100000A3C82551994D5E40C41E56D805483F40
658	6	121.212482915	31.2813337884	329843	3.46216e+06	-1.5617898	0	0	3	3	0	2	1	3	0101000020E61000008FB8F051994D5E401370BC7D05483F40
659	6	121.212483103	31.2813285378	329843.03	3.4621592e+06	-1.5596969	0	0	3	3	0	2	1	3	0101000020E61000009B95BA52994D5E405248A52505483F40
660	6	121.212483282	31.2813231606	329843.03	3.4621588e+06	-1.5591184	0	0	3	3	0	2	1	3	0101000020E6100000C0C87A53994D5E4065626ECB04483F40
661	6	121.212483433	31.281317741	329843.03	3.4621582e+06	-1.560496	0	0	3	3	0	2	1	3	0101000020E610000051EB1C54994D5E402D61817004483F40
662	6	121.21248357	31.2813122354	329843.03	3.4621575e+06	-1.5624565	0	0	3	3	0	2	1	3	0101000020E61000009705B054994D5E40F601231404483F40
663	6	121.212483702	31.2813065794	329843.03	3.462157e+06	-1.5642291	0	0	3	3	0	2	1	3	0101000020E610000079C13D55994D5E4031AC3EB503483F40
664	6	121.212483846	31.2813011248	329843.03	3.4621562e+06	-1.5643008	0	0	3	3	0	2	1	3	0101000020E6100000E45FD855994D5E401158BB5903483F40
665	6	121.212484029	31.2812958553	329843.06	3.4621558e+06	-1.5617694	0	0	3	3	0	2	1	3	0101000020E61000008CDE9C56994D5E408C03530103483F40
666	6	121.212484251	31.2812904917	329843.06	3.4621552e+06	-1.5578374	0	0	3	3	0	2	1	3	0101000020E6100000723D8B57994D5E40FA8656A702483F40
667	6	121.212484499	31.2812850298	329843.06	3.4621545e+06	-1.5539814	0	0	3	3	0	2	1	3	0101000020E61000002A879558994D5E406BD8B34B02483F40
668	6	121.212484758	31.2812797277	329843.1	3.462154e+06	-1.5505658	0	0	3	3	0	2	1	3	0101000020E61000008BA0AB59994D5E40D17FBFF201483F40
669	6	121.212485037	31.2812743035	329843.1	3.4621532e+06	-1.5475588	0	0	3	3	0	2	1	3	0101000020E61000007A33D75A994D5E40D8BCBE9701483F40
670	6	121.212485325	31.2812688731	329843.12	3.4621528e+06	-1.5451677	0	0	3	3	0	2	1	3	0101000020E610000051700C5C994D5E40E758A33C01483F40
671	6	121.212485605	31.2812634451	329843.16	3.4621522e+06	-1.5440826	0	0	3	3	0	2	1	3	0101000020E61000002116395D994D5E40C94392E100483F40
672	6	121.212485866	31.2812577483	329843.16	3.4621515e+06	-1.545353	0	0	3	3	0	2	1	3	0101000020E61000004355515E994D5E40F2B1FE8100483F40
673	6	121.212486085	31.2812521743	329843.16	3.462151e+06	-1.5483915	0	0	3	3	0	2	1	3	0101000020E6100000867B3C5F994D5E40228C7A2400483F40
674	6	121.212486276	31.2812464693	329843.2	3.4621502e+06	-1.5522819	0	0	3	3	0	2	1	3	0101000020E610000035910960994D5E404CC2C3C4FF473F40
675	6	121.212486419	31.2812412586	329843.2	3.4621498e+06	-1.5563134	0	0	3	3	0	2	1	3	0101000020E6100000BF1CA360994D5E400FF9576DFF473F40
676	6	121.212486534	31.2812362135	329843.2	3.4621492e+06	-1.5603099	0	0	3	3	0	2	1	3	0101000020E6100000B5971E61994D5E40F26EB318FF473F40
677	6	121.212486621	31.2812315701	329843.2	3.4621485e+06	-1.5638098	0	0	3	3	0	2	1	3	0101000020E610000015027C61994D5E40A72ECCCAFE473F40
678	6	121.212486709	31.2812257712	329843.2	3.462148e+06	-1.5677654	0	0	3	3	0	2	1	3	0101000020E6100000577FDA61994D5E40AC188269FE473F40
679	6	121.212486767	31.2812203453	329843.2	3.4621472e+06	-1.5714475	0	0	3	3	0	2	1	3	0101000020E610000042C61862994D5E4088087A0EFE473F40
680	6	121.2124868	31.2812153522	329843.16	3.4621468e+06	-1.5748469	0	0	3	3	0	2	1	3	0101000020E61000003A353C62994D5E4006D5B4BAFD473F40
681	6	121.212486812	31.2812105403	329843.16	3.4621462e+06	-1.5781299	0	0	3	3	0	2	1	3	0101000020E6100000C3174962994D5E4005E1F969FD473F40
682	6	121.212486807	31.281205857	329843.16	3.4621458e+06	-1.5812769	0	0	3	3	0	2	1	3	0101000020E610000060B94362994D5E403742671BFD473F40
683	6	121.212486789	31.2812012376	329843.16	3.4621452e+06	-1.5841277	0	0	3	3	0	2	1	3	0101000020E610000092653062994D5E403416E7CDFC473F40
684	6	121.212486751	31.281195896	329843.12	3.4621448e+06	-1.5873357	0	0	3	3	0	2	1	3	0101000020E610000036980762994D5E40E4164974FC473F40
685	6	121.212486706	31.2811912546	329843.12	3.4621442e+06	-1.5898876	0	0	3	3	0	2	1	3	0101000020E6100000B546D761994D5E409F6D6A26FC473F40
686	6	121.21248672	31.2811862845	329843.12	3.4621435e+06	-1.5879302	0	0	3	3	0	2	1	3	0101000020E6100000FF4EE661994D5E40E10208D3FB473F40
687	6	121.212486796	31.2811814911	329843.1	3.462143e+06	-1.5830383	0	1	3	3	0	2	1	3	0101000020E6100000B7E93762994D5E40D8839C82FB473F40
688	6	121.212486909	31.281176745	329843.12	3.4621425e+06	-1.5780686	0	1	3	3	0	2	1	3	0101000020E6100000EC3EB162994D5E40B52BFC32FB473F40
689	6	121.212487052	31.281171998	329843.12	3.462142e+06	-1.5726624	0	1	3	3	0	2	1	3	0101000020E610000076CA4A63994D5E4003F657E3FA473F40
690	6	121.212487226	31.2811672369	329843.12	3.4621415e+06	-1.5670877	0	1	3	3	0	2	1	3	0101000020E6100000379F0564994D5E4033317793FA473F40
691	6	121.21248747	31.2811618549	329843.12	3.462141e+06	-1.5602233	0	1	3	3	0	2	1	3	0101000020E61000006D9D0B65994D5E409EAD2B39FA473F40
692	6	121.212487732	31.2811568878	329843.16	3.4621402e+06	-1.5539	0	1	3	3	0	2	1	3	0101000020E610000070EF2466994D5E406A25D6E5F9473F40
693	6	121.212488044	31.2811516573	329843.2	3.4621398e+06	-1.5474967	0	1	3	3	0	2	1	3	0101000020E610000058F17367994D5E40D851158EF9473F40
694	6	121.212488322	31.2811471237	329843.2	3.4621392e+06	-1.5433002	0	1	3	3	0	2	1	3	0101000020E610000067719E68994D5E40EDA70542F9473F40
695	6	121.212488593	31.2811426128	329843.22	3.4621388e+06	-1.5407789	0	1	3	3	0	2	1	3	0101000020E6100000516DC169994D5E40ED7C57F6F8473F40
696	6	121.212488841	31.2811378198	329843.22	3.4621382e+06	-1.5412613	0	1	3	3	0	2	1	3	0101000020E610000009B7CB6A994D5E40B2B5EDA5F8473F40
697	6	121.21248904	31.2811326813	329843.25	3.4621378e+06	-1.5456198	0	1	3	3	0	2	1	3	0101000020E6100000BD63A16B994D5E403205B84FF8473F40
698	6	121.212489218	31.2811274027	329843.25	3.462137e+06	-1.550112	0	1	3	3	0	2	1	3	0101000020E61000000284606C994D5E401E9B28F7F7473F40
699	6	121.212489395	31.2811220664	329843.25	3.4621365e+06	-1.5531375	0	1	3	3	0	2	1	3	0101000020E610000066911E6D994D5E40375FA19DF7473F40
700	6	121.212489566	31.2811165824	329843.25	3.4621358e+06	-1.5557499	0	1	3	3	0	2	1	3	0101000020E6100000852DD66D994D5E4073C59F41F7473F40
701	6	121.212489737	31.2811108292	329843.25	3.4621352e+06	-1.5579157	0	1	3	3	0	2	1	3	0101000020E6100000A4C98D6E994D5E4027F719E1F6473F40
702	6	121.212489892	31.2811052457	329843.28	3.4621345e+06	-1.5598924	0	1	3	3	0	2	1	3	0101000020E6100000B737346F994D5E40FB036D83F6473F40
703	6	121.212490036	31.2810996454	329843.28	3.462134e+06	-1.5617609	0	1	3	3	0	2	1	3	0101000020E610000023D6CE6F994D5E4003E97725F6473F40
704	6	121.212490175	31.2810937337	329843.28	3.4621332e+06	-1.5637169	0	1	3	3	0	2	1	3	0101000020E61000002A166470994D5E401F5A49C2F5473F40
705	6	121.212490301	31.2810878254	329843.28	3.4621328e+06	-1.56567	0	1	3	3	0	2	1	3	0101000020E6100000C860EB70994D5E409265295FF5473F40
706	6	121.212490389	31.2810832971	329843.28	3.4621322e+06	-1.5671662	0	1	3	3	0	2	1	3	0101000020E610000009DE4971994D5E40117F3013F5473F40
707	6	121.212490469	31.2810787057	329843.28	3.4621318e+06	-1.568682	0	1	3	3	0	2	1	3	0101000020E610000045C49F71994D5E40619528C6F4473F40
708	6	121.212490544	31.2810739792	329843.28	3.4621312e+06	-1.5702412	0	1	3	3	0	2	1	3	0101000020E61000001D4CF071994D5E40AB6BDC76F4473F40
709	6	121.212490608	31.2810693573	329843.28	3.4621308e+06	-1.571765	0	1	3	3	0	2	1	3	0101000020E61000004C043572994D5E40E1825129F4473F40
710	6	121.212490665	31.2810647117	329843.28	3.46213e+06	-1.5732965	0	1	3	3	0	2	1	3	0101000020E610000057387272994D5E40A9CF60DBF3473F40
711	6	121.212490716	31.2810597747	329843.25	3.4621295e+06	-1.5749233	0	1	3	3	0	2	1	3	0101000020E61000001DFBA872994D5E40C18E8C88F3473F40
712	6	121.212490757	31.2810549328	329843.25	3.462129e+06	-1.5765176	0	1	3	3	0	2	1	3	0101000020E61000001B01D572994D5E4067C15037F3473F40
713	6	121.21249078514774	31.2810507559487	329843.25	3.4621285e+06	-1.5780324	0	1	3	3	0	2	1	3	0101000020E61000004C3AF372994D5E40D5503DF1F2473F40
55	9	121.211069501	31.281688518	329709.1	3.4622015e+06	-3.1081436	0	1	3	3	1	2	1	3	0101000020E610000098FEA629824D5E40FD771CBD1C483F40
206	14	121.210854144	31.2809666175	329687.28	3.4621218e+06	-1.6098561	0	1	3	3	1	2	1	3	0101000020E6100000302D61A27E4D5E40246EA16DED473F40
329	13	121.211551046	31.2807998351	329753.34	3.4621022e+06	0.011292868	0	1	3	3	1	2	1	3	0101000020E61000004B00660D8A4D5E40097A7C7FE2473F40
472	8	121.211768345	31.281506217	329775.28	3.4621802e+06	1.5724922	0	1	3	3	1	2	1	3	0101000020E61000009806D19C8D4D5E40BCA29BCA10483F40
589	5	121.212375391	31.2816145538	329833.28	3.4621912e+06	0.012198874	0	1	3	3	1	2	1	3	0101000020E61000007EB4F38E974D5E400DA632E417483F40
119	14	121.210842433	31.2814905222	329687.12	3.4621798e+06	-1.6199863	0	2	3	3	2	2	1	3	0101000020E6100000059642717E4D5E40A3FB4AC30F483F40
270	13	121.211121988	31.2807969311	329712.47	3.4621025e+06	-0.020786488	0	2	3	3	2	2	1	3	0101000020E61000004C7BCC05834D5E4044E4C34EE2473F40
385	8	121.211774651	31.2809941533	329774.97	3.4621235e+06	1.5588441	0	2	3	3	2	2	1	3	0101000020E6100000AD0A44B78D4D5E406CCA9A3BEF473F40
504	5	121.211864025	31.2816064421	329784.6	3.4621912e+06	0.024693362	0	2	3	3	2	2	1	3	0101000020E6100000BBA4202E8F4D5E4094291B5C17483F40
619	6	121.212480691	31.2815324765	329843.16	3.462182e+06	-1.6357403	0	2	3	3	2	2	1	3	0101000020E610000018B89C48994D5E4053542B8312483F40
\.


--
-- Data for Name: result_points_tbl; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.result_points_tbl (id, point, lon, lat) FROM stdin;
1	0101000020E6100000D9F7EE00884D5E4019C008491D483F40	121.21142600380644	31.281696858059238
2	0101000020E61000002D0DC4FE874D5E40B439CD481D483F40	121.211425487	31.2816968442
3	0101000020E6100000D747D5E6874D5E40648D8B461D483F40	121.211419781	31.2816967097
4	0101000020E6100000F24CEECB874D5E4001B841441D483F40	121.211413367	31.2816965733
5	0101000020E6100000CE1998AF874D5E406C360B421D483F40	121.211406611	31.2816964414
6	0101000020E610000084317398874D5E4088542F401D483F40	121.211401093	31.2816963306
7	0101000020E6100000889D4A82874D5E4016F93D3E1D483F40	121.21139581	31.2816962148
8	0101000020E610000042967768874D5E4091619D3B1D483F40	121.211389653	31.2816960582
9	0101000020E61000004E79D74D874D5E404E607F381D483F40	121.211383305	31.2816958724
10	0101000020E610000010542833874D5E4002C109351D483F40	121.211376943	31.2816956662
11	0101000020E610000038C54315874D5E40DCF0B6301D483F40	121.211369816	31.2816954085
12	0101000020E61000000E3F02F6864D5E408461D02B1D483F40	121.211362364	31.2816951164
13	0101000020E6100000B5A20AD5864D5E40D78F71261D483F40	121.211354504	31.2816947963
14	0101000020E61000004E2F75B3864D5E40C426C7201D483F40	121.211346497	31.2816944586
15	0101000020E6100000C9CEE091864D5E40144E191B1D483F40	121.211338491	31.2816941201
16	0101000020E610000015526A6E864D5E40D6FB55151D483F40	121.211330036	31.2816937766
17	0101000020E61000007ACD7849864D5E400009BB0F1D483F40	121.211321228	31.2816934425
18	0101000020E610000083CCA523864D5E4092AD6B0A1D483F40	121.21131221	31.281693126
19	0101000020E6100000CD9978FD854D5E4039BA96051D483F40	121.211303108	31.281692838
20	0101000020E6100000D68B7BD5854D5E4046FA1E011D483F40	121.211293574	31.2816925717
21	0101000020E6100000344F2FAA854D5E40A3ADF3FC1C483F40	121.211283251	31.2816923232
22	0101000020E6100000611CAD81854D5E406E06B2F91C483F40	121.211273593	31.2816921291
23	0101000020E610000000D2396A854D5E40054918F81C483F40	121.211268002	31.2816920337
24	0101000020E610000027FB843F854D5E409D40B4F51C483F40	121.21125782	31.2816918912
25	0101000020E610000018A86E29854D5E40226DB9F41C483F40	121.211252554	31.2816918328
26	0101000020E610000057E9BE01854D5E40661C60F31C483F40	121.211243092	31.2816917524
27	0101000020E6100000837F49DE844D5E404D49A8F21C483F40	121.211234638	31.2816917096
28	0101000020E6100000E6B568BA844D5E405B0773F21C483F40	121.211226084	31.2816916972
29	0101000020E6100000CFAAF79D844D5E402198B2F21C483F40	121.211219303	31.281691712
30	0101000020E610000027903A83844D5E40F7C140F31C483F40	121.211212928	31.2816917451
31	0101000020E61000005C059E6C844D5E401095F8F31C483F40	121.211207537	31.2816917879
32	0101000020E610000088201851844D5E40AEF226F51C483F40	121.211200975	31.2816918583
33	0101000020E610000025261A37844D5E4012E194F61C483F40	121.211194778	31.2816919435
34	0101000020E61000008B476A20844D5E40F383FEF71C483F40	121.211189369	31.2816920277
35	0101000020E6100000876CAE07844D5E40B79DA8F71C483F40	121.211183472	31.2816920077
36	0101000020E6100000538866EF834D5E4010B5C9F61C483F40	121.211177683	31.2816919558
37	0101000020E61000007B2000D8834D5E403FABAEF51C483F40	121.211172104	31.2816918899
38	0101000020E6100000B21E70BD834D5E40AAD830F41C483F40	121.211165771	31.281691801
39	0101000020E610000060CF379F834D5E40904319F21C483F40	121.211158566	31.2816916763
40	0101000020E6100000A9AC1589834D5E4081B047F01C483F40	121.211153289	31.2816915679
41	0101000020E610000046BF4171834D5E4011E50FEE1C483F40	121.211147608	31.2816914357
42	0101000020E610000069AD7C57834D5E405A695FEB1C483F40	121.211141464	31.2816912754
43	0101000020E610000022B9FF3F834D5E408432A7E81C483F40	121.211135864	31.2816911133
44	0101000020E6100000C16E8C28834D5E40C94BB9E51C483F40	121.211130273	31.2816909387
45	0101000020E6100000122E8E11834D5E404E07A6E21C483F40	121.211124791	31.2816907554
46	0101000020E6100000ABC722F2824D5E40491E26DE1C483F40	121.2111173	31.2816904872
47	0101000020E6100000402F1ED6824D5E40CCEBE7D91C483F40	121.21111062	31.2816902343
48	0101000020E61000008D3E95BC824D5E40E8FCE1D51C483F40	121.211104532	31.2816899945
49	0101000020E610000019867BA4824D5E40D2FD00D21C483F40	121.211098786	31.2816897633
50	0101000020E6100000DC74F48D824D5E4049B058CE1C483F40	121.211093415	31.2816895453
51	0101000020E61000002AD5B273824D5E40366DFCC91C483F40	121.211087155	31.2816892854
52	0101000020E610000005C2DC5B824D5E404DE7EDC51C483F40	121.211081472	31.2816890436
53	0101000020E610000086C85B41824D5E40C07F4FC11C483F40	121.211075153	31.2816887683
54	0101000020E6100000AD15DA2A824D5E408B2955BD1C483F40	121.211069787	31.2816885312
55	0101000020E610000098FEA629824D5E40FD771CBD1C483F40	121.211069501	31.281688518
56	0101000020E610000072A0B512824D5E40DC25AEB91C483F40	121.211064031	31.2816883135
57	0101000020E61000007204BDF8814D5E40745BEAB51C483F40	121.211057839	31.2816880891
58	0101000020E61000002760B5DE814D5E4030347EB11C483F40	121.211051633	31.2816878255
59	0101000020E61000008E74DBC5814D5E40290C8CAC1C483F40	121.211045708	31.2816875307
60	0101000020E610000002464BA9814D5E407EB73FA61C483F40	121.211038898	31.2816871553
61	0101000020E61000004A60488E814D5E40C4C9A49F1C483F40	121.211032458	31.2816867616
62	0101000020E61000007E773A76814D5E40E11EBC981C483F40	121.211026723	31.2816863498
63	0101000020E6100000A6BE8C5F814D5E40A1F7F1901C483F40	121.211021316	31.2816858855
64	0101000020E610000026D86147814D5E40152B35871C483F40	121.211015554	31.2816853051
65	0101000020E61000007CD0FA2E814D5E408F4C457B1C483F40	121.211009736	31.2816845936
66	0101000020E6100000CA8E2A16814D5E4024662B6C1C483F40	121.21100382	31.2816836935
67	0101000020E6100000626293FF804D5E4062D25B5C1C483F40	121.210998434	31.2816827511
68	0101000020E61000008456FAE5804D5E40FAC595481C483F40	121.210992331	31.2816815725
69	0101000020E61000008124CBCD804D5E4029E311341C483F40	121.210986565	31.2816803497
70	0101000020E6100000499624B3804D5E407EAFDF1B1C483F40	121.210980211	31.2816789075
71	0101000020E61000006C40429C804D5E40BB90A3051C483F40	121.210974755	31.2816775822
72	0101000020E6100000BBA7FE83804D5E40CDCC6BEC1B483F40	121.21096897	31.2816760791
73	0101000020E61000004D287C6A804D5E404DCCECCF1B483F40	121.210962888	31.2816743806
74	0101000020E610000035306150804D5E40B5EB8BB01B483F40	121.210956664	31.2816725103
75	0101000020E610000093ACAC35804D5E40CF07C78D1B483F40	121.210950297	31.2816704379
76	0101000020E6100000DFB4251A804D5E400B6816671B483F40	121.210943734	31.2816681318
77	0101000020E610000034C01404804D5E406EBE06461B483F40	121.210938473	31.2816661612
78	0101000020E61000001F7884EC7F4D5E40D1FAA2201B483F40	121.210932855	31.2816639326
79	0101000020E61000006B490AD67F4D5E401E07E8FA1A483F40	121.210927496	31.2816616837
80	0101000020E610000005C6FFBF7F4D5E400C58D9D31A483F40	121.210922241	31.2816593557
81	0101000020E6100000F7BD04A97F4D5E40D248BEA81A483F40	121.210916762	31.2816567864
82	0101000020E6100000C832D4937F4D5E405DB1BE7E1A483F40	121.21091171	31.2816542831
83	0101000020E6100000286CD47D7F4D5E405E5581501A483F40	121.210906465	31.281651527
84	0101000020E6100000BFF421687F4D5E407FD3EC1F1A483F40	121.210901292	31.2816486314
85	0101000020E61000001D2D50507F4D5E401CA6E0E619483F40	121.210895613	31.2816452311
86	0101000020E610000095C2723A7F4D5E40DCB1C0AE19483F40	121.2108904	31.2816418858
87	0101000020E61000002124AD267F4D5E40F9E83A7819483F40	121.210885686	31.281638636
88	0101000020E61000000C47B8127F4D5E40F812363D19483F40	121.210880928	31.2816351182
89	0101000020E610000075CD77FF7E4D5E4052869AFF18483F40	121.210876338	31.2816314461
90	0101000020E61000006D76D9EC7E4D5E40A467A1BE18483F40	121.210871899	31.2816275734
91	0101000020E6100000ACE886DB7E4D5E406ED0067C18483F40	121.210867769	31.2816236035
92	0101000020E610000036BB88CB7E4D5E40E0DCFD3718483F40	121.210863956	31.2816195483
93	0101000020E610000027ED61BD7E4D5E40EDDBE5F417483F40	121.210860582	31.2816155492
94	0101000020E6100000C2E81AAF7E4D5E40FA5B5BA817483F40	121.210857178	31.281610987
95	0101000020E61000006A19E3A27E4D5E40D5F1435D17483F40	121.210854265	31.2816065112
96	0101000020E61000007BA356987E4D5E400CF1661217483F40	121.21085175	31.281602049
97	0101000020E6100000BB43808F7E4D5E40F6B532C916483F40	121.210849643	31.2815976857
98	0101000020E61000005DF793877E4D5E4076D5B67B16483F40	121.210847754	31.2815930673
99	0101000020E610000033AFD9807E4D5E406836D82B16483F40	121.21084615	31.2815883067
100	0101000020E61000001D847E7B7E4D5E404367A2DB15483F40	121.210844873	31.2815835258
101	0101000020E61000005A4A54777E4D5E4052D9BD8715483F40	121.21084388	31.2815785254
102	0101000020E61000008B408A747E4D5E40F486F03115483F40	121.210843215	31.2815734112
103	0101000020E6100000010638737E4D5E4022D4E4E014483F40	121.2108429	31.2815685805
104	0101000020E6100000DD7B04737E4D5E406F20FE8C14483F40	121.210842852	31.2815635796
105	0101000020E61000007F63C0737E4D5E40212A0F3E14483F40	121.210843027	31.2815588748
106	0101000020E6100000B80427757E4D5E400349A6EB13483F40	121.210843361	31.2815539628
107	0101000020E6100000D0BEBA767E4D5E40BE929D9B13483F40	121.210843737	31.2815491924
108	0101000020E61000004ECBFB777E4D5E4089A6594813483F40	121.210844036	31.2815442294
109	0101000020E61000004BD753787E4D5E402935EBF412483F40	121.210844118	31.2815392565
110	0101000020E6100000BC5D3E787E4D5E40F9ECD9A312483F40	121.210844098	31.2815344245
111	0101000020E61000006384BD777E4D5E40F41CC95012483F40	121.210843978	31.2815294734
112	0101000020E610000088590C777E4D5E40F322910112483F40	121.210843813	31.2815247516
113	0101000020E6100000516132767E4D5E4002D9C9AA11483F40	121.21084361	31.2815195792
114	0101000020E6100000783055757E4D5E40745D9F5211483F40	121.210843404	31.2815143241
115	0101000020E61000000E8C8E747E4D5E4028C6F30611483F40	121.210843219	31.2815098138
116	0101000020E610000087F49C737E4D5E40070805AD10483F40	121.210842994	31.2815044534
117	0101000020E6100000883FB8727E4D5E404A14385A10483F40	121.210842781	31.2814995181
118	0101000020E61000005147DE717E4D5E403E6D7C0110483F40	121.210842578	31.2814942292
119	0101000020E6100000059642717E4D5E40A3FB4AC30F483F40	121.210842433	31.2814905222
120	0101000020E6100000FE55AD707E4D5E40E3247B6A0F483F40	121.210842294	31.2814852286
121	0101000020E610000033F641707E4D5E409D4177150F483F40	121.210842194	31.2814801613
122	0101000020E61000004218FB6F7E4D5E4022D2B5C60E483F40	121.210842128	31.2814754671
123	0101000020E6100000BB2FC26F7E4D5E402DE8D8610E483F40	121.210842075	31.2814694552
124	0101000020E6100000AF01B16F7E4D5E4088CB57ED0D483F40	121.210842059	31.281462511
125	0101000020E610000019F7BE6F7E4D5E4008BDB0950D483F40	121.210842072	31.2814572865
126	0101000020E6100000B39EE56F7E4D5E40AC6883350D483F40	121.210842108	31.2814515539
127	0101000020E61000009FE523707E4D5E4070FC58DA0C483F40	121.210842166	31.28144612
128	0101000020E6100000A71F8D707E4D5E405799ED6E0C483F40	121.210842264	31.2814397173
129	0101000020E6100000267D15717E4D5E4042C190060C483F40	121.210842391	31.2814334968
130	0101000020E610000082F9F6717E4D5E404B136B7F0B483F40	121.210842601	31.2814254414
131	0101000020E6100000FE62D7727E4D5E402CAE5B100B483F40	121.21084281	31.2814188217
132	0101000020E6100000D98D88737E4D5E40E8CC59C20A483F40	121.210842975	31.2814141721
133	0101000020E61000001F57D4747E4D5E40CC8E833C0A483F40	121.210843284	31.2814061948
134	0101000020E61000007B826E767E4D5E40441EF7A409483F40	121.210843666	31.2813971618
135	0101000020E6100000793753777E4D5E40096A345509483F40	121.210843879	31.2813924077
136	0101000020E6100000206BFC787E4D5E40A986AFC608483F40	121.210844275	31.2813839129
137	0101000020E6100000F20AFD797E4D5E40CDC4337308483F40	121.210844514	31.2813789369
138	0101000020E6100000F656EA7A7E4D5E40D0550A2708483F40	121.210844735	31.2813743973
139	0101000020E610000069E4D27C7E4D5E40358F7A8A07483F40	121.21084519	31.2813650655
140	0101000020E610000008879F7E7E4D5E4076C276F006483F40	121.210845619	31.2813558855
141	0101000020E6100000CEF88E7F7E4D5E40A3DCF29B06483F40	121.210845842	31.281350848
142	0101000020E6100000F3DA07817E4D5E40F120610B06483F40	121.210846193	31.281342231
143	0101000020E6100000C28660827E4D5E4003B0EF7605483F40	121.210846514	31.2813333831
144	0101000020E61000000A4A80837E4D5E40E7E57CE504483F40	121.210846782	31.2813247137
145	0101000020E6100000D74C4C847E4D5E40D720826004483F40	121.210846972	31.2813167875
146	0101000020E6100000D35ED0847E4D5E408C0DC8E303483F40	121.210847095	31.2813093532
147	0101000020E610000008AE1D857E4D5E408F0FE96503483F40	121.210847167	31.2813018507
148	0101000020E6100000B61432857E4D5E405C0A3CF602483F40	121.210847186	31.2812951943
149	0101000020E61000008BF921857E4D5E406E893C8702483F40	121.210847171	31.2812885783
150	0101000020E6100000938AFE847E4D5E402CB0D22402483F40	121.210847138	31.2812827124
151	0101000020E61000005013CC847E4D5E4084FACFBF01483F40	121.210847091	31.2812766917
152	0101000020E6100000967EA6847E4D5E40FE85966401483F40	121.210847056	31.2812712543
153	0101000020E6100000AFD49C847E4D5E403CE7E90B01483F40	121.210847047	31.2812659689
154	0101000020E6100000FF73B4847E4D5E402B408CB600483F40	121.210847069	31.2812608807
155	0101000020E6100000103FFA847E4D5E4069A7754200483F40	121.210847134	31.2812539613
156	0101000020E6100000386662857E4D5E40E2C46ED7FF473F40	121.210847231	31.281247582
157	0101000020E61000007252E4857E4D5E40759EFD74FF473F40	121.210847352	31.2812417144
158	0101000020E610000073FB70867E4D5E40572B7B18FF473F40	121.210847483	31.2812362004
159	0101000020E61000005020F6867E4D5E40177514C5FE473F40	121.210847607	31.2812312293
160	0101000020E610000026AE72877E4D5E40B3BFE677FE473F40	121.210847723	31.2812266291
161	0101000020E6100000EF130A887E4D5E40B4FA4119FE473F40	121.210847864	31.2812209879
162	0101000020E61000002F9794887E4D5E4057C845C6FD473F40	121.210847993	31.2812160416
163	0101000020E61000002AA918897E4D5E40BDC3CA78FD473F40	121.210848116	31.2812114234
164	0101000020E6100000E7E09E897E4D5E4058918A2CFD473F40	121.210848241	31.2812068785
165	0101000020E610000033923A8A7E4D5E408A059FDEFC473F40	121.210848386	31.2812022341
166	0101000020E610000032471F8B7E4D5E40854B807EFC473F40	121.210848599	31.2811965049
167	0101000020E610000044BBF18B7E4D5E4007EE932EFC473F40	121.210848795	31.2811917411
168	0101000020E61000004E9EE78C7E4D5E405ABE0EDAFB473F40	121.210849024	31.2811867033
169	0101000020E6100000E069168E7E4D5E40CA161982FB473F40	121.210849306	31.2811814605
170	0101000020E610000076D2798F7E4D5E4089F5162BFB473F40	121.210849637	31.2811762744
171	0101000020E6100000DF2B25917E4D5E40984631CFFA473F40	121.210850035	31.2811707969
172	0101000020E610000070A60C937E4D5E405773446BFA473F40	121.210850489	31.2811648409
173	0101000020E6100000C1A395947E4D5E40FF93331BFA473F40	121.210850855	31.2811600686
174	0101000020E610000091F87A967E4D5E40D85BDBB7F9473F40	121.210851307	31.2811541472
175	0101000020E6100000E0FB2F987E4D5E40E3D61C58F9473F40	121.210851714	31.2811484404
176	0101000020E610000045B27A997E4D5E4077D3AE04F9473F40	121.210852022	31.2811434676
177	0101000020E6100000E4A58E9A7E4D5E406A6748B8F8473F40	121.210852279	31.2811389138
178	0101000020E61000004B56AD9B7E4D5E406F997D65F8473F40	121.210852546	31.281133979
179	0101000020E61000005CD0AB9C7E4D5E405C389D17F8473F40	121.210852783	31.2811293372
180	0101000020E6100000320DE19D7E4D5E409AE705B2F7473F40	121.210853071	31.2811232819
181	0101000020E61000003CF0D69E7E4D5E40F0B7225AF7473F40	121.2108533	31.2811180434
182	0101000020E61000001427E09F7E4D5E40B44E43F1F6473F40	121.210853547	31.2811117925
183	0101000020E610000053B096A07E4D5E40E069EFA1F6473F40	121.210853717	31.2811070642
184	0101000020E61000006CB545A17E4D5E407FDF5C4FF6473F40	121.21085388	31.2811021425
185	0101000020E61000009E10EBA17E4D5E4045C67BFAF5473F40	121.210854034	31.2810970833
186	0101000020E61000002F338DA27E4D5E40D0165C9FF5473F40	121.210854185	31.2810916519
187	0101000020E6100000FE2F2DA37E4D5E40B55D5C3BF5473F40	121.210854334	31.2810856915
188	0101000020E6100000A7A8C5A37E4D5E406FF236D2F4473F40	121.210854476	31.2810794243
189	0101000020E610000007194FA47E4D5E40C7D71568F4473F40	121.210854604	31.2810730985
190	0101000020E6100000479CD9A47E4D5E40AD447DEBF3473F40	121.210854733	31.281065672
191	0101000020E6100000F20E46A57E4D5E402A28F375F3473F40	121.210854834	31.2810586661
192	0101000020E6100000B040A0A57E4D5E40CE2426FBF2473F40	121.210854918	31.2810513466
193	0101000020E6100000A11EE7A57E4D5E40C47AF378F2473F40	121.210854984	31.2810435862
194	0101000020E6100000813714A67E4D5E4005ACD4F1F1473F40	121.210855026	31.2810355324
195	0101000020E6100000AC5224A67E4D5E40E3556875F1473F40	121.210855041	31.2810281162
196	0101000020E6100000C5A81AA67E4D5E404C8237EDF0473F40	121.210855032	31.2810199986
197	0101000020E6100000CD39F7A57E4D5E40C1BF786EF0473F40	121.210854999	31.281012444
198	0101000020E6100000E789C1A57E4D5E4048164DFBEF473F40	121.210854949	31.2810055793
199	0101000020E6100000547377A57E4D5E4073D3A090EF473F40	121.21085488	31.2809992211
200	0101000020E61000009BD825A57E4D5E409D36E535EF473F40	121.210854804	31.280993813
201	0101000020E61000003B6EC8A47E4D5E40AC5D36E1EE473F40	121.210854717	31.2809887655
202	0101000020E610000076A565A47E4D5E40A0F1CF94EE473F40	121.210854625	31.2809842117
203	0101000020E6100000308BD2A37E4D5E408EE35133EE473F40	121.210854488	31.2809784007
204	0101000020E6100000A06830A37E4D5E40E7DE09D6ED473F40	121.210854337	31.2809728407
205	0101000020E6100000F0588FA27E4D5E40A5AB9083ED473F40	121.210854187	31.2809679249
206	0101000020E6100000302D61A27E4D5E40246EA16DED473F40	121.210854144	31.2809666175
207	0101000020E610000010468EA27E4D5E4024022117ED473F40	121.210854186	31.2809614616
208	0101000020E6100000754D20A37E4D5E40230B34BAEC473F40	121.210854322	31.2809559228
209	0101000020E61000003CB9E3A37E4D5E40FB3B5C6DEC473F40	121.210854504	31.2809513426
210	0101000020E61000005DFE27A57E4D5E40CB540710EC473F40	121.210854806	31.2809457796
211	0101000020E610000054D1E8A67E4D5E402CD96DABEB473F40	121.210855224	31.2809397834
212	0101000020E610000064FA9FA87E4D5E406787EB5DEB473F40	121.210855633	31.2809351635
213	0101000020E610000047C357AB7E4D5E407D61B4FAEA473F40	121.210856281	31.2809292498
214	0101000020E6100000A9E36BAE7E4D5E4070B47BA3EA473F40	121.210857015	31.280924051
215	0101000020E6100000071604B27E4D5E409DE84B53EA473F40	121.210857872	31.2809192715
216	0101000020E6100000766B27B77E4D5E409ED90CF9E9473F40	121.210859097	31.2809138924
217	0101000020E610000048257CBC7E4D5E40791EAEAEE9473F40	121.210860368	31.2809094596
218	0101000020E6100000B5E468C37E4D5E403DA20E5FE9473F40	121.210862019	31.2809047137
219	0101000020E6100000AFD24FCB7E4D5E4048345911E9473F40	121.210863903	31.2809000819
220	0101000020E6100000859474D47E4D5E40CAA5F6C4E8473F40	121.210866083	31.280895529
221	0101000020E6100000C10CE4DE7E4D5E407AFB017CE8473F40	121.210868571	31.2808911805
222	0101000020E6100000CCD938EB7E4D5E40D8C94730E8473F40	121.210871511	31.2808866668
223	0101000020E6100000D4B811F87E4D5E4057104AE9E7473F40	121.210874574	31.2808824354
224	0101000020E6100000A6489D047F4D5E404B56CEA9E7473F40	121.210877565	31.2808786515
225	0101000020E61000000C47B8127F4D5E4083517667E7473F40	121.210880928	31.2808746971
226	0101000020E6100000128A2E217F4D5E402D442127E7473F40	121.210884376	31.2808708626
227	0101000020E610000039705A327F4D5E4020949DDDE6473F40	121.21088847	31.2808664808
228	0101000020E610000033E330427F4D5E40CE042D9EE6473F40	121.210892246	31.2808626995
229	0101000020E6100000208C93537F4D5E406CC9C65DE6473F40	121.210896391	31.280858861
230	0101000020E6100000CCB16B647F4D5E40AAE06623E6473F40	121.210900407	31.2808553816
231	0101000020E6100000AAE7CD767F4D5E40A58C7DE7E5473F40	121.21090479	31.2808518106
232	0101000020E6100000949C88887F4D5E40184B5FB0E5473F40	121.210909017	31.2808485253
233	0101000020E610000088D6C7997F4D5E403B39B37CE5473F40	121.210913129	31.2808454454
234	0101000020E6100000AD2091AC7F4D5E4076188E45E5473F40	121.210917608	31.2808421585
235	0101000020E6100000BCC4EEC17F4D5E4046A36809E5473F40	121.210922702	31.2808385735
236	0101000020E6100000627A3CD97F4D5E40FA91DCCAE4473F40	121.210928258	31.2808348454
237	0101000020E6100000168F62EB7F4D5E40F7923E9CE4473F40	121.210932585	31.2808320668
238	0101000020E61000002BCAC800804D5E40EC7D8367E4473F40	121.210937687	31.2808289238
239	0101000020E610000076F60A15804D5E40E62CCC37E4473F40	121.210942517	31.2808260797
240	0101000020E61000009BFCB62A804D5E4002781D07E4473F40	121.210947684	31.280823178
241	0101000020E61000002BF24440804D5E40EE621ED9E3473F40	121.210952823	31.2808204364
242	0101000020E6100000C378AF55804D5E40E7FFEFADE3473F40	121.210957929	31.2808178626
243	0101000020E6100000501C246A804D5E40E346CF86E3473F40	121.210962806	31.2808155304
244	0101000020E61000008143B77D804D5E40673F4563E3473F40	121.210967473	31.2808134121
245	0101000020E61000002E9D3797804D5E401EFBC937E3473F40	121.210973553	31.2808108204
246	0101000020E61000005A9F5BB0804D5E40318B2410E3473F40	121.210979547	31.2808084573
247	0101000020E61000004DB5FDC8804D5E40D2FF2FECE2473F40	121.21098542	31.2808063142
248	0101000020E6100000EED342E0804D5E4071FED5CCE2473F40	121.210990968	31.2808044455
249	0101000020E61000003276A6F6804D5E40F14821B1E2473F40	121.210996306	31.2808027941
250	0101000020E6100000E05E5F0C814D5E4017897598E2473F40	121.211001485	31.2808013236
251	0101000020E6100000FE82E722814D5E40E02D4781E2473F40	121.211006857	31.2807999419
252	0101000020E6100000539F493A814D5E40D94CBD6BE2473F40	121.211012432	31.2807986581
253	0101000020E61000006A976454814D5E40ED6AB656E2473F40	121.211018656	31.2807974048
254	0101000020E61000009634196A814D5E406E93B747E2473F40	121.211023831	31.280796511
255	0101000020E6100000AD705181814D5E40D401833AE2473F40	121.211029367	31.2807957239
256	0101000020E6100000D47DFB98814D5E4020405530E2473F40	121.211035009	31.2807951172
257	0101000020E6100000206D1EB2814D5E40AB8B9D29E2473F40	121.211041002	31.2807947168
258	0101000020E6100000FFD0FCCC814D5E406D086427E2473F40	121.211047408	31.2807945842
259	0101000020E610000078EF57EA814D5E40D08C662AE2473F40	121.211054407	31.2807947636
260	0101000020E61000006FBC8207824D5E404B87DF31E2473F40	121.211061361	31.280795209
261	0101000020E6100000A17C2D27824D5E40F6BD5C3CE2473F40	121.211068911	31.2807958342
262	0101000020E6100000514F4548824D5E4050DE2C46E2473F40	121.211076801	31.2807964191
263	0101000020E61000007F8F5A5E824D5E40400EEB4AE2473F40	121.211082066	31.2807967018
264	0101000020E6100000835DEC74824D5E40DABCBE4EE2473F40	121.211087447	31.2807969299
265	0101000020E6100000B63A368B824D5E40850A5E51E2473F40	121.211092761	31.2807970862
266	0101000020E610000076A4EBA3824D5E4095EE7652E2473F40	121.211098652	31.2807971516
267	0101000020E6100000554CE7BB824D5E4079271752E2473F40	121.21110437	31.2807971293
268	0101000020E6100000BF99D0D8824D5E403E5AE550E2473F40	121.211111263	31.2807970581
269	0101000020E6100000B0C2C8F3824D5E4010BBC44FE2473F40	121.211117693	31.2807969909
270	0101000020E61000004C7BCC05834D5E4044E4C34EE2473F40	121.211121988	31.2807969311
271	0101000020E6100000EEA00F1F834D5E409D4C2C4DE2473F40	121.211128011	31.2807968362
272	0101000020E610000086F08639834D5E4031EC314BE2473F40	121.211134321	31.2807967183
273	0101000020E61000005EB65E52834D5E40FF960949E2473F40	121.211140244	31.2807965897
274	0101000020E6100000373F1770834D5E40381E1C46E2473F40	121.21114733	31.2807964152
275	0101000020E610000074FF5687834D5E40BC88C143E2473F40	121.211152873	31.2807962749
276	0101000020E6100000D3AD67A0834D5E400ADC3C41E2473F40	121.211158849	31.2807961248
277	0101000020E6100000BD96CABA834D5E40D647A23EE2473F40	121.21116514	31.2807959696
278	0101000020E6100000D78FB7D6834D5E405E0ADE3BE2473F40	121.211171798	31.2807958047
279	0101000020E61000006BBABFF6834D5E409921AA38E2473F40	121.211179435	31.2807956138
280	0101000020E610000059B81C17844D5E4063B28B35E2473F40	121.211187151	31.2807954279
281	0101000020E6100000B35B123A844D5E4032768732E2473F40	121.211195486	31.2807952481
282	0101000020E6100000FED5505B844D5E409F840A30E2473F40	121.211203412	31.2807950998
283	0101000020E61000004EA37A7F844D5E406FF7BE2DE2473F40	121.211212034	31.280794963
284	0101000020E6100000260896A4844D5E40B7C6D82BE2473F40	121.211220881	31.2807948498
285	0101000020E6100000AA7B80C8844D5E407F89602AE2473F40	121.211229444	31.2807947622
286	0101000020E61000009B9403EF844D5E402132FB28E2473F40	121.211238626	31.280794679
287	0101000020E6100000ED682B15854D5E405254AB27E2473F40	121.211247723	31.2807946008
288	0101000020E61000005E7B993A854D5E4031AB7826E2473F40	121.211256647	31.2807945294
289	0101000020E6100000DD135E61854D5E40C4315A25E2473F40	121.21126589	31.2807944627
290	0101000020E6100000D6B79187854D5E4082054324E2473F40	121.211274998	31.2807943977
291	0101000020E6100000A1F4C7AC854D5E4086AE2023E2473F40	121.21128387	31.2807943301
292	0101000020E6100000100D62D2854D5E4038F00922E2473F40	121.211292835	31.2807942652
293	0101000020E6100000DE3D40F7854D5E4097022221E2473F40	121.211301625	31.2807942112
294	0101000020E61000009613CE1D864D5E40C2A07020E2473F40	121.211310817	31.2807941699
295	0101000020E6100000CD39BA42864D5E4072015920E2473F40	121.21131962	31.2807941644
296	0101000020E6100000928EA068864D5E40B84DE320E2473F40	121.211328656	31.2807941966
297	0101000020E6100000D397828E864D5E40C00A3A22E2473F40	121.211337688	31.2807942764
298	0101000020E6100000100430B4864D5E404FF56724E2473F40	121.211346671	31.2807944063
299	0101000020E61000006BB44FD9864D5E4081F94827E2473F40	121.211355522	31.2807945779
300	0101000020E6100000232C6CFE864D5E408AEF942AE2473F40	121.21136437	31.2807947744
301	0101000020E610000038648721874D5E403DAFC02DE2473F40	121.21137274	31.2807949634
302	0101000020E6100000EC756241874D5E4054C7C530E2473F40	121.211380335	31.2807951434
303	0101000020E6100000E6D8C35C874D5E401E6C7E33E2473F40	121.211386863	31.2807953056
304	0101000020E6100000CE25C478874D5E40A76E5C36E2473F40	121.211393539	31.2807954765
305	0101000020E61000006FC9F897874D5E40EE3EA639E2473F40	121.211400979	31.2807956725
306	0101000020E6100000ADE17DB0874D5E401CD8493CE2473F40	121.211406825	31.2807958298
307	0101000020E6100000524CB0C8874D5E40E6E0F03EE2473F40	121.211412594	31.2807959879
308	0101000020E610000066EBB3DF874D5E40604A8041E2473F40	121.211418081	31.2807961405
309	0101000020E61000007F7207F6874D5E402B1B0444E2473F40	121.211423404	31.2807962904
310	0101000020E6100000CEA6190E884D5E40AF4EB646E2473F40	121.211429143	31.2807964511
311	0101000020E610000080F51329884D5E407ACEF249E2473F40	121.211435575	31.280796644
312	0101000020E61000000B884147884D5E40C5419D4DE2473F40	121.21144277	31.2807968625
313	0101000020E610000000FD2663884D5E40E6930B51E2473F40	121.211449421	31.280797067
314	0101000020E610000071B46E79884D5E4044ADD053E2473F40	121.211454733	31.2807972321
315	0101000020E6100000C32D7090884D5E4038D7B356E2473F40	121.211460218	31.2807974042
316	0101000020E6100000FA63D1A9884D5E40F728DF59E2473F40	121.211466269	31.2807975931
317	0101000020E610000074C01DC4884D5E40E1C7115DE2473F40	121.211472539	31.2807977837
318	0101000020E6100000ADAC35E0884D5E402F2F6460E2473F40	121.211479237	31.2807979817
319	0101000020E61000004FECCCFD884D5E407537D163E2473F40	121.211486292	31.2807981859
320	0101000020E61000008C82431C894D5E4025674367E2473F40	121.211493555	31.2807983913
321	0101000020E6100000C7383A3F894D5E40DED01E6BE2473F40	121.211501891	31.2807986212
322	0101000020E6100000E3A3C060894D5E407414B56EE2473F40	121.211509884	31.280798835
323	0101000020E6100000BB3FCF80894D5E4000560572E2473F40	121.211517527	31.2807990325
324	0101000020E6100000D783D79B894D5E4021F9BA74E2473F40	121.211523972	31.280799194
325	0101000020E6100000CDEB92B5894D5E40BA814077E2473F40	121.211530107	31.2807993443
326	0101000020E6100000A3AA6CCC894D5E40B3937379E2473F40	121.211535555	31.2807994754
327	0101000020E6100000DF7D02E6894D5E409FE7DB7BE2473F40	121.211541655	31.2807996189
328	0101000020E61000000DCB41FE894D5E4046921A7EE2473F40	121.211547436	31.2807997527
329	0101000020E61000004B00660D8A4D5E40097A7C7FE2473F40	121.211551046	31.2807998351
330	0101000020E610000014A484268A4D5E4067D1E180E2473F40	121.211557035	31.2807999183
331	0101000020E61000007B34B93E8A4D5E40D4AEEE81E2473F40	121.211562806	31.2807999809
332	0101000020E610000039F5E1568A4D5E4090ECA082E2473F40	121.211568566	31.2808000224
333	0101000020E610000061D296758A4D5E407904C990E2473F40	121.211575887	31.2808008662
334	0101000020E6100000844F368F8A4D5E402E95809EE2473F40	121.211581996	31.2808016838
335	0101000020E61000002DC238AB8A4D5E40FE8F56B0E2473F40	121.211588674	31.2808027469
336	0101000020E6100000DD36DFCA8A4D5E400B7901C8E2473F40	121.21159622	31.2808041576
337	0101000020E6100000987B25E98A4D5E4038D996E2E2473F40	121.211603438	31.2808057421
338	0101000020E610000005CAE0078B4D5E4089544902E3473F40	121.211610765	31.2808076314
339	0101000020E6100000DE5FC3278B4D5E40B28A7327E3473F40	121.211618367	31.2808098466
340	0101000020E6100000DE3FD93E8B4D5E4019C56445E3473F40	121.211623871	31.2808116313
341	0101000020E6100000E48C2E5D8B4D5E40C1CAB070E3473F40	121.211631103	31.280814212
342	0101000020E6100000D36A0B798B4D5E400F8D109DE3473F40	121.211637746	31.2808168569
343	0101000020E610000009B4C2948B4D5E40D77A5FCDE3473F40	121.211644354	31.2808197363
344	0101000020E6100000219FB3AC8B4D5E404DE1E8FAE3473F40	121.211650062	31.2808224505
345	0101000020E610000064988AC28B4D5E40EEE5C027E4473F40	121.211655269	31.2808251234
346	0101000020E610000024E8EBD68B4D5E4083589954E4473F40	121.211660128	31.2808277964
347	0101000020E6100000783B41E98B4D5E407F55937FE4473F40	121.211664499	31.280830358
348	0101000020E6100000AC63A6FE8B4D5E40318B50B5E4473F40	121.2116696	31.2808335611
349	0101000020E610000018194A118C4D5E40BD9F73E7E4473F40	121.211674044	31.2808365495
350	0101000020E6100000B4DE77258C4D5E4019B96421E5473F40	121.211678855	31.2808400031
351	0101000020E61000000A7DE8368C4D5E40D4DDC056E5473F40	121.211683013	31.2808431836
352	0101000020E6100000FA19F3478C4D5E401E12C78DE5473F40	121.211687076	31.2808464633
353	0101000020E61000007A88585A8C4D5E408A9390CBE5473F40	121.211691462	31.2808501461
354	0101000020E61000002BB6006F8C4D5E4042DB4D13E6473F40	121.211696387	31.2808544221
355	0101000020E610000066A6357F8C4D5E40754A984DE6473F40	121.211700251	31.2808578965
356	0101000020E6100000259127908C4D5E4012AAF48CE6473F40	121.211704291	31.2808616731
357	0101000020E6100000AF26A0A08C4D5E40CA08D4CCE6473F40	121.211708218	31.2808654802
358	0101000020E610000071B4F2B18C4D5E4009831312E7473F40	121.211712348	31.2808696077
359	0101000020E6100000766204C48C4D5E403CABF55CE7473F40	121.211716656	31.2808740711
360	0101000020E6100000E0BF62D58C4D5E40D95ECBA7E7473F40	121.211720797	31.2808785316
361	0101000020E61000008A4F04E88C4D5E409B346BFBE7473F40	121.211725239	31.280883516
362	0101000020E6100000877265FA8C4D5E40CD84FB51E8473F40	121.211729621	31.2808886756
363	0101000020E610000024D4F50C8D4D5E4070D33BAEE8473F40	121.211734047	31.2808941742
364	0101000020E6100000426664198D4D5E40734DD1EEE8473F40	121.211737011	31.2808980237
365	0101000020E61000008DBC6F268D4D5E40CDD93A35E9473F40	121.211740121	31.2809022206
366	0101000020E61000002CA00E338D4D5E40671B267CE9473F40	121.21174313	31.2809064477
367	0101000020E61000001B23C53F8D4D5E40DB11AEC6E9473F40	121.211746161	31.2809108901
368	0101000020E61000001B13504B8D4D5E403A6D430DEA473F40	121.211748913	31.2809150972
369	0101000020E6100000CA18A8578D4D5E400450515CEA473F40	121.211751856	31.2809198092
370	0101000020E61000006C4136638D4D5E4089AEB1AAEA473F40	121.211754611	31.2809244808
371	0101000020E61000009B3A4D6E8D4D5E400E0795FAEA473F40	121.211757255	31.2809292425
372	0101000020E610000041A8CA788D4D5E406B7DAE4AEB473F40	121.211759756	31.2809340168
373	0101000020E61000009D070D838D4D5E4052A55E9EEB473F40	121.211762202	31.280939005
374	0101000020E6100000AFAF878C8D4D5E402171ECF1EB473F40	121.211764462	31.2809439852
375	0101000020E610000060EDA4958D4D5E40B9DBB549EC473F40	121.211766635	31.2809492177
376	0101000020E6100000A4E39A9D8D4D5E40B2EECD9EEC473F40	121.211768533	31.2809542897
377	0101000020E6100000B2E1B6A48D4D5E40F8808AF4EC473F40	121.211770228	31.2809594
378	0101000020E6100000A6DA23AB8D4D5E40F48EF74CED473F40	121.21177176	31.2809646706
379	0101000020E61000007F2555B08D4D5E408C94A29FED473F40	121.211772998	31.280969598
380	0101000020E6100000DE007AB48D4D5E403B6586EDED473F40	121.211773986	31.2809742406
381	0101000020E6100000E6FCF1B78D4D5E409F3B9C5CEE473F40	121.211774813	31.2809808618
382	0101000020E6100000BDD589B78D4D5E4084AC14AAEE473F40	121.211774716	31.2809854794
383	0101000020E6100000BCDBB5B78D4D5E4016F76A02EF473F40	121.211774757	31.2809907447
384	0101000020E6100000CCF742B78D4D5E40317F2636EF473F40	121.21177465	31.2809938282
385	0101000020E6100000AD0A44B78D4D5E406CCA9A3BEF473F40	121.211774651	31.2809941533
386	0101000020E610000094B44DB78D4D5E40562A108CEF473F40	121.21177466	31.280998949
387	0101000020E61000004E4973B78D4D5E40F34F2EDEEF473F40	121.211774695	31.2810038436
388	0101000020E6100000AA169CB78D4D5E40429A9638F0473F40	121.211774733	31.2810092323
389	0101000020E61000004C4F9FB78D4D5E40FE1C4B89F0473F40	121.211774736	31.2810140427
390	0101000020E6100000E9F099B78D4D5E40CABD3DE2F0473F40	121.211774731	31.2810193444
391	0101000020E610000021348FB78D4D5E40F8BF2F2FF1473F40	121.211774721	31.2810239307
392	0101000020E6100000F6187FB78D4D5E40F6CB3C7DF1473F40	121.211774706	31.2810285829
393	0101000020E6100000421B62B78D4D5E40DBBE59E0F1473F40	121.211774679	31.2810344905
394	0101000020E6100000B3A14CB78D4D5E4090F5CF31F2473F40	121.211774659	31.281039346
395	0101000020E6100000116949B78D4D5E402C36728DF2473F40	121.211774656	31.2810448078
396	0101000020E6100000D28E4BB78D4D5E409C6000E4F2473F40	121.211774658	31.2810499669
397	0101000020E6100000170052B78D4D5E40A5D9EE35F3473F40	121.211774664	31.2810548504
398	0101000020E6100000DEBC5CB78D4D5E4009F30A83F3473F40	121.211774674	31.2810594465
399	0101000020E6100000AC1070B78D4D5E40B9ACCCD8F3473F40	121.211774692	31.281064558
400	0101000020E6100000C36C92B78D4D5E40CD572E3CF4473F40	121.211774724	31.2810704816
401	0101000020E61000003990B1B78D4D5E403C4F448FF4473F40	121.211774753	31.2810754339
402	0101000020E6100000AFB3D0B78D4D5E40D656F1E8F4473F40	121.211774782	31.281080779
403	0101000020E61000008835F5B78D4D5E40EA5FC44DF5473F40	121.211774816	31.2810867886
404	0101000020E6100000674E22B88D4D5E40F81261BDF5473F40	121.211774858	31.2810934412
405	0101000020E61000009E9743B88D4D5E404E1D6B0BF6473F40	121.211774889	31.2810980927
406	0101000020E6100000393F6AB88D4D5E40B07C0460F6473F40	121.211774925	31.2811031352
407	0101000020E610000070888BB88D4D5E40BC6E8AB9F6473F40	121.211774956	31.2811084712
408	0101000020E61000005DC99DB88D4D5E40F4CDE815F7473F40	121.211774973	31.2811139768
409	0101000020E61000006360A6B88D4D5E40A0838D77F7473F40	121.211774981	31.2811197968
410	0101000020E6100000FF01A1B88D4D5E4072986CDBF7473F40	121.211774976	31.2811257496
411	0101000020E61000007DB070B88D4D5E400B8A2742F8473F40	121.211774931	31.2811318728
412	0101000020E6100000369C09B88D4D5E401704CFAFF8473F40	121.211774835	31.2811384087
413	0101000020E610000029C56BB78D4D5E404DA65F21F9473F40	121.211774688	31.2811451777
414	0101000020E6100000DE0DA4B68D4D5E40C467DD91F9473F40	121.211774502	31.2811518827
415	0101000020E6100000C26594B58D4D5E404227840EFA473F40	121.211774249	31.2811593125
416	0101000020E6100000CC3B60B48D4D5E406E720C89FA473F40	121.211773962	31.281166616
417	0101000020E6100000CBE31AB38D4D5E4027A04300FB473F40	121.211773659	31.2811737218
418	0101000020E61000000FF7AFB18D4D5E40B362027FFB473F40	121.211773321	31.2811812764
419	0101000020E6100000603856B08D4D5E408991BEFDFB473F40	121.211772999	31.2811888304
420	0101000020E6100000CE6C27AF8D4D5E40DCAE9B83FC473F40	121.211772717	31.2811968093
421	0101000020E6100000BDF228AE8D4D5E40F3B95709FD473F40	121.21177248	31.2812047805
422	0101000020E6100000753535AD8D4D5E40B02C6599FD473F40	121.211772253	31.2812133667
423	0101000020E6100000ECA36FAC8D4D5E40E4A21125FE473F40	121.211772069	31.2812216919
424	0101000020E61000002D7215AC8D4D5E40C6894D71FE473F40	121.211771985	31.2812262358
425	0101000020E6100000B5AB95AB8D4D5E4057C675F9FE473F40	121.211771866	31.2812343514
426	0101000020E6100000BD3646AB8D4D5E40C97B3E8AFF473F40	121.211771792	31.2812429812
427	0101000020E6100000BF301AAB8D4D5E40E740061F00483F40	121.211771751	31.2812518492
428	0101000020E6100000490DFBAA8D4D5E4025A306B800483F40	121.211771722	31.2812609688
429	0101000020E6100000519ED7AA8D4D5E40FE4D8C4A01483F40	121.211771689	31.2812697022
430	0101000020E6100000EF39A6AA8D4D5E40AB21CEDB01483F40	121.211771643	31.2812783602
431	0101000020E6100000C5186AAA8D4D5E40C2CA9D6802483F40	121.211771587	31.2812867532
432	0101000020E6100000B54D24AA8D4D5E40156A38EA02483F40	121.211771522	31.2812944782
433	0101000020E61000009EEBD5A98D4D5E4045AFE56203483F40	121.211771449	31.2813016711
434	0101000020E6100000E65084A98D4D5E400F286FCD03483F40	121.211771373	31.2813080212
435	0101000020E6100000CFEE35A98D4D5E40F0DF942704483F40	121.2117713	31.2813133944
436	0101000020E6100000C6B4CCA88D4D5E40451C2C9104483F40	121.211771202	31.2813196881
437	0101000020E6100000C9A874A88D4D5E40803A5ADF04483F40	121.21177112	31.281324348
438	0101000020E6100000A71815A88D4D5E40997D0D2E05483F40	121.211771031	31.2813290389
439	0101000020E6100000669BB6A78D4D5E409612857B05483F40	121.211770943	31.2813336563
440	0101000020E61000003D744EA78D4D5E40811A06C905483F40	121.211770846	31.2813382759
441	0101000020E6100000B2EEE0A68D4D5E404BF0E91506483F40	121.211770744	31.2813428589
442	0101000020E6100000266973A68D4D5E405931576406483F40	121.211770642	31.2813475335
443	0101000020E6100000179801A68D4D5E409FA9BFB506483F40	121.211770536	31.2813523858
444	0101000020E610000046A18DA58D4D5E409444700807483F40	121.211770428	31.2813573145
445	0101000020E6100000FF8C26A58D4D5E400AE6355C07483F40	121.211770332	31.2813623077
446	0101000020E61000005E4EF7A48D4D5E403F8132B007483F40	121.211770288	31.2813673137
447	0101000020E6100000405BCCA48D4D5E40DE10BC0008483F40	121.211770248	31.2813721141
448	0101000020E6100000E48DA3A48D4D5E4073AAF94D08483F40	121.21177021	31.281376718
449	0101000020E6100000517759A48D4D5E40EEF69EA708483F40	121.211770141	31.2813820613
450	0101000020E6100000B232FEA38D4D5E402331DF0709483F40	121.211770056	31.2813877983
451	0101000020E6100000F997ACA38D4D5E40422B6C5909483F40	121.21176998	31.2813926591
452	0101000020E61000003B6652A38D4D5E4022D0EAB209483F40	121.211769896	31.2813979934
453	0101000020E6100000769DEFA28D4D5E404E2F63190A483F40	121.211769804	31.2814041011
454	0101000020E61000005AA498A28D4D5E4062A94E800A483F40	121.211769723	31.2814102356
455	0101000020E6100000B3CE60A28D4D5E402A40C0CC0A483F40	121.211769671	31.281414792
456	0101000020E610000000CB17A28D4D5E40301DE23B0B483F40	121.211769603	31.281421416
457	0101000020E61000007F79E7A18D4D5E40949B1C8E0B483F40	121.211769558	31.2814263172
458	0101000020E6100000FD27B7A18D4D5E403B8C8DE50B483F40	121.211769513	31.2814315291
459	0101000020E61000009BC385A18D4D5E404C02753E0C483F40	121.211769467	31.2814368282
460	0101000020E6100000D5004FA18D4D5E406360AC960C483F40	121.211769416	31.2814420863
461	0101000020E6100000865B0BA18D4D5E4034AC7EF50C483F40	121.211769353	31.2814477381
462	0101000020E61000001232C0A08D4D5E403B853E530D483F40	121.211769283	31.281453326
463	0101000020E610000073ED64A08D4D5E40D965F6BC0D483F40	121.211769198	31.2814596273
464	0101000020E6100000128307A08D4D5E4079E70C200E483F40	121.211769111	31.2814655334
465	0101000020E6100000AC81A19F8D4D5E4038829C840E483F40	121.211769016	31.2814715273
466	0101000020E610000020FC339F8D4D5E403C3DEBE90E483F40	121.211768914	31.2814775657
467	0101000020E6100000D250C49E8D4D5E40B126304C0F483F40	121.21176881	31.281483423
468	0101000020E61000002D75609E8D4D5E40D33436A00F483F40	121.211768717	31.2814884312
469	0101000020E61000005150DB9D8D4D5E40D8806E0A10483F40	121.211768593	31.2814947624
470	0101000020E6100000A6DD6E9D8D4D5E401054305B10483F40	121.211768492	31.2814995759
471	0101000020E6100000B6F9FB9C8D4D5E40B1809DAD10483F40	121.211768385	31.2815044889
472	0101000020E61000009806D19C8D4D5E40BCA29BCA10483F40	121.211768345	31.281506217
473	0101000020E6100000A485299C8D4D5E4029E3791711483F40	121.211768189	31.2815107987
474	0101000020E6100000615F3E9B8D4D5E404DE6577011483F40	121.21176797	31.2815160956
475	0101000020E61000006007F9998D4D5E409080F1CE11483F40	121.211767667	31.2815217342
476	0101000020E610000030AD259B8D4D5E40004ADC1712483F40	121.211767947	31.2815260804
477	0101000020E610000000F5E09A8D4D5E4023ED466812483F40	121.211767883	31.2815308736
478	0101000020E61000009B9C079B8D4D5E40F9FD65B712483F40	121.211767919	31.2815355896
479	0101000020E61000000541CE9B8D4D5E401FF2B90313483F40	121.211768104	31.2815401391
480	0101000020E61000009B58EA9D8D4D5E40E54CEB5B13483F40	121.211768607	31.2815453958
481	0101000020E61000004F12EAA08D4D5E40ED9F8BAC13483F40	121.211769322	31.2815502015
482	0101000020E61000001446E8A48D4D5E40343795FB13483F40	121.211770274	31.2815549125
483	0101000020E6100000A62B6BAA8D4D5E401B59C85014483F40	121.211771588	31.2815599908
484	0101000020E6100000381E18B28D4D5E40D804FDAE14483F40	121.211773418	31.2815656059
485	0101000020E6100000006710BC8D4D5E4057277B0C15483F40	121.211775795	31.2815711785
486	0101000020E6100000B2B157C78D4D5E40B7E9305F15483F40	121.211778484	31.2815761084
487	0101000020E610000015AFA0D38D4D5E40E41C99A815483F40	121.211781413	31.2815804838
488	0101000020E6100000FF05EAE38D4D5E40753E6FF315483F40	121.211785296	31.2815849444
489	0101000020E6100000A04EF8F38D4D5E40AE2A6E2916483F40	121.211789124	31.2815881628
490	0101000020E610000057B537078E4D5E4071503E5D16483F40	121.211793713	31.2815912511
491	0101000020E610000021DB641A8E4D5E40055F288A16483F40	121.211798285	31.2815939282
492	0101000020E6100000A838182E8E4D5E40D902C3B416483F40	121.211802982	31.2815964676
493	0101000020E61000006E1F82428E4D5E4014DABADF16483F40	121.211807849	31.2815990287
494	0101000020E61000007D1427578E4D5E402DAA0E0817483F40	121.211812771	31.2816014324
495	0101000020E610000086DD5E6F8E4D5E4004E4DF3017483F40	121.211818545	31.2816038653
496	0101000020E61000005B44F3848E4D5E40570D1B4C17483F40	121.21182369	31.2816054884
497	0101000020E6100000B604889E8E4D5E40B9E6C65F17483F40	121.211829789	31.2816066609
498	0101000020E6100000E53E71B48E4D5E40FE8B756817483F40	121.211835013	31.2816071784
499	0101000020E61000002AFB28CF8E4D5E408135EB6917483F40	121.211841383	31.2816072654
500	0101000020E6100000CF07EAE58E4D5E40B6F9CB6317483F40	121.211846808	31.2816069005
501	0101000020E6100000DC0FE5FC8E4D5E404DBDBF5817483F40	121.211852287	31.281606242
502	0101000020E6100000352A38088F4D5E4059DEA65617483F40	121.211854987	31.281606117
503	0101000020E610000007AA4E208F4D5E40D7C51A5A17483F40	121.21186073	31.2816063228
504	0101000020E6100000BBA4202E8F4D5E4094291B5C17483F40	121.211864025	31.2816064421
505	0101000020E6100000C3665A448F4D5E4039E0B15E17483F40	121.211869324	31.2816065964
506	0101000020E6100000C44D6E5D8F4D5E40DBA2A06117483F40	121.211875303	31.2816067712
507	0101000020E610000043A560798F4D5E40A21B8E6417483F40	121.211881966	31.2816069457
508	0101000020E61000002E8897938F4D5E4004C50C6717483F40	121.211888216	31.2816070944
509	0101000020E61000009FB7A4AF8F4D5E402161736917483F40	121.211894904	31.2816072375
510	0101000020E6100000D143A7C68F4D5E40F27D356B17483F40	121.21190039	31.2816073423
511	0101000020E6100000415F8CDE8F4D5E40EA18D36C17483F40	121.211906087	31.2816074386
512	0101000020E61000000E5CC2FA8F4D5E404049766E17483F40	121.211912813	31.2816075362
513	0101000020E6100000816B4F12904D5E400B589A6F17483F40	121.211918428	31.2816076042
514	0101000020E6100000A0386429904D5E4060D9847017483F40	121.211923931	31.2816076588
515	0101000020E6100000AF8B7A3F904D5E401C17377117483F40	121.211929197	31.2816077003
516	0101000020E6100000578CE355904D5E409A42C87117483F40	121.21193454	31.2816077341
517	0101000020E6100000BB42C472904D5E40CE654A7217483F40	121.211941425	31.2816077644
518	0101000020E6100000121CDB8E904D5E405AEBB77217483F40	121.211948122	31.2816077899
519	0101000020E61000005B82F1AB904D5E404ED04D7317483F40	121.211955057	31.2816078248
520	0101000020E61000000C0E0BC2904D5E406A33BF7317483F40	121.211960326	31.2816078512
521	0101000020E610000048FBF4DF904D5E40E85E507417483F40	121.211967458	31.281607885
522	0101000020E6100000433ED6F6904D5E40FE2AB97417483F40	121.211972913	31.2816079094
523	0101000020E610000000052B0F914D5E4013F7217517483F40	121.211978714	31.2816079338
524	0101000020E6100000F1714027914D5E405F76C67517483F40	121.211984456	31.2816079721
525	0101000020E610000003820B42914D5E401594057717483F40	121.211990844	31.2816080464
526	0101000020E610000006C1645C914D5E40B1FD8B7817483F40	121.211997126	31.2816081373
527	0101000020E6100000E7CDCF77914D5E40172B6C7A17483F40	121.212003663	31.2816082491
528	0101000020E61000007D81E493914D5E406ED2A47C17483F40	121.212010358	31.2816083815
529	0101000020E6100000875E44B0914D5E401984327F17483F40	121.212017123	31.2816085337
530	0101000020E610000062E1D0CD914D5E409C8B198217483F40	121.212024168	31.2816087067
531	0101000020E610000025083BEB914D5E406468F58417483F40	121.212031181	31.2816088771
532	0101000020E61000003D1AAA09924D5E40C0DE598717483F40	121.212038437	31.2816090197
533	0101000020E6100000E303A029924D5E409892248917483F40	121.212046057	31.2816091265
534	0101000020E6100000104AE13F924D5E403EB3268A17483F40	121.212051363	31.2816091866
535	0101000020E6100000ACFF995F924D5E4084AE698B17483F40	121.212058926	31.2816092618
536	0101000020E610000084ECEF7E924D5E4035998E8C17483F40	121.212066397	31.28160933
537	0101000020E6100000DA943FA0924D5E401D37EF8D17483F40	121.212074339	31.2816094121
538	0101000020E6100000ADF18AC1924D5E40574FEB8F17483F40	121.212082277	31.2816095304
539	0101000020E61000003F3F5ED8924D5E40CB3A969117483F40	121.212087719	31.2816096298
540	0101000020E61000003F1F74EF924D5E403EFA759317483F40	121.212093223	31.2816097415
541	0101000020E610000064D4D805934D5E40E239779517483F40	121.212098562	31.281609861
542	0101000020E61000008B903B1E934D5E408F17D09717483F40	121.212104376	31.2816100009
543	0101000020E61000009EDEF735934D5E40D269359A17483F40	121.212110035	31.2816101437
544	0101000020E61000003FFD3C4D934D5E408B759F9C17483F40	121.212115583	31.2816102876
545	0101000020E6100000E904A465934D5E40C7681F9F17483F40	121.212121401	31.2816104366
546	0101000020E6100000B8E1597D934D5E402AA257A117483F40	121.212127054	31.2816105689
547	0101000020E610000012447D95934D5E40CFA935A317483F40	121.212132809	31.2816106802
548	0101000020E61000001B0DB5AD934D5E40896AD5A417483F40	121.212138583	31.281610777
549	0101000020E6100000DBC7B1C5934D5E40F3533AA617483F40	121.212144302	31.2816108601
550	0101000020E610000030E413DD934D5E4092B168A717483F40	121.212149877	31.2816109305
551	0101000020E61000000B4D4CF6934D5E40B4BE89A817483F40	121.21215589	31.2816109978
552	0101000020E6100000717F0F0D944D5E40C5CE6DA917483F40	121.212161317	31.2816110509
553	0101000020E610000088702C25944D5E4054F73BAA17483F40	121.212167066	31.2816110989
554	0101000020E6100000F497693D944D5E404878E3AA17483F40	121.212172845	31.2816111379
555	0101000020E6100000BAE96E55944D5E40E02B62AB17483F40	121.212178572	31.2816111674
556	0101000020E6100000438FF275944D5E409EF9CDAB17483F40	121.212186324	31.2816111925
557	0101000020E6100000FED33894944D5E40E79DEEAB17483F40	121.212193542	31.2816112001
558	0101000020E6100000FB88B3B0944D5E408471E0AB17483F40	121.212200332	31.2816111968
559	0101000020E6100000B41045CA944D5E402C0FF5AB17483F40	121.212206428	31.2816112016
560	0101000020E6100000EEDCDCE1944D5E406FB81EAC17483F40	121.212212053	31.2816112113
561	0101000020E6100000BD5B21F8944D5E405B634BAC17483F40	121.212217362	31.2816112217
562	0101000020E6100000EF550A05954D5E40477266AC17483F40	121.21222044	31.281611228
563	0101000020E61000008A8CFF1C954D5E40F5553CAD17483F40	121.212226152	31.2816112778
564	0101000020E6100000DA5C7433954D5E40100478AE17483F40	121.212231506	31.2816113513
565	0101000020E61000003BF82E4A954D5E404841F0AF17483F40	121.212236925	31.2816114389
566	0101000020E61000003B36B662954D5E408FB3C8B117483F40	121.212242773	31.2816115489
567	0101000020E6100000488FF878954D5E40080AB1B317483F40	121.21224808	31.2816116626
568	0101000020E6100000F6E24C92954D5E4038CF1FB617483F40	121.212254119	31.2816118076
569	0101000020E610000070E127AB954D5E4002D8C6B817483F40	121.212260045	31.2816119657
570	0101000020E6100000358AA0C2954D5E40035C86BB17483F40	121.212265641	31.2816121295
571	0101000020E6100000CBDF43DD954D5E401185ECBE17483F40	121.212271992	31.2816123321
572	0101000020E61000006C18DDF8954D5E403B11C4C217483F40	121.212278572	31.2816125611
573	0101000020E6100000219F9C10964D5E40864C4BC617483F40	121.212284234	31.2816127714
574	0101000020E6100000B3F99929964D5E40D9E6B7C917483F40	121.212290192	31.2816129755
575	0101000020E6100000F8069943964D5E4073371ACC17483F40	121.21229639	31.2816131176
576	0101000020E61000008F05C95E964D5E4076D4FDCD17483F40	121.212302872	31.2816132302
577	0101000020E6100000BC655E79964D5E40229FAFCF17483F40	121.21230921	31.2816133312
578	0101000020E61000002A435294964D5E40D1F91AD117483F40	121.212315636	31.2816134158
579	0101000020E6100000BDA44DAF964D5E40E54871D217483F40	121.212322069	31.2816134955
580	0101000020E6100000050466CA964D5E40E7369CD317483F40	121.212328529	31.2816135651
581	0101000020E610000017B6BFE3964D5E40799EDCD417483F40	121.212334573	31.2816136397
582	0101000020E610000065F7FBFD964D5E405DE4A6D617483F40	121.212340828	31.2816137464
583	0101000020E6100000F40C0A18974D5E40B5B7AAD817483F40	121.21234704	31.2816138665
584	0101000020E6100000197E5131974D5E40BCBFCBDA17483F40	121.212353067	31.2816139934
585	0101000020E610000090CCA147974D5E40D6B8D1DC17483F40	121.212358387	31.281614114
586	0101000020E610000064A4FD60974D5E40253948DF17483F40	121.212364433	31.2816142608
587	0101000020E61000008A533677974D5E4017EC95E117483F40	121.212369731	31.2816143981
588	0101000020E6100000CA071E8E974D5E40E2BC19E417483F40	121.212375192	31.281614548
589	0101000020E61000007EB4F38E974D5E400DA632E417483F40	121.212375391	31.2816145538
590	0101000020E61000003EC037A6974D5E4086B150E517483F40	121.212380938	31.2816146204
591	0101000020E6100000E18709BE974D5E40BDC87AE517483F40	121.212386617	31.2816146302
592	0101000020E61000002CBB49D4974D5E40637864E417483F40	121.212391922	31.2816145654
593	0101000020E61000001AE36FED974D5E4081139BE117483F40	121.212397918	31.2816143993
594	0101000020E61000002B8ECB04984D5E40EBD581DC17483F40	121.212403487	31.2816140954
595	0101000020E610000071372D1D984D5E40B4C2D2D317483F40	121.2124093	31.2816135778
596	0101000020E6100000D1DF1136984D5E404533DBC517483F40	121.212415235	31.2816127453
597	0101000020E61000005A44C34B984D5E40294FF5B417483F40	121.212420407	31.2816117381
598	0101000020E61000009C43C661984D5E404950359F17483F40	121.212425655	31.2816104417
599	0101000020E6100000C8890778984D5E4080D1E38317483F40	121.212430961	31.2816088134
600	0101000020E6100000C2235C8E984D5E401ADB0F6317483F40	121.212436285	31.2816068567
601	0101000020E6100000A2BE2DA4984D5E40B37F1D3D17483F40	121.212441487	31.2816045949
602	0101000020E610000064B1EFB8984D5E40A04C4F1317483F40	121.212446436	31.2816021031
603	0101000020E6100000511635CD984D5E40E373EDE316483F40	121.212451269	31.2815992789
604	0101000020E6100000ACAF4BE0984D5E40D6450EB016483F40	121.21245582	31.2815961871
605	0101000020E61000003E3412F2984D5E40D1249D7716483F40	121.212460058	31.2815928229
606	0101000020E6100000015EC701994D5E40FB31343E16483F40	121.212463803	31.281589401
607	0101000020E610000081619110994D5E407059510016483F40	121.212467329	31.2815857123
608	0101000020E61000005837DE1D994D5E40871662C015483F40	121.2124705	31.2815819015
609	0101000020E61000000543622A994D5E402E9D9F7A15483F40	121.212473484	31.2815777435
610	0101000020E61000003EC72935994D5E408ABD5C3315483F40	121.212476054	31.281573496
611	0101000020E6100000E0E28D3E994D5E4082CFDAE714483F40	121.212478293	31.2815689954
612	0101000020E61000007FFD1F46994D5E408ACBEE9B14483F40	121.212480098	31.2815644701
613	0101000020E61000005E8E124C994D5E404B0A604D14483F40	121.212481516	31.2815597877
614	0101000020E6100000BD693750994D5E40AA99D5FD13483F40	121.212482504	31.2815550467
615	0101000020E61000006DD74952994D5E40484766AB13483F40	121.212482998	31.2815501332
616	0101000020E6100000110A2152994D5E4025ABA85E13483F40	121.21248296	31.2815455591
617	0101000020E6100000C6F4E74F994D5E406F031A0B13483F40	121.21248243	31.2815405787
618	0101000020E6100000A4F9EC4B994D5E4085E144B912483F40	121.212481481	31.2815357011
619	0101000020E610000018B89C48994D5E4053542B8312483F40	121.212480691	31.2815324765
620	0101000020E6100000F0903448994D5E40049CDC3212483F40	121.212480594	31.2815276898
621	0101000020E610000029D42948994D5E403DFEBDE211483F40	121.212480584	31.2815229143
622	0101000020E610000091CF6348994D5E40E394709111483F40	121.212480638	31.2815180683
623	0101000020E610000023ECD948994D5E401B9A104511483F40	121.212480748	31.281513516
624	0101000020E61000000FDCA449994D5E40838DB8EE10483F40	121.212480937	31.2815083695
625	0101000020E61000000C97B54A994D5E401694429110483F40	121.212481191	31.2815027988
626	0101000020E6100000DD3CE24B994D5E40C0CA233410483F40	121.212481471	31.2814972484
627	0101000020E610000038BFEF4C994D5E40B005F7E20F483F40	121.212481722	31.28149241
628	0101000020E610000079EB064E994D5E40391E128B0F483F40	121.212481982	31.2814871711
629	0101000020E6100000B0E3E04E994D5E40BE3C08350F483F40	121.212482185	31.2814820428
630	0101000020E61000007FE0804F994D5E40D1B442DC0E483F40	121.212482334	31.2814767516
631	0101000020E61000004FD7F44F994D5E40C7D1E88B0E483F40	121.212482442	31.2814719623
632	0101000020E61000003FBB6750994D5E409EE58A2E0E483F40	121.212482549	31.2814663972
633	0101000020E61000000384CA50994D5E40CA8BBCCB0D483F40	121.212482641	31.2814605079
634	0101000020E610000072160D51994D5E4096A17A7F0D483F40	121.212482703	31.2814559626
635	0101000020E610000038D94351994D5E40A8F01B330D483F40	121.212482754	31.2814514106
636	0101000020E610000036DF6F51994D5E40D363BCE60C483F40	121.212482795	31.2814468584
637	0101000020E61000006D289151994D5E40F412B7980C483F40	121.212482826	31.281442208
638	0101000020E6100000FCA1A651994D5E4083092A4A0C483F40	121.212482846	31.281437526
639	0101000020E61000000239AF51994D5E401C3229F30B483F40	121.212482854	31.2814323402
640	0101000020E6100000BDC7A851994D5E4062DB3FA20B483F40	121.212482848	31.2814275175
641	0101000020E610000092AC9851994D5E4099FEAE510B483F40	121.212482833	31.2814227154
642	0101000020E61000001D897951994D5E405E81DFFC0A483F40	121.212482804	31.2814176603
643	0101000020E6100000E0A84F51994D5E40A41DCCA90A483F40	121.212482765	31.2814127086
644	0101000020E6100000979A1451994D5E40542B58520A483F40	121.21248271	31.281407496
645	0101000020E6100000C5A9CC50994D5E40E86B07FC09483F40	121.212482643	31.2814023512
646	0101000020E61000004BE97850994D5E401C4957A709483F40	121.212482565	31.2813973034
647	0101000020E6100000A60D1550994D5E404CE6625009483F40	121.212482472	31.2813921205
648	0101000020E6100000B092994F994D5E40B2D708F308483F40	121.212482357	31.2813865563
649	0101000020E6100000D46D144F994D5E40DDE9059908483F40	121.212482233	31.2813811912
650	0101000020E6100000A0189B4E994D5E402BEA243F08483F40	121.21248212	31.281375834
651	0101000020E61000003286584E994D5E408A7D15E707483F40	121.212482058	31.2813705852
652	0101000020E6100000D4BE5B4E994D5E4060F6D58E07483F40	121.212482061	31.2813653252
653	0101000020E6100000A6AFA34E994D5E4007660A3707483F40	121.212482128	31.2813600922
654	0101000020E61000005D50214F994D5E40040C5FDF06483F40	121.212482245	31.2813548667
655	0101000020E610000045A3B74F994D5E40CEECA28806483F40	121.212482385	31.2813496969
656	0101000020E61000003FBB6750994D5E400540582F06483F40	121.212482549	31.2813443747
657	0101000020E6100000A3C82551994D5E40C41E56D805483F40	121.212482726	31.2813391886
658	0101000020E61000008FB8F051994D5E401370BC7D05483F40	121.212482915	31.2813337884
659	0101000020E61000009B95BA52994D5E405248A52505483F40	121.212483103	31.2813285378
660	0101000020E6100000C0C87A53994D5E4065626ECB04483F40	121.212483282	31.2813231606
661	0101000020E610000051EB1C54994D5E402D61817004483F40	121.212483433	31.281317741
662	0101000020E61000009705B054994D5E40F601231404483F40	121.21248357	31.2813122354
663	0101000020E610000079C13D55994D5E4031AC3EB503483F40	121.212483702	31.2813065794
664	0101000020E6100000E45FD855994D5E401158BB5903483F40	121.212483846	31.2813011248
665	0101000020E61000008CDE9C56994D5E408C03530103483F40	121.212484029	31.2812958553
666	0101000020E6100000723D8B57994D5E40FA8656A702483F40	121.212484251	31.2812904917
667	0101000020E61000002A879558994D5E406BD8B34B02483F40	121.212484499	31.2812850298
668	0101000020E61000008BA0AB59994D5E40D17FBFF201483F40	121.212484758	31.2812797277
669	0101000020E61000007A33D75A994D5E40D8BCBE9701483F40	121.212485037	31.2812743035
670	0101000020E610000051700C5C994D5E40E758A33C01483F40	121.212485325	31.2812688731
671	0101000020E61000002116395D994D5E40C94392E100483F40	121.212485605	31.2812634451
672	0101000020E61000004355515E994D5E40F2B1FE8100483F40	121.212485866	31.2812577483
673	0101000020E6100000867B3C5F994D5E40228C7A2400483F40	121.212486085	31.2812521743
674	0101000020E610000035910960994D5E404CC2C3C4FF473F40	121.212486276	31.2812464693
675	0101000020E6100000BF1CA360994D5E400FF9576DFF473F40	121.212486419	31.2812412586
676	0101000020E6100000B5971E61994D5E40F26EB318FF473F40	121.212486534	31.2812362135
677	0101000020E610000015027C61994D5E40A72ECCCAFE473F40	121.212486621	31.2812315701
678	0101000020E6100000577FDA61994D5E40AC188269FE473F40	121.212486709	31.2812257712
679	0101000020E610000042C61862994D5E4088087A0EFE473F40	121.212486767	31.2812203453
680	0101000020E61000003A353C62994D5E4006D5B4BAFD473F40	121.2124868	31.2812153522
681	0101000020E6100000C3174962994D5E4005E1F969FD473F40	121.212486812	31.2812105403
682	0101000020E610000060B94362994D5E403742671BFD473F40	121.212486807	31.281205857
683	0101000020E610000092653062994D5E403416E7CDFC473F40	121.212486789	31.2812012376
684	0101000020E610000036980762994D5E40E4164974FC473F40	121.212486751	31.281195896
685	0101000020E6100000B546D761994D5E409F6D6A26FC473F40	121.212486706	31.2811912546
686	0101000020E6100000FF4EE661994D5E40E10208D3FB473F40	121.21248672	31.2811862845
687	0101000020E6100000B7E93762994D5E40D8839C82FB473F40	121.212486796	31.2811814911
688	0101000020E6100000EC3EB162994D5E40B52BFC32FB473F40	121.212486909	31.281176745
689	0101000020E610000076CA4A63994D5E4003F657E3FA473F40	121.212487052	31.281171998
690	0101000020E6100000379F0564994D5E4033317793FA473F40	121.212487226	31.2811672369
691	0101000020E61000006D9D0B65994D5E409EAD2B39FA473F40	121.21248747	31.2811618549
692	0101000020E610000070EF2466994D5E406A25D6E5F9473F40	121.212487732	31.2811568878
693	0101000020E610000058F17367994D5E40D851158EF9473F40	121.212488044	31.2811516573
694	0101000020E610000067719E68994D5E40EDA70542F9473F40	121.212488322	31.2811471237
695	0101000020E6100000516DC169994D5E40ED7C57F6F8473F40	121.212488593	31.2811426128
696	0101000020E610000009B7CB6A994D5E40B2B5EDA5F8473F40	121.212488841	31.2811378198
697	0101000020E6100000BD63A16B994D5E403205B84FF8473F40	121.21248904	31.2811326813
698	0101000020E61000000284606C994D5E401E9B28F7F7473F40	121.212489218	31.2811274027
699	0101000020E610000066911E6D994D5E40375FA19DF7473F40	121.212489395	31.2811220664
700	0101000020E6100000852DD66D994D5E4073C59F41F7473F40	121.212489566	31.2811165824
701	0101000020E6100000A4C98D6E994D5E4027F719E1F6473F40	121.212489737	31.2811108292
702	0101000020E6100000B737346F994D5E40FB036D83F6473F40	121.212489892	31.2811052457
703	0101000020E610000023D6CE6F994D5E4003E97725F6473F40	121.212490036	31.2810996454
704	0101000020E61000002A166470994D5E401F5A49C2F5473F40	121.212490175	31.2810937337
705	0101000020E6100000C860EB70994D5E409265295FF5473F40	121.212490301	31.2810878254
706	0101000020E610000009DE4971994D5E40117F3013F5473F40	121.212490389	31.2810832971
707	0101000020E610000045C49F71994D5E40619528C6F4473F40	121.212490469	31.2810787057
708	0101000020E61000001D4CF071994D5E40AB6BDC76F4473F40	121.212490544	31.2810739792
709	0101000020E61000004C043572994D5E40E1825129F4473F40	121.212490608	31.2810693573
710	0101000020E610000057387272994D5E40A9CF60DBF3473F40	121.212490665	31.2810647117
711	0101000020E61000001DFBA872994D5E40C18E8C88F3473F40	121.212490716	31.2810597747
712	0101000020E61000001B01D572994D5E4067C15037F3473F40	121.212490757	31.2810549328
713	0101000020E61000004C3AF372994D5E40D5503DF1F2473F40	121.21249078514774	31.2810507559487
\.


--
-- Data for Name: shortpath; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.shortpath (pgr_fromatontob) FROM stdin;
0102000020E6100000C9020000D9F7EE00884D5E4019C008491D483F402D0DC4FE874D5E40B439CD481D483F40D747D5E6874D5E40648D8B461D483F40F24CEECB874D5E4001B841441D483F40CE1998AF874D5E406C360B421D483F4084317398874D5E4088542F401D483F40889D4A82874D5E4016F93D3E1D483F4042967768874D5E4091619D3B1D483F404E79D74D874D5E404E607F381D483F4010542833874D5E4002C109351D483F4038C54315874D5E40DCF0B6301D483F400E3F02F6864D5E408461D02B1D483F40B5A20AD5864D5E40D78F71261D483F404E2F75B3864D5E40C426C7201D483F40C9CEE091864D5E40144E191B1D483F4015526A6E864D5E40D6FB55151D483F407ACD7849864D5E400009BB0F1D483F4083CCA523864D5E4092AD6B0A1D483F40CD9978FD854D5E4039BA96051D483F40D68B7BD5854D5E4046FA1E011D483F40344F2FAA854D5E40A3ADF3FC1C483F40611CAD81854D5E406E06B2F91C483F4000D2396A854D5E40054918F81C483F4027FB843F854D5E409D40B4F51C483F4018A86E29854D5E40226DB9F41C483F4057E9BE01854D5E40661C60F31C483F40837F49DE844D5E404D49A8F21C483F40E6B568BA844D5E405B0773F21C483F40CFAAF79D844D5E402198B2F21C483F4027903A83844D5E40F7C140F31C483F405C059E6C844D5E401095F8F31C483F4088201851844D5E40AEF226F51C483F4025261A37844D5E4012E194F61C483F408B476A20844D5E40F383FEF71C483F40876CAE07844D5E40B79DA8F71C483F40538866EF834D5E4010B5C9F61C483F407B2000D8834D5E403FABAEF51C483F40B21E70BD834D5E40AAD830F41C483F4060CF379F834D5E40904319F21C483F40A9AC1589834D5E4081B047F01C483F4046BF4171834D5E4011E50FEE1C483F4069AD7C57834D5E405A695FEB1C483F4022B9FF3F834D5E408432A7E81C483F40C16E8C28834D5E40C94BB9E51C483F40122E8E11834D5E404E07A6E21C483F40ABC722F2824D5E40491E26DE1C483F40402F1ED6824D5E40CCEBE7D91C483F408D3E95BC824D5E40E8FCE1D51C483F4019867BA4824D5E40D2FD00D21C483F40DC74F48D824D5E4049B058CE1C483F402AD5B273824D5E40366DFCC91C483F4005C2DC5B824D5E404DE7EDC51C483F4086C85B41824D5E40C07F4FC11C483F40AD15DA2A824D5E408B2955BD1C483F4098FEA629824D5E40FD771CBD1C483F4072A0B512824D5E40DC25AEB91C483F407204BDF8814D5E40745BEAB51C483F402760B5DE814D5E4030347EB11C483F408E74DBC5814D5E40290C8CAC1C483F4002464BA9814D5E407EB73FA61C483F404A60488E814D5E40C4C9A49F1C483F407E773A76814D5E40E11EBC981C483F40A6BE8C5F814D5E40A1F7F1901C483F4026D86147814D5E40152B35871C483F407CD0FA2E814D5E408F4C457B1C483F40CA8E2A16814D5E4024662B6C1C483F40626293FF804D5E4062D25B5C1C483F408456FAE5804D5E40FAC595481C483F408124CBCD804D5E4029E311341C483F40499624B3804D5E407EAFDF1B1C483F406C40429C804D5E40BB90A3051C483F40BBA7FE83804D5E40CDCC6BEC1B483F404D287C6A804D5E404DCCECCF1B483F4035306150804D5E40B5EB8BB01B483F4093ACAC35804D5E40CF07C78D1B483F40DFB4251A804D5E400B6816671B483F4034C01404804D5E406EBE06461B483F401F7884EC7F4D5E40D1FAA2201B483F406B490AD67F4D5E401E07E8FA1A483F4005C6FFBF7F4D5E400C58D9D31A483F40F7BD04A97F4D5E40D248BEA81A483F40C832D4937F4D5E405DB1BE7E1A483F40286CD47D7F4D5E405E5581501A483F40BFF421687F4D5E407FD3EC1F1A483F401D2D50507F4D5E401CA6E0E619483F4095C2723A7F4D5E40DCB1C0AE19483F402124AD267F4D5E40F9E83A7819483F400C47B8127F4D5E40F812363D19483F4075CD77FF7E4D5E4052869AFF18483F406D76D9EC7E4D5E40A467A1BE18483F40ACE886DB7E4D5E406ED0067C18483F4036BB88CB7E4D5E40E0DCFD3718483F4027ED61BD7E4D5E40EDDBE5F417483F40C2E81AAF7E4D5E40FA5B5BA817483F406A19E3A27E4D5E40D5F1435D17483F407BA356987E4D5E400CF1661217483F40BB43808F7E4D5E40F6B532C916483F405DF793877E4D5E4076D5B67B16483F4033AFD9807E4D5E406836D82B16483F401D847E7B7E4D5E404367A2DB15483F405A4A54777E4D5E4052D9BD8715483F408B408A747E4D5E40F486F03115483F40010638737E4D5E4022D4E4E014483F40DD7B04737E4D5E406F20FE8C14483F407F63C0737E4D5E40212A0F3E14483F40B80427757E4D5E400349A6EB13483F40D0BEBA767E4D5E40BE929D9B13483F404ECBFB777E4D5E4089A6594813483F404BD753787E4D5E402935EBF412483F40BC5D3E787E4D5E40F9ECD9A312483F406384BD777E4D5E40F41CC95012483F4088590C777E4D5E40F322910112483F40516132767E4D5E4002D9C9AA11483F40783055757E4D5E40745D9F5211483F400E8C8E747E4D5E4028C6F30611483F4087F49C737E4D5E40070805AD10483F40883FB8727E4D5E404A14385A10483F405147DE717E4D5E403E6D7C0110483F40059642717E4D5E40A3FB4AC30F483F40FE55AD707E4D5E40E3247B6A0F483F4033F641707E4D5E409D4177150F483F404218FB6F7E4D5E4022D2B5C60E483F40BB2FC26F7E4D5E402DE8D8610E483F40AF01B16F7E4D5E4088CB57ED0D483F4019F7BE6F7E4D5E4008BDB0950D483F40B39EE56F7E4D5E40AC6883350D483F409FE523707E4D5E4070FC58DA0C483F40A71F8D707E4D5E405799ED6E0C483F40267D15717E4D5E4042C190060C483F4082F9F6717E4D5E404B136B7F0B483F40FE62D7727E4D5E402CAE5B100B483F40D98D88737E4D5E40E8CC59C20A483F401F57D4747E4D5E40CC8E833C0A483F407B826E767E4D5E40441EF7A409483F40793753777E4D5E40096A345509483F40206BFC787E4D5E40A986AFC608483F40F20AFD797E4D5E40CDC4337308483F40F656EA7A7E4D5E40D0550A2708483F4069E4D27C7E4D5E40358F7A8A07483F4008879F7E7E4D5E4076C276F006483F40CEF88E7F7E4D5E40A3DCF29B06483F40F3DA07817E4D5E40F120610B06483F40C28660827E4D5E4003B0EF7605483F400A4A80837E4D5E40E7E57CE504483F40D74C4C847E4D5E40D720826004483F40D35ED0847E4D5E408C0DC8E303483F4008AE1D857E4D5E408F0FE96503483F40B61432857E4D5E405C0A3CF602483F408BF921857E4D5E406E893C8702483F40938AFE847E4D5E402CB0D22402483F405013CC847E4D5E4084FACFBF01483F40967EA6847E4D5E40FE85966401483F40AFD49C847E4D5E403CE7E90B01483F40FF73B4847E4D5E402B408CB600483F40103FFA847E4D5E4069A7754200483F40386662857E4D5E40E2C46ED7FF473F407252E4857E4D5E40759EFD74FF473F4073FB70867E4D5E40572B7B18FF473F405020F6867E4D5E40177514C5FE473F4026AE72877E4D5E40B3BFE677FE473F40EF130A887E4D5E40B4FA4119FE473F402F9794887E4D5E4057C845C6FD473F402AA918897E4D5E40BDC3CA78FD473F40E7E09E897E4D5E4058918A2CFD473F4033923A8A7E4D5E408A059FDEFC473F4032471F8B7E4D5E40854B807EFC473F4044BBF18B7E4D5E4007EE932EFC473F404E9EE78C7E4D5E405ABE0EDAFB473F40E069168E7E4D5E40CA161982FB473F4076D2798F7E4D5E4089F5162BFB473F40DF2B25917E4D5E40984631CFFA473F4070A60C937E4D5E405773446BFA473F40C1A395947E4D5E40FF93331BFA473F4091F87A967E4D5E40D85BDBB7F9473F40E0FB2F987E4D5E40E3D61C58F9473F4045B27A997E4D5E4077D3AE04F9473F40E4A58E9A7E4D5E406A6748B8F8473F404B56AD9B7E4D5E406F997D65F8473F405CD0AB9C7E4D5E405C389D17F8473F40320DE19D7E4D5E409AE705B2F7473F403CF0D69E7E4D5E40F0B7225AF7473F401427E09F7E4D5E40B44E43F1F6473F4053B096A07E4D5E40E069EFA1F6473F406CB545A17E4D5E407FDF5C4FF6473F409E10EBA17E4D5E4045C67BFAF5473F402F338DA27E4D5E40D0165C9FF5473F40FE2F2DA37E4D5E40B55D5C3BF5473F40A7A8C5A37E4D5E406FF236D2F4473F4007194FA47E4D5E40C7D71568F4473F40479CD9A47E4D5E40AD447DEBF3473F40F20E46A57E4D5E402A28F375F3473F40B040A0A57E4D5E40CE2426FBF2473F40A11EE7A57E4D5E40C47AF378F2473F40813714A67E4D5E4005ACD4F1F1473F40AC5224A67E4D5E40E3556875F1473F40C5A81AA67E4D5E404C8237EDF0473F40CD39F7A57E4D5E40C1BF786EF0473F40E789C1A57E4D5E4048164DFBEF473F40547377A57E4D5E4073D3A090EF473F409BD825A57E4D5E409D36E535EF473F403B6EC8A47E4D5E40AC5D36E1EE473F4076A565A47E4D5E40A0F1CF94EE473F40308BD2A37E4D5E408EE35133EE473F40A06830A37E4D5E40E7DE09D6ED473F40F0588FA27E4D5E40A5AB9083ED473F40302D61A27E4D5E40246EA16DED473F4010468EA27E4D5E4024022117ED473F40754D20A37E4D5E40230B34BAEC473F403CB9E3A37E4D5E40FB3B5C6DEC473F405DFE27A57E4D5E40CB540710EC473F4054D1E8A67E4D5E402CD96DABEB473F4064FA9FA87E4D5E406787EB5DEB473F4047C357AB7E4D5E407D61B4FAEA473F40A9E36BAE7E4D5E4070B47BA3EA473F40071604B27E4D5E409DE84B53EA473F40766B27B77E4D5E409ED90CF9E9473F4048257CBC7E4D5E40791EAEAEE9473F40B5E468C37E4D5E403DA20E5FE9473F40AFD24FCB7E4D5E4048345911E9473F40859474D47E4D5E40CAA5F6C4E8473F40C10CE4DE7E4D5E407AFB017CE8473F40CCD938EB7E4D5E40D8C94730E8473F40D4B811F87E4D5E4057104AE9E7473F40A6489D047F4D5E404B56CEA9E7473F400C47B8127F4D5E4083517667E7473F40128A2E217F4D5E402D442127E7473F4039705A327F4D5E4020949DDDE6473F4033E330427F4D5E40CE042D9EE6473F40208C93537F4D5E406CC9C65DE6473F40CCB16B647F4D5E40AAE06623E6473F40AAE7CD767F4D5E40A58C7DE7E5473F40949C88887F4D5E40184B5FB0E5473F4088D6C7997F4D5E403B39B37CE5473F40AD2091AC7F4D5E4076188E45E5473F40BCC4EEC17F4D5E4046A36809E5473F40627A3CD97F4D5E40FA91DCCAE4473F40168F62EB7F4D5E40F7923E9CE4473F402BCAC800804D5E40EC7D8367E4473F4076F60A15804D5E40E62CCC37E4473F409BFCB62A804D5E4002781D07E4473F402BF24440804D5E40EE621ED9E3473F40C378AF55804D5E40E7FFEFADE3473F40501C246A804D5E40E346CF86E3473F408143B77D804D5E40673F4563E3473F402E9D3797804D5E401EFBC937E3473F405A9F5BB0804D5E40318B2410E3473F404DB5FDC8804D5E40D2FF2FECE2473F40EED342E0804D5E4071FED5CCE2473F403276A6F6804D5E40F14821B1E2473F40E05E5F0C814D5E4017897598E2473F40FE82E722814D5E40E02D4781E2473F40539F493A814D5E40D94CBD6BE2473F406A976454814D5E40ED6AB656E2473F409634196A814D5E406E93B747E2473F40AD705181814D5E40D401833AE2473F40D47DFB98814D5E4020405530E2473F40206D1EB2814D5E40AB8B9D29E2473F40FFD0FCCC814D5E406D086427E2473F4078EF57EA814D5E40D08C662AE2473F406FBC8207824D5E404B87DF31E2473F40A17C2D27824D5E40F6BD5C3CE2473F40514F4548824D5E4050DE2C46E2473F407F8F5A5E824D5E40400EEB4AE2473F40835DEC74824D5E40DABCBE4EE2473F40B63A368B824D5E40850A5E51E2473F4076A4EBA3824D5E4095EE7652E2473F40554CE7BB824D5E4079271752E2473F40BF99D0D8824D5E403E5AE550E2473F40B0C2C8F3824D5E4010BBC44FE2473F404C7BCC05834D5E4044E4C34EE2473F40EEA00F1F834D5E409D4C2C4DE2473F4086F08639834D5E4031EC314BE2473F405EB65E52834D5E40FF960949E2473F40373F1770834D5E40381E1C46E2473F4074FF5687834D5E40BC88C143E2473F40D3AD67A0834D5E400ADC3C41E2473F40BD96CABA834D5E40D647A23EE2473F40D78FB7D6834D5E405E0ADE3BE2473F406BBABFF6834D5E409921AA38E2473F4059B81C17844D5E4063B28B35E2473F40B35B123A844D5E4032768732E2473F40FED5505B844D5E409F840A30E2473F404EA37A7F844D5E406FF7BE2DE2473F40260896A4844D5E40B7C6D82BE2473F40AA7B80C8844D5E407F89602AE2473F409B9403EF844D5E402132FB28E2473F40ED682B15854D5E405254AB27E2473F405E7B993A854D5E4031AB7826E2473F40DD135E61854D5E40C4315A25E2473F40D6B79187854D5E4082054324E2473F40A1F4C7AC854D5E4086AE2023E2473F40100D62D2854D5E4038F00922E2473F40DE3D40F7854D5E4097022221E2473F409613CE1D864D5E40C2A07020E2473F40CD39BA42864D5E4072015920E2473F40928EA068864D5E40B84DE320E2473F40D397828E864D5E40C00A3A22E2473F40100430B4864D5E404FF56724E2473F406BB44FD9864D5E4081F94827E2473F40232C6CFE864D5E408AEF942AE2473F4038648721874D5E403DAFC02DE2473F40EC756241874D5E4054C7C530E2473F40E6D8C35C874D5E401E6C7E33E2473F40CE25C478874D5E40A76E5C36E2473F406FC9F897874D5E40EE3EA639E2473F40ADE17DB0874D5E401CD8493CE2473F40524CB0C8874D5E40E6E0F03EE2473F4066EBB3DF874D5E40604A8041E2473F407F7207F6874D5E402B1B0444E2473F40CEA6190E884D5E40AF4EB646E2473F4080F51329884D5E407ACEF249E2473F400B884147884D5E40C5419D4DE2473F4000FD2663884D5E40E6930B51E2473F4071B46E79884D5E4044ADD053E2473F40C32D7090884D5E4038D7B356E2473F40FA63D1A9884D5E40F728DF59E2473F4074C01DC4884D5E40E1C7115DE2473F40ADAC35E0884D5E402F2F6460E2473F404FECCCFD884D5E407537D163E2473F408C82431C894D5E4025674367E2473F40C7383A3F894D5E40DED01E6BE2473F40E3A3C060894D5E407414B56EE2473F40BB3FCF80894D5E4000560572E2473F40D783D79B894D5E4021F9BA74E2473F40CDEB92B5894D5E40BA814077E2473F40A3AA6CCC894D5E40B3937379E2473F40DF7D02E6894D5E409FE7DB7BE2473F400DCB41FE894D5E4046921A7EE2473F404B00660D8A4D5E40097A7C7FE2473F4014A484268A4D5E4067D1E180E2473F407B34B93E8A4D5E40D4AEEE81E2473F4039F5E1568A4D5E4090ECA082E2473F4061D296758A4D5E407904C990E2473F40844F368F8A4D5E402E95809EE2473F402DC238AB8A4D5E40FE8F56B0E2473F40DD36DFCA8A4D5E400B7901C8E2473F40987B25E98A4D5E4038D996E2E2473F4005CAE0078B4D5E4089544902E3473F40DE5FC3278B4D5E40B28A7327E3473F40DE3FD93E8B4D5E4019C56445E3473F40E48C2E5D8B4D5E40C1CAB070E3473F40D36A0B798B4D5E400F8D109DE3473F4009B4C2948B4D5E40D77A5FCDE3473F40219FB3AC8B4D5E404DE1E8FAE3473F4064988AC28B4D5E40EEE5C027E4473F4024E8EBD68B4D5E4083589954E4473F40783B41E98B4D5E407F55937FE4473F40AC63A6FE8B4D5E40318B50B5E4473F4018194A118C4D5E40BD9F73E7E4473F40B4DE77258C4D5E4019B96421E5473F400A7DE8368C4D5E40D4DDC056E5473F40FA19F3478C4D5E401E12C78DE5473F407A88585A8C4D5E408A9390CBE5473F402BB6006F8C4D5E4042DB4D13E6473F4066A6357F8C4D5E40754A984DE6473F40259127908C4D5E4012AAF48CE6473F40AF26A0A08C4D5E40CA08D4CCE6473F4071B4F2B18C4D5E4009831312E7473F40766204C48C4D5E403CABF55CE7473F40E0BF62D58C4D5E40D95ECBA7E7473F408A4F04E88C4D5E409B346BFBE7473F40877265FA8C4D5E40CD84FB51E8473F4024D4F50C8D4D5E4070D33BAEE8473F40426664198D4D5E40734DD1EEE8473F408DBC6F268D4D5E40CDD93A35E9473F402CA00E338D4D5E40671B267CE9473F401B23C53F8D4D5E40DB11AEC6E9473F401B13504B8D4D5E403A6D430DEA473F40CA18A8578D4D5E400450515CEA473F406C4136638D4D5E4089AEB1AAEA473F409B3A4D6E8D4D5E400E0795FAEA473F4041A8CA788D4D5E406B7DAE4AEB473F409D070D838D4D5E4052A55E9EEB473F40AFAF878C8D4D5E402171ECF1EB473F4060EDA4958D4D5E40B9DBB549EC473F40A4E39A9D8D4D5E40B2EECD9EEC473F40B2E1B6A48D4D5E40F8808AF4EC473F40A6DA23AB8D4D5E40F48EF74CED473F407F2555B08D4D5E408C94A29FED473F40DE007AB48D4D5E403B6586EDED473F40E6FCF1B78D4D5E409F3B9C5CEE473F40BDD589B78D4D5E4084AC14AAEE473F40BCDBB5B78D4D5E4016F76A02EF473F40CCF742B78D4D5E40317F2636EF473F40AD0A44B78D4D5E406CCA9A3BEF473F4094B44DB78D4D5E40562A108CEF473F404E4973B78D4D5E40F34F2EDEEF473F40AA169CB78D4D5E40429A9638F0473F404C4F9FB78D4D5E40FE1C4B89F0473F40E9F099B78D4D5E40CABD3DE2F0473F4021348FB78D4D5E40F8BF2F2FF1473F40F6187FB78D4D5E40F6CB3C7DF1473F40421B62B78D4D5E40DBBE59E0F1473F40B3A14CB78D4D5E4090F5CF31F2473F40116949B78D4D5E402C36728DF2473F40D28E4BB78D4D5E409C6000E4F2473F40170052B78D4D5E40A5D9EE35F3473F40DEBC5CB78D4D5E4009F30A83F3473F40AC1070B78D4D5E40B9ACCCD8F3473F40C36C92B78D4D5E40CD572E3CF4473F403990B1B78D4D5E403C4F448FF4473F40AFB3D0B78D4D5E40D656F1E8F4473F408835F5B78D4D5E40EA5FC44DF5473F40674E22B88D4D5E40F81261BDF5473F409E9743B88D4D5E404E1D6B0BF6473F40393F6AB88D4D5E40B07C0460F6473F4070888BB88D4D5E40BC6E8AB9F6473F405DC99DB88D4D5E40F4CDE815F7473F406360A6B88D4D5E40A0838D77F7473F40FF01A1B88D4D5E4072986CDBF7473F407DB070B88D4D5E400B8A2742F8473F40369C09B88D4D5E401704CFAFF8473F4029C56BB78D4D5E404DA65F21F9473F40DE0DA4B68D4D5E40C467DD91F9473F40C26594B58D4D5E404227840EFA473F40CC3B60B48D4D5E406E720C89FA473F40CBE31AB38D4D5E4027A04300FB473F400FF7AFB18D4D5E40B362027FFB473F40603856B08D4D5E408991BEFDFB473F40CE6C27AF8D4D5E40DCAE9B83FC473F40BDF228AE8D4D5E40F3B95709FD473F40753535AD8D4D5E40B02C6599FD473F40ECA36FAC8D4D5E40E4A21125FE473F402D7215AC8D4D5E40C6894D71FE473F40B5AB95AB8D4D5E4057C675F9FE473F40BD3646AB8D4D5E40C97B3E8AFF473F40BF301AAB8D4D5E40E740061F00483F40490DFBAA8D4D5E4025A306B800483F40519ED7AA8D4D5E40FE4D8C4A01483F40EF39A6AA8D4D5E40AB21CEDB01483F40C5186AAA8D4D5E40C2CA9D6802483F40B54D24AA8D4D5E40156A38EA02483F409EEBD5A98D4D5E4045AFE56203483F40E65084A98D4D5E400F286FCD03483F40CFEE35A98D4D5E40F0DF942704483F40C6B4CCA88D4D5E40451C2C9104483F40C9A874A88D4D5E40803A5ADF04483F40A71815A88D4D5E40997D0D2E05483F40669BB6A78D4D5E409612857B05483F403D744EA78D4D5E40811A06C905483F40B2EEE0A68D4D5E404BF0E91506483F40266973A68D4D5E405931576406483F40179801A68D4D5E409FA9BFB506483F4046A18DA58D4D5E409444700807483F40FF8C26A58D4D5E400AE6355C07483F405E4EF7A48D4D5E403F8132B007483F40405BCCA48D4D5E40DE10BC0008483F40E48DA3A48D4D5E4073AAF94D08483F40517759A48D4D5E40EEF69EA708483F40B232FEA38D4D5E402331DF0709483F40F997ACA38D4D5E40422B6C5909483F403B6652A38D4D5E4022D0EAB209483F40769DEFA28D4D5E404E2F63190A483F405AA498A28D4D5E4062A94E800A483F40B3CE60A28D4D5E402A40C0CC0A483F4000CB17A28D4D5E40301DE23B0B483F407F79E7A18D4D5E40949B1C8E0B483F40FD27B7A18D4D5E403B8C8DE50B483F409BC385A18D4D5E404C02753E0C483F40D5004FA18D4D5E406360AC960C483F40865B0BA18D4D5E4034AC7EF50C483F401232C0A08D4D5E403B853E530D483F4073ED64A08D4D5E40D965F6BC0D483F40128307A08D4D5E4079E70C200E483F40AC81A19F8D4D5E4038829C840E483F4020FC339F8D4D5E403C3DEBE90E483F40D250C49E8D4D5E40B126304C0F483F402D75609E8D4D5E40D33436A00F483F405150DB9D8D4D5E40D8806E0A10483F40A6DD6E9D8D4D5E401054305B10483F40B6F9FB9C8D4D5E40B1809DAD10483F409806D19C8D4D5E40BCA29BCA10483F40A485299C8D4D5E4029E3791711483F40615F3E9B8D4D5E404DE6577011483F406007F9998D4D5E409080F1CE11483F4030AD259B8D4D5E40004ADC1712483F4000F5E09A8D4D5E4023ED466812483F409B9C079B8D4D5E40F9FD65B712483F400541CE9B8D4D5E401FF2B90313483F409B58EA9D8D4D5E40E54CEB5B13483F404F12EAA08D4D5E40ED9F8BAC13483F401446E8A48D4D5E40343795FB13483F40A62B6BAA8D4D5E401B59C85014483F40381E18B28D4D5E40D804FDAE14483F40006710BC8D4D5E4057277B0C15483F40B2B157C78D4D5E40B7E9305F15483F4015AFA0D38D4D5E40E41C99A815483F40FF05EAE38D4D5E40753E6FF315483F40A04EF8F38D4D5E40AE2A6E2916483F4057B537078E4D5E4071503E5D16483F4021DB641A8E4D5E40055F288A16483F40A838182E8E4D5E40D902C3B416483F406E1F82428E4D5E4014DABADF16483F407D1427578E4D5E402DAA0E0817483F4086DD5E6F8E4D5E4004E4DF3017483F405B44F3848E4D5E40570D1B4C17483F40B604889E8E4D5E40B9E6C65F17483F40E53E71B48E4D5E40FE8B756817483F402AFB28CF8E4D5E408135EB6917483F40CF07EAE58E4D5E40B6F9CB6317483F40DC0FE5FC8E4D5E404DBDBF5817483F40352A38088F4D5E4059DEA65617483F4007AA4E208F4D5E40D7C51A5A17483F40BBA4202E8F4D5E4094291B5C17483F40C3665A448F4D5E4039E0B15E17483F40C44D6E5D8F4D5E40DBA2A06117483F4043A560798F4D5E40A21B8E6417483F402E8897938F4D5E4004C50C6717483F409FB7A4AF8F4D5E402161736917483F40D143A7C68F4D5E40F27D356B17483F40415F8CDE8F4D5E40EA18D36C17483F400E5CC2FA8F4D5E404049766E17483F40816B4F12904D5E400B589A6F17483F40A0386429904D5E4060D9847017483F40AF8B7A3F904D5E401C17377117483F40578CE355904D5E409A42C87117483F40BB42C472904D5E40CE654A7217483F40121CDB8E904D5E405AEBB77217483F405B82F1AB904D5E404ED04D7317483F400C0E0BC2904D5E406A33BF7317483F4048FBF4DF904D5E40E85E507417483F40433ED6F6904D5E40FE2AB97417483F4000052B0F914D5E4013F7217517483F40F1714027914D5E405F76C67517483F4003820B42914D5E401594057717483F4006C1645C914D5E40B1FD8B7817483F40E7CDCF77914D5E40172B6C7A17483F407D81E493914D5E406ED2A47C17483F40875E44B0914D5E401984327F17483F4062E1D0CD914D5E409C8B198217483F4025083BEB914D5E406468F58417483F403D1AAA09924D5E40C0DE598717483F40E303A029924D5E409892248917483F40104AE13F924D5E403EB3268A17483F40ACFF995F924D5E4084AE698B17483F4084ECEF7E924D5E4035998E8C17483F40DA943FA0924D5E401D37EF8D17483F40ADF18AC1924D5E40574FEB8F17483F403F3F5ED8924D5E40CB3A969117483F403F1F74EF924D5E403EFA759317483F4064D4D805934D5E40E239779517483F408B903B1E934D5E408F17D09717483F409EDEF735934D5E40D269359A17483F403FFD3C4D934D5E408B759F9C17483F40E904A465934D5E40C7681F9F17483F40B8E1597D934D5E402AA257A117483F4012447D95934D5E40CFA935A317483F401B0DB5AD934D5E40896AD5A417483F40DBC7B1C5934D5E40F3533AA617483F4030E413DD934D5E4092B168A717483F400B4D4CF6934D5E40B4BE89A817483F40717F0F0D944D5E40C5CE6DA917483F4088702C25944D5E4054F73BAA17483F40F497693D944D5E404878E3AA17483F40BAE96E55944D5E40E02B62AB17483F40438FF275944D5E409EF9CDAB17483F40FED33894944D5E40E79DEEAB17483F40FB88B3B0944D5E408471E0AB17483F40B41045CA944D5E402C0FF5AB17483F40EEDCDCE1944D5E406FB81EAC17483F40BD5B21F8944D5E405B634BAC17483F40EF550A05954D5E40477266AC17483F408A8CFF1C954D5E40F5553CAD17483F40DA5C7433954D5E40100478AE17483F403BF82E4A954D5E404841F0AF17483F403B36B662954D5E408FB3C8B117483F40488FF878954D5E40080AB1B317483F40F6E24C92954D5E4038CF1FB617483F4070E127AB954D5E4002D8C6B817483F40358AA0C2954D5E40035C86BB17483F40CBDF43DD954D5E401185ECBE17483F406C18DDF8954D5E403B11C4C217483F40219F9C10964D5E40864C4BC617483F40B3F99929964D5E40D9E6B7C917483F40F8069943964D5E4073371ACC17483F408F05C95E964D5E4076D4FDCD17483F40BC655E79964D5E40229FAFCF17483F402A435294964D5E40D1F91AD117483F40BDA44DAF964D5E40E54871D217483F40050466CA964D5E40E7369CD317483F4017B6BFE3964D5E40799EDCD417483F4065F7FBFD964D5E405DE4A6D617483F40F40C0A18974D5E40B5B7AAD817483F40197E5131974D5E40BCBFCBDA17483F4090CCA147974D5E40D6B8D1DC17483F4064A4FD60974D5E40253948DF17483F408A533677974D5E4017EC95E117483F40CA071E8E974D5E40E2BC19E417483F407EB4F38E974D5E400DA632E417483F403EC037A6974D5E4086B150E517483F40E18709BE974D5E40BDC87AE517483F402CBB49D4974D5E40637864E417483F401AE36FED974D5E4081139BE117483F402B8ECB04984D5E40EBD581DC17483F4071372D1D984D5E40B4C2D2D317483F40D1DF1136984D5E404533DBC517483F405A44C34B984D5E40294FF5B417483F409C43C661984D5E404950359F17483F40C8890778984D5E4080D1E38317483F40C2235C8E984D5E401ADB0F6317483F40A2BE2DA4984D5E40B37F1D3D17483F4064B1EFB8984D5E40A04C4F1317483F40511635CD984D5E40E373EDE316483F40ACAF4BE0984D5E40D6450EB016483F403E3412F2984D5E40D1249D7716483F40015EC701994D5E40FB31343E16483F4081619110994D5E407059510016483F405837DE1D994D5E40871662C015483F400543622A994D5E402E9D9F7A15483F403EC72935994D5E408ABD5C3315483F40E0E28D3E994D5E4082CFDAE714483F407FFD1F46994D5E408ACBEE9B14483F405E8E124C994D5E404B0A604D14483F40BD693750994D5E40AA99D5FD13483F406DD74952994D5E40484766AB13483F40110A2152994D5E4025ABA85E13483F40C6F4E74F994D5E406F031A0B13483F40A4F9EC4B994D5E4085E144B912483F4018B89C48994D5E4053542B8312483F40F0903448994D5E40049CDC3212483F4029D42948994D5E403DFEBDE211483F4091CF6348994D5E40E394709111483F4023ECD948994D5E401B9A104511483F400FDCA449994D5E40838DB8EE10483F400C97B54A994D5E401694429110483F40DD3CE24B994D5E40C0CA233410483F4038BFEF4C994D5E40B005F7E20F483F4079EB064E994D5E40391E128B0F483F40B0E3E04E994D5E40BE3C08350F483F407FE0804F994D5E40D1B442DC0E483F404FD7F44F994D5E40C7D1E88B0E483F403FBB6750994D5E409EE58A2E0E483F400384CA50994D5E40CA8BBCCB0D483F4072160D51994D5E4096A17A7F0D483F4038D94351994D5E40A8F01B330D483F4036DF6F51994D5E40D363BCE60C483F406D289151994D5E40F412B7980C483F40FCA1A651994D5E4083092A4A0C483F400239AF51994D5E401C3229F30B483F40BDC7A851994D5E4062DB3FA20B483F4092AC9851994D5E4099FEAE510B483F401D897951994D5E405E81DFFC0A483F40E0A84F51994D5E40A41DCCA90A483F40979A1451994D5E40542B58520A483F40C5A9CC50994D5E40E86B07FC09483F404BE97850994D5E401C4957A709483F40A60D1550994D5E404CE6625009483F40B092994F994D5E40B2D708F308483F40D46D144F994D5E40DDE9059908483F40A0189B4E994D5E402BEA243F08483F403286584E994D5E408A7D15E707483F40D4BE5B4E994D5E4060F6D58E07483F40A6AFA34E994D5E4007660A3707483F405D50214F994D5E40040C5FDF06483F4045A3B74F994D5E40CEECA28806483F403FBB6750994D5E400540582F06483F40A3C82551994D5E40C41E56D805483F408FB8F051994D5E401370BC7D05483F409B95BA52994D5E405248A52505483F40C0C87A53994D5E4065626ECB04483F4051EB1C54994D5E402D61817004483F409705B054994D5E40F601231404483F4079C13D55994D5E4031AC3EB503483F40E45FD855994D5E401158BB5903483F408CDE9C56994D5E408C03530103483F40723D8B57994D5E40FA8656A702483F402A879558994D5E406BD8B34B02483F408BA0AB59994D5E40D17FBFF201483F407A33D75A994D5E40D8BCBE9701483F4051700C5C994D5E40E758A33C01483F402116395D994D5E40C94392E100483F404355515E994D5E40F2B1FE8100483F40867B3C5F994D5E40228C7A2400483F4035910960994D5E404CC2C3C4FF473F40BF1CA360994D5E400FF9576DFF473F40B5971E61994D5E40F26EB318FF473F4015027C61994D5E40A72ECCCAFE473F40577FDA61994D5E40AC188269FE473F4042C61862994D5E4088087A0EFE473F403A353C62994D5E4006D5B4BAFD473F40C3174962994D5E4005E1F969FD473F4060B94362994D5E403742671BFD473F4092653062994D5E403416E7CDFC473F4036980762994D5E40E4164974FC473F40B546D761994D5E409F6D6A26FC473F40FF4EE661994D5E40E10208D3FB473F40B7E93762994D5E40D8839C82FB473F40EC3EB162994D5E40B52BFC32FB473F4076CA4A63994D5E4003F657E3FA473F40379F0564994D5E4033317793FA473F406D9D0B65994D5E409EAD2B39FA473F4070EF2466994D5E406A25D6E5F9473F4058F17367994D5E40D851158EF9473F4067719E68994D5E40EDA70542F9473F40516DC169994D5E40ED7C57F6F8473F4009B7CB6A994D5E40B2B5EDA5F8473F40BD63A16B994D5E403205B84FF8473F400284606C994D5E401E9B28F7F7473F4066911E6D994D5E40375FA19DF7473F40852DD66D994D5E4073C59F41F7473F40A4C98D6E994D5E4027F719E1F6473F40B737346F994D5E40FB036D83F6473F4023D6CE6F994D5E4003E97725F6473F402A166470994D5E401F5A49C2F5473F40C860EB70994D5E409265295FF5473F4009DE4971994D5E40117F3013F5473F4045C49F71994D5E40619528C6F4473F401D4CF071994D5E40AB6BDC76F4473F404C043572994D5E40E1825129F4473F4057387272994D5E40A9CF60DBF3473F401DFBA872994D5E40C18E8C88F3473F401B01D572994D5E4067C15037F3473F404C3AF372994D5E40D5503DF1F2473F40
\.


--
-- Data for Name: spatial_ref_sys; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.spatial_ref_sys (srid, auth_name, auth_srid, srtext, proj4text) FROM stdin;
\.


--
-- Data for Name: stop_point_candidate; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.stop_point_candidate (id) FROM stdin;
472
589
55
206
329
\.


--
-- Data for Name: stop_point_in_buffer; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.stop_point_in_buffer (p_id, utmx, utmy, heading, curv, mode, speed_mode, event_mode, opposite_side_mode, lane_num, lane_seq, lane_width, geom) FROM stdin;
1	329775.28	3.4621802e+06	1.5724922	0	1	3	1	3	2	1	3	0101000020E61000009806D19C8D4D5E40BCA29BCA10483F40
3	329833.28	3.4621912e+06	0.012198874	0	1	3	1	3	2	1	3	0101000020E61000007EB4F38E974D5E400DA632E417483F40
9	329709.1	3.4622015e+06	-3.1081436	0	1	3	1	3	2	1	3	0101000020E610000098FEA629824D5E40FD771CBD1C483F40
10	329687.28	3.4621218e+06	-1.6098561	0	1	3	1	3	2	1	3	0101000020E6100000302D61A27E4D5E40246EA16DED473F40
11	329753.34	3.4621022e+06	0.011292868	0	1	3	1	3	2	1	3	0101000020E61000004B00660D8A4D5E40097A7C7FE2473F40
\.


--
-- Data for Name: stop_points; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.stop_points (p_id, utmx, utmy, heading, curv, mode, speed_mode, event_mode, opposite_side_mode, lane_num, lane_seq, lane_width, geom) FROM stdin;
0	329764.25	3.4621245e+06	-1.573572	0	1	3	1	3	2	1	3	0101000020E61000005EAAA4DE8B4D5E400D909CCFEF473F40
1	329775.28	3.4621802e+06	1.5724922	0	1	3	1	3	2	1	3	0101000020E61000009806D19C8D4D5E40BCA29BCA10483F40
2	329753.66	3.462191e+06	0.0032000542	0	1	3	1	3	2	1	3	0101000020E6100000876AADDC894D5E40175684F316483F40
3	329833.28	3.4621912e+06	0.012198874	0	1	3	1	3	2	1	3	0101000020E61000007EB4F38E974D5E400DA632E417483F40
4	329843.2	3.462125e+06	-1.589001	0	1	3	1	3	2	1	3	0101000020E61000009E4CD972994D5E4066C822DDF0473F40
5	329786.8	3.4621128e+06	3.131791	0	1	3	1	3	2	1	3	0101000020E6100000884296C88F4D5E4041BE5119E9473F40
6	329713.16	3.4621128e+06	-3.1401467	0	1	3	1	3	2	1	3	0101000020E61000004321791C834D5E400CD01154E8473F40
7	329697.88	3.4621805e+06	1.561506	0	1	3	1	3	2	1	3	0101000020E610000055A59A4A804D5E4078DAEF4710483F40
8	329784.88	3.462202e+06	3.1240144	0	1	3	1	3	2	1	3	0101000020E610000066E099338F4D5E40C6A5A3C51D483F40
9	329709.1	3.4622015e+06	-3.1081436	0	1	3	1	3	2	1	3	0101000020E610000098FEA629824D5E40FD771CBD1C483F40
10	329687.28	3.4621218e+06	-1.6098561	0	1	3	1	3	2	1	3	0101000020E6100000302D61A27E4D5E40246EA16DED473F40
11	329753.34	3.4621022e+06	0.011292868	0	1	3	1	3	2	1	3	0101000020E61000004B00660D8A4D5E40097A7C7FE2473F40
12	329832.56	3.462102e+06	0.0012159994	0	1	3	1	3	2	1	3	0101000020E610000021B06DAF974D5E409F6B631DE3473F40
13	329854.1	3.4621805e+06	1.5732495	0	1	3	1	3	2	1	3	0101000020E610000066B0812B9B4D5E40ED7381B611483F40
\.


--
-- Data for Name: topo_map; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.topo_map (gid, l_id, the_geom, source, target, speed, length, cost, geog) FROM stdin;
8	4	0102000020E61000001C0000006007F9998D4D5E409080F1CE11483F4030AD259B8D4D5E40004ADC1712483F4000F5E09A8D4D5E4023ED466812483F409B9C079B8D4D5E40F9FD65B712483F400541CE9B8D4D5E401FF2B90313483F409B58EA9D8D4D5E40E54CEB5B13483F404F12EAA08D4D5E40ED9F8BAC13483F401446E8A48D4D5E40343795FB13483F40A62B6BAA8D4D5E401B59C85014483F40381E18B28D4D5E40D804FDAE14483F40006710BC8D4D5E4057277B0C15483F40B2B157C78D4D5E40B7E9305F15483F4015AFA0D38D4D5E40E41C99A815483F40FF05EAE38D4D5E40753E6FF315483F40A04EF8F38D4D5E40AE2A6E2916483F4057B537078E4D5E4071503E5D16483F4021DB641A8E4D5E40055F288A16483F40A838182E8E4D5E40D902C3B416483F406E1F82428E4D5E4014DABADF16483F407D1427578E4D5E402DAA0E0817483F4086DD5E6F8E4D5E4004E4DF3017483F405B44F3848E4D5E40570D1B4C17483F40B604889E8E4D5E40B9E6C65F17483F40E53E71B48E4D5E40FE8B756817483F402AFB28CF8E4D5E408135EB6917483F40CF07EAE58E4D5E40B6F9CB6317483F40DC0FE5FC8E4D5E404DBDBF5817483F40352A38088F4D5E4059DEA65617483F40	1	2	1	14.698460047865089	14.698460047865089	0105000020E61000000100000001020000001C0000006007F9998D4D5E409080F1CE11483F4030AD259B8D4D5E40004ADC1712483F4000F5E09A8D4D5E4023ED466812483F409B9C079B8D4D5E40F9FD65B712483F400541CE9B8D4D5E401FF2B90313483F409B58EA9D8D4D5E40E54CEB5B13483F404F12EAA08D4D5E40ED9F8BAC13483F401446E8A48D4D5E40343795FB13483F40A62B6BAA8D4D5E401B59C85014483F40381E18B28D4D5E40D804FDAE14483F40006710BC8D4D5E4057277B0C15483F40B2B157C78D4D5E40B7E9305F15483F4015AFA0D38D4D5E40E41C99A815483F40FF05EAE38D4D5E40753E6FF315483F40A04EF8F38D4D5E40AE2A6E2916483F4057B537078E4D5E4071503E5D16483F4021DB641A8E4D5E40055F288A16483F40A838182E8E4D5E40D902C3B416483F406E1F82428E4D5E4014DABADF16483F407D1427578E4D5E402DAA0E0817483F4086DD5E6F8E4D5E4004E4DF3017483F405B44F3848E4D5E40570D1B4C17483F40B604889E8E4D5E40B9E6C65F17483F40E53E71B48E4D5E40FE8B756817483F402AFB28CF8E4D5E408135EB6917483F40CF07EAE58E4D5E40B6F9CB6317483F40DC0FE5FC8E4D5E404DBDBF5817483F40352A38088F4D5E4059DEA65617483F40
4	9	0102000020E61000002D00000051D7F1978A4D5E4029DE2DFA16483F4067C80EB08A4D5E40EB16D7FA16483F40EFEECEC88A4D5E405C4C7AFB16483F40AFB6F5E28A4D5E40635A18FC16483F40CE9D5EFE8A4D5E40C266B3FC16483F403D82501B8B4D5E40E89347FD16483F4067FB67388B4D5E405496D0FD16483F40DF7734578B4D5E40B93974FE16483F40865400758B4D5E40532C65FF16483F4000CBA0938B4D5E402905AC0017483F40134D05B48B4D5E40D6334C0217483F401216A8D48B4D5E406BB5820417483F405E49E8EA8B4D5E40EB6A500617483F401EA673018C4D5E40F2D65F0817483F404D8F15188C4D5E4036F1A10A17483F4071F95E2F8C4D5E401B4A130D17483F40600E2F468C4D5E40FF3E960F17483F40E6DCA95D8C4D5E40E1074E1217483F403AAEF0758C4D5E407105231517483F40597B058D8C4D5E402F7CCA1717483F4078F7D2A48C4D5E40CD376A1A17483F40E812B8BC8C4D5E4003F8CF1C17483F40853D55D48C4D5E406F58CA1E17483F40260B53EC8C4D5E40BD29882017483F40BFF62C058D4D5E40E2751B2217483F40166BD41D8D4D5E404C5F802317483F40D42BFD358D4D5E4086C8C32417483F4078A985508D4D5E40F1B1282617483F4064281F698D4D5E40BFC79B2717483F40121E02818D4D5E4008663F2917483F4026CA2F9A8D4D5E40F17A352B17483F40B09350B38D4D5E40AC16592D17483F402F80A7CB8D4D5E407977962F17483F40084D7DE68D4D5E4011383F3217483F40D2EA6FFF8D4D5E401A1BE43417483F405F06AA198E4D5E4052B6CD3717483F40DF9B8D328E4D5E40E14FB43A17483F40A1A1A5498E4D5E407F0B543D17483F40133622698E4D5E406BBD2D4117483F40D0C583868E4D5E403718EE4417483F4074F2C4A18E4D5E40F9A88B4817483F40DE255ABA8E4D5E400836E04B17483F406DDDF6D28E4D5E400A31354F17483F405323F9EC8E4D5E4023C0CF5217483F40352A38088F4D5E4059DEA65617483F40	3	2	1	25.80547659884517	25.80547659884517	0105000020E61000000100000001020000002D00000051D7F1978A4D5E4029DE2DFA16483F4067C80EB08A4D5E40EB16D7FA16483F40EFEECEC88A4D5E405C4C7AFB16483F40AFB6F5E28A4D5E40635A18FC16483F40CE9D5EFE8A4D5E40C266B3FC16483F403D82501B8B4D5E40E89347FD16483F4067FB67388B4D5E405496D0FD16483F40DF7734578B4D5E40B93974FE16483F40865400758B4D5E40532C65FF16483F4000CBA0938B4D5E402905AC0017483F40134D05B48B4D5E40D6334C0217483F401216A8D48B4D5E406BB5820417483F405E49E8EA8B4D5E40EB6A500617483F401EA673018C4D5E40F2D65F0817483F404D8F15188C4D5E4036F1A10A17483F4071F95E2F8C4D5E401B4A130D17483F40600E2F468C4D5E40FF3E960F17483F40E6DCA95D8C4D5E40E1074E1217483F403AAEF0758C4D5E407105231517483F40597B058D8C4D5E402F7CCA1717483F4078F7D2A48C4D5E40CD376A1A17483F40E812B8BC8C4D5E4003F8CF1C17483F40853D55D48C4D5E406F58CA1E17483F40260B53EC8C4D5E40BD29882017483F40BFF62C058D4D5E40E2751B2217483F40166BD41D8D4D5E404C5F802317483F40D42BFD358D4D5E4086C8C32417483F4078A985508D4D5E40F1B1282617483F4064281F698D4D5E40BFC79B2717483F40121E02818D4D5E4008663F2917483F4026CA2F9A8D4D5E40F17A352B17483F40B09350B38D4D5E40AC16592D17483F402F80A7CB8D4D5E407977962F17483F40084D7DE68D4D5E4011383F3217483F40D2EA6FFF8D4D5E401A1BE43417483F405F06AA198E4D5E4052B6CD3717483F40DF9B8D328E4D5E40E14FB43A17483F40A1A1A5498E4D5E407F0B543D17483F40133622698E4D5E406BBD2D4117483F40D0C583868E4D5E403718EE4417483F4074F2C4A18E4D5E40F9A88B4817483F40DE255ABA8E4D5E400836E04B17483F406DDDF6D28E4D5E400A31354F17483F405323F9EC8E4D5E4023C0CF5217483F40352A38088F4D5E4059DEA65617483F40
5	5	0102000020E61000001900000051D7F1978A4D5E4029DE2DFA16483F4078ADA8B48A4D5E402C1C89DB16483F402C47BECE8A4D5E40C66889CC16483F405ED3C0E58A4D5E406EEF94BA16483F40344153FD8A4D5E40B78027A016483F403D4EA8128B4D5E40228DC17C16483F4055708C258B4D5E406970E44C16483F40E4F380358B4D5E407ED7601416483F40D2DAD4438B4D5E40752434D515483F407C43F84F8B4D5E40C2D1F08E15483F40162CF15A8B4D5E40D786824915483F401C04CC658B4D5E40C60D50FE14483F40A578F26F8B4D5E40928EA9B514483F40AD9BE8798B4D5E407D3F9E6614483F40E4C76A838B4D5E4045652F1214483F40B1F1B48A8B4D5E4055D3CFC613483F40714461918B4D5E4087EFEF7513483F40C6F872978B4D5E40F474532613483F40AD1A429D8B4D5E408871E5D212483F4041F46CA28B4D5E4039684F8312483F40BD77A1A78B4D5E408FF44E2E12483F40A14757AC8B4D5E407284FEDD11483F40332DDAB18B4D5E40B3F34D8911483F40098463B78B4D5E407D32033411483F407AD35ABC8B4D5E4063723DE610483F40	3	4	1	13.341525328347373	13.341525328347373	0105000020E61000000100000001020000001900000051D7F1978A4D5E4029DE2DFA16483F4078ADA8B48A4D5E402C1C89DB16483F402C47BECE8A4D5E40C66889CC16483F405ED3C0E58A4D5E406EEF94BA16483F40344153FD8A4D5E40B78027A016483F403D4EA8128B4D5E40228DC17C16483F4055708C258B4D5E406970E44C16483F40E4F380358B4D5E407ED7601416483F40D2DAD4438B4D5E40752434D515483F407C43F84F8B4D5E40C2D1F08E15483F40162CF15A8B4D5E40D786824915483F401C04CC658B4D5E40C60D50FE14483F40A578F26F8B4D5E40928EA9B514483F40AD9BE8798B4D5E407D3F9E6614483F40E4C76A838B4D5E4045652F1214483F40B1F1B48A8B4D5E4055D3CFC613483F40714461918B4D5E4087EFEF7513483F40C6F872978B4D5E40F474532613483F40AD1A429D8B4D5E408871E5D212483F4041F46CA28B4D5E4039684F8312483F40BD77A1A78B4D5E408FF44E2E12483F40A14757AC8B4D5E407284FEDD11483F40332DDAB18B4D5E40B3F34D8911483F40098463B78B4D5E407D32033411483F407AD35ABC8B4D5E4063723DE610483F40
10	2	0102000020E61000001B000000EF9C51E68B4D5E407AFE98FCED473F402B3260E78B4D5E40A22037CEED473F4050A93DE58B4D5E40BD000680ED473F407B59B2E18B4D5E40717AFF2DED473F40A8A589DC8B4D5E402684B2DBEC473F407D63FBD58B4D5E4026328689EC473F4035795DCE8B4D5E406C390E3AEC473F40F0D9DAC58B4D5E4025718EEEEB473F40151D10BB8B4D5E40F7A58F9CEB473F40520DAFAF8B4D5E40E21F9152EB473F40D50AB7A28B4D5E40C42EB30BEB473F406D630F948B4D5E40837CAEC9EA473F4096CBB3838B4D5E406079B78AEA473F401F97B7718B4D5E403168804EEA473F401ED7215F8B4D5E406A790119EA473F40F397DE4A8B4D5E4031A756E7E9473F40333B53348B4D5E4028ADC1B8E9473F40768E52208B4D5E40F8C0ED95E9473F403A84C90B8B4D5E405B37AF77E9473F40A39FEDF48A4D5E4029E29F5BE9473F4017DCF8DB8A4D5E402E15D341E9473F40604E3BC28A4D5E400E099D28E9473F40FBFBF7A68A4D5E4060DA150DE9473F405232828B8A4D5E4079424BF0E8473F408B2AC6708A4D5E4025B521D5E8473F40DD92545A8A4D5E40960432C0E8473F407643F24C8A4D5E40D20727B7E8473F40	5	6	1	14.206612853337807	14.206612853337807	0105000020E61000000100000001020000001B000000EF9C51E68B4D5E407AFE98FCED473F402B3260E78B4D5E40A22037CEED473F4050A93DE58B4D5E40BD000680ED473F407B59B2E18B4D5E40717AFF2DED473F40A8A589DC8B4D5E402684B2DBEC473F407D63FBD58B4D5E4026328689EC473F4035795DCE8B4D5E406C390E3AEC473F40F0D9DAC58B4D5E4025718EEEEB473F40151D10BB8B4D5E40F7A58F9CEB473F40520DAFAF8B4D5E40E21F9152EB473F40D50AB7A28B4D5E40C42EB30BEB473F406D630F948B4D5E40837CAEC9EA473F4096CBB3838B4D5E406079B78AEA473F401F97B7718B4D5E403168804EEA473F401ED7215F8B4D5E406A790119EA473F40F397DE4A8B4D5E4031A756E7E9473F40333B53348B4D5E4028ADC1B8E9473F40768E52208B4D5E40F8C0ED95E9473F403A84C90B8B4D5E405B37AF77E9473F40A39FEDF48A4D5E4029E29F5BE9473F4017DCF8DB8A4D5E402E15D341E9473F40604E3BC28A4D5E400E099D28E9473F40FBFBF7A68A4D5E4060DA150DE9473F405232828B8A4D5E4079424BF0E8473F408B2AC6708A4D5E4025B521D5E8473F40DD92545A8A4D5E40960432C0E8473F407643F24C8A4D5E40D20727B7E8473F40
1	6	0102000020E61000003900000066E099338F4D5E40C6A5A3C51D483F4078DFF1208F4D5E40CC52D0841D483F405281000A8F4D5E4042AE63831D483F4021E2A7F08E4D5E40789215801D483F40374A8CD58E4D5E4059FB597A1D483F40890362BE8E4D5E404A036A731D483F40FD3F6DA58E4D5E40A28871681D483F4068420F8C8E4D5E40ADE4C6581D483F402AE66C768E4D5E4032F445471D483F4093AA1D608E4D5E4053BD62311D483F407D75E34A8E4D5E4019101B191D483F4094F71B348E4D5E40AFE181FB1C483F4077CD671D8E4D5E4014643DDA1C483F40021B7A058E4D5E40A7AFE2B21C483F40D85D45EB8D4D5E403E8364821C483F40FDBD19D78D4D5E405EAB3B591C483F4026A7FABE8D4D5E406C6FB5231C483F40F6D7ACAC8D4D5E406B58C5F71B483F40D862DD968D4D5E401E5B10BF1B483F40462234828D4D5E4023CBCC841B483F402323866E8D4D5E40F174A6491B483F401DC6BB5B8D4D5E40455E2C0E1B483F4063C9454A8D4D5E40808E4ED61A483F40815420398D4D5E404A45B0A01A483F40C2692E288D4D5E403B29A66B1A483F400D0AED168D4D5E4084D9F2341A483F40D0702B068D4D5E40C0CB74FE19483F400B9EE9F58C4D5E40351A10C719483F402BD250E58C4D5E40076DC78A19483F40F2260BD48C4D5E40517ED04719483F4028BDC0C38C4D5E40E177750519483F40CE8819B48C4D5E408FC833C318483F401AD264A38C4D5E409379DB7918483F4077373D928C4D5E402F15F52B18483F40889406818C4D5E40414A42DB17483F403D4427748C4D5E40DB5F819D17483F40816731678C4D5E40ACE3AE5D17483F401C12A3598C4D5E402474071917483F40D2BB974C8C4D5E4019CC02D516483F40C63F8A3F8C4D5E4008B8EE8E16483F408CB4C4328C4D5E4004B3EC4716483F40FBF2DE258C4D5E40B5C3AAFC15483F40557E63198C4D5E40AD77B7AF15483F408F1CE90C8C4D5E40B13F815E15483F40243D7C018C4D5E40125FB00E15483F40341837F68B4D5E40E69FABB914483F40A55D4FEB8B4D5E40FCA79F6014483F403ADC53E18B4D5E403653EB0614483F40128143D88B4D5E40DA63DCAB13483F402A5876D08B4D5E40BA0F595013483F40554C08CA8B4D5E404ECB18F212483F4024D1E2C48B4D5E40E2C3488D12483F40877305C28B4D5E40577D983F12483F406E10E5BF8B4D5E407BA1C8E711483F402BEAF9BE8B4D5E40015ED29811483F408D4E2BBF8B4D5E401A508F2F11483F407AD35ABC8B4D5E4063723DE610483F40	7	4	1	32.225750498212804	32.225750498212804	0105000020E61000000100000001020000003900000066E099338F4D5E40C6A5A3C51D483F4078DFF1208F4D5E40CC52D0841D483F405281000A8F4D5E4042AE63831D483F4021E2A7F08E4D5E40789215801D483F40374A8CD58E4D5E4059FB597A1D483F40890362BE8E4D5E404A036A731D483F40FD3F6DA58E4D5E40A28871681D483F4068420F8C8E4D5E40ADE4C6581D483F402AE66C768E4D5E4032F445471D483F4093AA1D608E4D5E4053BD62311D483F407D75E34A8E4D5E4019101B191D483F4094F71B348E4D5E40AFE181FB1C483F4077CD671D8E4D5E4014643DDA1C483F40021B7A058E4D5E40A7AFE2B21C483F40D85D45EB8D4D5E403E8364821C483F40FDBD19D78D4D5E405EAB3B591C483F4026A7FABE8D4D5E406C6FB5231C483F40F6D7ACAC8D4D5E406B58C5F71B483F40D862DD968D4D5E401E5B10BF1B483F40462234828D4D5E4023CBCC841B483F402323866E8D4D5E40F174A6491B483F401DC6BB5B8D4D5E40455E2C0E1B483F4063C9454A8D4D5E40808E4ED61A483F40815420398D4D5E404A45B0A01A483F40C2692E288D4D5E403B29A66B1A483F400D0AED168D4D5E4084D9F2341A483F40D0702B068D4D5E40C0CB74FE19483F400B9EE9F58C4D5E40351A10C719483F402BD250E58C4D5E40076DC78A19483F40F2260BD48C4D5E40517ED04719483F4028BDC0C38C4D5E40E177750519483F40CE8819B48C4D5E408FC833C318483F401AD264A38C4D5E409379DB7918483F4077373D928C4D5E402F15F52B18483F40889406818C4D5E40414A42DB17483F403D4427748C4D5E40DB5F819D17483F40816731678C4D5E40ACE3AE5D17483F401C12A3598C4D5E402474071917483F40D2BB974C8C4D5E4019CC02D516483F40C63F8A3F8C4D5E4008B8EE8E16483F408CB4C4328C4D5E4004B3EC4716483F40FBF2DE258C4D5E40B5C3AAFC15483F40557E63198C4D5E40AD77B7AF15483F408F1CE90C8C4D5E40B13F815E15483F40243D7C018C4D5E40125FB00E15483F40341837F68B4D5E40E69FABB914483F40A55D4FEB8B4D5E40FCA79F6014483F403ADC53E18B4D5E403653EB0614483F40128143D88B4D5E40DA63DCAB13483F402A5876D08B4D5E40BA0F595013483F40554C08CA8B4D5E404ECB18F212483F4024D1E2C48B4D5E40E2C3488D12483F40877305C28B4D5E40577D983F12483F406E10E5BF8B4D5E407BA1C8E711483F402BEAF9BE8B4D5E40015ED29811483F408D4E2BBF8B4D5E401A508F2F11483F407AD35ABC8B4D5E4063723DE610483F40
14	10	0102000020E61000002D00000039F5E1568A4D5E4090ECA082E2473F409413E8718A4D5E4028040E83E2473F405A3B24918A4D5E409DF53583E2473F40B2FAE6A88A4D5E4060E31483E2473F406AD3BFC18A4D5E40AE43BA82E2473F40C5A07EDD8A4D5E40C6F02382E2473F4071B6E1F98A4D5E4095C14981E2473F40BDBF58178B4D5E40EFA04780E2473F4048B0F7368B4D5E4058A2FE7EE2473F407D202D598B4D5E40B00A677DE2473F40CCF3827D8B4D5E40C62D947BE2473F40BC0508A28B4D5E402D54C779E2473F4028886BC98B4D5E4095DEE877E2473F404C5A05F28B4D5E40F0721C76E2473F40AF5EE21B8C4D5E40A6D47874E2473F4034F94A468C4D5E409F172273E2473F40ECD1235F8C4D5E40410B8772E2473F401D88858A8C4D5E407F36CC71E2473F405F436BA38C4D5E40305F9171E2473F4090D599BA8C4D5E40BA097B71E2473F40906468D28C4D5E408E588571E2473F40333338EC8C4D5E40DE93AE71E2473F40B81409068D4D5E403302F271E2473F407F6D0C208D4D5E4055C44872E2473F40F296ED3B8D4D5E40FA6DB572E2473F40234370578D4D5E40B2402A73E2473F404B5718718D4D5E40700E9673E2473F409B4E0B8E8D4D5E40012B0C74E2473F40833D9AA88D4D5E40DEB37F74E2473F40038F60C48D4D5E4051B1FF74E2473F4034480DE28D4D5E40869C5E75E2473F4014FD32FC8D4D5E4084709375E2473F405F9B0E168E4D5E409CCCB575E2473F401B2472318E4D5E403ED3C175E2473F40E8137E4B8E4D5E40C719BD75E2473F40BD873C638E4D5E4096999B75E2473F40DEAC967B8E4D5E4034356A75E2473F402449CE918E4D5E40A0EC2875E2473F4056F903BD8E4D5E40B9616F74E2473F40563440E38E4D5E402AD5B273E2473F403AAD18048F4D5E400DAA6473E2473F40B21CBB208F4D5E4045257D73E2473F40EBAA613B8F4D5E4008260374E2473F40A29690568F4D5E40BA741675E2473F40A089FC6F8F4D5E4087269B76E2473F40	8	9	1	29.630666149625945	29.630666149625945	0105000020E61000000100000001020000002D00000039F5E1568A4D5E4090ECA082E2473F409413E8718A4D5E4028040E83E2473F405A3B24918A4D5E409DF53583E2473F40B2FAE6A88A4D5E4060E31483E2473F406AD3BFC18A4D5E40AE43BA82E2473F40C5A07EDD8A4D5E40C6F02382E2473F4071B6E1F98A4D5E4095C14981E2473F40BDBF58178B4D5E40EFA04780E2473F4048B0F7368B4D5E4058A2FE7EE2473F407D202D598B4D5E40B00A677DE2473F40CCF3827D8B4D5E40C62D947BE2473F40BC0508A28B4D5E402D54C779E2473F4028886BC98B4D5E4095DEE877E2473F404C5A05F28B4D5E40F0721C76E2473F40AF5EE21B8C4D5E40A6D47874E2473F4034F94A468C4D5E409F172273E2473F40ECD1235F8C4D5E40410B8772E2473F401D88858A8C4D5E407F36CC71E2473F405F436BA38C4D5E40305F9171E2473F4090D599BA8C4D5E40BA097B71E2473F40906468D28C4D5E408E588571E2473F40333338EC8C4D5E40DE93AE71E2473F40B81409068D4D5E403302F271E2473F407F6D0C208D4D5E4055C44872E2473F40F296ED3B8D4D5E40FA6DB572E2473F40234370578D4D5E40B2402A73E2473F404B5718718D4D5E40700E9673E2473F409B4E0B8E8D4D5E40012B0C74E2473F40833D9AA88D4D5E40DEB37F74E2473F40038F60C48D4D5E4051B1FF74E2473F4034480DE28D4D5E40869C5E75E2473F4014FD32FC8D4D5E4084709375E2473F405F9B0E168E4D5E409CCCB575E2473F401B2472318E4D5E403ED3C175E2473F40E8137E4B8E4D5E40C719BD75E2473F40BD873C638E4D5E4096999B75E2473F40DEAC967B8E4D5E4034356A75E2473F402449CE918E4D5E40A0EC2875E2473F4056F903BD8E4D5E40B9616F74E2473F40563440E38E4D5E402AD5B273E2473F403AAD18048F4D5E400DAA6473E2473F40B21CBB208F4D5E4045257D73E2473F40EBAA613B8F4D5E4008260374E2473F40A29690568F4D5E40BA741675E2473F40A089FC6F8F4D5E4087269B76E2473F40
15	8	0102000020E61000003200000039F5E1568A4D5E4090ECA082E2473F4061D296758A4D5E407904C990E2473F40844F368F8A4D5E402E95809EE2473F402DC238AB8A4D5E40FE8F56B0E2473F40DD36DFCA8A4D5E400B7901C8E2473F40987B25E98A4D5E4038D996E2E2473F4005CAE0078B4D5E4089544902E3473F40DE5FC3278B4D5E40B28A7327E3473F40DE3FD93E8B4D5E4019C56445E3473F40E48C2E5D8B4D5E40C1CAB070E3473F40D36A0B798B4D5E400F8D109DE3473F4009B4C2948B4D5E40D77A5FCDE3473F40219FB3AC8B4D5E404DE1E8FAE3473F4064988AC28B4D5E40EEE5C027E4473F4024E8EBD68B4D5E4083589954E4473F40783B41E98B4D5E407F55937FE4473F40AC63A6FE8B4D5E40318B50B5E4473F4018194A118C4D5E40BD9F73E7E4473F40B4DE77258C4D5E4019B96421E5473F400A7DE8368C4D5E40D4DDC056E5473F40FA19F3478C4D5E401E12C78DE5473F407A88585A8C4D5E408A9390CBE5473F402BB6006F8C4D5E4042DB4D13E6473F4066A6357F8C4D5E40754A984DE6473F40259127908C4D5E4012AAF48CE6473F40AF26A0A08C4D5E40CA08D4CCE6473F4071B4F2B18C4D5E4009831312E7473F40766204C48C4D5E403CABF55CE7473F40E0BF62D58C4D5E40D95ECBA7E7473F408A4F04E88C4D5E409B346BFBE7473F40877265FA8C4D5E40CD84FB51E8473F4024D4F50C8D4D5E4070D33BAEE8473F40426664198D4D5E40734DD1EEE8473F408DBC6F268D4D5E40CDD93A35E9473F402CA00E338D4D5E40671B267CE9473F401B23C53F8D4D5E40DB11AEC6E9473F401B13504B8D4D5E403A6D430DEA473F40CA18A8578D4D5E400450515CEA473F406C4136638D4D5E4089AEB1AAEA473F409B3A4D6E8D4D5E400E0795FAEA473F4041A8CA788D4D5E406B7DAE4AEB473F409D070D838D4D5E4052A55E9EEB473F40AFAF878C8D4D5E402171ECF1EB473F4060EDA4958D4D5E40B9DBB549EC473F40A4E39A9D8D4D5E40B2EECD9EEC473F40B2E1B6A48D4D5E40F8808AF4EC473F40A6DA23AB8D4D5E40F48EF74CED473F407F2555B08D4D5E408C94A29FED473F40DE007AB48D4D5E403B6586EDED473F40E6FCF1B78D4D5E409F3B9C5CEE473F40	8	10	1	30.04372526214687	30.04372526214687	0105000020E61000000100000001020000003200000039F5E1568A4D5E4090ECA082E2473F4061D296758A4D5E407904C990E2473F40844F368F8A4D5E402E95809EE2473F402DC238AB8A4D5E40FE8F56B0E2473F40DD36DFCA8A4D5E400B7901C8E2473F40987B25E98A4D5E4038D996E2E2473F4005CAE0078B4D5E4089544902E3473F40DE5FC3278B4D5E40B28A7327E3473F40DE3FD93E8B4D5E4019C56445E3473F40E48C2E5D8B4D5E40C1CAB070E3473F40D36A0B798B4D5E400F8D109DE3473F4009B4C2948B4D5E40D77A5FCDE3473F40219FB3AC8B4D5E404DE1E8FAE3473F4064988AC28B4D5E40EEE5C027E4473F4024E8EBD68B4D5E4083589954E4473F40783B41E98B4D5E407F55937FE4473F40AC63A6FE8B4D5E40318B50B5E4473F4018194A118C4D5E40BD9F73E7E4473F40B4DE77258C4D5E4019B96421E5473F400A7DE8368C4D5E40D4DDC056E5473F40FA19F3478C4D5E401E12C78DE5473F407A88585A8C4D5E408A9390CBE5473F402BB6006F8C4D5E4042DB4D13E6473F4066A6357F8C4D5E40754A984DE6473F40259127908C4D5E4012AAF48CE6473F40AF26A0A08C4D5E40CA08D4CCE6473F4071B4F2B18C4D5E4009831312E7473F40766204C48C4D5E403CABF55CE7473F40E0BF62D58C4D5E40D95ECBA7E7473F408A4F04E88C4D5E409B346BFBE7473F40877265FA8C4D5E40CD84FB51E8473F4024D4F50C8D4D5E4070D33BAEE8473F40426664198D4D5E40734DD1EEE8473F408DBC6F268D4D5E40CDD93A35E9473F402CA00E338D4D5E40671B267CE9473F401B23C53F8D4D5E40DB11AEC6E9473F401B13504B8D4D5E403A6D430DEA473F40CA18A8578D4D5E400450515CEA473F406C4136638D4D5E4089AEB1AAEA473F409B3A4D6E8D4D5E400E0795FAEA473F4041A8CA788D4D5E406B7DAE4AEB473F409D070D838D4D5E4052A55E9EEB473F40AFAF878C8D4D5E402171ECF1EB473F4060EDA4958D4D5E40B9DBB549EC473F40A4E39A9D8D4D5E40B2EECD9EEC473F40B2E1B6A48D4D5E40F8808AF4EC473F40A6DA23AB8D4D5E40F48EF74CED473F407F2555B08D4D5E408C94A29FED473F40DE007AB48D4D5E403B6586EDED473F40E6FCF1B78D4D5E409F3B9C5CEE473F40
9	7	0102000020E61000005F000000E6FCF1B78D4D5E409F3B9C5CEE473F40BDD589B78D4D5E4084AC14AAEE473F40BCDBB5B78D4D5E4016F76A02EF473F40CCF742B78D4D5E40317F2636EF473F40AD0A44B78D4D5E406CCA9A3BEF473F4094B44DB78D4D5E40562A108CEF473F404E4973B78D4D5E40F34F2EDEEF473F40AA169CB78D4D5E40429A9638F0473F404C4F9FB78D4D5E40FE1C4B89F0473F40E9F099B78D4D5E40CABD3DE2F0473F4021348FB78D4D5E40F8BF2F2FF1473F40F6187FB78D4D5E40F6CB3C7DF1473F40421B62B78D4D5E40DBBE59E0F1473F40B3A14CB78D4D5E4090F5CF31F2473F40116949B78D4D5E402C36728DF2473F40D28E4BB78D4D5E409C6000E4F2473F40170052B78D4D5E40A5D9EE35F3473F40DEBC5CB78D4D5E4009F30A83F3473F40AC1070B78D4D5E40B9ACCCD8F3473F40C36C92B78D4D5E40CD572E3CF4473F403990B1B78D4D5E403C4F448FF4473F40AFB3D0B78D4D5E40D656F1E8F4473F408835F5B78D4D5E40EA5FC44DF5473F40674E22B88D4D5E40F81261BDF5473F409E9743B88D4D5E404E1D6B0BF6473F40393F6AB88D4D5E40B07C0460F6473F4070888BB88D4D5E40BC6E8AB9F6473F405DC99DB88D4D5E40F4CDE815F7473F406360A6B88D4D5E40A0838D77F7473F40FF01A1B88D4D5E4072986CDBF7473F407DB070B88D4D5E400B8A2742F8473F40369C09B88D4D5E401704CFAFF8473F4029C56BB78D4D5E404DA65F21F9473F40DE0DA4B68D4D5E40C467DD91F9473F40C26594B58D4D5E404227840EFA473F40CC3B60B48D4D5E406E720C89FA473F40CBE31AB38D4D5E4027A04300FB473F400FF7AFB18D4D5E40B362027FFB473F40603856B08D4D5E408991BEFDFB473F40CE6C27AF8D4D5E40DCAE9B83FC473F40BDF228AE8D4D5E40F3B95709FD473F40753535AD8D4D5E40B02C6599FD473F40ECA36FAC8D4D5E40E4A21125FE473F402D7215AC8D4D5E40C6894D71FE473F40B5AB95AB8D4D5E4057C675F9FE473F40BD3646AB8D4D5E40C97B3E8AFF473F40BF301AAB8D4D5E40E740061F00483F40490DFBAA8D4D5E4025A306B800483F40519ED7AA8D4D5E40FE4D8C4A01483F40EF39A6AA8D4D5E40AB21CEDB01483F40C5186AAA8D4D5E40C2CA9D6802483F40B54D24AA8D4D5E40156A38EA02483F409EEBD5A98D4D5E4045AFE56203483F40E65084A98D4D5E400F286FCD03483F40CFEE35A98D4D5E40F0DF942704483F40C6B4CCA88D4D5E40451C2C9104483F40C9A874A88D4D5E40803A5ADF04483F40A71815A88D4D5E40997D0D2E05483F40669BB6A78D4D5E409612857B05483F403D744EA78D4D5E40811A06C905483F40B2EEE0A68D4D5E404BF0E91506483F40266973A68D4D5E405931576406483F40179801A68D4D5E409FA9BFB506483F4046A18DA58D4D5E409444700807483F40FF8C26A58D4D5E400AE6355C07483F405E4EF7A48D4D5E403F8132B007483F40405BCCA48D4D5E40DE10BC0008483F40E48DA3A48D4D5E4073AAF94D08483F40517759A48D4D5E40EEF69EA708483F40B232FEA38D4D5E402331DF0709483F40F997ACA38D4D5E40422B6C5909483F403B6652A38D4D5E4022D0EAB209483F40769DEFA28D4D5E404E2F63190A483F405AA498A28D4D5E4062A94E800A483F40B3CE60A28D4D5E402A40C0CC0A483F4000CB17A28D4D5E40301DE23B0B483F407F79E7A18D4D5E40949B1C8E0B483F40FD27B7A18D4D5E403B8C8DE50B483F409BC385A18D4D5E404C02753E0C483F40D5004FA18D4D5E406360AC960C483F40865B0BA18D4D5E4034AC7EF50C483F401232C0A08D4D5E403B853E530D483F4073ED64A08D4D5E40D965F6BC0D483F40128307A08D4D5E4079E70C200E483F40AC81A19F8D4D5E4038829C840E483F4020FC339F8D4D5E403C3DEBE90E483F40D250C49E8D4D5E40B126304C0F483F402D75609E8D4D5E40D33436A00F483F405150DB9D8D4D5E40D8806E0A10483F40A6DD6E9D8D4D5E401054305B10483F40B6F9FB9C8D4D5E40B1809DAD10483F409806D19C8D4D5E40BCA29BCA10483F40A485299C8D4D5E4029E3791711483F40615F3E9B8D4D5E404DE6577011483F406007F9998D4D5E409080F1CE11483F40	10	1	1	59.97684410647711	59.97684410647711	0105000020E61000000100000001020000005F000000E6FCF1B78D4D5E409F3B9C5CEE473F40BDD589B78D4D5E4084AC14AAEE473F40BCDBB5B78D4D5E4016F76A02EF473F40CCF742B78D4D5E40317F2636EF473F40AD0A44B78D4D5E406CCA9A3BEF473F4094B44DB78D4D5E40562A108CEF473F404E4973B78D4D5E40F34F2EDEEF473F40AA169CB78D4D5E40429A9638F0473F404C4F9FB78D4D5E40FE1C4B89F0473F40E9F099B78D4D5E40CABD3DE2F0473F4021348FB78D4D5E40F8BF2F2FF1473F40F6187FB78D4D5E40F6CB3C7DF1473F40421B62B78D4D5E40DBBE59E0F1473F40B3A14CB78D4D5E4090F5CF31F2473F40116949B78D4D5E402C36728DF2473F40D28E4BB78D4D5E409C6000E4F2473F40170052B78D4D5E40A5D9EE35F3473F40DEBC5CB78D4D5E4009F30A83F3473F40AC1070B78D4D5E40B9ACCCD8F3473F40C36C92B78D4D5E40CD572E3CF4473F403990B1B78D4D5E403C4F448FF4473F40AFB3D0B78D4D5E40D656F1E8F4473F408835F5B78D4D5E40EA5FC44DF5473F40674E22B88D4D5E40F81261BDF5473F409E9743B88D4D5E404E1D6B0BF6473F40393F6AB88D4D5E40B07C0460F6473F4070888BB88D4D5E40BC6E8AB9F6473F405DC99DB88D4D5E40F4CDE815F7473F406360A6B88D4D5E40A0838D77F7473F40FF01A1B88D4D5E4072986CDBF7473F407DB070B88D4D5E400B8A2742F8473F40369C09B88D4D5E401704CFAFF8473F4029C56BB78D4D5E404DA65F21F9473F40DE0DA4B68D4D5E40C467DD91F9473F40C26594B58D4D5E404227840EFA473F40CC3B60B48D4D5E406E720C89FA473F40CBE31AB38D4D5E4027A04300FB473F400FF7AFB18D4D5E40B362027FFB473F40603856B08D4D5E408991BEFDFB473F40CE6C27AF8D4D5E40DCAE9B83FC473F40BDF228AE8D4D5E40F3B95709FD473F40753535AD8D4D5E40B02C6599FD473F40ECA36FAC8D4D5E40E4A21125FE473F402D7215AC8D4D5E40C6894D71FE473F40B5AB95AB8D4D5E4057C675F9FE473F40BD3646AB8D4D5E40C97B3E8AFF473F40BF301AAB8D4D5E40E740061F00483F40490DFBAA8D4D5E4025A306B800483F40519ED7AA8D4D5E40FE4D8C4A01483F40EF39A6AA8D4D5E40AB21CEDB01483F40C5186AAA8D4D5E40C2CA9D6802483F40B54D24AA8D4D5E40156A38EA02483F409EEBD5A98D4D5E4045AFE56203483F40E65084A98D4D5E400F286FCD03483F40CFEE35A98D4D5E40F0DF942704483F40C6B4CCA88D4D5E40451C2C9104483F40C9A874A88D4D5E40803A5ADF04483F40A71815A88D4D5E40997D0D2E05483F40669BB6A78D4D5E409612857B05483F403D744EA78D4D5E40811A06C905483F40B2EEE0A68D4D5E404BF0E91506483F40266973A68D4D5E405931576406483F40179801A68D4D5E409FA9BFB506483F4046A18DA58D4D5E409444700807483F40FF8C26A58D4D5E400AE6355C07483F405E4EF7A48D4D5E403F8132B007483F40405BCCA48D4D5E40DE10BC0008483F40E48DA3A48D4D5E4073AAF94D08483F40517759A48D4D5E40EEF69EA708483F40B232FEA38D4D5E402331DF0709483F40F997ACA38D4D5E40422B6C5909483F403B6652A38D4D5E4022D0EAB209483F40769DEFA28D4D5E404E2F63190A483F405AA498A28D4D5E4062A94E800A483F40B3CE60A28D4D5E402A40C0CC0A483F4000CB17A28D4D5E40301DE23B0B483F407F79E7A18D4D5E40949B1C8E0B483F40FD27B7A18D4D5E403B8C8DE50B483F409BC385A18D4D5E404C02753E0C483F40D5004FA18D4D5E406360AC960C483F40865B0BA18D4D5E4034AC7EF50C483F401232C0A08D4D5E403B853E530D483F4073ED64A08D4D5E40D965F6BC0D483F40128307A08D4D5E4079E70C200E483F40AC81A19F8D4D5E4038829C840E483F4020FC339F8D4D5E403C3DEBE90E483F40D250C49E8D4D5E40B126304C0F483F402D75609E8D4D5E40D33436A00F483F405150DB9D8D4D5E40D8806E0A10483F40A6DD6E9D8D4D5E401054305B10483F40B6F9FB9C8D4D5E40B1809DAD10483F409806D19C8D4D5E40BCA29BCA10483F40A485299C8D4D5E4029E3791711483F40615F3E9B8D4D5E404DE6577011483F406007F9998D4D5E409080F1CE11483F40
11	1	0102000020E610000038000000EF9C51E68B4D5E407AFE98FCED473F408A005BE98B4D5E4084F2CFA7ED473F400189E9EC8B4D5E40F105EB50ED473F409C9BABF08B4D5E40E0653AFFEC473F401E0730F58B4D5E4046BCFEA6EC473F40008CCAFA8B4D5E40B4437D44EC473F40A91EB7FF8B4D5E405857A0F5EB473F4024A817058C4D5E4044BDCAA5EB473F40C16A640B8C4D5E40F185674FEB473F406C5596118C4D5E407C244401EB473F40900648188C4D5E40E14445B3EA473F408D391E1F8C4D5E40D430D369EA473F40D91E62288C4D5E40AE58EE0EEA473F4072005D318C4D5E408D7D60BEE9473F401373343A8C4D5E40EFC30D75E9473F40B88240438C4D5E40662E182FE9473F40FAEF294F8C4D5E40B4E49CD9E8473F40346EC5598C4D5E40663B7A92E8473F40B2BBD8658C4D5E40C2CA4D46E8473F40899751738C4D5E4089F10EF7E7473F40FEAAFB7E8C4D5E40450DC2B6E7473F408195438B8C4D5E40A3CB9276E7473F40349641998C4D5E4094099831E7473F40CBA4E6A88C4D5E401F203AE9E6473F4001ADCBB98C4D5E401E3AD99FE6473F401FB7FFCB8C4D5E402F32B655E6473F40B048F0DF8C4D5E40A2B9BD09E6473F4026EEB3F58C4D5E403F305BBCE5473F405AFCC4068D4D5E4059B74A83E5473F40D1573F1F8D4D5E4081878A36E5473F4073F892308D4D5E4053219103E5473F40A9AF30428D4D5E40E5AE51D2E4473F4055337A548D4D5E402B7FCDA1E4473F404E685F678D4D5E403D264A72E4473F409E7CF17A8D4D5E404DECC543E4473F40A31390968D4D5E4039E08E06E4473F40D85C47B28D4D5E40229615CEE3473F405E965DCD8D4D5E408D47899BE3473F40A49D30E78D4D5E40475BA46FE3473F407D5DDCFF8D4D5E406BAA9B49E3473F40A4BBCD168E4D5E40D74D9C29E3473F40C524452C8E4D5E406211C30EE3473F40348447418E4D5E4001316AF7E2473F409F02F8588E4D5E40C3EDEBE0E2473F40009EB26F8E4D5E403977C3CFE2473F4076FF58888E4D5E40EFE894C2E2473F403A4A609E8E4D5E40A1B2B8BBE2473F401F8964B68E4D5E4033134CB9E2473F40EA2657CF8E4D5E40C52945BBE2473F40B466D8E68E4D5E403C618CBFE2473F406B9624FF8E4D5E40A9E7D4C2E2473F404FCE2A158F4D5E405C116CC4E2473F40C09FC62F8F4D5E405C0BEFC5E2473F4081107A4A8F4D5E40D15A88C7E2473F40BAE23D628F4D5E401DEDD3C8E2473F40A089FC6F8F4D5E4087269B76E2473F40	5	9	1	31.749715821136697	31.749715821136697	0105000020E610000001000000010200000038000000EF9C51E68B4D5E407AFE98FCED473F408A005BE98B4D5E4084F2CFA7ED473F400189E9EC8B4D5E40F105EB50ED473F409C9BABF08B4D5E40E0653AFFEC473F401E0730F58B4D5E4046BCFEA6EC473F40008CCAFA8B4D5E40B4437D44EC473F40A91EB7FF8B4D5E405857A0F5EB473F4024A817058C4D5E4044BDCAA5EB473F40C16A640B8C4D5E40F185674FEB473F406C5596118C4D5E407C244401EB473F40900648188C4D5E40E14445B3EA473F408D391E1F8C4D5E40D430D369EA473F40D91E62288C4D5E40AE58EE0EEA473F4072005D318C4D5E408D7D60BEE9473F401373343A8C4D5E40EFC30D75E9473F40B88240438C4D5E40662E182FE9473F40FAEF294F8C4D5E40B4E49CD9E8473F40346EC5598C4D5E40663B7A92E8473F40B2BBD8658C4D5E40C2CA4D46E8473F40899751738C4D5E4089F10EF7E7473F40FEAAFB7E8C4D5E40450DC2B6E7473F408195438B8C4D5E40A3CB9276E7473F40349641998C4D5E4094099831E7473F40CBA4E6A88C4D5E401F203AE9E6473F4001ADCBB98C4D5E401E3AD99FE6473F401FB7FFCB8C4D5E402F32B655E6473F40B048F0DF8C4D5E40A2B9BD09E6473F4026EEB3F58C4D5E403F305BBCE5473F405AFCC4068D4D5E4059B74A83E5473F40D1573F1F8D4D5E4081878A36E5473F4073F892308D4D5E4053219103E5473F40A9AF30428D4D5E40E5AE51D2E4473F4055337A548D4D5E402B7FCDA1E4473F404E685F678D4D5E403D264A72E4473F409E7CF17A8D4D5E404DECC543E4473F40A31390968D4D5E4039E08E06E4473F40D85C47B28D4D5E40229615CEE3473F405E965DCD8D4D5E408D47899BE3473F40A49D30E78D4D5E40475BA46FE3473F407D5DDCFF8D4D5E406BAA9B49E3473F40A4BBCD168E4D5E40D74D9C29E3473F40C524452C8E4D5E406211C30EE3473F40348447418E4D5E4001316AF7E2473F409F02F8588E4D5E40C3EDEBE0E2473F40009EB26F8E4D5E403977C3CFE2473F4076FF58888E4D5E40EFE894C2E2473F403A4A609E8E4D5E40A1B2B8BBE2473F401F8964B68E4D5E4033134CB9E2473F40EA2657CF8E4D5E40C52945BBE2473F40B466D8E68E4D5E403C618CBFE2473F406B9624FF8E4D5E40A9E7D4C2E2473F404FCE2A158F4D5E405C116CC4E2473F40C09FC62F8F4D5E405C0BEFC5E2473F4081107A4A8F4D5E40D15A88C7E2473F40BAE23D628F4D5E401DEDD3C8E2473F40A089FC6F8F4D5E4087269B76E2473F40
12	9	0102000020E61000002D0000008529BD7B8F4D5E40F0A0F714E9473F4093C2D3638F4D5E4089AB3A13E9473F40960D59478F4D5E40FA88E910E9473F406C36D0288F4D5E40CEB7220EE9473F40E8B270108F4D5E40F6549F0BE9473F403571A0F78E4D5E40513A1A09E9473F406141FFDC8E4D5E4027F90C06E9473F40614795C18E4D5E40FFABA702E9473F40215F05A28E4D5E409F08A6FEE8473F402432C57F8E4D5E40B40B02FAE8473F407FD7075C8E4D5E4041903FF5E8473F403C76E0348E4D5E403A55A6EFE8473F40E1674F0E8E4D5E40B62DEEE9E8473F402A452DF88D4D5E4076BC89E6E8473F40ACF085CE8D4D5E401269FDDFE8473F401B1BE2A18D4D5E40692DD5D8E8473F40B954F6748D4D5E401728CDD1E8473F40EA26F95D8D4D5E4015F554CEE8473F4091492F328D4D5E40F31E27C8E8473F40F3CD4A1B8D4D5E4014E628C5E8473F404D18FD038D4D5E40142A46C2E8473F40FE415CED8C4D5E40D9FEA2BFE8473F40359E3DD48C4D5E407A81EFBCE8473F40350245BA8C4D5E404B206FBAE8473F4090D1508F8C4D5E40CCD40CB7E8473F40A8FC15798C4D5E40ACF3B6B5E8473F4034A26D628C4D5E40C85CA5B4E8473F40A970FC378C4D5E4062426CB3E8473F40EC0719218C4D5E404B4A38B3E8473F40E4412CFB8B4D5E40B34572B3E8473F40EA9DF8D48B4D5E406F8324B4E8473F40DC542BB38B4D5E402E2C14B5E8473F40584CDC928B4D5E4069F945B6E8473F40A197A0728B4D5E409B9FB5B7E8473F40427EF4558B4D5E40181611B9E8473F406A5AAB3B8B4D5E40603D1FBAE8473F40AD9356238B4D5E40CB77CBBAE8473F40527550088B4D5E40F36C3CBBE8473F4057E127F28A4D5E40C6F369BBE8473F40E1A94AD28A4D5E409AA662BBE8473F40DB0BAEB48A4D5E403A6EFCBAE8473F40AF5AD19A8A4D5E40F48560BAE8473F403C9C8B828A4D5E4084189AB9E8473F4097C6BD668A4D5E40BA0976B8E8473F407643F24C8A4D5E40D20727B7E8473F40	11	6	1	30.134564635892175	30.134564635892175	0105000020E61000000100000001020000002D0000008529BD7B8F4D5E40F0A0F714E9473F4093C2D3638F4D5E4089AB3A13E9473F40960D59478F4D5E40FA88E910E9473F406C36D0288F4D5E40CEB7220EE9473F40E8B270108F4D5E40F6549F0BE9473F403571A0F78E4D5E40513A1A09E9473F406141FFDC8E4D5E4027F90C06E9473F40614795C18E4D5E40FFABA702E9473F40215F05A28E4D5E409F08A6FEE8473F402432C57F8E4D5E40B40B02FAE8473F407FD7075C8E4D5E4041903FF5E8473F403C76E0348E4D5E403A55A6EFE8473F40E1674F0E8E4D5E40B62DEEE9E8473F402A452DF88D4D5E4076BC89E6E8473F40ACF085CE8D4D5E401269FDDFE8473F401B1BE2A18D4D5E40692DD5D8E8473F40B954F6748D4D5E401728CDD1E8473F40EA26F95D8D4D5E4015F554CEE8473F4091492F328D4D5E40F31E27C8E8473F40F3CD4A1B8D4D5E4014E628C5E8473F404D18FD038D4D5E40142A46C2E8473F40FE415CED8C4D5E40D9FEA2BFE8473F40359E3DD48C4D5E407A81EFBCE8473F40350245BA8C4D5E404B206FBAE8473F4090D1508F8C4D5E40CCD40CB7E8473F40A8FC15798C4D5E40ACF3B6B5E8473F4034A26D628C4D5E40C85CA5B4E8473F40A970FC378C4D5E4062426CB3E8473F40EC0719218C4D5E404B4A38B3E8473F40E4412CFB8B4D5E40B34572B3E8473F40EA9DF8D48B4D5E406F8324B4E8473F40DC542BB38B4D5E402E2C14B5E8473F40584CDC928B4D5E4069F945B6E8473F40A197A0728B4D5E409B9FB5B7E8473F40427EF4558B4D5E40181611B9E8473F406A5AAB3B8B4D5E40603D1FBAE8473F40AD9356238B4D5E40CB77CBBAE8473F40527550088B4D5E40F36C3CBBE8473F4057E127F28A4D5E40C6F369BBE8473F40E1A94AD28A4D5E409AA662BBE8473F40DB0BAEB48A4D5E403A6EFCBAE8473F40AF5AD19A8A4D5E40F48560BAE8473F403C9C8B828A4D5E4084189AB9E8473F4097C6BD668A4D5E40BA0976B8E8473F407643F24C8A4D5E40D20727B7E8473F40
13	7	0102000020E61000001D0000008529BD7B8F4D5E40F0A0F714E9473F4081921E608F4D5E40BB1AB719E9473F40544CDD498F4D5E40CAAD881BE9473F40B2D552318F4D5E40CD6A3D20E9473F40FEF71F1A8F4D5E40DFA71C26E9473F404913EF008F4D5E400D1D742EE9473F40284C06EA8E4D5E4018B1D338E9473F4090B871D28E4D5E4050F17D46E9473F40AADD0ABC8E4D5E40BD747B57E9473F403EBDCBA58E4D5E400B57C56CE9473F400A2ACB8C8E4D5E4049E70F8BE9473F40FADCE0768E4D5E401B7C7EACE9473F40B6F65F638E4D5E400A942FD1E9473F4042B60B518E4D5E40A95F12FCE9473F40D65ED93F8E4D5E403C1EE52EEA473F406182222F8E4D5E40F46F9A6CEA473F40C8CAF01E8E4D5E408ED156B6EA473F40778C95128E4D5E40EB443EFAEA473F40C6E3DC058E4D5E4012596349EB473F40435706FB8D4D5E4096D59293EB473F4021E445F18D4D5E40FB1EF5D7EB473F40F1EA2EE68D4D5E407022A724EC473F402AED51DB8D4D5E40B7489871EC473F40945046D18D4D5E40C4B4FEBDEC473F40F77AA3C88D4D5E404B32000AED473F406AD4E3C18D4D5E40BBE4C85AED473F40D5FAB8BC8D4D5E40B2230AB3ED473F40889366B98D4D5E405439550DEE473F40E6FCF1B78D4D5E409F3B9C5CEE473F40	11	10	1	15.423168874990141	15.423168874990141	0105000020E61000000100000001020000001D0000008529BD7B8F4D5E40F0A0F714E9473F4081921E608F4D5E40BB1AB719E9473F40544CDD498F4D5E40CAAD881BE9473F40B2D552318F4D5E40CD6A3D20E9473F40FEF71F1A8F4D5E40DFA71C26E9473F404913EF008F4D5E400D1D742EE9473F40284C06EA8E4D5E4018B1D338E9473F4090B871D28E4D5E4050F17D46E9473F40AADD0ABC8E4D5E40BD747B57E9473F403EBDCBA58E4D5E400B57C56CE9473F400A2ACB8C8E4D5E4049E70F8BE9473F40FADCE0768E4D5E401B7C7EACE9473F40B6F65F638E4D5E400A942FD1E9473F4042B60B518E4D5E40A95F12FCE9473F40D65ED93F8E4D5E403C1EE52EEA473F406182222F8E4D5E40F46F9A6CEA473F40C8CAF01E8E4D5E408ED156B6EA473F40778C95128E4D5E40EB443EFAEA473F40C6E3DC058E4D5E4012596349EB473F40435706FB8D4D5E4096D59293EB473F4021E445F18D4D5E40FB1EF5D7EB473F40F1EA2EE68D4D5E407022A724EC473F402AED51DB8D4D5E40B7489871EC473F40945046D18D4D5E40C4B4FEBDEC473F40F77AA3C88D4D5E404B32000AED473F406AD4E3C18D4D5E40BBE4C85AED473F40D5FAB8BC8D4D5E40B2230AB3ED473F40889366B98D4D5E405439550DEE473F40E6FCF1B78D4D5E409F3B9C5CEE473F40
16	1	0102000020E6100000650000007AD35ABC8B4D5E4063723DE610483F40B1E588C18B4D5E4062F3158F10483F402E5667C48B4D5E40F6D8854310483F40B3F9B0C58B4D5E40C5D5C1F40F483F400399C8C58B4D5E40AD41C7A60F483F40068D70C58B4D5E40F4102C570F483F40061F8ACF8B4D5E40FA657DF90E483F407A991CCF8B4D5E40F2DE34A00E483F40715FB3CE8B4D5E40EDCA06500E483F40628E41CE8B4D5E406BD4B1F50D483F40EAC7C1CD8B4D5E400F7A07970D483F40DBF64FCD8B4D5E40705CDF330D483F40090608CD8B4D5E409BB52BE30C483F40C0F7CCCC8B4D5E4006C4F9920C483F40192295CC8B4D5E40215D793B0C483F409E676DCC8B4D5E401D3BF1E30B483F40F59761CC8B4D5E402D9FB2880B483F4001C672CC8B4D5E4010E1652B0B483F408317A3CC8B4D5E40F6B1A2C80A483F403BB2F4CC8B4D5E40B9D8DA620A483F401F6856CD8B4D5E40D13CE0000A483F403B61ADCD8B4D5E40D9828EB009483F408FA325CE8B4D5E405256ED4909483F40F5A48BCE8B4D5E40946350FC08483F400476FDCE8B4D5E40E6B8BAAF08483F400ABCBECF8B4D5E403287AA3D08483F40169988D08B4D5E40740F37CE07483F40B4E30FD18B4D5E40662C3B8107483F40CDE8BED18B4D5E4001292A0D07483F40B19E20D28B4D5E409B92FBC006483F40D32E80D28B4D5E40D157F96E06483F400F15D6D28B4D5E40533E2A1C06483F40C1181FD38B4D5E40DF2024CB05483F40EB395BD38B4D5E40627EEA7805483F408C788AD38B4D5E40A57DF32305483F4084E7ADD38B4D5E408A05C6CA04483F40334EC2D38B4D5E400823487604483F4096ACC7D38B4D5E40FBC6561E04483F409015BFD38B4D5E40C3C569C303483F40C3C1ABD38B4D5E409825066C03483F40F56D98D38B4D5E4059A5620E03483F403ADF9ED38B4D5E403944B5B002483F40B599C6D38B4D5E40C4B8E14F02483F403D82FFD38B4D5E40A76BADF401483F4092BE4BD48B4D5E40C3E32B9501483F403103A7D48B4D5E4069193B3601483F40DC7513D58B4D5E406B673DD800483F40166295D58B4D5E40B332407700483F4075D21ED68B4D5E4012BF831B00483F40A82DC4D68B4D5E4002650BB4FF473F4071935BD78B4D5E40451DC555FF473F40BD44F7D78B4D5E40097DF2F1FE473F40E4718BD88B4D5E40D598468AFE473F40F8D905D98B4D5E40FC2B2F2AFE473F4007AB77D98B4D5E409C9D3AC6FD473F400A4ED8D98B4D5E40CF71DB66FD473F40075A30DA8B4D5E4061766302FD473F405C967CDA8B4D5E409474CD9BFC473F4067CAB9DA8B4D5E40C6539637FC473F400709E9DA8B4D5E40AFB48CD4FB473F4000780CDB8B4D5E40C417C070FB473F40700423DB8B4D5E40D26C550CFB473F4037C12DDB8B4D5E4002BB34A3FA473F40BA0C32DB8B4D5E4049593C3DFA473F40F8E62FDB8B4D5E40986685E5F9473F40B47529DB8B4D5E4087D97B94F9473F40C73417DB8B4D5E40BDBF9330F9473F40D55CFCDA8B4D5E405D5170CFF8473F40404CDEDA8B4D5E40D7B26D7BF8473F40036CB4DA8B4D5E4002A11E27F8473F401EBC7EDA8B4D5E40E9FFF9D6F7473F40380C49DA8B4D5E40C2A4857EF7473F4098CD19DA8B4D5E4016A1CB29F7473F405556E7D98B4D5E40D17548C6F6473F40A158CAD98B4D5E405024B476F6473F40BAAEC0D98B4D5E40A4C28820F6473F40E5C9D0D98B4D5E40D0446EC3F5473F405BEDEFD98B4D5E40F88C8C62F5473F4078E01ADA8B4D5E400D714DF5F4473F40BB574DDA8B4D5E401B726985F4473F40D94A78DA8B4D5E40F5963938F4473F401CC2AADA8B4D5E40A2D11EE9F3473F40E81BEADA8B4D5E4074ECCB92F3473F4056AE2CDB8B4D5E40CA210740F3473F402E367DDB8B4D5E40FB287EE7F2473F40A8F6D0DB8B4D5E401F5AD891F2473F404DD234DC8B4D5E401341D431F2473F400C048FDC8B4D5E40887534DCF1473F4047EAE4DC8B4D5E4009F87689F1473F40EDC548DD8B4D5E405D484F26F1473F4067869CDD8B4D5E40785994D4F0473F405EFBEBDD8B4D5E40996C7D86F0473F40B23738DE8B4D5E404E39C03AF0473F408ABF88DE8B4D5E404C77ABEBEF473F405EAAA4DE8B4D5E400D909CCFEF473F4047F70EDF8B4D5E409465CA7FEF473F406E24A3DF8B4D5E40C805AA31EF473F40DE5F72E08B4D5E4032AA0CE3EE473F40B2A2D3E18B4D5E40F6D6A895EE473F406210E6E38B4D5E40B13AF449EE473F40EF9C51E68B4D5E407AFE98FCED473F40	4	5	1	59.134364559575125	59.134364559575125	0105000020E6100000010000000102000000650000007AD35ABC8B4D5E4063723DE610483F40B1E588C18B4D5E4062F3158F10483F402E5667C48B4D5E40F6D8854310483F40B3F9B0C58B4D5E40C5D5C1F40F483F400399C8C58B4D5E40AD41C7A60F483F40068D70C58B4D5E40F4102C570F483F40061F8ACF8B4D5E40FA657DF90E483F407A991CCF8B4D5E40F2DE34A00E483F40715FB3CE8B4D5E40EDCA06500E483F40628E41CE8B4D5E406BD4B1F50D483F40EAC7C1CD8B4D5E400F7A07970D483F40DBF64FCD8B4D5E40705CDF330D483F40090608CD8B4D5E409BB52BE30C483F40C0F7CCCC8B4D5E4006C4F9920C483F40192295CC8B4D5E40215D793B0C483F409E676DCC8B4D5E401D3BF1E30B483F40F59761CC8B4D5E402D9FB2880B483F4001C672CC8B4D5E4010E1652B0B483F408317A3CC8B4D5E40F6B1A2C80A483F403BB2F4CC8B4D5E40B9D8DA620A483F401F6856CD8B4D5E40D13CE0000A483F403B61ADCD8B4D5E40D9828EB009483F408FA325CE8B4D5E405256ED4909483F40F5A48BCE8B4D5E40946350FC08483F400476FDCE8B4D5E40E6B8BAAF08483F400ABCBECF8B4D5E403287AA3D08483F40169988D08B4D5E40740F37CE07483F40B4E30FD18B4D5E40662C3B8107483F40CDE8BED18B4D5E4001292A0D07483F40B19E20D28B4D5E409B92FBC006483F40D32E80D28B4D5E40D157F96E06483F400F15D6D28B4D5E40533E2A1C06483F40C1181FD38B4D5E40DF2024CB05483F40EB395BD38B4D5E40627EEA7805483F408C788AD38B4D5E40A57DF32305483F4084E7ADD38B4D5E408A05C6CA04483F40334EC2D38B4D5E400823487604483F4096ACC7D38B4D5E40FBC6561E04483F409015BFD38B4D5E40C3C569C303483F40C3C1ABD38B4D5E409825066C03483F40F56D98D38B4D5E4059A5620E03483F403ADF9ED38B4D5E403944B5B002483F40B599C6D38B4D5E40C4B8E14F02483F403D82FFD38B4D5E40A76BADF401483F4092BE4BD48B4D5E40C3E32B9501483F403103A7D48B4D5E4069193B3601483F40DC7513D58B4D5E406B673DD800483F40166295D58B4D5E40B332407700483F4075D21ED68B4D5E4012BF831B00483F40A82DC4D68B4D5E4002650BB4FF473F4071935BD78B4D5E40451DC555FF473F40BD44F7D78B4D5E40097DF2F1FE473F40E4718BD88B4D5E40D598468AFE473F40F8D905D98B4D5E40FC2B2F2AFE473F4007AB77D98B4D5E409C9D3AC6FD473F400A4ED8D98B4D5E40CF71DB66FD473F40075A30DA8B4D5E4061766302FD473F405C967CDA8B4D5E409474CD9BFC473F4067CAB9DA8B4D5E40C6539637FC473F400709E9DA8B4D5E40AFB48CD4FB473F4000780CDB8B4D5E40C417C070FB473F40700423DB8B4D5E40D26C550CFB473F4037C12DDB8B4D5E4002BB34A3FA473F40BA0C32DB8B4D5E4049593C3DFA473F40F8E62FDB8B4D5E40986685E5F9473F40B47529DB8B4D5E4087D97B94F9473F40C73417DB8B4D5E40BDBF9330F9473F40D55CFCDA8B4D5E405D5170CFF8473F40404CDEDA8B4D5E40D7B26D7BF8473F40036CB4DA8B4D5E4002A11E27F8473F401EBC7EDA8B4D5E40E9FFF9D6F7473F40380C49DA8B4D5E40C2A4857EF7473F4098CD19DA8B4D5E4016A1CB29F7473F405556E7D98B4D5E40D17548C6F6473F40A158CAD98B4D5E405024B476F6473F40BAAEC0D98B4D5E40A4C28820F6473F40E5C9D0D98B4D5E40D0446EC3F5473F405BEDEFD98B4D5E40F88C8C62F5473F4078E01ADA8B4D5E400D714DF5F4473F40BB574DDA8B4D5E401B726985F4473F40D94A78DA8B4D5E40F5963938F4473F401CC2AADA8B4D5E40A2D11EE9F3473F40E81BEADA8B4D5E4074ECCB92F3473F4056AE2CDB8B4D5E40CA210740F3473F402E367DDB8B4D5E40FB287EE7F2473F40A8F6D0DB8B4D5E401F5AD891F2473F404DD234DC8B4D5E401341D431F2473F400C048FDC8B4D5E40887534DCF1473F4047EAE4DC8B4D5E4009F87689F1473F40EDC548DD8B4D5E405D484F26F1473F4067869CDD8B4D5E40785994D4F0473F405EFBEBDD8B4D5E40996C7D86F0473F40B23738DE8B4D5E404E39C03AF0473F408ABF88DE8B4D5E404C77ABEBEF473F405EAAA4DE8B4D5E400D909CCFEF473F4047F70EDF8B4D5E409465CA7FEF473F406E24A3DF8B4D5E40C805AA31EF473F40DE5F72E08B4D5E4032AA0CE3EE473F40B2A2D3E18B4D5E40F6D6A895EE473F406210E6E38B4D5E40B13AF449EE473F40EF9C51E68B4D5E407AFE98FCED473F40
2	10	0102000020E61000005D010000EEC984DE894D5E40AF034F701D483F40A2891AC6894D5E4067DC406F1D483F406B60E3AE894D5E40C5C28D6E1D483F40F20A9596894D5E40EED0226E1D483F400DBF667C894D5E40E901F76D1D483F40066B135C894D5E40432CBF6D1D483F4006A89C3B894D5E4098A07F6C1D483F4007711325894D5E4034B2116B1D483F40F12EAF0D894D5E40ECDB4A691D483F407AFDFDED884D5E405AB2AA661D483F4065307ECE884D5E402C6A4E631D483F40CFDADAB3884D5E40492A01601D483F406D38229B884D5E407EAAC45C1D483F40E424B884884D5E409C45FB591D483F4005C1D969884D5E404C3B48561D483F40A719C752884D5E402ABD0E531D483F40AD651E38884D5E40D1EF874F1D483F40E00A771A884D5E4037DDC54B1D483F402D0DC4FE874D5E40B439CD481D483F40D747D5E6874D5E40648D8B461D483F40F24CEECB874D5E4001B841441D483F40CE1998AF874D5E406C360B421D483F4084317398874D5E4088542F401D483F40889D4A82874D5E4016F93D3E1D483F4042967768874D5E4091619D3B1D483F404E79D74D874D5E404E607F381D483F4010542833874D5E4002C109351D483F4038C54315874D5E40DCF0B6301D483F400E3F02F6864D5E408461D02B1D483F40B5A20AD5864D5E40D78F71261D483F404E2F75B3864D5E40C426C7201D483F40C9CEE091864D5E40144E191B1D483F4015526A6E864D5E40D6FB55151D483F407ACD7849864D5E400009BB0F1D483F4083CCA523864D5E4092AD6B0A1D483F40CD9978FD854D5E4039BA96051D483F40D68B7BD5854D5E4046FA1E011D483F40344F2FAA854D5E40A3ADF3FC1C483F40611CAD81854D5E406E06B2F91C483F4000D2396A854D5E40054918F81C483F4027FB843F854D5E409D40B4F51C483F4018A86E29854D5E40226DB9F41C483F4057E9BE01854D5E40661C60F31C483F40837F49DE844D5E404D49A8F21C483F40E6B568BA844D5E405B0773F21C483F40CFAAF79D844D5E402198B2F21C483F4027903A83844D5E40F7C140F31C483F405C059E6C844D5E401095F8F31C483F4088201851844D5E40AEF226F51C483F4025261A37844D5E4012E194F61C483F408B476A20844D5E40F383FEF71C483F40876CAE07844D5E40B79DA8F71C483F40538866EF834D5E4010B5C9F61C483F407B2000D8834D5E403FABAEF51C483F40B21E70BD834D5E40AAD830F41C483F4060CF379F834D5E40904319F21C483F40A9AC1589834D5E4081B047F01C483F4046BF4171834D5E4011E50FEE1C483F4069AD7C57834D5E405A695FEB1C483F4022B9FF3F834D5E408432A7E81C483F40C16E8C28834D5E40C94BB9E51C483F40122E8E11834D5E404E07A6E21C483F40ABC722F2824D5E40491E26DE1C483F40402F1ED6824D5E40CCEBE7D91C483F408D3E95BC824D5E40E8FCE1D51C483F4019867BA4824D5E40D2FD00D21C483F40DC74F48D824D5E4049B058CE1C483F402AD5B273824D5E40366DFCC91C483F4005C2DC5B824D5E404DE7EDC51C483F4086C85B41824D5E40C07F4FC11C483F40AD15DA2A824D5E408B2955BD1C483F4098FEA629824D5E40FD771CBD1C483F4072A0B512824D5E40DC25AEB91C483F407204BDF8814D5E40745BEAB51C483F402760B5DE814D5E4030347EB11C483F408E74DBC5814D5E40290C8CAC1C483F4002464BA9814D5E407EB73FA61C483F404A60488E814D5E40C4C9A49F1C483F407E773A76814D5E40E11EBC981C483F40A6BE8C5F814D5E40A1F7F1901C483F4026D86147814D5E40152B35871C483F407CD0FA2E814D5E408F4C457B1C483F40CA8E2A16814D5E4024662B6C1C483F40626293FF804D5E4062D25B5C1C483F408456FAE5804D5E40FAC595481C483F408124CBCD804D5E4029E311341C483F40499624B3804D5E407EAFDF1B1C483F406C40429C804D5E40BB90A3051C483F40BBA7FE83804D5E40CDCC6BEC1B483F404D287C6A804D5E404DCCECCF1B483F4035306150804D5E40B5EB8BB01B483F4093ACAC35804D5E40CF07C78D1B483F40DFB4251A804D5E400B6816671B483F4034C01404804D5E406EBE06461B483F401F7884EC7F4D5E40D1FAA2201B483F406B490AD67F4D5E401E07E8FA1A483F4005C6FFBF7F4D5E400C58D9D31A483F40F7BD04A97F4D5E40D248BEA81A483F40C832D4937F4D5E405DB1BE7E1A483F40286CD47D7F4D5E405E5581501A483F40BFF421687F4D5E407FD3EC1F1A483F401D2D50507F4D5E401CA6E0E619483F4095C2723A7F4D5E40DCB1C0AE19483F402124AD267F4D5E40F9E83A7819483F400C47B8127F4D5E40F812363D19483F4075CD77FF7E4D5E4052869AFF18483F406D76D9EC7E4D5E40A467A1BE18483F40ACE886DB7E4D5E406ED0067C18483F4036BB88CB7E4D5E40E0DCFD3718483F4027ED61BD7E4D5E40EDDBE5F417483F40C2E81AAF7E4D5E40FA5B5BA817483F406A19E3A27E4D5E40D5F1435D17483F407BA356987E4D5E400CF1661217483F40BB43808F7E4D5E40F6B532C916483F405DF793877E4D5E4076D5B67B16483F4033AFD9807E4D5E406836D82B16483F401D847E7B7E4D5E404367A2DB15483F405A4A54777E4D5E4052D9BD8715483F408B408A747E4D5E40F486F03115483F40010638737E4D5E4022D4E4E014483F40DD7B04737E4D5E406F20FE8C14483F407F63C0737E4D5E40212A0F3E14483F40B80427757E4D5E400349A6EB13483F40D0BEBA767E4D5E40BE929D9B13483F404ECBFB777E4D5E4089A6594813483F404BD753787E4D5E402935EBF412483F40BC5D3E787E4D5E40F9ECD9A312483F406384BD777E4D5E40F41CC95012483F4088590C777E4D5E40F322910112483F40516132767E4D5E4002D9C9AA11483F40783055757E4D5E40745D9F5211483F400E8C8E747E4D5E4028C6F30611483F4087F49C737E4D5E40070805AD10483F40883FB8727E4D5E404A14385A10483F405147DE717E4D5E403E6D7C0110483F40059642717E4D5E40A3FB4AC30F483F40FE55AD707E4D5E40E3247B6A0F483F4033F641707E4D5E409D4177150F483F404218FB6F7E4D5E4022D2B5C60E483F40BB2FC26F7E4D5E402DE8D8610E483F40AF01B16F7E4D5E4088CB57ED0D483F4019F7BE6F7E4D5E4008BDB0950D483F40B39EE56F7E4D5E40AC6883350D483F409FE523707E4D5E4070FC58DA0C483F40A71F8D707E4D5E405799ED6E0C483F40267D15717E4D5E4042C190060C483F4082F9F6717E4D5E404B136B7F0B483F40FE62D7727E4D5E402CAE5B100B483F40D98D88737E4D5E40E8CC59C20A483F401F57D4747E4D5E40CC8E833C0A483F407B826E767E4D5E40441EF7A409483F40793753777E4D5E40096A345509483F40206BFC787E4D5E40A986AFC608483F40F20AFD797E4D5E40CDC4337308483F40F656EA7A7E4D5E40D0550A2708483F4069E4D27C7E4D5E40358F7A8A07483F4008879F7E7E4D5E4076C276F006483F40CEF88E7F7E4D5E40A3DCF29B06483F40F3DA07817E4D5E40F120610B06483F40C28660827E4D5E4003B0EF7605483F400A4A80837E4D5E40E7E57CE504483F40D74C4C847E4D5E40D720826004483F40D35ED0847E4D5E408C0DC8E303483F4008AE1D857E4D5E408F0FE96503483F40B61432857E4D5E405C0A3CF602483F408BF921857E4D5E406E893C8702483F40938AFE847E4D5E402CB0D22402483F405013CC847E4D5E4084FACFBF01483F40967EA6847E4D5E40FE85966401483F40AFD49C847E4D5E403CE7E90B01483F40FF73B4847E4D5E402B408CB600483F40103FFA847E4D5E4069A7754200483F40386662857E4D5E40E2C46ED7FF473F407252E4857E4D5E40759EFD74FF473F4073FB70867E4D5E40572B7B18FF473F405020F6867E4D5E40177514C5FE473F4026AE72877E4D5E40B3BFE677FE473F40EF130A887E4D5E40B4FA4119FE473F402F9794887E4D5E4057C845C6FD473F402AA918897E4D5E40BDC3CA78FD473F40E7E09E897E4D5E4058918A2CFD473F4033923A8A7E4D5E408A059FDEFC473F4032471F8B7E4D5E40854B807EFC473F4044BBF18B7E4D5E4007EE932EFC473F404E9EE78C7E4D5E405ABE0EDAFB473F40E069168E7E4D5E40CA161982FB473F4076D2798F7E4D5E4089F5162BFB473F40DF2B25917E4D5E40984631CFFA473F4070A60C937E4D5E405773446BFA473F40C1A395947E4D5E40FF93331BFA473F4091F87A967E4D5E40D85BDBB7F9473F40E0FB2F987E4D5E40E3D61C58F9473F4045B27A997E4D5E4077D3AE04F9473F40E4A58E9A7E4D5E406A6748B8F8473F404B56AD9B7E4D5E406F997D65F8473F405CD0AB9C7E4D5E405C389D17F8473F40320DE19D7E4D5E409AE705B2F7473F403CF0D69E7E4D5E40F0B7225AF7473F401427E09F7E4D5E40B44E43F1F6473F4053B096A07E4D5E40E069EFA1F6473F406CB545A17E4D5E407FDF5C4FF6473F409E10EBA17E4D5E4045C67BFAF5473F402F338DA27E4D5E40D0165C9FF5473F40FE2F2DA37E4D5E40B55D5C3BF5473F40A7A8C5A37E4D5E406FF236D2F4473F4007194FA47E4D5E40C7D71568F4473F40479CD9A47E4D5E40AD447DEBF3473F40F20E46A57E4D5E402A28F375F3473F40B040A0A57E4D5E40CE2426FBF2473F40A11EE7A57E4D5E40C47AF378F2473F40813714A67E4D5E4005ACD4F1F1473F40AC5224A67E4D5E40E3556875F1473F40C5A81AA67E4D5E404C8237EDF0473F40CD39F7A57E4D5E40C1BF786EF0473F40E789C1A57E4D5E4048164DFBEF473F40547377A57E4D5E4073D3A090EF473F409BD825A57E4D5E409D36E535EF473F403B6EC8A47E4D5E40AC5D36E1EE473F4076A565A47E4D5E40A0F1CF94EE473F40308BD2A37E4D5E408EE35133EE473F40A06830A37E4D5E40E7DE09D6ED473F40F0588FA27E4D5E40A5AB9083ED473F40302D61A27E4D5E40246EA16DED473F4010468EA27E4D5E4024022117ED473F40754D20A37E4D5E40230B34BAEC473F403CB9E3A37E4D5E40FB3B5C6DEC473F405DFE27A57E4D5E40CB540710EC473F4054D1E8A67E4D5E402CD96DABEB473F4064FA9FA87E4D5E406787EB5DEB473F4047C357AB7E4D5E407D61B4FAEA473F40A9E36BAE7E4D5E4070B47BA3EA473F40071604B27E4D5E409DE84B53EA473F40766B27B77E4D5E409ED90CF9E9473F4048257CBC7E4D5E40791EAEAEE9473F40B5E468C37E4D5E403DA20E5FE9473F40AFD24FCB7E4D5E4048345911E9473F40859474D47E4D5E40CAA5F6C4E8473F40C10CE4DE7E4D5E407AFB017CE8473F40CCD938EB7E4D5E40D8C94730E8473F40D4B811F87E4D5E4057104AE9E7473F40A6489D047F4D5E404B56CEA9E7473F400C47B8127F4D5E4083517667E7473F40128A2E217F4D5E402D442127E7473F4039705A327F4D5E4020949DDDE6473F4033E330427F4D5E40CE042D9EE6473F40208C93537F4D5E406CC9C65DE6473F40CCB16B647F4D5E40AAE06623E6473F40AAE7CD767F4D5E40A58C7DE7E5473F40949C88887F4D5E40184B5FB0E5473F4088D6C7997F4D5E403B39B37CE5473F40AD2091AC7F4D5E4076188E45E5473F40BCC4EEC17F4D5E4046A36809E5473F40627A3CD97F4D5E40FA91DCCAE4473F40168F62EB7F4D5E40F7923E9CE4473F402BCAC800804D5E40EC7D8367E4473F4076F60A15804D5E40E62CCC37E4473F409BFCB62A804D5E4002781D07E4473F402BF24440804D5E40EE621ED9E3473F40C378AF55804D5E40E7FFEFADE3473F40501C246A804D5E40E346CF86E3473F408143B77D804D5E40673F4563E3473F402E9D3797804D5E401EFBC937E3473F405A9F5BB0804D5E40318B2410E3473F404DB5FDC8804D5E40D2FF2FECE2473F40EED342E0804D5E4071FED5CCE2473F403276A6F6804D5E40F14821B1E2473F40E05E5F0C814D5E4017897598E2473F40FE82E722814D5E40E02D4781E2473F40539F493A814D5E40D94CBD6BE2473F406A976454814D5E40ED6AB656E2473F409634196A814D5E406E93B747E2473F40AD705181814D5E40D401833AE2473F40D47DFB98814D5E4020405530E2473F40206D1EB2814D5E40AB8B9D29E2473F40FFD0FCCC814D5E406D086427E2473F4078EF57EA814D5E40D08C662AE2473F406FBC8207824D5E404B87DF31E2473F40A17C2D27824D5E40F6BD5C3CE2473F40514F4548824D5E4050DE2C46E2473F407F8F5A5E824D5E40400EEB4AE2473F40835DEC74824D5E40DABCBE4EE2473F40B63A368B824D5E40850A5E51E2473F4076A4EBA3824D5E4095EE7652E2473F40554CE7BB824D5E4079271752E2473F40BF99D0D8824D5E403E5AE550E2473F40B0C2C8F3824D5E4010BBC44FE2473F404C7BCC05834D5E4044E4C34EE2473F40EEA00F1F834D5E409D4C2C4DE2473F4086F08639834D5E4031EC314BE2473F405EB65E52834D5E40FF960949E2473F40373F1770834D5E40381E1C46E2473F4074FF5687834D5E40BC88C143E2473F40D3AD67A0834D5E400ADC3C41E2473F40BD96CABA834D5E40D647A23EE2473F40D78FB7D6834D5E405E0ADE3BE2473F406BBABFF6834D5E409921AA38E2473F4059B81C17844D5E4063B28B35E2473F40B35B123A844D5E4032768732E2473F40FED5505B844D5E409F840A30E2473F404EA37A7F844D5E406FF7BE2DE2473F40260896A4844D5E40B7C6D82BE2473F40AA7B80C8844D5E407F89602AE2473F409B9403EF844D5E402132FB28E2473F40ED682B15854D5E405254AB27E2473F405E7B993A854D5E4031AB7826E2473F40DD135E61854D5E40C4315A25E2473F40D6B79187854D5E4082054324E2473F40A1F4C7AC854D5E4086AE2023E2473F40100D62D2854D5E4038F00922E2473F40DE3D40F7854D5E4097022221E2473F409613CE1D864D5E40C2A07020E2473F40CD39BA42864D5E4072015920E2473F40928EA068864D5E40B84DE320E2473F40D397828E864D5E40C00A3A22E2473F40100430B4864D5E404FF56724E2473F406BB44FD9864D5E4081F94827E2473F40232C6CFE864D5E408AEF942AE2473F4038648721874D5E403DAFC02DE2473F40EC756241874D5E4054C7C530E2473F40E6D8C35C874D5E401E6C7E33E2473F40CE25C478874D5E40A76E5C36E2473F406FC9F897874D5E40EE3EA639E2473F40ADE17DB0874D5E401CD8493CE2473F40524CB0C8874D5E40E6E0F03EE2473F4066EBB3DF874D5E40604A8041E2473F407F7207F6874D5E402B1B0444E2473F40CEA6190E884D5E40AF4EB646E2473F4080F51329884D5E407ACEF249E2473F400B884147884D5E40C5419D4DE2473F4000FD2663884D5E40E6930B51E2473F4071B46E79884D5E4044ADD053E2473F40C32D7090884D5E4038D7B356E2473F40FA63D1A9884D5E40F728DF59E2473F4074C01DC4884D5E40E1C7115DE2473F40ADAC35E0884D5E402F2F6460E2473F404FECCCFD884D5E407537D163E2473F408C82431C894D5E4025674367E2473F40C7383A3F894D5E40DED01E6BE2473F40E3A3C060894D5E407414B56EE2473F40BB3FCF80894D5E4000560572E2473F40D783D79B894D5E4021F9BA74E2473F40CDEB92B5894D5E40BA814077E2473F40A3AA6CCC894D5E40B3937379E2473F40DF7D02E6894D5E409FE7DB7BE2473F400DCB41FE894D5E4046921A7EE2473F404B00660D8A4D5E40097A7C7FE2473F4014A484268A4D5E4067D1E180E2473F407B34B93E8A4D5E40D4AEEE81E2473F4039F5E1568A4D5E4090ECA082E2473F40	12	8	1	218.4697349919618	218.4697349919618	0105000020E61000000100000001020000005D010000EEC984DE894D5E40AF034F701D483F40A2891AC6894D5E4067DC406F1D483F406B60E3AE894D5E40C5C28D6E1D483F40F20A9596894D5E40EED0226E1D483F400DBF667C894D5E40E901F76D1D483F40066B135C894D5E40432CBF6D1D483F4006A89C3B894D5E4098A07F6C1D483F4007711325894D5E4034B2116B1D483F40F12EAF0D894D5E40ECDB4A691D483F407AFDFDED884D5E405AB2AA661D483F4065307ECE884D5E402C6A4E631D483F40CFDADAB3884D5E40492A01601D483F406D38229B884D5E407EAAC45C1D483F40E424B884884D5E409C45FB591D483F4005C1D969884D5E404C3B48561D483F40A719C752884D5E402ABD0E531D483F40AD651E38884D5E40D1EF874F1D483F40E00A771A884D5E4037DDC54B1D483F402D0DC4FE874D5E40B439CD481D483F40D747D5E6874D5E40648D8B461D483F40F24CEECB874D5E4001B841441D483F40CE1998AF874D5E406C360B421D483F4084317398874D5E4088542F401D483F40889D4A82874D5E4016F93D3E1D483F4042967768874D5E4091619D3B1D483F404E79D74D874D5E404E607F381D483F4010542833874D5E4002C109351D483F4038C54315874D5E40DCF0B6301D483F400E3F02F6864D5E408461D02B1D483F40B5A20AD5864D5E40D78F71261D483F404E2F75B3864D5E40C426C7201D483F40C9CEE091864D5E40144E191B1D483F4015526A6E864D5E40D6FB55151D483F407ACD7849864D5E400009BB0F1D483F4083CCA523864D5E4092AD6B0A1D483F40CD9978FD854D5E4039BA96051D483F40D68B7BD5854D5E4046FA1E011D483F40344F2FAA854D5E40A3ADF3FC1C483F40611CAD81854D5E406E06B2F91C483F4000D2396A854D5E40054918F81C483F4027FB843F854D5E409D40B4F51C483F4018A86E29854D5E40226DB9F41C483F4057E9BE01854D5E40661C60F31C483F40837F49DE844D5E404D49A8F21C483F40E6B568BA844D5E405B0773F21C483F40CFAAF79D844D5E402198B2F21C483F4027903A83844D5E40F7C140F31C483F405C059E6C844D5E401095F8F31C483F4088201851844D5E40AEF226F51C483F4025261A37844D5E4012E194F61C483F408B476A20844D5E40F383FEF71C483F40876CAE07844D5E40B79DA8F71C483F40538866EF834D5E4010B5C9F61C483F407B2000D8834D5E403FABAEF51C483F40B21E70BD834D5E40AAD830F41C483F4060CF379F834D5E40904319F21C483F40A9AC1589834D5E4081B047F01C483F4046BF4171834D5E4011E50FEE1C483F4069AD7C57834D5E405A695FEB1C483F4022B9FF3F834D5E408432A7E81C483F40C16E8C28834D5E40C94BB9E51C483F40122E8E11834D5E404E07A6E21C483F40ABC722F2824D5E40491E26DE1C483F40402F1ED6824D5E40CCEBE7D91C483F408D3E95BC824D5E40E8FCE1D51C483F4019867BA4824D5E40D2FD00D21C483F40DC74F48D824D5E4049B058CE1C483F402AD5B273824D5E40366DFCC91C483F4005C2DC5B824D5E404DE7EDC51C483F4086C85B41824D5E40C07F4FC11C483F40AD15DA2A824D5E408B2955BD1C483F4098FEA629824D5E40FD771CBD1C483F4072A0B512824D5E40DC25AEB91C483F407204BDF8814D5E40745BEAB51C483F402760B5DE814D5E4030347EB11C483F408E74DBC5814D5E40290C8CAC1C483F4002464BA9814D5E407EB73FA61C483F404A60488E814D5E40C4C9A49F1C483F407E773A76814D5E40E11EBC981C483F40A6BE8C5F814D5E40A1F7F1901C483F4026D86147814D5E40152B35871C483F407CD0FA2E814D5E408F4C457B1C483F40CA8E2A16814D5E4024662B6C1C483F40626293FF804D5E4062D25B5C1C483F408456FAE5804D5E40FAC595481C483F408124CBCD804D5E4029E311341C483F40499624B3804D5E407EAFDF1B1C483F406C40429C804D5E40BB90A3051C483F40BBA7FE83804D5E40CDCC6BEC1B483F404D287C6A804D5E404DCCECCF1B483F4035306150804D5E40B5EB8BB01B483F4093ACAC35804D5E40CF07C78D1B483F40DFB4251A804D5E400B6816671B483F4034C01404804D5E406EBE06461B483F401F7884EC7F4D5E40D1FAA2201B483F406B490AD67F4D5E401E07E8FA1A483F4005C6FFBF7F4D5E400C58D9D31A483F40F7BD04A97F4D5E40D248BEA81A483F40C832D4937F4D5E405DB1BE7E1A483F40286CD47D7F4D5E405E5581501A483F40BFF421687F4D5E407FD3EC1F1A483F401D2D50507F4D5E401CA6E0E619483F4095C2723A7F4D5E40DCB1C0AE19483F402124AD267F4D5E40F9E83A7819483F400C47B8127F4D5E40F812363D19483F4075CD77FF7E4D5E4052869AFF18483F406D76D9EC7E4D5E40A467A1BE18483F40ACE886DB7E4D5E406ED0067C18483F4036BB88CB7E4D5E40E0DCFD3718483F4027ED61BD7E4D5E40EDDBE5F417483F40C2E81AAF7E4D5E40FA5B5BA817483F406A19E3A27E4D5E40D5F1435D17483F407BA356987E4D5E400CF1661217483F40BB43808F7E4D5E40F6B532C916483F405DF793877E4D5E4076D5B67B16483F4033AFD9807E4D5E406836D82B16483F401D847E7B7E4D5E404367A2DB15483F405A4A54777E4D5E4052D9BD8715483F408B408A747E4D5E40F486F03115483F40010638737E4D5E4022D4E4E014483F40DD7B04737E4D5E406F20FE8C14483F407F63C0737E4D5E40212A0F3E14483F40B80427757E4D5E400349A6EB13483F40D0BEBA767E4D5E40BE929D9B13483F404ECBFB777E4D5E4089A6594813483F404BD753787E4D5E402935EBF412483F40BC5D3E787E4D5E40F9ECD9A312483F406384BD777E4D5E40F41CC95012483F4088590C777E4D5E40F322910112483F40516132767E4D5E4002D9C9AA11483F40783055757E4D5E40745D9F5211483F400E8C8E747E4D5E4028C6F30611483F4087F49C737E4D5E40070805AD10483F40883FB8727E4D5E404A14385A10483F405147DE717E4D5E403E6D7C0110483F40059642717E4D5E40A3FB4AC30F483F40FE55AD707E4D5E40E3247B6A0F483F4033F641707E4D5E409D4177150F483F404218FB6F7E4D5E4022D2B5C60E483F40BB2FC26F7E4D5E402DE8D8610E483F40AF01B16F7E4D5E4088CB57ED0D483F4019F7BE6F7E4D5E4008BDB0950D483F40B39EE56F7E4D5E40AC6883350D483F409FE523707E4D5E4070FC58DA0C483F40A71F8D707E4D5E405799ED6E0C483F40267D15717E4D5E4042C190060C483F4082F9F6717E4D5E404B136B7F0B483F40FE62D7727E4D5E402CAE5B100B483F40D98D88737E4D5E40E8CC59C20A483F401F57D4747E4D5E40CC8E833C0A483F407B826E767E4D5E40441EF7A409483F40793753777E4D5E40096A345509483F40206BFC787E4D5E40A986AFC608483F40F20AFD797E4D5E40CDC4337308483F40F656EA7A7E4D5E40D0550A2708483F4069E4D27C7E4D5E40358F7A8A07483F4008879F7E7E4D5E4076C276F006483F40CEF88E7F7E4D5E40A3DCF29B06483F40F3DA07817E4D5E40F120610B06483F40C28660827E4D5E4003B0EF7605483F400A4A80837E4D5E40E7E57CE504483F40D74C4C847E4D5E40D720826004483F40D35ED0847E4D5E408C0DC8E303483F4008AE1D857E4D5E408F0FE96503483F40B61432857E4D5E405C0A3CF602483F408BF921857E4D5E406E893C8702483F40938AFE847E4D5E402CB0D22402483F405013CC847E4D5E4084FACFBF01483F40967EA6847E4D5E40FE85966401483F40AFD49C847E4D5E403CE7E90B01483F40FF73B4847E4D5E402B408CB600483F40103FFA847E4D5E4069A7754200483F40386662857E4D5E40E2C46ED7FF473F407252E4857E4D5E40759EFD74FF473F4073FB70867E4D5E40572B7B18FF473F405020F6867E4D5E40177514C5FE473F4026AE72877E4D5E40B3BFE677FE473F40EF130A887E4D5E40B4FA4119FE473F402F9794887E4D5E4057C845C6FD473F402AA918897E4D5E40BDC3CA78FD473F40E7E09E897E4D5E4058918A2CFD473F4033923A8A7E4D5E408A059FDEFC473F4032471F8B7E4D5E40854B807EFC473F4044BBF18B7E4D5E4007EE932EFC473F404E9EE78C7E4D5E405ABE0EDAFB473F40E069168E7E4D5E40CA161982FB473F4076D2798F7E4D5E4089F5162BFB473F40DF2B25917E4D5E40984631CFFA473F4070A60C937E4D5E405773446BFA473F40C1A395947E4D5E40FF93331BFA473F4091F87A967E4D5E40D85BDBB7F9473F40E0FB2F987E4D5E40E3D61C58F9473F4045B27A997E4D5E4077D3AE04F9473F40E4A58E9A7E4D5E406A6748B8F8473F404B56AD9B7E4D5E406F997D65F8473F405CD0AB9C7E4D5E405C389D17F8473F40320DE19D7E4D5E409AE705B2F7473F403CF0D69E7E4D5E40F0B7225AF7473F401427E09F7E4D5E40B44E43F1F6473F4053B096A07E4D5E40E069EFA1F6473F406CB545A17E4D5E407FDF5C4FF6473F409E10EBA17E4D5E4045C67BFAF5473F402F338DA27E4D5E40D0165C9FF5473F40FE2F2DA37E4D5E40B55D5C3BF5473F40A7A8C5A37E4D5E406FF236D2F4473F4007194FA47E4D5E40C7D71568F4473F40479CD9A47E4D5E40AD447DEBF3473F40F20E46A57E4D5E402A28F375F3473F40B040A0A57E4D5E40CE2426FBF2473F40A11EE7A57E4D5E40C47AF378F2473F40813714A67E4D5E4005ACD4F1F1473F40AC5224A67E4D5E40E3556875F1473F40C5A81AA67E4D5E404C8237EDF0473F40CD39F7A57E4D5E40C1BF786EF0473F40E789C1A57E4D5E4048164DFBEF473F40547377A57E4D5E4073D3A090EF473F409BD825A57E4D5E409D36E535EF473F403B6EC8A47E4D5E40AC5D36E1EE473F4076A565A47E4D5E40A0F1CF94EE473F40308BD2A37E4D5E408EE35133EE473F40A06830A37E4D5E40E7DE09D6ED473F40F0588FA27E4D5E40A5AB9083ED473F40302D61A27E4D5E40246EA16DED473F4010468EA27E4D5E4024022117ED473F40754D20A37E4D5E40230B34BAEC473F403CB9E3A37E4D5E40FB3B5C6DEC473F405DFE27A57E4D5E40CB540710EC473F4054D1E8A67E4D5E402CD96DABEB473F4064FA9FA87E4D5E406787EB5DEB473F4047C357AB7E4D5E407D61B4FAEA473F40A9E36BAE7E4D5E4070B47BA3EA473F40071604B27E4D5E409DE84B53EA473F40766B27B77E4D5E409ED90CF9E9473F4048257CBC7E4D5E40791EAEAEE9473F40B5E468C37E4D5E403DA20E5FE9473F40AFD24FCB7E4D5E4048345911E9473F40859474D47E4D5E40CAA5F6C4E8473F40C10CE4DE7E4D5E407AFB017CE8473F40CCD938EB7E4D5E40D8C94730E8473F40D4B811F87E4D5E4057104AE9E7473F40A6489D047F4D5E404B56CEA9E7473F400C47B8127F4D5E4083517667E7473F40128A2E217F4D5E402D442127E7473F4039705A327F4D5E4020949DDDE6473F4033E330427F4D5E40CE042D9EE6473F40208C93537F4D5E406CC9C65DE6473F40CCB16B647F4D5E40AAE06623E6473F40AAE7CD767F4D5E40A58C7DE7E5473F40949C88887F4D5E40184B5FB0E5473F4088D6C7997F4D5E403B39B37CE5473F40AD2091AC7F4D5E4076188E45E5473F40BCC4EEC17F4D5E4046A36809E5473F40627A3CD97F4D5E40FA91DCCAE4473F40168F62EB7F4D5E40F7923E9CE4473F402BCAC800804D5E40EC7D8367E4473F4076F60A15804D5E40E62CCC37E4473F409BFCB62A804D5E4002781D07E4473F402BF24440804D5E40EE621ED9E3473F40C378AF55804D5E40E7FFEFADE3473F40501C246A804D5E40E346CF86E3473F408143B77D804D5E40673F4563E3473F402E9D3797804D5E401EFBC937E3473F405A9F5BB0804D5E40318B2410E3473F404DB5FDC8804D5E40D2FF2FECE2473F40EED342E0804D5E4071FED5CCE2473F403276A6F6804D5E40F14821B1E2473F40E05E5F0C814D5E4017897598E2473F40FE82E722814D5E40E02D4781E2473F40539F493A814D5E40D94CBD6BE2473F406A976454814D5E40ED6AB656E2473F409634196A814D5E406E93B747E2473F40AD705181814D5E40D401833AE2473F40D47DFB98814D5E4020405530E2473F40206D1EB2814D5E40AB8B9D29E2473F40FFD0FCCC814D5E406D086427E2473F4078EF57EA814D5E40D08C662AE2473F406FBC8207824D5E404B87DF31E2473F40A17C2D27824D5E40F6BD5C3CE2473F40514F4548824D5E4050DE2C46E2473F407F8F5A5E824D5E40400EEB4AE2473F40835DEC74824D5E40DABCBE4EE2473F40B63A368B824D5E40850A5E51E2473F4076A4EBA3824D5E4095EE7652E2473F40554CE7BB824D5E4079271752E2473F40BF99D0D8824D5E403E5AE550E2473F40B0C2C8F3824D5E4010BBC44FE2473F404C7BCC05834D5E4044E4C34EE2473F40EEA00F1F834D5E409D4C2C4DE2473F4086F08639834D5E4031EC314BE2473F405EB65E52834D5E40FF960949E2473F40373F1770834D5E40381E1C46E2473F4074FF5687834D5E40BC88C143E2473F40D3AD67A0834D5E400ADC3C41E2473F40BD96CABA834D5E40D647A23EE2473F40D78FB7D6834D5E405E0ADE3BE2473F406BBABFF6834D5E409921AA38E2473F4059B81C17844D5E4063B28B35E2473F40B35B123A844D5E4032768732E2473F40FED5505B844D5E409F840A30E2473F404EA37A7F844D5E406FF7BE2DE2473F40260896A4844D5E40B7C6D82BE2473F40AA7B80C8844D5E407F89602AE2473F409B9403EF844D5E402132FB28E2473F40ED682B15854D5E405254AB27E2473F405E7B993A854D5E4031AB7826E2473F40DD135E61854D5E40C4315A25E2473F40D6B79187854D5E4082054324E2473F40A1F4C7AC854D5E4086AE2023E2473F40100D62D2854D5E4038F00922E2473F40DE3D40F7854D5E4097022221E2473F409613CE1D864D5E40C2A07020E2473F40CD39BA42864D5E4072015920E2473F40928EA068864D5E40B84DE320E2473F40D397828E864D5E40C00A3A22E2473F40100430B4864D5E404FF56724E2473F406BB44FD9864D5E4081F94827E2473F40232C6CFE864D5E408AEF942AE2473F4038648721874D5E403DAFC02DE2473F40EC756241874D5E4054C7C530E2473F40E6D8C35C874D5E401E6C7E33E2473F40CE25C478874D5E40A76E5C36E2473F406FC9F897874D5E40EE3EA639E2473F40ADE17DB0874D5E401CD8493CE2473F40524CB0C8874D5E40E6E0F03EE2473F4066EBB3DF874D5E40604A8041E2473F407F7207F6874D5E402B1B0444E2473F40CEA6190E884D5E40AF4EB646E2473F4080F51329884D5E407ACEF249E2473F400B884147884D5E40C5419D4DE2473F4000FD2663884D5E40E6930B51E2473F4071B46E79884D5E4044ADD053E2473F40C32D7090884D5E4038D7B356E2473F40FA63D1A9884D5E40F728DF59E2473F4074C01DC4884D5E40E1C7115DE2473F40ADAC35E0884D5E402F2F6460E2473F404FECCCFD884D5E407537D163E2473F408C82431C894D5E4025674367E2473F40C7383A3F894D5E40DED01E6BE2473F40E3A3C060894D5E407414B56EE2473F40BB3FCF80894D5E4000560572E2473F40D783D79B894D5E4021F9BA74E2473F40CDEB92B5894D5E40BA814077E2473F40A3AA6CCC894D5E40B3937379E2473F40DF7D02E6894D5E409FE7DB7BE2473F400DCB41FE894D5E4046921A7EE2473F404B00660D8A4D5E40097A7C7FE2473F4014A484268A4D5E4067D1E180E2473F407B34B93E8A4D5E40D4AEEE81E2473F4039F5E1568A4D5E4090ECA082E2473F40
3	10	0102000020E61000002D00000066E099338F4D5E40C6A5A3C51D483F4004ED991B8F4D5E408D9AD1C51D483F409BFD21008F4D5E406D17EDC51D483F401F3C66E28E4D5E404C9408C61D483F404A5D0CC78E4D5E40F6F9F9C51D483F40D23187A78E4D5E40B9AFB5C51D483F405B347E908E4D5E40729B4EC51D483F40B66BDA768E4D5E407DB6B8C41D483F40F157C65B8E4D5E4001B7F2C31D483F4055BC61408E4D5E406728F0C21D483F407F2751228E4D5E40862198C11D483F40B4B327028E4D5E408320C6BF1D483F407CA091DF8D4D5E4083778ABD1D483F405A3367BA8D4D5E40000CB5BA1D483F40AF92E8958D4D5E40BD0A97B71D483F40818C116D8D4D5E408CBBEBB31D483F4014B966468D4D5E400ACD28B01D483F406DEC0F1D8D4D5E40296EDCAB1D483F402F7964F58C4D5E408459D4A71D483F408DF8FACC8C4D5E409F3E03A41D483F40F89F76A48C4D5E40D0B777A01D483F4093AEEF7C8C4D5E408288489D1D483F4046BB19548C4D5E403EBF4D9A1D483F4063CE2E3D8C4D5E40BB79C6981D483F40D2767C168C4D5E4034526C961D483F40CF3A6EEE8B4D5E40EE6F4D941D483F400C31A3C78B4D5E40B9C28E921D483F405F4509A48B4D5E40B20538911D483F40986716828B4D5E4013A809901D483F40E1B2DA618B4D5E4088D7D18E1D483F4092F9D8418B4D5E4017F3758D1D483F40066DD7238B4D5E40056CFC8B1D483F40CED106078B4D5E4044B0658A1D483F40BC70F4EC8A4D5E400740D3881D483F40CF93E9D28A4D5E40994F1F871D483F4042D620BA8A4D5E4095EA5E851D483F409DBC35A18A4D5E40E4B46F831D483F408435E28A8A4D5E40533A88811D483F40D217AF6A8A4D5E40BED1757E1D483F407E25164C8A4D5E40B5DB297B1D483F4060CDE52C8A4D5E40BDCF73771D483F402DF09B168A4D5E40465A8C741D483F403E87390E8A4D5E40569569731D483F40632B2BF7894D5E40F536B5711D483F40EEC984DE894D5E40AF034F701D483F40	7	12	1	30.997734213079685	30.997734213079685	0105000020E61000000100000001020000002D00000066E099338F4D5E40C6A5A3C51D483F4004ED991B8F4D5E408D9AD1C51D483F409BFD21008F4D5E406D17EDC51D483F401F3C66E28E4D5E404C9408C61D483F404A5D0CC78E4D5E40F6F9F9C51D483F40D23187A78E4D5E40B9AFB5C51D483F405B347E908E4D5E40729B4EC51D483F40B66BDA768E4D5E407DB6B8C41D483F40F157C65B8E4D5E4001B7F2C31D483F4055BC61408E4D5E406728F0C21D483F407F2751228E4D5E40862198C11D483F40B4B327028E4D5E408320C6BF1D483F407CA091DF8D4D5E4083778ABD1D483F405A3367BA8D4D5E40000CB5BA1D483F40AF92E8958D4D5E40BD0A97B71D483F40818C116D8D4D5E408CBBEBB31D483F4014B966468D4D5E400ACD28B01D483F406DEC0F1D8D4D5E40296EDCAB1D483F402F7964F58C4D5E408459D4A71D483F408DF8FACC8C4D5E409F3E03A41D483F40F89F76A48C4D5E40D0B777A01D483F4093AEEF7C8C4D5E408288489D1D483F4046BB19548C4D5E403EBF4D9A1D483F4063CE2E3D8C4D5E40BB79C6981D483F40D2767C168C4D5E4034526C961D483F40CF3A6EEE8B4D5E40EE6F4D941D483F400C31A3C78B4D5E40B9C28E921D483F405F4509A48B4D5E40B20538911D483F40986716828B4D5E4013A809901D483F40E1B2DA618B4D5E4088D7D18E1D483F4092F9D8418B4D5E4017F3758D1D483F40066DD7238B4D5E40056CFC8B1D483F40CED106078B4D5E4044B0658A1D483F40BC70F4EC8A4D5E400740D3881D483F40CF93E9D28A4D5E40994F1F871D483F4042D620BA8A4D5E4095EA5E851D483F409DBC35A18A4D5E40E4B46F831D483F408435E28A8A4D5E40533A88811D483F40D217AF6A8A4D5E40BED1757E1D483F407E25164C8A4D5E40B5DB297B1D483F4060CDE52C8A4D5E40BDCF73771D483F402DF09B168A4D5E40465A8C741D483F403E87390E8A4D5E40569569731D483F40632B2BF7894D5E40F536B5711D483F40EEC984DE894D5E40AF034F701D483F40
6	9	0102000020E610000052010000352A38088F4D5E4059DEA65617483F4007AA4E208F4D5E40D7C51A5A17483F40BBA4202E8F4D5E4094291B5C17483F40C3665A448F4D5E4039E0B15E17483F40C44D6E5D8F4D5E40DBA2A06117483F4043A560798F4D5E40A21B8E6417483F402E8897938F4D5E4004C50C6717483F409FB7A4AF8F4D5E402161736917483F40D143A7C68F4D5E40F27D356B17483F40415F8CDE8F4D5E40EA18D36C17483F400E5CC2FA8F4D5E404049766E17483F40816B4F12904D5E400B589A6F17483F40A0386429904D5E4060D9847017483F40AF8B7A3F904D5E401C17377117483F40578CE355904D5E409A42C87117483F40BB42C472904D5E40CE654A7217483F40121CDB8E904D5E405AEBB77217483F405B82F1AB904D5E404ED04D7317483F400C0E0BC2904D5E406A33BF7317483F4048FBF4DF904D5E40E85E507417483F40433ED6F6904D5E40FE2AB97417483F4000052B0F914D5E4013F7217517483F40F1714027914D5E405F76C67517483F4003820B42914D5E401594057717483F4006C1645C914D5E40B1FD8B7817483F40E7CDCF77914D5E40172B6C7A17483F407D81E493914D5E406ED2A47C17483F40875E44B0914D5E401984327F17483F4062E1D0CD914D5E409C8B198217483F4025083BEB914D5E406468F58417483F403D1AAA09924D5E40C0DE598717483F40E303A029924D5E409892248917483F40104AE13F924D5E403EB3268A17483F40ACFF995F924D5E4084AE698B17483F4084ECEF7E924D5E4035998E8C17483F40DA943FA0924D5E401D37EF8D17483F40ADF18AC1924D5E40574FEB8F17483F403F3F5ED8924D5E40CB3A969117483F403F1F74EF924D5E403EFA759317483F4064D4D805934D5E40E239779517483F408B903B1E934D5E408F17D09717483F409EDEF735934D5E40D269359A17483F403FFD3C4D934D5E408B759F9C17483F40E904A465934D5E40C7681F9F17483F40B8E1597D934D5E402AA257A117483F4012447D95934D5E40CFA935A317483F401B0DB5AD934D5E40896AD5A417483F40DBC7B1C5934D5E40F3533AA617483F4030E413DD934D5E4092B168A717483F400B4D4CF6934D5E40B4BE89A817483F40717F0F0D944D5E40C5CE6DA917483F4088702C25944D5E4054F73BAA17483F40F497693D944D5E404878E3AA17483F40BAE96E55944D5E40E02B62AB17483F40438FF275944D5E409EF9CDAB17483F40FED33894944D5E40E79DEEAB17483F40FB88B3B0944D5E408471E0AB17483F40B41045CA944D5E402C0FF5AB17483F40EEDCDCE1944D5E406FB81EAC17483F40BD5B21F8944D5E405B634BAC17483F40EF550A05954D5E40477266AC17483F408A8CFF1C954D5E40F5553CAD17483F40DA5C7433954D5E40100478AE17483F403BF82E4A954D5E404841F0AF17483F403B36B662954D5E408FB3C8B117483F40488FF878954D5E40080AB1B317483F40F6E24C92954D5E4038CF1FB617483F4070E127AB954D5E4002D8C6B817483F40358AA0C2954D5E40035C86BB17483F40CBDF43DD954D5E401185ECBE17483F406C18DDF8954D5E403B11C4C217483F40219F9C10964D5E40864C4BC617483F40B3F99929964D5E40D9E6B7C917483F40F8069943964D5E4073371ACC17483F408F05C95E964D5E4076D4FDCD17483F40BC655E79964D5E40229FAFCF17483F402A435294964D5E40D1F91AD117483F40BDA44DAF964D5E40E54871D217483F40050466CA964D5E40E7369CD317483F4017B6BFE3964D5E40799EDCD417483F4065F7FBFD964D5E405DE4A6D617483F40F40C0A18974D5E40B5B7AAD817483F40197E5131974D5E40BCBFCBDA17483F4090CCA147974D5E40D6B8D1DC17483F4064A4FD60974D5E40253948DF17483F408A533677974D5E4017EC95E117483F40CA071E8E974D5E40E2BC19E417483F407EB4F38E974D5E400DA632E417483F403EC037A6974D5E4086B150E517483F40E18709BE974D5E40BDC87AE517483F402CBB49D4974D5E40637864E417483F401AE36FED974D5E4081139BE117483F402B8ECB04984D5E40EBD581DC17483F4071372D1D984D5E40B4C2D2D317483F40D1DF1136984D5E404533DBC517483F405A44C34B984D5E40294FF5B417483F409C43C661984D5E404950359F17483F40C8890778984D5E4080D1E38317483F40C2235C8E984D5E401ADB0F6317483F40A2BE2DA4984D5E40B37F1D3D17483F4064B1EFB8984D5E40A04C4F1317483F40511635CD984D5E40E373EDE316483F40ACAF4BE0984D5E40D6450EB016483F403E3412F2984D5E40D1249D7716483F40015EC701994D5E40FB31343E16483F4081619110994D5E407059510016483F405837DE1D994D5E40871662C015483F400543622A994D5E402E9D9F7A15483F403EC72935994D5E408ABD5C3315483F40E0E28D3E994D5E4082CFDAE714483F407FFD1F46994D5E408ACBEE9B14483F405E8E124C994D5E404B0A604D14483F40BD693750994D5E40AA99D5FD13483F406DD74952994D5E40484766AB13483F40110A2152994D5E4025ABA85E13483F40C6F4E74F994D5E406F031A0B13483F40A4F9EC4B994D5E4085E144B912483F4018B89C48994D5E4053542B8312483F40F0903448994D5E40049CDC3212483F4029D42948994D5E403DFEBDE211483F4091CF6348994D5E40E394709111483F4023ECD948994D5E401B9A104511483F400FDCA449994D5E40838DB8EE10483F400C97B54A994D5E401694429110483F40DD3CE24B994D5E40C0CA233410483F4038BFEF4C994D5E40B005F7E20F483F4079EB064E994D5E40391E128B0F483F40B0E3E04E994D5E40BE3C08350F483F407FE0804F994D5E40D1B442DC0E483F404FD7F44F994D5E40C7D1E88B0E483F403FBB6750994D5E409EE58A2E0E483F400384CA50994D5E40CA8BBCCB0D483F4072160D51994D5E4096A17A7F0D483F4038D94351994D5E40A8F01B330D483F4036DF6F51994D5E40D363BCE60C483F406D289151994D5E40F412B7980C483F40FCA1A651994D5E4083092A4A0C483F400239AF51994D5E401C3229F30B483F40BDC7A851994D5E4062DB3FA20B483F4092AC9851994D5E4099FEAE510B483F401D897951994D5E405E81DFFC0A483F40E0A84F51994D5E40A41DCCA90A483F40979A1451994D5E40542B58520A483F40C5A9CC50994D5E40E86B07FC09483F404BE97850994D5E401C4957A709483F40A60D1550994D5E404CE6625009483F40B092994F994D5E40B2D708F308483F40D46D144F994D5E40DDE9059908483F40A0189B4E994D5E402BEA243F08483F403286584E994D5E408A7D15E707483F40D4BE5B4E994D5E4060F6D58E07483F40A6AFA34E994D5E4007660A3707483F405D50214F994D5E40040C5FDF06483F4045A3B74F994D5E40CEECA28806483F403FBB6750994D5E400540582F06483F40A3C82551994D5E40C41E56D805483F408FB8F051994D5E401370BC7D05483F409B95BA52994D5E405248A52505483F40C0C87A53994D5E4065626ECB04483F4051EB1C54994D5E402D61817004483F409705B054994D5E40F601231404483F4079C13D55994D5E4031AC3EB503483F40E45FD855994D5E401158BB5903483F408CDE9C56994D5E408C03530103483F40723D8B57994D5E40FA8656A702483F402A879558994D5E406BD8B34B02483F408BA0AB59994D5E40D17FBFF201483F407A33D75A994D5E40D8BCBE9701483F4051700C5C994D5E40E758A33C01483F402116395D994D5E40C94392E100483F404355515E994D5E40F2B1FE8100483F40867B3C5F994D5E40228C7A2400483F4035910960994D5E404CC2C3C4FF473F40BF1CA360994D5E400FF9576DFF473F40B5971E61994D5E40F26EB318FF473F4015027C61994D5E40A72ECCCAFE473F40577FDA61994D5E40AC188269FE473F4042C61862994D5E4088087A0EFE473F403A353C62994D5E4006D5B4BAFD473F40C3174962994D5E4005E1F969FD473F4060B94362994D5E403742671BFD473F4092653062994D5E403416E7CDFC473F4036980762994D5E40E4164974FC473F40B546D761994D5E409F6D6A26FC473F40FF4EE661994D5E40E10208D3FB473F40B7E93762994D5E40D8839C82FB473F40EC3EB162994D5E40B52BFC32FB473F4076CA4A63994D5E4003F657E3FA473F40379F0564994D5E4033317793FA473F406D9D0B65994D5E409EAD2B39FA473F4070EF2466994D5E406A25D6E5F9473F4058F17367994D5E40D851158EF9473F4067719E68994D5E40EDA70542F9473F40516DC169994D5E40ED7C57F6F8473F4009B7CB6A994D5E40B2B5EDA5F8473F40BD63A16B994D5E403205B84FF8473F400284606C994D5E401E9B28F7F7473F4066911E6D994D5E40375FA19DF7473F40852DD66D994D5E4073C59F41F7473F40A4C98D6E994D5E4027F719E1F6473F40B737346F994D5E40FB036D83F6473F4023D6CE6F994D5E4003E97725F6473F402A166470994D5E401F5A49C2F5473F40C860EB70994D5E409265295FF5473F4009DE4971994D5E40117F3013F5473F4045C49F71994D5E40619528C6F4473F401D4CF071994D5E40AB6BDC76F4473F404C043572994D5E40E1825129F4473F4057387272994D5E40A9CF60DBF3473F401DFBA872994D5E40C18E8C88F3473F401B01D572994D5E4067C15037F3473F40524AF672994D5E40F77923EAF2473F40640F1073994D5E405BCB9A98F2473F40CE041E73994D5E40919DC248F2473F40703D2173994D5E40400011E8F1473F40A9801673994D5E409FE4488FF1473F40FA190273994D5E401453CE40F1473F40A4E3E172994D5E407313BAEDF0473F409E4CD972994D5E4066C822DDF0473F4039F4FF72994D5E40D979F389F0473F4043283D73994D5E4040881F3DF0473F4066B89C73994D5E409201D6EAEF473F402A81FF73994D5E4092DCA49DEF473F4034B53C74994D5E40DA2FC950EF473F402F1E3474994D5E4022631C01EF473F40A19EF273994D5E4060C7A1B4EE473F40FFB63673994D5E40FBD87E65EE473F409469E371994D5E408BA4F818EE473F4097FF1970994D5E400526C7C7ED473F408F61136E994D5E400BE3687BED473F40BF5D756B994D5E4097564C2FED473F40AC33EC67994D5E40EE16F4E2EC473F401DF1C962994D5E40DEA9BB94EC473F40705D0B5C994D5E40E1783247EC473F40C65F8353994D5E40EDF9D7FFEB473F4069A8FB47994D5E40C3D46BBAEB473F40A6DCB739994D5E40E6092876EB473F40975FD82A994D5E40C8F52D3AEB473F407EAEBB1B994D5E4086200D03EB473F40CC3CF609994D5E40176DFBC6EA473F40A11707FA984D5E403C674990EA473F408BABD9E9984D5E400FA95B5AEA473F40E179A9D8984D5E40A5F81D27EA473F40878F4BC6984D5E405C7602FDE9473F4090A581B2984D5E40FE487FDAE9473F4027D75B9D984D5E40CDE19ABFE9473F4082280C88984D5E407A7572ACE9473F40A5D85571984D5E40299588A1E9473F40A3FE6B5A984D5E404F00BDA0E9473F408725FF42984D5E4024C0DFA2E9473F407E0B802B984D5E406718C2A3E9473F40545B7513984D5E4094D5B1A0E9473F406C6CE6F8974D5E403A5E1D99E9473F4046BDADE2974D5E40DE697692E9473F40157CC6CA974D5E403C8D338BE9473F4066A037B7974D5E40E6423C85E9473F4053A3C29E974D5E40BE337780E9473F40F23EFB82974D5E4007AB9C7BE9473F406BAD9F66974D5E4087E8A477E9473F40E1850D4C974D5E40D993B674E9473F407665CE35974D5E40C6F99572E9473F40CD06F41D974D5E40764D5470E9473F40B31A3104974D5E40C7A71E6EE9473F40F3A351E9964D5E40F1BB306CE9473F40021DE8CC964D5E401840896AE9473F405E850BAE964D5E40EBCC3369E9473F407388808F964D5E407CC35B68E9473F40D4DE1F70964D5E400367F667E9473F401B301050964D5E40281DF567E9473F403EE05939964D5E40EE112368E9473F40056C0723964D5E401ECA6768E9473F40145D630C964D5E40610FA368E9473F4061CA4BF4954D5E40C4D7C268E9473F40AA9AFFDB954D5E403396D068E9473F40AC6376C5954D5E408499D668E9473F40B071DCAD954D5E407807D768E9473F40D70FA296954D5E40014ED268E9473F40B447E77D954D5E40FBB6C968E9473F40EB013A66954D5E40928FC468E9473F401135644B954D5E40F61FC168E9473F40F1605132954D5E406642BD68E9473F409F2B6D18954D5E4028CCAD68E9473F402F54A5FD944D5E40C6677C68E9473F4019565EE3944D5E4040791768E9473F4013C5EBC7944D5E4078A96567E9473F40D893E4AC944D5E4010636166E9473F40D6FD1793944D5E4079641865E9473F40613E0079944D5E40B3117963E9473F405CA7615D944D5E40FDA86F61E9473F40E444E942944D5E40DEE03D5FE9473F400826FA26944D5E40BB11BD5CE9473F4074C4FE0B944D5E40BF5C295AE9473F40BDBE7BEC934D5E40CD5E1157E9473F40D27DD3D0934D5E40C57B6C54E9473F40D23222B6934D5E400B0C1452E9473F406C447C9C934D5E40282A3850E9473F403608EF82934D5E40AB17CB4EE9473F407847C66A934D5E409ACFC34DE9473F405A233E54934D5E40A0B7134DE9473F408C9ECD3D934D5E40D657A84CE9473F40D79F4820934D5E404842814CE9473F40C8832505934D5E40C266C34CE9473F40A90758ED924D5E40F08E4E4DE9473F40228326D3924D5E40BCC93D4EE9473F40EFA5DCBC924D5E40A823234FE9473F40105C52A6924D5E40B6E1ED4EE9473F409C52F18E924D5E40B33E8D4EE9473F40EE5C0E77924D5E40675BFA4DE9473F4005100E5B924D5E4068D80C4DE9473F40FBD43C3D924D5E4042FCBF4BE9473F4087F5A41E924D5E40FF58134AE9473F4020E080FE914D5E4005E3F147E9473F40B73426E0914D5E408A4D9745E9473F4040D9ABC7914D5E4000FA7143E9473F40B396F3A6914D5E40930F3B40E9473F4016C49B90914D5E40E695D03DE9473F40929EAD79914D5E405B67273BE9473F402D084D61914D5E4068A13238E9473F40BF378348914D5E4012AF2F35E9473F400EFDB031914D5E402F828932E9473F40C20D8E18914D5E403A90C92FE9473F409DFAB700914D5E4037E03E2DE9473F40187758E8904D5E403430B42AE9473F40B625E7CE904D5E408F791D28E9473F40B4ED8BB6904D5E40FC23B225E9473F4094C8319E904D5E408D205723E9473F40759DAB85904D5E40B4910821E9473F40812FC46B904D5E406F13D81EE9473F409B924E52904D5E401BAB111DE9473F40AC49D632904D5E40E499641BE9473F40D40B3914904D5E408BAD3C1AE9473F405612B8F98F4D5E40329C9819E9473F400ECDF3E28F4D5E40CB044D19E9473F406817A6CB8F4D5E4093254619E9473F40884296C88F4D5E4041BE5119E9473F40BEF5EAAE8F4D5E403A9D0C18E9473F40ABA72E978F4D5E40B52BBA16E9473F408529BD7B8F4D5E40F0A0F714E9473F40	2	11	1	189.65322868710817	189.65322868710817	0105000020E610000001000000010200000052010000352A38088F4D5E4059DEA65617483F4007AA4E208F4D5E40D7C51A5A17483F40BBA4202E8F4D5E4094291B5C17483F40C3665A448F4D5E4039E0B15E17483F40C44D6E5D8F4D5E40DBA2A06117483F4043A560798F4D5E40A21B8E6417483F402E8897938F4D5E4004C50C6717483F409FB7A4AF8F4D5E402161736917483F40D143A7C68F4D5E40F27D356B17483F40415F8CDE8F4D5E40EA18D36C17483F400E5CC2FA8F4D5E404049766E17483F40816B4F12904D5E400B589A6F17483F40A0386429904D5E4060D9847017483F40AF8B7A3F904D5E401C17377117483F40578CE355904D5E409A42C87117483F40BB42C472904D5E40CE654A7217483F40121CDB8E904D5E405AEBB77217483F405B82F1AB904D5E404ED04D7317483F400C0E0BC2904D5E406A33BF7317483F4048FBF4DF904D5E40E85E507417483F40433ED6F6904D5E40FE2AB97417483F4000052B0F914D5E4013F7217517483F40F1714027914D5E405F76C67517483F4003820B42914D5E401594057717483F4006C1645C914D5E40B1FD8B7817483F40E7CDCF77914D5E40172B6C7A17483F407D81E493914D5E406ED2A47C17483F40875E44B0914D5E401984327F17483F4062E1D0CD914D5E409C8B198217483F4025083BEB914D5E406468F58417483F403D1AAA09924D5E40C0DE598717483F40E303A029924D5E409892248917483F40104AE13F924D5E403EB3268A17483F40ACFF995F924D5E4084AE698B17483F4084ECEF7E924D5E4035998E8C17483F40DA943FA0924D5E401D37EF8D17483F40ADF18AC1924D5E40574FEB8F17483F403F3F5ED8924D5E40CB3A969117483F403F1F74EF924D5E403EFA759317483F4064D4D805934D5E40E239779517483F408B903B1E934D5E408F17D09717483F409EDEF735934D5E40D269359A17483F403FFD3C4D934D5E408B759F9C17483F40E904A465934D5E40C7681F9F17483F40B8E1597D934D5E402AA257A117483F4012447D95934D5E40CFA935A317483F401B0DB5AD934D5E40896AD5A417483F40DBC7B1C5934D5E40F3533AA617483F4030E413DD934D5E4092B168A717483F400B4D4CF6934D5E40B4BE89A817483F40717F0F0D944D5E40C5CE6DA917483F4088702C25944D5E4054F73BAA17483F40F497693D944D5E404878E3AA17483F40BAE96E55944D5E40E02B62AB17483F40438FF275944D5E409EF9CDAB17483F40FED33894944D5E40E79DEEAB17483F40FB88B3B0944D5E408471E0AB17483F40B41045CA944D5E402C0FF5AB17483F40EEDCDCE1944D5E406FB81EAC17483F40BD5B21F8944D5E405B634BAC17483F40EF550A05954D5E40477266AC17483F408A8CFF1C954D5E40F5553CAD17483F40DA5C7433954D5E40100478AE17483F403BF82E4A954D5E404841F0AF17483F403B36B662954D5E408FB3C8B117483F40488FF878954D5E40080AB1B317483F40F6E24C92954D5E4038CF1FB617483F4070E127AB954D5E4002D8C6B817483F40358AA0C2954D5E40035C86BB17483F40CBDF43DD954D5E401185ECBE17483F406C18DDF8954D5E403B11C4C217483F40219F9C10964D5E40864C4BC617483F40B3F99929964D5E40D9E6B7C917483F40F8069943964D5E4073371ACC17483F408F05C95E964D5E4076D4FDCD17483F40BC655E79964D5E40229FAFCF17483F402A435294964D5E40D1F91AD117483F40BDA44DAF964D5E40E54871D217483F40050466CA964D5E40E7369CD317483F4017B6BFE3964D5E40799EDCD417483F4065F7FBFD964D5E405DE4A6D617483F40F40C0A18974D5E40B5B7AAD817483F40197E5131974D5E40BCBFCBDA17483F4090CCA147974D5E40D6B8D1DC17483F4064A4FD60974D5E40253948DF17483F408A533677974D5E4017EC95E117483F40CA071E8E974D5E40E2BC19E417483F407EB4F38E974D5E400DA632E417483F403EC037A6974D5E4086B150E517483F40E18709BE974D5E40BDC87AE517483F402CBB49D4974D5E40637864E417483F401AE36FED974D5E4081139BE117483F402B8ECB04984D5E40EBD581DC17483F4071372D1D984D5E40B4C2D2D317483F40D1DF1136984D5E404533DBC517483F405A44C34B984D5E40294FF5B417483F409C43C661984D5E404950359F17483F40C8890778984D5E4080D1E38317483F40C2235C8E984D5E401ADB0F6317483F40A2BE2DA4984D5E40B37F1D3D17483F4064B1EFB8984D5E40A04C4F1317483F40511635CD984D5E40E373EDE316483F40ACAF4BE0984D5E40D6450EB016483F403E3412F2984D5E40D1249D7716483F40015EC701994D5E40FB31343E16483F4081619110994D5E407059510016483F405837DE1D994D5E40871662C015483F400543622A994D5E402E9D9F7A15483F403EC72935994D5E408ABD5C3315483F40E0E28D3E994D5E4082CFDAE714483F407FFD1F46994D5E408ACBEE9B14483F405E8E124C994D5E404B0A604D14483F40BD693750994D5E40AA99D5FD13483F406DD74952994D5E40484766AB13483F40110A2152994D5E4025ABA85E13483F40C6F4E74F994D5E406F031A0B13483F40A4F9EC4B994D5E4085E144B912483F4018B89C48994D5E4053542B8312483F40F0903448994D5E40049CDC3212483F4029D42948994D5E403DFEBDE211483F4091CF6348994D5E40E394709111483F4023ECD948994D5E401B9A104511483F400FDCA449994D5E40838DB8EE10483F400C97B54A994D5E401694429110483F40DD3CE24B994D5E40C0CA233410483F4038BFEF4C994D5E40B005F7E20F483F4079EB064E994D5E40391E128B0F483F40B0E3E04E994D5E40BE3C08350F483F407FE0804F994D5E40D1B442DC0E483F404FD7F44F994D5E40C7D1E88B0E483F403FBB6750994D5E409EE58A2E0E483F400384CA50994D5E40CA8BBCCB0D483F4072160D51994D5E4096A17A7F0D483F4038D94351994D5E40A8F01B330D483F4036DF6F51994D5E40D363BCE60C483F406D289151994D5E40F412B7980C483F40FCA1A651994D5E4083092A4A0C483F400239AF51994D5E401C3229F30B483F40BDC7A851994D5E4062DB3FA20B483F4092AC9851994D5E4099FEAE510B483F401D897951994D5E405E81DFFC0A483F40E0A84F51994D5E40A41DCCA90A483F40979A1451994D5E40542B58520A483F40C5A9CC50994D5E40E86B07FC09483F404BE97850994D5E401C4957A709483F40A60D1550994D5E404CE6625009483F40B092994F994D5E40B2D708F308483F40D46D144F994D5E40DDE9059908483F40A0189B4E994D5E402BEA243F08483F403286584E994D5E408A7D15E707483F40D4BE5B4E994D5E4060F6D58E07483F40A6AFA34E994D5E4007660A3707483F405D50214F994D5E40040C5FDF06483F4045A3B74F994D5E40CEECA28806483F403FBB6750994D5E400540582F06483F40A3C82551994D5E40C41E56D805483F408FB8F051994D5E401370BC7D05483F409B95BA52994D5E405248A52505483F40C0C87A53994D5E4065626ECB04483F4051EB1C54994D5E402D61817004483F409705B054994D5E40F601231404483F4079C13D55994D5E4031AC3EB503483F40E45FD855994D5E401158BB5903483F408CDE9C56994D5E408C03530103483F40723D8B57994D5E40FA8656A702483F402A879558994D5E406BD8B34B02483F408BA0AB59994D5E40D17FBFF201483F407A33D75A994D5E40D8BCBE9701483F4051700C5C994D5E40E758A33C01483F402116395D994D5E40C94392E100483F404355515E994D5E40F2B1FE8100483F40867B3C5F994D5E40228C7A2400483F4035910960994D5E404CC2C3C4FF473F40BF1CA360994D5E400FF9576DFF473F40B5971E61994D5E40F26EB318FF473F4015027C61994D5E40A72ECCCAFE473F40577FDA61994D5E40AC188269FE473F4042C61862994D5E4088087A0EFE473F403A353C62994D5E4006D5B4BAFD473F40C3174962994D5E4005E1F969FD473F4060B94362994D5E403742671BFD473F4092653062994D5E403416E7CDFC473F4036980762994D5E40E4164974FC473F40B546D761994D5E409F6D6A26FC473F40FF4EE661994D5E40E10208D3FB473F40B7E93762994D5E40D8839C82FB473F40EC3EB162994D5E40B52BFC32FB473F4076CA4A63994D5E4003F657E3FA473F40379F0564994D5E4033317793FA473F406D9D0B65994D5E409EAD2B39FA473F4070EF2466994D5E406A25D6E5F9473F4058F17367994D5E40D851158EF9473F4067719E68994D5E40EDA70542F9473F40516DC169994D5E40ED7C57F6F8473F4009B7CB6A994D5E40B2B5EDA5F8473F40BD63A16B994D5E403205B84FF8473F400284606C994D5E401E9B28F7F7473F4066911E6D994D5E40375FA19DF7473F40852DD66D994D5E4073C59F41F7473F40A4C98D6E994D5E4027F719E1F6473F40B737346F994D5E40FB036D83F6473F4023D6CE6F994D5E4003E97725F6473F402A166470994D5E401F5A49C2F5473F40C860EB70994D5E409265295FF5473F4009DE4971994D5E40117F3013F5473F4045C49F71994D5E40619528C6F4473F401D4CF071994D5E40AB6BDC76F4473F404C043572994D5E40E1825129F4473F4057387272994D5E40A9CF60DBF3473F401DFBA872994D5E40C18E8C88F3473F401B01D572994D5E4067C15037F3473F40524AF672994D5E40F77923EAF2473F40640F1073994D5E405BCB9A98F2473F40CE041E73994D5E40919DC248F2473F40703D2173994D5E40400011E8F1473F40A9801673994D5E409FE4488FF1473F40FA190273994D5E401453CE40F1473F40A4E3E172994D5E407313BAEDF0473F409E4CD972994D5E4066C822DDF0473F4039F4FF72994D5E40D979F389F0473F4043283D73994D5E4040881F3DF0473F4066B89C73994D5E409201D6EAEF473F402A81FF73994D5E4092DCA49DEF473F4034B53C74994D5E40DA2FC950EF473F402F1E3474994D5E4022631C01EF473F40A19EF273994D5E4060C7A1B4EE473F40FFB63673994D5E40FBD87E65EE473F409469E371994D5E408BA4F818EE473F4097FF1970994D5E400526C7C7ED473F408F61136E994D5E400BE3687BED473F40BF5D756B994D5E4097564C2FED473F40AC33EC67994D5E40EE16F4E2EC473F401DF1C962994D5E40DEA9BB94EC473F40705D0B5C994D5E40E1783247EC473F40C65F8353994D5E40EDF9D7FFEB473F4069A8FB47994D5E40C3D46BBAEB473F40A6DCB739994D5E40E6092876EB473F40975FD82A994D5E40C8F52D3AEB473F407EAEBB1B994D5E4086200D03EB473F40CC3CF609994D5E40176DFBC6EA473F40A11707FA984D5E403C674990EA473F408BABD9E9984D5E400FA95B5AEA473F40E179A9D8984D5E40A5F81D27EA473F40878F4BC6984D5E405C7602FDE9473F4090A581B2984D5E40FE487FDAE9473F4027D75B9D984D5E40CDE19ABFE9473F4082280C88984D5E407A7572ACE9473F40A5D85571984D5E40299588A1E9473F40A3FE6B5A984D5E404F00BDA0E9473F408725FF42984D5E4024C0DFA2E9473F407E0B802B984D5E406718C2A3E9473F40545B7513984D5E4094D5B1A0E9473F406C6CE6F8974D5E403A5E1D99E9473F4046BDADE2974D5E40DE697692E9473F40157CC6CA974D5E403C8D338BE9473F4066A037B7974D5E40E6423C85E9473F4053A3C29E974D5E40BE337780E9473F40F23EFB82974D5E4007AB9C7BE9473F406BAD9F66974D5E4087E8A477E9473F40E1850D4C974D5E40D993B674E9473F407665CE35974D5E40C6F99572E9473F40CD06F41D974D5E40764D5470E9473F40B31A3104974D5E40C7A71E6EE9473F40F3A351E9964D5E40F1BB306CE9473F40021DE8CC964D5E401840896AE9473F405E850BAE964D5E40EBCC3369E9473F407388808F964D5E407CC35B68E9473F40D4DE1F70964D5E400367F667E9473F401B301050964D5E40281DF567E9473F403EE05939964D5E40EE112368E9473F40056C0723964D5E401ECA6768E9473F40145D630C964D5E40610FA368E9473F4061CA4BF4954D5E40C4D7C268E9473F40AA9AFFDB954D5E403396D068E9473F40AC6376C5954D5E408499D668E9473F40B071DCAD954D5E407807D768E9473F40D70FA296954D5E40014ED268E9473F40B447E77D954D5E40FBB6C968E9473F40EB013A66954D5E40928FC468E9473F401135644B954D5E40F61FC168E9473F40F1605132954D5E406642BD68E9473F409F2B6D18954D5E4028CCAD68E9473F402F54A5FD944D5E40C6677C68E9473F4019565EE3944D5E4040791768E9473F4013C5EBC7944D5E4078A96567E9473F40D893E4AC944D5E4010636166E9473F40D6FD1793944D5E4079641865E9473F40613E0079944D5E40B3117963E9473F405CA7615D944D5E40FDA86F61E9473F40E444E942944D5E40DEE03D5FE9473F400826FA26944D5E40BB11BD5CE9473F4074C4FE0B944D5E40BF5C295AE9473F40BDBE7BEC934D5E40CD5E1157E9473F40D27DD3D0934D5E40C57B6C54E9473F40D23222B6934D5E400B0C1452E9473F406C447C9C934D5E40282A3850E9473F403608EF82934D5E40AB17CB4EE9473F407847C66A934D5E409ACFC34DE9473F405A233E54934D5E40A0B7134DE9473F408C9ECD3D934D5E40D657A84CE9473F40D79F4820934D5E404842814CE9473F40C8832505934D5E40C266C34CE9473F40A90758ED924D5E40F08E4E4DE9473F40228326D3924D5E40BCC93D4EE9473F40EFA5DCBC924D5E40A823234FE9473F40105C52A6924D5E40B6E1ED4EE9473F409C52F18E924D5E40B33E8D4EE9473F40EE5C0E77924D5E40675BFA4DE9473F4005100E5B924D5E4068D80C4DE9473F40FBD43C3D924D5E4042FCBF4BE9473F4087F5A41E924D5E40FF58134AE9473F4020E080FE914D5E4005E3F147E9473F40B73426E0914D5E408A4D9745E9473F4040D9ABC7914D5E4000FA7143E9473F40B396F3A6914D5E40930F3B40E9473F4016C49B90914D5E40E695D03DE9473F40929EAD79914D5E405B67273BE9473F402D084D61914D5E4068A13238E9473F40BF378348914D5E4012AF2F35E9473F400EFDB031914D5E402F828932E9473F40C20D8E18914D5E403A90C92FE9473F409DFAB700914D5E4037E03E2DE9473F40187758E8904D5E403430B42AE9473F40B625E7CE904D5E408F791D28E9473F40B4ED8BB6904D5E40FC23B225E9473F4094C8319E904D5E408D205723E9473F40759DAB85904D5E40B4910821E9473F40812FC46B904D5E406F13D81EE9473F409B924E52904D5E401BAB111DE9473F40AC49D632904D5E40E499641BE9473F40D40B3914904D5E408BAD3C1AE9473F405612B8F98F4D5E40329C9819E9473F400ECDF3E28F4D5E40CB044D19E9473F406817A6CB8F4D5E4093254619E9473F40884296C88F4D5E4041BE5119E9473F40BEF5EAAE8F4D5E403A9D0C18E9473F40ABA72E978F4D5E40B52BBA16E9473F408529BD7B8F4D5E40F0A0F714E9473F40
7	3	0102000020E6100000370000006007F9998D4D5E409080F1CE11483F406DD16C988D4D5E4099008D2412483F409AD926968D4D5E4030AC288712483F40A7F4E1938D4D5E40F66EC3D612483F403E43F1908D4D5E404A7B2D2F13483F4012BD458D8D4D5E407DC0958A13483F40CF2BBF888D4D5E40C386FAE813483F4026EA19838D4D5E4063CA704D14483F401D89327C8D4D5E40D5E68BB614483F40E4C04D748D4D5E409B96661F15483F406F76B06D8D4D5E40D6FF5E6E15483F40F9C077638D4D5E4094B572DC15483F4024A8DF5A8D4D5E40F371AB3016483F400D1815528D4D5E40E4814D8016483F403CEDF8488D4D5E40DA0827CD16483F4024AE753F8D4D5E406B2AFD1717483F40DD07F5348D4D5E40E334FE6417483F4042B460268D4D5E406E9FE1C617483F40270970178D4D5E40F9CF9F2118483F404844D5088D4D5E409A028A7218483F407304BFF98C4D5E40631091BF18483F405CFC2EEB8C4D5E4033E5E60319483F408B9D6AD98C4D5E4060D4315019483F40F8CD88C88C4D5E40BC21CB9219483F40417473B78C4D5E40F7D12BD119483F407BEB7AA48C4D5E4082B738111A483F403B51E7918C4D5E405D79CD4A1A483F4054E11B7F8C4D5E409DC4B1801A483F405C4EC56A8C4D5E40CBE68DB61A483F40BDD80C548C4D5E407B3B4AED1A483F40D855EB3E8C4D5E404CBAC61B1B483F409FD46E268C4D5E4071ECD34C1B483F40E458350D8C4D5E404A1B7D7A1B483F40585123F78B4D5E4090312B9F1B483F4004DE4DE08B4D5E4003C728C21B483F401EA575C88B4D5E40509380E41B483F40157ECCAE8B4D5E40CDC6D5071C483F409C6C9B938B4D5E405C0A0F2C1C483F40836D82778B4D5E40130B96501C483F40CFC6425B8B4D5E409DE391741C483F40FC834B3E8B4D5E40AB6B80981C483F405D4586228B4D5E40B129EEB81C483F40086D41088B4D5E4064D659D51C483F403049F8ED8A4D5E40548E39EE1C483F408C8752D68A4D5E40D1593E001D483F40D2C1CFBF8A4D5E409EFB4D0D1D483F40CB87D0A38A4D5E40606222181D483F4014A9CB8A8A4D5E40BB307B1D1D483F401866EA738A4D5E4035FEF81F1D483F40448790588A4D5E409AFF0D221D483F4035D608418A4D5E40B33037241D483F40B97FE8268A4D5E40E291B7261D483F4059E42D108A4D5E40C40FA5281D483F40D014E1F6894D5E40FF40C5291D483F40EEC984DE894D5E40AF034F701D483F40	1	12	1	32.942781228634985	32.942781228634985	0105000020E6100000010000000102000000370000006007F9998D4D5E409080F1CE11483F406DD16C988D4D5E4099008D2412483F409AD926968D4D5E4030AC288712483F40A7F4E1938D4D5E40F66EC3D612483F403E43F1908D4D5E404A7B2D2F13483F4012BD458D8D4D5E407DC0958A13483F40CF2BBF888D4D5E40C386FAE813483F4026EA19838D4D5E4063CA704D14483F401D89327C8D4D5E40D5E68BB614483F40E4C04D748D4D5E409B96661F15483F406F76B06D8D4D5E40D6FF5E6E15483F40F9C077638D4D5E4094B572DC15483F4024A8DF5A8D4D5E40F371AB3016483F400D1815528D4D5E40E4814D8016483F403CEDF8488D4D5E40DA0827CD16483F4024AE753F8D4D5E406B2AFD1717483F40DD07F5348D4D5E40E334FE6417483F4042B460268D4D5E406E9FE1C617483F40270970178D4D5E40F9CF9F2118483F404844D5088D4D5E409A028A7218483F407304BFF98C4D5E40631091BF18483F405CFC2EEB8C4D5E4033E5E60319483F408B9D6AD98C4D5E4060D4315019483F40F8CD88C88C4D5E40BC21CB9219483F40417473B78C4D5E40F7D12BD119483F407BEB7AA48C4D5E4082B738111A483F403B51E7918C4D5E405D79CD4A1A483F4054E11B7F8C4D5E409DC4B1801A483F405C4EC56A8C4D5E40CBE68DB61A483F40BDD80C548C4D5E407B3B4AED1A483F40D855EB3E8C4D5E404CBAC61B1B483F409FD46E268C4D5E4071ECD34C1B483F40E458350D8C4D5E404A1B7D7A1B483F40585123F78B4D5E4090312B9F1B483F4004DE4DE08B4D5E4003C728C21B483F401EA575C88B4D5E40509380E41B483F40157ECCAE8B4D5E40CDC6D5071C483F409C6C9B938B4D5E405C0A0F2C1C483F40836D82778B4D5E40130B96501C483F40CFC6425B8B4D5E409DE391741C483F40FC834B3E8B4D5E40AB6B80981C483F405D4586228B4D5E40B129EEB81C483F40086D41088B4D5E4064D659D51C483F403049F8ED8A4D5E40548E39EE1C483F408C8752D68A4D5E40D1593E001D483F40D2C1CFBF8A4D5E409EFB4D0D1D483F40CB87D0A38A4D5E40606222181D483F4014A9CB8A8A4D5E40BB307B1D1D483F401866EA738A4D5E4035FEF81F1D483F40448790588A4D5E409AFF0D221D483F4035D608418A4D5E40B33037241D483F40B97FE8268A4D5E40E291B7261D483F4059E42D108A4D5E40C40FA5281D483F40D014E1F6894D5E40FF40C5291D483F40EEC984DE894D5E40AF034F701D483F40
17	10	0102000020E610000068010000A089FC6F8F4D5E4087269B76E2473F40E057388B8F4D5E4099F8DE78E2473F4091276F9E8F4D5E402AABE97AE2473F40FCB249B88F4D5E4062AAC17DE2473F404B9614D18F4D5E407363E180E2473F4085C01DEA8F4D5E402FC16484E2473F40FE2FC006904D5E40095DDD88E2473F40E9E82D28904D5E403AB9B28EE2473F4058628441904D5E4055D28993E2473F4075481B5B904D5E401CF4B298E2473F40D85C6D79904D5E40DF6CFE9EE2473F4032EC3A98904D5E4029389DA5E2473F403DDDC2B8904D5E40EA33D6ACE2473F4092DC85D9904D5E4072243DB4E2473F4030A666FD904D5E4051C259BCE2473F40F49CDB21914D5E40CD3368C4E2473F40307A504B914D5E40DF9018CDE2473F405C865374914D5E4094A90AD5E2473F402474BB8A914D5E403F551BD9E2473F40746863B5914D5E406C4036E0E2473F405EB916E1914D5E4002A49DE6E2473F402951DDF9914D5E4024BEE8E9E2473F4049CDAA11924D5E40AD5CD8ECE2473F40D7D58E29924D5E404BB489EFE2473F40E0EF0D41924D5E40EB9BF4F1E2473F400D437959924D5E40030541F4E2473F409A5EB373924D5E40216984F6E2473F406E360F8D924D5E40425D81F8E2473F4011B497A7924D5E40764263FAE2473F400573C6C0924D5E406946F8FBE2473F4044F6E6DC924D5E40236B86FDE2473F405DE8D5F6924D5E403187C2FEE2473F4027E43911934D5E402847DCFFE2473F40F912092A934D5E405EA9D000E3473F4063F55643934D5E403D71B601E3473F40E4840E5C934D5E408EBF8602E3473F40BBA15974934D5E4081DC3F03E3473F40009C028C934D5E40957CDD03E3473F4049AE86B7934D5E40BE4CD204E3473F404F80CBDD934D5E408BEBAF05E3473F4089F8D003944D5E40E703A306E3473F40EB639621944D5E40C72F7707E3473F401040793E944D5E4067B97008E3473F40A4944A57944D5E404CB47009E3473F409C687376944D5E40E186EE0AE3473F4022441890944D5E40D452600CE3473F4047B55FA9944D5E40695D010EE3473F401EC580BF944D5E40423D970FE3473F406361B8D5944D5E401C811B11E3473F40C6A5FFEC944D5E408657D911E3473F4050C06705954D5E4073799B12E3473F4051B4A520954D5E4022893C13E3473F406EEB8339954D5E407D2A9A13E3473F40F7256C56954D5E40431FC813E3473F40B7B69F75954D5E4005A9B813E3473F405C043397954D5E40A4A87513E3473F407CAE7CB7954D5E4037DE0F13E3473F40CA0F39D6954D5E407FA7AC12E3473F40A140ACF2954D5E40E0944812E3473F408A2F3B0D964D5E4022C7DC11E3473F408ABE0925964D5E408F467811E3473F40D154153B964D5E408C3F2911E3473F40E6B6F956964D5E400CE8CC10E3473F407CFF726F964D5E40CA068010E3473F4024AF9486964D5E407ACB5610E3473F407F1EE2A0964D5E4027389710E3473F400B918FBA964D5E409FCC1F11E3473F40E700C6D5964D5E409285EA11E3473F4043FF4BEC964D5E4078ACB512E3473F402B32F803974D5E409CE5A113E3473F40859A471C974D5E403510B614E3473F4025242837974D5E408DD01216E3473F4037E3AB52974D5E400F169A17E3473F408D71A76F974D5E40137B5A19E3473F402476038B974D5E409494161BE3473F40188679A3974D5E40736FA31CE3473F4021B06DAF974D5E409F6B631DE3473F4060717FC8974D5E40F4E6D01FE3473F40FB562DE1974D5E40BD8B8922E3473F40378834FC974D5E4014913326E3473F4017372E16984D5E40BA1CD12AE3473F407DDAB830984D5E40A2E72C31E3473F40B5AC7C48984D5E40EDC57238E3473F404BE8CB5E984D5E40A4B9E840E3473F405460BC77984D5E40BAD9CA4CE3473F40F032148E984D5E400ADA855AE3473F4007C6BFA4984D5E40C1B05C6CE3473F4057E77BBA984D5E4017567A81E3473F4082D577CF984D5E406D1B6999E3473F40A1F3D3E5984D5E402ADDC1B6E3473F40CD3217FA984D5E40F84A10D5E3473F4045D9AC11994D5E4091FB2AFDE3473F408299EC28994D5E40FCB0B529E4473F40AA93403E994D5E40146F9256E4473F4045BD0B54994D5E402891BB86E4473F40E6DB506B994D5E400FF077BBE4473F40A4D36C7E994D5E402B7056E6E4473F40ABD9C391994D5E40C14C9A11E5473F407AED6CA4994D5E40021F893CE5473F40680869BB994D5E40DA176F74E5473F40091A84D0994D5E40652DC2ABE5473F4083F1E0E2994D5E404E1C38E0E5473F4043E3D0F5994D5E407E97DA1AE6473F400A082C079A4D5E40D9EE6B53E6473F40052ABB179A4D5E40B3D57C8CE6473F407C0046289A4D5E40D1FC1DC9E6473F407D626A399A4D5E40171A600BE7473F409C08014A9A4D5E404D0F6C4FE7473F406A0882589A4D5E4052575B8EE7473F403120B3679A4D5E40A132C6D3E7473F40ADE4B9779A4D5E402797A320E8473F4093DCBF849A4D5E4086292C62E8473F4096CD1C929A4D5E40CC15F8A7E8473F40108B25A09A4D5E40D642ECF3E8473F4093D3DEAD9A4D5E40FFC07C41E9473F407EC68ABC9A4D5E4019C89198E9473F4020ABFBCA9A4D5E4035021FF3E9473F4081120ED99A4D5E407D583D51EA473F40FE1406E69A4D5E4080DFE3ADEA473F40BB3C17F29A4D5E407595180AEB473F40B43D54FC9A4D5E403C9ED25DEB473F4043EB11059B4D5E400CF817AAEB473F40FEFEFA0C9B4D5E40AF8096F3EB473F4003D0D7159B4D5E408B0B1F4CEC473F406216981D9B4D5E40F94DFF9FEC473F40E682EE239B4D5E400DA832EAEC473F408ADC432A9B4D5E40F52C243AED473F40A64D60309B4D5E4024825F8DED473F40D7773E369B4D5E40E67153E5ED473F4017151D3B9B4D5E4004A40337EE473F40E8762C3F9B4D5E401E579F82EE473F40538062439B4D5E40CAC5F4DAEE473F40F8C661479B4D5E40B4F1A83CEF473F407149C44A9B4D5E40DB74CBA0EF473F40D951284D9B4D5E40612D22F9EF473F4076FA204F9B4D5E400C0EC058F0473F40E8DE7C509B4D5E4084491EBCF0473F40ABAD0B519B4D5E406C774B0EF1473F40E2F62C519B4D5E407941235CF1473F409D8526519B4D5E40C185B76DF1473F406054B5519B4D5E40402AF3C6F1473F4063F715529B4D5E40EDF5891BF2473F4016FB5E529B4D5E407918EE6EF2473F40848DA1529B4D5E40D60382D2F2473F409652BB529B4D5E40AA7D3A1EF3473F40388BBE529B4D5E409CB32A6FF3473F40E8EBA6529B4D5E4029B242C8F3473F40994663529B4D5E40667EE02BF4473F40A962F0519B4D5E40DDBA6E94F4473F402EF90F519B4D5E4087E4A221F5473F40502BFE4F9B4D5E401DEDC49DF5473F403677964E9B4D5E40F6CB0B1FF6473F40B6C1C84C9B4D5E408653FEA7F6473F40CDC5A54B9B4D5E4033EBECF3F6473F40141E2A499B4D5E40337FE989F7473F403944C0479B4D5E40602A17D9F7473F401AF94F469B4D5E40A79BD226F8473F40D629D8449B4D5E400DE1B973F8473F40F8AC0D439B4D5E40FD7CF8CEF8473F40905362419B4D5E400ED98B23F9473F40B2D6973F9B4D5E406D6BFB7EF9473F40F24CF83D9B4D5E405086DFD3F9473F406B064E3C9B4D5E408E1ED52EFA473F407239B93A9B4D5E407699CE88FA473F403B9226399B4D5E404E31DFE6FA473F40FC59B7379B4D5E402A270343FB473F404D9B5D369B4D5E4088AD78A1FB473F40A90A15359B4D5E4058381B04FC473F407506E3339B4D5E40E9535B6AFC473F40459FE5329B4D5E402F9155C9FC473F40C49EFC319B4D5E409658E12BFD473F407AE734319B4D5E4076824F8DFD473F40FD8380309B4D5E4045D081F6FD473F409D13F72F9B4D5E4077BF5559FE473F408E42852F9B4D5E40EA45DCC0FE473F40B7BA342F9B4D5E403EF70624FF473F402FD2FB2E9B4D5E407E4DE188FF473F40F888DA2E9B4D5E4034BB31EFFF473F408E93CC2E9B4D5E4076198B5900483F400B48C82E9B4D5E405643F9BA00483F40E6C3C02E9B4D5E40A05D792101483F40BBA8B02E9B4D5E40AD58AE8501483F4007AB932E9B4D5E40A25F2AE101483F40666C642E9B4D5E40DED87E3E02483F403D4B282E9B4D5E40DA51879A02483F400D93E32D9B4D5E406F1F6DF002483F4054F8912D9B4D5E40BA86D24403483F4077D9382D9B4D5E40236F1A9203483F40079E692C9B4D5E4066B6381E04483F4066B0812B9B4D5E40FB60E99804483F40175C852A9B4D5E40DCDED00505483F40E8F487299B4D5E4091509C6305483F40228398289B4D5E400DD304B305483F40D62244279B4D5E4029ACD51906483F40517FFA259B4D5E4098D7937506483F4008C206259B4D5E4026FF44C206483F40C83E7C249B4D5E404618731107483F406F65FB239B4D5E402E59476407483F40AB9C98239B4D5E403F7BB5B107483F40A8F937239B4D5E40606D4C1408483F401A7AF6229B4D5E405875818B08483F40EF5EE6229B4D5E40CC68BEE308483F40972EF2229B4D5E40BCEBD83F09483F4077471F239B4D5E40109D03A309483F402FE270239B4D5E402068940C0A483F40C795EF239B4D5E401DE183820A483F4077A590249B4D5E40C4BE34F30A483F40A26F59259B4D5E40FA0F7E650B483F40237042269B4D5E401862E6D30B483F40D62244279B4D5E407C0B013E0C483F409A9A5F289B4D5E40B6AC03A70C483F409DEC78299B4D5E40CD89AD080D483F40AE66772A9B4D5E40099EE3600D483F4068AA552B9B4D5E40D5D33AB60D483F409A05FB2B9B4D5E40FEAF3C050E483F407BC7B42C9B4D5E40DABC69730E483F4057EC392D9B4D5E40841C7ADC0E483F40D1AC8D2D9B4D5E4029E8CC370F483F403411BF2D9B4D5E40FAA1858C0F483F40C28AD42D9B4D5E404F97CDEC0F483F40976FC42D9B4D5E40B1090E4210483F4010878B2D9B4D5E404E661F8F10483F40BD44132D9B4D5E404F742EE410483F40AF6D752C9B4D5E40D43D2A3611483F40C183D62B9B4D5E40727C6C8711483F4066B0812B9B4D5E40ED7381B611483F403AECE42A9B4D5E401AFFDD0212483F404765112A9B4D5E4097B4BE5512483F4075C510299B4D5E40B9B6CAAC12483F4040C70A289B4D5E40D745BCFA12483F40052CD0269B4D5E40832F224B13483F40775449259B4D5E409F1BB39813483F40AF8A14239B4D5E40627512EF13483F40947E67209B4D5E4023328E4314483F40D78AFE1C9B4D5E407C13A19A14483F40B29B5B199B4D5E4029FCD6E514483F405D45BB149B4D5E4099F9693715483F403BEC4E0F9B4D5E404373E78A15483F40AE458F089B4D5E40665DA1E415483F40438076019B4D5E403416883716483F40347B5CF89A4D5E4050BBB09516483F40E64AFDEF9A4D5E404C75A4E216483F40BA52B8E69A4D5E402EBA513017483F40692187DC9A4D5E405F59277F17483F40C6FE24D19A4D5E402E188BD017483F4003E299C39A4D5E4030D0A72918483F4053D543B59A4D5E409A9B508018483F40324203A79A4D5E40D080E7CF18483F405FAB79989A4D5E4015818A1B19483F406C7838899A4D5E40240C926519483F40C4FCBE7A9A4D5E40883523A719483F40D278366C9A4D5E40FC79C0E419483F406A22D65C9A4D5E4015B911221A483F4086114E4D9A4D5E40B8E6695C1A483F400113DE3C9A4D5E40C76792961A483F404716682B9A4D5E40460BB7D01A483F40562118199A4D5E403D81040A1B483F40021F0A069A4D5E403E3738421B483F4021978EF2994D5E40C7C24E781B483F401BCF28DC994D5E403F078EB21B483F40EB943FC6994D5E404BCB01E81B483F405BE3CEAD994D5E40BAC8AD1F1C483F409ECB3296994D5E40DD19BA511C483F400CC27C7C994D5E40F6B34A841C483F404FE1D35F994D5E404EB215B81C483F40E618DA4A994D5E4057BC1FDB1C483F406669A22D994D5E4040D55E081D483F403E3BA60F994D5E406644DC321D483F40DDFD5CFA984D5E408E33B74E1D483F400BCF8DE1984D5E40BAFEE76C1D483F4082A1CFC6984D5E40AB7FD48A1D483F407ED33DB0984D5E400E8CF8A11D483F4068409299984D5E4062054BB71D483F400D8D2782984D5E4038A453CB1D483F402EE52B6A984D5E403B5EBDDD1D483F4060A4D850984D5E404351BEEE1D483F4069E4D735984D5E40F6AF2DFE1D483F404AAC271B984D5E401216D10A1E483F40B20B6901984D5E40212E92141E483F4069F699E3974D5E407C54DF1C1E483F40206905C0974D5E4099CAC5511E483F40372F5BA6974D5E40745E614D1E483F4049C31790974D5E40652254491E483F40BA93B571974D5E40219D76431E483F405CB8FA51974D5E40B08B1F3D1E483F40DC735E38974D5E40469B0D381E483F4042C5A31A974D5E40BF7FAD321E483F40E1B123FE964D5E409B77372E1E483F40F3A69CDB964D5E4091BF8B291E483F40672A10B2964D5E4029460F251E483F40C3516188964D5E40080748221E483F40760D4460964D5E40936667211E483F40DE557C34964D5E40C02A04221E483F40BF2AF61B964D5E4068DBBF221E483F40E3255B04964D5E404E9E9C231E483F40EFC49DEC954D5E406B78A3241E483F400B15D2D0954D5E40F41C10261E483F40EC2D69B5954D5E40FAAD9B271E483F40F3152399954D5E40C75F20291E483F40901B257F954D5E408B734D2A1E483F40E5A12461954D5E40FF4B512B1E483F40295DDE42954D5E40638B062C1E483F40132BEF1F954D5E40725C782C1E483F40D7DF9300954D5E40F5A77C2C1E483F40509F7FE3944D5E40D2E5252C1E483F4049B6C7C6944D5E40A08A802B1E483F4055EA6EAB944D5E40632D952A1E483F40F32D628E944D5E40E5B639291E483F406ED4CB6E944D5E40AB3A4F271E483F4070185350944D5E40D84209251E483F405C018A33944D5E405E758B221E483F402114A015944D5E40ED96AC1F1E483F409A17A9F5934D5E401B1C791C1E483F40CC279DDB934D5E403953C1191E483F40C89D28C2934D5E409C5FFE161E483F40E9F52CAA934D5E400B9A4C141E483F407637E791934D5E40170C7B111E483F40246D9E7B934D5E4053FECA0E1E483F40EAA00664934D5E4054A6D60B1E483F40E075AA3A934D5E40B56677061E483F40BB14D815934D5E40872498011E483F40AC2FA8F5924D5E408DA14CFD1D483F408760EFDA924D5E401CB0C6F91D483F4099C003BC924D5E40C89EC4F51D483F403F4B8AA1924D5E40FEE664F21D483F407CFA568B924D5E40DEA79DEF1D483F40C234D474924D5E4041B4DAEC1D483F40CDD9425D924D5E405FEB22EA1D483F40A62A0A47924D5E400B9C80E71D483F40060CC52F924D5E408135B4E41D483F40B140AA17924D5E401DE9D4E11D483F4041189BFD914D5E40C15FC9DE1D483F40D63B79E4914D5E406AA5E9DB1D483F4009FB25CB914D5E40DAA714D91D483F40C395E1AF914D5E4031EA2ED61D483F40AE9E9897914D5E406551CED31D483F4034A0BD7E914D5E40D03386D11D483F4015C67E65914D5E40010B6CCF1D483F4068BD454B914D5E40F33F77CD1D483F40210DE630914D5E406D8FB2CB1D483F4064DBF514914D5E40A0A50ACA1D483F40EE5FFBF7904D5E40C0668FC81D483F40F12246E1904D5E40B6B590C71D483F40E44414C3904D5E40BFF576C61D483F40C577FFAB904D5E401DDCC3C51D483F408565A693904D5E40B23D29C51D483F40527B327B904D5E40E841ACC41D483F407FF41D61904D5E40ADBF44C41D483F40DDC1B045904D5E40962BFFC31D483F401317942A904D5E40DC64E2C31D483F40BC37510E904D5E407E6BEEC31D483F405CC65FF08F4D5E40B41E2AC41D483F409AF73AD48F4D5E409E9D8BC41D483F4054E33DB88F4D5E40D388EAC41D483F404411649A8F4D5E40FDA926C51D483F406B37647D8F4D5E4033C150C51D483F40CE9BFF618F4D5E40A31B70C51D483F402F26474B8F4D5E40329585C51D483F406B2C87348F4D5E40EC5BA2C51D483F4066E099338F4D5E40C6A5A3C51D483F40	9	7	1	220.69216946926966	220.69216946926966	0105000020E610000001000000010200000068010000A089FC6F8F4D5E4087269B76E2473F40E057388B8F4D5E4099F8DE78E2473F4091276F9E8F4D5E402AABE97AE2473F40FCB249B88F4D5E4062AAC17DE2473F404B9614D18F4D5E407363E180E2473F4085C01DEA8F4D5E402FC16484E2473F40FE2FC006904D5E40095DDD88E2473F40E9E82D28904D5E403AB9B28EE2473F4058628441904D5E4055D28993E2473F4075481B5B904D5E401CF4B298E2473F40D85C6D79904D5E40DF6CFE9EE2473F4032EC3A98904D5E4029389DA5E2473F403DDDC2B8904D5E40EA33D6ACE2473F4092DC85D9904D5E4072243DB4E2473F4030A666FD904D5E4051C259BCE2473F40F49CDB21914D5E40CD3368C4E2473F40307A504B914D5E40DF9018CDE2473F405C865374914D5E4094A90AD5E2473F402474BB8A914D5E403F551BD9E2473F40746863B5914D5E406C4036E0E2473F405EB916E1914D5E4002A49DE6E2473F402951DDF9914D5E4024BEE8E9E2473F4049CDAA11924D5E40AD5CD8ECE2473F40D7D58E29924D5E404BB489EFE2473F40E0EF0D41924D5E40EB9BF4F1E2473F400D437959924D5E40030541F4E2473F409A5EB373924D5E40216984F6E2473F406E360F8D924D5E40425D81F8E2473F4011B497A7924D5E40764263FAE2473F400573C6C0924D5E406946F8FBE2473F4044F6E6DC924D5E40236B86FDE2473F405DE8D5F6924D5E403187C2FEE2473F4027E43911934D5E402847DCFFE2473F40F912092A934D5E405EA9D000E3473F4063F55643934D5E403D71B601E3473F40E4840E5C934D5E408EBF8602E3473F40BBA15974934D5E4081DC3F03E3473F40009C028C934D5E40957CDD03E3473F4049AE86B7934D5E40BE4CD204E3473F404F80CBDD934D5E408BEBAF05E3473F4089F8D003944D5E40E703A306E3473F40EB639621944D5E40C72F7707E3473F401040793E944D5E4067B97008E3473F40A4944A57944D5E404CB47009E3473F409C687376944D5E40E186EE0AE3473F4022441890944D5E40D452600CE3473F4047B55FA9944D5E40695D010EE3473F401EC580BF944D5E40423D970FE3473F406361B8D5944D5E401C811B11E3473F40C6A5FFEC944D5E408657D911E3473F4050C06705954D5E4073799B12E3473F4051B4A520954D5E4022893C13E3473F406EEB8339954D5E407D2A9A13E3473F40F7256C56954D5E40431FC813E3473F40B7B69F75954D5E4005A9B813E3473F405C043397954D5E40A4A87513E3473F407CAE7CB7954D5E4037DE0F13E3473F40CA0F39D6954D5E407FA7AC12E3473F40A140ACF2954D5E40E0944812E3473F408A2F3B0D964D5E4022C7DC11E3473F408ABE0925964D5E408F467811E3473F40D154153B964D5E408C3F2911E3473F40E6B6F956964D5E400CE8CC10E3473F407CFF726F964D5E40CA068010E3473F4024AF9486964D5E407ACB5610E3473F407F1EE2A0964D5E4027389710E3473F400B918FBA964D5E409FCC1F11E3473F40E700C6D5964D5E409285EA11E3473F4043FF4BEC964D5E4078ACB512E3473F402B32F803974D5E409CE5A113E3473F40859A471C974D5E403510B614E3473F4025242837974D5E408DD01216E3473F4037E3AB52974D5E400F169A17E3473F408D71A76F974D5E40137B5A19E3473F402476038B974D5E409494161BE3473F40188679A3974D5E40736FA31CE3473F4021B06DAF974D5E409F6B631DE3473F4060717FC8974D5E40F4E6D01FE3473F40FB562DE1974D5E40BD8B8922E3473F40378834FC974D5E4014913326E3473F4017372E16984D5E40BA1CD12AE3473F407DDAB830984D5E40A2E72C31E3473F40B5AC7C48984D5E40EDC57238E3473F404BE8CB5E984D5E40A4B9E840E3473F405460BC77984D5E40BAD9CA4CE3473F40F032148E984D5E400ADA855AE3473F4007C6BFA4984D5E40C1B05C6CE3473F4057E77BBA984D5E4017567A81E3473F4082D577CF984D5E406D1B6999E3473F40A1F3D3E5984D5E402ADDC1B6E3473F40CD3217FA984D5E40F84A10D5E3473F4045D9AC11994D5E4091FB2AFDE3473F408299EC28994D5E40FCB0B529E4473F40AA93403E994D5E40146F9256E4473F4045BD0B54994D5E402891BB86E4473F40E6DB506B994D5E400FF077BBE4473F40A4D36C7E994D5E402B7056E6E4473F40ABD9C391994D5E40C14C9A11E5473F407AED6CA4994D5E40021F893CE5473F40680869BB994D5E40DA176F74E5473F40091A84D0994D5E40652DC2ABE5473F4083F1E0E2994D5E404E1C38E0E5473F4043E3D0F5994D5E407E97DA1AE6473F400A082C079A4D5E40D9EE6B53E6473F40052ABB179A4D5E40B3D57C8CE6473F407C0046289A4D5E40D1FC1DC9E6473F407D626A399A4D5E40171A600BE7473F409C08014A9A4D5E404D0F6C4FE7473F406A0882589A4D5E4052575B8EE7473F403120B3679A4D5E40A132C6D3E7473F40ADE4B9779A4D5E402797A320E8473F4093DCBF849A4D5E4086292C62E8473F4096CD1C929A4D5E40CC15F8A7E8473F40108B25A09A4D5E40D642ECF3E8473F4093D3DEAD9A4D5E40FFC07C41E9473F407EC68ABC9A4D5E4019C89198E9473F4020ABFBCA9A4D5E4035021FF3E9473F4081120ED99A4D5E407D583D51EA473F40FE1406E69A4D5E4080DFE3ADEA473F40BB3C17F29A4D5E407595180AEB473F40B43D54FC9A4D5E403C9ED25DEB473F4043EB11059B4D5E400CF817AAEB473F40FEFEFA0C9B4D5E40AF8096F3EB473F4003D0D7159B4D5E408B0B1F4CEC473F406216981D9B4D5E40F94DFF9FEC473F40E682EE239B4D5E400DA832EAEC473F408ADC432A9B4D5E40F52C243AED473F40A64D60309B4D5E4024825F8DED473F40D7773E369B4D5E40E67153E5ED473F4017151D3B9B4D5E4004A40337EE473F40E8762C3F9B4D5E401E579F82EE473F40538062439B4D5E40CAC5F4DAEE473F40F8C661479B4D5E40B4F1A83CEF473F407149C44A9B4D5E40DB74CBA0EF473F40D951284D9B4D5E40612D22F9EF473F4076FA204F9B4D5E400C0EC058F0473F40E8DE7C509B4D5E4084491EBCF0473F40ABAD0B519B4D5E406C774B0EF1473F40E2F62C519B4D5E407941235CF1473F409D8526519B4D5E40C185B76DF1473F406054B5519B4D5E40402AF3C6F1473F4063F715529B4D5E40EDF5891BF2473F4016FB5E529B4D5E407918EE6EF2473F40848DA1529B4D5E40D60382D2F2473F409652BB529B4D5E40AA7D3A1EF3473F40388BBE529B4D5E409CB32A6FF3473F40E8EBA6529B4D5E4029B242C8F3473F40994663529B4D5E40667EE02BF4473F40A962F0519B4D5E40DDBA6E94F4473F402EF90F519B4D5E4087E4A221F5473F40502BFE4F9B4D5E401DEDC49DF5473F403677964E9B4D5E40F6CB0B1FF6473F40B6C1C84C9B4D5E408653FEA7F6473F40CDC5A54B9B4D5E4033EBECF3F6473F40141E2A499B4D5E40337FE989F7473F403944C0479B4D5E40602A17D9F7473F401AF94F469B4D5E40A79BD226F8473F40D629D8449B4D5E400DE1B973F8473F40F8AC0D439B4D5E40FD7CF8CEF8473F40905362419B4D5E400ED98B23F9473F40B2D6973F9B4D5E406D6BFB7EF9473F40F24CF83D9B4D5E405086DFD3F9473F406B064E3C9B4D5E408E1ED52EFA473F407239B93A9B4D5E407699CE88FA473F403B9226399B4D5E404E31DFE6FA473F40FC59B7379B4D5E402A270343FB473F404D9B5D369B4D5E4088AD78A1FB473F40A90A15359B4D5E4058381B04FC473F407506E3339B4D5E40E9535B6AFC473F40459FE5329B4D5E402F9155C9FC473F40C49EFC319B4D5E409658E12BFD473F407AE734319B4D5E4076824F8DFD473F40FD8380309B4D5E4045D081F6FD473F409D13F72F9B4D5E4077BF5559FE473F408E42852F9B4D5E40EA45DCC0FE473F40B7BA342F9B4D5E403EF70624FF473F402FD2FB2E9B4D5E407E4DE188FF473F40F888DA2E9B4D5E4034BB31EFFF473F408E93CC2E9B4D5E4076198B5900483F400B48C82E9B4D5E405643F9BA00483F40E6C3C02E9B4D5E40A05D792101483F40BBA8B02E9B4D5E40AD58AE8501483F4007AB932E9B4D5E40A25F2AE101483F40666C642E9B4D5E40DED87E3E02483F403D4B282E9B4D5E40DA51879A02483F400D93E32D9B4D5E406F1F6DF002483F4054F8912D9B4D5E40BA86D24403483F4077D9382D9B4D5E40236F1A9203483F40079E692C9B4D5E4066B6381E04483F4066B0812B9B4D5E40FB60E99804483F40175C852A9B4D5E40DCDED00505483F40E8F487299B4D5E4091509C6305483F40228398289B4D5E400DD304B305483F40D62244279B4D5E4029ACD51906483F40517FFA259B4D5E4098D7937506483F4008C206259B4D5E4026FF44C206483F40C83E7C249B4D5E404618731107483F406F65FB239B4D5E402E59476407483F40AB9C98239B4D5E403F7BB5B107483F40A8F937239B4D5E40606D4C1408483F401A7AF6229B4D5E405875818B08483F40EF5EE6229B4D5E40CC68BEE308483F40972EF2229B4D5E40BCEBD83F09483F4077471F239B4D5E40109D03A309483F402FE270239B4D5E402068940C0A483F40C795EF239B4D5E401DE183820A483F4077A590249B4D5E40C4BE34F30A483F40A26F59259B4D5E40FA0F7E650B483F40237042269B4D5E401862E6D30B483F40D62244279B4D5E407C0B013E0C483F409A9A5F289B4D5E40B6AC03A70C483F409DEC78299B4D5E40CD89AD080D483F40AE66772A9B4D5E40099EE3600D483F4068AA552B9B4D5E40D5D33AB60D483F409A05FB2B9B4D5E40FEAF3C050E483F407BC7B42C9B4D5E40DABC69730E483F4057EC392D9B4D5E40841C7ADC0E483F40D1AC8D2D9B4D5E4029E8CC370F483F403411BF2D9B4D5E40FAA1858C0F483F40C28AD42D9B4D5E404F97CDEC0F483F40976FC42D9B4D5E40B1090E4210483F4010878B2D9B4D5E404E661F8F10483F40BD44132D9B4D5E404F742EE410483F40AF6D752C9B4D5E40D43D2A3611483F40C183D62B9B4D5E40727C6C8711483F4066B0812B9B4D5E40ED7381B611483F403AECE42A9B4D5E401AFFDD0212483F404765112A9B4D5E4097B4BE5512483F4075C510299B4D5E40B9B6CAAC12483F4040C70A289B4D5E40D745BCFA12483F40052CD0269B4D5E40832F224B13483F40775449259B4D5E409F1BB39813483F40AF8A14239B4D5E40627512EF13483F40947E67209B4D5E4023328E4314483F40D78AFE1C9B4D5E407C13A19A14483F40B29B5B199B4D5E4029FCD6E514483F405D45BB149B4D5E4099F9693715483F403BEC4E0F9B4D5E404373E78A15483F40AE458F089B4D5E40665DA1E415483F40438076019B4D5E403416883716483F40347B5CF89A4D5E4050BBB09516483F40E64AFDEF9A4D5E404C75A4E216483F40BA52B8E69A4D5E402EBA513017483F40692187DC9A4D5E405F59277F17483F40C6FE24D19A4D5E402E188BD017483F4003E299C39A4D5E4030D0A72918483F4053D543B59A4D5E409A9B508018483F40324203A79A4D5E40D080E7CF18483F405FAB79989A4D5E4015818A1B19483F406C7838899A4D5E40240C926519483F40C4FCBE7A9A4D5E40883523A719483F40D278366C9A4D5E40FC79C0E419483F406A22D65C9A4D5E4015B911221A483F4086114E4D9A4D5E40B8E6695C1A483F400113DE3C9A4D5E40C76792961A483F404716682B9A4D5E40460BB7D01A483F40562118199A4D5E403D81040A1B483F40021F0A069A4D5E403E3738421B483F4021978EF2994D5E40C7C24E781B483F401BCF28DC994D5E403F078EB21B483F40EB943FC6994D5E404BCB01E81B483F405BE3CEAD994D5E40BAC8AD1F1C483F409ECB3296994D5E40DD19BA511C483F400CC27C7C994D5E40F6B34A841C483F404FE1D35F994D5E404EB215B81C483F40E618DA4A994D5E4057BC1FDB1C483F406669A22D994D5E4040D55E081D483F403E3BA60F994D5E406644DC321D483F40DDFD5CFA984D5E408E33B74E1D483F400BCF8DE1984D5E40BAFEE76C1D483F4082A1CFC6984D5E40AB7FD48A1D483F407ED33DB0984D5E400E8CF8A11D483F4068409299984D5E4062054BB71D483F400D8D2782984D5E4038A453CB1D483F402EE52B6A984D5E403B5EBDDD1D483F4060A4D850984D5E404351BEEE1D483F4069E4D735984D5E40F6AF2DFE1D483F404AAC271B984D5E401216D10A1E483F40B20B6901984D5E40212E92141E483F4069F699E3974D5E407C54DF1C1E483F40206905C0974D5E4099CAC5511E483F40372F5BA6974D5E40745E614D1E483F4049C31790974D5E40652254491E483F40BA93B571974D5E40219D76431E483F405CB8FA51974D5E40B08B1F3D1E483F40DC735E38974D5E40469B0D381E483F4042C5A31A974D5E40BF7FAD321E483F40E1B123FE964D5E409B77372E1E483F40F3A69CDB964D5E4091BF8B291E483F40672A10B2964D5E4029460F251E483F40C3516188964D5E40080748221E483F40760D4460964D5E40936667211E483F40DE557C34964D5E40C02A04221E483F40BF2AF61B964D5E4068DBBF221E483F40E3255B04964D5E404E9E9C231E483F40EFC49DEC954D5E406B78A3241E483F400B15D2D0954D5E40F41C10261E483F40EC2D69B5954D5E40FAAD9B271E483F40F3152399954D5E40C75F20291E483F40901B257F954D5E408B734D2A1E483F40E5A12461954D5E40FF4B512B1E483F40295DDE42954D5E40638B062C1E483F40132BEF1F954D5E40725C782C1E483F40D7DF9300954D5E40F5A77C2C1E483F40509F7FE3944D5E40D2E5252C1E483F4049B6C7C6944D5E40A08A802B1E483F4055EA6EAB944D5E40632D952A1E483F40F32D628E944D5E40E5B639291E483F406ED4CB6E944D5E40AB3A4F271E483F4070185350944D5E40D84209251E483F405C018A33944D5E405E758B221E483F402114A015944D5E40ED96AC1F1E483F409A17A9F5934D5E401B1C791C1E483F40CC279DDB934D5E403953C1191E483F40C89D28C2934D5E409C5FFE161E483F40E9F52CAA934D5E400B9A4C141E483F407637E791934D5E40170C7B111E483F40246D9E7B934D5E4053FECA0E1E483F40EAA00664934D5E4054A6D60B1E483F40E075AA3A934D5E40B56677061E483F40BB14D815934D5E40872498011E483F40AC2FA8F5924D5E408DA14CFD1D483F408760EFDA924D5E401CB0C6F91D483F4099C003BC924D5E40C89EC4F51D483F403F4B8AA1924D5E40FEE664F21D483F407CFA568B924D5E40DEA79DEF1D483F40C234D474924D5E4041B4DAEC1D483F40CDD9425D924D5E405FEB22EA1D483F40A62A0A47924D5E400B9C80E71D483F40060CC52F924D5E408135B4E41D483F40B140AA17924D5E401DE9D4E11D483F4041189BFD914D5E40C15FC9DE1D483F40D63B79E4914D5E406AA5E9DB1D483F4009FB25CB914D5E40DAA714D91D483F40C395E1AF914D5E4031EA2ED61D483F40AE9E9897914D5E406551CED31D483F4034A0BD7E914D5E40D03386D11D483F4015C67E65914D5E40010B6CCF1D483F4068BD454B914D5E40F33F77CD1D483F40210DE630914D5E406D8FB2CB1D483F4064DBF514914D5E40A0A50ACA1D483F40EE5FFBF7904D5E40C0668FC81D483F40F12246E1904D5E40B6B590C71D483F40E44414C3904D5E40BFF576C61D483F40C577FFAB904D5E401DDCC3C51D483F408565A693904D5E40B23D29C51D483F40527B327B904D5E40E841ACC41D483F407FF41D61904D5E40ADBF44C41D483F40DDC1B045904D5E40962BFFC31D483F401317942A904D5E40DC64E2C31D483F40BC37510E904D5E407E6BEEC31D483F405CC65FF08F4D5E40B41E2AC41D483F409AF73AD48F4D5E409E9D8BC41D483F4054E33DB88F4D5E40D388EAC41D483F404411649A8F4D5E40FDA926C51D483F406B37647D8F4D5E4033C150C51D483F40CE9BFF618F4D5E40A31B70C51D483F402F26474B8F4D5E40329585C51D483F406B2C87348F4D5E40EC5BA2C51D483F4066E099338F4D5E40C6A5A3C51D483F40
18	9	0102000020E6100000440100007643F24C8A4D5E40D20727B7E8473F405AF2BF2F8A4D5E40710D61B5E8473F40CA9EC0188A4D5E408504C3B3E8473F408D73E5FD894D5E40726AA2B1E8473F40A00176E7894D5E40A279ABAFE8473F4023AE36DB894D5E40E39898AEE8473F40087E56C4894D5E40C70FD9ACE8473F40EE404CAB894D5E4049358FABE8473F404DC49592894D5E40266091AAE8473F4075FEBD79894D5E40FB2BAEA9E8473F4007DDAC61894D5E40921DCDA8E8473F401877954B894D5E40D96FD4A7E8473F408A173E34894D5E4073F1ACA6E8473F400A31131C894D5E40716770A5E8473F406D4A9301894D5E408AC90FA4E8473F400994B2E4884D5E40141688A2E8473F40CA1092C8884D5E40F5FC0EA1E8473F40ED8C33A9884D5E406625889FE8473F40CF68AB92884D5E405C74899EE8473F406A9EA271884D5E4047C1449DE8473F407F367C4F884D5E401922249CE8473F40EDE8A838884D5E404A577B9BE8473F4003BC2821884D5E40A1DEE29AE8473F40A1197008884D5E404E00599AE8473F40B2F775EF874D5E407E09E599E8473F4036500ED6874D5E4017D68799E8473F40118155BB874D5E4058403F99E8473F400E42FCA0874D5E406C951299E8473F40366FFA85874D5E40B765FE98E8473F40F3FD5D6A874D5E4060670199E8473F40F3543B4E874D5E4008051699E8473F4017364C32874D5E4045173799E8473F408D5F0117874D5E40F2E76599E8473F409A8052F9864D5E403BC4A999E8473F40FF8050DC864D5E40B584FD99E8473F4022FDF1BC864D5E4034786B9AE8473F405DCF899D864D5E408EB5DA9AE8473F4040D5CA7F864D5E401039029BE8473F4009851562864D5E4013C9BB9AE8473F40F861F444864D5E40CD440E9AE8473F40B1532329864D5E40F5DB0D99E8473F40303F7C08864D5E40FD407097E8473F40F08A94F1854D5E4029300696E8473F40A537D4D6854D5E40EEB31B94E8473F405C15DBB6854D5E40E6D07691E8473F40964B1099854D5E40AD6DB08EE8473F40DF5FE17D854D5E40E42CE68BE8473F40B319A067854D5E4018948589E8473F406882C24F854D5E40D1D6E286E8473F40D2394937854D5E40BDC52C84E8473F40CCC22A20854D5E40FAB77C81E8473F40822B4D08854D5E407CE3AF7EE8473F4035D7BAEB844D5E402F7C5D7BE8473F40F37946D4844D5E40CF9ABB78E8473F40354E82B8844D5E40320BE775E8473F40053E629B844D5E40FD125E73E8473F407FF7217E844D5E40F8D21971E8473F40FBAAB560844D5E400A8B096FE8473F401C8BF442844D5E40C3E00D6DE8473F4016A93A28844D5E40C2A7186BE8473F40F4890C10844D5E404C45D868E8473F40E50F78F3834D5E40C30AD765E8473F402E82BAD9834D5E40F596F262E8473F406988FAC2834D5E404AE56460E8473F40C0D1DAA9834D5E408E369A5DE8473F402C1F988F834D5E4072C6FE5AE8473F4035BD0876834D5E40B02FE458E8473F40D033D25F834D5E4078F26B57E8473F40FB105B47834D5E40E8520856E8473F405B435D2F834D5E4031D1DA54E8473F404321791C834D5E400CD01154E8473F40C19EEB05834D5E402A787253E8473F40ED889EEF824D5E40C4A80353E8473F4061231BD8824D5E403BC6F652E8473F4025FF3DBF824D5E401EE67253E8473F4003E00FA7824D5E40521C9C54E8473F401EFF7C90824D5E40869D8F56E8473F400320E478824D5E40256CD659E8473F40A7215E62824D5E40AE17F05EE8473F402A3C054C824D5E40D80E6366E8473F4097F45D35824D5E4001C2B870E8473F4033B6421E824D5E409797687EE8473F40F559A008824D5E40962AB28DE8473F406A907FEF814D5E40356149A2E8473F40B3B17AD6814D5E40ED77C2B9E8473F40DA4F40BF814D5E408AED29D2E8473F40C177A5A9814D5E40443D98EBE8473F40EDB99D94814D5E40B2C94407E9473F4008E63480814D5E409379BF24E9473F40FCD70D69814D5E4073D3A549E9473F40819B4153814D5E405BAC4271E9473F409F6A393F814D5E40D1F8779AE9473F401C59732C814D5E4028FBBDC5E9473F40AFA9FB19814D5E40529E85F5E9473F40C79DCD08814D5E40ED831A28EA473F40E9C5DCF7804D5E40E5AE9D60EA473F40CBC8D2E7804D5E40C6D0479FEA473F40162574D9804D5E403F4FACE0EA473F4054B175CC804D5E40CA4E0D25EB473F40C741A9C0804D5E4090B2066EEB473F409F37E0B6804D5E40396E9FB7EB473F40D3A9F8AD804D5E40AD47880BEC473F4047ACC5A7804D5E40CD201455EC473F40681BD3A1804D5E401ED4B7A8EC473F40E8FA699C804D5E404F4217FBEC473F401935BD96804D5E40DC97F351ED473F40A5426591804D5E404693E29FED473F408F170A8C804D5E408930C7F0ED473F40A2BCA387804D5E400781893EEE473F40A8E81084804D5E40DCC9CB8DEE473F405B307781804D5E4057E2C8DEEE473F40722E2880804D5E40A6A1C432EF473F40722E2880804D5E40177ADB84EF473F405F6F3A80804D5E40A5CE87D6EF473F40228F1080804D5E4056D762EDEF473F40A81F047F804D5E408DC2763BF0473F405F62107E804D5E409F31B190F0473F4003E62E7D804D5E403CCF94E8F0473F405C613E7C804D5E408F8E1953F1473F402F9DA17B804D5E40CE624FA2F1473F403585F17A804D5E40E3402909F2473F40FD92437A804D5E40D4DC4E7EF2473F40789EB279804D5E40A35E29F0F2473F4058082779804D5E40372E566AF3473F4031DB9278804D5E405F28F5E8F3473F4081CBF177804D5E40DDB0A86AF4473F4043423B77804D5E4026AFA0F9F4473F40753F6F76804D5E40367FAA93F5473F406C050676804D5E409CD738E1F5473F401CB73575804D5E40A3220F7AF6473F4096C8D074804D5E40EC2901C6F6473F40EB556474804D5E406F5DC619F7473F4040E3F773804D5E4017CB4970F7473F4065B84673804D5E40B490A907F8473F40CC0AF472804D5E40FF21D854F8473F4077CEA772804D5E40FC5261A2F8473F4066036272804D5E40196540F1F8473F403DE22572804D5E40ACBF0B3EF9473F4001FCCF71804D5E409113B3C8F9473F40B8ED9471804D5E40ED3AB357FA473F40A6287B71804D5E4054FCA8D5FA473F40C6157A71804D5E40CB04764FFB473F40B2568C71804D5E40BE035ABFFB473F40287AAB71804D5E4021F19126FC473F409E9DCA71804D5E404DA02187FC473F400D2AE171804D5E40B4A41DE3FC473F402680D771804D5E400973E73CFD473F40A52EA771804D5E4032C67E8CFD473F40C9092271804D5E4045998EFBFD473F408A806B70804D5E409732C564FE473F40EE2FB86F804D5E40FBA08AC2FE473F4058760D6F804D5E40BF572B15FF473F404A9F6F6E804D5E40E2F3E861FF473F4089CAB46D804D5E4055A438C2FF473F40C6FB256D804D5E40CE636F0E00483F40CCE3756C804D5E40CEA8715E00483F406F67946B804D5E40264465B100483F40A558706A804D5E40BFE1A60C01483F4028460369804D5E40E21FDA7101483F406F4D4067804D5E40A90F57E001483F4043DAEA65804D5E400847A02C02483F40374E6864804D5E408DB22A7D02483F40E84AB362804D5E40CF3D98D202483F40F271C660804D5E40CF6CA82F03483F4009C7EA5E804D5E408BD83A8803483F40B526015D804D5E403A0445E503483F40BE53405B804D5E40721F864404483F405500C159804D5E40C76B92A204483F4021FC8E58804D5E40FA3835F804483F4081087B57804D5E405365125005483F4044799856804D5E40C36822A005483F40D53DC955804D5E40AB3131ED05483F4099A8BA54804D5E401BCBBC5806483F403D2CD953804D5E402A8721B906483F40EBE33453804D5E40C559F60407483F40BE1F9852804D5E407187795207483F4059180652804D5E40B068D9A307483F40FAA77C51804D5E400424FCFC07483F40C6520351804D5E40F6D1B15308483F40DC059950804D5E40513ACF9F08483F40A8B01F50804D5E40166FB2F608483F400A66984F804D5E404DFC3B5D09483F4027B0364F804D5E408F1561AB09483F400420D74E804D5E40C66B10FD09483F4046EE7C4E804D5E4027ABD84E0A483F40AC402A4E804D5E4050A7ABA00A483F403817DF4D804D5E4035DA83EF0A483F404739984D804D5E40454E69410B483F405CF2594D804D5E4078D4BC8D0B483F4013E41E4D804D5E40923943E60B483F409292EE4C804D5E40FF225D350C483F404984B34C804D5E40E29737940C483F400076784C804D5E40151B40EE0C483F40F5413B4C804D5E4081B7744F0D483F4093DD094C804D5E40751FAD9E0D483F403179D84B804D5E406F76E2F00D483F40AF27A84B804D5E40164DFF430E483F400FE9784B804D5E40B954F0960E483F406EAA494B804D5E40F5F70DED0E483F40AE7E1B4B804D5E40B806A3420F483F402D2DEB4A804D5E401981C0A00F483F402E27BF4A804D5E403F59A5FB0F483F4055A59A4A804D5E40FAAE5E4710483F4055A59A4A804D5E4078DAEF4710483F4090DC374A804D5E403963C39310483F40B1C30A4A804D5E40DD2567E410483F402D7E324A804D5E40E1845F3511483F4053ABC64A804D5E40DD9CC48311483F402451F34B804D5E40D0C10FDB11483F40FA3CE14D804D5E40EB05952B12483F4081E1FC50804D5E405C0F227A12483F405F1AAA55804D5E40C12EBBC512483F4028A7BF5C804D5E40AC43661513483F40E26F8D65804D5E4039C30B6513483F4098A85070804D5E40CD12AFB613483F40E349777C804D5E40A2484A0514483F4068DD4B89804D5E4022FCCA4D14483F40EF71F297804D5E406565079614483F40BE272AA9804D5E40EAE7B3DE14483F40D7A153BD804D5E40A97BAF2515483F405968FED0804D5E40E67B275E15483F40BE3552E4804D5E4065E4F68B15483F40C74FD1FB804D5E40E1E188B915483F4001608610814D5E401BBDFBDB15483F403F6BE126814D5E40DE8D22FF15483F400E8CB43B814D5E4037B9011E16483F408288EB50814D5E40AEE3053A16483F40EFAF2869814D5E40BDF2FE5216483F407EB80C81814D5E4092AFD66216483F40D8BCBE97814D5E402EB7706B16483F40C14DDCB0814D5E400678656F16483F400F3ED1CB814D5E40B7F1716E16483F40A6F1E5E7814D5E40EAF4226C16483F407927B302824D5E4041EDA26D16483F405E084619824D5E40A840D17016483F40C3EFED30824D5E403595C87416483F40CFE3C043824D5E405C987A9616483F40AB8AEA59824D5E40FA91BA9716483F40431E7F71824D5E401960699816483F403F1D438B824D5E402D514E9816483F407C2ECAA1824D5E408CAE309816483F40DE21CAB9824D5E408617289816483F40587E16D4824D5E408B4A429816483F406E8987F0824D5E404A7C9C9816483F40E13AA306834D5E40C730989816483F404F5CB41E834D5E408323809816483F409BED6536834D5E407401C79816483F40F7F8154F834D5E402C382A9916483F4092EBED69834D5E40479B9B9916483F401F659985834D5E40172E219A16483F40FE845AA3834D5E407CD1C49A16483F40AF9563C1834D5E40635C7E9B16483F40B24C82E1834D5E404F1A529C16483F4048C9A302844D5E40A0CC109D16483F40D091F219844D5E40BB2F829D16483F40162E2A30844D5E407366E59D16483F40B6AAE048844D5E40D4023A9E16483F406FEE5465844D5E40B3B7789E16483F40CDA2917E844D5E402FA4979E16483F4097E21296844D5E401C17A19E16483F40D796FAAC844D5E402FA4979E16483F405BBCE8C3844D5E404F277C9E16483F4067CA0FDB844D5E4095C44D9E16483F4061C2D5F2844D5E4034C40A9E16483F407323E80C854D5E40248FAA9D16483F40CB401C26854D5E40BEBF3B9D16483F40080E863F854D5E406A19D59C16483F40BF3DD257854D5E4047F38F9C16483F4045197771854D5E4048BB6C9C16483F403CCC4D8A854D5E40B2E2719C16483F4093E981A3854D5E40EE90A49C16483F40BAF62BBB854D5E40300E039D16483F4010BC1AD3854D5E40BA67A59D16483F40414E49EA854D5E408706839E16483F404C45FD0A864D5E406D7818A016483F40CD9DC128864D5E40C244CDA116483F402C59FC43864D5E40A75274A316483F40328CFD5D864D5E407568E7A416483F407E6EF674864D5E40142A04A616483F40DA86D08F864D5E40B28732A716483F401CE444A7864D5E400EA025A816483F402BF33DC0864D5E401FB009A916483F40BD9158D6864D5E40F411BBA916483F409D846FED864D5E4066AB4CAA16483F40D9F99305874D5E404234C0AA16483F4056EC161E874D5E408AAC15AB16483F40D9173135874D5E40BBC848AB16483F4073F0B44B874D5E40F24361AB16483F40737F8363874D5E40E01A59AB16483F409834E879874D5E40ED7435AB16483F4091904B93874D5E403B71ECAA16483F4073DBA7AB874D5E400C8184AA16483F4005D833C3874D5E4038DBB5AC16483F40B86A4BDB874D5E40CFF9CFB016483F4069F664F1874D5E40FF1CB0B416483F407CE6AF07884D5E406FF511B916483F40C877611F884D5E40714ED8BD16483F40D4344137884D5E402240AAC216483F40D7B7B74E884D5E403DAAC8C616483F40E11C5265884D5E4026811ECA16483F4049F8A17C884D5E40F4E15BCC16483F4065D10E94884D5E40269B72CE16483F40620EC4AA884D5E4074B7FAD016483F40795D52C4884D5E40DCB9E1D416483F409536BFDB884D5E403193C0D816483F40EB08D8F5884D5E40A2CF10DD16483F40D5F13A10894D5E40AE7B64E116483F40A8BC6C27894D5E4091BBB1E416483F40A350953D894D5E40042A4AE716483F400D400D59894D5E406DE166EA16483F40C4CDCA72894D5E4077F0D6EC16483F40E2A00B8A894D5E40F641B6EE16483F4034787EA2894D5E408AE868F016483F408E2BE9B9894D5E40FA68D6F116483F405677C2D1894D5E4028A408F316483F40876AADDC894D5E40175684F316483F40132178F3894D5E4097F8AAF416483F408193D00A8A4D5E403087ADF516483F40ACEC67238A4D5E40BEE79EF616483F403E8B82398A4D5E4028BE5CF716483F40503DDC528A4D5E4021721EF816483F4033820C6B8A4D5E402121D7F816483F40F8CC13818A4D5E408C5B83F916483F4051D7F1978A4D5E4029DE2DFA16483F40	6	3	1	186.65433289547764	186.65433289547764	0105000020E6100000010000000102000000440100007643F24C8A4D5E40D20727B7E8473F405AF2BF2F8A4D5E40710D61B5E8473F40CA9EC0188A4D5E408504C3B3E8473F408D73E5FD894D5E40726AA2B1E8473F40A00176E7894D5E40A279ABAFE8473F4023AE36DB894D5E40E39898AEE8473F40087E56C4894D5E40C70FD9ACE8473F40EE404CAB894D5E4049358FABE8473F404DC49592894D5E40266091AAE8473F4075FEBD79894D5E40FB2BAEA9E8473F4007DDAC61894D5E40921DCDA8E8473F401877954B894D5E40D96FD4A7E8473F408A173E34894D5E4073F1ACA6E8473F400A31131C894D5E40716770A5E8473F406D4A9301894D5E408AC90FA4E8473F400994B2E4884D5E40141688A2E8473F40CA1092C8884D5E40F5FC0EA1E8473F40ED8C33A9884D5E406625889FE8473F40CF68AB92884D5E405C74899EE8473F406A9EA271884D5E4047C1449DE8473F407F367C4F884D5E401922249CE8473F40EDE8A838884D5E404A577B9BE8473F4003BC2821884D5E40A1DEE29AE8473F40A1197008884D5E404E00599AE8473F40B2F775EF874D5E407E09E599E8473F4036500ED6874D5E4017D68799E8473F40118155BB874D5E4058403F99E8473F400E42FCA0874D5E406C951299E8473F40366FFA85874D5E40B765FE98E8473F40F3FD5D6A874D5E4060670199E8473F40F3543B4E874D5E4008051699E8473F4017364C32874D5E4045173799E8473F408D5F0117874D5E40F2E76599E8473F409A8052F9864D5E403BC4A999E8473F40FF8050DC864D5E40B584FD99E8473F4022FDF1BC864D5E4034786B9AE8473F405DCF899D864D5E408EB5DA9AE8473F4040D5CA7F864D5E401039029BE8473F4009851562864D5E4013C9BB9AE8473F40F861F444864D5E40CD440E9AE8473F40B1532329864D5E40F5DB0D99E8473F40303F7C08864D5E40FD407097E8473F40F08A94F1854D5E4029300696E8473F40A537D4D6854D5E40EEB31B94E8473F405C15DBB6854D5E40E6D07691E8473F40964B1099854D5E40AD6DB08EE8473F40DF5FE17D854D5E40E42CE68BE8473F40B319A067854D5E4018948589E8473F406882C24F854D5E40D1D6E286E8473F40D2394937854D5E40BDC52C84E8473F40CCC22A20854D5E40FAB77C81E8473F40822B4D08854D5E407CE3AF7EE8473F4035D7BAEB844D5E402F7C5D7BE8473F40F37946D4844D5E40CF9ABB78E8473F40354E82B8844D5E40320BE775E8473F40053E629B844D5E40FD125E73E8473F407FF7217E844D5E40F8D21971E8473F40FBAAB560844D5E400A8B096FE8473F401C8BF442844D5E40C3E00D6DE8473F4016A93A28844D5E40C2A7186BE8473F40F4890C10844D5E404C45D868E8473F40E50F78F3834D5E40C30AD765E8473F402E82BAD9834D5E40F596F262E8473F406988FAC2834D5E404AE56460E8473F40C0D1DAA9834D5E408E369A5DE8473F402C1F988F834D5E4072C6FE5AE8473F4035BD0876834D5E40B02FE458E8473F40D033D25F834D5E4078F26B57E8473F40FB105B47834D5E40E8520856E8473F405B435D2F834D5E4031D1DA54E8473F404321791C834D5E400CD01154E8473F40C19EEB05834D5E402A787253E8473F40ED889EEF824D5E40C4A80353E8473F4061231BD8824D5E403BC6F652E8473F4025FF3DBF824D5E401EE67253E8473F4003E00FA7824D5E40521C9C54E8473F401EFF7C90824D5E40869D8F56E8473F400320E478824D5E40256CD659E8473F40A7215E62824D5E40AE17F05EE8473F402A3C054C824D5E40D80E6366E8473F4097F45D35824D5E4001C2B870E8473F4033B6421E824D5E409797687EE8473F40F559A008824D5E40962AB28DE8473F406A907FEF814D5E40356149A2E8473F40B3B17AD6814D5E40ED77C2B9E8473F40DA4F40BF814D5E408AED29D2E8473F40C177A5A9814D5E40443D98EBE8473F40EDB99D94814D5E40B2C94407E9473F4008E63480814D5E409379BF24E9473F40FCD70D69814D5E4073D3A549E9473F40819B4153814D5E405BAC4271E9473F409F6A393F814D5E40D1F8779AE9473F401C59732C814D5E4028FBBDC5E9473F40AFA9FB19814D5E40529E85F5E9473F40C79DCD08814D5E40ED831A28EA473F40E9C5DCF7804D5E40E5AE9D60EA473F40CBC8D2E7804D5E40C6D0479FEA473F40162574D9804D5E403F4FACE0EA473F4054B175CC804D5E40CA4E0D25EB473F40C741A9C0804D5E4090B2066EEB473F409F37E0B6804D5E40396E9FB7EB473F40D3A9F8AD804D5E40AD47880BEC473F4047ACC5A7804D5E40CD201455EC473F40681BD3A1804D5E401ED4B7A8EC473F40E8FA699C804D5E404F4217FBEC473F401935BD96804D5E40DC97F351ED473F40A5426591804D5E404693E29FED473F408F170A8C804D5E408930C7F0ED473F40A2BCA387804D5E400781893EEE473F40A8E81084804D5E40DCC9CB8DEE473F405B307781804D5E4057E2C8DEEE473F40722E2880804D5E40A6A1C432EF473F40722E2880804D5E40177ADB84EF473F405F6F3A80804D5E40A5CE87D6EF473F40228F1080804D5E4056D762EDEF473F40A81F047F804D5E408DC2763BF0473F405F62107E804D5E409F31B190F0473F4003E62E7D804D5E403CCF94E8F0473F405C613E7C804D5E408F8E1953F1473F402F9DA17B804D5E40CE624FA2F1473F403585F17A804D5E40E3402909F2473F40FD92437A804D5E40D4DC4E7EF2473F40789EB279804D5E40A35E29F0F2473F4058082779804D5E40372E566AF3473F4031DB9278804D5E405F28F5E8F3473F4081CBF177804D5E40DDB0A86AF4473F4043423B77804D5E4026AFA0F9F4473F40753F6F76804D5E40367FAA93F5473F406C050676804D5E409CD738E1F5473F401CB73575804D5E40A3220F7AF6473F4096C8D074804D5E40EC2901C6F6473F40EB556474804D5E406F5DC619F7473F4040E3F773804D5E4017CB4970F7473F4065B84673804D5E40B490A907F8473F40CC0AF472804D5E40FF21D854F8473F4077CEA772804D5E40FC5261A2F8473F4066036272804D5E40196540F1F8473F403DE22572804D5E40ACBF0B3EF9473F4001FCCF71804D5E409113B3C8F9473F40B8ED9471804D5E40ED3AB357FA473F40A6287B71804D5E4054FCA8D5FA473F40C6157A71804D5E40CB04764FFB473F40B2568C71804D5E40BE035ABFFB473F40287AAB71804D5E4021F19126FC473F409E9DCA71804D5E404DA02187FC473F400D2AE171804D5E40B4A41DE3FC473F402680D771804D5E400973E73CFD473F40A52EA771804D5E4032C67E8CFD473F40C9092271804D5E4045998EFBFD473F408A806B70804D5E409732C564FE473F40EE2FB86F804D5E40FBA08AC2FE473F4058760D6F804D5E40BF572B15FF473F404A9F6F6E804D5E40E2F3E861FF473F4089CAB46D804D5E4055A438C2FF473F40C6FB256D804D5E40CE636F0E00483F40CCE3756C804D5E40CEA8715E00483F406F67946B804D5E40264465B100483F40A558706A804D5E40BFE1A60C01483F4028460369804D5E40E21FDA7101483F406F4D4067804D5E40A90F57E001483F4043DAEA65804D5E400847A02C02483F40374E6864804D5E408DB22A7D02483F40E84AB362804D5E40CF3D98D202483F40F271C660804D5E40CF6CA82F03483F4009C7EA5E804D5E408BD83A8803483F40B526015D804D5E403A0445E503483F40BE53405B804D5E40721F864404483F405500C159804D5E40C76B92A204483F4021FC8E58804D5E40FA3835F804483F4081087B57804D5E405365125005483F4044799856804D5E40C36822A005483F40D53DC955804D5E40AB3131ED05483F4099A8BA54804D5E401BCBBC5806483F403D2CD953804D5E402A8721B906483F40EBE33453804D5E40C559F60407483F40BE1F9852804D5E407187795207483F4059180652804D5E40B068D9A307483F40FAA77C51804D5E400424FCFC07483F40C6520351804D5E40F6D1B15308483F40DC059950804D5E40513ACF9F08483F40A8B01F50804D5E40166FB2F608483F400A66984F804D5E404DFC3B5D09483F4027B0364F804D5E408F1561AB09483F400420D74E804D5E40C66B10FD09483F4046EE7C4E804D5E4027ABD84E0A483F40AC402A4E804D5E4050A7ABA00A483F403817DF4D804D5E4035DA83EF0A483F404739984D804D5E40454E69410B483F405CF2594D804D5E4078D4BC8D0B483F4013E41E4D804D5E40923943E60B483F409292EE4C804D5E40FF225D350C483F404984B34C804D5E40E29737940C483F400076784C804D5E40151B40EE0C483F40F5413B4C804D5E4081B7744F0D483F4093DD094C804D5E40751FAD9E0D483F403179D84B804D5E406F76E2F00D483F40AF27A84B804D5E40164DFF430E483F400FE9784B804D5E40B954F0960E483F406EAA494B804D5E40F5F70DED0E483F40AE7E1B4B804D5E40B806A3420F483F402D2DEB4A804D5E401981C0A00F483F402E27BF4A804D5E403F59A5FB0F483F4055A59A4A804D5E40FAAE5E4710483F4055A59A4A804D5E4078DAEF4710483F4090DC374A804D5E403963C39310483F40B1C30A4A804D5E40DD2567E410483F402D7E324A804D5E40E1845F3511483F4053ABC64A804D5E40DD9CC48311483F402451F34B804D5E40D0C10FDB11483F40FA3CE14D804D5E40EB05952B12483F4081E1FC50804D5E405C0F227A12483F405F1AAA55804D5E40C12EBBC512483F4028A7BF5C804D5E40AC43661513483F40E26F8D65804D5E4039C30B6513483F4098A85070804D5E40CD12AFB613483F40E349777C804D5E40A2484A0514483F4068DD4B89804D5E4022FCCA4D14483F40EF71F297804D5E406565079614483F40BE272AA9804D5E40EAE7B3DE14483F40D7A153BD804D5E40A97BAF2515483F405968FED0804D5E40E67B275E15483F40BE3552E4804D5E4065E4F68B15483F40C74FD1FB804D5E40E1E188B915483F4001608610814D5E401BBDFBDB15483F403F6BE126814D5E40DE8D22FF15483F400E8CB43B814D5E4037B9011E16483F408288EB50814D5E40AEE3053A16483F40EFAF2869814D5E40BDF2FE5216483F407EB80C81814D5E4092AFD66216483F40D8BCBE97814D5E402EB7706B16483F40C14DDCB0814D5E400678656F16483F400F3ED1CB814D5E40B7F1716E16483F40A6F1E5E7814D5E40EAF4226C16483F407927B302824D5E4041EDA26D16483F405E084619824D5E40A840D17016483F40C3EFED30824D5E403595C87416483F40CFE3C043824D5E405C987A9616483F40AB8AEA59824D5E40FA91BA9716483F40431E7F71824D5E401960699816483F403F1D438B824D5E402D514E9816483F407C2ECAA1824D5E408CAE309816483F40DE21CAB9824D5E408617289816483F40587E16D4824D5E408B4A429816483F406E8987F0824D5E404A7C9C9816483F40E13AA306834D5E40C730989816483F404F5CB41E834D5E408323809816483F409BED6536834D5E407401C79816483F40F7F8154F834D5E402C382A9916483F4092EBED69834D5E40479B9B9916483F401F659985834D5E40172E219A16483F40FE845AA3834D5E407CD1C49A16483F40AF9563C1834D5E40635C7E9B16483F40B24C82E1834D5E404F1A529C16483F4048C9A302844D5E40A0CC109D16483F40D091F219844D5E40BB2F829D16483F40162E2A30844D5E407366E59D16483F40B6AAE048844D5E40D4023A9E16483F406FEE5465844D5E40B3B7789E16483F40CDA2917E844D5E402FA4979E16483F4097E21296844D5E401C17A19E16483F40D796FAAC844D5E402FA4979E16483F405BBCE8C3844D5E404F277C9E16483F4067CA0FDB844D5E4095C44D9E16483F4061C2D5F2844D5E4034C40A9E16483F407323E80C854D5E40248FAA9D16483F40CB401C26854D5E40BEBF3B9D16483F40080E863F854D5E406A19D59C16483F40BF3DD257854D5E4047F38F9C16483F4045197771854D5E4048BB6C9C16483F403CCC4D8A854D5E40B2E2719C16483F4093E981A3854D5E40EE90A49C16483F40BAF62BBB854D5E40300E039D16483F4010BC1AD3854D5E40BA67A59D16483F40414E49EA854D5E408706839E16483F404C45FD0A864D5E406D7818A016483F40CD9DC128864D5E40C244CDA116483F402C59FC43864D5E40A75274A316483F40328CFD5D864D5E407568E7A416483F407E6EF674864D5E40142A04A616483F40DA86D08F864D5E40B28732A716483F401CE444A7864D5E400EA025A816483F402BF33DC0864D5E401FB009A916483F40BD9158D6864D5E40F411BBA916483F409D846FED864D5E4066AB4CAA16483F40D9F99305874D5E404234C0AA16483F4056EC161E874D5E408AAC15AB16483F40D9173135874D5E40BBC848AB16483F4073F0B44B874D5E40F24361AB16483F40737F8363874D5E40E01A59AB16483F409834E879874D5E40ED7435AB16483F4091904B93874D5E403B71ECAA16483F4073DBA7AB874D5E400C8184AA16483F4005D833C3874D5E4038DBB5AC16483F40B86A4BDB874D5E40CFF9CFB016483F4069F664F1874D5E40FF1CB0B416483F407CE6AF07884D5E406FF511B916483F40C877611F884D5E40714ED8BD16483F40D4344137884D5E402240AAC216483F40D7B7B74E884D5E403DAAC8C616483F40E11C5265884D5E4026811ECA16483F4049F8A17C884D5E40F4E15BCC16483F4065D10E94884D5E40269B72CE16483F40620EC4AA884D5E4074B7FAD016483F40795D52C4884D5E40DCB9E1D416483F409536BFDB884D5E403193C0D816483F40EB08D8F5884D5E40A2CF10DD16483F40D5F13A10894D5E40AE7B64E116483F40A8BC6C27894D5E4091BBB1E416483F40A350953D894D5E40042A4AE716483F400D400D59894D5E406DE166EA16483F40C4CDCA72894D5E4077F0D6EC16483F40E2A00B8A894D5E40F641B6EE16483F4034787EA2894D5E408AE868F016483F408E2BE9B9894D5E40FA68D6F116483F405677C2D1894D5E4028A408F316483F40876AADDC894D5E40175684F316483F40132178F3894D5E4097F8AAF416483F408193D00A8A4D5E403087ADF516483F40ACEC67238A4D5E40BEE79EF616483F403E8B82398A4D5E4028BE5CF716483F40503DDC528A4D5E4021721EF816483F4033820C6B8A4D5E402121D7F816483F40F8CC13818A4D5E408C5B83F916483F4051D7F1978A4D5E4029DE2DFA16483F40
\.


--
-- Data for Name: topo_map_vertices_pgr; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.topo_map_vertices_pgr (id, cnt, chk, ein, eout, the_geom) FROM stdin;
1	\N	\N	\N	\N	0101000020E61000006007F9998D4D5E409080F1CE11483F40
2	\N	\N	\N	\N	0101000020E6100000352A38088F4D5E4059DEA65617483F40
3	\N	\N	\N	\N	0101000020E610000051D7F1978A4D5E4029DE2DFA16483F40
4	\N	\N	\N	\N	0101000020E61000007AD35ABC8B4D5E4063723DE610483F40
5	\N	\N	\N	\N	0101000020E6100000EF9C51E68B4D5E407AFE98FCED473F40
6	\N	\N	\N	\N	0101000020E61000007643F24C8A4D5E40D20727B7E8473F40
7	\N	\N	\N	\N	0101000020E610000066E099338F4D5E40C6A5A3C51D483F40
8	\N	\N	\N	\N	0101000020E610000039F5E1568A4D5E4090ECA082E2473F40
9	\N	\N	\N	\N	0101000020E6100000A089FC6F8F4D5E4087269B76E2473F40
10	\N	\N	\N	\N	0101000020E6100000E6FCF1B78D4D5E409F3B9C5CEE473F40
11	\N	\N	\N	\N	0101000020E61000008529BD7B8F4D5E40F0A0F714E9473F40
12	\N	\N	\N	\N	0101000020E6100000EEC984DE894D5E40AF034F701D483F40
\.


--
-- Data for Name: geocode_settings; Type: TABLE DATA; Schema: tiger; Owner: postgres
--

COPY tiger.geocode_settings (name, setting, unit, category, short_desc) FROM stdin;
\.


--
-- Data for Name: pagc_gaz; Type: TABLE DATA; Schema: tiger; Owner: postgres
--

COPY tiger.pagc_gaz (id, seq, word, stdword, token, is_custom) FROM stdin;
\.


--
-- Data for Name: pagc_lex; Type: TABLE DATA; Schema: tiger; Owner: postgres
--

COPY tiger.pagc_lex (id, seq, word, stdword, token, is_custom) FROM stdin;
\.


--
-- Data for Name: pagc_rules; Type: TABLE DATA; Schema: tiger; Owner: postgres
--

COPY tiger.pagc_rules (id, rule, is_custom) FROM stdin;
\.


--
-- Data for Name: topology; Type: TABLE DATA; Schema: topology; Owner: postgres
--

COPY topology.topology (id, name, srid, "precision", hasz) FROM stdin;
\.


--
-- Data for Name: layer; Type: TABLE DATA; Schema: topology; Owner: postgres
--

COPY topology.layer (topology_id, layer_id, schema_name, table_name, feature_column, feature_type, level, child_id) FROM stdin;
\.


--
-- Name: topo_map_gid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.topo_map_gid_seq', 18, true);


--
-- Name: topo_map_vertices_pgr_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.topo_map_vertices_pgr_id_seq', 12, true);


--
-- Name: topo_map topo_map_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.topo_map
    ADD CONSTRAINT topo_map_pkey PRIMARY KEY (gid);


--
-- Name: topo_map_vertices_pgr topo_map_vertices_pgr_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.topo_map_vertices_pgr
    ADD CONSTRAINT topo_map_vertices_pgr_pkey PRIMARY KEY (id);


--
-- Name: topo_map_source_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX topo_map_source_idx ON public.topo_map USING btree (source);


--
-- Name: topo_map_target_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX topo_map_target_idx ON public.topo_map USING btree (target);


--
-- Name: topo_map_the_geom_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX topo_map_the_geom_idx ON public.topo_map USING gist (the_geom);


--
-- Name: topo_map_vertices_pgr_the_geom_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX topo_map_vertices_pgr_the_geom_idx ON public.topo_map_vertices_pgr USING gist (the_geom);


--
-- PostgreSQL database dump complete
--

