CREATE OR REPLACE FUNCTION public.calculate_cost(tbl character varying) RETURNS double precision
    LANGUAGE plpgsql STRICT
    AS $$ 
declare 
    start_point geometry;
    end_point geometry;
    start_gid integer;
    end_gid integer;
    start_line geometry;
    end_line geometry;
    start_line_cost double precision;
    end_line_cost double precision;

    per_start float;
    per_end float;
    
    sum_cost double precision;
begin
    -- 从结果拓扑中获取起点和终点
    --execute 'select st_startpoint(*) from shortpath' into start_point;
    --execute 'select st_endpoint(*) from shortpath' into end_point;
    select st_startpoint(pgr_fromatontob) from shortpath into start_point;
    select st_endpoint(pgr_fromatontob) from shortpath into end_point;

    -- 有可能出现多个查询结果, 使用最近搜索.
    -- 这个查询语句非常不优雅, 但是牵扯到excute的用法, 目前只能这样
    -- 找到起点终点最近的线段
    execute '
	SELECT gid, the_geom, cost FROM '||tbl||'
	WHERE ST_DWithin(geog, geography( (select st_startpoint(pgr_fromatontob) from shortpath) ), 0.5)
    ORDER BY ST_Distance(geog, geography( (select st_startpoint(pgr_fromatontob) from shortpath) )) limit 1'
    INTO start_gid, start_line, start_line_cost;
    execute'
    SELECT gid, the_geom, cost FROM '||tbl||'
	WHERE  ST_DWithin(geog, geography( (select st_endpoint(pgr_fromatontob) from shortpath) ), 0.5)
    ORDER BY ST_Distance(geog, geography( (select st_endpoint(pgr_fromatontob) from shortpath) )) limit 1'
    INTO end_gid, end_line, end_line_cost;
    
    -- 为了解决可能出现的重复路段id, 对table edge_id进行处理
    -- 去掉重复的edge_id
    delete from edge_id where ctid not in (select min(ctid) from edge_id group by edge);
    
    drop table if exists path_cost;
    -- 改为创建临时表
    -- 找到edge_id对应的gid和cost
    execute '
    create temp table path_cost as select t.gid, t.cost from '||tbl||' t, edge_id e
    where t.gid = e.edge;';

    -- 找到起点和终点在起始段和终点段上的百分比
    select ST_LineLocatePoint(start_line, start_point) into per_start;
    select ST_LineLocatePoint(end_line, end_point) into per_end;

    -- 除去起始和终点段之外其他的cost
    select sum(cost) from path_cost where gid != start_gid and gid != end_gid
    into sum_cost;
    
    -- 按百分比计算起始和终点段的cost
    if start_gid = end_gid then
        sum_cost := (per_end-per_start) * start_line_cost;
    else
        sum_cost := sum_cost + (1-per_start)*start_line_cost + per_end*end_line_cost;
    end if;
    RAISE NOTICE 'start gid: %; end gid: %',start_gid,end_gid;
    RAISE NOTICE 'start per: %; end per: %',per_start,per_end;
    RAISE NOTICE 'sum cost: %',sum_cost;
    
    return sum_cost;
end;
$$;


ALTER FUNCTION public.calculate_cost(tbl character varying) OWNER TO postgres;

CREATE OR REPLACE FUNCTION public.calculate_cost_test(tbl character varying, edge_ids integer[]) RETURNS double precision
    LANGUAGE plpgsql STRICT
    AS $$ 
declare
    sum_cost double precision;
begin
    -- 直接累加所有edge_id对应gid的路径的cost
    execute 'select sum(t.cost) from unnest($1) edge_id left join ' || tbl || ' t on edge_id = t.gid' into sum_cost using edge_ids;
    return sum_cost;
end;
$$;


ALTER FUNCTION public.calculate_cost_test(tbl character varying, edge_ids integer[]) OWNER TO postgres;

--
-- Name: line2points(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE OR REPLACE FUNCTION public.line2points(tbl character varying) RETURNS void
    LANGUAGE plpgsql STRICT
    AS $$ 
declare 
	ii integer; -- 循环次数
	n integer;
	point_temp geometry; --path节点
	longtitude double precision;
	latitude double precision;

begin
	-- 根据short path的顺序依次获取节点
	drop table if exists result_points_tbl;
	CREATE TEMP TABLE result_points_tbl(id integer,point geometry,lon double precision ,lat double precision);
    create index result_points_tbl_idx on result_points_tbl using GIST(point);

	execute 'select ST_NPoints(pgr_fromatontob) from ' || tbl|| ' ' into n;

	ii:=1;
	FOR ii IN 1..n LOOP	-- shaungfu: begin with num 1 instead of 0

	execute 'SELECT ST_PointN(pgr_fromatontob, ' ||ii|| ') FROM ' ||tbl|| ' ' into point_temp;
		   
	SELECT st_x(point_temp) into longtitude;
	SELECT st_y(point_temp) into latitude;
	insert into result_points_tbl(id,point,lon,lat) values(ii,point_temp,longtitude,latitude);
	end loop;

	-- 选取shortpath缓冲区的的属性点.
	-- 空间关系由buffer改为intersects, 节省时间..
	-- drop table if exists point_in_buffer;
	-- create table point_in_buffer as SELECT *  FROM all_points
	-- WHERE ST_Intersects(geom, (SELECT * FROM shortpath));

	-- drop table if exists point_in_buffer;
	-- create table point_in_buffer as SELECT *  FROM all_points
	-- WHERE ST_DWithin(geography(geom), geography((SELECT * FROM shortpath)), 0.5);
	
	drop table if exists point_in_buffer;
	create temp table point_in_buffer as SELECT * FROM all_points
    WHERE ST_Within(geom, (SELECT ST_Buffer((SELECT * FROM shortpath),0.000001,'endcap=round join=round')));
    create index point_in_buffer_idx on point_in_buffer using GIST(geom);

	-- 选取shortpath 0.5m缓冲区内的所有停止点.
	drop table if exists stop_point_in_buffer;
	create temp table stop_point_in_buffer as SELECT * FROM stop_points 
    WHERE ST_DWithin(geography(geom), geography((SELECT * FROM shortpath)), 0.5);
    create index stop_point_in_buffer_idx on stop_point_in_buffer using GIST(geom);
	-- 选取shortpath 0.5m缓冲区内的所有起始点.
	drop table if exists begin_point_in_buffer;
	create temp table begin_point_in_buffer as SELECT * FROM begin_points 
    WHERE ST_DWithin(geography(geom), geography((SELECT * FROM shortpath)), 0.5);
    create index begin_point_in_buffer_idx on begin_point_in_buffer using GIST(geom);
	

	-- 通过最近邻搜索, 获取属性点.
	-- 这里隐含了对result_path_points_meta的排序(以result_points_tbl为准)
	drop table if exists result_path_points_meta;
	create temp table result_path_points_meta as 
	SELECT p.id, b.p_id, p.lon, p.lat, b.utmx, b.utmy ,b.heading, b.curv, b.mode, b.speed_mode, 
	b.opposite_side_Mode, b.event_mode, b.lane_num, b.lane_seq, b.lane_width, p.point
	FROM result_points_tbl p 
	CROSS JOIN LATERAL ( 
	SELECT * 
	FROM point_in_buffer r 
	ORDER BY r.geom <-> p.point
	LIMIT 1 
	)b;

	-- 搜索停止点id
	drop table if exists stop_point_candidate;
	create temp table stop_point_candidate as 
	SELECT b.id
	FROM stop_point_in_buffer p 
	CROSS JOIN LATERAL ( 
	SELECT * FROM result_path_points_meta r 
	ORDER BY p.geom <-> r.point LIMIT 1 )b;
	-- 搜索起始点(路口结束, 新路段开启点)
	drop table if exists begin_point_candidate;
	create temp table begin_point_candidate as 
	SELECT b.id
	FROM begin_point_in_buffer p 
	CROSS JOIN LATERAL ( 
	SELECT * FROM result_path_points_meta r 
	ORDER BY p.geom <-> r.point LIMIT 1 )b;
	-- 更新停止点
	-- 这个操作导致了停止点信息被改变, 排到了表的末尾.	停止线是3
	update result_path_points_meta set event_mode = 0 where event_mode = 1 or event_mode = 2;
	update result_path_points_meta a set event_mode = 1 from stop_point_candidate b where a.id = b.id;
	update result_path_points_meta a set event_mode = 2 from begin_point_candidate b where a.id = b.id;

	-- 存入最后的结果, 并排序.
	drop table if exists result_path_points;
	create temp table result_path_points as 
	select p.p_id, p.lon, p.lat, p.utmx, p.utmy, p.heading, p.curv, p.mode, p.speed_mode,
	p.event_mode, p.opposite_side_Mode, p.lane_num, p.lane_seq, p.lane_width
	from result_path_points_meta p order by p.id;

end;
$$;


ALTER FUNCTION public.line2points(tbl character varying) OWNER TO postgres;

--
-- Name: line2points(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE OR REPLACE FUNCTION public.line2points(tbl character varying, edge_ids integer[], out p__id bigint, out lon double precision, out lat double precision,
    out utmx real, out utmy real, out heading real, out curv real, out mode bigint, out speed_mode bigint,
    out event_mode bigint, out opposite_side_Mode bigint, out lane_num bigint, out lane_seq bigint, out lane_width real) RETURNS SETOF record
    LANGUAGE plpgsql STRICT
    AS $$ 
declare 
	n integer := array_length(edge_ids, 1);
    point_ids integer[];
begin
	-- path首尾相连，取所有的beginid和最后一个endid
    -- 参照edge_ids的顺序
    execute 'select array(select t.beginid from unnest($1) edge_id left join ' || tbl || ' t on edge_id = t.gid)'
        into point_ids using edge_ids;
    
    execute 'select $1 || (select endid from '|| tbl ||' where gid = $2 limit 1)'
        into point_ids using point_ids, edge_ids[n];
    
    return query select p.p_id, st_x(p.geom), st_y(p.geom), p.utmx, p.utmy, p.heading, p.curv, p.mode, p.speed_mode,
        p.event_mode, p.opposite_side_Mode, p.lane_num, p.lane_seq, p.lane_width from unnest(point_ids) point_id left join all_points_test p on point_id = p.p_id;

    return;
end;
$$;


ALTER FUNCTION public.line2points(tbl character varying, edge_ids integer[]) OWNER TO postgres;

--
-- Name: line2points(character varying, double precision, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE OR REPLACE FUNCTION public.line2points(tbl character varying, distance double precision, link_points_tbl character varying) RETURNS void
    LANGUAGE plpgsql STRICT
    AS $$ 
declare 
	ii integer; -- 循环次数
	n integer;
	point_temp geometry;--内插点 
	longtitude double precision;
	latitude double precision;
	id_temp integer;
	 
	begin
	drop table if exists result_points_tbl;
	CREATE TEMP TABLE result_points_tbl(id integer,point geometry,lon double precision ,lat double precision);

	execute 'select ST_NPoints(pgr_fromatob) from ' || tbl|| ' ' into n;

	ii:=1;
	FOR ii IN 1..n LOOP

	execute 'SELECT ST_PointN(pgr_fromatob, ' ||ii|| ') FROM ' ||tbl|| ' ' into point_temp;
		   
	SELECT st_x(point_temp) into longtitude;
	SELECT st_y(point_temp) into latitude;
	insert into result_points_tbl(id,point,lon,lat) values(ii,point_temp,longtitude,latitude);
	end loop;
end;
$$;


ALTER FUNCTION public.line2points(tbl character varying, distance double precision, link_points_tbl character varying) OWNER TO postgres;

--
-- Name: pgr_fromatob(character varying, double precision, double precision, double precision, double precision); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE OR REPLACE FUNCTION public.pgr_fromatob(tbl character varying, startx double precision, starty double precision, endx double precision, endy double precision) RETURNS public.geometry
    LANGUAGE plpgsql STRICT
    AS $$  
declare  
    v_startLine geometry;--离起点最近的线  
    v_endLine geometry;--离终点最近的线

    gid_start_line int;--离起点最近的线gid
    gid_end_line int;--离终点最近的线gid
      
    v_startTarget integer;--距离起点最近线的终点
    v_endSource integer;--距离终点最近线的起点
  
    v_statpoint geometry;--在v_startLine上距离起点最近的点  
    v_endpoint geometry;--在v_endLine上距离终点最近的点  
      
    v_res geometry;--最短路径分析结果  
  
    v_perStart float;--v_statpoint在v_startLine上的百分比  
    v_perEnd float;--v_endpoint在v_endLine上的百分比
    v_start_res geometry; -- 起点到起点线段终点的一段路
    v_end_res geometry; -- 终点线起点到终点的一段路
  
    v_shPath geometry;--最终结果
    tempnode float;
begin     

    --查询离起点最近的线
    execute 'select gid, the_geom ,target from ' ||tbl||
            ' where ST_DWithin(geog,ST_Geogfromtext(''SRID=4326; point('|| startx ||' ' || starty||')''),20) 
            order by ST_Distance(geog,ST_Geogfromtext(''SRID=4326; point('|| startx ||' ' || starty||')'')) limit 1'
            into gid_start_line, v_startLine ,v_startTarget;

    --查询离终点最近的线  
    execute 'select gid, the_geom, source from ' ||tbl||
            ' where ST_DWithin(geog,ST_Geogfromtext(''SRID=4326; point('|| endx ||' ' || endy||')''),15) 
            order by ST_Distance(geog,ST_Geogfromtext(''SRID=4326; point('|| endx ||' ' || endy||')'')) limit 1'
            into gid_end_line, v_endLine, v_endSource;
  
    --如果没找到最近的线，就返回null  
    if (v_startLine is null) or (v_endLine is null) then
        return null;  
    end if ;  
  
    select  ST_ClosestPoint(v_startLine, ST_Geometryfromtext('point('|| startx ||' ' || starty ||')',4326)) into v_statpoint;  
    select  ST_ClosestPoint(v_endLine, ST_GeometryFromText('point('|| endx ||' ' || endy ||')',4326)) into v_endpoint;  
  
    --最短路径
    drop table if exists dijk;
    execute 'create temp table dijk as
    select * from pgr_dijkstra(''SELECT gid as id, source, target, cost FROM '||tbl||''' ,' ||v_startTarget||','||v_endSource||', true);';
    -- 合并查询路线
    execute 'SELECT st_linemerge(st_union(b.the_geom)) 
    FROM dijk a,' || tbl || ' b WHERE a.edge = b.gid' into v_res;
    -- 初始化记录的edge id
    drop table if exists edge_id; --存储寻路的id
    create temp table edge_id(edge int);
    -- 记录路段id, 包括起点和终点所在的路段.
    insert into edge_id(edge) values(gid_start_line);
    insert into edge_id(edge) select edge from dijk;
    insert into edge_id(edge) values(gid_end_line);
    
    RAISE NOTICE 'v_res: %',v_res; 
    --如果找不到最短路径，就返回null  
    --if(v_res is null) then  
    --    return null;  
    --end if;

    --将v_res,v_start_res,v_end_res进行拼接, 作为最终结果
    select ST_LineLocatePoint(v_startLine, v_statpoint) into v_perStart;
    select ST_LineLocatePoint(v_endLine, v_endpoint) into v_perEnd;

    -- 如果起点终点在同一路段,且起点在终点之前:
    RAISE NOTICE 'v_perStartes: %, v_perEnd: %',v_perStart,  v_perEnd;
    if(gid_start_line = gid_end_line) and (v_perStart < v_perEnd) then
        SELECT ST_LineSubString(v_startLine, v_perStart, v_perEnd) into v_start_res;
        return v_start_res;
    end if;

    SELECT ST_LineSubString(v_startLine,v_perStart, 1) into v_start_res;
    SELECT ST_LineSubString(v_endLine,0, v_perEnd) into v_end_res;
    -- 正常merge
    select  st_linemerge(ST_Union(array[v_start_res, v_res, v_end_res])) into v_shPath;
    return v_shPath;
end;  
$$;


ALTER FUNCTION public.pgr_fromatob(tbl character varying, startx double precision, starty double precision, endx double precision, endy double precision) OWNER TO postgres;

--
-- Name: pgr_fromatob(character varying, double precision, double precision, double precision, double precision); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE OR REPLACE FUNCTION public.pgr_fromatob_test(tbl character varying, startx double precision, starty double precision, endx double precision, endy double precision) RETURNS integer[]
    LANGUAGE plpgsql STRICT
    AS $$  
declare  
    v_startLine geometry;--离起点最近的线  
    v_endLine geometry;--离终点最近的线

    gid_start_line int;--离起点最近的线gid
    gid_end_line int;--离终点最近的线gid
      
    v_startTarget integer;--距离起点最近线的终点
    v_endSource integer;--距离终点最近线的起点
  
    v_statpoint geometry;--在v_startLine上距离起点最近的点  
    v_endpoint geometry;--在v_endLine上距离终点最近的点  

    dijk integer[];--结果的edgeid数组
begin     

    --查询离起点最近的线
    execute 'select gid, the_geom ,target from ' ||tbl||
        ' where ST_DWithin(geog,ST_Geogfromtext(''SRID=4326; point('|| startx ||' ' || starty||')''),20) 
        order by ST_Distance(geog,ST_Geogfromtext(''SRID=4326; point('|| startx ||' ' || starty||')'')) limit 1'
        into gid_start_line, v_startLine ,v_startTarget;

    --查询离终点最近的线  
    execute 'select gid, the_geom, source from ' ||tbl||
        ' where ST_DWithin(geog,ST_Geogfromtext(''SRID=4326; point('|| endx ||' ' || endy||')''),15) 
        order by ST_Distance(geog,ST_Geogfromtext(''SRID=4326; point('|| endx ||' ' || endy||')'')) limit 1'
        into gid_end_line, v_endLine, v_endSource;
  
    --如果没找到最近的线，引发错误  
    if (v_startLine is null) or (v_endLine is null) then
        RAISE EXCEPTION  'lines closest to begin point or end point not fonud';
    end if ;  
  
    -- select  ST_ClosestPoint(v_startLine, ST_Geometryfromtext('point('|| startx ||' ' || starty ||')',4326)) into v_statpoint;  
    -- select  ST_ClosestPoint(v_endLine, ST_GeometryFromText('point('|| endx ||' ' || endy ||')',4326)) into v_endpoint;  
  
    --最短路径edgeid数组
    execute 'select array(select edge::integer from pgr_dijkstra(''SELECT gid as id, source, target, cost FROM '||tbl||''' ,' ||v_startTarget||','||v_endSource||', true));'
        into dijk using gid_start_line, gid_end_line;
    -- -1表示最后一个结点
    dijk:=array_replace(dijk, -1, gid_end_line);
	-- raise notice 'start %, end %', gid_start_line, gid_end_line;
	-- raise notice 'dijk %', dijk;
    return dijk;
end;  
$$;


ALTER FUNCTION public.pgr_fromatob_test(tbl character varying, startx double precision, starty double precision, endx double precision, endy double precision) OWNER TO postgres;

--
-- Name: pgr_fromatob_unnormal(character varying, character varying, double precision, double precision, double precision, double precision, double precision, double precision); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE OR REPLACE FUNCTION public.pgr_fromatob_unnormal(tbl character varying, vertices character varying, blockedx double precision, blockedy double precision, startx double precision, starty double precision, endx double precision, endy double precision) RETURNS public.geometry
    LANGUAGE plpgsql STRICT
    AS $$  
declare  
    v_startLine geometry;--离起点最近的线  
    v_endLine geometry;--离终点最近的线  
    v_blockedLine geometry; --堵塞的线
      
    v_startTarget integer;--距离起点最近线的终点  
    v_startSource integer;--距离起点最近线的起点
    v_endSource integer;--距离终点最近线的起点  
    v_bloackedTarget integer;--距离堵塞点最近线的终点
    
    v_startSourceGeom geometry;
    v_blockedTargetGeom geometry;
    v_linebetweenBTSS geometry;
  
    v_statpoint geometry;--在v_startLine上距离起点最近的点  
    v_endpoint geometry;--在v_endLine上距离终点最近的点  
    v_blockedpoint geometry; --在v_blockedLine上距离堵塞点最近的点
      
    v_res geometry;--最短路径分析结果 
    v_shPathAdd geometry;--合并额外道路
    v_startLineRes geometry;--startLine上的额外道路
    v_blockedLineRes geometry;--blockedLine上的额外道路
  
    v_perStart float;--v_statpoint在v_res上的百分比  
    v_perEnd float;--v_endpoint在v_res上的百分比  
    v_perBlockedPoint float;
    v_perStartLineStart float;

  
    v_shPath geometry;--最终结果
    tempnode float; 
begin     
          
    --查询离起点最近的线   
    execute 'select the_geom ,target, source  from ' ||tbl||
            ' where 
            ST_DWithin(the_geom,ST_Geometryfromtext(''point('|| startx ||' ' || starty||')'',4326),15) 
            order by ST_Distance(the_geom,ST_GeometryFromText(''point('|| startx ||' '|| starty ||')'',4326))  limit 1' 
            into v_startLine ,v_startTarget, v_startSource; 

    execute 'select the_geom from ' || vertices ||
            ' where id = ' || v_startSource
            into v_startSourceGeom;
      
    --查询离终点最近的线  
    execute 'select the_geom,source  from ' ||tbl||
            ' where ST_DWithin(the_geom,ST_Geometryfromtext(''point('|| endx || ' ' || endy ||')'',4326),15) 
            order by ST_Distance(the_geom,ST_GeometryFromText(''point('|| endx ||' ' || endy ||')'',4326))  limit 1' 
            into v_endLine,v_endSource; 

    --查询离堵塞点最近的线 
    execute 'select the_geom,target  from ' ||tbl||
            ' where ST_DWithin(the_geom,ST_Geometryfromtext(''point('|| blockedx || ' ' || blockedy ||')'',4326),15) 
            order by ST_Distance(the_geom,ST_GeometryFromText(''point('|| blockedx ||' ' || blockedy ||')'',4326))  limit 1' 
            into v_blockedLine,v_bloackedTarget; 

    execute 'select the_geom from ' || vertices ||
            ' where id = ' || v_bloackedTarget
            into v_blockedTargetGeom;
  
    --如果没找到最近的线，就返回null  
    if (v_startLine is null) or (v_endLine is null) then  
        return null;  
    end if;  
  
    select  ST_ClosestPoint(v_startLine, ST_Geometryfromtext('point('|| startx ||' ' || starty ||')',4326)) into v_statpoint;  
    select  ST_ClosestPoint(v_endLine, ST_GeometryFromText('point('|| endx ||' ' || endy ||')',4326)) into v_endpoint;  
    select  ST_ClosestPoint(v_blockedLine, ST_GeometryFromText('point('|| blockedx ||' ' || blockedy ||')',4326)) into v_blockedpoint;
  
      
    --最短路径  
    execute 'SELECT st_linemerge(st_union(b.the_geom))' ||
    'FROM pgr_dijkstra(  
    ''SELECT gid as id, source, target, length as cost FROM ' || tbl ||''' ,' ||
    v_startTarget || ',' ||v_endSource || ', true  ) a,' || tbl || ' b    
    WHERE a.edge = b.gid'
    into v_res;  
  
    --如果找不到最短路径，就返回null  
    if(v_res is null) then  
        return null;  
    end if;  
      
    --将v_res,v_startLine,v_endLine进行拼接  
    select  st_linemerge(ST_Union(array[v_res,v_startLine,v_endLine])) into v_res;  
      
    select  ST_LineLocatePoint(v_res, v_statpoint) into v_perStart;  
    select  ST_LineLocatePoint(v_res, v_endpoint) into v_perEnd;  
    select  ST_LineLocatePoint(v_blockedLine, v_blockedpoint) into v_perBlockedPoint;  
    select  ST_LineLocatePoint(v_startLine, v_statpoint) into v_perStartLineStart;
    select  ST_MakeLine(v_blockedTargetGeom, v_startSourceGeom) into v_linebetweenBTSS;
    
    if(v_perStart > v_perEnd) then  
        tempnode =  v_perStart;
        v_perStart = v_perEnd;
        v_perEnd = tempnode;
    end if;

    --截取v_res  
    SELECT ST_LineSubString(v_res,v_perStart, v_perEnd) into v_shPath;
    SELECT ST_LineSubString(v_blockedLine,v_perBlockedPoint,1.0) into v_blockedLineRes;
    SELECT ST_LineSubString(v_startLine,0.0,v_perStartLineStart) into v_startLineRes;

    SELECT st_linemerge(ST_Union(array[v_blockedLineRes,v_linebetweenBTSS,v_startLineRes,v_shPath])) into v_shPathAdd;
        
    return v_shPathAdd;  

end;  
$$;


ALTER FUNCTION public.pgr_fromatob_unnormal(tbl character varying, vertices character varying, blockedx double precision, blockedy double precision, startx double precision, starty double precision, endx double precision, endy double precision) OWNER TO postgres;

--
-- Name: pgr_pointoffset(character varying, double precision, double precision); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE OR REPLACE FUNCTION public.pgr_pointoffset(tbl character varying, blockedx double precision, blockedy double precision) RETURNS SETOF double precision
    LANGUAGE plpgsql STRICT
    AS $$  
declare  
    v_blockedLine geometry;--被阻断的线 
    point_in_line geometry;
    endpoint geometry;
    targetpoint geometry;
    id integer;--被阻断的线  
    start_x float;
    start_y float;
    sin float;
    cos float;
    end_x float;
    end_y float;
    target_x float;
    target_y float;
begin
    --查询离起点最近的线  
    execute 'select the_geom ,gid  from '|| tbl ||' 
                where 
            ST_DWithin(the_geom,ST_Geometryfromtext(''point('|| blockedx ||' ' || blockedy||')'',4326),15) 
            order by ST_Distance(the_geom,ST_GeometryFromText(''point('|| blockedx ||' '|| blockedy ||')'',4326))  limit 1' 
            into v_blockedLine ,id;  

    -- 得到线上距离阻塞点最近的一点
    select ST_LineInterpolatePoint(v_blockedLine, ST_LineLocatePoint(v_blockedLine ,ST_GeometryFromText('point('|| blockedx ||' ' || blockedy ||')',4326))) into point_in_line;
    --select ST_LineInterpolatePoint( v_blockedLine ||, ST_LineLocatePoint( v_blockedLine ||',''point('|| blockedx ||' ' || blockedy ||')''))' into point_in_line;
    --execute 'select ST_LineInterpolatePoint('|| v_blockedLine ||', ST_LineLocatePoint('|| v_blockedLine ||',ST_GeometryFromText(''point('|| blockedx ||' ' || blockedy ||')'',4326)))' into point_in_line;
    select st_x(ST_Transform(point_in_line,900913)) into start_x;
    select st_y(ST_Transform(point_in_line,900913)) into start_y;

    select ST_EndPoint(v_blockedLine) into  endpoint;
    select st_x(ST_Transform(endpoint,900913)) into end_x;
    select st_y(ST_Transform(endpoint,900913)) into end_y;

    sin = (end_x - start_x) / (|/ ((start_y - end_y)^2+(start_x - end_x)^2));
    cos = -(end_y - start_y) / (|/ ((start_y - end_y)^2+(start_x - end_x)^2));


    target_x = start_x + 10*cos;
    target_y = start_y + 10*sin;

    select ST_Transform(ST_Geometryfromtext('point('|| target_x ||' ' || target_y||')',900913),4326) into targetpoint;
    select st_x(targetpoint) into target_x;
    select st_y(targetpoint) into target_y;


    return next target_x;
    return next target_y;
    --return targetpoint;

end;  
$$;


ALTER FUNCTION public.pgr_pointoffset(tbl character varying, blockedx double precision, blockedy double precision) OWNER TO postgres;

--
-- Name: pgr_updatetopomap(character varying, double precision, double precision); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE OR REPLACE FUNCTION public.pgr_updatetopomap(tbl character varying, blockedx double precision, blockedy double precision) RETURNS void
    LANGUAGE plpgsql STRICT
    AS $$  
declare  
    v_blockedLine geometry;--被阻断的线 
       
    id integer;--被阻断的线  
begin     
          
    --查询离起点最近的线  
    execute 'select the_geom ,gid  from '||tbl||' where 
            ST_DWithin(the_geom,ST_Geometryfromtext(''point('|| blockedx ||' ' || blockedy||')'',4326),15) 
            order by ST_Distance(the_geom,ST_GeometryFromText(''point('|| blockedx ||' '|| blockedy ||')'',4326))  limit 1' 
            into v_blockedLine ,id;  

    execute 'UPDATE '||tbl||'
            SET cost = 99999
            WHERE gid = '||id||' ';
  
      
end;  
$$;


ALTER FUNCTION public.pgr_updatetopomap(tbl character varying, blockedx double precision, blockedy double precision) OWNER TO postgres;
