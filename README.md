# Routing Service

提供高性能的一次规划服务，使用`docker run -d --network host --name routing-service routing-service`启动

在server多客户端连接时存在并发上锁的情形，多客户端会被阻塞 需等待



循环间隔数\客户端个数		1		2		3		5

1s			             2s~3s                       3~5s                      后续6~8s              后续12~13s

3s			             2s~3s                       2~5s                      后续4~8s              后续10~13s

5s			             2s~3s	             2~3s                      后续5~8s               后续8~13s 