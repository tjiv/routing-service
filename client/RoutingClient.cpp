#include <grpcpp/grpcpp.h>

#include <fstream>
#include <iomanip>
#include <iostream>
#include <memory>
#include <string>
#include <thread>
#include <vector>

#include "routing_service.grpc.pb.h"

using grpc::Channel;
using grpc::ClientContext;
using grpc::ClientWriter;
using grpc::Status;
using routing_service::RefRoad;
using routing_service::RefRoadPoint;
using routing_service::RoutingService;
using routing_service::TaskPoints;

using routing_service::CarInfo;
// using routing_service::CarInfoList;
using routing_service::MapService;
using routing_service::Point;
using routing_service::RoadPoints;
using routing_service::TaskPoint;
using routing_service::TaskRequest;

using google::protobuf::Empty;
using google::protobuf::StringValue;

class RoutingClient
{
public:
	RoutingClient(std::shared_ptr<Channel> channel)
		: stub_(RoutingService::NewStub(channel)),
		  map_stub(MapService::NewStub(channel)),
		  map("simu_add2"),
		  running(false) {}
	void readTaskPoints(const std::vector<std::pair<double, double>> &points)
	{
		task_points = points;
		//设置第一个点为初始点
		lon = task_points[0].first;
		lat = task_points[0].second;
	}
	int requestUpdateReferenceRoad(const std::vector<std::pair<double, double>> &task_points)
	{
		if (task_points.size() < 2)
		{
			std::cerr << "not enough points" << std::endl;
			return -1;
		}
		//调用service，处理返回结果
		TaskPoints request;
		for (const auto &p : task_points)
		{
			auto point = request.add_task_point();
			point->set_lon(p.first);
			point->set_lat(p.second);
		}
		//指定使用的地图
		request.set_map(map);
		ClientContext context;
		RefRoad response;
		Status status = stub_->RequestUpdateReferenceRoad(&context, request, &response);
		// TODO: 获取time cost，路点写入文件
		if (status.ok())
		{
			std::cout << "RPC success" << std::endl;
			int sum_costs = response.time_cost();
			std::cout << "estimated time cost: " << sum_costs << "s, "
					  << sum_costs / 60 << "mins" << std::endl;
			size_t size = response.point_size();
			std::cout << "id lon lat utmx utmy heading curv mode speed_mode "
						 "event_mode opposite_side_mode lane_num lane_seq lane_width"
					  << std::endl;
			// 同时写入文件
			std::ofstream outfile("routing_result");
			for (size_t i = 0; i < size; i++)
			{
				auto p = response.point(i);
				// std::cout << p.id() << " " << p.lon() << " " << p.lat() << " "
				//           << p.utmx() << " " << p.utmy() << " " << p.heading() << " "
				//           << p.curv() << " " << p.mode() << " " << p.speed_mode() <<
				//           " "
				//           << p.event_mode() << " " << p.opposite_side_mode() << " "
				//           << p.lane_num() << " " << p.lane_seq() << " "
				//           << p.lane_width() << std::endl;
				outfile << std::fixed << std::setprecision(7) << p.lon() << " "
						<< p.lat() << std::endl;
			}
			return sum_costs;
		}
		else
		{
			std::cerr << "RPC failed" << std::endl;
			return -1;
		}
	}
	int findReferenceRoad(const std::vector<std::pair<double, double>> &task_points, bool blocked = false)
	{
		if (task_points.size() < 2)
		{
			std::cerr << "not enough points" << std::endl;
			return -1;
		}
		//调用service，处理返回结果
		TaskPoints request;
		request.set_blocked(blocked);
		for (const auto &p : task_points)
		{
			auto point = request.add_task_point();
			point->set_lon(p.first);
			point->set_lat(p.second);
		}
		//指定使用的地图
		request.set_map(map);
		ClientContext context;
		RefRoad response;
		Status status = stub_->FindReferenceRoad(&context, request, &response);
		// TODO: 获取time cost，路点写入文件
		if (status.ok())
		{
			std::cout << "RPC success" << std::endl;
			int sum_costs = response.time_cost();
			std::cout << "estimated time cost: " << sum_costs << "s, "
					  << sum_costs / 60 << "mins" << std::endl;
			size_t size = response.point_size();
			std::cout << "id lon lat utmx utmy heading curv mode speed_mode "
						 "event_mode opposite_side_mode lane_num lane_seq lane_width"
					  << std::endl;
			// 同时写入文件
			std::ofstream outfile("routing_result");
			for (size_t i = 0; i < size; i++)
			{
				auto p = response.point(i);
				// std::cout << p.id() << " " << p.lon() << " " << p.lat() << " "
				//           << p.utmx() << " " << p.utmy() << " " << p.heading() << " "
				//           << p.curv() << " " << p.mode() << " " << p.speed_mode() <<
				//           " "
				//           << p.event_mode() << " " << p.opposite_side_mode() << " "
				//           << p.lane_num() << " " << p.lane_seq() << " "
				//           << p.lane_width() << std::endl;
				outfile << std::fixed << std::setprecision(7) << p.lon() << " "
						<< p.lat() << std::endl;
			}
			return sum_costs;
		}
		else
		{
			std::cerr << "RPC failed" << std::endl;
			return -1;
		}
	}

	void updateCarInfo()
	{
		ClientContext context;
		Empty empty;
		// 循环，更新车辆状态
		std::unique_ptr<ClientWriter<CarInfo>> writer(
			map_stub->UpdateCarInfo(&context, &empty));
		while (true)
		{
			CarInfo info;
			auto pos = info.mutable_pos();
			{
				std::lock_guard<std::mutex> lk(car_info_mutex);
				info.set_map(map);
				pos->set_lon(lon);
				pos->set_lat(lat);
				info.set_running(running);
			}
			if (!writer->Write(info))
			{
				std::cerr << "update car info failed" << std::endl;
				return;
			}
			std::cout << "update car info success, running: " << running << std::endl;
			std::this_thread::sleep_for(std::chrono::seconds(2));
		}
	}

	void waitTaskPoint()
	{
		TaskPoint res;
		Empty empty;
		std::pair<double, double> start, end;
		while (true)
		{
			ClientContext context;
			Status status = map_stub->WaitForTaskPoint(&context, empty, &res);
			if (status.ok())
			{
				{
					std::lock_guard<std::mutex> lk(car_info_mutex);
					start.first = lon;
					start.second = lat;
					running = true;
				}
				end.first = res.lon();
				end.second = res.lat();
				//执行任务
				std::this_thread::sleep_for(std::chrono::seconds(5));
				executeTask(start, end);
				//结束后修改标志
				{
					std::lock_guard<std::mutex> lk(car_info_mutex);
					running = false;
				}
				std::this_thread::sleep_for(std::chrono::seconds(5));
			}
			else
			{
				std::cerr << "car get task point failed" << std::endl;
				return;
			}
		}
	}

	void callCar()
	{
		CarInfo info;
		Point on_car_pos;
		on_car_pos.set_lon(task_points[1].first);
		on_car_pos.set_lat(task_points[1].second);
		ClientContext context;
		Status status = map_stub->GetOneAvailableCar(&context, on_car_pos, &info);
		if (status.ok())
		{
			id = info.id();
			Point pos = info.pos();
			std::cout << "get one available car success, id: " << id
					  << ", lon: " << pos.lon() << ", lat: " << pos.lat()
					  << std::endl;
			// 发送第一个任务
			TaskRequest req;
			req.set_id(id);
			req.set_map(info.map());
			req.set_on_or_off(false);
			auto p = req.mutable_point();
			p->set_lon(on_car_pos.lon());
			p->set_lat(on_car_pos.lat());
			RoadPoints road;
			ClientContext new_context;
			Status status = map_stub->SendTaskPoint(&new_context, req, &road);
			if (!status.ok())
			{
				std::cerr << "send on car task failed" << std::endl;
			}
		}
		else
		{
			std::cerr << "get one available car failed" << std::endl;
		}
	}

	void getOnCar()
	{
		// 等待车辆状态变为 running == false
		StringValue id_str;
		id_str.set_value(id);
		CarInfo info;
		while (true)
		{
			ClientContext context;
			Status status = map_stub->GetCarInfoById(&context, id_str, &info);
			if (!status.ok())
			{
				std::cerr << "get car info by id failed" << std::endl;
				return;
			}
			if (info.running() == false)
				break;
			std::this_thread::sleep_for(std::chrono::seconds(1));
		}
		// //结束上个任务
		// Empty empty;
		// ClientContext tmp_context;
		// Status status = map_stub->FinishTask(&tmp_context, id_str, &empty);
		// if (!status.ok()) {
		//   std::cerr << "finish on car task failed" << std::endl;
		//   return;
		// }
		std::cout << "get on car success" << std::endl;
		// 发送下一个任务
		TaskRequest req;
		req.set_id(id);
		req.set_map(map);
		req.set_on_or_off(true);
		auto p = req.mutable_point();
		p->set_lon(task_points[2].first);
		p->set_lat(task_points[2].second);
		RoadPoints road;
		ClientContext new_context;
		Status status = map_stub->SendTaskPoint(&new_context, req, &road);
		if (!status.ok())
		{
			std::cerr << "send off car task failed" << std::endl;
		}
	}

	void getOffCar()
	{
		// 再次等待车辆的状态变为 running == false
		StringValue id_str;
		id_str.set_value(id);
		CarInfo info;
		while (true)
		{
			ClientContext context;
			Status status = map_stub->GetCarInfoById(&context, id_str, &info);
			if (!status.ok())
			{
				std::cerr << "get car info by id failed" << std::endl;
				return;
			}
			if (info.running() == false)
				break;
			std::this_thread::sleep_for(std::chrono::seconds(1));
		}
		// // finish task
		// Empty empty;
		// ClientContext new_context;
		// Status status = map_stub->FinishTask(&new_context, id_str, &empty);
		// if (status.ok()) {
		//   std::cout << "car task success" << std::endl;
		// } else {
		//   std::cerr << "can't finish task" << std::endl;
		// }
	}

private:
	std::unique_ptr<RoutingService::Stub> stub_;
	std::unique_ptr<MapService::Stub> map_stub;

	//模拟车辆信息
	std::mutex car_info_mutex;
	std::string map; //所使用地图
	double lon, lat; //位置
	bool running;
	std::vector<std::pair<double, double>> task_points;

	//模拟客户端存储的信息
	std::string id;

	void executeTask(const std::pair<double, double> &start,
					 const std::pair<double, double> &end)
	{
		//获取reference road
		std::vector<std::pair<double, double>> task{start, end};
		int cost = findReferenceRoad(task);
		if (cost == -1)
		{
			std::cerr << "find refernece road failed" << std::endl;
			return;
		}
		double lon, lat;
		std::ifstream infile("routing_result");
		while (infile >> lon >> lat)
		{
			{
				std::lock_guard<std::mutex> lk(car_info_mutex);
				this->lon = lon;
				this->lat = lat;
				std::cout << "now car at: " << std::fixed << std::setprecision(7)
						  << this->lon << " " << this->lat << std::endl;
			}
			// std::this_thread::sleep_for(std::chrono::milliseconds(500));
		}
		std::cout << "execute task success" << std::endl;
	}
};

int main(int argc, char **argv)
{
	std::string target_str = "localhost:50051";
	RoutingClient client(
		grpc::CreateChannel(target_str, grpc::InsecureChannelCredentials()));

	//测试规划
	std::ifstream file("task_points.txt");
	if (!file)
	{
		std::cerr << "open file error" << std::endl;
		return -1;
	}
	std::vector<std::pair<double, double>> task_points;
	double lon, lat;
	while (file >> lon >> lat)
		task_points.push_back({lon, lat});
	//测试规划时间
	for (int i = 1; i <= 1; ++i)
	{
		auto start = std::chrono::steady_clock::now();
		client.findReferenceRoad({task_points[1], task_points[2]});
		auto end = std::chrono::steady_clock::now();
		std::cout << i << "nd, time: "
				  << std::chrono::duration_cast<std::chrono::milliseconds>(end -
																		   start)
						 .count()
				  << "ms" << std::endl;
		std::this_thread::sleep_for(std::chrono::milliseconds(500));
	}
	// client.readTaskPoints(task_points);
	// // 线程不断更新车辆状态
	// std::thread update_thread(&RoutingClient::updateCarInfo, &client);
	// // 模拟车辆的动作：等待客户端任务，然后执行
	// std::thread car_thread(&RoutingClient::waitTaskPoint, &client);
	// // 模拟客户端，依次呼叫车辆、上车并发送终点位置、下车并结束任务
	// client.callCar();
	// client.getOnCar();
	// client.getOffCar();
	// update_thread.join();
	// car_thread.join();
	return 0;
}