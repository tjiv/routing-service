#!/bin/bash
# set -e使得出现错误时停止，set -x输出脚本执行内容
set -ex
cd build/server
./routing_server > tmp.log 2>&1 &
# 打印上一条命令的pid
echo $!
