cmake_minimum_required(VERSION 3.0.0)
project(HelloWorld)

add_subdirectory(protos)

add_subdirectory(server)

add_subdirectory(client)